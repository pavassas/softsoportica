<?php
	error_reporting(0);
	include('data/Conexion.php');
	require("Classes/PHPMailer-master/class.phpmailer.php");
	date_default_timezone_set('America/Bogota');
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_SESSION['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];
	
	// verifica si no se ha loggeado
	if(!isset($_SESSION["persona"]))
	{
	  session_destroy();
	  header("LOCATION:index.php");
	}else{
	}
	
	/*$year = date('Y');
	$week = 53;
	$fechaInicioSemana  = date('d-m-Y', strtotime($year . 'W' . str_pad($week , 2, '0', STR_PAD_LEFT)));
	$fecha_lunes = $fechaInicioSemana; //Lunes
	$fecha_martes = date('d-m-Y', strtotime($fechaInicioSemana.' 1 day')); //Martes
	$fecha_miercoles = date('d-m-Y', strtotime($fechaInicioSemana.' 2 day')); //Miercoles
	$fecha_jueves = date('d-m-Y', strtotime($fechaInicioSemana.' 3 day')); //Jueves
	$fecha_viernes = date('d-m-Y', strtotime($fechaInicioSemana.' 4 day')); //Viernes
	$fecha_sabado = date('d-m-Y', strtotime($fechaInicioSemana.' 5 day')); //Sabado
	$fecha_domingo = date('d-m-Y', strtotime($fechaInicioSemana.' 6 day'));; //Domingo*/
	
	//echo $fechaInicioSemana." / ".$fecha_martes." / ".$fecha_miercoles." / ".$fecha_jueves." / ".$fecha_viernes." / ".$fecha_sabado." / ".$fecha_domingo;
	
	//ALERTAS INACTIVAS A PETICION DE CAMILO EL DIA 30 DE OCTUBRE DE 2015
	/*
	//BUSCO LOS USUARIOS QUE SE HAN PASADO DE 193 HORAS en el mes actual
	//Nomina
	$ano = date('Y');
	$mes = date('m');
	$fi = $ano."-".$mes.'-01';
	$con = mysqli_query($conectar,"select LAST_DAY('".$fi."') ultimodia from usuario limit 1");
	$dato = mysqli_fetch_array($con);
	$ff = $dato['ultimodia'];
	$con = mysqli_query($conectar,"select COUNT(*) horas,u.usu_clave_int,u.usu_nombre,u.usu_email from horas_h hh inner join horas_d hd on (hd.hoh_clave_int = hh.hoh_clave_int) inner join usuario u on (u.usu_clave_int = hh.usu_clave_int) inner join cliente c on (c.cli_clave_int = hd.cli_clave_int) inner join proyecto p on (p.pro_clave_int = hd.pro_clave_int) where hh.hoh_fecha BETWEEN '".$fi."' and '".$ff."' and u.fac_clave_int != 1 and u.usu_sw_activo = 1 and u.usu_clave_int not in (select usu_clave_int from notificacion where not_ano = '".$ano."' and not_mes = '".$mes."') GROUP BY hh.usu_clave_int HAVING COUNT(*) > 193");
	$num = mysqli_num_rows($con);
	
	for($i = 0; $i < $num; $i++)
	{
		$dato = mysqli_fetch_array($con);
		$horas = $dato['horas'];
		$clausu = $dato['usu_clave_int'];
		if($horas > 193)
		{
			$mail = new PHPMailer();

			$mail->Body = '';
			$ema = '';
			$nom = $dato['usu_nombre'];
			$ema = $dato['usu_email'];

			$mail->From = "admin@pavastecnologia.com";
			$mail->FromName = "Soportica";
			$mail->AddAddress($ema, "Destino");
			$mail->Subject = "Alerta. Ha registrado mas de 193 horas en este mes";

			// Cuerpo del mensaje
			$mail->Body .= "Hola ".$nom."!\n\n";
			$mail->Body .= "HOJAS DE TIEMPOS registra que ha ingresado mas de 193 horas en este mes.\n";
			$mail->Body .= date("d/m/Y H:m:s")."\n\n";
			$mail->Body .= "Este mensaje es generado automáticamente por HOJAS DE TIEMPOS, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.co/Horas \n";
			if (!$mail->Send()) {
                //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
            } else {
                mysqli_query($conectar,"insert into notificacion(not_ano,not_mes,not_sw_enviado,usu_clave_int) values('".$ano."','".$mes."','1','".$clausu."')");
            }
		}
	}
	
	//Prestador de servicio
	$ano = date('Y');
	$mes = date('m');
	if($mes == 01 or $mes == 1)
	{
		$newano = $ano-1;
		$newmes = 12;
	}
	else
	{
		$newano = $ano;
		$newmes = $mes-1;
	}
	$fi = $newano."-".$newmes.'-20';
	$ff = $ano."-".$mes.'-19';
	$con = mysqli_query($conectar,"select COUNT(*) horas,u.usu_clave_int,u.usu_nombre,u.usu_email from horas_h hh inner join horas_d hd on (hd.hoh_clave_int = hh.hoh_clave_int) inner join usuario u on (u.usu_clave_int = hh.usu_clave_int) inner join cliente c on (c.cli_clave_int = hd.cli_clave_int) inner join proyecto p on (p.pro_clave_int = hd.pro_clave_int) where hh.hoh_fecha BETWEEN '".$fi."' and '".$ff."' and u.fac_clave_int = 1 and u.usu_sw_activo = 1 and u.usu_clave_int not in (select usu_clave_int from notificacion where not_ano = '".$ano."' and not_mes = '".$mes."') GROUP BY hh.usu_clave_int HAVING COUNT(*) > 193");
	$num = mysqli_num_rows($con);
	for($i = 0; $i < $num; $i++)
	{
		$dato = mysqli_fetch_array($con);
		$horas = $dato['horas'];
		$clausu = $dato['usu_clave_int'];
		if($horas > 193)
		{
			$mail = new PHPMailer();

			$mail->Body = '';
			$ema = '';
			$nom = $dato['usu_nombre'];
			$ema = $dato['usu_email'];

			$mail->From = "admin@pavastecnologia.com";
			$mail->FromName = "Soportica";
			$mail->AddAddress($ema, "Destino");
			$mail->Subject = "Alerta. Ha registrado mas de 193 horas en este mes";

			// Cuerpo del mensaje
			$mail->Body .= "Hola ".$nom."!\n\n";
			$mail->Body .= "HOJAS DE TIEMPOS registra que ha ingresado mas de 193 horas en este mes.\n";
			$mail->Body .= date("d/m/Y H:m:s")."\n\n";
			$mail->Body .= "Este mensaje es generado automáticamente por HOJAS DE TIEMPOS, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.co/Horas \n";
			if (!$mail->Send()) {
                //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
            } else {
                mysqli_query($conectar,"insert into notificacion(not_ano,not_mes,not_sw_enviado,usu_clave_int) values('".$ano."','".$mes."','1','".$clausu."')");
            }
		}
	}*/
	/*
	//VERIFICO SI EL USUARIO HA TENIDO ACTIVIDAD LOS ULTIMOS 3 DIAS
	$con = mysqli_query($conectar,"select * from usuario u left outer join log_actividades l on (l.loa_usu_actualiz = u.usu_usuario) where (DATEDIFF(CURDATE(),loa_fec_actualiz) >= 3 or u.usu_usuario not in (select loa_usu_actualiz from log_actividades)) and (usu_fec_notifica_actividad != CURDATE() or usu_fec_notifica_actividad is null or usu_fec_notifica_actividad = '' or usu_fec_notifica_actividad = '0000-00-00') and (u.usu_sw_activo = 1) and (u.usu_email != '') GROUP BY u.usu_clave_int");
	$num = mysqli_num_rows($con);
	for($i = 0; $i < $num; $i++)
	{
		$dato = mysqli_fetch_array($con);
		$mail = new PHPMailer();

		$mail->Body = '';
		$ema = '';
		$clausu = $dato['usu_clave_int'];
		$nom = $dato['usu_nombre'];
		$ema = $dato['usu_email'];

		$mail->From = "admin@pavastecnologia.com";
		$mail->FromName = "Soportica";
		$mail->AddAddress($ema, "Destino");
		$mail->Subject = "Alerta. No se ha registrado actividad hace mas de dos dias";

		// Cuerpo del mensaje
		$mail->Body .= "Hola ".$nom."!\n\n";
		$mail->Body .= "HOJAS DE TIEMPOS le informa que no hemos recibido actualizacion del sistema hace mas de dos dias, lo invitamos a ingresar al aplicativo y actualizar sus actividades.\n";
		$mail->Body .= date("d/m/Y H:m:s")."\n\n";
		$mail->Body .= "Este mensaje es generado automáticamente por HOJAS DE TIEMPOS, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.co/Horas \n";
		if (!$mail->Send()) {
            //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
            //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
        } else {
            mysqli_query($conectar,"update usuario set usu_fec_notifica_actividad = CURDATE() where usu_clave_int = '".$clausu."'");
        }
	}*/
?>
<?php
	$cantsw = 0;
	$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
	$dato = mysqli_fetch_array($con);
	$perfil = $dato['prf_descripcion'];
	$percla = $dato['prf_clave_int'];
	$claveusuario = $dato['usu_clave_int'];
	$nomusu = substr($dato['usu_nombre'], 0, 20);
	$fecha=date("Y/m/d H:i:s");
	
	$con = mysqli_query($conectar,"select * from permiso where prf_clave_int = '".$percla."' and ven_clave_int = 6");
	$dato = mysqli_fetch_array($con);
	$mettie = $dato['per_metodo'];
	
	$con = mysqli_query($conectar,"select * from permiso where prf_clave_int = '".$percla."' and ven_clave_int = 7");
	$dato = mysqli_fetch_array($con);
	$metinf = $dato['per_metodo'];
		
	$con = mysqli_query($conectar,"select * from permiso where prf_clave_int = '".$percla."' and ven_clave_int = 9");
	$dato = mysqli_fetch_array($con);
	$metcob = $dato['per_metodo'];
	
	$con = mysqli_query($conectar,"select * from permiso where prf_clave_int = '".$percla."' and ven_clave_int = 12");
	$dato = mysqli_fetch_array($con);
	$metins = $dato['per_metodo'];
	
	$con = mysqli_query($conectar,"select * from permiso where prf_clave_int = '".$percla."' and ven_clave_int = 3");
	$dato = mysqli_fetch_array($con);
	$metadm = $dato['per_metodo'];
	
	$con = mysqli_query($conectar,"select * from permiso where prf_clave_int = '".$percla."' and ven_clave_int = 5");
	$dato = mysqli_fetch_array($con);
	$metpro = $dato['per_metodo'];
	
	$con = mysqli_query($conectar,"select * from permiso where prf_clave_int = '".$percla."' and ven_clave_int = 1");
	$dato = mysqli_fetch_array($con);
	$metusu = $dato['per_metodo'];			
	
	$con = mysqli_query($conectar,"select * from permiso where prf_clave_int = '".$percla."' and ven_clave_int = 4");
	$dato = mysqli_fetch_array($con);
	$metregact = $dato['per_metodo'];
	
	$con = mysqli_query($conectar,"select * from permiso where prf_clave_int = '".$percla."' and ven_clave_int = 8");
	$dato = mysqli_fetch_array($con);
	$metfac = $dato['per_metodo'];
	
	$con = mysqli_query($conectar,"select * from permiso where prf_clave_int = '".$percla."' and ven_clave_int = 10");
	$dato = mysqli_fetch_array($con);
	$metcli = $dato['per_metodo'];
	
	$con = mysqli_query($conectar,"select * from permiso where prf_clave_int = '".$percla."' and ven_clave_int = 11");
	$dato = mysqli_fetch_array($con);
	$metregi = $dato['per_metodo'];
	
	$con = mysqli_query($conectar,"select * from permiso where prf_clave_int = '".$percla."' and ven_clave_int = 2");
	$dato = mysqli_fetch_array($con);
	$metcontra = $dato['per_metodo'];
?>
<?php
	if($_GET['modulousuarios'] == 'si')
	{
		$fecha=date("Y/m/d H:i:s");
		mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'1',2,'','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 1=Acceso a ventana
		$sql = mysqli_query($conectar,"update usuario set ven_clave_int = 1 where usu_usuario = '".$usuario."'");
		echo '<iframe src="modulos/usuarios/usuarios.php" id="iframe1" frameborder="1" style="width: 100%;height:100%; border-style:groove" scrolling=no onLoad=autoResize("iframe1")></iframe>';
		exit();
	}
	if($_GET['modulocancambiarcontrasena'] == 'si')
	{
		$fecha=date("Y/m/d H:i:s");
		mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'2',2,'','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 1=Acceso a ventana
		$sql = mysqli_query($conectar,"update usuario set ven_clave_int = 2 where usu_usuario = '".$usuario."'");
		echo '<iframe src="modulos/cambiarcontrasena/cambiar.php" id="iframe2" frameborder="1" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen style="width: 100%;height:100%; border-style:groove;overflow:hidden; overflow-y:hidden; overflow-x:hidden;" scrolling=no onLoad=autoResize("iframe2")></iframe>';
		exit();
	}
	if($_GET['moduloregistroactividades'] == 'si')
	{
		$fecha=date("Y/m/d H:i:s");
		mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'4',2,'','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 1=Acceso a ventana
		$sql = mysqli_query($conectar,"update usuario set ven_clave_int = 4 where usu_usuario = '".$usuario."'");
		echo '<iframe src="modulos/registroactividades/registro.php" id="iframe3" frameborder="1" style="width: 100%;height:100%; border-style:groove;overflow:hidden; overflow-y:hidden; overflow-x:hidden;" scrolling=no onLoad=autoResize("iframe3")></iframe>';
		exit();
	}
	if($_GET['moduloproyectos'] == 'si')
	{
		$fecha=date("Y/m/d H:i:s");
		mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'5',2,'','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 1=Acceso a ventana
		$sql = mysqli_query($conectar,"update usuario set ven_clave_int = 5 where usu_usuario = '".$usuario."'");
		echo '<iframe src="modulos/proyectos/proyectos.php" id="iframe4" frameborder="1" style="width: 100%;height:100%; border-style:groove;overflow:hidden; overflow-y:hidden; overflow-x:hidden;" scrolling=no onLoad=autoResize("iframe4")></iframe>';
		exit();
	}
	if($_GET['modulotiempos'] == 'si')
	{
		$fecha=date("Y/m/d H:i:s");
		mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'6',2,'','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 1=Acceso a ventana
		$sql = mysqli_query($conectar,"update usuario set ven_clave_int = 6 where usu_usuario = '".$usuario."'");
		echo '<iframe src="modulos/tiempos/tiempos.php" id="iframe5" frameborder="1" style="width: 100%;height:100%; border-style:groove;overflow:hidden; overflow-y:hidden; overflow-x:hidden;" scrolling=no onLoad=autoResize("iframe5")></iframe>';
		exit();
	}
	if($_GET['moduloinformetiempos'] == 'si')
	{
		$fecha=date("Y/m/d H:i:s");
		mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'7',2,'','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 1=Acceso a ventana
		$sql = mysqli_query($conectar,"update usuario set ven_clave_int = 7 where usu_usuario = '".$usuario."'");
		echo '<iframe src="modulos/informetiempos/informe.php" id="iframe6" frameborder="1" style="width: 100%;height:100%; border-style:groove;overflow:hidden; overflow-y:hidden; overflow-x:hidden;" scrolling=no onLoad=autoResize("iframe6")></iframe>';
		exit();
	}
	if($_GET['modulofactorprestacional'] == 'si')
	{
		$fecha=date("Y/m/d H:i:s");
		mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'8',2,'','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 1=Acceso a ventana
		$sql = mysqli_query($conectar,"update usuario set ven_clave_int = 8 where usu_usuario = '".$usuario."'");
		echo '<iframe src="modulos/factorprestacional/factorprestacional.php" id="iframe7" frameborder="1" style="width: 100%;height:100%; border-style:groove;overflow:hidden; overflow-y:hidden; overflow-x:hidden;" scrolling=no onLoad=autoResize("iframe7")></iframe>';
		exit();
	}
	if($_GET['modulocobros'] == 'si')
	{
		$fecha=date("Y/m/d H:i:s");
		mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'9',2,'','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 1=Acceso a ventana
		$sql = mysqli_query($conectar,"update usuario set ven_clave_int = 9 where usu_usuario = '".$usuario."'");
		echo '<iframe src="modulos/cuentasdecobro/cobro.php" id="iframe8" frameborder="1" style="width: 100%;height:100%; border-style:groove;overflow:hidden; overflow-y:hidden; overflow-x:hidden;" scrolling=no onLoad=autoResize("iframe8")></iframe>';
		exit();
	}
	if($_GET['moduloclientes'] == 'si')
	{
		$fecha=date("Y/m/d H:i:s");
		mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'10',2,'','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 1=Acceso a ventana
		$sql = mysqli_query($conectar,"update usuario set ven_clave_int = 10 where usu_usuario = '".$usuario."'");
		echo '<iframe src="modulos/clientes/clientes.php" id="iframe9" frameborder="1" style="width: 100%;height:100%; border-style:groove;overflow:hidden; overflow-y:hidden; overflow-x:hidden;" scrolling=no onLoad=autoResize("iframe9")></iframe>';
		exit();
	}
	if($_GET['moduloregiones'] == 'si')
	{
		$fecha=date("Y/m/d H:i:s");
		mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'11',2,'','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 1=Acceso a ventana
		$sql = mysqli_query($conectar,"update usuario set ven_clave_int = 11 where usu_usuario = '".$usuario."'");
		echo '<iframe src="modulos/regiones/regiones.php" id="iframe10" frameborder="1" style="width: 100%;height:100%; border-style:groove;overflow:hidden; overflow-y:hidden; overflow-x:hidden;" scrolling=no onLoad=autoResize("iframe10")></iframe>';
		exit();
	}
	
	if($_GET['mostrarvideo'] == 'si')
	{
		$vid = $_GET['vid'];
		if($vid == 'TIEMPOS')
		{
			echo '<video src="videos/tiempos.mp4" id="myvideo" width="600" height="330" controls autoplay preload>
			  Tu navegador no implementa el elemento <code>video</code>.
			</video>';
		}
		else
		if($vid == 'INFORMES')
		{
			echo '<video src="videos/informes.mp4" id="myvideo" width="600" height="330" controls autoplay preload>
			  Tu navegador no implementa el elemento <code>video</code>.
			</video>';
		}
		else
		if($vid == 'CUENTACOBRO')
		{
			echo '<video src="videos/cuenta.mp4" id="myvideo" width="600" height="330" controls autoplay preload>
			  Tu navegador no implementa el elemento <code>video</code>.
			</video>';
		}
		else
		if($vid == 'CLIENTES')
		{
			echo '<video src="videos/clientes.mp4" id="myvideo" width="600" height="330" controls autoplay preload>
			  Tu navegador no implementa el elemento <code>video</code>.
			</video>';
		}
		else
		if($vid == 'FACTORPRESTACIONAL')
		{
			echo '<video src="videos/factor.mp4" id="myvideo" width="600" height="330" controls autoplay preload>
			  Tu navegador no implementa el elemento <code>video</code>.
			</video>';
		}
		else
		if($vid == 'PROYECTOS')
		{
			echo '<video src="videos/proyectos.mp4" id="myvideo" width="600" height="330" controls autoplay preload>
			  Tu navegador no implementa el elemento <code>video</code>.
			</video>';
		}
		else
		if($vid == 'REGIONES')
		{
			echo '<video src="videos/regiones.mp4" id="myvideo" width="600" height="330" controls autoplay preload>
			  Tu navegador no implementa el elemento <code>video</code>.
			</video>';
		}
		else
		if($vid == 'REGISTROACTIVIDADES')
		{
			echo '<video src="videos/actividades.mp4" id="myvideo" width="600" height="330" controls autoplay preload>
			  Tu navegador no implementa el elemento <code>video</code>.
			</video>';
		}
		else
		if($vid == 'USUARIOS')
		{
			echo '<video src="videos/usuarios.mp4" id="myvideo" width="600" height="330" controls autoplay preload>
			  Tu navegador no implementa el elemento <code>video</code>.
			</video>';
		}
		else
		if($vid == 'CAMBIARCONTRASENA')
		{
			echo '<video src="videos/cambiar.mp4" id="myvideo" width="600" height="330" controls autoplay preload>
			  Tu navegador no implementa el elemento <code>video</code>.
			</video>';
		}
		exit();
	}
	
	if($_GET['moduloinstructivo'] == 'si')
	{
		?>
		<table style="width: 100%" class="auto-style2">
			<tr>
				<td style="width: 45px;text-align:center;font-size:x-large" class="auto-style1">
				<strong>MENU</strong></td>
				<td class="auto-style1" style="text-align:center;font-size:x-large"><strong>VIDEO</strong></td>
			</tr>
			<tr>
				<td style="width: 45px" class="auto-style1">
				<div id="accordian">
					<ul>
						<li style="display:<?php if($mettie == ''){ echo 'none'; } ?>" onclick="MOSTRARVIDEO('TIEMPOS')">
							<h3><span class="fa fa-clock-o"></span>TIEMPOS</h3>
						</li>
						<!-- we will keep this LI open by default -->
						<li style="display:<?php if($metinf == ''){ echo 'none'; } ?>" onclick="MOSTRARVIDEO('INFORMES')">
							<h3><span class="fa fa-pie-chart"></span>INFORMES</h3>
						</li>
						<li style="display:<?php if($metcob == ''){ echo 'none'; } ?>" onclick="MOSTRARVIDEO('CUENTACOBRO')">
							<h3><span class="fa fa-pencil-square-o"></span>CUENTA DE COBRO</h3>
						</li>
						<li style="display:<?php if($metadm == ''){ echo 'none'; } ?>" class="active">
							<h3><span class="fa fa-user"></span>ADMINISTRADOR</h3>
							<ul>
								<li style="display:<?php if($metcli == ''){ echo 'none'; } ?>" onclick="MOSTRARVIDEO('CLIENTES')"><a href="#">Clientes</a></li>
								<li style="display:<?php if($metfac == ''){ echo 'none'; } ?>" onclick="MOSTRARVIDEO('FACTORPRESTACIONAL')"><a href="#">Factor prestacional</a></li>
								<li style="display:<?php if($metpro == ''){ echo 'none'; } ?>" onclick="MOSTRARVIDEO('PROYECTOS')"><a href="#">Proyectos</a></li>
								<li style="display:<?php if($metregi == ''){ echo 'none'; } ?>" onclick="MOSTRARVIDEO('REGIONES')"><a href="#">Regiones</a></li>
								<li style="display:<?php if($metregact == ''){ echo 'none'; } ?>" onclick="MOSTRARVIDEO('REGISTROACTIVIDADES')"><a href="#">Registro actividades</a></li>
								<li style="display:<?php if($metusu == ''){ echo 'none'; } ?>" onclick="MOSTRARVIDEO('USUARIOS')"><a href="#">Usuarios</a></li>
							</ul>
						</li>
						<li style="display:<?php if($metcontra == ''){ echo 'none'; } ?>" onclick="MOSTRARVIDEO('CAMBIARCONTRASENA')">
							<h3><span class="fa fa-exchange"></span>CAMBIAR CONTRASEÑA</h3>
						</li>
					</ul>
				</div>
				</td>
				<td class="auto-style1" style="height:350px">
				<div id="video" align="center">
				<img src="images/video.png" height="50" width="50">
				</div>
				</td>
			</tr>
		</table>
		<?php
		mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'12',2,'','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 1=Acceso a ventana
		$sql = mysqli_query($conectar,"update usuario set ven_clave_int = 12 where usu_usuario = '".$usuario."'");
		/*$fecha=date("Y/m/d H:i:s");
		mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'12',2,'','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 1=Acceso a ventana
		$sql = mysqli_query($conectar,"update usuario set ven_clave_int = 12 where usu_usuario = '".$usuario."'");
		echo '<iframe src="modulos/instructivo/instructivo.php" id="iframe11" frameborder="1" style="width: 100%; border-style:groove;overflow:hidden; overflow-y:hidden; overflow-x:hidden;" scrolling=no onLoad=autoResize("iframe11")></iframe>';*/
		exit();
	}
?>
<html>
<head>
<title>Hoja de Tiempos</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="Desciption" content="This web design site is a very good site with wonderful web design resources">
<meta name="keywords" content="This,web,design,site,is,a,very,good,site,with,wonderful,web,design,resources">
<meta name="google-translate-customization" content="d1afdfdf6ae53bdd-e40bbef8d39c50fd-g1986d13f3a0f7c14-1c"></meta>
<link rel="shortcut icon" href="images/logo.png" />
<link href="css/style1.css" rel="stylesheet" type="text/css">
<link href="css/animatedpanelmenu.css" rel="stylesheet" type="text/css">
<!-- Start css3menu.com HEAD section -->
	<link rel="stylesheet" href="CSS3 Menu_files/css3menu1/style.css" type="text/css" /><style type="text/css">._css3m{display:none}</style>
	<!-- End css3menu.com HEAD section -->

<style type="text/css">
</style>
        <style>
            #galeria {
                margin:0 auto 0 auto;
                width:630px;
                height:368px;
                -webkit-box-shadow: 0px 1px 5px 0px #4a4a4a;
                -moz-box-shadow: 0px 1px 5px 0px #4a4a4a;
                box-shadow: 0px 1px 5px 0px #4a4a4a;
            }
    
        .style1 {
			font-size: small;
			background-color: #5a825a;
		}
    
        .style2 {
			font-size: x-large;
		}
    
        .style3 {
			font-size: medium;
		}
    
        .style4 {
	text-align: left;
			font-size: large;
		}
    
        .style7 {
			text-align: center;
		}
    
        .style8 {
			font-size: large;
		}
    
        .section {
	border-bottom: 5px solid #ccc;
	padding: 20px;
}
    
		.auto-style2 {
			font-size:35px;
		}
    
		.auto-style3 {
			color: #000000;
		}
    
		</style>
<script type="text/javascript" src="llamadas3.js"></script>
<script src="js/jquery-1.7.2.min.js" type="text/javascript"></script>
<link rel="stylesheet" media="screen,projection" href="css/ui.totop.css" />
<!-- Font Awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<script language="JavaScript"> 
<!-- 
function autoResize(id){ 
    var newheight; 
    var newwidth; 

    if(document.getElementById){ 
        newheight=document.getElementById(id).contentWindow.document .body.scrollHeight; 
        newwidth=document.getElementById(id).contentWindow.document .body.scrollWidth; 
    } 
	$('#'+id, window.parent.document).height(newheight+'px');
    //document.getElementById(id).height= (newheight) + "px"; 
    //document.getElementById(id).width= (newwidth) + "px";
} 
//--> 
</script>

</head>

<body>
<form name='formulario' id='formulario' method='post' action='confirmacion.php' target='_self' enctype="multipart/form-data"> 
<center>
<div class="wrapper">
<div class="header">
<div style="float:left"><br>
<div style="width: 150px;" align="center">
<img alt="" src="images/logo.png" height="100" style="margin-top:-10px">
</div>
</div>
<div style="float:left; margin-left:70px; margin-top:20px; height: 116px; width: 400px;">
<div style="text-align:left; font-family:tahoma; font-size:16pt; color:#636363; margin-left:5px;margin-top:5px; width: 400px; height: 19px;">
				<b>
				<span style="font-size:25px">Registro Diario de Actividades</span><br><span style="float:left;  text-align:left; font-family:tahoma; font-size:10pt; color:#b1b1b1;"><b>
				&nbsp;Sistema de Seguimiento y Control de Actividades</b></span>
				<br><b><span style="float:right;  text-align:left; font-family:tahoma; font-size:7pt; color:#b1b1b1;">
				&nbsp;</span><span style="float:right;  text-align:left; font-family:tahoma; font-size:7pt; " class="auto-style3">by 
				PAVAS TECNOLOGIA</span></b></div>
</div>
</div>
	</b>
<div class="menu">
<!-- Start css3menu.com BODY section -->
<ul class="menu" id="css3menu1" style="left: 0px; top: 0px">
	<li class="topfirst" style="left: 0px; top: 0px">
	<a style="width:80px; height:25px;line-height:23px;cursor:pointer" <?php if(($mettie <> '' and ($mettie == 0 || $mettie == 1))){ echo "onClick=MODULO('TIEMPOS')"; } ?>><?php if(($mettie <> '' and ($mettie == 0 || $mettie == 1))){ echo "TIEMPOS"; }else{ echo "<span style='color:silver'></span>"; } ?></a>
	</li>
	<li class="topfirst" style="left: 0px; top: 0px">
	<a style="width:80px; height:25px;line-height:23px;cursor:pointer" <?php if(($metinf <> '' and ($metinf == 0 || $metinf == 1))){ echo "onClick=MODULO('INFORMES')"; } ?>><?php if(($metinf <> '' and ($metinf == 0 || $metinf == 1))){ echo 'INFORMES'; }else{ echo "<span style='color:silver'></span>"; } ?></a>
	</li>
	<li class="topfirst" style="left: 0px; top: 0px">
	<a style="width:120px; height:25px;line-height:23px;cursor:pointer" <?php if(($metcob <> '' and ($metcob == 0 || $metcob == 1))){ echo "onClick=MODULO('COBROS')"; } ?>><?php if(($metcob <> '' and ($metcob == 0 || $metcob == 1))){ echo 'CUENTA DE COBRO'; }else{ echo "<span style='color:silver'></span>"; } ?></a>
	</li>
	<li class="topfirst" style="left: 0px; top: 0px">
	<a style="width:120px; height:25px;line-height:23px;cursor:pointer" <?php if(($metins <> '' and ($metins == 0 || $metins == 1))){ echo "onClick=MODULO('INSTRUCTIVO')"; } ?>><?php if(($metins <> '' and ($metins == 0 || $metins == 1))){ echo 'INSTRUCTIVO'; }else{ echo "<span style='color:silver'></span>"; } ?></a>
	</li>
	<li class="topmenu">
	<a style="width:378px;height:25px;line-height:23px;cursor:pointer;"></a>
	</li>
	<li class="topmenu">
	<a style="width:125px;height:25px;line-height:23px;cursor:pointer"><?php if($metadm == ''){ echo '<span style="color:silver"></span>'; }else{ echo 'ADMINISTRADOR <img src="images/flecha.png" alt="" height="10" width="12" />'; } ?></a>
	<?php 
		if($metadm <> '' and ($metadm == 0 || $metadm == 1))
		{
	?>
	<ul>
		<li class="subfirst"><a style="width: 132px" <?php if($metcli <> '' and ($metcli == 0 || $metcli == 1)){ echo "onClick=MODULO('CLIENTES')"; } ?>><?php if($metcli <> '' and ($metcli == 0 || $metcli == 1)){ echo "Clientes"; }else{ echo "<span style='color:silver'></span>"; } ?></a></li>
		<li class="topmenu"><a style="width: 132px" <?php if($metfac <> '' and ($metfac == 0 || $metfac == 1)){ echo "onClick=MODULO('FACTORPRESTACIONAL')"; } ?>><?php if($metfac <> '' and ($metfac == 0 || $metfac == 1)){ echo "Factor prestacional"; }else{ echo "<span style='color:silver'></span>"; } ?></a></li>
		<li class="topmenu"><a style="width: 132px" <?php if($metpro <> '' and ($metpro == 0 || $metpro == 1)){ echo "onClick=MODULO('PROYECTOS')"; } ?>><?php if($metpro <> '' and ($metpro == 0 || $metpro == 1)){ echo "Proyectos"; }else{ echo "<span style='color:silver'></span>"; } ?></a></li>
		<li class="topmenu"><a style="width: 132px" <?php if($metregi <> '' and ($metregi == 0 || $metregi == 1)){ echo "onClick=MODULO('REGIONES')"; } ?>><?php if($metregi <> '' and ($metregi == 0 || $metregi == 1)){ echo "Regiones"; }else{ echo "<span style='color:silver'></span>"; } ?></a></li>
		<li class="topmenu"><a style="width: 132px" <?php if($metregact <> '' and ($metregact == 0 || $metregact == 1)){ echo "onClick=MODULO('REGISTROACTIVIDADES')"; } ?>><?php if($metregact <> '' and ($metregact == 0 || $metregact == 1)){ echo "Registro actividades"; }else{ echo "<span style='color:silver'></span>"; } ?></a></li>
		<li class="topmenu"><a style="width: 132px" <?php if($metusu <> '' and ($metusu == 0 || $metusu == 1)){ echo "onClick=MODULO('USUARIOS')"; } ?>><?php if($metusu <> '' and ($metusu == 0 || $metusu == 1)){ echo "Usuarios"; }else{ echo "<span style='color:silver'></span>"; } ?></a></li>
	</ul>
	<?php
		}
	?>
	</li>
	<li class="toplast">
	<a style="width:200px;height:25px;line-height:23px;">
	<span style="background-image:url('images/user.png'); background-repeat:no-repeat;height: 26px;cursor:pointer; width: 200px;"><?php echo strtoupper($usuario); ?> <img src="images/flecha.png" alt="" height="10" width="12" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></a>
	<ul>
		<li class="subfirst"><a style="width: 200px; text-align:right" <?php if($metcontra <> '' and ($metcontra == 0 || $metcontra == 1)){ echo "onClick=MODULO('CAMBIARCONTRASENA')"; } ?><?php if($metcontra <> '' and ($metcontra == 0 || $metcontra == 1)){ echo "<label style='cursor:pointer'>Cambiar Contraseña</label>"; }else{ echo "<label style='color:silver;cursor:pointer'></label>"; } ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
		</li>
		<li class="topmenu"><a style="width: 200px; text-align:right" href="data/logout.php">
		Cerrar Sesion&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
	</ul></li>
</ul>
</div>
<div style="width: 1246px;">
<div style="border:thin" id="modulos">
</div>
</div>
</div>
<div class="style1" style="height: 57px; font-family:Arial, Helvetica, sans-serif; bottom: 95px; left:0px; width: 1250px; overflow:hidden; clear:both; filter: alpha(opacity=60); z-index: 10; color:white">
<br>PAVAS TECNOLOGÍA S.A.S.<br>Copyright © <?php echo date('Y'); ?> Todos los 
	derechos reservados</div>
</center>
</form>
<!-- ASIGANAR ÚLTIMA VENTANA DE ACCESO DEL USUARIO LOGEADO -->
<?php
	$con = mysqli_query($conectar,"select * from usuario u inner join ventana ven on (ven.ven_clave_int = u.ven_clave_int) where u.usu_usuario = '".$usuario."'");
	$dato = mysqli_fetch_array($con);
	$ventana = $dato['ven_nombre'];
	
	if($cantsw == 1)
	{
		if($swinm == 1)
		{
			$ventana = 'INMOBILIARIA';
		}
		elseif($swvis == 1)
		{
			$ventana = 'VISITALOCAL';
		}
		elseif($swdis == 1)
		{
			$ventana = 'DISENO';
		}
		elseif($swlic == 1)
		{
			$ventana = 'LICENCIA';
		}
		elseif($swint == 1)
		{
			$ventana = 'INTERVENTORIA';
		}
		elseif($swcon == 1)
		{
			$ventana = 'CONSTRUCTOR';
		}
		elseif($swseg == 1)
		{
			$ventana = 'INSTALADORSEGURIDAD';
		}
	}
	else
	if($cantsw > 1)
	{
		$ventana = 'INICIO';
	}
?>
<script type='text/javascript' language='javascript'>setTimeout("MODULO('<?php echo $ventana; ?>')", 10);</script>
<input name="oculto" value="<?php echo $ventana; ?>" type="hidden" >
	</b>
<!-- UItoTop plugin -->
<script src="js/jquery.ui.totop.js" type="text/javascript"></script>
<!-- Starting the plugin -->
<script type="text/javascript">
	$(document).ready(function() {
		/*
		var defaults = {
  			containerID: 'toTop', // fading element id
			containerHoverID: 'toTopHover', // fading element hover id
			scrollSpeed: 1200,
			easingType: 'linear' 
 		};
		*/
		
		$().UItoTop({ easingType: 'easeOutQuart' });
		
	});
</script>
<!-- prefix free to deal with vendor prefixes -->
<script src="https://thecodeplayer.com/uploads/js/prefixfree-1.0.7.js" type="text/javascript" type="text/javascript"></script>

<!-- jQuery -->
<script src="https://thecodeplayer.com/uploads/js/jquery-1.7.1.min.js" type="text/javascript"></script>
</body>
</html>