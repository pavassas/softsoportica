<?php
	include('data/Conexion.php');
	date_default_timezone_set('America/Bogota');
	
	$codigo = $_GET['codigo'];
	
	$con = mysqli_query($conectar,"select * from recuperar where rec_codigo = '".$codigo."' and rec_estado = 1");
	$num = mysqli_num_rows($con);
	
	if($num > 0)
	{
		echo '<script>alert("Este codigo ya fue usado anteriormente"); window.location.href="index.php";</script>';
	}
	
	function encrypt($string, $key) 
	{
		$result = "";	
		for($i=0; $i<strlen($string); $i++) 
		{
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char)+ord($keychar));
			$result.=$char;
		}
		return base64_encode($result);
	}
	
	if($_GET['restablecer'] == "si")
	{
		header("Cache-Control: no-store, no-cache, must-revalidate");
		sleep(1);
		$cod = $_GET['cod'];
		$con1 = $_GET['con1'];
		$con2 = $_GET['con2'];
		
		$con = mysqli_query($conectar,"select * from recuperar where rec_codigo = '".$cod."' and rec_estado = 1");
		$num = mysqli_num_rows($con);
		
		if($num > 0)
		{
			echo "<div class='validaciones'>Este codigo ya fue usado anteriormente</div>";
		}
		else
		{
			if($con1 == $con2)
			{
				$con = mysqli_query($conectar,"select * from recuperar where rec_codigo = '".$cod."'");
				$dato = mysqli_fetch_array($con);
				$usu = $dato['usu_clave_int'];
				
				$con = mysqli_query($conectar,"update recuperar set rec_estado = 1 where rec_codigo = '".$cod."'");
				$con = mysqli_query($conectar,"update usuario set usu_clave = '".encrypt($con1,"p4v4svasquez")."' where usu_clave_int = '".$usu."'");
				
				if($con >= 1)
				{
					$fecha=date("Y/m/d H:i:s");
					$con1 = mysqli_query($conectar,"select * from usuario where usu_clave_int = '".$usu."'");
					$dato1 = mysqli_fetch_array($con1);
					$usuario = $dato1['usu_usuario'];
				
					mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'',67,'','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 67=Restablecer Nueva Contraseña					
					echo "<div class='ok'>Su contrase&ntilde;a a sido restablecida correctamente!</div>";
				}
				else
				{
					echo "<div class='validaciones'>Error al restablecer la contraseña</div>";
				}
			}
			else
			{
				echo "<div class='validaciones'>Las contraseñas no coinciden</div>";
			}
		}
		exit();
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <title>Control De Materiales</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="description" content="Expand, contract, animate forms with jQuery wihtout leaving the page" />
        <meta name="keywords" content="expand, form, css3, jquery, animate, width, height, adapt, unobtrusive javascript"/>
		<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>
        <link rel="stylesheet" type="text/css" href="css/stylelogin.css" />
        
        <?php //VENTANA EMERGENTE ?>
		<link rel="stylesheet" href="css/reveal.css" />
		<script type="text/javascript" src="llamadas3.js"></script>
		<script type="text/javascript" src="js/jquery-1.6.min.js"></script>
		<script type="text/javascript" src="js/jquery.reveal.js"></script>
    	<style type="text/css">
.auto-style1 {
	color: #FFFFFF;
}
</style>
    </head>
    <body>
    <form name="form" method="post" autocomplete="off" >
		<div class="wrapper">
			<div class="content">
				<div id="form_wrapper" class="form_wrapper">
					<form class="login active">
						<h3>Restablecer Contraeña</h3>
						<div>
							<label>Nueva Contraseña:</label>
							<input type="password" name="contrasena1" required id="contrasena1" />
						</div>
						<div>
							<label>Repetir Contraseña:</label>
							<input type="password" name="contrasena2" required id="contrasena2" />
						</div>
						<div class="bottom">
							<input type="button" onclick="RESTABLECER('<?php echo $codigo; ?>')" value="Restablecer"></input>
							<div id="clear" class="clear">
							</div>
							<div>
							<a href="index.php" class="auto-style1">Iniciar Sesion</a>
							</div>
						</div>
					</form>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	 	</form>
    </body>
</html>