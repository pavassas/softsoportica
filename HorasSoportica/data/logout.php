<?php
	session_start();
	// Borramos toda la sesion
    setcookie("usIdentificacion", "", time() - 3600, "/");
    setcookie("clave", "", time() - 3600, "/");
	session_destroy();
	header("LOCATION:../index.php");
?>