<?php
	error_reporting(0);
	include('../../data/Conexion.php');
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_SESSION['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];
		
	// verifica si no se ha loggeado
	if(!isset($_SESSION["persona"]))
	{
	  session_destroy();
	  header("LOCATION:index.php");
	}else{
	}
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");
	
	$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
	$dato = mysqli_fetch_array($con);
	$perfil = $dato['prf_descripcion'];
	$claveperfil = $dato['prf_clave_int'];
	$claveusuario = $dato['usu_clave_int'];
	$ultimaobra = $dato['obr_clave_int'];
	
	$con = mysqli_query($conectar,"select per_metodo from permiso where prf_clave_int = '".$claveperfil."' and ven_clave_int = 5");
	$dato = mysqli_fetch_array($con);
	$metodo = $dato['per_metodo'];
	
	if($_GET['editarciu'] == 'si')
	{
		$ciuedi = $_GET['ciuedi'];
		$con = mysqli_query($conectar,"select * from proyecto where pro_clave_int = '".$ciuedi."'"); 
		$dato = mysqli_fetch_array($con); 
		$ciu = $dato['pro_nombre'];
		$cod = $dato['pro_codigo'];
		$cli = $dato['usu_clave_int'];
		$act = $dato['pro_sw_activo'];
		$reg = $dato['reg_clave_int'];
		$codcon = $dato['pro_codigo_contable'];
?>

		<table style="width: 38%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Código Proyecto:</td>
			<td class="auto-style3">
			<input name="codigo1" id="codigo1" value="<?php echo $cod; ?>" class="inputs" type="text" style="width: 200px" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Código Contable:</td>
			<td class="auto-style3">
			<input name="codigocontable1" id="codigocontable1" value="<?php echo $codcon; ?>" class="inputs" type="text" style="width: 200px" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Región :</td>
			<td class="auto-style3">
			<select class="inputs" name="region1" id="region1" <?php if($claveperfil != 1 and $claveperfil != 7){ echo 'disabled="disabled"'; } ?> style="width: 200px">
			<option value="">-Seleccione-</option>
			<?php
				$con = mysqli_query($conectar,"select * from region order by reg_nombre");
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$clave = $dato['reg_clave_int'];
					$nombre = $dato['reg_nombre'];
			?>
				<option value="<?php echo $clave; ?>" <?php if($reg == $clave){  echo "selected='selected'"; } ?>><?php echo $nombre; ?></option>
			<?php
				}
			?>
			</select></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Nombre Proyecto:</td>
			<td class="auto-style3">
			<input name="proyecto1" id="proyecto1" value="<?php echo $ciu; ?>" class="inputs" type="text" style="width: 200px" />&nbsp;
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3" colspan="2">Activa:<input class="inputs" <?php if($act == 1){ echo 'checked="checked"'; } ?> name="activo1" type="checkbox" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4">
			<input name="submit" type="button" value="Guardar" onclick="GUARDAR('CIUDAD','<?php echo $ciuedi; ?>')"  style="width: 348px; height: 25px; cursor:pointer" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">
			<div id="datos">
			</div>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
<?php
		exit();
	}
	
	if($_GET['guardarpro'] == 'si')
	{
		sleep(1);
		$cod = $_GET['cod'];
		$pro = $_GET['pro'];
		$act = $_GET['act'];
		$lp = $_GET['lp'];
		$p = $_GET['p'];
		$codcon = $_GET['codcon'];
		$reg = $_GET['reg'];
		
		$fecha=date("Y/m/d H:i:s");
		
		$sql = mysqli_query($conectar,"select * from proyecto where (UPPER(pro_nombre) = UPPER('".$pro."')) AND pro_clave_int <> '".$p."' and reg_clave_int = '".$reg."'");
		$dato = mysqli_fetch_array($sql);
		$conpro = $dato['pro_nombre'];
		
		if($cod == '')
		{
			echo "<div class='validaciones'>Debe ingresar el código del proyecto</div>";
		}
		else if($codcon=="")
		{
			echo "<div class='validaciones'>Debe ingresar el código contable del proyecto</div>";
		}
		else if($reg=="" || $reg==NULL)
		{
			echo "<div class='validaciones'>Debe seleccionar la región del proyecto</div>"; 
		}
		else
		if($pro == '')
		{
			echo "<div class='validaciones'>Debe ingresar el nombre del proyecto</div>";
		}
		else
		if(STRTOUPPER($conpro) == STRTOUPPER($pro))
		{
			echo "<div class='validaciones'>El proyecto ingresado ya existe</div>";
		}
		else
		if($lp < 3)
		{
			echo "<div class='validaciones'>El nombre del proyecto debe ser mí­nimo de 3 dijitos</div>";
		}
		else
		{			
			if($act == 'false'){$swact = 0;}elseif($act == 'true'){$swact = 1;}
			
			$con = mysqli_query($conectar,"update proyecto set pro_codigo = '".$cod."',pro_nombre = '".$pro."',pro_sw_activo = '".$swact."', pro_usu_actualiz = '".$usuario."', pro_fec_actualiz = '".$fecha."',pro_codigo_contable = '".$codcon."',reg_clave_int = '".$reg."' where pro_clave_int = '".$p."'");
			
			if($con >= 1)
			{
				//mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_encabezado,obr_clave_int,loa_usu_actualiz,loa_fec_actualiz) values(null,18,58,'".$c."','".$ultimaobra."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 58=Actualización proyecto
				echo "<div class='ok'>Datos grabados correctamente</div>";
			}
			else
			{
				echo "<div class='validaciones'>No se han podido guardar los datos</div>";
			}	
		}
		exit();
	}
	
	if($_GET['nuevoproyecto'] == 'si')
	{
		sleep(1);
		$cod = $_GET['cod'];
		$pro = $_GET['pro'];
		$act = $_GET['act'];
		$lp = $_GET['lp'];
        $codcon = $_GET['codcon'];
        $reg = $_GET['reg'];
		
		$fecha=date("Y/m/d H:i:s");
		
		$sql = mysqli_query($conectar,"select * from proyecto where (UPPER(pro_nombre) = UPPER('".$pro."')) and reg_clave_int  = '".$reg."'");
		$dato = mysqli_fetch_array($sql);
		$conpro = $dato['pro_nombre'];
		
		if($cod == '')
		{
			echo "<div class='validaciones'>Debe ingresar el código del proyecto</div>";
		}
		else if($codcon=="")
		{
			echo "<div class='validaciones'>Debe ingresar el código contable del proyecto</div>";
		}
		else if($reg=="" || $reg==NULL)
		{
			echo "<div class='validaciones'>Debe seleccionar la región del proyecto</div>"; 
		}
		else

		if($pro == '')
		{
			echo "<div class='validaciones'>Debe ingresar el nombre del proyecto</div>";
		}
		else
		if(STRTOUPPER($conpro) == STRTOUPPER($pro))
		{
			echo "<div class='validaciones'>El proyecto ingresado ya existe</div>";
		}
		else
		if($lp < 3)
		{
			echo "<div class='validaciones'>El nombre del proyecto debe ser mí­nimo de 3 dijitos</div>";
		}
		else
		{
			if($act == 'false'){$swact = 0;}elseif($act == 'true'){$swact = 1;}
			
			$con = mysqli_query($conectar,"insert into proyecto(pro_codigo,pro_nombre,pro_sw_activo,pro_usu_actualiz,pro_fec_actualiz,pro_codigo_contable,reg_clave_int) values('".$cod."','".$pro."','".$swact."','".$usuario."','".$fecha."','".$codcon."','".$reg."')");				
			
			if($con >= 1)
			{
				/*$con = mysqli_query($conectar,"select pro_clave_int from proyecto where UPPER(pro_nombre) = UPPER('".$ciu."')");
				$dato = mysqli_fetch_array($con);
				$claciu = $dato['pro_clave_int'];
				mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_encabezado,obr_clave_int,loa_usu_actualiz,loa_fec_actualiz) values(null,18,57,'".$claciu."','".$ultimaobra."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 57=Creación proyecto*/
				echo "<div class='ok'>Datos grabados correctamente</div>";
			}
			else
			{
				echo "<div class='validaciones'>No se han podido guardar los datos</div>";
			}
		}
		exit();
	}

    if($_GET['buscarreg'])
    {
    	$cod = $_GET['cod'];
		$pro = $_GET['pro'];
		$act = $_GET['act'];
		$reg = $_GET['reg'];
		$codcon = $_GET['codcon'];
        
		$rows=mysqli_query($conectar,"select * from proyecto where (pro_codigo LIKE REPLACE('%".$cod."%',' ','%') OR '".$cod."' IS NULL OR '".$cod."' = '') and (pro_nombre LIKE REPLACE('%".$pro."%',' ','%') OR '".$pro."' IS NULL OR '".$pro."' = '') and (pro_sw_activo = '".$act."' OR '".$act."' IS NULL OR '".$act."' = '') and (pro_codigo_contable LIKE REPLACE('%".$codcon."%',' ','%') OR '".$codcon."' IS NULL OR '".$codcon."' = '') and (reg_clave_int = '".$reg."' OR '".$reg."' IS NULL OR '".$reg."' = '') ");
		$total=mysqli_num_rows($rows);

		?>
		<table style="width: 100%;border-collapse:collapse">
		<tr>
			<td class="auto-style3" style="width: 27px">
				<input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<?php echo $total;?>,this);" class="auto-style6" /><span class="auto-style6">
				</span>
			</td>
			<td class="auto-style3" colspan="7">
			<?php 
			if($metodo == 1)
			{
			?>
			<table style="width: 30%">
				<tr>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../images/activo.png" alt="" class="auto-style6" /><input type="submit" value="Activar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../images/inactivo.png" alt="" class="auto-style6" /><input type="submit" value="Inactivar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../images/eliminar.png" alt="" class="auto-style6" /><input type="submit" value="Eliminar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
				</tr>
			</table>
			<?php
			}
			?>
			</td>
		</tr>
		<tr>
			<td class="auto-style3"><strong>REGIÓN</strong></td>
			<td class="auto-style3" style="width: 130px">&nbsp;</td>
			<td class="auto-style3" style="width: 130px">&nbsp;</td>
			<td class="auto-style3" style="width: 130px">&nbsp;</td>
			<td class="auto-style3" style="width: 130px">&nbsp;</td>
			<td class="auto-style3" style="width: 200px">&nbsp;</td>
			<td class="auto-style3" style="width: 98px">&nbsp;</td>			
		</tr>
		<tr>
			<td class="auto-style3" colspan="7"><hr></td>
		</tr>
		<?php
			$contador=0;
			$con = mysqli_query($conectar,"select r.reg_clave_int,r.reg_nombre from  proyecto p  left outer join  region r on r.reg_clave_int = p.reg_clave_int where (pro_codigo LIKE REPLACE('%".$cod."%',' ','%') OR '".$cod."' IS NULL OR '".$cod."' = '') and (pro_nombre LIKE REPLACE('%".$pro."%',' ','%') OR '".$pro."' IS NULL OR '".$pro."' = '') and (pro_sw_activo = '".$act."' OR '".$act."' IS NULL OR '".$act."' = '') and (pro_codigo_contable LIKE REPLACE('%".$codcon."%',' ','%') OR '".$codcon."' IS NULL OR '".$codcon."' = '') and (r.reg_clave_int = '".$reg."' OR '".$reg."' IS NULL OR '".$reg."' = '')  group by r.reg_clave_int order by r.reg_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clareg = $dato['reg_clave_int']; if($clareg=="" || $clareg==NULL){ $clareg = 0; }
				$nomreg = $dato['reg_nombre']; if($nomreg=="" || $nomreg==NULL){ $nomreg="Sin región"; }
				?>

				<tr style="cursor:pointer;<?php if($i % 2 == 0){ echo 'background-color:#B8CEB6"'; } ?>" onclick="PROYECTOS('<?php echo $i; ?>','<?php echo $clareg; ?>')">
			<td class="auto-style3" style="height: 26px"><?php echo $nomreg; ?></td>
			<td class="auto-style3" style="width: 130px">&nbsp;</td>
			<td class="auto-style3" style="width: 130px">&nbsp;</td>
			<td class="auto-style3" style="width: 130px">&nbsp;</td>
			<td class="auto-style3" style="width: 130px">&nbsp;</td>
			<td class="auto-style3" style="width: 200px">&nbsp;</td>
			<td class="auto-style3" style="width: 98px">&nbsp;</td>			
		</tr>
		<tr>
			<td class="auto-style3" colspan="7" style="height:1px">
			<input name="oculto<?php echo $i; ?>" id="oculto<?php echo $i; ?>" value="0" type="hidden" />
			<div id="verproyectos<?php echo $i; ?>" style="display:none;width:100%"></div>
			</td>
		</tr>
				<?php


			}
			?>
		</table>
			<?php
			exit();

    }
	if($_GET['buscarpro'] == 'si')
	{
		$cod = $_GET['cod'];
		$pro = $_GET['pro'];
		$act = $_GET['act'];
		$reg = $_GET['reg'];
		$codcon = $_GET['codcon'];		
?>
		<table style="width: 100%;border-collapse:collapse">		
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 65px"><strong>Código</strong></td>
			<td class="auto-style5" style="width: 65px"><strong>Código Contable</strong></td>
			<td class="auto-style5" style="width: 200px"><strong>Proyecto</strong></td>
			<td class="auto-style5" style="width: 98px"><strong>Actualizado por</strong></td>
			<td class="auto-style5" style="width: 127px"><strong>Actualización</strong></td>
			<td class="auto-style5" style="width: 29px"><strong>Activo</strong></td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
		<?php
			$contador=0;
			$con = mysqli_query($conectar,"select * from proyecto where (pro_codigo LIKE REPLACE('%".$cod."%',' ','%') OR '".$cod."' IS NULL OR '".$cod."' = '') and (pro_nombre LIKE REPLACE('%".$pro."%',' ','%') OR '".$pro."' IS NULL OR '".$pro."' = '') and (pro_sw_activo = '".$act."' OR '".$act."' IS NULL OR '".$act."' = '') and (pro_codigo_contable LIKE REPLACE('%".$codcon."%',' ','%') OR '".$codcon."' IS NULL OR '".$codcon."' = '') and (reg_clave_int = '".$reg."' OR '".$reg."' IS NULL OR '".$reg."' = '') order by pro_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$cod = $dato['pro_codigo'];
				$ciu = $dato['pro_nombre'];
				$act = $dato['pro_sw_activo'];
				$usuact = $dato['pro_usu_actualiz'];
				$fecact = $dato['pro_fec_actualiz'];
				$codcontable = $dato['pro_codigo_contable'];
				$contador=$contador+1;
				if($i == 0)
				{
		?>
		<tr>
			<td class="auto-style5" colspan="7"><hr></td>
		</tr>
		<?php
				}
		?>
		<tr style="<?php if($i % 2 == 0){ echo 'background-color:#B8CEB8"'; } ?>">
			<td class="auto-style3" style="width: 27px">
				<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<?php echo $contador;?>" value="<?php echo $dato['pro_clave_int'];?>" class="auto-style6" /></td>
			<td class="auto-style3" style="width: 65px"><?php echo $cod; ?></td>
			<td class="auto-style3" style="width: 65px"><?php echo $codcontable; ?></td>
			<td class="auto-style5" style="width: 200px"><?php echo $ciu; ?></td>
			<td class="auto-style5" style="width: 98px"><?php echo $usuact; ?></td>
			<td class="auto-style5" style="width: 127px"><?php echo $fecact; ?></td>
			<td class="auto-style1" style="width: 30px">
			<input name="activarinactivar" id="activarinactivar" type="checkbox" <?php if($act == 1){ echo 'checked="checked"'; } ?> disabled="disabled" class="auto-style6" ></td>
			<td class="auto-style5">
			<?php 
			if($metodo == 1)
			{
			?>
			<a data-reveal-id="editarciudad" data-animation="fade" style="cursor:pointer" onclick="EDITAR('<?php echo $dato['pro_clave_int']; ?>','CIUDAD')"><img src="../../images/editar.png" alt="" height="22" width="21" /></a>
			<?php
			}
			?>
			</td>
		</tr>
		<?php
			}
		?>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 65px">&nbsp;</td>
			<td class="auto-style5" style="width: 65px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 98px">&nbsp;</td>
			<td class="auto-style5" style="width: 127px">&nbsp;</td>
			<td class="auto-style5" style="width: 29px">&nbsp;</td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
	</table>
<?php
		exit();
	}
?>
<!DOCTYPE HTML>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

	
<title>CONTROL DE OBRAS</title>
<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
<!--[if lte IE 7]>
<link rel="stylesheet" href="css/ie.c32bc18afe0e2883ee4912a51f86c119.css" type="text/css" />
<![endif]-->

<style type="text/css">
.auto-style1 {
	text-align: center;
}
.auto-style2 {
	font-size: 12px;
	font-family: Arial, helvetica;
	outline: none;
/*transition: all 0.75s ease-in-out;*/ /*-webkit-transition: all 0.75s ease-in-out;*/ /*-moz-transition: all 0.75s ease-in-out;*/border-radius: 3px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 3px;
	border: 1px solid rgba(0,0,0, 0.2);
	background-color: #eee;
	padding: 3px;
	box-shadow: 0 0 10px #aaa;
	-webkit-box-shadow: 0 0 10px #aaa;
	-moz-box-shadow: 0 0 10px #aaa;
	border: 1px solid #999;
	background-color: white;
	text-align: center;
}
.auto-style3 {
	text-align: left;
}
.inputs{
	font-size:14px;
    font-family: Arial, helvetica;
    outline:none;
    border-radius:3px;
    -webkit-border-radius:6px;
    -moz-border-radius:3px;
    border:1px solid rgba(0,0,0, 0.5);
    padding: 3px;
}
.validaciones{
	font-size:14px;
    font-family: Arial, helvetica;
    outline:none;
    border-radius:3px;
    -webkit-border-radius:6px;
    -moz-border-radius:3px;
    border:1px solid rgba(0,0,0, 0.2);
    color:maroon;
    background-color:#eee;
    padding: 3px;
    text-align:center;
}
.ok{
	font-size:14px;
    font-family: Arial, helvetica;
    outline:none;
    border-radius:3px;
    -webkit-border-radius:6px;
    -moz-border-radius:3px;
    border:1px solid rgba(0,0,0, 0.2);
    color:green;
    background-color:#eee;
    padding: 3px;
    text-align:center;
}
.auto-style5 {
	text-align: left;
	font-family: Arial, Helvetica, sans-serif;
}
.auto-style6 {
	font-family: Arial, Helvetica, sans-serif;
}
.auto-style7 {
	text-align: center;
	font-size: large;
}
</style>

<script type="text/javascript" language="javascript">
	selecteds=0;
	
	function CheckUncheck(total,check){
		checkbox=null;
		for(i=1;i<=total;i++){
			checkbox=document.getElementById("idcat"+i);
			//alert(checkbox.value);
			checkbox.checked=check.checked;
		}
		
		if(check.checked){
			selecteds=total;
		}else{
			selecteds=0;
		}
		
	}
	
	function contadorVals(check){
		if(check.checked){
			selecteds=selecteds+1;
		}else{
			selecteds=selecteds-1;
		}
	}
	
	function selectedVals(){
		if(selecteds==0){
			alert("Seleccione al menos un registro.");
			return false;
		}else{
			return true;
		}
	}
</script>
<?php //VALIDACIONES ?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="llamadas2.js?<?php echo time();?>"></script>

<?php //VENTANA EMERGENTE ?>
<link rel="stylesheet" href="../../css/reveal.css" />
<script type="text/javascript" src="../../js/jquery.reveal.js"></script>
<script type="text/javascript" language="javascript">
function OCULTARSCROLL()
{
	setTimeout("parent.autoResize('iframe4')",500);
	setTimeout("parent.autoResize('iframe4')",1000);
	setTimeout("parent.autoResize('iframe4')",2000);
	setTimeout("parent.autoResize('iframe4')",3000);
	setTimeout("parent.autoResize('iframe4')",4000);
	setTimeout("parent.autoResize('iframe4')",5000);
	setTimeout("parent.autoResize('iframe4')",6000);
	setTimeout("parent.autoResize('iframe4')",7000);
	setTimeout("parent.autoResize('iframe4')",8000);
	setTimeout("parent.autoResize('iframe4')",9000);
	setTimeout("parent.autoResize('iframe4')",10000);
	setTimeout("parent.autoResize('iframe4')",11000);
	setTimeout("parent.autoResize('iframe4')",12000);
	setTimeout("parent.autoResize('iframe4')",13000);
	setTimeout("parent.autoResize('iframe4')",14000);
	setTimeout("parent.autoResize('iframe4')",15000);
	setTimeout("parent.autoResize('iframe4')",16000);
	setTimeout("parent.autoResize('iframe4')",17000);
	setTimeout("parent.autoResize('iframe4')",18000);
	setTimeout("parent.autoResize('iframe4')",19000);
	setTimeout("parent.autoResize('iframe4')",20000);
}
setTimeout("parent.autoResize('iframe4')",500);
setTimeout("parent.autoResize('iframe4')",1000);
setTimeout("parent.autoResize('iframe4')",2000);
setTimeout("parent.autoResize('iframe4')",3000);
setTimeout("parent.autoResize('iframe4')",4000);
setTimeout("parent.autoResize('iframe4')",5000);
setTimeout("parent.autoResize('iframe4')",6000);
setTimeout("parent.autoResize('iframe4')",7000);
setTimeout("parent.autoResize('iframe4')",8000);
setTimeout("parent.autoResize('iframe4')",9000);
setTimeout("parent.autoResize('iframe4')",10000);
setTimeout("parent.autoResize('iframe4')",11000);
setTimeout("parent.autoResize('iframe4')",12000);
setTimeout("parent.autoResize('iframe4')",13000);
setTimeout("parent.autoResize('iframe4')",14000);
setTimeout("parent.autoResize('iframe4')",15000);
setTimeout("parent.autoResize('iframe4')",16000);
setTimeout("parent.autoResize('iframe4')",17000);
setTimeout("parent.autoResize('iframe4')",18000);
setTimeout("parent.autoResize('iframe4')",19000);
setTimeout("parent.autoResize('iframe4')",20000);
</script>

</head>
<body>
<?php
$rows=mysqli_query($conectar,"select * from proyecto");
$total=mysqli_num_rows($rows);
?>
<form name="form1" id="form1" action="confirmar.php" method="post" onsubmit="return selectedVals();">
<!--[if lte IE 7]>
<div class="ieWarning">Este navegador no es compatible con el sistema. Por favor, use Chrome, Safari, Firefox o Internet Explorer 8 o superior.</div>
<![endif]-->
<table style="width: 100%">
	<tr>
		<td class="auto-style2" onclick="CONSULTAMODULO('TODOS')" onmouseover="this.style.backgroundColor='#5A825A';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="width: 60px; cursor:pointer">
		Todos
		<?php
			$con = mysqli_query($conectar,"select COUNT(*) cant from proyecto");
			$dato = mysqli_fetch_array($con);
			echo $dato['cant'];
		?>
		</td>
		<td class="auto-style7" style="cursor:pointer" colspan="4">
		MAESTRA PROYECTOS</td>
		<td class="auto-style2" onmouseover="this.style.backgroundColor='#CCCCCC';this.style.color='#0F213C';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="width: 55px; cursor:pointer">
		<?php 
		if($metodo == 1)
		{
		?>
		<a data-reveal-id="nuevaciudad" data-animation="fade" style="cursor:pointer">
		<table style="width: 100%">
			<tr>
				<td style="width: 14px">
				<img alt="" src="../../images/add2.png">
				</td>
				<td>Añadir</td>
			</tr>
		</table>
		</a>
		<?php
		}
		?>
		</td>
	</tr>
	<tr>
		<td class="auto-style2" colspan="6">
		<div id="filtro">
			<table style="width: 33%">
				<tr>
					<td class="auto-style1"><strong>Filtro:<img src="../../images/buscar.png" alt="" height="18" width="15" /></strong></td>
					<td class="auto-style1">
					<input class="inputs" onkeyup="BUSCAR()" name="buscodigo" id="buscodigo" maxlength="70" type="text" placeholder="Código Proyecto" style="width: 150px" />
					</td>
					<td class="auto-style1">
					<input class="inputs" onkeyup="BUSCAR()" name="buscodcontable" id="buscodcontable" maxlength="70" type="text" placeholder="Código Contable" style="width: 150px" />
					</td>
					<td class="auto-style1">
					<input class="inputs" onkeyup="BUSCAR()" name="busproyecto" id="busproyecto" maxlength="70" type="text" placeholder="Nombre Proyecto" style="width: 150px" /></td>
					<td class="auto-style1">
						<select class="inputs" name="busregion" onchange="BUSCAR()" style="width: 160px">
							<option value="">Región</option>
							<?php
							$con = mysqli_query($conectar,"select * from region order by reg_nombre");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$clave = $dato['reg_clave_int'];
								$nombre = $dato['reg_nombre'];
							?>
								<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
							<?php
							}
							?>
						</select>
					</td>
					<td class="auto-style1">
					<strong>Activo:</strong></td>
					<td class="auto-style1">
					<select name="buscaractivos" class="inputs" onchange="BUSCAR()">
					<option value="">Todos</option>
					<option value="1">Activos</option>
					<option value="0">Inactivo</option>
					</select></td>
				</tr>
			</table>
		</div>
		</td>
	</tr>
	<tr>
		<td colspan="6" class="auto-style2">
		<div id="editarciudad" class="reveal-modal" style="left: 57%; top: 10px; height: 150px; width: 350px;">
			<table style="width: 38%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Nombre:</td>
			<td class="auto-style3"><input class="inputs" name="nombre1" maxlength="50" value="<?php echo $nom; ?>" type="text" style="width: 200px" />
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Activo:</td>
			<td class="auto-style3"><input class="inputs" <?php if($act == 1){ echo 'checked="checked"'; } ?> name="activo1" type="checkbox" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4">
			<input name="submit" type="button" value="Guardar" onclick="GUARDAR('proyecto','<?php echo $ubiedi; ?>')"  style="width: 348px; height: 25px; cursor:pointer" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">
			<div id="datos">
			</div>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
			<a class="close-reveal-modal">&#215;</a>
		</div>
		<div id="ciudades"></div>
<div id="nuevaciudad" class="reveal-modal" style="left: 57%; top: 10px; height: 150px; width: 350px;">
	<table style="width: 38%" align="center">
		
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Código Proyecto:</td>
			<td class="auto-style3">
			<input name="codigo" id="codigo" class="inputs" type="text" style="width: 200px" />
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Código Contable:</td>
			<td class="auto-style3">
			<input name="codigocontable" id="codigocontable" class="inputs" type="text" style="width: 200px" />
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Región:</td>
			<td class="auto-style3">
				<select class="inputs" name="region" id="region" style="width: 200px">
				<option value="">-Seleccione-</option>
				<?php
				$con = mysqli_query($conectar,"select * from region order by reg_nombre");
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
				$dato = mysqli_fetch_array($con);
				$clave = $dato['reg_clave_int'];
				$nombre = $dato['reg_nombre'];
				?>
				<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
				<?php
				}
				?>
				</select>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Nombre Proyecto:</td>
			<td class="auto-style3">
			<input name="proyecto" id="proyecto"  type="text" style="width: 200px" class="inputs" />
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Activo:</td>
			<td class="auto-style3"><input class="inputs" name="activo" checked="checked" type="checkbox" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4">
			<input name="nuevo1" type="button" value="Guardar" onclick="NUEVO('CIUDAD')"  style="width: 348px; height: 25px; cursor:pointer" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">
			<div id="datos1">
			</div>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
</div>
		</td>
	</tr>
	<tr>
		<td style="width: 60px">&nbsp;</td>
		<td style="width: 117px">&nbsp;</td>
		<td style="width: 65px">&nbsp;</td>
		<td style="width: 70px">&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td style="width: 60px">&nbsp;</td>
		<td style="width: 117px">&nbsp;</td>
		<td style="width: 65px">&nbsp;</td>
		<td style="width: 70px">&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>
</form>
</body>
</html>
<?php 
 echo "<style onload=BUSCAR()></style>";
?>