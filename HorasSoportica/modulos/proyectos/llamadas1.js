function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function EDITAR(v,m)
{	
	if(m == 'CIUDAD')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('editarciudad').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#editarciudad").html("<img alt='cargando' src='img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?editarciu=si&ciuedi="+v,true);
		ajax.send(null);
		OCULTARSCROLL();
	}
}
function GUARDAR(v,id)
{
	if(v == 'CIUDAD')
	{
		var cod = form1.codigo1.value;
		var pro = form1.proyecto1.value;
		var lp = pro.length;
		var act = form1.activo1.checked;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('datos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#datos").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?guardarpro=si&cod="+cod+"&pro="+pro+"&act="+act+"&lp="+lp+"&p="+id,true);
		ajax.send(null);
		if(cod != '' && pro != '' && lp >= 3)
		{
			setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
		}
	}
}
function NUEVO(v)
{
	var cod = form1.codigo.value;
	var pro = form1.proyecto.value;
	var lp = pro.length;
	var act = form1.activo.checked;
		
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('datos1').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#datos1").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?nuevoproyecto=si&cod="+cod+"&pro="+pro+"&act="+act+"&lp="+lp,true);
	ajax.send(null);
	if(cod != '' && pro != '' && lp >= 3)
	{
		form1.codigo.value = '';
		form1.proyecto.value = '';
		setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
	}
	OCULTARSCROLL();
}
function CONSULTAMODULO(v)
{
	if(v == 'TODOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('ciudades').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#ciudades").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?todos=si",true);
		ajax.send(null);
		setTimeout("OCULTARSCROLL();",1000);
	}
}
function BUSCAR()
{	
	var cod = form1.buscodigo.value;
	var pro = form1.busproyecto.value;
	var cli = form1.buscliente.value;
	var act = form1.buscaractivos.value;
			
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('ciudades').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#ciudades").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?buscarpro=si&cod="+cod+"&pro="+pro+"&cli="+cli+"&act="+act,true);
	ajax.send(null);
	setTimeout("OCULTARSCROLL();",1000);
}