<?php
	error_reporting(0);
	include('../../data/Conexion.php');
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_SESSION['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];
		
	// verifica si no se ha loggeado
	if(!isset($_SESSION["persona"]))
	{
	  session_destroy();
	  header("LOCATION:index.php");
	}else{
	}
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");
	
	$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
	$dato = mysqli_fetch_array($con);
	$claveperfil = $dato['prf_clave_int'];
	
	$con = mysqli_query($conectar,"select per_metodo from permiso where prf_clave_int = '".$claveperfil."' and ven_clave_int = 1");
	$dato = mysqli_fetch_array($con);
	$metodo = $dato['per_metodo'];
	
	if($_GET['accion'] == 'ACTIVARINACTIVAR')
	{
		$usuact = $_GET['act'];
		$con = mysqli_query($conectar,"select * from usuario where usu_clave_int = '".$usuact."'");
		$dato = mysqli_fetch_array($con);
		$act = $dato['usu_sw_activo'];
		$fecha=date("Y/m/d H:i:s");
		
		if($act == 1)
		{
			$con = mysqli_query($conectar,"update usuario set usu_sw_activo = 0 where usu_usuario = '".$usuact."'");
		}
		elseif($act == 0)
		{
			$con = mysqli_query($conectar,"update usuario set usu_sw_activo = 1 where usu_usuario = '".$usuact."'");
		}
		mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,1,3,'".$usuact."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 3=Actualización usuario
	}

	if($_GET['editarusu'] == 'si')
	{
		$usuedi = $_GET['usuedi'];
		$con = mysqli_query($conectar,"select * from usuario where usu_clave_int = '".$usuedi."'"); 
		$dato = mysqli_fetch_array($con); 
		$nom = $dato['usu_nombre'];
		$ced = $dato['usu_cedula'];
		$usu = $dato['usu_usuario'];
		$con = $dato['usu_clave'];
		$ema = $dato['usu_email'];
		$epr = $dato['epr_clave_int'];		
		$act = $dato['usu_sw_activo'];
		$per = $dato['prf_clave_int'];
		$sal = $dato['usu_salario'];
		$vrhora = $dato['usu_vr_hora'];
		$fac = $dato['fac_clave_int'];
		$rut = $dato['usu_rut'];
		
		function decrypt($string, $key)
		{
			$result = "";
			$string = base64_decode($string);
			for($i=0; $i<strlen($string); $i++) 
			{
				$char = substr($string, $i, 1);
				$keychar = substr($key, ($i % strlen($key))-1, 1);
				$char = chr(ord($char)-ord($keychar));
				$result.=$char;
			}
			return $result;
		}
		$con = decrypt($con,"p4v4svasquez");
?>
		<table style="width: 38%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Nombre:</td>
			<td class="auto-style3"><input class="inputs" name="nombre1" maxlength="50" value="<?php echo $nom; ?>" type="text" style="width: 250px" />
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Cédula:</td>
			<td class="auto-style3">
			<input class="inputs" name="cedula1" id="cedula1" maxlength="50" type="text" value="<?php echo $ced; ?>" style="width: 250px" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Usuario:</td>
			<td class="auto-style3"><input class="inputs" name="usuario1" maxlength="15" type="text" value="<?php echo $usu; ?>" style="width: 250px" />
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Contraseña:</td>
			<td class="auto-style3"><input class="inputs" name="Password3" maxlength="15" type="password" value="<?php echo $con; ?>" style="width: 250px" />
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Repetir Contraseña:</td>
			<td class="auto-style3">
			<input class="inputs" name="Password4" maxlength="15" onkeyup="VALIDAR('CONTRASENA')" type="password" value="<?php echo $con; ?>" style="width: 250px" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Perfil:</td>
			<td class="auto-style3">
				<select class="inputs" name="perfil1" style="width: 260px">
				<?php
					$con = mysqli_query($conectar,"select * from perfil order by prf_descripcion");
					$num = mysqli_num_rows($con);
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$clave = $dato['prf_clave_int'];
						$perfil = $dato['prf_descripcion'];
				?>
					<option value="<?php echo $clave; ?>" <?php $con1 = mysqli_query($conectar,"select prf_clave_int from usuario where usu_clave_int = '".$usuedi."'"); $dato = mysqli_fetch_array($con1); $p = $dato['prf_clave_int']; if($p == $clave){  echo "selected='selected'"; } ?>><?php echo $perfil; ?></option>
				<?php
					}
				?>
				</select>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">E-mail:</td>
			<td class="auto-style3"><input class="inputs" name="email1" maxlength="50" type="text" value="<?php echo $ema; ?>" style="width: 250px" /></td>
			<td>&nbsp;</td>
		</tr>
		<?php
		$con = mysqli_query($conectar,"select * from perfil where prf_clave_int = '".$per."'");
		$dato = mysqli_fetch_array($con);
		$prf = $dato['prf_descripcion'];
		?>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Rut:</td>
			<td class="auto-style3">
			<input class="inputs" name="rut1" id="rut1" maxlength="20" type="text" value="<?php echo $rut; ?>" style="width: 250px" />
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Salario Neto:</td>
			<td class="auto-style3">
			<input class="inputs" name="salario1" id="salario1" type="text" value="<?php echo $sal; ?>" style="width: 250px" />
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Valor Hora:</td>
			<td class="auto-style3">
			<input class="inputs" name="valorhora1" id="valorhora1" type="text" value="<?php echo $vrhora; ?>" style="width: 250px" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Fact. Prestacional:</td>
			<td class="auto-style3">
			<select class="inputs" name="factor1" id="factor1" style="width: 260px">
			<option value="">-Seleccione-</option>
			<?php
				$con = mysqli_query($conectar,"select * from factor_prestacional order by fac_nombre");
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$clave = $dato['fac_clave_int'];
					$nom = $dato['fac_nombre'];
					$tar = $dato['fac_tarifa'];
			?>
				<option value="<?php echo $clave; ?>" <?php if($fac == $clave){ echo 'selected="selected"'; } ?>><?php echo $nom." / ".$tar; ?></option>
			<?php
				}
			?>
			</select>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3" colspan="2">Activo:<input class="inputs" <?php if($act == 1){ echo 'checked="checked"'; } ?> name="activo1" type="checkbox" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4">
			<input name="submit" type="button" value="Guardar" onclick="GUARDAR('USUARIO','<?php echo $usuedi; ?>')"  style="width: 450px; height: 25px; cursor:pointer" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">
			<div id="datos">
			
			</div>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
<?php
		exit();
	}
	if($_GET['editarper'] == 'si')
	{
		$peredi = $_GET['peredi'];
		$con = mysqli_query($conectar,"select * from perfil where prf_clave_int = '".$peredi."'"); 
		$dato = mysqli_fetch_array($con); 
		$cod = $dato['prf_codigo'];
		$des = $dato['prf_descripcion'];
?>
		<table style="width: 60%">
			<tr>
				<td>
				<strong>Editar perfil:</strong></td>
				<td>
				<input class="inputs" name="codigoper1" id="codigoper1" value="<?php echo $cod; ?>" maxlength="70" type="text" value="Código" onBlur="if(this.value=='') this.value='Código'" onFocus="if(this.value =='Código' ) this.value=''" style="width: 150px" /></td>
				<td>
				<input class="inputs" name="descripcionper1" id="descripcionper1" value="<?php echo $des; ?>" maxlength="70" type="text" value="Descripción" onBlur="if(this.value=='') this.value='Descripción'" onFocus="if(this.value =='Descripción' ) this.value=''" style="width: 150px" /></td>
				<td><a onclick="GUARDAR('PERFIL','<?php echo $peredi; ?>')" style="cursor:pointer" class="auto-style26"><span class="auto-style27">
				<strong>ACTUALIZAR PERFIL</strong></span></a></td>
				<td>
				<a onclick="NUEVO('PERFIL1')" style="cursor:pointer" class="auto-style26"><span class="auto-style27">
				<strong>NUEVO PERFIL</strong></span></a>
				</td>
			</tr>
		</table>
<?php
		exit();
	}
	if($_GET['nuevoper'] == 'si')
	{
?>
		<table style="width: 60%">
			<tr>
				<td>
				<strong>Nuevo perfil:</strong></td>
				<td>
				<input class="inputs" name="codigoper" id="codigoper" maxlength="70" type="text" value="Código" onBlur="if(this.value=='') this.value='Código'" onFocus="if(this.value =='Código' ) this.value=''" style="width: 150px" /></td>
				<td>
				<input class="inputs" name="descripcionper" id="descripcionper" maxlength="70" type="text" value="Descripción" onBlur="if(this.value=='') this.value='Descripción'" onFocus="if(this.value =='Descripción' ) this.value=''" style="width: 150px" /></td>
				<td><a onclick="NUEVO('PERFIL')" style="cursor:pointer" class="auto-style26"><span class="auto-style27">
				<strong>GUARDAR PERFIL</strong></span></a></td>
				<td></td>
			</tr>
		</table>
<?php
		exit();
	}
	if($_GET['guardarusu'] == 'si')
	{
		sleep(1);
		$nom = $_GET['nom'];
		$ced = $_GET['ced'];
		$usu = $_GET['usu'];
		$con1 = $_GET['con1'];
		$con2 = $_GET['con2'];
		$per = $_GET['per'];
		$ema = $_GET['ema'];
		$sal = $_GET['sal'];
		$valhor = $_GET['valhor'];
		$fact = $_GET['fact'];
		$rut = $_GET['rut'];
		$act = $_GET['act'];
		$lu = $_GET['lu'];
		$lc = $_GET['lc'];
		$u = $_GET['u'];
		
		$sql = mysqli_query($conectar,"select * from usuario where (UPPER(usu_usuario) = UPPER('".$usu."') OR UPPER(usu_email) = UPPER('".$ema."') OR UPPER(usu_rut) = UPPER('".$rut."')) AND usu_clave_int <> '".$u."'");
		$dato = mysqli_fetch_array($sql);
		$conusu = $dato['usu_usuario'];
		$conema = $dato['usu_email'];
		$conrut = $dato['usu_rut'];
		
		if($nom == '')
		{
			echo "<div class='validaciones'>Debe ingresar el Nombre</div>";
		}
		else
		if($usu == '' || is_null($usu))
		{
			echo "<div class='validaciones'>Debe ingresar nombre de Usuario</div>";
		}
		else
		if(STRTOUPPER($conusu) == STRTOUPPER($usu))
		{
			echo "<div class='validaciones'>El usuario ingresado ya existe</div>";
		}
		else
		if($lu < 6)
		{
			echo "<div class='validaciones'>El usuario debe ser mí­nimo de 6 dijitos $usu</div>";
		}
		else
		if($con1 != $con2)
		{
			echo "<div class='validaciones'>Las contraseñas no coinciden</div>";
		}
		else
		if($lc < 6)
		{
			echo "<div class='validaciones'>La contraseña debe ser mí­nimo de 6 dijitos</div>";
		}
		else
		if($conrut != '' and $rut != '' and STRTOUPPER($conrut) == STRTOUPPER($rut))
		{
			echo "<div class='validaciones'>El rut ingresado ya existe</div>";
		}
		else
		{
			if($act == 'false'){$swact = 0;}elseif($act == 'true'){$swact = 1;}
			if($conema != '' and $ema != '' and STRTOUPPER($conema) == STRTOUPPER($ema))
			{
				echo "<div class='validaciones'>El e-mail ingresado ya existe</div>";
			}
			else
			{
				//$con1 = hash_hmac('sha512', 'salt' . $con1, 'p4v4svasquez');
				function encrypt($string, $key) 
				{
					$result = "";	
					for($i=0; $i<strlen($string); $i++) 
					{
						$char = substr($string, $i, 1);
						$keychar = substr($key, ($i % strlen($key))-1, 1);
						$char = chr(ord($char)+ord($keychar));
						$result.=$char;
					}
					return base64_encode($result);
				}
				
				$con = mysqli_query($conectar,"update usuario set usu_usuario = '".$usu."', usu_clave = '".encrypt($con1,"p4v4svasquez")."', usu_nombre = '".$nom."', usu_cedula = '".$ced."', prf_clave_int = '".$per."', usu_sw_activo = '".$swact."', usu_email = '".$ema."', usu_usu_actualiz = '".$usuario."', usu_fec_actualiz = '".$fecha."', usu_salario = '".$sal."', usu_vr_hora = '".$valhor."', fac_clave_int = '".$fact."', usu_rut = '".$rut."' where usu_clave_int = '".$u."'");

				if($con >= 1)
				{
					//mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,1,3,'".$u."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 3=Actualización usuario
					echo "<div class='ok'>Datos grabados correctamente</div>";
				}
				else
				{
					echo "<div class='validaciones'>No se han podido guardar los datos</div>";
				}
			}	
		}

		exit();
	}
	if($_GET['guardarper'] == 'si')
	{
		sleep(1);
		$cod = $_GET['cod'];
		$des = $_GET['des'];
		$p = $_GET['p'];
		
		if($cod == 'Código'){ $cod = ''; }
		if($des == 'Descripción'){ $des = ''; }
		
		$con = mysqli_query($conectar,"select prf_codigo from perfil where prf_codigo = '".$cod."' and prf_clave_int <> '".$p."'");
		$datocod = mysqli_fetch_array($con);
		$concod = $datocod['prf_codigo'];
		
		$con = mysqli_query($conectar,"select prf_descripcion from perfil where UPPER(prf_descripcion) = UPPER('".$des."') and prf_clave_int <> '".$p."'");
		$datodes = mysqli_fetch_array($con);
		$condes = $datodes['prf_descripcion'];
		
		if($cod == '')
		{
			echo "<div class='validaciones' style='width: 99%' align='center'>Debe ingresar el Código</div>";
		}
		else
		if($cod == $concod)
		{
			echo "<div class='validaciones' style='width: 99%' align='center'>El Código ingresado ya existe</div>";
		}
		else
		if($des == '')
		{
			echo "<div class='validaciones' style='width: 99%' align='center'>Debe ingresar la Descripción</div>";
		}
		else
		if(STRTOUPPER($des) == STRTOUPPER($condes))
		{
			echo "<div class='validaciones' style='width: 99%' align='center'>La Descripción ingresada ya existe</div>";
		}
		else
		{			
			$con = mysqli_query($conectar,"update perfil set prf_codigo = '".$cod."', prf_descripcion = '".$des."', prf_usu_actualiz = '".$usuario."', prf_fec_actualiz = '".$fecha."' where prf_clave_int = '".$p."'");
			
			if($con >= 1)
			{
				echo "<div class='ok'>Datos grabados correctamente</div>";
			}
			else
			{
				echo "<div class='validaciones'>No se han podido guardar los datos</div>";
			}
		}
		
		$rows=mysqli_query($conectar,"select * from perfil");
		$total=mysqli_num_rows($rows);
?>
		<table style="width: 100%;border-collapse:collapse">
			<tr>
				<td class="auto-style3" style="width: 27px">
					<input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<?php echo $total;?>,this);" class="auto-style6" /><span class="auto-style6">
					</span>
				</td>
				<td class="auto-style3" colspan="5">
				<?php 
				if($metodo == 1)
				{
				?>
				<table style="width: 30%">
					<tr>
						<td class="auto-style1">
						<p style="cursor:pointer; width: 100px;">
						<img src="../../images/eliminar.png" alt="" class="auto-style6" /><input type="submit" value="Eliminar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
						<td class="auto-style1"></td>
						<td class="auto-style1"></td>
					</tr>
				</table>
				<?php
				}
				?>
				</td>
			</tr>
			<tr>
				<td class="auto-style5" style="width: 27px">&nbsp;</td>
				<td class="auto-style5" style="width: 180px"><strong>Código</strong></td>
				<td class="auto-style5" style="width: 120px"><strong>Descripción</strong></td>
				<td class="auto-style5" style="width: 98px"><strong>Creado Por</strong></td>
				<td class="auto-style5" style="width: 127px"><strong>
					Actualización</strong></td>
				<td class="auto-style5">&nbsp;</td>
			</tr>
			<tr>
				<td class="auto-style3" colspan="9"><hr></td>
			</tr>
			<?php
				$contador=0;
				$con = mysqli_query($conectar,"select * from perfil order by prf_descripcion");
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$claprf = $dato['prf_clave_int'];
					$cod = $dato['prf_codigo'];
					$des = $dato['prf_descripcion'];
					$usuact = $dato['prf_usu_actualiz'];
					$fecact = $dato['prf_fec_actualiz'];
					$contador=$contador+1;
			?>
			<tr style="<?php if($i % 2 == 0){ echo 'background-color:#B8CEB8'; }else{ echo 'background-color:#ffffff'; } ?>">
				<td class="auto-style3" style="width: 27px">
					<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<?php echo $contador;?>" value="<?php echo $claprf; ?>" class="auto-style6" /></td>
				<td class="auto-style5" style="width: 180px"><?php echo $cod; ?></td>
				<td class="auto-style5" style="width: 120px"><?php echo $des; ?></td>
				<td class="auto-style5" style="width: 98px"><?php echo $usuact; ?></td>
				<td class="auto-style5" style="width: 127px"><?php echo $fecact; ?></td>
				<td class="auto-style5">
				<?php 
				if($metodo == 1)
				{
				?>
				<a style="cursor:pointer" onclick="EDITAR('<?php echo $claprf; ?>','PERFIL')"><img src="../../images/editar.png" alt="" height="22" width="21" /></a>
				<?php
				}
				?>
				</td>
			</tr>
			<?php
				}
			?>
			<tr>
				<td class="auto-style5" style="width: 27px">&nbsp;</td>
				<td class="auto-style5" style="width: 180px">&nbsp;</td>
				<td class="auto-style5" style="width: 120px">&nbsp;</td>
				<td class="auto-style5" style="width: 98px">&nbsp;</td>
				<td class="auto-style5" style="width: 127px">&nbsp;</td>
				<td class="auto-style5">&nbsp;</td>
			</tr>
		</table>
<?php
		exit();
	}
	if($_GET['contrasena'] == 'si')
	{
		$con1 = $_GET['con1'];
		$con2 = $_GET['con2'];
		if($con1 != $con2)
		{
			echo "<div class='validaciones'>Las contraseñas no coinciden</div>";
		}
		else
		{
			
		}
		exit();
	}
	
	if($_GET['nuevousuario'] == 'si')
	{
		sleep(1);
		$nom = $_GET['nom'];
		$ced = $_GET['ced'];
		$usu = $_GET['usu'];
		$con1 = $_GET['con1'];
		$con2 = $_GET['con2'];
		$per = $_GET['per'];
		$ema = $_GET['ema'];
		$rut = $_GET['rut'];
		$sal = $_GET['sal'];
		$valhor = $_GET['valhor'];
		$fact = $_GET['fact'];
		$act = $_GET['act'];
		$lu = $_GET['lu'];
		$lc = $_GET['lc'];
		
		$fecha=date("Y/m/d H:i:s");
		
		$sql = mysqli_query($conectar,"select * from usuario where (UPPER(usu_usuario) = UPPER('".$usu."') OR UPPER(usu_email) = UPPER('".$ema."') OR UPPER(usu_rut) = UPPER('".$rut."'))");
		$dato = mysqli_fetch_array($sql);
		$conusu = $dato['usu_usuario'];
		$conema = $dato['usu_email'];
		$conrut = $dato['usu_rut'];
		
		if($nom == '')
		{
			echo "<div class='validaciones'>Debe ingresar el Nombre</div>";
		}
		else
		if($usu == '' || is_null($usu))
		{
			echo "<div class='validaciones'>Debe ingresar nombre de Usuario</div>";
		}
		else
		if(STRTOUPPER($conusu) == STRTOUPPER($usu))
		{
			echo "<div class='validaciones'>El usuario ingresado ya existe</div>";
		}
		else
		if($lu < 6)
		{
			echo "<div class='validaciones'>El usuario debe ser mí­nimo de 6 dijitos</div>";
		}
		else
		if($con1 != $con2)
		{
			echo "<div class='validaciones'>Las contraseñas no coinciden</div>";
		}
		else
		if($lc < 6)
		{
			echo "<div class='validaciones'>La contraseña debe ser mí­nimo de 6 dijitos</div>";
		}
		else
		if($conrut != '' and $rut != '' and STRTOUPPER($conrut) == STRTOUPPER($rut))
		{
			echo "<div class='validaciones'>El rut ingresado ya existe</div>";
		}
		else
		{
			if($act == 'false'){$swact = 0;}elseif($act == 'true'){$swact = 1;}
			if($conema != '' and $ema != '' and STRTOUPPER($conema) == STRTOUPPER($ema))
			{
				echo "<div class='validaciones'>El e-mail ingresado ya existe</div>";
			}
			else
			{
				//$con1 = hash_hmac('sha512', 'salt' . $con1, 'p4v4svasquez');
				function encrypt($string, $key) 
				{
					$result = "";	
					for($i=0; $i<strlen($string); $i++) 
					{
						$char = substr($string, $i, 1);
						$keychar = substr($key, ($i % strlen($key))-1, 1);
						$char = chr(ord($char)+ord($keychar));
						$result.=$char;
					}
					return base64_encode($result);
				}
				
				$con = mysqli_query($conectar,"insert into usuario(usu_usuario,usu_clave,usu_nombre,usu_cedula,prf_clave_int,usu_sw_activo,usu_email,usu_salario,usu_vr_hora,fac_clave_int,usu_rut,usu_usu_actualiz,usu_fec_actualiz) values('".$usu."','".encrypt($con1,"p4v4svasquez")."','".$nom."','".$ced."','".$per."','".$swact."','".$ema."','".$sal."','".$valhor."','".$fact."','".$rut."','".$usuario."','".$fecha."')");
								
				if($con >= 1)
				{
					echo "<div class='ok'>Datos grabados correctamente</div>";
					/*					
					//Busco el usuario grabado, no por el último, si no por los datos grabados
					$con = mysqli_query($conectar,"select usu_clave_int from usuario where usu_usuario = '".$usu."'");
					$dato = mysqli_fetch_array($con);
					$clausu = $dato['usu_clave_int'];
					
					mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,1,2,'".$clausu."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 2=Creación usuario*/
				}
				else
				{
					echo "<div class='validaciones'>No se han podido guardar los datos</div>";
				}
			}	
		}

		exit();
	}
	if($_GET['todos'] == 'si')
	{
		sleep(1);
		$rows=mysqli_query($conectar,"select * from usuario");
		$total=mysqli_num_rows($rows);
?>
		<table style="width: 100%;border-collapse:collapse">
		<tr>
			<td class="auto-style3" style="width: 27px">
				<input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<?php echo $total;?>,this);" class="auto-style6" /><span class="auto-style6">
				</span>
			</td>
			<td class="auto-style3" colspan="13">
			<?php 
			if($metodo == 1)
			{
			?>
			<table style="width: 30%">
				<tr>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../images/activo.png" alt="" class="auto-style6" /><input type="submit" value="Activar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../images/inactivo.png" alt="" class="auto-style6" /><input type="submit" value="Inactivar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../images/eliminar.png" alt="" class="auto-style6" /><input type="submit" value="Eliminar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
				</tr>
			</table>
			<?php
			}
			?>
			</td>
		</tr>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 180px"><strong>Nombre</strong></td>
			<td class="auto-style5" style="width: 120px"><strong>Cédula</strong></td>
			<td class="auto-style5" style="width: 120px"><strong>Usuario</strong></td>
			<td class="auto-style5" style="width: 110px"><strong>Perfil</strong></td>
			<td class="auto-style5" style="width: 200px"><strong>E-mail</strong></td>
			<td class="auto-style5" style="width: 130px"><strong>Rut</strong></td>
			<td class="auto-style5" style="width: 130px"><strong>Salario Neto</strong></td>
			<td class="auto-style5" style="width: 130px"><strong>Valor Hora</strong></td>
			<td class="auto-style5" style="width: 130px"><strong>Fact. Prestacional</strong></td>
			<td class="auto-style5" style="width: 130px"><strong>Creado Por</strong></td>
			<td class="auto-style5" style="width: 98px"><strong>Actualización</strong></td>
			<td class="auto-style5" style="width: 65px"><strong>
			Activo</strong></td>
			<td class="auto-style5" style="width: 29px">&nbsp;</td>
		</tr>
		<tr>
			<td class="auto-style3" colspan="14"><hr></td>
		</tr>
		<?php
			$contador=0;
			$con = mysqli_query($conectar,"select * from usuario u inner join perfil prf ON (prf.prf_clave_int = u.prf_clave_int) left outer join factor_prestacional f on (f.fac_clave_int = u.fac_clave_int) order by u.usu_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clausu = $dato['usu_clave_int'];
				$nom = $dato['usu_nombre'];
				$ced = $dato['usu_cedula'];
				$usu = $dato['usu_usuario'];
				$pernom = $dato['prf_descripcion'];
				$act = $dato['usu_sw_activo'];
				$ema = $dato['usu_email'];
				$rut = $dato['usu_rut'];
				$sal = $dato['usu_salario'];
				$valhor = $dato['usu_vr_hora'];
				$fact = $dato['fac_tarifa'];
				if($fact == '' or $fact == null){ $fact = 0; }
				$epr = $dato['epr_nombre'];
				$usuact = $dato['usu_usu_actualiz'];
				$fecact = $dato['usu_fec_actualiz'];
				$contador=$contador+1;
		?>
		<tr style="<?php if($i % 2 == 0){ echo 'background-color:#B8CEB8'; }else{ echo 'background-color:#ffffff'; } ?>">
			<td class="auto-style3" style="width: 27px">
				<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<?php echo $contador;?>" value="<?php echo $dato['usu_clave_int'];?>" class="auto-style6" /></td>
			<td class="auto-style5" style="width: 180px"><?php echo $nom; ?></td>
			<td class="auto-style5" style="width: 120px"><?php echo $ced; ?></td>
			<td class="auto-style5" style="width: 120px"><?php echo $usu; ?></td>
			<td class="auto-style5" style="width: 110px"><?php echo $pernom; ?></td>
			<td class="auto-style5" style="width: 200px"><?php echo $ema; ?></td>
			<td class="auto-style5" style="width: 130px"><?php echo $rut; ?></td>
			<td class="auto-style5" style="width: 130px"><?php echo "$".number_format($sal,0 , "," ,"."); ?></td>
			<td class="auto-style5" style="width: 130px"><?php echo "$".number_format($valhor,0 , "," ,"."); ?></td>
			<td class="auto-style5" style="width: 130px"><?php echo $fact; ?></td>
			<td class="auto-style5" style="width: 130px"><?php echo $usuact; ?></td>
			<td class="auto-style5" style="width: 98px"><?php echo $fecact; ?></td>
			<td class="auto-style3" style="width: 65px">
			<input name="activarinactivar" id="activarinactivar" type="checkbox" <?php if($act == 1){ echo 'checked="checked"'; } ?> disabled="disabled" class="auto-style6" ></td>
			<td class="auto-style5" style="width: 29px">
			<?php 
			if($metodo == 1)
			{
			?>
			<a data-reveal-id="editarusuario" data-animation="fade" style="cursor:pointer" onclick="EDITAR('<?php echo $dato['usu_clave_int']; ?>','USUARIO')"><img src="../../images/editar.png" alt="" height="22" width="21" /></a>
			<?php
			}
			?>
			</td>
		</tr>
		<?php
			}
		?>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 180px">&nbsp;</td>
			<td class="auto-style5" style="width: 120px">&nbsp;</td>
			<td class="auto-style5" style="width: 120px">&nbsp;</td>
			<td class="auto-style5" style="width: 110px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 130px">&nbsp;</td>
			<td class="auto-style5" style="width: 130px">&nbsp;</td>
			<td class="auto-style5" style="width: 130px">&nbsp;</td>
			<td class="auto-style5" style="width: 130px">&nbsp;</td>
			<td class="auto-style5" style="width: 130px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 98px">&nbsp;</td>
			<td class="auto-style5" style="width: 98px">&nbsp;</td>
		</tr>
	</table>
<?php
		exit();
	}
	if($_GET['perfil'] == 'si')
	{
		sleep(1);
		
		if($_GET['nuevo'] == 'si')
		{
			$fecha=date("Y/m/d H:i:s");
			$cod = $_GET['cod'];
			$des = $_GET['des'];
			
			if($cod == 'Código'){ $cod = ''; }
			if($des == 'Descripción'){ $des = ''; }
			
			$con = mysqli_query($conectar,"select prf_codigo from perfil where prf_codigo = '".$cod."'");
			$datocod = mysqli_fetch_array($con);
			$concod = $datocod['prf_codigo'];
			
			$con = mysqli_query($conectar,"select prf_descripcion from perfil where UPPER(prf_descripcion) = UPPER('".$des."')");
			$datodes = mysqli_fetch_array($con);
			$condes = $datodes['prf_descripcion'];
			
			if($cod == '')
			{
				echo "<div class='validaciones' style='width: 99%' align='center'>Debe ingresar el Código</div>";
			}
			else
			if($cod == $concod)
			{
				echo "<div class='validaciones' style='width: 99%' align='center'>El Código ingresado ya existe</div>";
			}
			else
			if($des == '')
			{
				echo "<div class='validaciones' style='width: 99%' align='center'>Debe ingresar la Descripción</div>";
			}
			else
			if(STRTOUPPER($des) == STRTOUPPER($condes))
			{
				echo "<div class='validaciones' style='width: 99%' align='center'>La Descripción ingresada ya existe</div>";
			}
			else
			{
				$con = mysqli_query($conectar,"insert into perfil(prf_clave_int,prf_codigo,prf_descripcion,prf_usu_actualiz,prf_fec_actualiz) values(null,'".$cod."','".$des."','".$usuario."','".$fecha."')");
				
				if($con > 0)
				{
					$conp = mysqli_query($conectar,"select prf_clave_int from perfil where UPPER(prf_descripcion) = UPPER('".$des."')");
					$datop = mysqli_fetch_array($conp);
					$claprf = $datop['prf_clave_int'];
					
					mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,1,7,'".$claprf."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 7=Creación perfil
					echo "<div class='ok' style='width: 99%' align='center'>Datos Guardados Correctamente</div>";
				}
				else
				{
					echo "<div class='validaciones' style='width: 99%' align='center'>No se han podido guardar los datos</div>";
				}
			}
		}
		$rows=mysqli_query($conectar,"select * from perfil");
		$total=mysqli_num_rows($rows);
?>
		<table style="width: 100%;border-collapse:collapse">
		<tr>
			<td class="auto-style3" style="width: 27px">
				<input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<?php echo $total;?>,this);" class="auto-style6" /><span class="auto-style6">
				</span>
			</td>
			<td class="auto-style3" colspan="5">
			<?php 
			if($metodo == 1)
			{
			?>
			<table style="width: 30%">
				<tr>
					<td class="auto-style1">
					<p style="cursor:pointer; width: 120px;">
					<img src="../../images/eliminar.png" alt="" class="auto-style6" /><input type="submit" value="Eliminar Perfil" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"></td>
					<td class="auto-style1"></td>
				</tr>
			</table>
			<?php
			}
			?>
			</td>
			<td class="auto-style3">
			&nbsp;</td>
		</tr>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 180px"><strong>Código</strong></td>
			<td class="auto-style5" style="width: 120px"><strong>Descripción</strong></td>
			<td class="auto-style5" style="width: 98px"><strong>Creado Por</strong></td>
			<td class="auto-style5" style="width: 127px"><strong>
				Actualización</strong></td>
			<td class="auto-style5" style="width: 30px">&nbsp;</td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
		<tr>
			<td class="auto-style3" colspan="9"><hr></td>
		</tr>
		<?php
			$contador=0;
			$con = mysqli_query($conectar,"select * from perfil order by prf_descripcion");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$claprf = $dato['prf_clave_int'];
				$cod = $dato['prf_codigo'];
				$des = $dato['prf_descripcion'];
				$usuact = $dato['prf_usu_actualiz'];
				$fecact = $dato['prf_fec_actualiz'];
				$contador=$contador+1;
		?>
		<tr style="<?php if($i % 2 == 0){ echo 'background-color:#B8CEB8'; }else{ echo 'background-color:#ffffff'; } ?>">
			<td class="auto-style3" style="width: 27px">
				<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<?php echo $contador;?>" value="<?php echo $claprf; ?>" class="auto-style6" /></td>
			<td class="auto-style5" style="width: 180px"><?php echo $cod; ?></td>
			<td class="auto-style5" style="width: 120px"><?php echo $des; ?></td>
			<td class="auto-style5" style="width: 98px"><?php echo $usuact; ?></td>
			<td class="auto-style5" style="width: 127px"><?php echo $fecact; ?></td>
			<td class="auto-style5" style="width: 30px">
			<?php 
			if($metodo == 1)
			{
			?>
			<a style="cursor:pointer" onclick="EDITAR('<?php echo $claprf; ?>','PERFIL')"><img src="../../images/editar.png" alt="" height="22" width="21" title="Editar" /></a>
			<?php
			}
			?>
			</td>
			<td class="auto-style5">
			<?php 
			if($metodo == 1)
			{
			?>
			<a data-reveal-id="permisos" data-animation="fade" style="cursor:pointer" onclick="PERMISOS('<?php echo $claprf; ?>')"><img src="../../images/permisos.png" alt="" height="22" width="21" title="Permisos" /></a>
			<?php
			}
			?>
			</td>
		</tr>
		<?php
			}
		?>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 180px">&nbsp;</td>
			<td class="auto-style5" style="width: 120px">&nbsp;</td>
			<td class="auto-style5" style="width: 98px">&nbsp;</td>
			<td class="auto-style5" style="width: 127px">&nbsp;</td>
			<td class="auto-style5" style="width: 30px">&nbsp;</td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
	</table>
<?php
		exit();
	}
	if($_GET['buscarusu'] == 'si')
	{
		$nom = $_GET['nom'];
		$ema = $_GET['ema'];
		$usu = $_GET['usu'];
		
		if($nom == 'Nombre'){ $nom = ''; }
		if($ema == 'E-mail'){ $ema = ''; }
		if($usu == 'Usuario'){ $usu = ''; }

		$rows=mysqli_query($conectar,"select * from usuario u where (u.usu_nombre LIKE REPLACE('%".$nom."%',' ','%') OR '".$nom."' IS NULL OR '".$nom."' = '') and (u.usu_email LIKE REPLACE('".$ema."%',' ','%')  OR '".$ema."' IS NULL OR '".$ema."' = '') and (u.usu_usuario LIKE '".$usu."%' OR '".$usu."' IS NULL OR '".$usu."' = '')");
		$total=mysqli_num_rows($rows);
?>
		<table style="width: 100%;border-collapse:collapse">
		<tr>
			<td class="auto-style3" style="width: 27px">
				<input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<?php echo $total;?>,this);" class="auto-style6" /><span class="auto-style6">
				</span>
			</td>
			<td class="auto-style3" colspan="13">
			<?php 
			if($metodo == 1)
			{
			?>
			<table style="width: 30%">
				<tr>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../images/activo.png" alt="" class="auto-style6" /><input type="submit" value="Activar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../images/inactivo.png" alt="" class="auto-style6" /><input type="submit" value="Inactivar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../images/eliminar.png" alt="" class="auto-style6" /><input type="submit" value="Eliminar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
				</tr>
			</table>
			<?php
			}
			?>
			</td>
		</tr>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 180px"><strong>Nombre</strong></td>
			<td class="auto-style5" style="width: 120px"><strong>Cédula</strong></td>
			<td class="auto-style5" style="width: 120px"><strong>Usuario</strong></td>
			<td class="auto-style5" style="width: 110px"><strong>Perfil</strong></td>
			<td class="auto-style5" style="width: 200px"><strong>E-mail</strong></td>
			<td class="auto-style5" style="width: 130px"><strong>Rut</strong></td>
			<td class="auto-style5" style="width: 130px"><strong>Salario Neto</strong></td>
			<td class="auto-style5" style="width: 130px"><strong>Valor Hora</strong></td>
			<td class="auto-style5" style="width: 130px"><strong>Fact. Prestacional</strong></td>
			<td class="auto-style5" style="width: 130px"><strong>Creado Por</strong></td>
			<td class="auto-style5" style="width: 98px"><strong>Actualización</strong></td>
			<td class="auto-style5" style="width: 65px"><strong>
			Activo</strong></td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
		<tr>
			<td class="auto-style3" colspan="14"><hr></td>
		</tr>
		<?php
			$contador=0;
			$con = mysqli_query($conectar,"select * from usuario u inner join perfil prf ON (prf.prf_clave_int = u.prf_clave_int) left outer join factor_prestacional f on (f.fac_clave_int = u.fac_clave_int) where (u.usu_nombre LIKE REPLACE('%".$nom."%',' ','%') OR '".$nom."' IS NULL OR '".$nom."' = '') and (u.usu_email LIKE REPLACE('".$ema."%',' ','%')  OR '".$ema."' IS NULL OR '".$ema."' = '') and (u.usu_usuario LIKE '".$usu."%' OR '".$usu."' IS NULL OR '".$usu."' = '') order by u.usu_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clausu = $dato['usu_clave_int'];
				$nom = $dato['usu_nombre'];
				$ced = $dato['usu_cedula'];
				$usu = $dato['usu_usuario'];
				$pernom = $dato['prf_descripcion'];
				$act = $dato['usu_sw_activo'];
				$ema = $dato['usu_email'];
				$rut = $dato['usu_rut'];
				$sal = $dato['usu_salario'];
				$valhor = $dato['usu_vr_hora'];
				$fact = $dato['fac_tarifa'];
				if($fact == '' or $fact == null){ $fact = 0; }
				$epr = $dato['epr_nombre'];
				$usuact = $dato['usu_usu_actualiz'];
				$fecact = $dato['usu_fec_actualiz'];
				$contador=$contador+1;
		?>
		<tr style="<?php if($i % 2 == 0){ echo 'background-color:#B8CEB8'; }else{ echo 'background-color:#ffffff'; } ?>">
			<td class="auto-style3" style="width: 27px">
				<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<?php echo $contador;?>" value="<?php echo $dato['usu_clave_int'];?>" class="auto-style6" /></td>
			<td class="auto-style5" style="width: 180px"><?php echo $nom; ?></td>
			<td class="auto-style5" style="width: 120px"><?php echo $ced; ?></td>
			<td class="auto-style5" style="width: 120px"><?php echo $usu; ?></td>
			<td class="auto-style5" style="width: 110px"><?php echo $pernom; ?></td>
			<td class="auto-style5" style="width: 200px"><?php echo $ema; ?></td>
			<td class="auto-style5" style="width: 130px"><?php echo $rut; ?></td>
			<td class="auto-style5" style="width: 130px"><?php echo "$".number_format($sal,0 , "," ,"."); ?></td>
			<td class="auto-style5" style="width: 130px"><?php echo "$".number_format($valhor,0 , "," ,"."); ?></td>
			<td class="auto-style5" style="width: 130px"><?php echo $fact; ?></td>
			<td class="auto-style5" style="width: 130px"><?php echo $usuact; ?></td>
			<td class="auto-style5" style="width: 98px"><?php echo $fecact; ?></td>
			<td class="auto-style3" style="width: 65px">
			<input name="activarinactivar" id="activarinactivar" type="checkbox" <?php if($act == 1){ echo 'checked="checked"'; } ?> disabled="disabled" class="auto-style6" ></td>
			<td class="auto-style5">
			<?php 
			if($metodo == 1)
			{
			?>
			<a data-reveal-id="editarusuario" data-animation="fade" style="cursor:pointer" onclick="EDITAR('<?php echo $dato['usu_clave_int']; ?>','USUARIO')"><img src="../../images/editar.png" alt="" height="22" width="21" /></a>
			<?php
			}
			?>
			</td>
		</tr>
		<?php
			}
		?>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 180px">&nbsp;</td>
			<td class="auto-style5" style="width: 120px">&nbsp;</td>
			<td class="auto-style5" style="width: 120px">&nbsp;</td>
			<td class="auto-style5" style="width: 110px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 130px">&nbsp;</td>
			<td class="auto-style5" style="width: 130px">&nbsp;</td>
			<td class="auto-style5" style="width: 130px">&nbsp;</td>
			<td class="auto-style5" style="width: 130px">&nbsp;</td>
			<td class="auto-style5" style="width: 130px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 98px">&nbsp;</td>
			<td class="auto-style5" style="width: 98px">&nbsp;</td>
		</tr>
	</table>
<?php
		exit();
	}
	if($_GET['buscarper'] == 'si')
	{
		$cod = $_GET['cod'];
		$des = $_GET['des'];
		
		if($cod == 'Codigo'){ $cod = ''; }
		if($des == 'Descripcion'){ $des = ''; }

		$rows=mysqli_query($conectar,"select * from perfil where (prf_codigo LIKE REPLACE('".$cod."%',' ','%') OR '".$cod."' IS NULL OR '".$cod."' = '') and (prf_descripcion LIKE REPLACE('".$des."%',' ','%')  OR '".$des."' IS NULL OR '".$des."' = '')");
		$total=mysqli_num_rows($rows);
?>
		<table style="width: 100%;border-collapse:collapse">
		<tr>
			<td class="auto-style3" style="width: 27px">
				<input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<?php echo $total;?>,this);" class="auto-style6" /><span class="auto-style6">
				</span>
			</td>
			<td class="auto-style3" colspan="5">
			<?php 
			if($metodo == 1)
			{
			?>
			<table style="width: 30%">
				<tr>
					<td class="auto-style1">
					<p style="cursor:pointer; width: 120px;">
					<img src="../../images/eliminar.png" alt="" class="auto-style6" /><input type="submit" value="Eliminar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"></td>
					<td class="auto-style1"></td>
				</tr>
			</table>
			<?php
			}
			?>
			</td>
			<td class="auto-style3">
			&nbsp;</td>
		</tr>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 180px"><strong>Código</strong></td>
			<td class="auto-style5" style="width: 120px"><strong>Descripción</strong></td>
			<td class="auto-style5" style="width: 98px"><strong>Usuario</strong></td>
			<td class="auto-style5" style="width: 127px"><strong>
				Actualización</strong></td>
			<td class="auto-style5" style="width: 30px">&nbsp;</td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
		<tr>
			<td class="auto-style3" colspan="9"><hr></td>
		</tr>
		<?php
			$contador=0;
			$con = mysqli_query($conectar,"select * from perfil where (prf_codigo LIKE REPLACE('".$cod."%',' ','%') OR '".$cod."' IS NULL OR '".$cod."' = '') and (prf_descripcion LIKE REPLACE('".$des."%',' ','%')  OR '".$des."' IS NULL OR '".$des."' = '') order by prf_descripcion");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$cla = $dato['prf_clave_int'];
				$cod = $dato['prf_codigo'];
				$des = $dato['prf_descripcion'];
				$usuact = $dato['prf_usu_actualiz'];
				$fecact = $dato['prf_fec_actualiz'];
				$contador=$contador+1;
		?>
		<tr style="<?php if($i % 2 == 0){ echo 'background-color:#B8CEB8'; }else{ echo 'background-color:#ffffff'; } ?>">
			<td class="auto-style3" style="width: 27px">
				<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<?php echo $contador;?>" value="<?php echo $claprf; ?>" class="auto-style6" /></td>
			<td class="auto-style5" style="width: 180px"><?php echo $cod; ?></td>
			<td class="auto-style5" style="width: 120px"><?php echo $des; ?></td>
			<td class="auto-style5" style="width: 98px"><?php echo $usuact; ?></td>
			<td class="auto-style5" style="width: 127px"><?php echo $fecact; ?></td>
			<td class="auto-style5" style="width: 30px">
			<?php 
			if($metodo == 1)
			{
			?>
			<a style="cursor:pointer" onclick="EDITAR('<?php echo $claprf; ?>','PERFIL')"><img src="../../images/editar.png" alt="" height="22" width="21" title="Editar" /></a>
			<?php
			}
			?>
			</td>
			<td class="auto-style5">
			<?php 
			if($metodo == 1)
			{
			?>
			<a data-reveal-id="permisos" data-animation="fade" style="cursor:pointer" onclick="PERMISOS('<?php echo $claprf; ?>')"><img src="../../images/permisos.png" alt="" height="22" width="21" title="Permisos" /></a>
			<?php
			}
			?>
			</td>
		</tr>
		<?php
			}
		?>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 180px">&nbsp;</td>
			<td class="auto-style5" style="width: 120px">&nbsp;</td>
			<td class="auto-style5" style="width: 98px">&nbsp;</td>
			<td class="auto-style5" style="width: 127px">&nbsp;</td>
			<td class="auto-style5" style="width: 30px">&nbsp;</td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
	</table>
<?php
		exit();
	}
	if($_GET['filtroperfil'] == 'si')
	{
?>
		<table style="width: 30%">
			<tr>
			<td class="auto-style1"><strong>Filtro:<img src="../../images/buscar.png" alt="" height="18" width="15" /></strong></td>
			<td class="auto-style1">
			<input class="inputs" onkeyup="BUSCAR('PERFIL')" name="buscodigoper" id="buscodigoper" maxlength="70" type="text" value="Codigo" onBlur="if(this.value=='') this.value='Codigo'" onFocus="if(this.value =='Codigo' ) this.value=''" style="width: 150px" /></td>
			<td class="auto-style1">
			<input class="inputs" onkeyup="BUSCAR('PERFIL')" name="busdescripcionper" id="busdescripcionper" maxlength="70" type="text" value="Descripcion" onBlur="if(this.value=='') this.value='Descripcion'" onFocus="if(this.value =='Descripcion' ) this.value=''" style="width: 150px" /></td>
			<td class="auto-style1">
			&nbsp;</td>
			<td class="auto-style1">
			&nbsp;</td>
			</tr>
		</table>		
<?php
		exit();
	}
	if($_GET['filtrousuario'] == 'si')
	{
?>
		<table style="width: 50%">
			<tr>
			<td class="auto-style1"><strong>Filtro:<img src="../../images/buscar.png" alt="" height="18" width="15" /></strong></td>
			<td class="auto-style1">
			<input class="inputs" onkeyup="BUSCAR('USUARIO')" name="nombre2" maxlength="70" type="text" value="Nombre" onBlur="if(this.value=='') this.value='Nombre'" onFocus="if(this.value =='Nombre' ) this.value=''" style="width: 150px" /></td>
			<td class="auto-style1">
			<input class="inputs" onkeyup="BUSCAR('USUARIO')" name="email2" maxlength="70" type="text" value="E-mail" onBlur="if(this.value=='') this.value='E-mail'" onFocus="if(this.value =='E-mail' ) this.value=''" style="width: 150px" /></td>
			<td class="auto-style1">
			<input class="inputs" onkeyup="BUSCAR('USUARIO')" name="usuario2" maxlength="70" type="text" value="Usuario" onBlur="if(this.value=='') this.value='Usuario'" onFocus="if(this.value =='Usuario' ) this.value=''" /></td>
			</tr>
		</table>		
<?php
		exit();
	}
	if($_GET['agregar'] == 'si')
	{
		$per = $_GET['per'];
		$con = mysqli_query($conectar,"select prf_descripcion from perfil where prf_clave_int = '".$per."'");
		$dato = mysqli_fetch_array($con);
		$nom = $dato['prf_descripcion'];
?>
	<table style="width: 38%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td align="center" colspan="3">
			<?php echo "<div class='ok' style='width: 99%' align='center'>Perfil: $nom</div>"; ?>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td align="center">
			<div id="agregar1" align="center">
				<fieldset name="Group1" style="height: 320px">
				<legend>Registros Sin Seleccionar</legend>
					<select name="agregarven" id="agregarven" multiple="multiple" ondblclick="AGREGARPERMISOS('<?php echo $per; ?>')" style="width: 300px; height: 300px;" size="8">
					<?php
						$con = mysqli_query($conectar,"select ven_clave_int,ven_opcion from ventana where ven_clave_int not in (select ven_clave_int from permiso where prf_clave_int = '".$per."') order by ven_opcion");
						$num = mysqli_num_rows($con);
						for($i = 0; $i < $num; $i++)
						{
							$dato = mysqli_fetch_array($con);
							$clave = $dato['ven_clave_int'];
							$ventana = $dato['ven_opcion'];
					?>
						<option value="<?php echo $clave; ?>"><?php echo $ventana; ?></option>
					<?php
						}
					?>
					</select>
				</fieldset>
			</div>
			</td>
			<td align="center">
			<div style="width: 140px">
				<input type="button" class="pasar izq" onclick="AGREGARPERMISOS('<?php echo $per; ?>')" value="Pasar &raquo;"><input type="button" onclick="ELIMINARPERMISO('<?php echo $per; ?>')" class="quitar der" value="&laquo; Quitar"><br />
				<input type="button" class="pasartodos izq" onclick="AGREGARTODOS('<?php echo $per; ?>')" value="Todos &raquo;"><input type="button" onclick="QUITARTODOS('<?php echo $per; ?>')" class="quitartodos der" value="&laquo; Todos">
			</div>
			</td>
			<td align="center">
			<div id="agregados">
				<fieldset name="Group1" style="height: 320px">
					<legend>Registros Seleccionados</legend>
						<div style="overflow:auto;height: 300px; width:300px">
							<table style="width: 100%">
							   <tr>
								   <td>&nbsp;</td>
								   <td><strong>VENTANA</strong></td>
								   <td><strong>PERMISO</strong></td>
							   </tr>
							   <?php
									$con = mysqli_query($conectar,"select p.per_clave_int cla,v.ven_opcion ven, p.per_metodo met from permiso p inner join ventana v on (v.ven_clave_int = p.ven_clave_int) where p.prf_clave_int = '".$per."'");
									$num = mysqli_num_rows($con);
									for($i = 0; $i < $num; $i++)
									{
										$dato = mysqli_fetch_array($con);
										$claven = $dato['cla'];
										$ven = $dato['ven'];
										$met = $dato['met'];
								?>
							   <tr style="cursor:pointer" class="service_list" id="service<?php echo $claven; ?>" data="<?php echo $claven; ?>">
								   <td><input name="metodoseleccionado" id="metodoseleccionado<?php echo $claven; ?>" value="<?php echo $claven; ?>" type="checkbox" /></td>
								   <td><label for="metodoseleccionado<?php echo $claven; ?>"><?php echo $ven; ?></label></td>
								   <td>
								   <select name="metodo" id="metodo<?php echo $i; ?>" onchange="METODO('<?php echo $claven; ?>','<?php echo $i; ?>')">
										<option value="0" <?php if($met == 0){ echo "selected='selected'"; } ?>>Consulta</option>
										<option value="1" <?php if($met == 1){ echo "selected='selected'"; } ?>>Modificación</option>
									</select>
								   </td>
							   </tr>
							   <tr style="cursor:pointer" class="service_list" id="service<?php echo $claven; ?>" data="<?php echo $claven; ?>">
								   <td colspan="3"><hr></td>
							   </tr>
							   <?php
									}
								?>
							   </table>
						</div>
				</fieldset>
			</div>
			</td>
			<td>&nbsp;</td>
		</tr>
		</table>
<?php
		exit();
	}
	if($_GET['agregarseleccionados'] == 'si')
	{
		$ven = $_GET['ven'];
		$per = $_GET['per'];
		
		$seleccionados = explode(',',$ven);
		
		$num = count($seleccionados);
		
		for($i = 0; $i < $num; $i++)
		{
			$con = mysqli_query($conectar,"insert into permiso(per_clave_int,prf_clave_int,ven_clave_int,per_metodo) values(null,'".$per."','".$seleccionados[$i]."',1)");
		}
		mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,1,8,'".$per."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 8=Actualización perfil
?>
		<fieldset name="Group1" style="height: 320px">
			<legend>Registros Seleccionados</legend>
				<div style="overflow:auto;height: 300px; width:300px">
					<table style="width: 100%">
					   <tr>
						   <td>&nbsp;</td>
						   <td><strong>VENTANA</strong></td>
						   <td><strong>MÉTODO</strong></td>
					   </tr>
					   <?php
							$con = mysqli_query($conectar,"select p.per_clave_int cla,v.ven_opcion ven, p.per_metodo met from permiso p inner join ventana v on (v.ven_clave_int = p.ven_clave_int) where p.prf_clave_int = '".$per."'");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$claven = $dato['cla'];
								$ven = $dato['ven'];
								$met = $dato['met'];
						?>
					   <tr style="cursor:pointer" class="service_list" id="service<?php echo $claven; ?>" data="<?php echo $claven; ?>">
						   <td><input name="metodoseleccionado" id="metodoseleccionado<?php echo $claven; ?>" value="<?php echo $claven; ?>" type="checkbox" /></td>
						   <td><label for="metodoseleccionado<?php echo $claven; ?>"><?php echo $ven; ?></label></td>
						   <td>
						   <select name="metodo" id="metodo<?php echo $i; ?>" onchange="METODO('<?php echo $claven; ?>','<?php echo $i; ?>')">
								<option value="0" <?php if($met == 0){ echo "selected='selected'"; } ?>>Consulta</option>
								<option value="1" <?php if($met == 1){ echo "selected='selected'"; } ?>>Modificación</option>
							</select>
						   </td>
					   </tr>
					   <?php
							}
						?>
				   </table>
				</div>
		</fieldset>
<?php
		exit();
	}
	if($_GET['eliminaragregados'] == 'si')
	{
		$perm = $_GET['permiso'];
		$per = $_GET['per'];
		$seleccionados = explode(',',$perm);
		
		$num = count($seleccionados);
		
		for($i = 0; $i < $num; $i++)
		{
			if($seleccionados[$i] != '')
			{
				$con = mysqli_query($conectar,"delete from permiso where per_clave_int = '".$seleccionados[$i]."'");
			}
		}
?>
		<fieldset name="Group1" style="height: 320px">
		<legend>Registros Sin Seleccionar</legend>
			<select name="agregarven" id="agregarven" multiple="multiple" ondblclick="AGREGARPERMISOS('<?php echo $per; ?>')" style="width: 300px; height: 300px;" size="8">
			<?php
				$con = mysqli_query($conectar,"select ven_clave_int,ven_opcion from ventana where ven_clave_int not in (select ven_clave_int from permiso where prf_clave_int = '".$per."') order by ven_opcion");
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$clave = $dato['ven_clave_int'];
					$ventana = $dato['ven_opcion'];
			?>
				<option value="<?php echo $clave; ?>"><?php echo $ventana; ?></option>
			<?php
				}
			?>
			</select>
		</fieldset>
<?php
		exit();
	}
	if($_GET['agregartodos'] == 'si')
	{
		$per = $_GET['per'];
		
		$con = mysqli_query($conectar,"insert into permiso select null,'".$per."',ven_clave_int,1 from ventana where ven_clave_int not in (select ven_clave_int from permiso where prf_clave_int = '".$per."')");
		mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,1,8,'".$per."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 8=Actualización perfil
?>
		<fieldset name="Group1" style="height: 320px">
			<legend>Registros Seleccionados</legend>
				<div style="overflow:auto;height: 300px; width:300px">
					<table style="width: 100%">
					   <tr>
						   <td>&nbsp;</td>
						   <td><strong>VENTANA</strong></td>
						   <td><strong>PERMISO</strong></td>
					   </tr>
					   <?php
							$con = mysqli_query($conectar,"select p.per_clave_int cla,v.ven_opcion ven, p.per_metodo met from permiso p inner join ventana v on (v.ven_clave_int = p.ven_clave_int) where p.prf_clave_int = '".$per."'");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$claven = $dato['cla'];
								$ven = $dato['ven'];
								$met = $dato['met'];
						?>
					   <tr style="cursor:pointer" class="service_list" id="service<?php echo $claven; ?>" data="<?php echo $claven; ?>">
						   <td><input name="metodoseleccionado" id="metodoseleccionado<?php echo $claven; ?>" value="<?php echo $claven; ?>" type="checkbox" /></td>
						   <td><label for="metodoseleccionado<?php echo $claven; ?>"><?php echo $ven; ?></label></td>
						   <td>
						   <select name="metodo" id="metodo<?php echo $i; ?>" onchange="METODO('<?php echo $claven; ?>','<?php echo $i; ?>')">
								<option value="0" <?php if($met == 0){ echo "selected='selected'"; } ?>>Consulta</option>
								<option value="1" <?php if($met == 1){ echo "selected='selected'"; } ?>>Modificación</option>
							</select>
						   </td>
					   </tr>
					   <?php
							}
						?>
				   </table>
				</div>
		</fieldset>
<?php
		exit();
	}
	if($_GET['eliminartodos'] == 'si')
	{
		$per = $_GET['per'];
		$con = mysqli_query($conectar,"delete from permiso where prf_clave_int = '".$per."'");
?>
		<fieldset name="Group1" style="height: 320px">
		<legend>Registros Sin Seleccionar</legend>
			<select name="agregarven" id="agregarven" multiple="multiple" ondblclick="AGREGARPERMISOS('<?php echo $per; ?>')" style="width: 300px; height: 300px;" size="8">
			<?php
				$con = mysqli_query($conectar,"select ven_clave_int,ven_opcion from ventana where ven_clave_int not in (select ven_clave_int from permiso where prf_clave_int = '".$per."') order by ven_opcion");
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$clave = $dato['ven_clave_int'];
					$ventana = $dato['ven_opcion'];
			?>
				<option value="<?php echo $clave; ?>"><?php echo $ventana; ?></option>
			<?php
				}
			?>
			</select>
		</fieldset>
<?php
		exit();
	}
	if($_GET['cambiarmetodo'] == 'si')
	{
		$per = $_GET['per'];
		$met = $_GET['met'];
		
		$con = mysqli_query($conectar,"update permiso set per_metodo = '".$met."' where per_clave_int = '".$per."'");
		exit();
	}
?>
<!DOCTYPE HTML>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

	
<title>INTERVENTORIA</title>
<meta name="description" content="Service Desk">
<meta name="author" content="InvGate S.R.L.">

<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
<link rel="stylesheet" href="css/style.css" type="text/css" />

<script type="text/javascript" language="javascript">
	selecteds=0;
	
	function CheckUncheck(total,check){
		checkbox=null;
		for(i=1;i<=total;i++){
			checkbox=document.getElementById("idcat"+i);
			//alert(checkbox.value);
			checkbox.checked=check.checked;
		}
		
		if(check.checked){
			selecteds=total;
		}else{
			selecteds=0;
		}
		
	}
	
	function contadorVals(check){
		if(check.checked){
			selecteds=selecteds+1;
		}else{
			selecteds=selecteds-1;
		}
	}
	
	function selectedVals(){
		if(selecteds==0){
			alert("Seleccione al menos un registro.");
			return false;
		}else{
			return true;
		}
	}
</script>
<?php //VALIDACIONES ?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="llamadas2.js"></script>

<?php //VENTANA EMERGENTE ?>
<link rel="stylesheet" href="../../css/reveal.css" />
<script type="text/javascript" src="../../js/jquery.reveal.js"></script>
<script type="text/javascript" language="javascript">
	function OCULTARSCROLL()
	{
		setTimeout("parent.autoResize('iframe1')",500);
		setTimeout("parent.autoResize('iframe1')",1000);
		setTimeout("parent.autoResize('iframe1')",2000);
		setTimeout("parent.autoResize('iframe1')",3000);
		setTimeout("parent.autoResize('iframe1')",4000);
		setTimeout("parent.autoResize('iframe1')",5000);
		setTimeout("parent.autoResize('iframe1')",6000);
		setTimeout("parent.autoResize('iframe1')",7000);
		setTimeout("parent.autoResize('iframe1')",8000);
		setTimeout("parent.autoResize('iframe1')",9000);
		setTimeout("parent.autoResize('iframe1')",10000);
		setTimeout("parent.autoResize('iframe1')",11000);
		setTimeout("parent.autoResize('iframe1')",12000);
		setTimeout("parent.autoResize('iframe1')",13000);
		setTimeout("parent.autoResize('iframe1')",14000);
		setTimeout("parent.autoResize('iframe1')",15000);
		setTimeout("parent.autoResize('iframe1')",16000);
		setTimeout("parent.autoResize('iframe1')",17000);
		setTimeout("parent.autoResize('iframe1')",18000);
		setTimeout("parent.autoResize('iframe1')",19000);
		setTimeout("parent.autoResize('iframe1')",20000);
	}
	setTimeout("parent.autoResize('iframe1')",500);
	setTimeout("parent.autoResize('iframe1')",1000);
	setTimeout("parent.autoResize('iframe1')",2000);
	setTimeout("parent.autoResize('iframe1')",3000);
	setTimeout("parent.autoResize('iframe1')",4000);
	setTimeout("parent.autoResize('iframe1')",5000);
	setTimeout("parent.autoResize('iframe1')",6000);
	setTimeout("parent.autoResize('iframe1')",7000);
	setTimeout("parent.autoResize('iframe1')",8000);
	setTimeout("parent.autoResize('iframe1')",9000);
	setTimeout("parent.autoResize('iframe1')",10000);
	setTimeout("parent.autoResize('iframe1')",11000);
	setTimeout("parent.autoResize('iframe1')",12000);
	setTimeout("parent.autoResize('iframe1')",13000);
	setTimeout("parent.autoResize('iframe1')",14000);
	setTimeout("parent.autoResize('iframe1')",15000);
	setTimeout("parent.autoResize('iframe1')",16000);
	setTimeout("parent.autoResize('iframe1')",17000);
	setTimeout("parent.autoResize('iframe1')",18000);
	setTimeout("parent.autoResize('iframe1')",19000);
	setTimeout("parent.autoResize('iframe1')",20000);
</script>
</head>
<body>
<?php
$rows=mysqli_query($conectar,"select * from usuario");
$total=mysqli_num_rows($rows);
?>
<form name="form1" id="form1" action="confirmar.php" method="post" onsubmit="return selectedVals();">
<!--[if lte IE 7]>
<div class="ieWarning">Este navegador no es compatible con el sistema. Por favor, use Chrome, Safari, Firefox o Internet Explorer 8 o superior.</div>
<![endif]-->
<table style="width: 100%">
	<tr>
		<td class="auto-style2" onclick="CONSULTAMODULO('TODOS')" onmouseover="this.style.backgroundColor='#5A825A';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="width: 80px; cursor:pointer">
		Todos
		<?php
			$con = mysqli_query($conectar,"select COUNT(*) cant from usuario");
			$dato = mysqli_fetch_array($con);
			echo $dato['cant'];
		?>
		</td>
		<td class="auto-style2" onclick="CONSULTAMODULO('PERFIL')" onmouseover="this.style.backgroundColor='#5A825A';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="width: 80px; cursor:pointer">
		Perfil
		<?php
			$con = mysqli_query($conectar,"select COUNT(*) cant from perfil");
			$dato = mysqli_fetch_array($con);
			echo $dato['cant'];
		?>
		</td>
		<td class="auto-style7" style="cursor:pointer; text-align:center" colspan="4">
		MAESTRA DE USUARIOS
		<div id="prueba"></div>
		</td>
		<td class="auto-style2" onmouseover="this.style.backgroundColor='#5A825A';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="width: 55px; cursor:pointer">
		<?php 
		if($metodo == 1)
		{
		?>
		<a data-reveal-id="nuevousuario" data-animation="fade" style="cursor:pointer"><table style="width: 100%">
			<tr>
				<td style="width: 14px"><a data-reveal-id="nuevousuario" data-animation="fade" style="cursor:pointer"><img alt="" src="../../images/add2.png"></a></td>
				<td>Añadir</td>
			</tr>
		</table></a>
		<?php
		}
		?>
		</td>
	</tr>
	<tr>
		<td class="auto-style2" colspan="7">
		<div id="filtro">
			<table style="width: 50%">
				<tr>
					<td class="auto-style1"><strong>Filtro:<img src="../../images/buscar.png" alt="" height="18" width="15" /></strong></td>
					<td class="auto-style1">
			<input class="inputs" onkeyup="BUSCAR('USUARIO')" name="nombre2" maxlength="70" type="text" value="Nombre" onBlur="if(this.value=='') this.value='Nombre'" onFocus="if(this.value =='Nombre' ) this.value=''" style="width: 150px" /></td>
					<td class="auto-style1">
			<input class="inputs" onkeyup="BUSCAR('USUARIO')" name="email2" maxlength="70" type="text" value="E-mail" onBlur="if(this.value=='') this.value='E-mail'" onFocus="if(this.value =='E-mail' ) this.value=''" style="width: 150px" /></td>
					<td class="auto-style1">
			<input class="inputs" onkeyup="BUSCAR('USUARIO')" name="usuario2" maxlength="70" type="text" value="Usuario" onBlur="if(this.value=='') this.value='Usuario'" onFocus="if(this.value =='Usuario' ) this.value=''" /></td>
				</tr>
			</table>
		</div>
		</td>
	</tr>
	<tr>
		<td class="auto-style2" colspan="7">
		<div id="nuevoperfil" style="display:none">
		<?php 
		if($metodo == 1)
		{
		?>
			<table style="width: 50%">
				<tr>
					<td>
					<strong>Nuevo perfil:</strong></td>
					<td>
			<input class="inputs" name="codigoper" id="codigoper" maxlength="70" type="text" value="Código" onBlur="if(this.value=='') this.value='Código'" onFocus="if(this.value =='Código' ) this.value=''" style="width: 150px" /></td>
					<td>
			<input class="inputs" name="descripcionper" id="descripcionper" maxlength="70" type="text" value="Descripción" onBlur="if(this.value=='') this.value='Descripción'" onFocus="if(this.value =='Descripción' ) this.value=''" style="width: 150px" /></td>
					<td><a onclick="NUEVO('PERFIL')" style="cursor:pointer;background-color:#5A825A" class="auto-style26"><span class="auto-style27" style="color:white">
					<strong>GUARDAR PERFIL</strong></span></a></td>
				</tr>
			</table>
		<?php
		}
		?>
		</div>
		</td>
	</tr>
	<tr>
		<td colspan="7" class="auto-style2">
		<div id="editarusuario" class="reveal-modal" style="left: 57%; top: 20px; height: 365px; width: 450px">
			<table style="width: 38%" align="center">
				<tr>
					<td>&nbsp;</td>
					<td class="auto-style3">Nombre:</td>
					<td class="auto-style3"><input class="inputs" name="nombre1" maxlength="50" value="<?php echo $nom; ?>" type="text" style="width: 200px" />
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="auto-style3">Usuario:</td>
					<td class="auto-style3"><input class="inputs" name="usuario1" maxlength="15" type="text" value="<?php echo $usu; ?>" style="width: 200px" />
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="auto-style3">Contraseña:</td>
					<td class="auto-style3"><input class="inputs" name="Password3" maxlength="15" type="password" value="<?php echo $con; ?>" style="width: 200px" />
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="auto-style3">Repetir Contraseña:</td>
					<td class="auto-style3">
					<input class="inputs" name="Password4" maxlength="15" onkeyup="VALIDAR('CONTRASENA')" type="password" value="<?php echo $con; ?>" style="width: 200px" /></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="auto-style3">Perfil:</td>
					<td class="auto-style3">
						<select class="inputs" name="perfil1" style="width: 200px">
						<?php
							$con = mysqli_query($conectar,"select * from perfil order by prf_descripcion");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$clave = $dato['prf_clave_int'];
								$perfil = $dato['prf_descripcion'];
						?>
							<option value="<?php echo $clave; ?>" <?php $con1 = mysqli_query($conectar,"select prf_clave_int from usuario where usu_clave_int = '".$usuedi."'"); $dato = mysqli_fetch_array($con1); $p = $dato['prf_clave_int']; if($p == $clave){  echo "selected='selected'"; } ?>><?php echo $perfil; ?></option>
						<?php
							}
						?>
						</select>
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="auto-style3">E-mail:</td>
					<td class="auto-style3"><input class="inputs" name="email1" maxlength="50" type="text" value="<?php echo $ema; ?>" style="width: 200px" /></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="auto-style3">empresa:</td>
					<td class="auto-style3">
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="auto-style3">Activo:</td>
					<td class="auto-style3"><input class="inputs" <?php if($act == 1){ echo 'checked="checked"'; } ?> name="activo1" type="checkbox" /></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="4">
					<input name="submit" type="button" value="Guardar" onclick="GUARDAR('USUARIO','<?php echo $usuedi; ?>')"  style="width: 348px; height: 25px; cursor:pointer" /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td colspan="2">
					<div id="datos">
					</div>
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</div>
		<div id="usuarios">
		<table style="width: 100%;border-collapse:collapse">
			<tr>
				<td class="auto-style3" style="width: 27px">
					<input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<?php echo $total;?>,this);" />
				</td>
				<td class="auto-style3" colspan="13">
				<?php 
				if($metodo == 1)
				{
				?>
				<table style="width: 30%">
					<tr>
						<td class="auto-style1"><p style="cursor:pointer"><img src="../../images/activo.png" alt="" /><input type="submit" value="Activar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" /></p></td>
						<td class="auto-style1"><p style="cursor:pointer"><img src="../../images/inactivo.png" alt="" /><input type="submit" value="Inactivar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" /></p></td>
						<td class="auto-style1"><p style="cursor:pointer"><img src="../../images/eliminar.png" alt="" /><input type="submit" value="Eliminar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" /></p></td>
					</tr>
				</table>
				<?php
				}
				?>
				</td>
			</tr>
			<tr>
				<td class="auto-style3" style="width: 27px">&nbsp;</td>
				<td class="auto-style3" style="width: 180px"><strong>Nombre</strong></td>
				<td class="auto-style3" style="width: 120px"><strong>Cédula</strong></td>
				<td class="auto-style3" style="width: 120px"><strong>Usuario</strong></td>
				<td class="auto-style3" style="width: 110px"><strong>Perfil</strong></td>
				<td class="auto-style3" style="width: 200px"><strong>E-mail</strong></td>
				<td class="auto-style3" style="width: 130px"><strong>Rut</strong></td>
				<td class="auto-style3" style="width: 130px"><strong>Salario Neto</strong></td>
				<td class="auto-style3" style="width: 130px"><strong>Valor Hora</strong></td>
				<td class="auto-style3" style="width: 130px"><strong>Fact. Prestacional</strong></td>
				<td class="auto-style3" style="width: 130px"><strong>Creado Por</strong></td>
				<td class="auto-style3" style="width: 98px"><strong>
				Actualización</strong></td>
				<td class="auto-style3" style="width: 65px"><strong>
				Activo</strong></td>
				<td class="auto-style3">&nbsp;</td>
			</tr>
			<tr>
				<td class="auto-style3" colspan="14"><hr></td>
			</tr>
			<?php
				$contador=0;
				$con = mysqli_query($conectar,"select * from usuario u inner join perfil prf ON (prf.prf_clave_int = u.prf_clave_int) left outer join factor_prestacional f on (f.fac_clave_int = u.fac_clave_int) order by u.usu_nombre");
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$clausu = $dato['usu_clave_int'];
					$nom = $dato['usu_nombre'];
					$ced = $dato['usu_cedula'];
					$usu = $dato['usu_usuario'];
					$pernom = $dato['prf_descripcion'];
					$act = $dato['usu_sw_activo'];
					$ema = $dato['usu_email'];
					$rut = $dato['usu_rut'];
					$sal = $dato['usu_salario'];
					$valhor = $dato['usu_vr_hora'];
					$fact = $dato['fac_tarifa'];
					if($fact == '' or $fact == null){ $fact = 0; }
					$epr = $dato['epr_nombre'];
					$usuact = $dato['usu_usu_actualiz'];
					$fecact = $dato['usu_fec_actualiz'];
					$contador=$contador+1;
			?>
			<tr style="<?php if($i % 2 == 0){ echo 'background-color:#B8CEB8'; }else{ echo 'background-color:#ffffff'; } ?>">
				<td class="auto-style3" style="width: 27px">
					<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<?php echo $contador;?>" value="<?php echo $dato['usu_clave_int'];?>" /></td>
				<td class="auto-style3" style="width: 180px"><?php echo $nom; ?></td>
				<td class="auto-style3" style="width: 120px"><?php echo $ced; ?></td>
				<td class="auto-style3" style="width: 120px"><?php echo $usu; ?></td>
				<td class="auto-style3" style="width: 110px"><?php echo $pernom; ?></td>
				<td class="auto-style3" style="width: 200px"><?php echo $ema; ?></td>
				<td class="auto-style3" style="width: 130px"><?php echo $rut; ?></td>
				<td class="auto-style3" style="width: 130px"><?php echo "$".number_format($sal,0 , "," ,"."); ?></td>
				<td class="auto-style3" style="width: 130px"><?php echo "$".number_format($valhor,0 , "," ,"."); ?></td>
				<td class="auto-style3" style="width: 130px"><?php echo $fact; ?></td>
				<td class="auto-style3" style="width: 130px"><?php echo $usuact; ?></td>
				<td class="auto-style3" style="width: 98px"><?php echo $fecact; ?></td>
				<td class="auto-style3" style="width: 65px">
				<input name="activarinactivar" id="activarinactivar" type="checkbox" <?php if($act == 1){ echo 'checked="checked"'; } ?> disabled="disabled" ></td>
				<td class="auto-style3">
				<?php 
				if($metodo == 1)
				{
				?>
				<a data-reveal-id="editarusuario" data-animation="fade" style="cursor:pointer" onclick="EDITAR('<?php echo $dato['usu_clave_int']; ?>','USUARIO')">
				<img src="../../images/editar.png" alt="" height="22" width="21" />
				</a>
				<?php
				}
				?>
				</td>
				<?php
					}
				?>
			</tr>
			<tr>
				<td class="auto-style3" style="width: 27px">&nbsp;</td>
				<td class="auto-style3" style="width: 180px">&nbsp;</td>
				<td class="auto-style3" style="width: 120px">&nbsp;</td>
				<td class="auto-style3" style="width: 120px">&nbsp;</td>
				<td class="auto-style3" style="width: 110px">&nbsp;</td>
				<td class="auto-style3" style="width: 200px">&nbsp;</td>
				<td class="auto-style3" style="width: 130px">&nbsp;</td>
				<td class="auto-style3" style="width: 130px">&nbsp;</td>
				<td class="auto-style3" style="width: 130px">&nbsp;</td>
				<td class="auto-style3" style="width: 130px">&nbsp;</td>
				<td class="auto-style3" style="width: 130px">&nbsp;</td>
				<td class="auto-style3" style="width: 200px">&nbsp;</td>
				<td class="auto-style3" style="width: 98px">&nbsp;</td>
				<td class="auto-style3" style="width: 98px">&nbsp;</td>
			</tr>
		</table>
		</div>
<div id="nuevousuario" class="reveal-modal" style="left: 57%; top: 20px; height: 365px; width: 450px;">
<table style="width: 38%" align="center">
	<tr>
		<td>&nbsp;</td>
		<td class="auto-style3">Nombre:</td>
		<td><input class="inputs" name="nombre" maxlength="50" type="text" style="width: 250px" />
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td class="auto-style3">Cédula:</td>
		<td>
		<input class="inputs" name="cedula" id="cedula" maxlength="50" type="text" style="width: 250px" />
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td class="auto-style3">Usuario:</td>
		<td><input class="inputs" name="usuario" maxlength="15" type="text" style="width: 250px" />
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td class="auto-style3">Contraseña:</td>
		<td><input class="inputs" name="Password1" maxlength="15" type="password" style="width: 250px" />
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td class="auto-style3">Repetir Contraseña:</td>
		<td>
		<input class="inputs" name="Password2" maxlength="15" onkeyup="VALIDAR('CONTRASENA')" type="password" style="width: 250px" /></td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td class="auto-style3">Perfil:</td>
		<td>
			<select class="inputs" name="perfil" style="width: 260px">
			<option value="">-Seleccione-</option>
			<?php
				$con = mysqli_query($conectar,"select * from perfil order by prf_descripcion");
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$clave = $dato['prf_clave_int'];
					$perfil = $dato['prf_descripcion'];
			?>
				<option value="<?php echo $clave; ?>"><?php echo $perfil; ?></option>
			<?php
				}
			?>
			</select>
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td class="auto-style3">E-mail:</td>
		<td><input class="inputs" name="email" maxlength="50" type="text" style="width: 250px" /></td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td class="auto-style3">Rut:</td>
		<td><input class="inputs" name="rut" id="rut" maxlength="20" type="text" style="width: 250px" /></td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td class="auto-style3">Salario Neto:</td>
		<td>
			<input class="inputs" name="salario" id="salario" type="text" style="width: 250px" /></td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td class="auto-style3">Valor Hora:</td>
		<td><input class="inputs" name="valorhora" id="valorhora" type="text" style="width: 250px" /></td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td class="auto-style3">
		Fact. Prestacional:
		</td>
		<td>
		<select class="inputs" name="factor" id="factor" style="width: 260px">
		<option value="">-Seleccione-</option>
		<?php
			$con = mysqli_query($conectar,"select * from factor_prestacional order by fac_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clave = $dato['fac_clave_int'];
				$nom = $dato['fac_nombre'];
				$tar = $dato['fac_tarifa'];
		?>
			<option value="<?php echo $clave; ?>"><?php echo $nom." / ".$tar; ?></option>
		<?php
			}
		?>
		</select>
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td class="auto-style3">Activo:</td>
		<td class="auto-style3"><input class="inputs" name="activo" checked="checked" type="checkbox" /></td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="4">
		<input name="nuevo1" type="button" value="Guardar" onclick="NUEVO('USUARIO')"  style="width: 430px; height: 25px; cursor:pointer" /></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2">
		<div id="datos1">
				</div>
		</td>
		<td>&nbsp;</td>
	</tr>
	</table>
</div>
		</td>
	</tr>
	<tr>
		<td style="width: 80px">&nbsp;</td>
		<td style="width: 80px">&nbsp;</td>
		<td style="width: 80px">&nbsp;</td>
		<td style="width: 80px">&nbsp;</td>
		<td style="width: 80px">&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td style="width: 80px">&nbsp;</td>
		<td style="width: 80px">&nbsp;</td>
		<td style="width: 80px">&nbsp;</td>
		<td style="width: 80px">&nbsp;</td>
		<td style="width: 80px">&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>
<div id="permisos" class="reveal-modal" style="left: 36%; top: 50px; height: 345px; width: 830px;">
	<table style="width: 38%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td align="center">
			<div id="agregar" style="height: 180px" align="center">
				<fieldset name="Group1">
				<legend>Registros Sin Seleccionar</legend>
					<select class="auto-style8" <?php if($prf <> 'Cliente'){ echo "disabled='disabled'"; } ?> ondblclick="AGREGAR('EQUIPO','<?php echo $usuedi; ?>');CONSULTAMODULO('CLIENTES')" name="agregarequ" style="width: 150px; height: 150px;" size="8">
					<?php
						$con = mysqli_query($conectar,"select * from equipo where equ_clave_int NOT IN (select equ_clave_int from equipo_usuarios) order by equ_nombre");
						$num = mysqli_num_rows($con);
						for($i = 0; $i < $num; $i++)
						{
							$dato = mysqli_fetch_array($con);
							$clave = $dato['equ_clave_int'];
							$equipo = $dato['equ_nombre'];
					?>
						<option value="<?php echo $clave; ?>"><?php echo $equipo; ?></option>
					<?php
						}
					?>
					</select>
				</fieldset>
			</div>
			</td>
			<td align="center">
			<img src="../../agregar.png" height="30" width="40" style="cursor:pointer" />
			<img src="../../quitar.png" height="30" width="40" style="cursor:pointer" />
			</td>
			<td align="center">
			<div id="quitar" style="height: 180px">
				<fieldset name="Group1">
				<legend>Registros Seleccionados</legend>
					<select class="auto-style8" <?php if($prf <> 'Cliente'){ echo "disabled='disabled'"; } ?> ondblclick="QUITAR('EQUIPO','<?php echo $usuedi; ?>');CONSULTAMODULO('CLIENTES')" name="quitarequ" style="width: 150px; height: 150px;" size="3">
					<?php
						$con = mysqli_query($conectar,"select * from equipo_usuarios eu inner join equipo e on (e.equ_clave_int = eu.equ_clave_int) where eu.usu_clave_int = '".$usuedi."' order by e.equ_nombre");
						$num = mysqli_num_rows($con);
						for($i = 0; $i < $num; $i++)
						{
							$dato = mysqli_fetch_array($con);
							$clave = $dato['eus_clave_int'];
							$equipo = $dato['equ_nombre'];
					?>
						<option value="<?php echo $clave; ?>"><?php echo $equipo; ?></option>
					<?php
						}
					?>
					</select>
				</fieldset>
			</div>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5">
			<input name="submit" type="button" value="Guardar" onclick="GUARDAR('USUARIO','<?php echo $usuedi; ?>')"  style="width: 348px; height: 25px; cursor:pointer" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="3">
			<div id="datos">
			</div>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
</div>
</form>
</body>
</html>