function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function MODULO(v)
{	
	if(v == 'USUARIOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulousuarios=si",true);
		ajax.send(null);
	}
}
function EDITAR(v,m)
{	
	if(m == 'USUARIO')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('editarusuario').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#editarusuario").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?editarusu=si&usuedi="+v,true);
		ajax.send(null);
	}
	else
	if(m == 'PERFIL')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('nuevoperfil').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#nuevoperfil").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?editarper=si&peredi="+v,true);
		ajax.send(null);
	}
}
function VALIDAR(v)
{	
	if(v == 'CONTRASENA')
	{
		var con1 = form.Password1.value;
		var con2 = form.Password2.value;
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('datos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#datos").html("<img alt='cargando' src='images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?contrasena=si&con1="+con1+"&con2="+con2,true);
		ajax.send(null);
	}
}
function GUARDAR(v,id)
{	
	if(v == 'USUARIO')
	{
		var nom = form1.nombre1.value;
		var ced = form1.cedula1.value;
		var usu = form1.usuario1.value;
		var lu = usu.length;
		var con1 = form1.Password3.value;
		var lc = con1.length
		var con2 = form1.Password4.value;
		var per = form1.perfil1.value;
		var ema = form1.email1.value;
		var sal = form1.salario1.value;
		var valhor = form1.valorhora1.value;
		var fact = form1.factor1.value;
		var rut = form1.rut1.value;
		var act = form1.activo1.checked;

		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('datos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#datos").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?guardarusu=si&nom="+nom+"&ced="+ced+"&usu="+usu+"&con1="+con1+"&con2="+con2+"&per="+per+"&ema="+ema+"&act="+act+"&lu="+lu+"&lc="+lc+"&sal="+sal+"&valhor="+valhor+"&fact="+fact+"&rut="+rut+"&u="+id,true);
		ajax.send(null);
		if(per == 3)
		{
			if(nom != '' && usu != '' && lu >= 6 && con1 == con2)
			{
				setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
			}
		}
		else
		if(nom != '' && usu != '' && lu >= 6 && con1 == con2)
		{
			setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
		}
	}
	else
	if(v == 'PERFIL')
	{
		var cod = form1.codigoper1.value;
		var des = form1.descripcionper1.value;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('usuarios').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#usuarios").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?guardarper=si&cod="+cod+"&des="+des+"&p="+id,true);
		ajax.send(null);
	}
}
function NUEVO(v)
{
	if(v == 'USUARIO')
	{
		var nom = form1.nombre.value;
		var ced = form1.cedula.value;
		var usu = form1.usuario.value;
		var lu = usu.length;
		var con1 = form1.Password1.value;
		var lc = con1.length
		var con2 = form1.Password2.value;
		var per = form1.perfil.value;
		var ema = form1.email.value;
		var rut = form1.rut.value;
		var sal = form1.salario.value;
		var valhor = form1.valorhora.value;
		var fact = form1.factor.value;
		var act = form1.activo.checked;
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('datos1').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#datos1").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?nuevousuario=si&nom="+nom+"&ced="+ced+"&usu="+usu+"&con1="+con1+"&con2="+con2+"&per="+per+"&ema="+ema+"&act="+act+"&sal="+sal+"&valhor="+valhor+"&fact="+fact+"&rut="+rut+"&lu="+lu+"&lc="+lc,true);
		ajax.send(null);
		if(per == 3)
		{
			if(nom != '' && usu != '' && lu >= 6 && con1 == con2)
			{
				form1.nombre.value = '';
				form1.cedula.value = '';
				form1.usuario.value = '';
				form1.Password1.value = '';
				form1.Password2.value = '';
				form1.perfil.value = '';
				form1.email.value = '';
				form1.rut.value = '';
				form1.salario.value = '';
				form1.valorhora.value = '';
				form1.factor.value = '';
				setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
				/*setTimeout("CONSULTAMODULO('ADMINISTRADORES');",1000);
				setTimeout("CONSULTAMODULO('DIRECTORES');",1000);
				setTimeout("CONSULTAMODULO('CONTRATISTAS');",1000);*/
			}
		}
		else
		if(nom != '' && usu != '' && lu >= 6 && con1 == con2)
		{
			form1.nombre.value = '';
			form1.cedula.value = '';
			form1.usuario.value = '';
			form1.Password1.value = '';
			form1.Password2.value = '';
			form1.perfil.value = '';
			form1.email.value = '';
			form1.rut.value = '';
			form1.salario.value = '';
			form1.valorhora.value = '';
			setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
			/*setTimeout("CONSULTAMODULO('ADMINISTRADORES');",1000);
			setTimeout("CONSULTAMODULO('DIRECTORES');",1000);
			setTimeout("CONSULTAMODULO('CONTRATISTAS');",1000);*/
		}
	}
	else
	if(v == 'PERFIL')
	{
		var cod = form1.codigoper.value;
		var des = form1.descripcionper.value;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('usuarios').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#usuarios").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?perfil=si&cod="+cod+"&des="+des+"&nuevo=si",true);
		ajax.send(null);
	}
	else
	if(v == 'PERFIL1')
	{
		var cod = form1.codigoper1.value;
		var des = form1.descripcionper1.value;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('usuarios').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#usuarios").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?perfil=si&cod="+cod+"&des="+des+"&nuevo=si",true);
		ajax.send(null);
	}
}
function CONSULTAMODULO(v)
{
	if(v == 'TODOS')
	{
		var div = document.getElementById('nuevoperfil');
    	div.style.display = 'none';
    	
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('usuarios').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#usuarios").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?todos=si",true);
		ajax.send(null);
		CAMBIOFILTRO('USUARIO');
	}
	if(v == 'PERFIL')
	{
		var div = document.getElementById('nuevoperfil');
    	div.style.display = 'block';
    	
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('usuarios').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#usuarios").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?perfil=si&nuevo=no",true);
		ajax.send(null);
		CAMBIOFILTRO('PERFIL');
	}
}
function CAMBIOFILTRO(v)
{
	if(v == 'PERFIL')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('filtro').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#filtro").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?filtroperfil=si",true);
		ajax.send(null);
	}
	else
	if(v == 'USUARIO')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('filtro').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#filtro").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?filtrousuario=si",true);
		ajax.send(null);
	}
}
function BUSCAR(m)
{	
	if(m == 'USUARIO')
	{
		var nom = form1.nombre2.value;
		var ema = form1.email2.value;
		var usu = form1.usuario2.value;
				
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('usuarios').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#usuarios").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?buscarusu=si&nom="+nom+"&ema="+ema+"&usu="+usu,true);
		ajax.send(null);
	}
	else
	if(m == 'PERFIL')
	{
		var cod = form1.buscodigoper.value;
		var des = form1.busdescripcionper.value;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('usuarios').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#usuarios").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?buscarper=si&cod="+cod+"&des="+des,true);
		ajax.send(null);
	}
}
function BUSCAREQU(m,u)
{	
	if(m == 'EQUIPO')
	{
		var equ = form1.equipo2.value;
		var cod = form1.codigo2.value;
		var codalt = form1.codigoalterno2.value;
		var tip = form1.tipoequipo.value;
				
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('usuarios').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#usuarios").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?buscarequ=si&equ="+equ+"&cod="+cod+"&codalt="+codalt+"&tip="+tip+"&usu="+u,true);
		ajax.send(null);
	}
}
function PERMISOS(v)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('permisos').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#permisos").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?agregar=si&per="+v,true);
	ajax.send(null);
}
function AGREGARPERMISOS(v)
{
	var ven = $("#agregarven").val();
	
	if(ven != '')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('agregados').innerHTML=ajax.responseText;
		    }
		}
		
		jQuery("#agregados").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?agregarseleccionados=si&ven="+ven+"&per="+v,true);
		ajax.send(null);
		
		for (x=0;x<ven.length;x++)
		{
			$("#agregarven").find("option[value="+ven[x]+"]").remove();
		}
	}
	else
	{
		alert("Por favor seleccione almenos un registros.");
	}
}
function ELIMINARPERMISO(v)
{	
	var strChoices = "";
	var strChoices1 = "";
	var objCBarray = document.getElementsByName('metodoseleccionado');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	strChoices += objCBarray[i].value + ",";
	    	strChoices1 = objCBarray[i].value;
	        $('#service'+strChoices1).fadeOut("slow");//Esta linea me permite Eliminar las filas seleccionadas
	    }
	}
	
	if (strChoices.length <= 0) 
	{
		alert("Por favor seleccione almenos un registros.");
	}
	else
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('agregar1').innerHTML=ajax.responseText;
		    }
		}
		
		//jQuery("#agregados").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?eliminaragregados=si&permiso="+strChoices+"&per="+v,true);
		ajax.send(null);
	}
}
function AGREGARTODOS(v)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('agregados').innerHTML=ajax.responseText;
	    }
	}
	
	jQuery("#agregados").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?agregartodos=si&per="+v,true);
	ajax.send(null);
	$('#agregarven').html('');//Limpia todos los datos del select
}
function QUITARTODOS(v)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('agregar1').innerHTML=ajax.responseText;
	    }
	}
	
	jQuery("#agregar1").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?eliminartodos=si&per="+v,true);
	ajax.send(null);
	$('.service_list').fadeOut("slow");//Esta linea me permite Eliminar las filas seleccionadas
}
function METODO(v,i)
{
	var met = $('#metodo'+i).val();

	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('prueba').innerHTML=ajax.responseText;
	    }
	}
	//jQuery("#agregar").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?cambiarmetodo=si&per="+v+"&met="+met,true);
	ajax.send(null);
}
function AGREGAR(u)
{
	var obr = $("#agregarobr").val();
	
	if(obr != '')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('agregados').innerHTML=ajax.responseText;
		    }
		}
		
		jQuery("#agregados").html("<img alt='cargando' src='../../images/cargando.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?agregarobra=si&obr="+obr+"&usu="+u,true);
		ajax.send(null);
		
		for (x=0;x<obr.length;x++)
		{
			$("#agregarobr").find("option[value="+obr[x]+"]").remove();
		}
	}
	else
	{
		alert("Por favor seleccione almenos un registros.");
	}
}
function QUITAR(u)
{
	var obr = $("#quitarobr").val();
	
	if(obr != '')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('agregar').innerHTML=ajax.responseText;
		    }
		}
		
		jQuery("#agregar").html("<img alt='cargando' src='../../images/cargando.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?quitarobra=si&obr="+obr+"&usu="+u,true);
		ajax.send(null);
		
		for (x=0;x<obr.length;x++)
		{
			$("#quitarobr").find("option[value="+obr[x]+"]").remove();
		}
	}
	else
	{
		alert("Por favor seleccione almenos un registros.");
	}
}
function PONERTODOS(u)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('agregados').innerHTML=ajax.responseText;
	    }
	}
	
	jQuery("#agregados").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?ponertodos=si&usu="+u,true);
	ajax.send(null);
	$('#agregarobr').html('');//Limpia todos los datos del select
}
function REMOVERTODOS(u)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('agregar').innerHTML=ajax.responseText;
	    }
	}
	
	jQuery("#agregar").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?quitartodos=si&usu="+u,true);
	ajax.send(null);
	$('#quitarobr').html('');//Limpia todos los datos del select
}