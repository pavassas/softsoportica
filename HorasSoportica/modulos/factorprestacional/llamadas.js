function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function EDITAR(v,m)
{	
	if(m == 'AREA')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('editararea').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#editararea").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?editarare=si&areedi="+v,true);
		ajax.send(null);
	}
}
function GUARDAR(id)
{
	var fac = form1.factor1.value;
	var lf = fac.length;
	var tar = form1.tarifa1.value;
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('datos').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#datos").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?guardarfac=si&fac="+fac+"&lf="+lf+"&tar="+tar+"&f="+id,true);
	ajax.send(null);
	if(fac != '' && lf >= 3 && tar != '')
	{
		setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
	}
}
function NUEVO()
{
	var fac = form1.factor.value;
	var lf = fac.length;
	var tar = form1.tarifa.value;
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('datos1').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#datos1").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?nuevaarea=si&fac="+fac+"&lf="+lf+"&tar="+tar,true);
	ajax.send(null);
	if(fac != '' && lf >= 3 && tar != '')
	{
		form1.factor.value = '';
		form1.tarifa.value = '';
		setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
	}
}
function CONSULTAMODULO(v)
{
	if(v == 'TODOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('areas').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#areas").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?todos=si",true);
		ajax.send(null);
	}
}
function BUSCAR()
{	
	var fac = form1.busfactor.value;
	var tar = form1.bustarifa.value;
			
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('areas').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#areas").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?buscarfac=si&fac="+fac+"&tar="+tar,true);
	ajax.send(null);
}