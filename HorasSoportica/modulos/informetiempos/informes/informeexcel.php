<?php
error_reporting(0);
include('../../../data/Conexion.php');
require_once('../../../Classes/PHPExcel.php');
date_default_timezone_set('America/Bogota');
session_start();
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_SESSION['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];
	
// verifica si no se ha loggeado
if(!isset($_SESSION["persona"]))
{
  session_destroy();
  header("LOCATION:index.php");
}else{
}

$emp = $_GET['emp'];
$cli = $_GET['cli'];
$pro = $_GET['pro'];
$act = $_GET['act'];
$fi = $_GET['fi'];
$ff = $_GET['ff'];
$ti = $_GET['ti'];

$seleccionados = explode(',',$emp);
$num = count($seleccionados);
$empleados = array();
for($i = 0; $i < $num; $i++)
{
	if($seleccionados[$i] != '')
	{
		$empleados[$i]=$seleccionados[$i];
	}
}
$listaemplados=implode(',',$empleados);

$seleccionados = explode(',',$cli);
$num = count($seleccionados);
$clientes = array();
for($i = 0; $i < $num; $i++)
{
	if($seleccionados[$i] != '')
	{
		$clientes[$i]=$seleccionados[$i];
	}
}
$listaclientes=implode(',',$clientes);

$seleccionados = explode(',',$pro);
$num = count($seleccionados);
$proyectos = array();
for($i = 0; $i < $num; $i++)
{
	if($seleccionados[$i] != '')
	{
		$proyectos[$i]=$seleccionados[$i];
	}
}
$listaproyectos=implode(',',$proyectos);
		
$fecha=date("d/m/Y");
$fechaact=date("Y/m/d H:i:s");
//mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,obr_clave_int,loa_usu_actualiz,loa_fec_actualiz) values(null,27,71,'".$ultimaobra."','".$usuario."','".$fechaact."')");//Tercer campo tia_clave_int. 71=Archivo Impreso
//************ESTILOS******************

$styleA1 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => '000000'),
    'size'  => 12,
    'name'  => 'Calibri'
));
$styleA2 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => 'C83000'),
    'size'  => 10,
    'name'  => 'Arial'
));
$styleA3 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => '000000'),
    'size'  => 10,
    'name'  => 'Arial'
));
$styleA4 = array(
'font'  => array(
    'bold'  => false,
    'color' => array('rgb' => '000000'),
    'size'  => 10,
    'name'  => 'Arial'
));
$styleA3p1 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => '000000'),
    'size'  => 9,
    'name'  => 'Arial'
));
$styleA4p1 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => '000000'),
    'size'  => 9,
    'name'  => 'Arial'
));

$borders = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '000000'),
		)
	),
);

//*************************************

$objPHPExcel = new PHPExcel();
$archivo = 'PAVAS - Informe tiempos.xls';

//Propiedades de la hoja de excel
$objPHPExcel->getProperties()
		->setCreator("PAVAS TECNOLOGIA")
		->setLastModifiedBy("PAVAS TECNOLOGIA")
		->setTitle("Informe tiempos")
		->setSubject("Informe tiempos")
		->setDescription("Documento generado con el software Control de tiempos")
		->setKeywords("Control de tiempos")
		->setCategory("Reportes");
		
//Ancho de las Columnas
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(25);

//************A1**************
$objPHPExcel->getActiveSheet()->getStyle('A1')-> applyFromArray($styleA1);//
$objPHPExcel->getActiveSheet()->getCell('A1')->setValue("INFORME DE TIEMPOS POR USUARIO");
$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setWrapText(true); //Crea un enter entre palabras
$objPHPExcel->getActiveSheet()->mergeCells('A1:K1');//Conbinar celdas
$objPHPExcel->getActiveSheet()->getStyle('A1:K1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('00D8D8D8');//COLOR DE FONDO
//****************************

/**************** COLUMNAS ****************/

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('A2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('A2')->setValue("USUARIO");
$objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('B2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('B2')->setValue("REGIÓN");
$objPHPExcel->getActiveSheet()->getStyle('B2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//
//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('C2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('C2')->setValue("COD.CONTABLE");
$objPHPExcel->getActiveSheet()->getStyle('C2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//
//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('D2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('D2')->setValue("PROYECTO");
$objPHPExcel->getActiveSheet()->getStyle('D2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('E2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('E2')->setValue("FECHA");
$objPHPExcel->getActiveSheet()->getStyle('E2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('F2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('F2')->setValue("ACTIVIDAD");
$objPHPExcel->getActiveSheet()->getStyle('F2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('G2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('G2')->setValue("CLIENTE");
$objPHPExcel->getActiveSheet()->getStyle('G2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('H2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('H2')->setValue("HORA INICIAL");
$objPHPExcel->getActiveSheet()->getStyle('H2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('I2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('I2')->setValue("HORA FINAL");
$objPHPExcel->getActiveSheet()->getStyle('I2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('J2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('J2')->setValue("VR. HORA");
$objPHPExcel->getActiveSheet()->getStyle('J2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('K2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('K2')->setValue("FACTOR PRESTACIONAL");
$objPHPExcel->getActiveSheet()->getStyle('K2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

$sql = '';
if($sql == ''){ if($listaproyectos <> ''){ $sql = 'p.pro_clave_int in ('.$listaproyectos.')'; } }else{ if($listaproyectos <> ''){ $sql .= ' and p.pro_clave_int in ('.$listaproyectos.')'; } }
if($sql == ''){ if($listaclientes <> ''){ $sql = 'hd.usu_clave_int in ('.$listaclientes.')'; } }else{ if($listaclientes <> ''){ $sql .= ' and hd.usu_clave_int in ('.$listaclientes.')'; } }
if($sql == ''){ if($listaemplados <> ''){ $sql = 'hh.usu_clave_int in ('.$listaemplados.')'; } }else{ if($listaemplados <> ''){ $sql .= ' and hh.usu_clave_int in ('.$listaemplados.')'; } }
if($sql == ''){ $sql = "(hd.hod_actividad LIKE REPLACE('%".$act."%',' ','%') OR '".$act."' IS NULL OR '".$act."' = '')"; }else{ $sql .= " and (hd.hod_actividad LIKE REPLACE('%".$act."%',' ','%') OR '".$act."' IS NULL OR '".$act."' = '')"; }
if($sql == ''){ $sql = "((hh.hoh_fecha = '".$fi."' OR '".$fi."' IS NULL OR '".$fi."' = '') or ((hh.hoh_fecha BETWEEN '".$fi." 00:00:00' AND '".$ff." 23:59:59') or ('".$fi."' Is Null and '".$ff."' Is Null) or ('".$fi."' = '' and '".$ff."' = '')))"; }else{ $sql .= " and ((hh.hoh_fecha = '".$fi."' OR '".$fi."' IS NULL OR '".$fi."' = '') or ((hh.hoh_fecha BETWEEN '".$fi." 00:00:00' AND '".$ff." 23:59:59') or ('".$fi."' Is Null and '".$ff."' Is Null) or ('".$fi."' = '' and '".$ff."' = '')))"; }

$con = mysqli_query($conectar,"select u.usu_nombre usuario,p.pro_nombre,hh.hoh_fecha,hd.hod_actividad,uc.usu_nombre cliente,substring(hd.hod_fila,1,1) hi,substring(hd.hod_fila,2,1) hf,u.usu_vr_hora,f.fac_tarifa,r.reg_nombre,p.pro_codigo_contable from horas_h hh inner join horas_d hd on (hd.hoh_clave_int = hh.hoh_clave_int) inner join usuario u on (u.usu_clave_int = hh.usu_clave_int) inner join usuario uc on (uc.usu_clave_int = hd.usu_clave_int) inner join proyecto p on (p.pro_clave_int = hd.pro_clave_int) left outer join factor_prestacional f on (f.fac_clave_int = u.fac_clave_int) left outer join region r on r.reg_clave_int = p.reg_clave_int where ".$sql."");
$num = mysqli_num_rows($con);

$j = 3;
for($i = 0; $i < $num; $i++)
{
	$dato = mysqli_fetch_array($con);
	$usu = $dato['usuario'];
	$pro = $dato['pro_nombre'];
	$fec = $dato['hoh_fecha'];
	$act = $dato['hod_actividad'];
	$cli = $dato['cliente'];
	$hi = $dato['hi'];
	$hf = $dato['hf'];
	$vrhor = $dato['usu_vr_hora'];
	$fact = $dato['fac_tarifa'];
	$reg = $dato['reg_nombre']; if($reg=="" || $reg==NULL){ $reg="No tiene región"; }
	$codcon = $dato['pro_codigo_contable'];
	
	/**************** DATOS DE LAS COLUMNAS ****************/ 

	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('A'.$j)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("A".$j, $usu);
	//****************************
	$objPHPExcel->getActiveSheet()->getStyle('B'.$j)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("B".$j, $reg);

	$objPHPExcel->getActiveSheet()->getStyle('C'.$j)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("C".$j, $codcon);
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('D'.$j)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("D".$j, $pro);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('E'.$j)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("E".$j, $fec);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('F'.$j)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("F".$j, $act);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('G'.$j)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("G".$j, $cli);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('H'.$j)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("H".$j, $hi);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('I'.$j)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("I".$j, $hf);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('J'.$j)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("J".$j, $vrhor);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('K'.$j)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("K".$j, $fact);
	//****************************
	
	$j++;
}
//DATOS DE SALIDA DEL EXCEL
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$archivo.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
?>