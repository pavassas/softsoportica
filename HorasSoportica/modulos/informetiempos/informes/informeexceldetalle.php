<?php
session_start();
error_reporting(0);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
ini_set('memory_limit','500M');
ini_set('max_execution_time', 600);
ini_set('upload_max_filesize', '300M');

include('../../../data/Conexion.php');


define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
date_default_timezone_set("America/Bogota");
setlocale (LC_TIME,"spanish", "es_ES@euro", "es_ES", "es");

require_once '../../../Classes/PHPExcel.php';
$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_sqlite;
if (PHPExcel_Settings::setCacheStorageMethod($cacheMethod)) {
    // echo date('H:i:s') , " Habilitar el almacenamiento en caché de la celda utilizando " , $cacheMethod , " metodo" , EOL;
} else {
    //echo date('H:i:s') , " No se puede establecer el almacenamiento en caché de la celda utilizando " , $cacheMethod , " método, volviendo a la memoria" , EOL;
}


PHPExcel_Cell::setValueBinder( new PHPExcel_Cell_AdvancedValueBinder() );

function convert_htmlentities($data)
{
    //$result = str_replace(
    //array("&aacute;","&eacute;","&iacute;","&oacute;","&uacute;","&ntilde;",
    //"&Aacute;","&Eacute;","&Iacute;","&Oacute;","&Uacute;","&Ntilde;"),array("á","é","í","ó","ú","ñ","Á","É","Í","Ó","Ú","Ñ") ,$data);
    $result = str_replace("á",htmlentities("á"),$data);
    $result = str_replace("é",htmlentities("é"),$result);
    $result = str_replace("í",htmlentities("í"),$result);
    $result = str_replace("ó",htmlentities("ó"),$result);
    $result = str_replace("ú",htmlentities("ú"),$result);
    $result = str_replace("Á",htmlentities("Á"),$result);
    $result = str_replace("É",htmlentities("É"),$result);
    $result = str_replace("Í",htmlentities("Í"),$result);
    $result = str_replace("Ó",htmlentities("Ó"),$result);
    $result = str_replace("Ú",htmlentities("Ú"),$result);
    $result = str_replace("ñ",htmlentities("ñ"),$result);
    $result = str_replace("Ñ",htmlentities("Ñ"),$result);
    $result = html_entity_decode($result, ENT_QUOTES, "ISO-8859-1");
    return $result;
}

function cellColor($cells,$color){
    global $objPHPExcel;

    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
            'rgb' => $color
        )
    ));
}


// variable login que almacena el login o nombre de usuario de la persona logueada
    $login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
    $usuario= $_SESSION['usuario'];
    $idUsuario= $_COOKIE["usIdentificacion"];
    $clave= $_COOKIE["clave"];
	$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
	$dato = mysqli_fetch_array($con);
	$perfil = $dato['prf_descripcion'];
	$claveperfil = $dato['prf_clave_int'];
	$claveusuario = $dato['usu_clave_int'];
	$tipoperfil = $dato['prf_tipo_perfil'];
	$visual = $dato['prf_visual'];
	//VISUALIZACION DE COSTOS Y TIEMPO
	if($perfil=="Administrador"){$vc = "visible"; $vt="visible";}else 
	if($visual=="1"){$vt="visible"; $vc="hidden";}else 
	if($visual==2){$vt="hidden"; $vc="visible";}else
	if($visual==3){$vt="visible";$vc = "visible"; }else{$vt="hidden";$vc="hidden";}
	
	
// verifica si no se ha loggeado
if(!isset($_SESSION["persona"]))
{
  session_destroy();
  header("LOCATION:index.php");
}else{
}

$emp = $_GET['emp'];
$cli = $_GET['cli'];
$pro = $_GET['pro'];
$act = $_GET['act'];
$fi = $_GET['fi'];
$ff = $_GET['ff'];
//$ti = $_GET['ti'];

$seleccionados = explode(',',$emp);
$num = count($seleccionados);
$empleados = array();
for($i = 0; $i < $num; $i++)
{
	if($seleccionados[$i] != '')
	{
		$empleados[$i]=$seleccionados[$i];
	}
}
$listaemplados=implode(',',$empleados);

$seleccionados = explode(',',$cli);
$num = count($seleccionados);
$clientes = array();
for($i = 0; $i < $num; $i++)
{
	if($seleccionados[$i] != '')
	{
		$clientes[$i]=$seleccionados[$i];
	}
}
$listaclientes=implode(',',$clientes);

$seleccionados = explode(',',$pro);
$num = count($seleccionados);
$proyectos = array();
for($i = 0; $i < $num; $i++)
{
	if($seleccionados[$i] != '')
	{
		$proyectos[$i]=$seleccionados[$i];
	}
}
$listaproyectos=implode(',',$proyectos);
		
$fecha=date("d/m/Y");
$fechaact=date("Y/m/d H:i:s");
//mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,obr_clave_int,loa_usu_actualiz,loa_fec_actualiz) values(null,27,71,'".$ultimaobra."','".$usuario."','".$fechaact."')");//Tercer campo tia_clave_int. 71=Archivo Impreso
//************ESTILOS******************

//*************************************
$objPHPExcel = new PHPExcel();

$archivo = 'PAVAS - Informe tiempos.xlsx';

//Propiedades de la hoja de excel
$objPHPExcel->getProperties()
		->setCreator("PAVAS TECNOLOGIA")
		->setLastModifiedBy("PAVAS TECNOLOGIA")
		->setTitle("Informe tiempos")
		->setSubject("Informe tiempos")
		->setDescription("Documento generado con el software Control de tiempos")
		->setKeywords("Control de tiempos")
		->setCategory("Reportes");
		
//Ancho de las Columnas

$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()
    ->setCellValue('A1' , "INFORME DE TIEMPOS POR USUARIO")
    ->setCellValue('A2' , "FECHA")
    ->setCellValue('B2' , "USUARIO")
    ->setCellValue('C2' , "REGION")
    ->setCellValue('D2' , "COD.CONTABLE")
    ->setCellValue('E2' , "PROYECTO")
    ->setCellValue('F2' , "CLIENTE")
    ->setCellValue('G2' , "ACTIVIDAD")
    ->setCellValue('H2' , "HORA INICIAL")
    ->setCellValue('I2' , "HORA FINAL")

    ->mergeCells('A1:I1');
    cellColor('A1:I2', 'B8CEB8');
    $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle("A1:I2")->getFont()->setBold(true)->setName('Calibri');
    $objPHPExcel->getActiveSheet()->getStyle('A1:I2')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(40);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);

    //************A1**************
    //$objPHPExcel->getActiveSheet()->getStyle('A1')-> applyFromArray($styleA1);//
    //$objPHPExcel->getActiveSheet()->getStyle('A2:I2')-> applyFromArray($styleA3);//
    $objPHPExcel->getActiveSheet()->getStyle('A1:I2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto


$sql = '';
if($sql == ''){ if($listaproyectos <> ''){ $sql = 'p.pro_clave_int in ('.$listaproyectos.')'; } }else{ if($listaproyectos <> ''){ $sql .= ' and p.pro_clave_int in ('.$listaproyectos.')'; } }
if($sql == ''){ if($listaclientes <> ''){ $sql = 'hd.cli_clave_int in ('.$listaclientes.')'; } }else{ if($listaclientes <> ''){ $sql .= ' and hd.cli_clave_int in ('.$listaclientes.')'; } }
if($sql == ''){ if($listaemplados <> ''){ $sql = 'hh.usu_clave_int in ('.$listaemplados.')'; } }else{ if($listaemplados <> ''){ $sql .= ' and hh.usu_clave_int in ('.$listaemplados.')'; } }
if($sql == ''){ $sql = "(hd.hod_actividad LIKE REPLACE('%".$act."%',' ','%') OR '".$act."' IS NULL OR '".$act."' = '')"; }else{ $sql .= " and (hd.hod_actividad LIKE REPLACE('%".$act."%',' ','%') OR '".$act."' IS NULL OR '".$act."' = '')"; }
if($sql == ''){ $sql = "((hh.hoh_fecha = '".$fi."' OR '".$fi."' IS NULL OR '".$fi."' = '') or ((hh.hoh_fecha BETWEEN '".$fi." 00:00:00' AND '".$ff." 23:59:59') or ('".$fi."' Is Null and '".$ff."' Is Null) or ('".$fi."' = '' and '".$ff."' = '')))"; }else{ $sql .= " and ((hh.hoh_fecha = '".$fi."' OR '".$fi."' IS NULL OR '".$fi."' = '') or ((hh.hoh_fecha BETWEEN '".$fi." 00:00:00' AND '".$ff." 23:59:59') or ('".$fi."' Is Null and '".$ff."' Is Null) or ('".$fi."' = '' and '".$ff."' = '')))"; }

$con = mysqli_query($conectar,"select u.usu_nombre usuario,p.pro_nombre,hh.hoh_fecha,hd.hod_actividad,c.cli_nombre cliente,CASE length(hd.hod_fila) WHEN 2 THEN substring(hd.hod_fila,1,1) WHEN 3 THEN substring(hd.hod_fila,1,1) WHEN 4 THEN substring(hd.hod_fila,1,2) END hi,CASE length(hd.hod_fila) WHEN 2 THEN substring(hd.hod_fila,2,1) WHEN 3 THEN substring(hd.hod_fila,2,2) WHEN 4 THEN substring(hd.hod_fila,3,2) END hf,hd.hod_vr_hora,hd.hod_fac_prestacional,r.reg_nombre,p.pro_codigo_contable from horas_h hh inner join horas_d hd on (hd.hoh_clave_int = hh.hoh_clave_int) inner join usuario u on (u.usu_clave_int = hh.usu_clave_int) inner join cliente c on (c.cli_clave_int = hd.cli_clave_int) inner join proyecto p on (p.pro_clave_int = hd.pro_clave_int) left outer join region r on r.reg_clave_int = p.reg_clave_int where ".$sql." and u.usu_sw_activo = 1");

//echo "select u.usu_nombre usuario,p.pro_nombre,hh.hoh_fecha,hd.hod_actividad,c.cli_nombre cliente,CASE length(hd.hod_fila) WHEN 2 THEN substring(hd.hod_fila,1,1) WHEN 3 THEN substring(hd.hod_fila,1,1) WHEN 4 THEN substring(hd.hod_fila,1,2) END hi,CASE length(hd.hod_fila) WHEN 2 THEN substring(hd.hod_fila,2,1) WHEN 3 THEN substring(hd.hod_fila,2,2) WHEN 4 THEN substring(hd.hod_fila,3,2) END hf,hd.hod_vr_hora,hd.hod_fac_prestacional,r.reg_nombre,p.pro_codigo_contable from horas_h hh inner join horas_d hd on (hd.hoh_clave_int = hh.hoh_clave_int) inner join usuario u on (u.usu_clave_int = hh.usu_clave_int) inner join cliente c on (c.cli_clave_int = hd.cli_clave_int) inner join proyecto p on (p.pro_clave_int = hd.pro_clave_int) left outer join region r on r.reg_clave_int = p.reg_clave_int where ".$sql." and u.usu_sw_activo = 1";
$num = mysqli_num_rows($con);
//echo $num;
$j = 3;

for($i = 0; $i < $num; $i++)
{
	$dato = mysqli_fetch_array($con);
	$usu = utf8_encode(convert_htmlentities($dato['usuario']));
	$pro = utf8_encode(convert_htmlentities($dato['pro_nombre']));
	$fec = $dato['hoh_fecha'];
	$act = $dato['hod_actividad'];
	$cli = utf8_encode(convert_htmlentities($dato['cliente']));
	$hi = $dato['hi'];
	$hf = $dato['hf'];
	if($vc=="hidden"){$vrhor="-"; }else{$vrhor = $dato['hod_vr_hora'];}
	$fact = $dato['hod_fac_prestacional'];
	$reg = $dato['reg_nombre']; if($reg=="" || $reg==NULL){ $reg="No tiene región"; }
	$reg = utf8_encode(convert_htmlentities($reg));
	$codcon = $dato['pro_codigo_contable'];
	//echo $pro."".$fec."".$act."".$usu.$hi."".$hf."".$fact."".$reg."".$codcon;
    // echo $vc;

    $objPHPExcel->getActiveSheet()//->setCellValue('A' . $j, $fec);
        ->setCellValue('B' . $j, $usu)
        ->setCellValue('C' . $j, $reg)
        ->setCellValue('D' . $j, $codcon)
        ->setCellValue('E' . $j, $pro)
        ->setCellValue('F' . $j, $cli)
        ->setCellValue('G' . $j, $act)
       // ->setCellValue('H' . $j, $hi)
        //->setCellValue('I' . $j, $hf)
    ;

        cellColor('A'. $j.':I'.$j, 'FFFFFF');
    $objPHPExcel->getActiveSheet()->getStyle('F'.$j.':G'.$j)->getAlignment()->setWrapText(true);
    $objPHPExcel->getActiveSheet()->getCell('A'. $j)->setValueExplicit($fec,PHPExcel_Cell_DataType::TYPE_STRING);
    $objPHPExcel->getActiveSheet()->getCell('H'. $j)->setValueExplicit($hi,PHPExcel_Cell_DataType::TYPE_STRING);
    $objPHPExcel->getActiveSheet()->getCell('I'. $j)->setValueExplicit($hf,PHPExcel_Cell_DataType::TYPE_STRING);

    $objPHPExcel->getActiveSheet()->getStyle('A'.$j.':I'.$j)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $objPHPExcel->getActiveSheet()->getStyle('A'.$j.':I'.$j)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	//****************************
		
	$j++;
}
//DATOS DE SALIDA DEL EXCEL
/*
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$archivo.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
*/
//zoom de la pagina
$objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(90);
//titulos de la hoja
$objPHPExcel->getActiveSheet()->setTitle("Informe Tiempos");

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$archivo.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
//exit;