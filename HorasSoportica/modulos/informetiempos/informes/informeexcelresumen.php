<?php
error_reporting(E_ALL);
include('../../../data/Conexion.php');
require_once('../../../Classes/PHPExcel.php');
date_default_timezone_set('America/Bogota');
session_start();
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_SESSION['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];
$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
$dato = mysqli_fetch_array($con);
$perfil = $dato['prf_descripcion'];
$claveperfil = $dato['prf_clave_int'];
$claveusuario = $dato['usu_clave_int'];
$tipoperfil = $dato['prf_tipo_perfil'];
$visual = $dato['prf_visual'];
	if($perfil=="Administrador"){$vc = "visible"; $vt="visible";}else 
	if($visual=="1"){$vt="visible"; $vc="hidden";}else 
	if($visual==2){$vt="hidden"; $vc="visible";}else
	if($visual==3){$vt="visible";$vc = "visible"; }else{$vt="hidden";$vc="hidden";}
	
// verifica si no se ha loggeado
if(!isset($_SESSION["persona"]))
{
  session_destroy();
  header("LOCATION:index.php");
}else{
}

$emp = $_GET['emp'];
$cli = $_GET['cli'];
$pro = $_GET['pro'];
$act = $_GET['act'];
$fi = $_GET['fi'];
$ff = $_GET['ff'];
//$ti = $_GET['ti'];

$seleccionados = explode(',',$emp);
$num = count($seleccionados);
$empleados = array();
for($i = 0; $i < $num; $i++)
{
	if($seleccionados[$i] != '')
	{
		$empleados[$i]=$seleccionados[$i];
	}
}
$listaemplados=implode(',',$empleados);

$seleccionados = explode(',',$cli);
$num = count($seleccionados);
$clientes = array();
for($i = 0; $i < $num; $i++)
{
	if($seleccionados[$i] != '')
	{
		$clientes[$i]=$seleccionados[$i];
	}
}
$listaclientes=implode(',',$clientes);

$seleccionados = explode(',',$pro);
$num = count($seleccionados);
$proyectos = array();
for($i = 0; $i < $num; $i++)
{
	if($seleccionados[$i] != '')
	{
		$proyectos[$i]=$seleccionados[$i];
	}
}
$listaproyectos=implode(',',$proyectos);
		
$fecha=date("d/m/Y");
$fechaact=date("Y/m/d H:i:s");
//mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,obr_clave_int,loa_usu_actualiz,loa_fec_actualiz) values(null,27,71,'".$ultimaobra."','".$usuario."','".$fechaact."')");//Tercer campo tia_clave_int. 71=Archivo Impreso
//************ESTILOS******************

$styleA1 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => '000000'),
    'size'  => 12,
    'name'  => 'Calibri'
));
$styleA2 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => 'C83000'),
    'size'  => 10,
    'name'  => 'Arial'
));
$styleA3 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => '000000'),
    'size'  => 10,
    'name'  => 'Arial'
));
$styleA4 = array(
'font'  => array(
    'bold'  => false,
    'color' => array('rgb' => '000000'),
    'size'  => 10,
    'name'  => 'Arial'
));
$styleA3p1 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => '000000'),
    'size'  => 9,
    'name'  => 'Arial'
));
$styleA4p1 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => '000000'),
    'size'  => 9,
    'name'  => 'Arial'
));

$borders = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '000000'),
		)
	),
);

function cellColor($cells,$color){
    global $objPHPExcel;

    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
            'rgb' => $color
        )
    ));
}


//*************************************

$objPHPExcel = new PHPExcel();
$archivo = 'PAVAS - Informe resumen por usuario.xls';

//Propiedades de la hoja de excel
$objPHPExcel->getProperties()
		->setCreator("PAVAS TECNOLOGIA")
		->setLastModifiedBy("PAVAS TECNOLOGIA")
		->setTitle("Informe tiempos")
		->setSubject("Informe tiempos")
		->setDescription("Documento generado con el software Control de tiempos")
		->setKeywords("Control de tiempos")
		->setCategory("Reportes");
		
//Ancho de las Columnas
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);


//************A1**************
$objPHPExcel->getActiveSheet()->getStyle('A1')-> applyFromArray($styleA1);//
$objPHPExcel->getActiveSheet()->getCell('A1')->setValue("INFORME RESUMEN POR USUARIO");
$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setWrapText(true); //Crea un enter entre palabras
$objPHPExcel->getActiveSheet()->mergeCells('A1:J1');//Conbinar celdas
$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('B8CCE4');//COLOR DE FONDO
cellColor('A1:J2', 'B8CEB8');
//****************************

/**************** COLUMNAS ****************/

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('A2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('A2')->setValue("USUARIO");
$objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************
//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('B2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('B2')->setValue("REGIÓN");
$objPHPExcel->getActiveSheet()->getStyle('B2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//
//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('C2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('C2')->setValue("COD.CONTABLE");
$objPHPExcel->getActiveSheet()->getStyle('C2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('D2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('D2')->setValue("PROYECTO");
$objPHPExcel->getActiveSheet()->getStyle('D2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('E2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('E2')->setValue("CLIENTE");
$objPHPExcel->getActiveSheet()->getStyle('E2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('F2')-> applyFromArray($styleA3);//
if($vt=="hidden"){$objPHPExcel->getActiveSheet()->getCell('F2')->setValue("");}else{
	$objPHPExcel->getActiveSheet()->getCell('F2')->setValue("TOTAL HORAS");}
$objPHPExcel->getActiveSheet()->getStyle('F2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('G2')-> applyFromArray($styleA3);//
if($vt=="hidden"){$objPHPExcel->getActiveSheet()->getCell('G2')->setValue("");}else{
	$objPHPExcel->getActiveSheet()->getCell('G2')->setValue("PORCENTAJE");}
$objPHPExcel->getActiveSheet()->getStyle('G2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************


//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('H2')-> applyFromArray($styleA3);//
if($vc=="hidden"){$objPHPExcel->getActiveSheet()->getCell('H2')->setValue("");}else{
$objPHPExcel->getActiveSheet()->getCell('H2')->setValue("VR. HORA");}
$objPHPExcel->getActiveSheet()->getStyle('H2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('I2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('I2')->setValue("FACTOR PRESTACIONAL");
$objPHPExcel->getActiveSheet()->getStyle('I2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('J2')-> applyFromArray($styleA3);//
if($vc=="hidden"){$objPHPExcel->getActiveSheet()->getCell('J2')->setValue("");}else{
$objPHPExcel->getActiveSheet()->getCell('J2')->setValue("TOTAL DEVENGADO");}
$objPHPExcel->getActiveSheet()->getStyle('J2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

$objPHPExcel->getActiveSheet()->getStyle('A1:J2')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

$sql = '';
if($sql == ''){ if($listaproyectos <> ''){ $sql = 'p.pro_clave_int in ('.$listaproyectos.')'; } }else{ if($listaproyectos <> ''){ $sql .= ' and p.pro_clave_int in ('.$listaproyectos.')'; } }
if($sql == ''){ if($listaclientes <> ''){ $sql = 'hd.cli_clave_int in ('.$listaclientes.')'; } }else{ if($listaclientes <> ''){ $sql .= ' and hd.cli_clave_int in ('.$listaclientes.')'; } }
if($sql == ''){ if($listaemplados <> ''){ $sql = 'hh.usu_clave_int in ('.$listaemplados.')'; } }else{ if($listaemplados <> ''){ $sql .= ' and hh.usu_clave_int in ('.$listaemplados.')'; } }
if($sql == ''){ $sql = "(hd.hod_actividad LIKE REPLACE('%".$act."%',' ','%') OR '".$act."' IS NULL OR '".$act."' = '')"; }else{ $sql .= " and (hd.hod_actividad LIKE REPLACE('%".$act."%',' ','%') OR '".$act."' IS NULL OR '".$act."' = '')"; }
if($sql == ''){ $sql = "((hh.hoh_fecha = '".$fi."' OR '".$fi."' IS NULL OR '".$fi."' = '') or ((hh.hoh_fecha BETWEEN '".$fi." 00:00:00' AND '".$ff." 23:59:59') or ('".$fi."' Is Null and '".$ff."' Is Null) or ('".$fi."' = '' and '".$ff."' = '')))"; }else{ $sql .= " and ((hh.hoh_fecha = '".$fi."' OR '".$fi."' IS NULL OR '".$fi."' = '') or ((hh.hoh_fecha BETWEEN '".$fi." 00:00:00' AND '".$ff." 23:59:59') or ('".$fi."' Is Null and '".$ff."' Is Null) or ('".$fi."' = '' and '".$ff."' = '')))"; }

$con = mysqli_query($conectar,"select u.usu_clave_int,u.usu_nombre usuario,p.pro_nombre,c.cli_nombre cliente,COUNT(*) horas,hd.hod_vr_hora,hd.hod_fac_prestacional, CASE WHEN hd.fac_clave_int = 1 THEN (COUNT(*)*u.usu_vr_hora)*hd.hod_fac_prestacional ELSE COUNT(*)*u.usu_vr_hora END total,r.reg_nombre,p.pro_codigo_contable from horas_h hh inner join horas_d hd on (hd.hoh_clave_int = hh.hoh_clave_int) inner join usuario u on (u.usu_clave_int = hh.usu_clave_int) inner join cliente c on (c.cli_clave_int = hd.cli_clave_int) inner join proyecto p on (p.pro_clave_int = hd.pro_clave_int) left outer join region r on r.reg_clave_int = p.reg_clave_int where ".$sql." and u.usu_sw_activo = 1 GROUP BY hh.usu_clave_int,p.pro_clave_int,hd.cli_clave_int");


$num = mysqli_num_rows($con);

$j = 3;
for($i = 0; $i < $num; $i++)
{
	$dato = mysqli_fetch_array($con);
	$clausu = $dato['usu_clave_int'];
	$usu = $dato['usuario'];
	$pro = $dato['pro_nombre'];
	$cli = $dato['cliente'];
	$reg = $dato['reg_nombre']; if($reg=="" || $reg==NULL){ $reg="No tiene región"; }
	$codcon = $dato['pro_codigo_contable'];
	
	if($vt=="hidden"){$horas="-";}else{$horas = $dato['horas'];}
	if($vc=="hidden"){$vrhor="-";}else{$vrhor = $dato['hod_vr_hora'];}
	$fact = $dato['hod_fac_prestacional'];
	if($vc=="hidden"){$total="-";}else{$total = $dato['total'];}
	
	$con1 = mysqli_query($conectar,"select COUNT(*) totalhoras from horas_h hh inner join horas_d hd on (hd.hoh_clave_int = hh.hoh_clave_int) inner join usuario u on (u.usu_clave_int = hh.usu_clave_int) inner join cliente c on (c.cli_clave_int = hd.cli_clave_int) inner join proyecto p on (p.pro_clave_int = hd.pro_clave_int)   where ".$sql." and u.usu_sw_activo = 1 and u.usu_clave_int = '".$clausu."'");
	$dato1 = mysqli_fetch_array($con1);
	$totalhoras = $dato1['totalhoras'];
	
	$porcentaje = ROUND(($horas/$totalhoras)*100,2);

	
	/**************** DATOS DE LAS COLUMNAS ****************/ 

	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('A'.$j)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("A".$j, $usu);
	//****************************
	$objPHPExcel->getActiveSheet()->getStyle('B'.$j)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("B".$j, $reg);

	$objPHPExcel->getActiveSheet()->getStyle('C'.$j)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("C".$j, $codcon);
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('D'.$j)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("D".$j, $pro);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('E'.$j)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("E".$j, $cli);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('F'.$j)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("F".$j, $horas);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('G'.$j)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("G".$j, $porcentaje);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('H'.$j)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("H".$j, $vrhor);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('I'.$j)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("I".$j, $fact);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('J'.$j)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("J".$j, $total);


    $objPHPExcel->getActiveSheet()->getStyle('A'.$j.':J'.$j)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	//****************************
	
	$j++;
}
//DATOS DE SALIDA DEL EXCEL
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$archivo.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
?>