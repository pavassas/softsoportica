<?php
	error_reporting(E_ALL);
	include('../../data/Conexion.php');
	
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_SESSION['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];
		
	// verifica si no se ha loggeado
	if(!isset($_SESSION["persona"]))
	{
	  session_destroy();
	  header("LOCATION:index.php");
	}else{
	}
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");
	$anocont = date("Y");
	require_once("lib/Zebra_Pagination.php");
	
	$con = mysqli_query($conectar,"select u.usu_clave_int,p.prf_clave_int,p.prf_descripcion,p.prf_sw_actualiza_info,p.prf_tipo_perfil as ti,prf_visual from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
	$dato = mysqli_fetch_array($con);
	$clausu = $dato['usu_clave_int'];
	$claprf = $dato['prf_clave_int'];
	$perfil = $dato['prf_descripcion'];
	$tipoperfil = $dato['ti'];
	$visual = $dato['prf_visual'];
	$actualizainfo = $dato['prf_sw_actualiza_info'];
	if($perfil=="Administrador"){$vc = "visible"; $vt="visible";}else 
	if($visual=="1"){$vt="visible"; $vc="hidden";}else 
	if($visual==2){$vt="hidden"; $vc="visible";}else
	if($visual==3){$vt="visible";$vc = "visible"; }else{$vt="hidden";$vc="hidden";}
	
	$con = mysqli_query($conectar,"select per_metodo from permiso where prf_clave_int = '".$claprf."' and ven_clave_int = 4");
	$dato = mysqli_fetch_array($con);
	$metodo = $dato['per_metodo'];
	
	if($_GET['buscar'] == 'si')
	{
		$emp = $_GET['emp'];
		$cli = $_GET['cli'];
		$pro = $_GET['pro'];
		$act = $_GET['act'];
		$fi = $_GET['fi'];
		$ff = $_GET['ff'];
		$ti = $_GET['ti'];
		
		$seleccionados = explode(',',$emp);
		$num = count($seleccionados);
		$empleados = array();
		for($i = 0; $i < $num; $i++)
		{
			if($seleccionados[$i] != '')
			{
				$empleados[$i]=$seleccionados[$i];
			}
		}
		$listaemplados=implode(',',$empleados);
		
		$seleccionados = explode(',',$cli);
		$num = count($seleccionados);
		$clientes = array();
		for($i = 0; $i < $num; $i++)
		{
			if($seleccionados[$i] != '')
			{
				$clientes[$i]=$seleccionados[$i];
			}
		}
		$listaclientes=implode(',',$clientes);
		
		$seleccionados = explode(',',$pro);
		$num = count($seleccionados);
		$proyectos = array();
		for($i = 0; $i < $num; $i++)
		{
			if($seleccionados[$i] != '')
			{
				$proyectos[$i]=$seleccionados[$i];
			}
		}
		$listaproyectos=implode(',',$proyectos);
		
		if($pro <= 0)
		{
			$conpro = mysqli_query($conectar,"select pro_clave_int from proyecto where UPPER(pro_nombre) = UPPER('".$pro."')");
			$datopro = mysqli_fetch_array($conpro);
			$listaproyectos = $datopro['pro_clave_int'];
		}
		
		$sql = '';
		if($sql == ''){ if($listaproyectos <> ''){ $sql = 'p.pro_clave_int in ('.$listaproyectos.')'; } }else{ if($listaproyectos <> ''){ $sql .= ' and p.pro_clave_int in ('.$listaproyectos.')'; } }
		if($sql == ''){ if($listaclientes <> ''){ $sql = 'hd.cli_clave_int in ('.$listaclientes.')'; } }else{ if($listaclientes <> ''){ $sql .= ' and hd.cli_clave_int in ('.$listaclientes.')'; } }
		if($sql == ''){ if($listaemplados <> ''){ $sql = 'hh.usu_clave_int in ('.$listaemplados.')'; } }else{ if($listaemplados <> ''){ $sql .= ' and hh.usu_clave_int in ('.$listaemplados.')'; } }
		if($sql == ''){ $sql = "(hd.hod_actividad LIKE REPLACE('%".$act."%',' ','%') OR '".$act."' IS NULL OR '".$act."' = '')"; }else{ $sql .= " and (hd.hod_actividad LIKE REPLACE('%".$act."%',' ','%') OR '".$act."' IS NULL OR '".$act."' = '')"; }
		if($sql == ''){ $sql = "((hh.hoh_fecha = '".$fi."' OR '".$fi."' IS NULL OR '".$fi."' = '') or ((hh.hoh_fecha BETWEEN '".$fi." 00:00:00' AND '".$ff." 23:59:59') or ('".$fi."' Is Null and '".$ff."' Is Null) or ('".$fi."' = '' and '".$ff."' = '')))"; }else{ $sql .= " and ((hh.hoh_fecha = '".$fi."' OR '".$fi."' IS NULL OR '".$fi."' = '') or ((hh.hoh_fecha BETWEEN '".$fi." 00:00:00' AND '".$ff." 23:59:59') or ('".$fi."' Is Null and '".$ff."' Is Null) or ('".$fi."' = '' and '".$ff."' = '')))"; }
		
		if($ti == 1)
		{
			?>
			<fieldset name="Group1">
				<legend align="center">
				<table style="width: 100%">
					<tr>
						<td align="center"><strong>RESUMEN</strong></td>
						<td align="center">&nbsp;</td>
						<td><strong>DETALLE</strong></td>
						<td>&nbsp;</td>
						<td><strong>SIN REPORTAR</strong></td>
					</tr>
					<tr>
						<td align="center"><img src="../../images/excel.png" onclick="EXPORTARRESUMEN()" title="EXPORTAR RESUMEN" style="height:35px;width:35px;cursor:pointer"></td>
						<td align="center">&nbsp;</td>
						<td align="center"><img src="../../images/excel.png" onclick="EXPORTARDETALLE()" title="EXPORTAR DETALLE" style="height:35px;width:35px;cursor:pointer"></td>
						<td align="center">&nbsp;</td>
						<td align="center"><img src="../../images/excel.png" onclick="EXPORTARSINREPORTAR()" title="EXPORTAR USUARIOS SIN REPORTAR" style="height:35px;width:35px;cursor:pointer"></td>
					</tr>
				</table>
				</legend>
			<ul class="auto-style10" style="width: 100%; list-style:none; float:left">
			<?php
			$con = mysqli_query($conectar,"select p.pro_clave_int,p.pro_nombre,r.reg_nombre,p.pro_codigo_contable from horas_h hh inner join horas_d hd on (hd.hoh_clave_int = hh.hoh_clave_int) inner join usuario u on (u.usu_clave_int = hh.usu_clave_int) inner join proyecto p on (p.pro_clave_int = hd.pro_clave_int) left outer join region r on r.reg_clave_int = p.reg_clave_int where ".$sql." GROUP BY p.pro_clave_int");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clapro = $dato['pro_clave_int'];
				$pro = $dato['pro_nombre'];
				$reg = $dato['reg_nombre'];
			?>
		        <li style="padding-top:5px; float:left; width:95%;">
				<div style="float:left">
					<img alt="" class="expand" id="expand" src="../../images/Minus.png" />
					<img alt="" class="collapse" id="collapse" onclick="VEREMPLEADOS('<?php echo $clapro; ?>')" src="../../images/Plus.png" />
				</div>
				<div style="<?php if($i % 2 == 0){ echo 'background-color:#B8CEB8"'; }else{ echo 'background-color:#E1E5E1"'; } ?>;float:left; width:95%; text-align:left" class="bordes">
				&nbsp;&nbsp;<div style="float:left; height: 15px;"><strong>REGION:</strong> <?php echo strtoupper($reg); ?>  <strong>PROYECTO:</strong> <?php echo strtoupper($pro); ?></div>
				<div style="float:left;text-align:right;font-size:small">
				&nbsp;&nbsp;&nbsp;
				<?php
				$sum = 0;
				$hor = 0;
				$con1 = mysqli_query($conectar,"select CASE hd.fac_clave_int WHEN 1 THEN hd.hod_vr_hora*hd.hod_fac_prestacional ELSE hd.hod_vr_hora END valor from horas_h hh inner join horas_d hd on (hd.hoh_clave_int = hh.hoh_clave_int) inner join proyecto p on (p.pro_clave_int = hd.pro_clave_int) inner join usuario u on (u.usu_clave_int = hh.usu_clave_int) where hd.pro_clave_int = '".$clapro."' and ".$sql."");
				$num1 = mysqli_num_rows($con1);
				//echo "select CASE hd.fac_clave_int WHEN 1 THEN hd.hod_vr_hora*hd.hod_fac_prestacional ELSE hd.hod_vr_hora END valor from horas_h hh inner join horas_d hd on (hd.hoh_clave_int = hh.hoh_clave_int) inner join proyecto p on (p.pro_clave_int = hd.pro_clave_int) inner join usuario u on (u.usu_clave_int = hh.usu_clave_int) where hd.pro_clave_int = '".$clapro."' and ".$sql."";
				for($j = 0; $j < $num1; $j++)
				{
					$dato1 = mysqli_fetch_array($con1);
					$valor = $dato1['valor'];
					$sum = $sum+$valor;
					$hor = $hor+1;
				}
				echo "<span style='visibility:".$vt."'><strong >TOTAL HORAS: </strong>".$hor."</span><span  style='visibility:".$vc."'> <strong> VR. TOTAL: </strong>$".number_format($sum,0 , "," ,".")."</span>";
				?>
				</div>
				</div>
				<ul style="display:none; list-style:none; float:left; width:100%;">
		        <div id="empleados<?php echo $clapro; ?>">
		        
		        </div>
		        </ul>
		        </li>
		    <?php
		    }
		    ?>
		    </ul>
		    </fieldset>
			<?php
		}
		else
		if($ti == 2)
		{
			?>
			<fieldset name="Group1">
				<legend align="center">
				<table style="width: 100%">
					<tr>
						<td align="center"><strong>RESUMEN</strong></td>
						<td align="center">&nbsp;</td>
						<td><strong>DETALLE</strong></td>
						<td>&nbsp;</td>
						<td><strong>SIN REPORTAR</strong></td>
					</tr>
					<tr>
						<td align="center"><img src="../../images/excel.png" onclick="EXPORTARRESUMEN()" title="EXPORTAR RESUMEN" style="height:35px;width:35px;cursor:pointer"></td>
						<td align="center">&nbsp;</td>
						<td align="center"><img src="../../images/excel.png" onclick="EXPORTARDETALLE()" title="EXPORTAR DETALLE" style="height:35px;width:35px;cursor:pointer"></td>
						<td align="center">&nbsp;</td>
						<td align="center"><img src="../../images/excel.png" onclick="EXPORTARSINREPORTAR()" title="EXPORTAR USUARIOS SIN REPORTAR" style="height:35px;width:35px;cursor:pointer"></td>
					</tr>
				</table>
				</legend>
			<ul class="auto-style10" style="width: 100%; list-style:none; float:left">
			<?php
			$con = mysqli_query($conectar,"select u.usu_clave_int, u.usu_nombre, COUNT(*) totalhoras, CASE WHEN hd.fac_clave_int = 1 THEN SUM(hd.hod_vr_hora*hd.hod_fac_prestacional) ELSE SUM(hd.hod_vr_hora) END total from horas_h hh inner join horas_d hd on (hd.hoh_clave_int = hh.hoh_clave_int) inner join proyecto p on (p.pro_clave_int = hd.pro_clave_int) inner join usuario u on (u.usu_clave_int = hh.usu_clave_int) where ".$sql." GROUP BY u.usu_clave_int");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clausu = $dato['usu_clave_int'];
				$nom = $dato['usu_nombre'];
				$tot = $dato['total'];
				$tothor = $dato['totalhoras'];
			?>
		        <li style="padding-top:5px; float:left; width:95%;">
				<div style="float:left">
					<img alt="" class="expand" id="expand" src="../../images/Minus.png" />
					<img alt="" class="collapse" id="collapse" onclick="VERPROYECTOS('<?php echo $clausu; ?>','<?php echo $i; ?>')" src="../../images/Plus.png" />
				</div>
				<div style="<?php if($i % 2 == 0){ echo 'background-color:#B8CEB8"'; }else{ echo 'background-color:#E1E5E1"'; } ?>;float:left; width:95%; text-align:left" class="bordes">
				&nbsp;&nbsp;<div style="float:left"><strong><?php echo strtoupper($nom); ?></strong></div>
				<div style="float:left;text-align:right;font-size:small">
				&nbsp;
				<?php
				echo "<span style='visibility:".$vt."'> <strong >TOTAL HORAS: </strong> ".$tothor."</span><span style='visibility:".$vc."'> <strong > VR. TOTAL:</strong> $".number_format($tot,0 , "," ,".")."</span>";
				?>
				</div>
				</div>
				<ul style="display:none; list-style:none; float:left; width:100%;">
		        <div id="proyecto<?php echo $clausu."".$i; ?>">
		        
		        </div>
		        </ul>
		        </li>
		    <?php
		    }
		    ?>
		    </ul>
		    </fieldset>
			<?php
		}
		else
		if($ti == 3)
		{
			?>
			
			<?php
		}
		exit();
	}
	if($_GET['verempleados'] == 'si')
	{
		$pro = $_GET['pro'];
		$emp = $_GET['emp'];
		$cli = $_GET['cli'];
		$act = $_GET['act'];
		$fi = $_GET['fi'];
		$ff = $_GET['ff'];
		
		$seleccionados = explode(',',$emp);
		$num = count($seleccionados);
		$empleados = array();
		for($i = 0; $i < $num; $i++)
		{
			if($seleccionados[$i] != '')
			{
				$empleados[$i]=$seleccionados[$i];
			}
		}
		$listaemplados=implode(',',$empleados);
		
		$seleccionados = explode(',',$cli);
		$num = count($seleccionados);
		$clientes = array();
		for($i = 0; $i < $num; $i++)
		{
			if($seleccionados[$i] != '')
			{
				$clientes[$i]=$seleccionados[$i];
			}
		}
		$listaclientes=implode(',',$clientes);
				
		$sql = '';
		if($sql == ''){ if($listaclientes <> ''){ $sql = 'hd.cli_clave_int in ('.$listaclientes.')'; } }else{ if($listaclientes <> ''){ $sql .= ' and hd.cli_clave_int in ('.$listaclientes.')'; } }
		if($sql == ''){ if($listaemplados <> ''){ $sql = 'hh.usu_clave_int in ('.$listaemplados.')'; } }else{ if($listaemplados <> ''){ $sql .= ' and hh.usu_clave_int in ('.$listaemplados.')'; } }
		if($sql == ''){ $sql = "(hd.hod_actividad LIKE REPLACE('%".$act."%',' ','%') OR '".$act."' IS NULL OR '".$act."' = '')"; }else{ $sql .= " and (hd.hod_actividad LIKE REPLACE('%".$act."%',' ','%') OR '".$act."' IS NULL OR '".$act."' = '')"; }
		if($sql == ''){ $sql = "((hh.hoh_fecha = '".$fi."' OR '".$fi."' IS NULL OR '".$fi."' = '') or ((hh.hoh_fecha BETWEEN '".$fi." 00:00:00' AND '".$ff." 23:59:59') or ('".$fi."' Is Null and '".$ff."' Is Null) or ('".$fi."' = '' and '".$ff."' = '')))"; }else{ $sql .= " and ((hh.hoh_fecha = '".$fi."' OR '".$fi."' IS NULL OR '".$fi."' = '') or ((hh.hoh_fecha BETWEEN '".$fi." 00:00:00' AND '".$ff." 23:59:59') or ('".$fi."' Is Null and '".$ff."' Is Null) or ('".$fi."' = '' and '".$ff."' = '')))"; }
		
		$con = mysqli_query($conectar,"select u.usu_clave_int, u.usu_nombre, COUNT(*) totalhoras, CASE WHEN hd.fac_clave_int = 1 THEN SUM(hd.hod_vr_hora*hd.hod_fac_prestacional) ELSE SUM(hd.hod_vr_hora) END total from horas_h hh inner join horas_d hd on (hd.hoh_clave_int = hh.hoh_clave_int) inner join usuario u on (u.usu_clave_int = hh.usu_clave_int) where hd.pro_clave_int = '".$pro."' and ".$sql." GROUP BY u.usu_clave_int");
		$num = mysqli_num_rows($con);
		for($i = 0; $i < $num; $i++)
		{
			$dato = mysqli_fetch_array($con);
			$clausu = $dato['usu_clave_int'];
			$nom = $dato['usu_nombre'];
			$tot = $dato['total'];
			$tothor = $dato['totalhoras'];
		?>
	        <li style="padding-top:5px; float:left; width:95%;">
			<div style="float:left">
				<img alt="" class="expand" id="expand" src="../../images/Minus.png" />
				<img alt="" class="collapse" id="collapse" onclick="VERFECHAS('<?php echo $clausu; ?>','<?php echo $i; ?>','<?php echo $pro; ?>')" src="../../images/Plus.png" />
			</div>
			<div style="<?php if($i % 2 == 0){ echo 'background-color:#B8CEB8"'; }else{ echo 'background-color:#E1E5E1"'; } ?>;float:left; width:95%; text-align:left" class="bordes">
			&nbsp;&nbsp;<div style="float:left"><strong><?php echo strtoupper($nom); ?></strong></div>
			<div style="float:left;text-align:right;font-size:small">
			&nbsp;
			<?php
			echo "<span style='visibility:".$vt."'><strong >TOTAL HORAS: </strong>".$tothor."</span><span style='visibility:".$vc."'><strong > VR. TOTAL: </strong> $".number_format($tot,0 , "," ,".")."</span>";
			?>
			</div>
			</div>
			<ul style="display:none; list-style:none; float:left; width:100%;">
	        <div id="fechas<?php echo $clausu."".$i."".$pro; ?>">
	        
	        </div>
	        </ul>
	        </li>
	    <?php
	    }
		exit();
	}
	if($_GET['verfechas'] == 'si')
	{
		$clausu = $_GET['clausu'];
		$pro = $_GET['pro'];
		$emp = $_GET['emp'];
		$cli = $_GET['cli'];
		$act = $_GET['act'];
		$fi = $_GET['fi'];
		$ff = $_GET['ff'];
		
		$seleccionados = explode(',',$emp);
		$num = count($seleccionados);
		$empleados = array();
		for($i = 0; $i < $num; $i++)
		{
			if($seleccionados[$i] != '')
			{
				$empleados[$i]=$seleccionados[$i];
			}
		}
		$listaemplados=implode(',',$empleados);
		
		$seleccionados = explode(',',$cli);
		$num = count($seleccionados);
		$clientes = array();
		for($i = 0; $i < $num; $i++)
		{
			if($seleccionados[$i] != '')
			{
				$clientes[$i]=$seleccionados[$i];
			}
		}
		$listaclientes=implode(',',$clientes);
				
		$sql = '';
		if($sql == ''){ if($listaclientes <> ''){ $sql = 'hd.cli_clave_int in ('.$listaclientes.')'; } }else{ if($listaclientes <> ''){ $sql .= ' and hd.cli_clave_int in ('.$listaclientes.')'; } }
		if($sql == ''){ if($listaemplados <> ''){ $sql = 'hh.usu_clave_int in ('.$listaemplados.')'; } }else{ if($listaemplados <> ''){ $sql .= ' and hh.usu_clave_int in ('.$listaemplados.')'; } }
		if($sql == ''){ $sql = "(hd.hod_actividad LIKE REPLACE('%".$act."%',' ','%') OR '".$act."' IS NULL OR '".$act."' = '')"; }else{ $sql .= " and (hd.hod_actividad LIKE REPLACE('%".$act."%',' ','%') OR '".$act."' IS NULL OR '".$act."' = '')"; }
		if($sql == ''){ $sql = "((hh.hoh_fecha = '".$fi."' OR '".$fi."' IS NULL OR '".$fi."' = '') or ((hh.hoh_fecha BETWEEN '".$fi." 00:00:00' AND '".$ff." 23:59:59') or ('".$fi."' Is Null and '".$ff."' Is Null) or ('".$fi."' = '' and '".$ff."' = '')))"; }else{ $sql .= " and ((hh.hoh_fecha = '".$fi."' OR '".$fi."' IS NULL OR '".$fi."' = '') or ((hh.hoh_fecha BETWEEN '".$fi." 00:00:00' AND '".$ff." 23:59:59') or ('".$fi."' Is Null and '".$ff."' Is Null) or ('".$fi."' = '' and '".$ff."' = '')))"; }
		
		$con = mysqli_query($conectar,"select u.usu_clave_int, hh.hoh_fecha, COUNT(*) horas, hd.hod_vr_hora, CASE WHEN hd.fac_clave_int = 1 THEN SUM(hd.hod_vr_hora*hd.hod_fac_prestacional) ELSE SUM(hd.hod_vr_hora) END total from horas_h hh inner join horas_d hd on (hd.hoh_clave_int = hh.hoh_clave_int) inner join usuario u on (u.usu_clave_int = hh.usu_clave_int) where u.usu_clave_int = '".$clausu."' and hd.pro_clave_int = '".$pro."' and ".$sql." GROUP BY hh.hoh_fecha");
		$num = mysqli_num_rows($con);
		for($i = 0; $i < $num; $i++)
		{
			$dato = mysqli_fetch_array($con);
			$clausu = $dato['usu_clave_int'];
			$fec = $dato['hoh_fecha'];
			$horas = $dato['horas'];
			$vrhora = $dato['hod_vr_hora'];
			$tot = $dato['total'];
		?>
	        <li style="padding-top:5px; float:left; width:95%;">
			<div style="float:left">
				<img alt="" class="expand" id="expand" src="../../images/Minus.png" />
				<img alt="" class="collapse" id="collapse" onclick="VERDETALLE('<?php echo $clausu; ?>','<?php echo $fec; ?>','<?php echo $pro; ?>')" src="../../images/Plus.png" />
			</div>
			<div style="<?php if($i % 2 == 0){ echo 'background-color:#B8CEB8"'; }else{ echo 'background-color:#E1E5E1"'; } ?>;float:left; width:95%; text-align:left" class="bordes">
			&nbsp;&nbsp;
			<div style="float:left">
				<table style="width: 100%;margin-top:-2px">
					<tr>
						<td><strong>FECHA:</strong></td>
						<td><?php echo $fec; ?></td>
						<td style='visibility:<?php echo $vt;?>'><strong>HORAS:</strong></td>
						<td style='visibility:<?php echo $vt;?>'><?php echo $horas; ?></td>
						<td style='visibility:<?php echo $vc;?>'><strong>VR. HORA:</strong></td>
						<td style='visibility:<?php echo $vc;?>'><?php echo "$".number_format($vrhora,0 , "," ,"."); ?></td>
						<td style='visibility:<?php echo $vc;?>'><strong>VR. TOTAL:</strong></td>
						<td style='visibility:<?php echo $vc;?>'><?php echo "$".number_format($tot,0 , "," ,"."); ?></td>
					</tr>
				</table>
			</div>
			</div>
			<ul style="display:none; list-style:none; float:left; width:100%;">
	        <div id="detalle<?php echo $clausu."".$fec."".$pro; ?>">
	        
	        </div>
	        </ul>
	        </li>
	    <?php
	    }
		exit();
	}
	if($_GET['verdetalle'] == 'si')
	{
		$clausu = $_GET['clausu'];
		$fec = $_GET['fec'];
		$pro = $_GET['pro'];
		$emp = $_GET['emp'];
		$cli = $_GET['cli'];
		$act = $_GET['act'];
		$fi = $_GET['fi'];
		$ff = $_GET['ff'];
		
		$seleccionados = explode(',',$emp);
		$num = count($seleccionados);
		$empleados = array();
		for($i = 0; $i < $num; $i++)
		{
			if($seleccionados[$i] != '')
			{
				$empleados[$i]=$seleccionados[$i];
			}
		}
		$listaemplados=implode(',',$empleados);
		
		$seleccionados = explode(',',$cli);
		$num = count($seleccionados);
		$clientes = array();
		for($i = 0; $i < $num; $i++)
		{
			if($seleccionados[$i] != '')
			{
				$clientes[$i]=$seleccionados[$i];
			}
		}
		$listaclientes=implode(',',$clientes);
				
		$sql = '';
		if($sql == ''){ if($listaclientes <> ''){ $sql = 'hd.cli_clave_int in ('.$listaclientes.')'; } }else{ if($listaclientes <> ''){ $sql .= ' and hd.cli_clave_int in ('.$listaclientes.')'; } }
		if($sql == ''){ if($listaemplados <> ''){ $sql = 'hh.usu_clave_int in ('.$listaemplados.')'; } }else{ if($listaemplados <> ''){ $sql .= ' and hh.usu_clave_int in ('.$listaemplados.')'; } }
		if($sql == ''){ $sql = "(hd.hod_actividad LIKE REPLACE('%".$act."%',' ','%') OR '".$act."' IS NULL OR '".$act."' = '')"; }else{ $sql .= " and (hd.hod_actividad LIKE REPLACE('%".$act."%',' ','%') OR '".$act."' IS NULL OR '".$act."' = '')"; }
		if($sql == ''){ $sql = "((hh.hoh_fecha = '".$fi."' OR '".$fi."' IS NULL OR '".$fi."' = '') or ((hh.hoh_fecha BETWEEN '".$fi." 00:00:00' AND '".$ff." 23:59:59') or ('".$fi."' Is Null and '".$ff."' Is Null) or ('".$fi."' = '' and '".$ff."' = '')))"; }else{ $sql .= " and ((hh.hoh_fecha = '".$fi."' OR '".$fi."' IS NULL OR '".$fi."' = '') or ((hh.hoh_fecha BETWEEN '".$fi." 00:00:00' AND '".$ff." 23:59:59') or ('".$fi."' Is Null and '".$ff."' Is Null) or ('".$fi."' = '' and '".$ff."' = '')))"; }
		
		$con = mysqli_query($conectar,"select hd.hod_actividad,CASE length(hd.hod_fila) WHEN 2 THEN substring(hd.hod_fila,1,1) WHEN 3 THEN substring(hd.hod_fila,1,1) WHEN 4 THEN substring(hd.hod_fila,1,2) END hi,CASE length(hd.hod_fila) WHEN 2 THEN substring(hd.hod_fila,2,1) WHEN 3 THEN substring(hd.hod_fila,2,2) WHEN 4 THEN substring(hd.hod_fila,3,2) END hf, c.cli_nombre from horas_h hh inner join horas_d hd on (hd.hoh_clave_int = hh.hoh_clave_int) inner join usuario u on (u.usu_clave_int = hh.usu_clave_int) inner join cliente c on (c.cli_clave_int = hd.cli_clave_int) where u.usu_clave_int = '".$clausu."' and hh.hoh_fecha = '".$fec."' and hd.pro_clave_int = '".$pro."' and ".$sql."");
		$num = mysqli_num_rows($con);
		for($i = 0; $i < $num; $i++)
		{
			$dato = mysqli_fetch_array($con);
			$act = $dato['hod_actividad'];
			$hi = $dato['hi'];
			$hf = $dato['hf'];
			$cli = $dato['cli_nombre'];
			?>
			<table style="width: 100%">
				<tr>
					<td style="width: 87px"><strong>ACTIVIDAD:</strong></td>
					<td style="width: 218px"><?php echo wordwrap($act, 30, "<br>", true); ?></td>
					<td style="width: 27px"><strong>HORA:</strong></td>
					<td style="width: 100px"><?php echo "DE ".$hi." A ".$hf; ?></td>
					<td style="width: 46px"><strong>CLIENTE:</strong></td>
					<td><?php echo $cli; ?></td>
				</tr>
			</table>
			<?php
		}
		exit();
	}
	if($_GET['verproyectos'] == 'si')
	{
		$clausu = $_GET['clausu'];
		$cli = $_GET['cli'];
		$pro = $_GET['pro'];
		$act = $_GET['act'];
		$fi = $_GET['fi'];
		$ff = $_GET['ff'];
				
		$seleccionados = explode(',',$cli);
		$num = count($seleccionados);
		$clientes = array();
		for($i = 0; $i < $num; $i++)
		{
			if($seleccionados[$i] != '')
			{
				$clientes[$i]=$seleccionados[$i];
			}
		}
		$listaclientes=implode(',',$clientes);
		
		$seleccionados = explode(',',$pro);
		$num = count($seleccionados);
		$proyectos = array();
		for($i = 0; $i < $num; $i++)
		{
			if($seleccionados[$i] != '')
			{
				$proyectos[$i]=$seleccionados[$i];
			}
		}
		$listaproyectos=implode(',',$proyectos);
				
		$sql = '';
		if($sql == ''){ if($listaproyectos <> ''){ $sql = 'p.pro_clave_int in ('.$listaproyectos.')'; } }else{ if($listaproyectos <> ''){ $sql .= ' and p.pro_clave_int in ('.$listaproyectos.')'; } }
		if($sql == ''){ if($listaclientes <> ''){ $sql = 'hd.usu_clave_int in ('.$listaclientes.')'; } }else{ if($listaclientes <> ''){ $sql .= ' and hd.usu_clave_int in ('.$listaclientes.')'; } }
		if($sql == ''){ $sql = "(hd.hod_actividad LIKE REPLACE('%".$act."%',' ','%') OR '".$act."' IS NULL OR '".$act."' = '')"; }else{ $sql .= " and (hd.hod_actividad LIKE REPLACE('%".$act."%',' ','%') OR '".$act."' IS NULL OR '".$act."' = '')"; }
		if($sql == ''){ $sql = "((hh.hoh_fecha = '".$fi."' OR '".$fi."' IS NULL OR '".$fi."' = '') or ((hh.hoh_fecha BETWEEN '".$fi." 00:00:00' AND '".$ff." 23:59:59') or ('".$fi."' Is Null and '".$ff."' Is Null) or ('".$fi."' = '' and '".$ff."' = '')))"; }else{ $sql .= " and ((hh.hoh_fecha = '".$fi."' OR '".$fi."' IS NULL OR '".$fi."' = '') or ((hh.hoh_fecha BETWEEN '".$fi." 00:00:00' AND '".$ff." 23:59:59') or ('".$fi."' Is Null and '".$ff."' Is Null) or ('".$fi."' = '' and '".$ff."' = '')))"; }
		
		$con = mysqli_query($conectar,"select p.pro_clave_int,p.pro_nombre,r.reg_nombre from horas_h hh inner join horas_d hd on (hd.hoh_clave_int = hh.hoh_clave_int) inner join usuario u on (u.usu_clave_int = hh.usu_clave_int) inner join proyecto p on (p.pro_clave_int = hd.pro_clave_int) left outer join region r on r.reg_clave_int = p.reg_clave_int where hh.usu_clave_int = '".$clausu."' and ".$sql." GROUP BY p.pro_clave_int");

	
		$num = mysqli_num_rows($con);
		for($i = 0; $i < $num; $i++)
		{
			$dato = mysqli_fetch_array($con);
			$clapro = $dato['pro_clave_int'];
			$pro = $dato['pro_nombre'];
			$reg = $dato['reg_nombre'];
		?>
	        <li style="padding-top:5px; float:left; width:95%;">
			<div style="float:left">
				<img alt="" class="expand" id="expand" src="../../images/Minus.png" />
				<img alt="" class="collapse" id="collapse" onclick="VERFECHAS('<?php echo $clausu; ?>','<?php echo $i; ?>','<?php echo $clapro; ?>')" src="../../images/Plus.png" />
			</div>
			<div style="<?php if($i % 2 == 0){ echo 'background-color:#B8CEB8"'; }else{ echo 'background-color:#E1E5E1"'; } ?>;float:left; width:95%; text-align:left" class="bordes">
			&nbsp;&nbsp;<div style="float:left"><strong>REGION:</strong> <?php echo strtoupper($reg); ?>  <strong>PROYECTO:</strong> <?php echo strtoupper($pro); ?></div>
			<div style="float:left;text-align:right;font-size:small">
			&nbsp;
			<?php
			$sum = 0;
			$hor = 0;
			$con1 = mysqli_query($conectar,"select CASE hd.fac_clave_int WHEN 1 THEN hd.hod_vr_hora*hd.hod_fac_prestacional ELSE hd.hod_vr_hora END valor from horas_h hh inner join horas_d hd on (hd.hoh_clave_int = hh.hoh_clave_int) inner join proyecto p on (p.pro_clave_int = hd.pro_clave_int) inner join usuario u on (u.usu_clave_int = hh.usu_clave_int) where hh.usu_clave_int = '".$clausu."' and p.pro_clave_int = '".$clapro."' and ".$sql."");
			$num1 = mysqli_num_rows($con1);
			for($j = 0; $j < $num1; $j++)
			{
				$dato1 = mysqli_fetch_array($con1);
				$valor = $dato1['valor'];
				$sum = $sum+$valor;
				$hor = $hor+1;
			}
			echo "<span  style='visibility:".$vt."'><strong>TOTAL HORAS: </strong>".$hor."</span><span  style='visibility:".$vc."'> <strong>VR. TOTAL:</strong> $".number_format($sum,0 , "," ,".")."</span>";
			?>
			</div>
			</div>
			<ul style="display:none; list-style:none; float:left; width:100%">
	        <div id="fechas<?php echo $clausu."".$i."".$clapro; ?>">
	        
	        </div>
	        </ul>
	        </li>
	    <?php
	    }
		exit();
	}
	if($_GET['mostrartotalhoras'] == 'si')
	{
		$emp = $_GET['emp'];
		$cli = $_GET['cli'];
		$pro = $_GET['pro'];
		$act = $_GET['act'];
		$fi = $_GET['fi'];
		$ff = $_GET['ff'];
		$ti = $_GET['ti'];
		
		$seleccionados = explode(',',$emp);
		$num = count($seleccionados);
		$empleados = array();
		for($i = 0; $i < $num; $i++)
		{
			if($seleccionados[$i] != '')
			{
				$empleados[$i]=$seleccionados[$i];
			}
		}
		$listaemplados=implode(',',$empleados);
		
		$seleccionados = explode(',',$cli);
		$num = count($seleccionados);
		$clientes = array();
		for($i = 0; $i < $num; $i++)
		{
			if($seleccionados[$i] != '')
			{
				$clientes[$i]=$seleccionados[$i];
			}
		}
		$listaclientes=implode(',',$clientes);
		
		$seleccionados = explode(',',$pro);
		$num = count($seleccionados);
		$proyectos = array();
		for($i = 0; $i < $num; $i++)
		{
			if($seleccionados[$i] != '')
			{
				$proyectos[$i]=$seleccionados[$i];
			}
		}
		$listaproyectos=implode(',',$proyectos);
		
		if($pro <= 0)
		{
			$conpro = mysqli_query($conectar,"select pro_clave_int from proyecto where UPPER(pro_nombre) = UPPER('".$pro."')");
			$datopro = mysqli_fetch_array($conpro);
			$listaproyectos = $datopro['pro_clave_int'];
		}
		
		$sql = '';
		if($sql == ''){ if($listaproyectos <> ''){ $sql = 'p.pro_clave_int in ('.$listaproyectos.')'; } }else{ if($listaproyectos <> ''){ $sql .= ' and p.pro_clave_int in ('.$listaproyectos.')'; } }
		if($sql == ''){ if($listaclientes <> ''){ $sql = 'hd.cli_clave_int in ('.$listaclientes.')'; } }else{ if($listaclientes <> ''){ $sql .= ' and hd.cli_clave_int in ('.$listaclientes.')'; } }
		if($sql == ''){ if($listaemplados <> ''){ $sql = 'hh.usu_clave_int in ('.$listaemplados.')'; } }else{ if($listaemplados <> ''){ $sql .= ' and hh.usu_clave_int in ('.$listaemplados.')'; } }
		if($sql == ''){ $sql = "(hd.hod_actividad LIKE REPLACE('%".$act."%',' ','%') OR '".$act."' IS NULL OR '".$act."' = '')"; }else{ $sql .= " and (hd.hod_actividad LIKE REPLACE('%".$act."%',' ','%') OR '".$act."' IS NULL OR '".$act."' = '')"; }
		if($sql == ''){ $sql = "((hh.hoh_fecha = '".$fi."' OR '".$fi."' IS NULL OR '".$fi."' = '') or ((hh.hoh_fecha BETWEEN '".$fi." 00:00:00' AND '".$ff." 23:59:59') or ('".$fi."' Is Null and '".$ff."' Is Null) or ('".$fi."' = '' and '".$ff."' = '')))"; }else{ $sql .= " and ((hh.hoh_fecha = '".$fi."' OR '".$fi."' IS NULL OR '".$fi."' = '') or ((hh.hoh_fecha BETWEEN '".$fi." 00:00:00' AND '".$ff." 23:59:59') or ('".$fi."' Is Null and '".$ff."' Is Null) or ('".$fi."' = '' and '".$ff."' = '')))"; }
		
		$con = mysqli_query($conectar,"select * from horas_h hh inner join horas_d hd on (hd.hoh_clave_int = hh.hoh_clave_int) inner join usuario u on (u.usu_clave_int = hh.usu_clave_int) inner join proyecto p on (p.pro_clave_int = hd.pro_clave_int) where ".$sql."");
		$num = mysqli_num_rows($con);
		echo "<div style='background-color:#5A825A;color:white;text-align:center; visibility:".$vt."' class='inputs'>TOTAL HORAS: ".$num."</div>";
		exit();
	}
?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>SEGUIMIENTO CAJEROS</title>
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />

<?php //VALIDACIONES ?>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="llamadas3.js?<?php echo time();?>"></script>

<script>
function OCULTARSCROLL()
{
	parent.autoResize('iframe6');
	setTimeout("parent.autoResize('iframe6')",500);
	setTimeout("parent.autoResize('iframe6')",1000);
	setTimeout("parent.autoResize('iframe6')",1500);
	setTimeout("parent.autoResize('iframe6')",2000);
	setTimeout("parent.autoResize('iframe6')",2500);
	setTimeout("parent.autoResize('iframe6')",3000);
	setTimeout("parent.autoResize('iframe6')",3500);
	setTimeout("parent.autoResize('iframe6')",4000);
	setTimeout("parent.autoResize('iframe6')",4500);
	setTimeout("parent.autoResize('iframe6')",5000);
	setTimeout("parent.autoResize('iframe6')",5500);
	setTimeout("parent.autoResize('iframe6')",6000);
	setTimeout("parent.autoResize('iframe6')",6500);
	setTimeout("parent.autoResize('iframe6')",7000);
	setTimeout("parent.autoResize('iframe6')",7500);
	setTimeout("parent.autoResize('iframe6')",8000);
	setTimeout("parent.autoResize('iframe6')",8500);
	setTimeout("parent.autoResize('iframe6')",9000);
	setTimeout("parent.autoResize('iframe6')",9500);
	setTimeout("parent.autoResize('iframe6')",10000);
}
parent.autoResize('iframe6');
setTimeout("parent.autoResize('iframe6')",500);
setTimeout("parent.autoResize('iframe6')",1000);
setTimeout("parent.autoResize('iframe6')",1500);
setTimeout("parent.autoResize('iframe6')",2000);
setTimeout("parent.autoResize('iframe6')",2500);
setTimeout("parent.autoResize('iframe6')",3000);
setTimeout("parent.autoResize('iframe6')",3500);
setTimeout("parent.autoResize('iframe6')",4000);
setTimeout("parent.autoResize('iframe6')",4500);
setTimeout("parent.autoResize('iframe6')",5000);
setTimeout("parent.autoResize('iframe6')",5500);
setTimeout("parent.autoResize('iframe6')",6000);
setTimeout("parent.autoResize('iframe6')",6500);
setTimeout("parent.autoResize('iframe6')",7000);
setTimeout("parent.autoResize('iframe6')",7500);
setTimeout("parent.autoResize('iframe6')",8000);
setTimeout("parent.autoResize('iframe6')",8500);
setTimeout("parent.autoResize('iframe6')",9000);
setTimeout("parent.autoResize('iframe6')",9500);
setTimeout("parent.autoResize('iframe6')",10000);
</script>
<script type="text/javascript">
// Load the Visualization API and the piechart package.
google.load('visualization', '1', {'packages':['corechart']});

function torta() 
{
	document.getElementById("filtrobusqueda").style.display = "block";
  	var empleados = "";
	var objCBarray = document.getElementsByName('multiselect_busempleado');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	empleados += objCBarray[i].value + ",";
	    }
	}
	
	var clientes = "";
	var objCBarray = document.getElementsByName('multiselect_buscliente');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	clientes += objCBarray[i].value + ",";
	    }
	}
	
	var proyectos = "";
	var objCBarray = document.getElementsByName('multiselect_busproyecto');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	proyectos += objCBarray[i].value + ",";
	    }
	}
	
	var act = form1.actividad.value;
	var fi = form1.fechainicial.value;
	var ff = form1.fechafinal.value;
	var ti = form1.tipoinforme.value;
	
	MOSTRARTOTALHORAS();
	
  var jsonData = $.ajax({
      url: "jsoninforme.php?emp="+empleados+"&cli="+clientes+"&pro="+proyectos+"&act="+act+"&fi="+fi+"&ff="+ff,
      dataType:"json",
      async: false
      }).responseText;

  // Create our data table out of JSON data loaded from server.
  var data = new google.visualization.DataTable(jsonData);

  var options = {
    title: 'Informe resumen por proyecto',
    width: 1000, 
    height: 500,
    hAxis: {title: 'Resumen por proyecto', titleTextStyle: {color: 'blue'}}
  };

  var chart = new google.visualization.PieChart(document.getElementById('busqueda'));

  function selectHandler() {
      var selectedItem = chart.getSelection()[0];
      if (selectedItem) {
        var topping = data.getValue(selectedItem.row, 0);
        //alert('Registro Seleccionado: ' + topping);
        BUSCAR1(topping);
        OCULTARSCROLL();
      }
    }
    google.visualization.events.addListener(chart, 'select', selectHandler);    
  	chart.draw(data, options);
  	OCULTARSCROLL();
}
function barras() 
{
	document.getElementById("filtrobusqueda").style.display = "block";
  	var empleados = "";
	var objCBarray = document.getElementsByName('multiselect_busempleado');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	empleados += objCBarray[i].value + ",";
	    }
	}
	
	var clientes = "";
	var objCBarray = document.getElementsByName('multiselect_buscliente');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	clientes += objCBarray[i].value + ",";
	    }
	}
	
	var proyectos = "";
	var objCBarray = document.getElementsByName('multiselect_busproyecto');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	proyectos += objCBarray[i].value + ",";
	    }
	}
	
	var act = form1.actividad.value;
	var fi = form1.fechainicial.value;
	var ff = form1.fechafinal.value;
	var ti = form1.tipoinforme.value;
	
  var jsonData = $.ajax({
      url: "jsoninforme.php?emp="+empleados+"&cli="+clientes+"&pro="+proyectos+"&act="+act+"&fi="+fi+"&ff="+ff,
      dataType:"json",
      async: false
      }).responseText;

  // Create our data table out of JSON data loaded from server.
  var data = new google.visualization.DataTable(jsonData);

  var options = {
    title: 'Informe resumen por proyecto',
    width: 1000, 
    height: 500,
    hAxis: {title: 'Resumen por proyecto', titleTextStyle: {color: 'blue'}}
  };

  var chart = new google.visualization.BarChart(document.getElementById('busqueda'));
  function selectHandler() {
      var selectedItem = chart.getSelection()[0];
      if (selectedItem) {
        var topping = data.getValue(selectedItem.row, 0);
        //alert('Registro Seleccionado: ' + topping);
        BUSCAR1(topping);
        OCULTARSCROLL();
      }
    }
    google.visualization.events.addListener(chart, 'select', selectHandler);
    chart.draw(data, options);
    OCULTARSCROLL();
}
function lineal() 
{
	document.getElementById("filtrobusqueda").style.display = "block";
  	var empleados = "";
	var objCBarray = document.getElementsByName('multiselect_busempleado');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	empleados += objCBarray[i].value + ",";
	    }
	}
	
	var clientes = "";
	var objCBarray = document.getElementsByName('multiselect_buscliente');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	clientes += objCBarray[i].value + ",";
	    }
	}
	
	var proyectos = "";
	var objCBarray = document.getElementsByName('multiselect_busproyecto');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	proyectos += objCBarray[i].value + ",";
	    }
	}
	
	var act = form1.actividad.value;
	var fi = form1.fechainicial.value;
	var ff = form1.fechafinal.value;
	var ti = form1.tipoinforme.value;
	
  var jsonData = $.ajax({
      url: "jsoninforme.php?emp="+empleados+"&cli="+clientes+"&pro="+proyectos+"&act="+act+"&fi="+fi+"&ff="+ff,
      dataType:"json",
      async: false
      }).responseText;

  // Create our data table out of JSON data loaded from server.
  var data = new google.visualization.DataTable(jsonData);

  var options = {
    title: 'Informe resumen por proyecto',
    width: 1000, 
    height: 500,
    hAxis: {title: 'Resumen por proyecto', titleTextStyle: {color: 'blue'}}
  };

  var chart = new google.visualization.LineChart(document.getElementById('busqueda'));
	function selectHandler() {
      var selectedItem = chart.getSelection()[0];
      if (selectedItem) {
        var topping = data.getValue(selectedItem.row, 0);
        //alert('Registro Seleccionado: ' + topping);
        BUSCAR1(topping);
        OCULTARSCROLL();
      }
    }
    google.visualization.events.addListener(chart, 'select', selectHandler);
  chart.draw(data, options);
  OCULTARSCROLL();
}
</script>
    
<?php //********ESTAS LIBRERIAS JS Y CSS SIRVEN PARA HACER LA BUSQUEDA DINAMICA CON CHECKLIST************//?>
<link rel="stylesheet" type="text/css" href="../../css/checklist/jquery.multiselect.css" />
<link rel="stylesheet" type="text/css" href="../../css/checklist/jquery.multiselect.filter.css" />
<link rel="stylesheet" type="text/css" href="../../css/checklist/styleselect.css" />
<link rel="stylesheet" type="text/css" href="../../css/checklist/prettify.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script type="text/javascript" src="../../js/checklist/jquery.multiselect.js"></script>
<script type="text/javascript" src="../../js/checklist/jquery.multiselect.filter.js"></script>
<script type="text/javascript" src="../../js/checklist/prettify.js"></script>
<?php //**************************************************************************************************//?>

</head>
<body style="background-color:#FDFDFC" id="content">
<form name="form1" id="form1" method="post">
<!--[if lte IE 7]>
<div class="ieWarning">Este navegador no es compatible con el sistema. Por favor, use Chrome, Safari, Firefox o Internet Explorer 8 o superior.</div>
<![endif]-->
	<input name="ocultoestado" id="ocultoestado" value="" type="hidden" />
	<table style="width: 100%">
		<tr>
			<td class="auto-style2" colspan="5" align="center">
			<div id="cajeros">
				<table style="width:100%">
			<tr>
				<td align="center" class="auto-style2">
					<table style="width: 85%" align="center">
						<tr>
							<td style="width: 150px; height: 31px" class="alinearizq">
							<strong>Usuario:</strong></td>
							<td style="height: 31px; " colspan="2" class="alinearizq">
							<strong>Cliente:</strong></td>
							<td style="width: 50px; height: 31px" class="alinearizq">
							<strong>Proyecto:</strong></td>
							<td style="width: 150px; height: 31px" class="alinearizq">&nbsp;
							</td>
							<td style="width: 150px;text-align:center" rowspan="4" align="center">
							<img src="../../images/buscarverde.png" height="140" width="140" align="center">
							</td>
						</tr>
						<tr>
							<td style="width: 150px; height: 31px;" class="alinearizq">
							<select multiple="multiple" <?php if(strtoupper($perfil) == strtoupper('Empleado') or strtoupper($perfil) == strtoupper('Empleados')){ echo 'disabled="disabled"'; } ?> onchange="BUSCAR()" name="busempleado" id="busempleado" style="width:320px">
							<?php
								if(strtoupper($perfil) == strtoupper('Empleado') or strtoupper($perfil) == strtoupper('Empleados'))
								{
									if($tipoperfil=="4")
									{
										$con = mysqli_query($conectar,"select usu_clave_int,usu_nombre,usu_cedula from usuario where usu_clave_int  in(select usu_clave_int_asig from usuario u join usuarios_asig a on a.usu_clave_int_asig = u.usu_clave_int where a.usu_clave_int  = '".$clausu."') or usu_clave_int='".$clausu."'");
									}
									else
									{
									$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."' order by u.usu_nombre");
									}
								}
								else
								{
									if($tipoperfil=="3" || $tipoperfil=="4")
									{
										$con = mysqli_query($conectar,"select usu_clave_int,usu_nombre,usu_cedula from usuario where usu_clave_int  in(select usu_clave_int_asig from usuario u join usuarios_asig a on a.usu_clave_int_asig = u.usu_clave_int where a.usu_clave_int  = '".$clausu."') or usu_clave_int='".$clausu."'");
									}
									else
									{
										$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) order by u.usu_nombre");
									}
								}
								$num = mysqli_num_rows($con);
								for($i = 1; $i <= $num; $i++)
								{
									$dato = mysqli_fetch_array($con);
									$clave = $dato['usu_clave_int'];
									$nombre = $dato['usu_nombre'];
									$ced = $dato['usu_cedula'];
							?>
								<option value="<?php echo $clave; ?>" <?php if(strtoupper($perfil) == strtoupper('Empleado') or strtoupper($perfil) == strtoupper('Empleados')){ echo 'selected="selected"'; } ?>><?php echo $ced." - ".$nombre; ?></option>
							<?php
								}
							?>
							</select>
							</td>
							<td style="height: 31px;" colspan="2" class="alinearizq">
							<select multiple="multiple" onchange="BUSCAR()" name="buscliente" id="buscliente" style="width:320px">
							<?php
								$con = mysqli_query($conectar,"select * from cliente where cli_sw_activo = 1 order by cli_nombre");
								$num = mysqli_num_rows($con);
								for($j = 0; $j < $num; $j++)
								{
									$dato = mysqli_fetch_array($con);
									$clave = $dato['cli_clave_int'];
									$nombre = $dato['cli_nombre'];
							?>
								<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
							<?php
								}
							?>
							</select>
							</td>
							<td style="height: 31px;" class="alinearizq" colspan="2">
							<select multiple="multiple" onchange="BUSCAR()" name="busproyecto" id="busproyecto" style="width:320px">
							<?php
								$con = mysqli_query($conectar,"select * from proyecto where pro_sw_activo = 1 order by pro_nombre");
								$num = mysqli_num_rows($con);
								for($j = 0; $j < $num; $j++)
								{
									$dato = mysqli_fetch_array($con);
									$clave = $dato['pro_clave_int'];
									$nombre = $dato['pro_nombre'];
									$codigo = $dato['pro_codigo'];
							?>
								<option value="<?php echo $clave; ?>" <?php if($pro == $clave){ echo 'selected="selected"'; } ?>><?php echo $codigo." - ".$nombre; ?></option>
							<?php
								}
							?>
							</select>
							</td>
						</tr>
						<tr>
							<td style="width: 150px; height: 31px;" class="alinearizq">
							<input name="actividad" id="actividad" onkeyup="BUSCAR()" class="inputs" style="width:310px" placeholder="Actividad" type="text"></td>
							<td style="width: 50px; height: 31px;" class="alinearizq">
							
							<strong>Fecha Inicial:</strong></td>
							<td style="width: 150px; height: 31px;">
							<input class="inputs" name="fechainicial" id="fechainicial" onchange="BUSCAR()" maxlength="70" type="date" style="width: 96%" />
							</td>
							<td style="width: 50px; height: 31px;" class="alinearizq">
							<strong>Fecha Final:</strong></td>
							<td style="width: 150px; height: 31px;" class="alinearizq">
							<input class="inputs" name="fechafinal" id="fechafinal" onchange="BUSCAR()" maxlength="70" type="date" style="width: 96%" />
							</td>
						</tr>
						<tr>
							<td style="height: 31px;" class="alinearizq" colspan="5">
							<table style="width: 100%">
								<tr>
									<td><input name="tipoinforme" id="tipoinforme" value="1" onclick="BUSCAR()" type="radio" /><label for="tipoinforme" style="cursor:pointer">Resumen por proyecto</label></td>
									<td>
									<input name="tipoinforme" id="tipoinforme1" value="2" onclick="BUSCAR()" type="radio" /><label for="tipoinforme1" style="cursor:pointer">Resumen por usuario</label>
									</td>
									<td>
									<input style='visibility:<?php echo $vc;?>' name="tipoinforme" id="tipoinforme2" value="3" onclick="torta()" type="radio" />
									<label style='visibility:<?php echo $vc;?>' for="tipoinforme2" style="cursor:pointer">Informe Gráfico</label>
									</td>
								</tr>
							</table>
							</td>
						</tr>
						</table>
				</td>
			</tr>
			<tr>
				<td class="auto-style2">
				<div id="filtrobusqueda" style="display:none">
				<table style="width: 100%">
					<tr>
						<td class="auto-style1">
						<input name="Button4" onclick="barras()" type="button" value="Informe Barras" style="width: 160px" /></td>
						<td class="auto-style1">
						<input name="Button3" onclick="lineal()" type="button" value="Inf. Lineas de Tendencia" style="width: 160px" /></td>
						<td class="auto-style1">
						<input name="Button2" onclick="torta()" type="button" value="Informe En Torta" style="width: 160px" /></td>
					</tr>
				</table>
				</div>
				<div id="totalhoras">
				</div>
				<div id="busqueda">
				</div>
				<div id="busquedagrafica">
				</div>
			</td>
			</tr>
			</table>		
			</div>
			</td>
		</tr>
		<tr>
			<td style="width: 100px">&nbsp;</td>
			<td style="width: 100px">&nbsp;</td>
			<td style="width: 100px">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
<script type="text/javascript" language="javascript">
    $("#busqueda").on("click",".expand",function(){
        $(this).toggle();
        $(this).next().toggle();
        $(this).parent().parent().children().last().toggle();
    });
    $("#busqueda").on("click",".collapse",function(){
        $(this).toggle();
        $(this).prev().toggle();
        $(this).parent().parent().children().last().toggle();
    });
    
	$("#busquedagrafica").on("click",".expand",function(){
        $(this).toggle();
        $(this).next().toggle();
        $(this).parent().parent().children().last().toggle();
    });
    $("#busquedagrafica").on("click",".collapse",function(){
        $(this).toggle();
        $(this).prev().toggle();
        $(this).parent().parent().children().last().toggle();
    });
    $("#busempleado").multiselect().multiselectfilter();
    $("#buscliente").multiselect().multiselectfilter();
    $("#busproyecto").multiselect().multiselectfilter();
</script>
</form>
</body>
</html>