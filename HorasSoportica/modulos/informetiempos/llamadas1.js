function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function MODULO(v,e)
{	
	if(v == 'CAJEROS')
	{
		window.location.href = "cajeros.php";
	}
	else
	if(v == 'TODOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('cajeros').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#cajeros").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?todos=si&est="+e,true);
		ajax.send(null);
		setTimeout("REFRESCARLISTAS()",500);
	}
	OCULTARSCROLL();
}
function BUSCAR(v,p)
{	
	var empleados = "";
	var objCBarray = document.getElementsByName('multiselect_busempleado');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	empleados += objCBarray[i].value + ",";
	    }
	}
	
	var clientes = "";
	var objCBarray = document.getElementsByName('multiselect_buscliente');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	clientes += objCBarray[i].value + ",";
	    }
	}
	
	var proyectos = "";
	var objCBarray = document.getElementsByName('multiselect_busproyecto');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	proyectos += objCBarray[i].value + ",";
	    }
	}
	
	var act = form1.actividad.value;
	var fi = form1.fechainicial.value;
	var ff = form1.fechafinal.value;
	var ti = form1.tipoinforme.value;
	if(ti == 3)
	{
		document.getElementById("filtrobusqueda").style.display = "block";
		torta();
	}
	else
	{
		jQuery("#busquedagrafica").html("");
		document.getElementById("filtrobusqueda").style.display = "none";
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('busqueda').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#busqueda").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='50' width='50' />"); //loading gif will be overwrited when ajax have success
		if(p > 0)
		{
			ajax.open("GET","?buscar=si&emp="+empleados+"&cli="+clientes+"&pro="+proyectos+"&act="+act+"&fi="+fi+"&ff="+ff+"&ti="+ti+"&page="+p,true);
		}
		else
		{
			ajax.open("GET","?buscar=si&emp="+empleados+"&cli="+clientes+"&pro="+proyectos+"&act="+act+"&fi="+fi+"&ff="+ff+"&ti="+ti,true);
		}
		ajax.send(null);
		OCULTARSCROLL();
		MOSTRARTOTALHORAS();
	}
}
function BUSCAR1(proyectos)
{	
	var empleados = "";
	var objCBarray = document.getElementsByName('multiselect_busempleado');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	empleados += objCBarray[i].value + ",";
	    }
	}
	
	var clientes = "";
	var objCBarray = document.getElementsByName('multiselect_buscliente');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	clientes += objCBarray[i].value + ",";
	    }
	}
	
	var act = form1.actividad.value;
	var fi = form1.fechainicial.value;
	var ff = form1.fechafinal.value;
	var ti = form1.tipoinforme.value;
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('busquedagrafica').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#busquedagrafica").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='50' width='50' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?buscar=si&emp="+empleados+"&cli="+clientes+"&pro="+proyectos+"&act="+act+"&fi="+fi+"&ff="+ff+"&ti=1",true);
	ajax.send(null);
	OCULTARSCROLL();
	MOSTRARTOTALHORAS();
}
function EXPORTARRESUMEN()
{
	var empleados = "";
	var objCBarray = document.getElementsByName('multiselect_busempleado');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	empleados += objCBarray[i].value + ",";
	    }
	}
	
	var clientes = "";
	var objCBarray = document.getElementsByName('multiselect_buscliente');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	clientes += objCBarray[i].value + ",";
	    }
	}
	
	var proyectos = "";
	var objCBarray = document.getElementsByName('multiselect_busproyecto');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	proyectos += objCBarray[i].value + ",";
	    }
	}
	
	var act = form1.actividad.value;
	var fi = form1.fechainicial.value;
	var ff = form1.fechafinal.value;
	var ti = form1.tipoinforme.value;
	
	window.location.href = "informes/informeexcelresumen.php?emp="+empleados+"&cli="+clientes+"&pro="+proyectos+"&act="+act+"&fi="+fi+"&ff="+ff;
}
function EXPORTARDETALLE()
{
	var empleados = "";
	var objCBarray = document.getElementsByName('multiselect_busempleado');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	empleados += objCBarray[i].value + ",";
	    }
	}
	
	var clientes = "";
	var objCBarray = document.getElementsByName('multiselect_buscliente');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	clientes += objCBarray[i].value + ",";
	    }
	}
	
	var proyectos = "";
	var objCBarray = document.getElementsByName('multiselect_busproyecto');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	proyectos += objCBarray[i].value + ",";
	    }
	}
	
	var act = form1.actividad.value;
	var fi = form1.fechainicial.value;
	var ff = form1.fechafinal.value;
	var ti = form1.tipoinforme.value;
	
	window.location.href = "informes/informeexceldetalle.php?emp="+empleados+"&cli="+clientes+"&pro="+proyectos+"&act="+act+"&fi="+fi+"&ff="+ff;
}
function VEREMPLEADOS(v)
{	
	var empleados = "";
	var objCBarray = document.getElementsByName('multiselect_busempleado');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	empleados += objCBarray[i].value + ",";
	    }
	}
	
	var clientes = "";
	var objCBarray = document.getElementsByName('multiselect_buscliente');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	clientes += objCBarray[i].value + ",";
	    }
	}
		
	var act = form1.actividad.value;
	var fi = form1.fechainicial.value;
	var ff = form1.fechafinal.value;
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('empleados'+v).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#empleados"+v).html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?verempleados=si&pro="+v+"&emp="+empleados+"&cli="+clientes+"&act="+act+"&fi="+fi+"&ff="+ff,true);
	ajax.send(null);
	OCULTARSCROLL();
}
function VERFECHAS(v,ii,p)
{	
	var empleados = "";
	var objCBarray = document.getElementsByName('multiselect_busempleado');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	empleados += objCBarray[i].value + ",";
	    }
	}
	
	var clientes = "";
	var objCBarray = document.getElementsByName('multiselect_buscliente');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	clientes += objCBarray[i].value + ",";
	    }
	}
	
	var act = form1.actividad.value;
	var fi = form1.fechainicial.value;
	var ff = form1.fechafinal.value;
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('fechas'+v+ii+p).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#fechas"+v+ii+p).html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?verfechas=si&clausu="+v+"&pro="+p+"&emp="+empleados+"&cli="+clientes+"&act="+act+"&fi="+fi+"&ff="+ff,true);
	ajax.send(null);
	OCULTARSCROLL();
}
function VERDETALLE(v,f,p)
{
	var empleados = "";
	var objCBarray = document.getElementsByName('multiselect_busempleado');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	empleados += objCBarray[i].value + ",";
	    }
	}
	
	var clientes = "";
	var objCBarray = document.getElementsByName('multiselect_buscliente');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	clientes += objCBarray[i].value + ",";
	    }
	}
	
	var act = form1.actividad.value;
	var fi = form1.fechainicial.value;
	var ff = form1.fechafinal.value;
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('detalle'+v+f+p).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#detalle"+v+f+p).html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?verdetalle=si&clausu="+v+"&fec="+f+"&pro="+p+"&emp="+empleados+"&cli="+clientes+"&act="+act+"&fi="+fi+"&ff="+ff,true);
	ajax.send(null);
	OCULTARSCROLL();
}
function VERPROYECTOS(v,ii)
{
	var clientes = "";
	var objCBarray = document.getElementsByName('multiselect_buscliente');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	clientes += objCBarray[i].value + ",";
	    }
	}
	
	var proyectos = "";
	var objCBarray = document.getElementsByName('multiselect_busproyecto');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	proyectos += objCBarray[i].value + ",";
	    }
	}
	
	var act = form1.actividad.value;
	var fi = form1.fechainicial.value;
	var ff = form1.fechafinal.value;
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('proyecto'+v+ii).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#proyecto"+v+ii).html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?verproyectos=si&clausu="+v+"&cli="+clientes+"&pro="+proyectos+"&act="+act+"&fi="+fi+"&ff="+ff,true);
	ajax.send(null);
	OCULTARSCROLL();
}
function MOSTRARTOTALHORAS()
{
	var empleados = "";
	var objCBarray = document.getElementsByName('multiselect_busempleado');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	empleados += objCBarray[i].value + ",";
	    }
	}
	
	var clientes = "";
	var objCBarray = document.getElementsByName('multiselect_buscliente');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	clientes += objCBarray[i].value + ",";
	    }
	}
	
	var proyectos = "";
	var objCBarray = document.getElementsByName('multiselect_busproyecto');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	proyectos += objCBarray[i].value + ",";
	    }
	}
	
	var act = form1.actividad.value;
	var fi = form1.fechainicial.value;
	var ff = form1.fechafinal.value;
	var ti = form1.tipoinforme.value;
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('totalhoras').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#totalhoras").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrartotalhoras=si&emp="+empleados+"&cli="+clientes+"&pro="+proyectos+"&act="+act+"&fi="+fi+"&ff="+ff,true);
	ajax.send(null);
	OCULTARSCROLL();
}