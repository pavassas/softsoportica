function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function EDITAR(v,m)
{	
	if(m == 'CIUDAD')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('editarciudad').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#editarciudad").html("<img alt='cargando' src='images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?editarciu=si&ciuedi="+v,true);
		ajax.send(null);
	}
}
function GUARDAR(v,id)
{
	if(v == 'CIUDAD')
	{
		var ciu = form1.ciudad1.value;
		var lc = ciu.length;
		var act = form1.activo1.checked;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('datos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#datos").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?guardarciu=si&ciu="+ciu+"&act="+act+"&lc="+lc+"&c="+id,true);
		ajax.send(null);
		if(ciu != '' && lc >= 3)
		{
			setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
		}
	}
}
function NUEVO(v)
{
	if(v == 'CIUDAD')
	{
		var ciu = form1.ciudad.value;
		var lc = ciu.length;		
		var act = form1.activo.checked;
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('datos1').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#datos1").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?nuevaciudad=si&ciu="+ciu+"&act="+act+"&lc="+lc,true);
		ajax.send(null);
		if(ciu != '' && lc >= 3)
		{
			form1.ciudad.value = '';
			setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
		}
	}
}
function CONSULTAMODULO(v)
{
	if(v == 'TODOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('ciudades').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#ciudades").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?todos=si",true);
		ajax.send(null);
	}
}
function BUSCAR(m)
{	
	if(m == 'CIUDAD')
	{
		var ciu = form1.ciudad2.value;
		var act = form1.buscaractivos.value;
				
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('ciudades').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#ciudades").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?buscarciu=si&ciu="+ciu+"&act="+act,true);
		ajax.send(null);
	}
}