<?php
error_reporting(0);
include('../../data/Conexion.php');
session_start();
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_SESSION['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];

$con = mysqli_query($conectar,"select * from usuario where usu_usuario = '".$usuario."'");
$dato = mysqli_fetch_array($con);
	
// verifica si no se ha loggeado
if(!isset($_SESSION["persona"]))
{
  session_destroy();
  header("LOCATION:index.php");
}else{
}
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");
$idcats=$_POST['idcat'];
$contador=0;
if(is_array($idcats)){
        for($i=0;$i<count($idcats);$i++){
	        if($_GET['accion'] == 'Activar')
        	{
        		if($idcats[$i] <> 0)
				{
        			$con = mysqli_query($conectar,"update usuario set usu_sw_activo = 1 where usu_clave_int = '".$idcats[$i]."'");
        			if($idcats[$i] <> '' and $idcats[$i] <> 0)
        			{
						mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,1,5,'".$idcats[$i]."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 5=Activar usuario
					}
        		}
        	}
        	else
        	if($_GET['accion'] == 'Inactivar')
        	{
				if($idcats[$i] <> 0)
				{
        			$con = mysqli_query($conectar,"update usuario set usu_sw_activo = 0 where usu_clave_int = '".$idcats[$i]."'");
					if($idcats[$i] <> '' and $idcats[$i] <> 0)
        			{
						mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,1,6,'".$idcats[$i]."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 6=Inactivar usuario
					}
				}
        	}
        	else
        	if($_GET['accion'] == 'Eliminar')
        	{
				if($idcats[$i] <> 0)
				{
        			$sql = mysqli_query($conectar,"delete from usuario where usu_clave_int = '".$idcats[$i]."'");
					if($idcats[$i] <> '' and $idcats[$i] <> 0)
        			{
						mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,1,4,'".$idcats[$i]."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 4=Eliminación usuario
					}
				}
        	}
        	else
        	if($_GET['accion'] == 'Eliminar Perfil')
        	{
				if($idcats[$i] <> 0)
				{
					$con = mysqli_query($conectar,"select * from usuario where prf_clave_int='".$idcats[$i]."'");
	        		$num = mysqli_num_rows($con);
	        		if($num > 0)
	        		{
	        			$dato = mysqli_fetch_array($con);
	        			$usu = $dato['usu_usuario'];
	        			$men = $men.",".$usu;
	        		}
	        		else
	        		{
	        			//Elimino primero los permisos asignados a ese perfil
	        			$sql = mysqli_query($conectar,"delete from permiso where prf_clave_int = '".$idcats[$i]."'");
        				$sql = mysqli_query($conectar,"delete from perfil where prf_clave_int = '".$idcats[$i]."'");
						if($idcats[$i] <> '' and $idcats[$i] <> 0)
        				{
							mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,1,9,'".$idcats[$i]."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 9=Eliminación perfil
						}
        			}
				}
        	}
        	$contador++;
		}
		//echo "Se han eliminado $contador Registros de la base de datos";
		if($_GET['accion'] == 'Activar')
        {
        	echo '<script>alert("Los Usuarios seleccionados han sido Activados correctamente"); window.location.href="usuarios.php";</script>';
        }
        else
        if($_GET['accion'] == 'Inactivar')
        {
        	echo '<script>alert("Los Usuarios seleccionados han sido Inactivados correctamente"); window.location.href="usuarios.php";</script>';
        }
        else
        if($_GET['accion'] == 'Eliminar')
        {
        	echo '<script>alert("Los Usuarios seleccionados han sido Eliminados correctamente"); window.location.href="usuarios.php";</script>';
        }
        else
        if($_GET['accion'] == 'Eliminar Perfil')
        {
        	if($men != '')
    		{
    			echo '<script>alert("Los Perfiles seleccionados han sido Eliminados correctamente, excepto los Perfiles que estan asignados a los siguientes usuarios: '.$men.'"); window.location.href="usuarios.php";</script>';
    		}
    		else
    		{
        		echo '<script>alert("Los Perfiles seleccionados han sido Eliminados correctamente"); window.location.href="usuarios.php";</script>';
        	}
        }
}else{
	echo '<script>alert("No se enviaron registros para eliminar"); window.location.href="usuarios.php";</script>';
}

?>