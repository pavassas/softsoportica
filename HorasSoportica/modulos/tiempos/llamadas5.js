function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }

function NUEVO(v)
{
	//fecha actual
	
	var emp = $('#empleado').val();
	var fec = $('#fecha').val();
	
	if(fec!='')
	{
		
		var dt = new Date();
		var ano1= fec.substr(0,4);
		var mes1= fec.substr(5,2);
		var dia1= fec.substr(8);
		
		var nuevafecha1= new Date(ano1+","+mes1+","+dia1);
		var mes = dt.getMonth()+1;
		var nuevafecha2= new Date(dt.getFullYear()+","+mes+","+dt.getDate());
		var Dif= nuevafecha1.getTime() - nuevafecha2.getTime();
		var dias= Math.floor(Dif/(1000*24*60*60));
		if(dias > 1)
		{
			alert('No es posible avanzar mas de un dia');
			$('#fecha').val('')
			$('#legen').html('')  
		}
		else
		{
			$.post('fecha.php',{fecha:fec},function(data)
			{
			$('#legen').html(data)   
			})
			if(emp != '' && fec != '')
			{
		
			CRUDTIEMPOS('TIEMPOS','');
			OCULTARSCROLL();
			setTimeout("CALCULARHORAS();",1000);
			}
		}
	}
}
function CONSULTAMODULO(v)
{
	if(v == 'TODOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('tiempos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#tiempos").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?todos=si",true);
		ajax.send(null);
		setTimeout("OCULTARSCROLL();",1000);
	}
}

function VERFECHAS(v)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('fechassemana').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#fechassemana").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?calcularfechas=si&sem="+v,true);
	ajax.send(null);
}
function SUMARTOTALHORAS(val)
{
	var l = $('#lunes'+val).val();
	var m = $('#martes'+val).val();
	var x = $('#miercoles'+val).val();
	var j = $('#jueves'+val).val();
	var v = $('#viernes'+val).val();
	var s = $('#sabado'+val).val();
	var d = $('#domingo'+val).val();
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('totalhoras'+val).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#totalhoras"+val).html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?sumartotalhoras=si&l="+l+"&m="+m+"&x="+x+"&j="+j+"&v="+v+"&s="+s+"&d="+d,true);
	ajax.send(null);
}


/* Elimina la fila indicada. */ 
var borrarFila = function (indice) 
{ 
	$("#fila" + indice).fadeOut("slow"); 
}

function MOSTRARMOVIMIENTO(v)
{
	var det = jQuery('#ocultodetalle'+v).val();
    if(det == 0)
    {
    	jQuery('#ocultodetalle'+v).val('1');
    	var div = document.getElementById('verdetalle'+v);
    	div.style.display = 'block'; 
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('verdetalle'+v).innerHTML=ajax.responseText;
		    }
		}
		jQuery("#verdetalle"+v).html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?mostrarmovimiento=si&clave="+v,true);
		ajax.send(null);
	}
	else
    { 
    	jQuery('#ocultodetalle'+v).val('0');
    	var div = document.getElementById('verdetalle'+v);
    	div.style.display = 'none';
    	//jQuery("#verocultaranexo"+v).html("VER ANEXOS");
    }
}
function SIGUIENTEDIA()
{
	var emp =$('#empleado').val();
	var fec = $('#fecha').val();
	var dt = new Date();
	var ano1= fec.substr(0,4);
	var mes1= fec.substr(5,2);
	var dia1= fec.substr(8);
	
	var nuevafecha1= new Date(ano1+","+mes1+","+dia1);
	var mes = dt.getMonth()+1;
	var nuevafecha2= new Date(dt.getFullYear()+","+mes+","+dt.getDate());
	var Dif= nuevafecha1.getTime() - nuevafecha2.getTime();
	var dias= Math.floor(Dif/(1000*24*60*60));
	
	if(dias > 0)
	{
		alert('No es posible avanzar mas de un dia');
	}
	else
	{
		if(emp != '' && fec != '')
		{
			
			fec = sumafecha(2,fec)
			//alert(fec);
			$('#fecha').val(fec);
			NUEVOSIGUIENTEDIA(emp,fec);
			OCULTARSCROLL();
			setTimeout("CALCULARHORAS();",1000);
		}
		else
		{
			alert("Debe indicar el empleado y la fecha");
		}
	}
}
function NUEVOSIGUIENTEDIA(emp,fec)
{	
	if(emp != '' && fec != '')
	{
		
		$.post('fecha.php',{fecha:fec},function(data)
		{
		$('#legen').html(data)   
		})		
		CRUDTIEMPOS('TIEMPOS','');
		OCULTARSCROLL();
	}
}
function DIAANTERIOR()
{
	var emp =$('#empleado').val();
	var fec = $('#fecha').val();
	
	var dt = new Date();
	var ano1= fec.substr(0,4);
	var mes1= fec.substr(5,2);
	var dia1= fec.substr(8);
	
	var nuevafecha1= new Date(ano1+","+mes1+","+dia1);
	var mes = dt.getMonth()+1;
	var nuevafecha2= new Date(dt.getFullYear()+","+mes+","+dt.getDate());
	var Dif= nuevafecha2.getTime() - nuevafecha1.getTime();
	var dias= Math.floor(Dif/(1000*24*60*60));
	
	/*if(dias > 1)
	{
		alert('No es posible avanzar mas de dos dias');
	}
	else
	{*/
		if(emp != '' && fec != '')
		{
			
			fec = sumafecha(0,fec)
			//alert(fec);
			$('#fecha').val(fec);
			NUEVODIAANTERIOR(emp,fec);
			OCULTARSCROLL();
			setTimeout("CALCULARHORAS();",1000);
		}
		else
		{
			alert("Debe indicar el empleado y la fecha");
		}
	//}
}
function NUEVODIAANTERIOR(emp,fec)
{	
	if(emp != '' && fec != '')
	{	
	    $.post('fecha.php',{fecha:fec},function(data)
		{
		$('#legen').html(data)   
		})	
	    CRUDTIEMPOS('TIEMPOS','');
		OCULTARSCROLL();
	}
}
function AGREGAR()
{
	var ma = $('#horamax').val();
	var sum = parseInt(ma)+1;
	var fila = ma+""+sum;
	if(ma>=24){}
	else{
		
	$('#horasagregadas'+fila).css('display','block');
	$('#horamax').val(sum);
	}
    OCULTARSCROLL();
}
function AGREGARM()
{
	var ma = parseInt($('#horamin').val())-1;
	var sum = parseInt(ma)+1;
	var fila = ma+""+sum;
	if(ma<0){}
	else{
		
	$('#horasagregadas'+fila).css('display','block');
	$('#horamin').val(ma);
	}
    OCULTARSCROLL();
}
function CALCULARHORAS()
{
	var emp = form1.empleado.value;
	var fec = form1.fecha.value;
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('totalhoras').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#totalhoras").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?calcularhoras=si&emp="+emp+"&fec="+fec,true);
	ajax.send(null);
}
function MOSTRARHASTA(v)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('divhasta').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#divhasta").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarhasta=si&hor="+v,true);
	ajax.send(null);
}




function CRUDTIEMPOS(o,f)
{
   if(o=="TIEMPOS")
   {
	   var fec = $('#fecha').val();
	   var emp = $('#empleado').val();
	   $('#resultado').html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />");
	   $.post('fnTiempos.php',{opcion:o,fecha:fec,empleado:emp},
	   function(data)
	   {
		   $('#resultado').html(data);
	   });
	   
   }
   else if(o=="GUARDARDETALLE")
   {
		var cli = $('#cliente'+f).val();
		var pro = $('#proyecto'+f).val();
		var act = $('#actividad'+f).val();
		var cla = $('#clavehoja').val();
		var act = CORREGIRTEXTO(act);
		$.post('fnTiempos.php',{opcion:o,cliente:cli,proyecto:pro,actividad:act,clave:cla,fila:f},
		function(data)
		{ 
		   if(data=="ok")
		   {
			 // $('#sp'+f).html('ok');
		     // $('#sp'+f).css('color','green');
			  setTimeout("CALCULARHORAS();",1000);
		   }
		   else
		   {			 
		     // $('#sp'+f).css('color','red'); 
			 // $('#sp'+f).html('error'); 		   
		   }
		});
   
   }
   else if(o=="AGREGARMASIVO")
   {
	   var emp = $('#empleado').val();
	   var fec = $('#fecha').val();
	
		if(emp != '' && fec != '')
		{
			var cli = $('#clientemaxivo').val();
			var pro = $('#proyectomaxivo').val();
			var des = $('#desde').val();
			var has = $('#hasta').val();
			var act = $('#actividadmasivo').val();
			
			if(cli != '' && pro != '' && des != '' && has != '' && act != '')
			{
			var act = CORREGIRTEXTO(act);
			$.post('fnTiempos.php',{opcion:o,emp:emp,fec:fec,des:des,has:has,act:act,cli:cli,pro:pro},
			    function(data)
				{
					if(data=="error1")
					{
						alert("Hubo un problema al guardar los datos, por favor intentelo de nuevo");
					}
					else if(data=="error2")
					{ 
					    alert("Debe ingresar todos los datos");
					}
					else
					{
						alert("Datos Guardados Correctamente");
				   		setTimeout(CRUDTIEMPOS('TIEMPOS',''),3000);
						 
					}
				}
			);
			}
			else
			{
			alert("Debe ingresar todos los datos");
			}
		}
		else
		{
		alert("Debe indicar el empleado y la fecha");
		}  
   }
   OCULTARSCROLL();
}
function CORREGIRTEXTO(v)
{
	var res = v.replace('#','REEMPLAZARNUMERAL');
	var res = res.replace('+','REEMPLAZARMAS');
	
	return res;
}
function sumafecha(d, fecha)
{
 var Fecha = new Date();
 var sFecha = fecha || ( Fecha.getFullYear()+ "-" + (Fecha.getMonth() +1) + "-" + Fecha.getDate());
 var sep = sFecha.indexOf('/') != -1 ? '/' : '-'; 
 var aFecha = sFecha.split(sep);
 var fecha = aFecha[0]+'-'+aFecha[1]+'-'+aFecha[2];
 fecha= new Date(fecha);
 fecha.setDate(fecha.getDate()+parseInt(d));
 var anno=fecha.getFullYear();
 var mes= fecha.getMonth()+1;
 var dia= fecha.getDate();
 mes = (mes < 10) ? ("0" + mes) : mes;
 dia = (dia < 10) ? ("0" + dia) : dia;
 var fechaFinal = anno+sep+mes+sep+dia;
 return (fechaFinal);
 }
 
 function CARGARPROYECTO(f)
{
	var cli = $('#cliente'+f).val();
    var pro = $('#proyecto'+f);

  pro.find('option').remove().end().append('<option value="">Cargando...</option>').val('');
  $.post('fnTiempos.php',{opcion:'CARGARPROYECTO',cliente:cli},
  function(data)
  {
		pro.empty();  
		
		pro.append('<option value="">-Seleccione Proyecto-</option>');
		
		for (var i=0; i<data.length; i++) 
		{
		pro.append('<option value="' + data[i].id + '">' + data[i].literal + '</option>');
		}
  		//REFRESCARLISTASDE(f);
  },"json");

}

 function CARGARPROYECTOM()
{
	var cli = $('#clientemaxivo').val();
    var pro = $('#proyectomaxivo');

  pro.find('option').remove().end().append('<option value="">Cargando...</option>').val('');
  $.post('fnTiempos.php',{opcion:'CARGARPROYECTO',cliente:cli},
  function(data)
  {
		pro.empty();  
		
		pro.append('<option value="">-Seleccione Proyecto-</option>');
		
		for (var i=0; i<data.length; i++) 
		{
		pro.append('<option value="' + data[i].id + '">' + data[i].literal + '</option>');
		}
  //$("select").searchable();
  },"json");

}