<?php
	error_reporting(0);
	include('../../data/Conexion.php');
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_SESSION['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];
		
	// verifica si no se ha loggeado
	if(!isset($_SESSION["persona"]))
	{
	  session_destroy();
	  header("LOCATION:index.php");
	}else{
	}
	date_default_timezone_set('America/Bogota');
	
	$fecha=date("Y/m/d H:i:s");
	
	$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
	$dato = mysqli_fetch_array($con);
	$perfil = $dato['prf_descripcion'];
	$claveperfil = $dato['prf_clave_int'];
	$claveusuario = $dato['usu_clave_int'];
	$ultimaobra = $dato['obr_clave_int'];
	$tipoperfil = $dato['prf_tipo_perfil'];
	$visual = $dato['prf_visual'];
	if($perfil=="Administrador"){$vc = "visible"; $vt="visible";}else 
	if($visual=="1"){$vt="visible"; $vc="hidden";}else 
	if($visual==2){$vt="hidden"; $vc="visible";}else
	if($visual==3){$vt="visible";$vc = "visible"; }else{$vt="hidden";$vc="hidden";}
	
	$con = mysqli_query($conectar,"select per_metodo from permiso where prf_clave_int = '".$claveperfil."' and ven_clave_int = 6");
	$dato = mysqli_fetch_array($con);
	$metodo = $dato['per_metodo'];
	
	
?>
<?php

	if($_GET['calcularfechas'] == 'si')
	{
		$sem = $_GET['sem'];
		$year = date('Y');
		$week = $sem;
		$fechaInicioSemana  = date('Y-m-d', strtotime($year . 'W' . str_pad($week , 2, '0', STR_PAD_LEFT)));
		$fecha_domingo = date('Y-m-d', strtotime($fechaInicioSemana.' 6 day'));; //Domingo
		
		echo "<strong>Desde:</strong> ".$fechaInicioSemana." <strong>Hasta:</strong> ".$fecha_domingo;
		exit();
	}
	if($_GET['sumartotalhoras'] == 'si')
	{
		$l = $_GET['l'];
		$m = $_GET['m'];
		$x = $_GET['x'];
		$j = $_GET['j'];
		$v = $_GET['v'];
		$s = $_GET['s'];
		$d = $_GET['d'];
		if($l == ''){ $l = 0; }
		if($m == ''){ $m = 0; }
		if($x == ''){ $x = 0; }
		if($j == ''){ $j = 0; }
		if($v == ''){ $v = 0; }
		if($s == ''){ $s = 0; }
		if($d == ''){ $d = 0; }
		echo $l+$m+$x+$j+$v+$s+$d;
		exit();
	}
	$legend="";
	

	if($_GET['mostrarmovimiento'] == 'si')
	{
		$clave = $_GET['clave'];
		?>
		<table style="width: 100%">
			<tr>
				<td><strong>Fecha</strong></td>
				<td><strong>Actividad</strong></td>
				<td><strong>Cliente</strong></td>
				<td><strong>Proyecto</strong></td>
				<td style="width: 90px"><strong>Hora Inicial</strong></td>
				<td><strong>Hora Final</strong></td>
			</tr>
			<?php
			$con = mysqli_query($conectar,"select * from horas_d hd inner join horas_h hh on (hh.hoh_clave_int = hd.hoh_clave_int) inner join cliente c on (c.cli_clave_int = hd.cli_clave_int) inner join proyecto p on (p.pro_clave_int = hd.pro_clave_int) where hd.hoh_clave_int = '".$clave."'");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$fec = $dato['hoh_fecha'];
				$act = $dato['hod_actividad'];
				$cli = $dato['cli_nombre'];
				$pro = $dato['pro_nombre'];
				$hi = substr($dato['hod_fila'],0,1);
				$hf = substr($dato['hod_fila'],1,1);
			?>
			<tr>
				<td>
				<?php 
				$con1 = mysqli_query($conectar,"SELECT DATE_FORMAT('".$fec."', '%W') sem,DATE_FORMAT('".$fec."', '%d') dia,DATE_FORMAT('".$fec."', '%b') mes from usuario limit 1");
				$dato1 = mysqli_fetch_array($con1);
				$sem = $dato1['sem'];
				$dia = $dato1['dia'];
				$mes = $dato1['mes'];
				
				if($sem == 'Monday'){ $sem = 'Lunes'; }elseif($sem == 'Tuesday'){ $sem = 'Martes'; }elseif($sem == 'Wednesday'){ $sem = 'Miércoles'; }elseif($sem == 'Thursday'){ $sem = 'Jueves'; }elseif($sem == 'Friday'){ $sem = 'Viernes'; }elseif($sem == 'Saturday'){ $sem = 'Sábado'; }elseif($sem == 'Sunday'){ $sem = 'Domingo'; }
				if($mes == 'Jan'){ $mes = 'Enero'; }elseif($mes == 'Feb'){ $mes = 'Febrero'; }elseif($mes == 'Mar'){ $mes = 'Marzo'; }elseif($mes == 'Apr'){ $mes = 'Abril'; }elseif($mes == 'May'){ $mes = 'Mayo'; }elseif($mes == 'Jun'){ $mes = 'Junio'; }elseif($mes == 'Jul'){ $mes = 'Julio'; }elseif($mes == 'Aug'){ $mes = 'Agosto'; }elseif($mes == 'Sep'){ $mes = 'Septiembre'; }elseif($mes == 'Oct'){ $mes = 'Octubre'; }elseif($mes == 'Nov'){ $mes = 'Noviembre'; }elseif($mes == 'Dec'){ $mes = 'Diciembre'; }
						
				echo $sem." ".$dia." De ".$mes; 
				?></td>
				<td><?php echo $act; ?></td>
				<td><?php echo $cli; ?></td>
				<td><?php echo $pro; ?></td>
				<td style="width: 90px"><?php echo $hi; ?></td>
				<td><?php echo $hf; ?></td>
			</tr>
			<?php
			}
			?>
		</table>
		<?php
		exit();
	}
	
	
	
	if($_GET['calcularhoras'] == 'si')
	{
		$emp = $_GET['emp'];
		$fec = $_GET['fec'];
		$con = mysqli_query($conectar,"select COUNT(*) tothor from horas_h hh inner join horas_d hd on (hd.hoh_clave_int = hh.hoh_clave_int) where hh.hoh_fecha = '".$fec."' and hh.usu_clave_int = '".$emp."'");
		$dato = mysqli_fetch_array($con);
		$tothor = $dato['tothor'];
		if($tothor >= 10)
		{
		?>
			<br><label class="inputs" style="background-color:yellow"><?php echo $tothor; ?></label>
		<?php
		}
		else
		{
			echo $tothor;
		}
		exit();
	}
	if($_GET['mostrarhasta'] == 'si')
	{
		$hor = $_GET['hor'];
		?>
		<select name="hasta" id="hasta" class="inputs" style="width:90px">
		<option value="">HASTA</option>
		<?php
		for($i = $hor; $i <= 24; $i++)
		{
		?>
			<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
		<?php
		}
		?>
		</select>
		<?php
		exit();
	}
	if($_GET['mostrarhasta1'] == 'si')
	{
		$hor = $_GET['hor'];
		?>
		<select name="hasta1" id="hasta1" class="inputs" style="width:90px">
		<option value="">HASTA</option>
		<?php
		for($i = $hor; $i <= 24; $i++)
		{
		?>
			<option value="<?php echo $i; ?>" <?php if($i == 16){ echo 'selected="selected"'; } ?>><?php echo $i; ?></option>
		<?php
		}
		?>
		</select>
		<?php
		exit();
	}
	
?>
<!DOCTYPE HTML>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>CONTROL DE OBRAS</title>
<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
<!--[if lte IE 7]>
<link rel="stylesheet" href="css/ie.c32bc18afe0e2883ee4912a51f86c119.css" type="text/css" />
<![endif]-->
<script type="text/javascript" language="javascript">
	selecteds=0;
	
	function CheckUncheck(total,check){
		checkbox=null;
		for(i=1;i<=total;i++){
			checkbox=document.getElementById("idcat"+i);
			//alert(checkbox.value);
			checkbox.checked=check.checked;
		}
		
		if(check.checked){
			selecteds=total;
		}else{
			selecteds=0;
		}
		
	}
	
	function contadorVals(check){
		if(check.checked){
			selecteds=selecteds+1;
		}else{
			selecteds=selecteds-1;
		}
	}
	
	function selectedVals(){
		if(selecteds==0){
			alert("Seleccione al menos un registro.");
			return false;
		}else{
			return true;
		}
	}
</script>
<?php //VALIDACIONES ?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="llamadas6.js?<?php echo time();?>"></script>
<link rel="stylesheet" href="css/style.css" />

<?php //VENTANA EMERGENTE ?>
<link rel="stylesheet" href="../../css/reveal.css" />
<script type="text/javascript" src="../../js/jquery.reveal.js"></script>

<script type="text/javascript" src="../../js/jquery.searchabledropdown-1.0.8.min1.js"></script>

<script type="text/javascript" language="javascript">
$(document).ready(function() {
	$("#empleado").searchable();
});
function REFRESCARLISTAS()
{
	$(document).ready(function() {
		$('#clientemaxivo').searchable();
		$('#clientemaxivo1').searchable();
		$('#proyectomaxivo').searchable();
		$('#proyectomaxivo1').searchable();
		$('#regionmaxivo1').searchable();
		$('#regionmaxivo').searchable();


		$("#cliente01").searchable();
		$("#cliente12").searchable();
		$("#cliente23").searchable();
		$("#cliente34").searchable();
		$("#cliente45").searchable();
		$("#cliente56").searchable();
		$("#cliente67").searchable();
		$("#cliente78").searchable();
		$("#cliente89").searchable();
		$("#cliente910").searchable();
		$("#cliente1011").searchable();
		$("#cliente1112").searchable();
		$("#cliente1213").searchable();
		$("#cliente1314").searchable();
		$("#cliente1415").searchable();
		$("#cliente1516").searchable();
		$("#cliente1617").searchable();
		$("#cliente1718").searchable();
		$("#cliente1819").searchable();
		$("#cliente1920").searchable();
		$("#cliente2021").searchable();
		$("#cliente2122").searchable();
		$("#cliente2223").searchable();
		$("#cliente2324").searchable();
		
		$("#proyecto01").searchable();
		$("#proyecto12").searchable();
		$("#proyecto23").searchable();
		$("#proyecto34").searchable();
		$("#proyecto45").searchable();
		$("#proyecto56").searchable();
		$("#proyecto67").searchable();
		$("#proyecto78").searchable();
		$("#proyecto89").searchable();
		$("#proyecto910").searchable();
		$("#proyecto1011").searchable();
		$("#proyecto1112").searchable();
		$("#proyecto1213").searchable();
		$("#proyecto1314").searchable();
		$("#proyecto1415").searchable();
		$("#proyecto1516").searchable();
		$("#proyecto1617").searchable();
		$("#proyecto1718").searchable();
		$("#proyecto1819").searchable();
		$("#proyecto1920").searchable();
		$("#proyecto2021").searchable();
		$("#proyecto2122").searchable();
		$("#proyecto2223").searchable();
		$("#proyecto2324").searchable();

		$("#region01").searchable();
		$("#region12").searchable();
		$("#region23").searchable();
		$("#region34").searchable();
		$("#region45").searchable();
		$("#region56").searchable();
		$("#region67").searchable();
		$("#region78").searchable();
		$("#region89").searchable();
		$("#region910").searchable();
		$("#region1011").searchable();
		$("#region1112").searchable();
		$("#region1213").searchable();
		$("#region1314").searchable();
		$("#region1415").searchable();
		$("#region1516").searchable();
		$("#region1617").searchable();
		$("#region1718").searchable();
		$("#region1819").searchable();
		$("#region1920").searchable();
		$("#region2021").searchable();
		$("#region2122").searchable();
		$("#region2223").searchable();
		$("#region2324").searchable();
	});
}
function REFRESCARLISTASDE(f)
{
	$(document).ready(function() {
		$("#cliente"+f).searchable();
		
		$("#proyecto"+f).searchable();
		$("#region"+f).searchable();
	});
}
function OCULTARSCROLL()
{
	setTimeout("parent.autoResize('iframe5')",500);
	setTimeout("parent.autoResize('iframe5')",1000);
	setTimeout("parent.autoResize('iframe5')",2000);
	setTimeout("parent.autoResize('iframe5')",3000);
	setTimeout("parent.autoResize('iframe5')",4000);
	setTimeout("parent.autoResize('iframe5')",5000);
	setTimeout("parent.autoResize('iframe5')",6000);
	setTimeout("parent.autoResize('iframe5')",7000);
	setTimeout("parent.autoResize('iframe5')",8000);
	setTimeout("parent.autoResize('iframe5')",9000);
	setTimeout("parent.autoResize('iframe5')",10000);
	setTimeout("parent.autoResize('iframe5')",11000);
	setTimeout("parent.autoResize('iframe5')",12000);
	setTimeout("parent.autoResize('iframe5')",13000);
	setTimeout("parent.autoResize('iframe5')",14000);
	setTimeout("parent.autoResize('iframe5')",15000);
	setTimeout("parent.autoResize('iframe5')",16000);
	setTimeout("parent.autoResize('iframe5')",17000);
	setTimeout("parent.autoResize('iframe5')",18000);
	setTimeout("parent.autoResize('iframe5')",19000);
	setTimeout("parent.autoResize('iframe5')",20000);
}
setTimeout("parent.autoResize('iframe5')",500);
setTimeout("parent.autoResize('iframe5')",1000);
setTimeout("parent.autoResize('iframe5')",2000);
setTimeout("parent.autoResize('iframe5')",3000);
setTimeout("parent.autoResize('iframe5')",4000);
setTimeout("parent.autoResize('iframe5')",5000);
setTimeout("parent.autoResize('iframe5')",6000);
setTimeout("parent.autoResize('iframe5')",7000);
setTimeout("parent.autoResize('iframe5')",8000);
setTimeout("parent.autoResize('iframe5')",9000);
setTimeout("parent.autoResize('iframe5')",10000);
setTimeout("parent.autoResize('iframe5')",11000);
setTimeout("parent.autoResize('iframe5')",12000);
setTimeout("parent.autoResize('iframe5')",13000);
setTimeout("parent.autoResize('iframe5')",14000);
setTimeout("parent.autoResize('iframe5')",15000);
setTimeout("parent.autoResize('iframe5')",16000);
setTimeout("parent.autoResize('iframe5')",17000);
setTimeout("parent.autoResize('iframe5')",18000);
setTimeout("parent.autoResize('iframe5')",19000);
setTimeout("parent.autoResize('iframe5')",20000);
</script>

</head>
<body>
<form name="form1" id="form1" method="post">
<!--[if lte IE 7]>
<div class="ieWarning">Este navegador no es compatible con el sistema. Por favor, use Chrome, Safari, Firefox o Internet Explorer 8 o superior.</div>
<![endif]-->
<table style="width: 100%">
	<tr>
		<td class="auto-style2" onclick="parent.MODULO('TIEMPOS')" onmouseover="this.style.backgroundColor='#5A825A';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="width: 60px; cursor:pointer; display:none">
		Nuevo
		</td>
		<td class="auto-style2" onclick="CONSULTAMODULO('TODOS')" onmouseover="this.style.backgroundColor='#5A825A';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="width: 60px; cursor:pointer; display:none">
		Todos
		<?php
			/*$con = mysqli_query($conectar,"select COUNT(*) cant from horas_h where hoh_clave_int in (select hoh_clave_int from horas_d where horas_h.hoh_clave_int = horas_d.hoh_clave_int)");
			$dato = mysqli_fetch_array($con);
			echo $dato['cant'];*/
		?>
		</td>
		<td class="auto-style7" style="cursor:pointer" colspan="7">
		REGISTRO DE TIEMPOS<a data-reveal-id="nuevaciudad" data-animation="fade" style="cursor:pointer"></a></td>
	</tr>
	<tr>
		<td colspan="7" class="auto-style2">
			<div id="tiempos">
		
			<table style="width: 100%">
				<tr>
					<td style="width: 90px">
					Total Horas: 
					<div id="totalhoras">
					0
					</div>
					</td>
					<td>
					
					<table style="width: 62%" align="center">
						<tr>
							<td class="auto-style3" style="width: 100px"><strong>Día Anterior</strong></td>
							<td class="auto-style3" style="width: 60px"><strong>Cc.:</strong></td>
							<td align="left">
							<select name="empleado" id="empleado" onchange="NUEVO()" class="inputs" style="width: 300px;background:#5A825A;color:white">
							<?php
								if((strtoupper($perfil) != strtoupper('Administrador') and strtoupper($perfil) != strtoupper('Administradores') and strtoupper($perfil) != strtoupper('Admin')) or ($metodo == 0))
								{
									/*if($tipoperfil=="4")
									{
										$con = mysqli_query($conectar,"select usu_clave_int,usu_nombre,usu_cedula from usuario where usu_clave_int  in(select usu_clave_int_asig from usuario u join usuarios_asig a on a.usu_clave_int_asig = u.usu_clave_int where a.usu_clave_int  = '".$claveusuario."') or usu_clave_int='".$claveusuario."'");
									}
									else
									{*/
									$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."' order by u.usu_nombre");
									//}
								}
								else
								{
									echo '<option value="">-Seleccione-</option>';
									$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) order by u.usu_nombre");
								}
								$num = mysqli_num_rows($con);
								for($i = 1; $i <= $num; $i++)
								{
									$dato = mysqli_fetch_array($con);
									$clave = $dato['usu_clave_int'];
									$nombre = $dato['usu_nombre'];
									$ced = $dato['usu_cedula'];
							?>
								<option value="<?php echo $clave; ?>" <?php if(strtoupper($perfil) != strtoupper('Administrador') and strtoupper($perfil) != strtoupper('Administradores') and strtoupper($perfil) != strtoupper('Admin')){ echo 'selected="selected"'; } ?>><?php echo $ced." - ".$nombre; ?></option>
							<?php
								}
							?>
							</select>
							</td>
							<td class="auto-style3">&nbsp;
							</td>
							<td class="auto-style3">
							<strong>Siguiente Día</strong></td>
						</tr>
						<tr>
							<td class="auto-style3" style="width: 100px">
							<img src="images/anterior.png" title="Día Anterior" onclick="DIAANTERIOR()" style="cursor:pointer" height="21" width="70">
							</td>
							<td class="auto-style3" style="width: 60px"><strong>Fecha:</strong></td>
							<td align="left">
							<div id="siguientedia">
							<input class="inputs" name="fecha" id="fecha" value="<?php echo date('Y-m-d'); ?>" onchange="NUEVO()" maxlength="70" type="date" placeholder="Fecha Registro" style="width: 300px" /><br>
                            <span id="legen" style="text-transform:capitalize">
                            <?php 

							if($legend=="")
							{
								date_default_timezone_set("America/Bogota");
								setlocale (LC_TIME,"spanish", "es_ES@euro", "es_ES", "es");
								$fecha=date("Y-m-d");
								$dia1 = date("l",strtotime($fecha));
								if($dia1 == 'Monday'){ $dia1 = 'Lunes'; }elseif($dia1 == 'Tuesday'){ $dia1 = 'Martes'; }elseif($dia1 == 'Wednesday'){ $dia1 = 'Miércoles'; }elseif($dia1 == 'Thursday'){ $dia1 = 'Jueves'; }elseif($dia1 == 'Friday'){ $dia1 = 'Viernes'; }elseif($dia1 == 'Saturday'){ $dia1 = 'Sábado'; }elseif($dia1 == 'Sunday'){ $dia1 = 'Domingo'; }
								
								$dia = strftime("%d", strtotime($fecha));
								$mes = strftime("%B", strtotime($fecha));
								$an = strftime("%Y", strtotime($fecha));
								$legend = $dia1.", ".$dia." de ".$mes." de ".$an;

							
								echo $legend; 
                           
							}
							else
							{
							  echo $legend;
							}
							
							?>
                                                    
                            </span>
							</div>
							</td>
							<td class="auto-style3">&nbsp;
							</td>
							<td class="auto-style3">
							<img src="images/siguiente.png" title="Siguiente Día" onclick="SIGUIENTEDIA()" style="cursor:pointer" height="21" width="70">
							</td>
						</tr>
					</table>
					
					</td>
					<td>
					
					<table style="width: 100%">
						<tr>
							<td>
							<fieldset name="Group1">
								<legend>REPORTE MASIVO<br>DE HORAS</legend>
								<a data-reveal-id="horasmasivas" data-animation="fade" style="cursor:pointer">
								<img style="cursor:pointer;height:45px;width:40px" title="Llenado masivo de actividades" src="../../images/nuevafila.png">
								</a>
							</fieldset>
							</td>
							<td>
							<strong></strong>
							</td>
							<td>
							<fieldset name="Group1">
								<legend>REPORTE DE VACACIONES<br>E INCAPACIDADES</legend>
								<a data-reveal-id="horasvacacionesincapacidades" data-animation="fade" style="cursor:pointer">
								<img style="cursor:pointer;height:45px;width:40px" title="Llenado masivo de Vacaciones e Incapacidades" src="../../images/nuevafila.png">
								</a>
							</fieldset>
							</td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td colspan="3">
					<hr>
					</td>
				</tr>
				<tr>
					<td colspan="3">
					<div id="resultado"></div>
					</td>
				</tr>
			</table>
		</div>

		</td>
	</tr>
	<tr>
		<td style="width: 60px">&nbsp;</td>
		<td style="width: 60px">&nbsp;</td>
		<td style="width: 117px">&nbsp;</td>
		<td style="width: 65px">&nbsp;</td>
		<td style="width: 70px">&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td style="width: 60px">&nbsp;</td>
		<td style="width: 60px">&nbsp;</td>
		<td style="width: 117px">&nbsp;</td>
		<td style="width: 65px">&nbsp;</td>
		<td style="width: 70px">&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>
<div id="horasmasivas" <?php echo 'class="reveal-modal"'; ?> style="left: 57%; top: 20px; height: 140px; width: 450px;">

	<table style="width: 100%">
		<tr>
			<td colspan="3" style="text-align:center">
			AGREGAR HORAS DE FORMA MASIVA</td>
		</tr>
		<tr>
			<td colspan="3" style="text-align:center">
			<hr>
			</td>
		</tr>
		<tr>
			<td colspan="1">
			<select name="clientemaxivo" id="clientemaxivo" onchange="CARGARPROYECTOM()" class="inputs" style="width:200px;">
			<option value="">-Seleccione Cliente-</option>
			<?php
				$con = mysqli_query($conectar,"select * from cliente where cli_sw_activo = 1 order by cli_nombre");
				$num = mysqli_num_rows($con);
				for($j = 0; $j < $num; $j++)
				{
					$dato = mysqli_fetch_array($con);
					$clave = $dato['cli_clave_int'];
					$nombre = $dato['cli_nombre'];
			?>
				<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
			<?php
				}
			?>
			</select>
			</td>
			<td colspan="2">
				<select name="regionmaxivo" id="regionmaxivo" onchange="CARGARPROYECTOM()" class="inputs" style="width:210px;">
        <option value="">-Seleccione Región-</option>
        <?php
        $con = mysqli_query($conectar,"select * from region where reg_sw_activo = 1 order by reg_nombre");
        $num = mysqli_num_rows($con);
        for($j = 0; $j < $num; $j++)
        {
			$dato = mysqli_fetch_array($con);
			$clave = $dato['reg_clave_int'];
			$nombre = $dato['reg_nombre'];
			?>
            <option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
			<?php
        }
        ?>
        </select>
			</td>
		</tr>
		<tr>
			<td>
			
			<select name="proyectomaxivo" id="proyectomaxivo" class="inputs" style="width:200px;">
			<option value="">-Seleccione Proyecto-</option>
			</select>
			
			</td>
		
			<td>
			<select name="desde" id="desde" class="inputs" style="width:90px" onchange="MOSTRARHASTA(this.value)">
			<option value="">DESDE</option>
			<?php
			for($i = 0; $i <= 23; $i++)
			{
			?>
				<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
			<?php
			}
			?>
			</select>
			</td>
			<td>
			<div id="divhasta">
			<select name="hasta" id="hasta" class="inputs" style="width:90px">
			<option value="">HASTA</option>
			</select>
			</div>
			</td></tr>
			<tr>
			<td colspan="3">
			<textarea class="inputs" maxlength="255" name="actividadmasivo" id="actividadmasivo" placeholder="Ingrese la actividad" style="height: 18px; width: 100%"></textarea>
			</td>
		</tr>
		<tr>
			<td colspan="3">
			<div style="cursor:pointer;text-align:center;width:100%;background-color:#5A825A;color:white" class="inputs" onclick="CRUDTIEMPOS('AGREGARMASIVO','')"  align="center">ACEPTAR</div>
			</td>
		</tr>		
	</table>

</div>

<div id="horasvacacionesincapacidades" <?php echo 'class="reveal-modal"'; ?> style="left: 57%; top: 20px; height: 200px; width: 450px;">

	<table style="width: 100%">
		<tr>
			<td colspan="2" style="text-align:center">
			VACACIONES E INCAPACIDADES</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align:center">
			<hr>
			</td>
		</tr>
		<tr>
			<td>
			<select name="clientemaxivo1" id="clientemaxivo1" onchange="CARGARPROYECTOM1()" class="inputs" style="width:200px;">
			<option value="">-Seleccione Cliente-</option>
			<?php
				$con = mysqli_query($conectar,"select * from cliente where cli_sw_activo = 1 order by cli_nombre");
				$num = mysqli_num_rows($con);
				for($j = 0; $j < $num; $j++)
				{
					$dato = mysqli_fetch_array($con);
					$clave = $dato['cli_clave_int'];
					$nombre = $dato['cli_nombre'];
			?>
				<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
			<?php
				}
			?>
			</select>
			</td>
			<td>
					<select name="regionmaxivo1" id="regionmaxivo1" onchange="CARGARPROYECTOM1()" class="inputs" style="width:210px;">
        <option value="">-Seleccione Región-</option>
        <?php
        $con = mysqli_query($conectar,"select * from region where reg_sw_activo = 1 order by reg_nombre");
        $num = mysqli_num_rows($con);
        for($j = 0; $j < $num; $j++)
        {
			$dato = mysqli_fetch_array($con);
			$clave = $dato['reg_clave_int'];
			$nombre = $dato['reg_nombre'];
			?>
            <option  value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
			<?php
        }
        ?>
        </select>
				</td>
			</tr>
<tr>
	<td colspan="2">
			
			<select name="proyectomaxivo1" id="proyectomaxivo1" class="inputs" style="width:430px;">
			<option value="">-Seleccione Proyecto-</option>
			</select>
			
			</td>
		</tr>
		<tr>
			<td colspan="">Fecha Inicio
			<input class="inputs" name="fechainicio1" id="fechainicio1" value="<?php echo date('Y-m-d'); ?>" maxlength="70" type="date" placeholder="Fecha Inicio" style="width: 190px" />
			</td>
			<td>Fecha Fin
			<input class="inputs" name="fechafin1" id="fechafin1" value="<?php echo date('Y-m-d'); ?>" maxlength="70" type="date" placeholder="Fecha Inicio" style="width: 190px" />
			</td>
		</tr>
		<tr>
			<td>
			<select name="desde1" id="desde1" class="inputs" style="width:90px" onchange="MOSTRARHASTA1(this.value)">
			<option value="">DESDE</option>
			<?php
			for($i = 0; $i <= 23; $i++)
			{
			?>
				<option value="<?php echo $i; ?>" <?php if($i == 8){ echo 'selected="selected"'; } ?>><?php echo $i; ?></option>
			<?php
			}
			?>
			</select>
			</td>
			<td>
			<div id="divhasta1">
			<select name="hasta1" id="hasta1" class="inputs" style="width:90px">
			<option value="">HASTA</option>
			<option value="16" selected="selected">16</option>
			</select>
			</div>
			</td>
		</tr>
		<tr>
			<td colspan="2">
			<textarea class="inputs" maxlength="255" name="actividadmasivo1" id="actividadmasivo1" placeholder="Ingrese la actividad" style="height: 18px; width: 430px"></textarea>
			</td>
		</tr>
		<tr>
			<td colspan="2">
			<div style="cursor:pointer;text-align:center;width:100%;background-color:#5A825A;color:white" class="inputs" onclick="CRUDTIEMPOS('VACACIONESINCAPACIDADES','')"  align="center">ACEPTAR</div>
			</td>
		</tr>		
		<tr>
			<td colspan="2">
			<div id="resultadomasivo"></div></td>
		</tr>		
	</table>

</div>
<script>
	$(document).ready(function(){
		$('#clientemaxivo').searchable();
		$('#clientemaxivo1').searchable();
		//$('#proyectomaxivo').searchable();
		//$('#proyectomaxivo1').searchable();
		$('#regionmaxivo1').searchable();
		$('#regionmaxivo').searchable();
		
		NUEVO();
	})
</script>
</form>
</body>
</html>