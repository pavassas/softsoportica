function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function EDITAR(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('tiempos').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#tiempos").html("<img alt='cargando' src='img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?editartiempos=si&clave="+v,true);
	ajax.send(null);
	OCULTARSCROLL();
}
function GUARDAR(v,id)
{
	if(v == 'CIUDAD')
	{
		var cod = form1.codigo1.value;
		var pro = form1.proyecto1.value;
		var lp = pro.length;
		var cli = form1.cliente1.value;
		var act = form1.activo1.checked;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('datos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#datos").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?guardarpro=si&cod="+cod+"&pro="+pro+"&act="+act+"&cli="+cli+"&lp="+lp+"&p="+id,true);
		ajax.send(null);
		if(cod != '' && pro != '' && lp >= 3 && cli != '')
		{
			setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
		}
	}
}
function NUEVO(v)
{
	var emp = form1.empleado.value;
	var fec = form1.fecha.value;
	$.post('fecha.php',{fecha:fec},function(data)
		{
		$('#legen').html(data)   
		})
	if(emp != '' && fec != '')
	{
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('resultado').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#resultado").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?nuevoreportetiempo=si&emp="+emp+"&fec="+fec,true);
		ajax.send(null);
		OCULTARSCROLL();
		setTimeout("CALCULARHORAS();",1000);
	}
}
function CONSULTAMODULO(v)
{
	if(v == 'TODOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('tiempos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#tiempos").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?todos=si",true);
		ajax.send(null);
		setTimeout("OCULTARSCROLL();",1000);
	}
}
function BUSCAR()
{	
	var cod = form1.buscodigo.value;
	var pro = form1.busproyecto.value;
	var cli = form1.buscliente.value;
	var act = form1.buscaractivos.value;
			
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('ciudades').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#ciudades").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?buscarpro=si&cod="+cod+"&pro="+pro+"&cli="+cli+"&act="+act,true);
	ajax.send(null);
	setTimeout("OCULTARSCROLL();",1000);
}
function VERFECHAS(v)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('fechassemana').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#fechassemana").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?calcularfechas=si&sem="+v,true);
	ajax.send(null);
}
function SUMARTOTALHORAS(val)
{
	var l = $('#lunes'+val).val();
	var m = $('#martes'+val).val();
	var x = $('#miercoles'+val).val();
	var j = $('#jueves'+val).val();
	var v = $('#viernes'+val).val();
	var s = $('#sabado'+val).val();
	var d = $('#domingo'+val).val();
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('totalhoras'+val).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#totalhoras"+val).html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?sumartotalhoras=si&l="+l+"&m="+m+"&x="+x+"&j="+j+"&v="+v+"&s="+s+"&d="+d,true);
	ajax.send(null);
}
function GUARDARDETALLE(f)
{
	var cli = $('#cliente'+f).val();
	var pro = $('#proyecto'+f).val();
	var act = $('#actividad'+f).val();	
	var ch = $('#clave').val();
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     //document.getElementById('prueba').innerHTML=ajax.responseText;
	    }
	}
	//jQuery("#prueba").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?guardardetalle=si&cli="+cli+"&pro="+pro+"&act="+act+"&ch="+ch+"&f="+f,true);
	ajax.send(null);
	setTimeout("CALCULARHORAS();",1000);
}
function VERPROYECTOS(f)
{
	var cli = $('#cliente'+f).val();

	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('verproyectos'+f).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#verproyectos"+f).html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?verproyectos=si&cli="+cli+"&f="+f,true);
	ajax.send(null);
}
var columnaActual = 1;
var nuevaFila = function() 
{
	/*var fila = '<tr id="fila'+ ++columnaActual +'">'
    +  '<td><input type="input" id="mecanica'+columnaActual+'" name="mecanica'+columnaActual+'" style="width: 250px;"/></td>' 
    +  '<td align="center"><input type="radio" id="grupo'+columnaActual+'" name="grupo'+columnaActual+'" value="cambiar"/></td>' 
    +  '<td align="center"><input type="radio" id="grupo'+columnaActual+'" name="grupo'+columnaActual+'" value="reparar"/></td>' 
    +  '<td align="center"><input type="button" value="Eliminar" onClick="borrarFila('+columnaActual+');"/></td>' 
    +  '</tr>';*/
    var fila = "<tr><td class=\"auto-style3\"><input name=actividad"+ ++columnaActual+" id=actividad"+columnaActual+" onchange=\"GUARDARDETALLE('"+columnaActual+"')\" class=\"inputs\" style=\"width: 200px\" type=\"text\"><\/td><td class=\"auto-style3\"><div id=listaclientes"+columnaActual+"><select name=\"cliente1\" id=\"cliente1\" onchange=\"GUARDARDETALLE('"+columnaActual+"')\" class=\"inputs\" style=\"width:210px\"><option value=\"\">-Seleccione-<\/option><?php $con = mysqli_query($conectar,'select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where UPPER(p.prf_descripcion) = UPPER('cliente') or UPPER(p.prf_descripcion) = UPPER('clientes') order by u.usu_nombre'); $num = mysqli_num_rows($con); for($i = 0; $i < $num; $i++){ $dato = mysqli_fetch_array($con); $clave = $dato['usu_clave_int']; $nombre = $dato['usu_nombre']; ?><option value=\"<?php echo $clave; ?>\"><?php echo $nombre; ?><\/option><?php } ?><\/select></div><\/td><td class=\"auto-style3\"><div id=listaproyectos"+columnaActual+"><select name=\"proyecto1\" id=\"proyecto1\" onchange=\"GUARDARDETALLE('"+columnaActual+"')\" class=\"inputs\" style=\"width:210px\"><option value=\"\">-Seleccione-<\/option><?php $con = mysqli_query($conectar,\"select * from proyecto order by pro_nombre\"); $num = mysqli_num_rows($con); for($i = 0; $i < $num; $i++){ $dato = mysqli_fetch_array($con); $clave = $dato['pro_clave_int']; $nombre = $dato['pro_nombre']; ?><option value=\"<?php echo $clave; ?>\"><?php echo $nombre; ?><\/option><?php } ?><\/select></div><\/td><td class=\"auto-style3\"><table style=\"width: 100%\"><tr><td><strong><div id=\"textolunes1\">L<\/div><\/strong><\/td><td><strong><div id=\"textomartes1\">M<\/div><\/strong><\/td><td><strong><div id=\"textomiercoles1\">X<\/div><\/strong><\/td><td><strong><div id=\"textojueves1\">J<\/div><\/strong><\/td><td><strong><div id=\"textoviernes1\">V<\/div><\/strong><\/td><td><strong><div id=\"textosabado1\">S<\/div><\/strong><\/td><td><strong><div id=\"textodomingo1\">D<\/div><\/strong><\/td><\/tr><tr><td><input name=\"lunes"+columnaActual+"\" id=\"lunes"+columnaActual+"\" onchange=SUMARTOTALHORAS("+columnaActual+");GUARDARDETALLE("+columnaActual+") class=\"inputs\" style=\"width: 40px\" type=\"text\"><\/td><td><input name=\"martes"+columnaActual+"\" id=\"martes"+columnaActual+"\" onchange=SUMARTOTALHORAS("+columnaActual+");GUARDARDETALLE("+columnaActual+") class=\"inputs\" style=\"width: 40px\" type=\"text\"><\/td><td><input name=\"miercoles"+columnaActual+"\" id=\"miercoles"+columnaActual+"\" onchange=SUMARTOTALHORAS("+columnaActual+");GUARDARDETALLE("+columnaActual+") class=\"inputs\" style=\"width: 40px\" type=\"text\"><\/td><td><input name=\"jueves"+columnaActual+"\" id=\"jueves"+columnaActual+"\" onchange=SUMARTOTALHORAS("+columnaActual+");GUARDARDETALLE("+columnaActual+") class=\"inputs\" style=\"width: 40px\" type=\"text\"><\/td><td><input name=\"viernes"+columnaActual+"\" id=\"viernes"+columnaActual+"\" onchange=SUMARTOTALHORAS("+columnaActual+");GUARDARDETALLE("+columnaActual+") class=\"inputs\" style=\"width: 40px\" type=\"text\"><\/td><td><input name=\"sabado"+columnaActual+"\" id=\"sabado"+columnaActual+"\" onchange=SUMARTOTALHORAS("+columnaActual+");GUARDARDETALLE("+columnaActual+") class=\"inputs\" style=\"width: 40px\" type=\"text\"><\/td><td><input name=\"domingo"+columnaActual+"\" id=\"domingo"+columnaActual+"\" onchange=SUMARTOTALHORAS("+columnaActual+");GUARDARDETALLE("+columnaActual+") class=\"inputs\" style=\"width: 40px\" type=\"text\"><\/td><\/tr><\/table><\/td><td class=\"auto-style3\"><div id=totalhoras"+columnaActual+">0<\/div><\/td><td class=\"auto-style3\"><input type=\"button\" value=\"Eliminar\" onClick=\"borrarFila('1');\"\/><\/td><\/tr><tr><td class='auto-style3' colspan='6'><hr></td></tr>";
    
    $("#filas").append(fila); 
    OCULTARSCROLL();
    setTimeout("MOSTRARLISTACLIENTES("+columnaActual+");",500);
    setTimeout("MOSTRARLISTAPROYECTOS("+columnaActual+");",500);
} 
/* Elimina la fila indicada. */ 
var borrarFila = function (indice) 
{ 
	$("#fila" + indice).fadeOut("slow"); 
}
function MOSTRARLISTACLIENTES(v)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('listaclientes'+v).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#listaclientes"+v).html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarlistaclientes=si&fil="+v,true);
	ajax.send(null);
}
function MOSTRARLISTAPROYECTOS(v)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('listaproyectos'+v).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#listaproyectos"+v).html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarlistaproyectos=si&fil="+v,true);
	ajax.send(null);
}
function MOSTRARMOVIMIENTO(v)
{
	var det = jQuery('#ocultodetalle'+v).val();
    if(det == 0)
    {
    	jQuery('#ocultodetalle'+v).val('1');
    	var div = document.getElementById('verdetalle'+v);
    	div.style.display = 'block'; 
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('verdetalle'+v).innerHTML=ajax.responseText;
		    }
		}
		jQuery("#verdetalle"+v).html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?mostrarmovimiento=si&clave="+v,true);
		ajax.send(null);
	}
	else
    { 
    	jQuery('#ocultodetalle'+v).val('0');
    	var div = document.getElementById('verdetalle'+v);
    	div.style.display = 'none';
    	//jQuery("#verocultaranexo"+v).html("VER ANEXOS");
    }
}
function SIGUIENTEDIA()
{
	var emp = form1.empleado.value;
	var fec = form1.fecha.value;
	var dt = new Date();
	var ano1= fec.substr(0,4);
	var mes1= fec.substr(5,2);
	var dia1= fec.substr(8);
	
	var nuevafecha1= new Date(ano1+","+mes1+","+dia1);
	var mes = dt.getMonth()+1;
	var nuevafecha2= new Date(dt.getFullYear()+","+mes+","+dt.getDate());
	var Dif= nuevafecha1.getTime() - nuevafecha2.getTime();
	var dias= Math.floor(Dif/(1000*24*60*60));
	
	if(dias > 0)
	{
		alert('No es posible avanzar mas de un dia');
	}
	else
	{
		if(emp != '' && fec != '')
		{
			var ajax;
			ajax=new ajaxFunction();
			ajax.onreadystatechange=function()
			{
				if(ajax.readyState==4)
			    {
		   		     document.getElementById('siguientedia').innerHTML=ajax.responseText;
			    }
			}
			jQuery("#siguientedia").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
			ajax.open("GET","?siguientedia=si&emp="+emp+"&fec="+fec,true);
			ajax.send(null);
			NUEVOSIGUIENTEDIA(emp,fec);
			OCULTARSCROLL();
			setTimeout("CALCULARHORAS();",1000);
		}
		else
		{
			alert("Debe indicar el empleado y la fecha");
		}
	}
}
function NUEVOSIGUIENTEDIA(emp,fec)
{	
	if(emp != '' && fec != '')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('resultado').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#resultado").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?nuevosiguientedia=si&emp="+emp+"&fec="+fec,true);
		ajax.send(null);
		OCULTARSCROLL();
	}
}
function DIAANTERIOR()
{
	var emp = form1.empleado.value;
	var fec = form1.fecha.value;
	
	var dt = new Date();
	var ano1= fec.substr(0,4);
	var mes1= fec.substr(5,2);
	var dia1= fec.substr(8);
	
	var nuevafecha1= new Date(ano1+","+mes1+","+dia1);
	var mes = dt.getMonth()+1;
	var nuevafecha2= new Date(dt.getFullYear()+","+mes+","+dt.getDate());
	var Dif= nuevafecha2.getTime() - nuevafecha1.getTime();
	var dias= Math.floor(Dif/(1000*24*60*60));
	
	if(dias > 1)
	{
		alert('No es posible avanzar mas de dos dias');
	}
	else
	{
		if(emp != '' && fec != '')
		{
			var ajax;
			ajax=new ajaxFunction();
			ajax.onreadystatechange=function()
			{
				if(ajax.readyState==4)
			    {
		   		     document.getElementById('siguientedia').innerHTML=ajax.responseText;
			    }
			}
			jQuery("#siguientedia").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
			ajax.open("GET","?diaanterior=si&emp="+emp+"&fec="+fec,true);
			ajax.send(null);
			NUEVODIAANTERIOR(emp,fec);
			OCULTARSCROLL();
			setTimeout("CALCULARHORAS();",1000);
		}
		else
		{
			alert("Debe indicar el empleado y la fecha");
		}
	}
}
function NUEVODIAANTERIOR(emp,fec)
{	
	if(emp != '' && fec != '')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('resultado').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#resultado").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?nuevodiaanterior=si&emp="+emp+"&fec="+fec,true);
		ajax.send(null);
		OCULTARSCROLL();
	}
}
function AGREGAR()
{
	var hor = form1.horas.value;
	var sum = parseInt(hor)+1;
	var fila = hor+""+sum;
	var div = document.getElementById('horasagregadas'+fila);
    div.style.display = 'block';
    form1.horas.value = sum;
    OCULTARSCROLL();
    REFRESCARLISTASDE(fila);
}
function CALCULARHORAS()
{
	var emp = form1.empleado.value;
	var fec = form1.fecha.value;
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('totalhoras').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#totalhoras").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?calcularhoras=si&emp="+emp+"&fec="+fec,true);
	ajax.send(null);
}
function MOSTRARHASTA(v)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('divhasta').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#divhasta").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarhasta=si&hor="+v,true);
	ajax.send(null);
}
function AGREGARMASIVO()
{
	var emp = form1.empleado.value;
	var fec = form1.fecha.value;
	
	if(emp != '' && fec != '')
	{
		var cli = form1.clientemasivo.value;
		var pro = form1.proyectomasivo.value;
		var des = form1.desde.value;
		var has = form1.hasta.value;
		var act = form1.actividadmasivo.value;
		if(cli != '' && pro != '' && des != '' && has != '' && act != '')
		{
			var ajax;
			ajax=new ajaxFunction();
			ajax.onreadystatechange=function()
			{
				if(ajax.readyState==4)
			    {
		   		     document.getElementById('datosmasivo').innerHTML=ajax.responseText;
			    }
			}
			jQuery("#datosmasivo").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
			ajax.open("GET","?agregarhorasmasivas=si&emp="+emp+"&fec="+fec+"&des="+des+"&has="+has+"&act="+act+"&cli="+cli+"&pro="+pro,true);
			ajax.send(null);
		}
		else
		{
			alert("Debe ingresar todos los datos");
		}
	}
	else
	{
		alert("Debe indicar el empleado y la fecha");
	}
}
function VERPROYECTOSMASIVOS(f)
{
	var cli = $('#clientemasivo').val();

	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('verproyectos').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#verproyectos").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?verproyectosmasivo=si&cli="+cli+"&f="+f,true);
	ajax.send(null);
}