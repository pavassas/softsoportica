<?php
	error_reporting(0);
	include('../../data/Conexion.php');
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_SESSION['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];
		
	// verifica si no se ha loggeado
	if(!isset($_SESSION["persona"]))
	{
	  session_destroy();
	  header("LOCATION:index.php");
	}else{
	}
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");
	
	$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
	$dato = mysqli_fetch_array($con);
	$perfil = $dato['prf_descripcion'];
	$claveperfil = $dato['prf_clave_int'];
	$claveusuario = $dato['usu_clave_int'];
	$ultimaobra = $dato['obr_clave_int'];
	
	$con = mysqli_query($conectar,"select per_metodo from permiso where prf_clave_int = '".$claveperfil."' and ven_clave_int = 10");
	$dato = mysqli_fetch_array($con);
	$metodo = $dato['per_metodo'];

	if($_GET['editarciu'] == 'si')
	{
		$ciuedi = $_GET['ciuedi'];
		$con = mysqli_query($conectar,"select * from cliente where cli_clave_int = '".$ciuedi."'"); 
		$dato = mysqli_fetch_array($con); 
		$cli = $dato['cli_nombre'];
		$cencos = $dato['cli_cen_cos'];
		$act = $dato['cli_sw_activo'];
?>
		<table style="width: 68%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Cliente:</td>
			<td class="auto-style3">
			<input name="cliente1" id="cliente1" value="<?php echo $cli; ?>" class="inputs" type="text" style="width: 200px" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Centro Costos:</td>
			<td class="auto-style3">
			<input name="centro1" id="centro1" value="<?php echo $cencos; ?>" class="inputs" type="text" style="width: 200px" />&nbsp;
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3" colspan="2">
			<table style="width: 100%">
				<tr>
					<td colspan="3"><strong>AGREGAR PROYECTOS AL CLIENTE: <?php echo $cli; ?></strong></td>
				</tr>
				<tr>
					<td>
					<div id="agregar">
						<select class="auto-style8" multiple="multiple" ondblclick="AGREGAR('<?php echo $ciuedi; ?>')" name="agregarpro" id="agregarpro" style="width: 250px" size="6">
						<?php
							$con = mysqli_query($conectar,"select p.pro_clave_int,p.pro_codigo,p.pro_nombre,r.reg_nombre from proyecto p left outer join region r on r.reg_clave_int = p.reg_clave_int where p.pro_clave_int NOT IN (select pro_clave_int from cliente_proyecto where cli_clave_int = '".$ciuedi."') and pro_sw_activo = 1 order by pro_nombre");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$clave = $dato['pro_clave_int'];
								$proyecto = $dato['pro_nombre'];
								$regi  = $dato['reg_nombre'];
						?>
							<option value="<?php echo $clave; ?>"><?php echo $proyecto." (".$regi.")"; ?></option>
						<?php
							}
						?>
						</select>
					</div>
					</td>
					<td align="center">
					<div style="width: 140px">
						<input type="button" class="pasar izq" onclick="AGREGAR('<?php echo $ciuedi; ?>')" value="Pasar &raquo;"><input type="button" onclick="QUITAR('<?php echo $ciuedi; ?>')" class="quitar der" value="&laquo; Quitar"><br />
						<input type="button" class="pasartodos izq" onclick="PONERTODOS('<?php echo $ciuedi; ?>')" value="Todos &raquo;"><input type="button" onclick="REMOVERTODOS('<?php echo $ciuedi; ?>')" class="quitartodos der" value="&laquo; Todos">
					</div>
					</td>
					<td>
					<div id="agregados">
					<select size="6" multiple="multiple" class="auto-style8" ondblclick="QUITAR('<?php echo $ciuedi; ?>')" name="quitarpro" id="quitarpro" style="width: 250px" size="6">
					<?php 
			        	$sql = mysqli_query($conectar,"select p.pro_clave_int,p.pro_nombre, r.reg_nombre from cliente_proyecto cp inner join proyecto p on (p.pro_clave_int = cp.pro_clave_int) left outer join region r on r.reg_clave_int = p.reg_clave_int where cp.cli_clave_int = '".$ciuedi."' order by p.pro_nombre");
			        	$num = mysqli_num_rows($sql);
			        	for($i = 0; $i < $num; $i++)
			        	{
			        		$dato = mysqli_fetch_array($sql);
			        		$clave = $dato['pro_clave_int'];
			        		$pro = $dato['pro_nombre'];
			        		$regi = $dato['reg_nombre'];
					?>
						<option value="<?php echo $clave; ?>"><?php echo $pro." (".$regi.")"; ?></option>
					<?php } ?>
			        </select>
					</div>
					</td>
				</tr>
			</table>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3" colspan="2">Activa:<input class="inputs" <?php if($act == 1){ echo 'checked="checked"'; } ?> name="activo1" id="activo1" type="checkbox" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4">
			<input name="submit" type="button" value="Guardar" onclick="GUARDAR('<?php echo $ciuedi; ?>')"  style="width: 348px; height: 25px; cursor:pointer" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">
			<div id="datos">
			</div>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
<?php
		exit();
	}
	
	if($_GET['guardarcli'] == 'si')
	{
		sleep(1);
		$cli = $_GET['cli'];
		$cencos = $_GET['cencos'];
		$act = $_GET['act'];
		$lc = $_GET['lc'];
		$c = $_GET['c'];
		
		$fecha=date("Y/m/d H:i:s");
		
		$sql = mysqli_query($conectar,"select * from cliente where (UPPER(cli_nombre) = UPPER('".$cli."')) AND cli_clave_int <> '".$c."'");
		$dato = mysqli_fetch_array($sql);
		$concli = $dato['cli_nombre'];
		
		if($cli == '')
		{
			echo "<div class='validaciones'>Debe ingresar el cliente</div>";
		}
		else
		if($cencos == '')
		{
			echo "<div class='validaciones'>Debe ingresar el centro de costos</div>";
		}
		else
		if(STRTOUPPER($concli) == STRTOUPPER($cli))
		{
			echo "<div class='validaciones'>El cliente ingresado ya existe</div>";
		}
		else
		if($lc < 3)
		{
			echo "<div class='validaciones'>El nombre del cliente debe ser mí­nimo de 3 dijitos</div>";
		}
		else
		{			
			if($act == 'false'){$swact = 0;}elseif($act == 'true'){$swact = 1;}
			
			$con = mysqli_query($conectar,"update cliente set cli_cen_cos = '".$cencos."',cli_nombre = '".$cli."',cli_sw_activo = '".$swact."', cli_usu_actualiz = '".$usuario."', cli_fec_actualiz = '".$fecha."' where cli_clave_int = '".$c."'");
			
			if($con >= 1)
			{
				//mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_encabezado,obr_clave_int,loa_usu_actualiz,loa_fec_actualiz) values(null,18,58,'".$c."','".$ultimaobra."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 58=Actualización proyecto
				echo "<div class='ok'>Datos grabados correctamente</div>";
			}
			else
			{
				echo "<div class='validaciones'>No se han podido guardar los datos</div>";
			}	
		}
		exit();
	}
	
	if($_GET['nuevocliente'] == 'si')
	{
		sleep(1);
		$cli = $_GET['cli'];
		$cencos = $_GET['cencos'];
		$act = $_GET['act'];
		$lc = $_GET['lc'];
		
		$fecha=date("Y/m/d H:i:s");
		
		$sql = mysqli_query($conectar,"select * from cliente where (UPPER(cli_nombre) = UPPER('".$cli."'))");
		$dato = mysqli_fetch_array($sql);
		$concli = $dato['cli_nombre'];
		
		if($cli == '')
		{
			echo "<div class='validaciones'>Debe ingresar el cliente</div>";
		}
		else
		if($cencos == '')
		{
			echo "<div class='validaciones'>Debe ingresar el centro de costos</div>";
		}
		else
		if(STRTOUPPER($concli) == STRTOUPPER($cli))
		{
			echo "<div class='validaciones'>El cliente ingresado ya existe</div>";
		}
		else
		if($lc < 3)
		{
			echo "<div class='validaciones'>El nombre del cliente debe ser mí­nimo de 3 dijitos</div>";
		}
		else
		{
			if($act == 'false'){$swact = 0;}elseif($act == 'true'){$swact = 1;}
			
			$con = mysqli_query($conectar,"insert into cliente(cli_cen_cos,cli_nombre,cli_sw_activo,cli_usu_actualiz,cli_fec_actualiz) values('".$cencos."','".$cli."','".$swact."','".$usuario."','".$fecha."')");	
			//echo "insert into cliente(cli_cen_cos,cli_nombre,cli_sw_activo,cli_usu_actualiz,cli_fec_actualiz) values('".$cencos."','".$cli."','".$swact."','".$usuario."','".$fecha."')";			
			
			if($con >= 1)
			{
				$con = mysqli_query($conectar,"select cli_clave_int from cliente where UPPER(cli_nombre) = UPPER('".$cli."')");
				$dato = mysqli_fetch_array($con);
				$ciuedi = $dato['cli_clave_int'];
				/*mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_encabezado,obr_clave_int,loa_usu_actualiz,loa_fec_actualiz) values(null,18,57,'".$claciu."','".$ultimaobra."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 57=Creación proyecto*/
				echo "<div class='ok'>Datos grabados correctamente</div>";
				?>
				<table style="width: 100%">
				<tr>
					<td colspan="3"><strong>AGREGAR PROYECTOS AL CLIENTE: <?php echo $cli; ?></strong></td>
				</tr>
				<tr>
					<td>
					<div id="agregar">
						<select class="auto-style8" multiple="multiple" ondblclick="AGREGAR('<?php echo $ciuedi; ?>')" name="agregarpro" id="agregarpro" style="width: 150px" size="6">
						<?php
							$con = mysqli_query($conectar,"select * from proyecto where pro_clave_int NOT IN (select pro_clave_int from cliente_proyecto where cli_clave_int = '".$ciuedi."') and pro_sw_activo = 1 order by pro_nombre");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$clave = $dato['pro_clave_int'];
								$proyecto = $dato['pro_nombre'];
						?>
							<option value="<?php echo $clave; ?>"><?php echo $proyecto; ?></option>
						<?php
							}
						?>
						</select>
					</div>
					</td>
					<td align="center">
					<div style="width: 140px">
						<input type="button" class="pasar izq" onclick="AGREGAR('<?php echo $ciuedi; ?>')" value="Pasar &raquo;"><input type="button" onclick="QUITAR('<?php echo $ciuedi; ?>')" class="quitar der" value="&laquo; Quitar"><br />
						<input type="button" class="pasartodos izq" onclick="PONERTODOS('<?php echo $ciuedi; ?>')" value="Todos &raquo;"><input type="button" onclick="REMOVERTODOS('<?php echo $ciuedi; ?>')" class="quitartodos der" value="&laquo; Todos">
					</div>
					</td>
					<td>
					<div id="agregados">
					<select size="6" multiple="multiple" class="auto-style8" ondblclick="QUITAR('<?php echo $ciuedi; ?>')" name="quitarpro" id="quitarpro" style="width: 150px" size="6">
					<?php 
			        	$sql = mysqli_query($conectar,"select * from cliente_proyecto cp inner join proyecto p on (p.pro_clave_int = cp.pro_clave_int) where cp.cli_clave_int = '".$ciuedi."' order by p.pro_nombre");
			        	$num = mysqli_num_rows($sql);
			        	for($i = 0; $i < $num; $i++)
			        	{
			        		$dato = mysqli_fetch_array($sql);
			        		$clave = $dato['pro_clave_int'];
			        		$obra = $dato['pro_nombre'];
					?>
						<option value="<?php echo $clave; ?>"><?php echo $obra; ?></option>
					<?php } ?>
			        </select>
					</div>
					</td>
				</tr>
			</table>
			<?php
			}
			else
			{
				echo "<div class='validaciones'>No se han podido guardar los datos</div>";
			}
		}
		exit();
	}
	if($_GET['todos'] == 'si')
	{
		sleep(1);
		$rows=mysqli_query($conectar,"select * from cliente");
		$total=mysqli_num_rows($rows);
?>
		<table style="width: 100%;border-collapse:collapse">
		<tr>
			<td class="auto-style3" style="width: 27px">
				<input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<?php echo $total;?>,this);" class="auto-style6" /><span class="auto-style6">
				</span>
			</td>
			<td class="auto-style3" colspan="6">
			<?php 
			if($metodo == 1)
			{
			?>
			<table style="width: 30%">
				<tr>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../images/activo.png" alt="" class="auto-style6" /><input type="submit" value="Activar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../images/inactivo.png" alt="" class="auto-style6" /><input type="submit" value="Inactivar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../images/eliminar.png" alt="" class="auto-style6" /><input type="submit" value="Eliminar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
				</tr>
			</table>
			<?php
			}
			?>
			</td>
		</tr>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px"><strong>Cliente</strong></td>
			<td class="auto-style5" style="width: 150px"><strong>Centro Costos</strong></td>
			<td class="auto-style5" style="width: 98px"><strong>Actualizado por</strong></td>
			<td class="auto-style5" style="width: 127px"><strong>
				Actualización</strong></td>
			<td class="auto-style5" style="width: 29px"><strong>
			Activo</strong></td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
		<?php
			$contador=0;
			$con = mysqli_query($conectar,"select * from cliente order by cli_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clave = $dato['cli_clave_int'];
				$nom = $dato['cli_nombre'];
				$cencos = $dato['cli_cen_cos'];
				$act = $dato['cli_sw_activo'];
				$usuact = $dato['cli_usu_actualiz'];
				$fecact = $dato['cli_fec_actualiz'];
				$contador=$contador+1;
				if($i == 0)
				{
		?>
		<tr>
			<td class="auto-style5" colspan="7"><hr></td>
		</tr>
		<?php
				}
		?>
		<tr style="<?php if($i % 2 == 0){ echo 'background-color:#B8CEB8"'; } ?>">
			<td class="auto-style3" style="width: 27px">
				<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<?php echo $contador;?>" value="<?php echo $dato['cli_clave_int'];?>" class="auto-style6" /></td>
			<td class="auto-style5" style="width: 200px"><?php echo $nom; ?></td>
			<td class="auto-style5" style="width: 150px"><?php echo $cencos; ?></td>
			<td class="auto-style5" style="width: 98px"><?php echo $usuact; ?></td>
			<td class="auto-style5" style="width: 127px"><?php echo $fecact; ?></td>
			<td class="auto-style1" style="width: 30px">
			<input name="activarinactivar" id="activarinactivar" type="checkbox" <?php if($act == 1){ echo 'checked="checked"'; } ?> disabled="disabled" class="auto-style6" ></td>
			<td class="auto-style5">
			<?php 
			if($metodo == 1)
			{
			?>
			<a data-reveal-id="editarciudad" data-animation="fade" style="cursor:pointer" onclick="EDITAR('<?php echo $dato['cli_clave_int']; ?>','CIUDAD')"><img src="../../images/editar.png" alt="" height="22" width="21" /></a>
			<?php
			}
			?>
			</td>
		</tr>
		<?php
			}
		?>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 150px">&nbsp;</td>
			<td class="auto-style5" style="width: 98px">&nbsp;</td>
			<td class="auto-style5" style="width: 127px">&nbsp;</td>
			<td class="auto-style5" style="width: 29px">&nbsp;</td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
	</table>
<?php
		exit();
	}
?>
<?php
	if($_GET['buscarcli'] == 'si')
	{
		$cli = $_GET['cli'];
		$cencos = $_GET['cencos'];
		$act = $_GET['act'];

		$rows=mysqli_query($conectar,"select * from cliente where (cli_cen_cos LIKE REPLACE('%".$cencos."%',' ','%') OR '".$cencos."' IS NULL OR '".$cencos."' = '') and (cli_nombre LIKE REPLACE('%".$cli."%',' ','%') OR '".$cli."' IS NULL OR '".$cli."' = '') and (cli_sw_activo = '".$act."' OR '".$act."' IS NULL OR '".$act."' = '')");
		$total=mysqli_num_rows($rows);
?>
		<table style="width: 100%;border-collapse:collapse">
		<tr>
			<td class="auto-style3" style="width: 27px">
				<input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<?php echo $total;?>,this);" class="auto-style6" /><span class="auto-style6">
				</span>
			</td>
			<td class="auto-style3" colspan="6">
			<?php 
			if($metodo == 1)
			{
			?>
			<table style="width: 30%">
				<tr>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../images/activo.png" alt="" class="auto-style6" /><input type="submit" value="Activar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../images/inactivo.png" alt="" class="auto-style6" /><input type="submit" value="Inactivar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../images/eliminar.png" alt="" class="auto-style6" /><input type="submit" value="Eliminar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
				</tr>
			</table>
			<?php
			}
			?>
			</td>
		</tr>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px"><strong>Cliente</strong></td>
			<td class="auto-style5" style="width: 150px"><strong>Centro Costos</strong></td>
			<td class="auto-style5" style="width: 98px"><strong>Actualizado por</strong></td>
			<td class="auto-style5" style="width: 127px"><strong>
				Actualización</strong></td>
			<td class="auto-style5" style="width: 29px"><strong>
			Activo</strong></td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
		<?php
			$contador=0;
			$con = mysqli_query($conectar,"select * from cliente where (cli_cen_cos LIKE REPLACE('%".$cencos."%',' ','%') OR '".$cencos."' IS NULL OR '".$cencos."' = '') and (cli_nombre LIKE REPLACE('%".$cli."%',' ','%') OR '".$cli."' IS NULL OR '".$cli."' = '') and (cli_sw_activo = '".$act."' OR '".$act."' IS NULL OR '".$act."' = '') order by cli_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clave = $dato['cli_clave_int'];
				$nom = $dato['cli_nombre'];
				$cencos = $dato['cli_cen_cos'];
				$act = $dato['cli_sw_activo'];
				$usuact = $dato['cli_usu_actualiz'];
				$fecact = $dato['cli_fec_actualiz'];
				$contador=$contador+1;
				if($i == 0)
				{
		?>
		<tr>
			<td class="auto-style5" colspan="7"><hr></td>
		</tr>
		<?php
				}
		?>
		<tr style="<?php if($i % 2 == 0){ echo 'background-color:#B8CEB8"'; } ?>">
			<td class="auto-style3" style="width: 27px">
				<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<?php echo $contador;?>" value="<?php echo $dato['cli_clave_int']; ?>" class="auto-style6" /></td>
			<td class="auto-style5" style="width: 200px"><?php echo $nom; ?></td>
			<td class="auto-style5" style="width: 150px"><?php echo $cencos; ?></td>
			<td class="auto-style5" style="width: 98px"><?php echo $usuact; ?></td>
			<td class="auto-style5" style="width: 127px"><?php echo $fecact; ?></td>
			<td class="auto-style1" style="width: 30px">
			<input name="activarinactivar" id="activarinactivar" type="checkbox" <?php if($act == 1){ echo 'checked="checked"'; } ?> disabled="disabled" class="auto-style6" ></td>
			<td class="auto-style5">
			<?php 
			if($metodo == 1)
			{
			?>
			<a data-reveal-id="editarciudad" data-animation="fade" style="cursor:pointer" onclick="EDITAR('<?php echo $dato['cli_clave_int']; ?>','CIUDAD')"><img src="../../images/editar.png" alt="" height="22" width="21" /></a>
			<?php
			}
			?>
			</td>
		</tr>
		<?php
			}
		?>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 150px">&nbsp;</td>
			<td class="auto-style5" style="width: 98px">&nbsp;</td>
			<td class="auto-style5" style="width: 127px">&nbsp;</td>
			<td class="auto-style5" style="width: 29px">&nbsp;</td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
	</table>
<?php
		exit();
	}
	if($_GET['agregarproyecto'] == "si")
	{
		$fecha=date("Y/m/d H:i:s");
		$pro = $_GET['pro'];
		$cli = $_GET['cli'];
		
		$seleccionados = explode(',',$pro);	
		$num = count($seleccionados);
		
		for($i = 0; $i < $num; $i++)
		{
			$con = mysqli_query($conectar,"select * from cliente_proyecto where cli_clave_int = '".$cli."' and pro_clave_int = '".$seleccionados[$i]."'");
			$num1 = mysqli_num_rows($con);
			
			if($num1 <= 0 and $pro != '' and $pro != 0 and $cli != '' and $cli != 0)
			{
				$sql = mysqli_query($conectar,"insert into cliente_proyecto(cli_clave_int,pro_clave_int,cpr_usu_actualiz,cpr_fec_actualiz) values('".$cli."','".$seleccionados[$i]."','".$usuario."','".$fecha."')");
			}
		}
		$sql1 = mysqli_query($conectar,"update cliente set cli_usu_actualiz = '".$usuario."', cli_fec_actualiz = '".$fecha."' where cli_clave_int = '".$cli."'");
		//mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,1,3,'".$usu."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 3=Actualización usuario
?>
		<select size="6" multiple="multiple" class="auto-style8" ondblclick="QUITAR('<?php echo $cli; ?>')" name="quitarpro" id="quitarpro" style="width: 250px">
		<?php 
        	$sql = mysqli_query($conectar,"select p.pro_clave_int,p.pro_nombre,r.reg_nombre from cliente_proyecto cp inner join proyecto p on (p.pro_clave_int = cp.pro_clave_int) left outer join region r on r.reg_clave_int = p.reg_clave_int where cp.cli_clave_int = '".$cli."' order by p.pro_nombre");
        	$num = mysqli_num_rows($sql);
        	for($i = 0; $i < $num; $i++)
        	{
        		$dato = mysqli_fetch_array($sql);
        		$clave = $dato['pro_clave_int'];
        		$proyecto = $dato['pro_nombre'];
        		$regi = $dato['reg_nombre'];
		?>
			<option value="<?php echo $clave; ?>"><?php echo $proyecto." (".$regi.")"; ?></option>
		<?php } ?>
        </select>
<?php
		exit();
	}
	if($_GET['ponertodos'] == "si")
	{
		$fecha=date("Y/m/d H:i:s");
		$cli = $_GET['cli'];
				
		$con = mysqli_query($conectar,"insert into cliente_proyecto select null,'".$cli."',pro_clave_int,'".$usuario."','".$fecha."' from proyecto where pro_clave_int not in (select pro_clave_int from cliente_proyecto where cli_clave_int = '".$cli."')");
		//mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,1,3,'".$usu."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 3=Actualización usuario
?>
		<select size="6" multiple="multiple" class="auto-style8" ondblclick="QUITAR('<?php echo $cli; ?>')" name="quitarpro" id="quitarpro" style="width: 250px">
		<?php 
        	$sql = mysqli_query($conectar,"select p.pro_clave_int,p.pro_nombre,r.reg_nombre from cliente_proyecto cp inner join proyecto p on (p.pro_clave_int = cp.pro_clave_int) left outer join region r on r.reg_clave_int = r.reg_clave_int where cp.cli_clave_int = '".$cli."' order by p.pro_nombre");
        	$num = mysqli_num_rows($sql);
        	for($i = 0; $i < $num; $i++)
        	{
        		$dato = mysqli_fetch_array($sql);
        		$clave = $dato['pro_clave_int'];
        		$proyecto = $dato['pro_nombre'];
        		$regi = $dato['reg_nombre'];
		?>
			<option value="<?php echo $clave; ?>"><?php echo $proyecto." (".$regi.")"; ?></option>
		<?php } ?>
        </select>
<?php
		exit();
	}
	if($_GET['quitarproyecto'] == "si")
	{
		$fecha=date("Y/m/d H:i:s");
		$pro = $_GET['pro'];
		$cli = $_GET['cli'];
				
		$seleccionados = explode(',',$pro);	
		$num = count($seleccionados);
		
		for($i = 0; $i < $num; $i++)
		{
			if($pro != '' and $cli != '')
			{
				$sql = mysqli_query($conectar,"delete from cliente_proyecto where pro_clave_int = '".$seleccionados[$i]."' and cli_clave_int = '".$cli."'");
			}
		}
		$sql1 = mysqli_query($conectar,"update cliente set cli_usu_actualiz = '".$usuario."', cli_fec_actualiz = '".$fecha."' where cli_clave_int = '".$cli."'");
		//mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,1,3,'".$usu."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 3=Actualización usuario
?>
		<select size="6" multiple="multiple" class="auto-style8" name="agregarpro" id="agregarpro" style="width: 250px" ondblclick="AGREGAR('<?php echo $cli; ?>')">
		<?php
			$con = mysqli_query($conectar,"select p.pro_clave_int,p.pro_nombre, r.reg_nombre from proyecto p left outer join region r on r.reg_clave_int = p.reg_clave_int where pro_clave_int NOT IN (select pro_clave_int from cliente_proyecto where cli_clave_int = '".$cli."') and pro_sw_activo = 1 order by pro_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clave = $dato['pro_clave_int'];
				$proyecto = $dato['pro_nombre'];
				$regi = $dato['reg_nombre'];
		?>
			<option value="<?php echo $clave; ?>"><?php echo $proyecto." (".$regi.")"; ?></option>
		<?php
			}
		?>
        </select>
<?php
		exit();
	}
	if($_GET['quitartodos'] == "si")
	{
		$fecha=date("Y/m/d H:i:s");
		$cli = $_GET['cli'];
		$con = mysqli_query($conectar,"delete from cliente_proyecto where cli_clave_int = '".$cli."'");
		//mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,1,3,'".$usu."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 3=Actualización usuario
?>
		<select class="auto-style8" multiple="multiple" ondblclick="AGREGAR('<?php echo $cli; ?>')" name="agregarpro" id="agregarpro" style="width: 250px" size="6">
		<?php
			$con = mysqli_query($conectar,"select p.pro_clave_int,p.pro_nombre,r.reg_nombre  from proyecto p left outer join  region on r.reg_clave_int = p.reg_clave_int where pro_clave_int NOT IN (select pro_clave_int from cliente_proyecto where cli_clave_int = '".$cli."') and pro_sw_activo = 1 order by pro_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clave = $dato['pro_clave_int'];
				$proyecto = $dato['pro_nombre'];
		?>
			<option value="<?php echo $clave; ?>"><?php echo $proyecto; ?></option>
		<?php
			}
		?>
		</select>

<?php
		exit();
	}
	if($_GET['mostrarproyectos'] == 'si')
	{
		$cli = $_GET['clacli'];
		$con = mysqli_query($conectar,"select p.pro_nombre,p.pro_codigo,r.reg_nombre from cliente_proyecto cp inner join proyecto p on (p.pro_clave_int = cp.pro_clave_int) left outer join region r on r.reg_clave_int = p.reg_clave_int where cp.cli_clave_int = '".$cli."' order by p.pro_nombre");
		$num = mysqli_num_rows($con);
		if($num <= 0)
		{
			echo "<div class='sinproyecto'>SIN PROYECTOS ASIGNADOS</div>";
		}
		else
		{
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$pro = $dato['pro_nombre'];
				$cod = $dato['pro_codigo'];
				$regi = $dato['reg_nombre'];
				echo "<div class='proyectos'>".$cod." ".$pro." (".$regi.")</div>";
			}
		}
		exit();
	}
?>
<!DOCTYPE HTML>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

	
<title>CONTROL DE OBRAS</title>
<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
<!--[if lte IE 7]>
<link rel="stylesheet" href="css/ie.c32bc18afe0e2883ee4912a51f86c119.css" type="text/css" />
<![endif]-->

<style type="text/css">
.auto-style1 {
	text-align: center;
}
.auto-style2 {
	font-size: 12px;
	font-family: Arial, helvetica;
	outline: none;
/*transition: all 0.75s ease-in-out;*/ /*-webkit-transition: all 0.75s ease-in-out;*/ /*-moz-transition: all 0.75s ease-in-out;*/border-radius: 3px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 3px;
	border: 1px solid rgba(0,0,0, 0.2);
	background-color: #eee;
	padding: 3px;
	box-shadow: 0 0 10px #aaa;
	-webkit-box-shadow: 0 0 10px #aaa;
	-moz-box-shadow: 0 0 10px #aaa;
	border: 1px solid #999;
	background-color: white;
	text-align: center;
}
.auto-style3 {
	text-align: left;
}
.inputs{
	font-size:14px;
    font-family: Arial, helvetica;
    outline:none;
    border-radius:3px;
    -webkit-border-radius:6px;
    -moz-border-radius:3px;
    border:1px solid rgba(0,0,0, 0.5);
    padding: 3px;
}
.validaciones{
	font-size:14px;
    font-family: Arial, helvetica;
    outline:none;
    border-radius:3px;
    -webkit-border-radius:6px;
    -moz-border-radius:3px;
    border:1px solid rgba(0,0,0, 0.2);
    color:maroon;
    background-color:#eee;
    padding: 3px;
    text-align:center;
}
.sinproyecto{
	font-size:14px;
    font-family: Arial, helvetica;
    outline:none;
    border-radius:3px;
    -webkit-border-radius:6px;
    -moz-border-radius:3px;
    border:1px solid rgba(0,0,0, 0.2);
    color:maroon;
    background-color:#eee;
    padding: 3px;
    text-align:left;
}
.ok{
	font-size:14px;
    font-family: Arial, helvetica;
    outline:none;
    border-radius:3px;
    -webkit-border-radius:6px;
    -moz-border-radius:3px;
    border:1px solid rgba(0,0,0, 0.2);
    color:green;
    background-color:#eee;
    padding: 3px;
    text-align:center;
}
.proyectos{
	font-size:14px;
    font-family: Arial, helvetica;
    outline:none;
    border-radius:3px;
    -webkit-border-radius:6px;
    -moz-border-radius:3px;
    border:1px solid rgba(0,0,0, 0.2);
    color:green;
    background-color:#eee;
    padding: 3px;
    text-align:left;
}
.auto-style5 {
	text-align: left;
	font-family: Arial, Helvetica, sans-serif;
}
.auto-style6 {
	font-family: Arial, Helvetica, sans-serif;
}
.auto-style7 {
	text-align: center;
	font-size: large;
}
</style>

<script type="text/javascript" language="javascript">
	selecteds=0;
	
	function CheckUncheck(total,check){
		checkbox=null;
		for(i=1;i<=total;i++){
			checkbox=document.getElementById("idcat"+i);
			//alert(checkbox.value);
			checkbox.checked=check.checked;
		}
		
		if(check.checked){
			selecteds=total;
		}else{
			selecteds=0;
		}
		
	}
	
	function contadorVals(check){
		if(check.checked){
			selecteds=selecteds+1;
		}else{
			selecteds=selecteds-1;
		}
	}
	
	function selectedVals(){
		if(selecteds==0){
			alert("Seleccione al menos un registro.");
			return false;
		}else{
			return true;
		}
	}
</script>
<?php //VALIDACIONES ?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="llamadas2.js"></script>

<?php //VENTANA EMERGENTE ?>
<link rel="stylesheet" href="../../css/reveal.css" />
<script type="text/javascript" src="../../js/jquery.reveal.js"></script>
<script type="text/javascript" language="javascript">
function OCULTARSCROLL()
{
	setTimeout("parent.autoResize('iframe9')",500);
	setTimeout("parent.autoResize('iframe9')",1000);
	setTimeout("parent.autoResize('iframe9')",2000);
	setTimeout("parent.autoResize('iframe9')",3000);
	setTimeout("parent.autoResize('iframe9')",4000);
	setTimeout("parent.autoResize('iframe9')",5000);
	setTimeout("parent.autoResize('iframe9')",6000);
	setTimeout("parent.autoResize('iframe9')",7000);
	setTimeout("parent.autoResize('iframe9')",8000);
	setTimeout("parent.autoResize('iframe9')",9000);
	setTimeout("parent.autoResize('iframe9')",10000);
	setTimeout("parent.autoResize('iframe9')",11000);
	setTimeout("parent.autoResize('iframe9')",12000);
	setTimeout("parent.autoResize('iframe9')",13000);
	setTimeout("parent.autoResize('iframe9')",14000);
	setTimeout("parent.autoResize('iframe9')",15000);
	setTimeout("parent.autoResize('iframe9')",16000);
	setTimeout("parent.autoResize('iframe9')",17000);
	setTimeout("parent.autoResize('iframe9')",18000);
	setTimeout("parent.autoResize('iframe9')",19000);
	setTimeout("parent.autoResize('iframe9')",20000);
}
setTimeout("parent.autoResize('iframe9')",500);
setTimeout("parent.autoResize('iframe9')",1000);
setTimeout("parent.autoResize('iframe9')",2000);
setTimeout("parent.autoResize('iframe9')",3000);
setTimeout("parent.autoResize('iframe9')",4000);
setTimeout("parent.autoResize('iframe9')",5000);
setTimeout("parent.autoResize('iframe9')",6000);
setTimeout("parent.autoResize('iframe9')",7000);
setTimeout("parent.autoResize('iframe9')",8000);
setTimeout("parent.autoResize('iframe9')",9000);
setTimeout("parent.autoResize('iframe9')",10000);
setTimeout("parent.autoResize('iframe9')",11000);
setTimeout("parent.autoResize('iframe9')",12000);
setTimeout("parent.autoResize('iframe9')",13000);
setTimeout("parent.autoResize('iframe9')",14000);
setTimeout("parent.autoResize('iframe9')",15000);
setTimeout("parent.autoResize('iframe9')",16000);
setTimeout("parent.autoResize('iframe9')",17000);
setTimeout("parent.autoResize('iframe9')",18000);
setTimeout("parent.autoResize('iframe9')",19000);
setTimeout("parent.autoResize('iframe9')",20000);
</script>

</head>
<body>
<?php
$rows=mysqli_query($conectar,"select * from cliente");
$total=mysqli_num_rows($rows);
?>
<form name="form1" id="form1" action="confirmar.php" method="post" onsubmit="return selectedVals();">
<!--[if lte IE 7]>
<div class="ieWarning">Este navegador no es compatible con el sistema. Por favor, use Chrome, Safari, Firefox o Internet Explorer 8 o superior.</div>
<![endif]-->
<table style="width: 100%">
	<tr>
		<td class="auto-style2" onclick="CONSULTAMODULO('TODOS')" onmouseover="this.style.backgroundColor='#5A825A';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="width: 60px; cursor:pointer">
		Todos
		<?php
			$con = mysqli_query($conectar,"select COUNT(*) cant from cliente");
			$dato = mysqli_fetch_array($con);
			echo $dato['cant'];
		?>
		</td>
		<td class="auto-style7" style="cursor:pointer" colspan="4">
		MAESTRA CLIENTES</td>
		<td class="auto-style2" onmouseover="this.style.backgroundColor='#CCCCCC';this.style.color='#0F213C';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="width: 55px; cursor:pointer">
		<?php 
		if($metodo == 1)
		{
		?>
		<a data-reveal-id="nuevaciudad" data-animation="fade" style="cursor:pointer">
		<table style="width: 100%">
			<tr>
				<td style="width: 14px">
				
				
				<img alt="" src="../../images/add2.png">
				</td>
				<td>Añadir</td>
			</tr>
		</table>
		</a>
		<?php
		}
		?>
		</td>
	</tr>
	<tr>
		<td class="auto-style2" colspan="6">
		<div id="filtro">
			<table style="width: 33%">
				<tr>
					<td class="auto-style1"><strong>Filtro:<img src="../../images/buscar.png" alt="" height="18" width="15" /></strong></td>
					<td class="auto-style1">
					<input class="inputs" onkeyup="BUSCAR()" name="buscliente" id="buscliente" maxlength="255" type="text" placeholder="Cliente" style="width: 150px" />
					</td>
					<td class="auto-style1">
					<input class="inputs" onkeyup="BUSCAR()" name="buscentro" id="buscentro" maxlength="10" type="text" placeholder="Centro Costos" style="width: 150px" /></td>
					<td class="auto-style1">
					<strong>Activo:</strong></td>
					<td class="auto-style1">
					<select name="buscaractivos" class="inputs" onchange="BUSCAR()">
					<option value="">Todos</option>
					<option value="1">Activos</option>
					<option value="0">Inactivo</option>
					</select></td>
				</tr>
			</table>
		</div>
		</td>
	</tr>
	<tr>
		<td colspan="6" class="auto-style2">
		<div id="editarciudad" class="reveal-modal" style="left: 44%; top: 10px; height: 250px; width: 675px;">
			<table style="width: 38%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Nombre:</td>
			<td class="auto-style3"><input class="inputs" name="nombre1" maxlength="50" value="<?php echo $nom; ?>" type="text" style="width: 200px" />
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Activo:</td>
			<td class="auto-style3"><input class="inputs" <?php if($act == 1){ echo 'checked="checked"'; } ?> name="activo1" type="checkbox" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4">
			<input name="submit" type="button" value="Guardar" onclick="GUARDAR('proyecto','<?php echo $ubiedi; ?>')"  style="width: 348px; height: 25px; cursor:pointer" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">
			<div id="datos">
			</div>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
			<a class="close-reveal-modal">&#215;</a>
		</div>
		<div id="ciudades">
		<table style="width: 100%;border-collapse:collapse">
			<tr>
				<td class="auto-style3" style="width: 27px">
					<input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<?php echo $total;?>,this);" />
				</td>
				<td class="auto-style3" colspan="6">
				<?php 
				if($metodo == 1)
				{
				?>
				<table style="width: 30%">
					<tr>
						<td class="auto-style1"><p style="cursor:pointer"><img src="../../images/activo.png" alt="" /><input type="submit" value="Activar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" /></p></td>
						<td class="auto-style1"><p style="cursor:pointer"><img src="../../images/inactivo.png" alt="" /><input type="submit" value="Inactivar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" /></p></td>
						<td class="auto-style1"><p style="cursor:pointer"><img src="../../images/eliminar.png" alt="" /><input type="submit" value="Eliminar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" /></p></td>
					</tr>
				</table>
				<?php
				}
				?>
				</td>
			</tr>
			<tr>
				<td class="auto-style3" style="width: 27px">&nbsp;</td>
				<td class="auto-style3" style="width: 200px" ><strong>Cliente</strong></td>
				<td class="auto-style3" style="width: 150px" ><strong>Centro Costos</strong></td>
				<td class="auto-style3" style="width: 98px"><strong>Actualizado por</strong></td>
				<td class="auto-style3" style="width: 127px"><strong>
				Actualización</strong></td>
				<td class="auto-style3" style="width: 29px"><strong>
				Activo</strong></td>
				<td class="auto-style3">&nbsp;</td>
			</tr>
			<?php
				$contador=0;
				$con = mysqli_query($conectar,"select * from cliente order by cli_nombre");
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$clave = $dato['cli_clave_int'];
					$nom = $dato['cli_nombre'];
					$cencos = $dato['cli_cen_cos'];
					$act = $dato['cli_sw_activo'];
					$usuact = $dato['cli_usu_actualiz'];
					$fecact = $dato['cli_fec_actualiz'];
					$contador=$contador+1;
					if($i == 0)
					{
			?>
			<tr>
				<td class="auto-style3" colspan="7"><hr></td>
			</tr>
			<?php
					}
			?>
			<tr style="cursor:pointer;<?php if($i % 2 == 0){ echo 'background-color:#B8CEB8"'; } ?>" onclick="MOSTRARPROYECTOS('<?php echo $i; ?>','<?php echo $clave; ?>')">
				<td class="auto-style3" style="width: 27px">
					<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<?php echo $contador; ?>" value="<?php echo $dato['cli_clave_int']; ?>" /></td>
				<td class="auto-style3" style="width: 200px" ><?php echo $nom; ?></td>
				<td class="auto-style3" style="width: 150px" ><?php echo $cencos; ?></td>
				<td class="auto-style3" style="width: 98px"><?php echo $usuact; ?></td>
				<td class="auto-style3" style="width: 127px"><?php echo $fecact; ?></td>
				<td class="auto-style1" style="width: 30px">
				<input name="activarinactivar" id="activarinactivar" type="checkbox" <?php if($act == 1){ echo 'checked="checked"'; } ?> disabled="disabled" ></td>
				<td class="auto-style3">
				<?php 
				if($metodo == 1)
				{
				?>
				<a data-reveal-id="editarciudad" data-animation="fade" style="cursor:pointer" onclick="EDITAR('<?php echo $dato['cli_clave_int']; ?>','CIUDAD')"><img src="../../images/editar.png" alt="" height="22" width="21" /></a>
				<?php
				}
				?>
				</td>
			</tr>
			<tr>
				<td class="auto-style3" colspan="7" style="height:1px">
				<input name="oculto<?php echo $i; ?>" id="oculto<?php echo $i; ?>" value="0" type="hidden" />
				<div id="verproyecto<?php echo $i; ?>" style="display:none"></div>
				</td>
			</tr>
			<?php
				}
			?>
		</table>
		</div>
<div id="nuevaciudad" class="reveal-modal" style="left: 57%; top: 10px; height: 250px; width: 500px;">
	<table style="width: 38%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Cliente:</td>
			<td class="auto-style3">
			<input name="cliente" id="cliente" class="inputs" type="text" style="width: 200px" />
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Centro Costo:</td>
			<td class="auto-style3">
			<input name="centro" id="centro" type="text" style="width: 200px" class="inputs" />
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Activo:</td>
			<td class="auto-style3"><input class="inputs" name="activo" id="activo" checked="checked" type="checkbox" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4">
			<input name="nuevo1" type="button" value="Guardar" onclick="NUEVO()"  style="width: 348px; height: 25px; cursor:pointer" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">
			<div id="datos1">
			<table style="width: 100%">
				<tr>
					<td colspan="3"><strong>AGREGAR PROYECTOS AL CLIENTE</strong></td>
				</tr>
				<tr>
					<td>
					<div id="agregar">
						<select class="auto-style8" disabled="disabled" multiple="multiple" ondblclick="AGREGAR('<?php echo $ciuedi; ?>')" name="agregarpro" id="agregarpro" style="width: 150px" size="6">
						<?php
							$con = mysqli_query($conectar,"select * from proyecto where pro_clave_int NOT IN (select pro_clave_int from cliente_proyecto where cli_clave_int = '".$ciuedi."') and pro_sw_activo = 1 order by pro_nombre");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$clave = $dato['pro_clave_int'];
								$proyecto = $dato['pro_nombre'];
						?>
							<option value="<?php echo $clave; ?>"><?php echo $proyecto; ?></option>
						<?php
							}
						?>
						</select>
					</div>
					</td>
					<td align="center">
					<div style="width: 140px">
						<input type="button" class="pasar izq" disabled="disabled" onclick="AGREGAR('<?php echo $ciuedi; ?>')" value="Pasar &raquo;"><input type="button" onclick="QUITAR('<?php echo $ciuedi; ?>')" disabled="disabled" class="quitar der" value="&laquo; Quitar"><br />
						<input type="button" class="pasartodos izq" disabled="disabled" onclick="PONERTODOS('<?php echo $ciuedi; ?>')" value="Todos &raquo;"><input type="button" onclick="REMOVERTODOS('<?php echo $ciuedi; ?>')" disabled="disabled" class="quitartodos der" value="&laquo; Todos">
					</div>
					</td>
					<td>
					<div id="agregados">
					<select size="6" multiple="multiple" disabled="disabled" class="auto-style8" ondblclick="QUITAR('<?php echo $ciuedi; ?>')" name="quitarpro" id="quitarpro" style="width: 150px" size="6">
					<?php 
			        	$sql = mysqli_query($conectar,"select * from cliente_proyecto cp inner join proyecto p on (p.pro_clave_int = cp.pro_clave_int) where cp.cli_clave_int = '".$ciuedi."' order by p.pro_nombre");
			        	$num = mysqli_num_rows($sql);
			        	for($i = 0; $i < $num; $i++)
			        	{
			        		$dato = mysqli_fetch_array($sql);
			        		$clave = $dato['pro_clave_int'];
			        		$obra = $dato['pro_nombre'];
					?>
						<option value="<?php echo $clave; ?>"><?php echo $obra; ?></option>
					<?php } ?>
			        </select>
					</div>
					</td>
				</tr>
			</table>
			</div>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
</div>
		</td>
	</tr>
	<tr>
		<td style="width: 60px">&nbsp;</td>
		<td style="width: 117px">&nbsp;</td>
		<td style="width: 65px">&nbsp;</td>
		<td style="width: 70px">&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td style="width: 60px">&nbsp;</td>
		<td style="width: 117px">&nbsp;</td>
		<td style="width: 65px">&nbsp;</td>
		<td style="width: 70px">&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>
</form>
</body>
</html>