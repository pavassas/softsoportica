function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function EDITAR(v,m)
{	
	if(m == 'CIUDAD')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('editarciudad').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#editarciudad").html("<img alt='cargando' src='img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?editarciu=si&ciuedi="+v,true);
		ajax.send(null);
		OCULTARSCROLL();
	}
}
function GUARDAR(id)
{
	var cli = form1.cliente1.value;
	var lc = cli.length;
	var cencos = form1.centro1.value;
	var act = form1.activo1.checked;
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('datos').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#datos").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?guardarcli=si&cli="+cli+"&cencos="+cencos+"&act="+act+"&lc="+lc+"&c="+id,true);
	ajax.send(null);
	if(cli != '' && cencos != '' && lc >= 3)
	{
		setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
	}
}
function NUEVO()
{
	var cli = form1.cliente.value;
	var lc = cli.length;
	var cencos = form1.centro.value;
	var act = form1.activo.checked;
		
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('datos1').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#datos1").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?nuevocliente=si&cli="+cli+"&cencos="+cencos+"&act="+act+"&lc="+lc,true);
	ajax.send(null);
	if(cli != '' && cencos != '' && lc >= 3)
	{
		form1.cliente.value = '';
		form1.centro.value = '';
		setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
	}
	OCULTARSCROLL();
}
function CONSULTAMODULO(v)
{
	if(v == 'TODOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('ciudades').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#ciudades").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?todos=si",true);
		ajax.send(null);
		setTimeout("OCULTARSCROLL();",1000);
	}
}
function BUSCAR()
{	
	var cli = form1.buscliente.value;
	var cencos = form1.buscentro.value;
	var act = form1.buscaractivos.value;
			
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('ciudades').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#ciudades").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?buscarcli=si&cli="+cli+"&cencos="+cencos+"&act="+act,true);
	ajax.send(null);
	setTimeout("OCULTARSCROLL();",1000);
}
function AGREGAR(c)
{
	var pro = $("#agregarpro").val();
	
	if(pro != '')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('agregados').innerHTML=ajax.responseText;
		    }
		}
		
		jQuery("#agregados").html("<img alt='cargando' src='../../images/cargando.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?agregarproyecto=si&pro="+pro+"&cli="+c,true);
		ajax.send(null);
		
		for (x=0;x<pro.length;x++)
		{
			$("#agregarpro").find("option[value="+pro[x]+"]").remove();
		}
	}
	else
	{
		alert("Por favor seleccione almenos un registros.");
	}
}
function PONERTODOS(c)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('agregados').innerHTML=ajax.responseText;
	    }
	}
	
	jQuery("#agregados").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?ponertodos=si&cli="+c,true);
	ajax.send(null);
	$('#agregarpro').html('');//Limpia todos los datos del select
}
function QUITAR(c)
{
	var pro = $("#quitarpro").val();
	
	if(pro != '')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('agregar').innerHTML=ajax.responseText;
		    }
		}
		
		jQuery("#agregar").html("<img alt='cargando' src='../../images/cargando.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?quitarproyecto=si&pro="+pro+"&cli="+c,true);
		ajax.send(null);
		
		for (x=0;x<pro.length;x++)
		{
			$("#quitarpro").find("option[value="+pro[x]+"]").remove();
		}
	}
	else
	{
		alert("Por favor seleccione almenos un registros.");
	}
}
function REMOVERTODOS(c)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('agregar').innerHTML=ajax.responseText;
	    }
	}
	
	jQuery("#agregar").html("<img alt='cargando' src='../../images/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?quitartodos=si&cli="+c,true);
	ajax.send(null);
	$('#quitarpro').html('');//Limpia todos los datos del select
}
function MOSTRARPROYECTOS(v,c)
{
    var ane = jQuery('#oculto'+v).val();
    if(ane == 0)
    {
    	jQuery('#oculto'+v).val('1');
    	var div = document.getElementById('verproyecto'+v);
    	div.style.display = 'block'; 
    	var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('verproyecto'+v).innerHTML=ajax.responseText;
		    }
		}
		jQuery("#verproyecto"+v).html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?mostrarproyectos=si&clacli="+c,true);
		ajax.send(null);
    }
    else
    { 
    	jQuery('#oculto'+v).val('0');
    	var div = document.getElementById('verproyecto'+v);
    	div.style.display = 'none';
    }
}