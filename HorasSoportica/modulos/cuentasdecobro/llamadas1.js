function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function COBRAR()
{	
	var clausu = form1.empleado.value;
	var ano = form1.ano.value;
	var mes = form1.mes.value;
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('vistaprevia').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#vistaprevia").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='50' width='50' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?cobrar=si&ano="+ano+"&mes="+mes+"&clausu="+clausu,true);
	ajax.send(null);
	OCULTARSCROLL();
}
function IMPRIMIR()
{
	var clausu = form1.empleado.value;
	var ano = form1.ano.value;
	var mes = form1.mes.value;
	window.open('informes/informeimp.php?ano='+ano+"&mes="+mes+"&clausu="+clausu, '_blank');
}
function EXPORTARPDF()
{
	var clausu = form1.empleado.value;
	var ano = form1.ano.value;
	var mes = form1.mes.value;
	window.open('informes/informepdf.php?ano='+ano+"&mes="+mes+"&clausu="+clausu, '_blank');
}
function ESTADOMES()
{	
	var clausu = form1.empleado.value;
	var ano = form1.ano.value;
	var mes = form1.mes.value;
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('estadomes').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#estadomes").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='50' width='50' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?estadomes=si&ano="+ano+"&mes="+mes+"&clausu="+clausu,true);
	ajax.send(null);
}
function PAGAR(ext)
{	
	var clausu = form1.empleado.value;
	var ano = form1.ano.value;
	var mes = form1.mes.value;
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('mespendiente').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#mespendiente").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='50' width='50' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?pagarmes=si&ano="+ano+"&mes="+mes+"&clausu="+clausu+"&ext="+ext,true);
	ajax.send(null);
}

function MOSTRARRUTA(v)
{
	var nomadj = $('#adjunto'+v).val();
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('resultadoadjunto').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#resultadoadjunto").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarruta=si&nomadj="+nomadj,true);
	ajax.send(null);
}