<style type="text/css">
.auto-style17 {
	background-color: #B8CEB8;
	border-bottom-style: solid;
	border-bottom-width: 1px;
}
.auto-style18 {
	border-top-style: solid;
	border-top-width: 1px;
}
.auto-style19 {
	border-left-style: none;
	border-left-width: medium;
	border-right-style: solid;
	border-right-width: 1px;
	border-top-style: solid;
	border-top-width: 1px;
}
.auto-style20 {
	border-left-style: solid;
	border-left-width: 1px;
	border-right-style: none;
	border-right-width: medium;
	border-top-style: solid;
	border-top-width: 1px;
}
.auto-style21 {
	border-width: 0px;
	border-collapse: collapse;
}
</style>
<?php
	error_reporting(0);
	include('../../data/Conexion.php');
	
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_SESSION['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];
		
	// verifica si no se ha loggeado
	if(!isset($_SESSION["persona"]))
	{
	  session_destroy();
	  header("LOCATION:index.php");
	}else{
	}
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");
	$anocont = date("Y");
	
	$con = mysqli_query($conectar,"select u.usu_clave_int,p.prf_clave_int,p.prf_descripcion,p.prf_sw_actualiza_info,p.prf_tipo_perfil as ti,prf_visual from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
	$dato = mysqli_fetch_array($con);
	$clausu = $dato['usu_clave_int'];
	$claveusu = $dato['usu_clave_int'];
	$claprf = $dato['prf_clave_int'];
	$perfil = $dato['prf_descripcion'];
	$tipoperfil = $dato['ti'];
	$visual = $dato['prf_visual'];
	if($perfil=="Administrador"){$vc = "visible"; $vt="visible";}else 
	if($visual=="1"){$vt="visible"; $vc="hidden";}else 
	if($visual==2){$vt="hidden"; $vc="visible";}else
	if($visual==3){$vt="visible";$vc = "visible"; }else{$vt="hidden";$vc="hidden";}

	$actualizainfo = $dato['prf_sw_actualiza_info'];
	
	$con = mysqli_query($conectar,"select per_metodo from permiso where prf_clave_int = '".$claprf."' and ven_clave_int = 9");
	$dato = mysqli_fetch_array($con);
	$metodo = $dato['per_metodo'];
	
	if($_GET['cobrar'] == 'si')
	{
		$clausu = $_GET['clausu'];
		$ano = $_GET['ano'];
		$mes = $_GET['mes'];
		
		$con = mysqli_query($conectar,"select * from usuario u inner join factor_prestacional f on (f.fac_clave_int = u.fac_clave_int) where u.usu_clave_int = '".$clausu."'");
		$dato = mysqli_fetch_array($con);
		$nom = $dato['usu_nombre'];
		$cc = $dato['usu_cedula'];
		$sal = $dato['usu_salario'];
		$fac = $dato['fac_tarifa'];
		$clafac = $dato['fac_clave_int'];//1 = Prestador de servicios, otros: Nomina
		$rut = $dato['usu_rut'];
		$vrhor = $dato['usu_vr_hora'];
		
		if($clafac == 1)
		{
			if($mes == 01 or $mes == 1)
			{
				$newano = $ano-1;
				$newmes = 12;
			}
			else
			{
				$newano = $ano;
				$newmes = $mes-1;
			}
			$fi = $newano."-".$newmes.'-20';
			$ff = $ano."-".$mes.'-19';
		}
		else
		{
			$fi = $ano."-".$mes.'-01';
			$con = mysqli_query($conectar,"select LAST_DAY('".$fi."') ultimodia from usuario limit 1");
			$dato = mysqli_fetch_array($con);
			$ff = $dato['ultimodia'];
		}
		
		$convaloresreales = mysqli_query($conectar,"select * from horas_h hh inner join horas_d hd on (hd.hoh_clave_int = hh.hoh_clave_int) inner join usuario u on (u.usu_clave_int = hh.usu_clave_int) where u.usu_clave_int = '".$clausu."' and hh.hoh_fecha BETWEEN '".$fi."' and '".$ff."' ORDER BY hod_fec_actualiz DESC LIMIT 1");
		$datovalreal = mysqli_fetch_array($convaloresreales);
		$sal = $datovalreal['hod_salario_base'];
		$fac = $datovalreal['hod_fac_prestacional'];
		$clafac = $datovalreal['fac_clave_int'];//1 = Prestador de servicios, otros: Nomina
		$vrhor = $datovalreal['hod_vr_hora'];
		
		//if($clafac == 1)
		//{
			
			
			$salnet = $sal*$fac;
			$vrhornet = $vrhor*$fac;
			$salbru = $sal*$fac;
			
		/*}
		else
		{
			$salnet = $sal;
			$vrhornet = $vrhor;
		}*/
		
		$con = mysqli_query($conectar,"select COUNT(*) horas from horas_h hh inner join horas_d hd on (hd.hoh_clave_int = hh.hoh_clave_int) inner join usuario u on (u.usu_clave_int = hh.usu_clave_int) inner join cliente c on (c.cli_clave_int = hd.cli_clave_int) inner join proyecto p on (p.pro_clave_int = hd.pro_clave_int) where u.usu_clave_int = '".$clausu."' and hh.hoh_fecha BETWEEN '".$fi."' and '".$ff."' GROUP BY hh.usu_clave_int");
		$dato = mysqli_fetch_array($con);
		$horasmes = $dato['horas'];
		
		//VALIDACION DE FACTOR PRESTACIONAÑ
		if($clafac==1)
		{
		  $vrhormes = "$".number_format($salbru/$horasmes,0 , "," ,".");
		}
		else if($clafac==2 || $clafac ==3)
		{
		  $vrhormes = "$".number_format($sal/$horasmes,0 , "," ,".");
		}
		$concobro = mysqli_query($conectar,"select * from cuenta_cobro where usu_clave_int = '".$clausu."' and cuc_ano = '".$ano."' and cuc_mes = '".$mes."'");
		$numcobro = mysqli_num_rows($concobro);
		?>
		<table id="impresion" style="width: 50%;" align="center" class="auto-style21">
			<tr id="fila1">
				<td class="auto-style20" style="background-color:#E1E5E1">
				<table style="width: 40%;text-align:center" align="left">
					<tr>
						<td>
						<?php
						if($numcobro > 0)
						{
						?>
						<img src="../../images/pdf.png" onclick="EXPORTARPDF()" style="height:45px;cursor:pointer" title="EXPORTAR">
						<?php
						}
						?>
						</td>
						<td>
						<?php
						if($numcobro > 0)
						{
						?>
						<img src="../../images/Imp.png" onclick="IMPRIMIR()" style="height:45px;cursor:pointer" title="IMPRIMIR">
						<?php
						}
						?>
						</td>
						<td>
						<?php
						$con = mysqli_query($conectar,"select * from cuenta_cobro where usu_clave_int = '".$clausu."' and cuc_ano = '".$ano."' and cuc_mes = '".$mes."'");
						$num = mysqli_num_rows($con); $numc = $num;
						if($num > 0)
						{ 
							echo '<div class="ok">MES PAGADO</div>'; 
						}
						else
						{ 
							echo '<div id="mespendiente"><div class="validaciones">MES PENDIENTE</div></div>';
							if(strtoupper($perfil) <> strtoupper('Empleado') and strtoupper($perfil) <> strtoupper('Empleados'))
							{
								echo "<input type='image' name='imageField' src='images/pagar.png'  onclick=ADJUNTAR('".$clausu."','NO') value='Subir' class='btnSubmit'/>";
								//echo "<img src='images/pagar.png' onclick=ADJUNTAR('".$clausu."','NO') style='cursor:pointer' />";
							}
						}
						?>
						</td>
					</tr>
				</table>
				</td>
				<td class="auto-style19" style="background-color:#E1E5E1">
				
				<table style="width: 100%">
					<tr>
						<td>Desde:</td>
						<td><?php echo $fi; ?></td>
					</tr>
					<tr>
						<td>Hasta:</td>
						<td><?php echo $ff; ?></td>
					</tr>
				</table>
				
				</td>
			</tr>
			<tr>
				<td class="auto-style12" colspan="2">
				
				<table style="width: 100%;background-color:#B8CEB8;font-size:small;font-family:Arial, Helvetica, sans-serif">
					<tr>
						<td style="width: 180px"><strong>NOMBRE:</strong></td>
						<td><?php echo $nom; ?></td>
						<td style="width: 140px"><strong>RUT:</strong></td>
						<td><?php echo $rut; ?></td>
					</tr>
					<tr>
						<td style="width: 180px"><strong>CC.:</strong></td>
						<td><?php echo $cc; ?></td>
						<td style="width: 140px"><strong>HORAS MES:</strong></td>
						<td><?php echo '193'; ?></td>
					</tr>
					<tr>
						<td style="width: 180px; visibility:<?php echo $vc;?>"><strong><?php if($clafac == 1){ echo "HONORARIO"; }else{ echo "SALARIO"; } ?> NETO:</strong></td>
						<td style="visibility:<?php echo $vc;?>"><?php echo "$".number_format($sal,0 , "," ,"."); ?></td>
						<td style="width: 140px; visibility:<?php echo $vc;?>"><strong>VLR. HORA NETO:</strong></td>
						<td style="visibility:<?php echo $vc;?>"><?php echo "$".number_format($vrhor,0 , "," ,"."); ?></td>
					</tr>
					<tr>
						<td style="width: 180px"><strong>FACTOR PRESTACIONAL:</strong></td>
						<td><?php echo $fac; ?></td>
						<td style="width: 140px; visibility:<?php echo $vc;?>"><strong>VLR. HORA BRUTO:</strong></td>
						<td style="visibility:<?php echo $vc;?>"><?php echo "$".number_format($vrhornet,0 , "," ,"."); ?></td>
					</tr>
					<tr>
						<td style="width: 180px; visibility:<?php echo $vc;?>"><strong><?php if($clafac == 1){ echo "HONORARIO"; }else{ echo "SALARIO"; } ?> BRUTO:</strong></td>
						<td style="visibility:<?php echo $vc;?>"><?php echo "$".number_format($salbru,0 , "," ,"."); ?></td>
						<td style="width: 140px"></td>
						<td>&nbsp;</td>
					</tr>
				</table>
				
				</td>
			</tr>
			<tr>
				<td class="auto-style10" style="background-color:#E1E5E1" colspan="2">
				
				<table style="width: 100%;font-size:small;font-family:Arial, Helvetica, sans-serif">
					<tr>
						<td style="visibility:<?php echo $vt;?>"><strong>TOTAL HORAS TRABAJADAS EN EL MES:</strong></td>
						<td style="visibility:<?php echo $vt;?>"><?php echo $horasmes; ?></td>
					</tr>
					<tr>
						<td style="visibility:<?php echo $vc;?>"><strong>VALOR HORA MES:</strong></td>
						<td style="visibility:<?Php echo $vc;?>"><?php echo $vrhormes; ?></td>
					</tr>
				</table>
				
				</td>
			</tr>
			<tr>
				<td class="auto-style10" colspan="2">
				
				<table style="width: 100%;font-size:small;font-family:Arial, Helvetica, sans-serif" class="auto-style16">
					<tr>
						<td class="auto-style17"><strong>PROYECTO</strong></td>
						<td class="auto-style17"><strong>COD.</strong></td>
						<td style="text-align:center" class="auto-style17"><strong>CLIENTE</strong></td>
						<td class="auto-style17" style="visibility:<?php echo $vt;?>"><strong>TOTAL HORAS</strong></td>
						<td class="auto-style17" style="visibility:<?php echo $vc;?>">
						<strong>%</strong>
						</td>
					</tr>
					<?php
					$sumhor = 0;
					$sumcos = 0;
					
					$conhoras = mysqli_query($conectar,"select COUNT(*) totalhoras from horas_h hh inner join horas_d hd on (hd.hoh_clave_int = hh.hoh_clave_int) inner join usuario u on (u.usu_clave_int = hh.usu_clave_int) inner join cliente c on (c.cli_clave_int = hd.cli_clave_int) inner join proyecto p on (p.pro_clave_int = hd.pro_clave_int) where u.usu_clave_int = '".$clausu."' and hh.hoh_fecha BETWEEN '".$fi."' and '".$ff."'");
					$datohoras = mysqli_fetch_array($conhoras);
					$totalhoras = $datohoras['totalhoras'];
					
					$con = mysqli_query($conectar,"select p.pro_codigo,p.pro_nombre,c.cli_nombre cliente,c.cli_cen_cos,COUNT(*) horas,hd.hod_vr_hora,hd.hod_fac_prestacional, CASE WHEN hd.fac_clave_int = 1 THEN (COUNT(*)*u.usu_vr_hora)*hd.hod_fac_prestacional ELSE COUNT(*)*u.usu_vr_hora END total from horas_h hh inner join horas_d hd on (hd.hoh_clave_int = hh.hoh_clave_int) inner join usuario u on (u.usu_clave_int = hh.usu_clave_int) inner join cliente c on (c.cli_clave_int = hd.cli_clave_int) inner join proyecto p on (p.pro_clave_int = hd.pro_clave_int) where u.usu_clave_int = '".$clausu."' and hh.hoh_fecha BETWEEN '".$fi."' and '".$ff."' GROUP BY hh.usu_clave_int,p.pro_clave_int,hd.cli_clave_int");
					$num = mysqli_num_rows($con);
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$pro = $dato['pro_nombre'];
						$cod = $dato['cli_cen_cos'];
						$cli = $dato['cliente'];
						$hor = $dato['horas'];
						$sumhor = $sumhor+$hor;
						if($clafac==1)
						{
						$costo = $hor*($salbru/$horasmes);
						$sumcos = $sumcos+($hor*($salbru/$horasmes));
						}
						else if($clafac==2 || $clafac==3)
						{
						$costo = $hor*($sal/$horasmes);
						$sumcos = $sumcos+($hor*($sal/$horasmes));
						}
					?>
					<tr>
						<td style="height: 7px" class="auto-style18"></td>
						<td style="height: 7px;text-align:center" class="auto-style18"></td>
						<td style="height: 7px" class="auto-style18"></td>
						<td style="height: 7px;text-align:center; visibility:<?php echo $vt;?>" class="auto-style18"></td>
						<td style="height: 7px; visibility:<?php echo $vc;?>" class="auto-style18"></td>
					</tr>
					<tr>
						<td style="height: 10px" class="auto-style13"><?php echo $pro; ?></td>
						<td style="height: 10px;text-align:center" class="auto-style13"><?php echo $cod; ?></td>
						<td style="height: 10px" class="auto-style13"><?php echo $cli; ?></td>
						<td style="height: 10px;text-align:center; visibility:<?php echo $vt;?>" class="auto-style13"><?php echo $hor; ?></td>
						<td style="height: 10px; visibility:<?php echo $vc;?>" class="auto-style13">
						<?php
						echo ROUND(($hor/$totalhoras)*100,2)."%";
						?>
						</td>
					</tr>
					<?php
					}
					?>
					<tr>
						<td class="auto-style14">&nbsp;</td>
						<td class="auto-style14">&nbsp;</td>
						<td class="auto-style15"><strong>TOTAL:</strong></td>
						<td style="text-align:center; visibility:<?php echo $vt;?>" class="auto-style15"><?php echo $sumhor; ?></td>
						<td class="auto-style15" style="visibility:<?php echo $vc; ?>">
						100%</td>
					</tr>
				</table>
				
				</td>
			</tr>
			<tr>
				<td class="auto-style11" colspan="2">
				
				<table style="width: 100%">
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td><strong>VoBo jefe inmediato:</strong></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td style="height: 65px">
                        <?php if($numc<=0)
						{
							?>
                        <div class="file-wrapper" style="width:60%;text-align:center;float:left">
                        <input name="adjunto3" id="adjunto3" onchange="MOSTRARRUTA('3')" value="" type="file" class="file" style="cursor:pointer; right: 57; top: 0;" /><span class="button">Adjuntar Archivo</span></div><?php } ?>
                        <div id="resultadoadjunto" style="width:35%; float:left;font-size:8px">
                        <?php
						$confirj = mysqli_query($conectar,"select cuc_firma from cuenta_cobro where usu_clave_int = '".$clausu."' and cuc_ano = '".$ano."' and cuc_mes = '".$mes."'");
						$datofirj = mysqli_fetch_array($confirj);
						$firj = $datofirj['cuc_firma'];
						if($firj == '' or $firj == null)
						{
							echo "Sin Firma";
						}
						else
						{
						?>
						<img src="<?php echo "../cuentasdecobro/".$firj; ?>?date=<?php echo $fecha; ?>" height="40" width="110" />
						<?php
						}
						?>
                        </div>
                        </td>
						<td style="height: 65px">
						<div id="firma">
						<?php
						$confir = mysqli_query($conectar,"select usu_firma from usuario where usu_clave_int = '".$clausu."'");
						$datofir = mysqli_fetch_array($confir);
						$fir = $datofir['usu_firma'];
						if($fir == '' or $fir == null)
						{
							echo "Sin Firma";
						}
						else
						{
						?>
						<img src="<?php echo "../usuarios/".$fir; ?>?date=<?php echo $fecha; ?>" height="40" width="110" />
						<?php
						}
						?>
						</div>
						</td>
					</tr>
					<tr>
						<td colspan="2"><hr></td>
					</tr>
					<tr style="background-color:#B8CEB8;border-collapse:collapse">
						<td>Firma Jefe Inmediato</td>
						<td>Firma Empleado</td>
					</tr>
				</table>
				
				</td>
			</tr>
		</table>
		<?php
		exit();
	}
	if($_GET['estadomes'] == 'si')
	{
		$clausu = $_GET['clausu'];
		$ano = $_GET['ano'];
		$mes = $_GET['mes'];
		
		$conmes = mysqli_query($conectar,"select * from cuenta_cobro where cuc_ano = '".$ano."' and cuc_mes = 1 and usu_clave_int = '".$clausu."'");
		$num1 = mysqli_num_rows($conmes);
		$conmes = mysqli_query($conectar,"select * from cuenta_cobro where cuc_ano = '".$ano."' and cuc_mes = 2 and usu_clave_int = '".$clausu."'");
		$num2 = mysqli_num_rows($conmes);
		$conmes = mysqli_query($conectar,"select * from cuenta_cobro where cuc_ano = '".$ano."' and cuc_mes = 3 and usu_clave_int = '".$clausu."'");
		$num3 = mysqli_num_rows($conmes);
		$conmes = mysqli_query($conectar,"select * from cuenta_cobro where cuc_ano = '".$ano."' and cuc_mes = 4 and usu_clave_int = '".$clausu."'");
		$num4 = mysqli_num_rows($conmes);
		$conmes = mysqli_query($conectar,"select * from cuenta_cobro where cuc_ano = '".$ano."' and cuc_mes = 5 and usu_clave_int = '".$clausu."'");
		$num5 = mysqli_num_rows($conmes);
		$conmes = mysqli_query($conectar,"select * from cuenta_cobro where cuc_ano = '".$ano."' and cuc_mes = 6 and usu_clave_int = '".$clausu."'");
		$num6 = mysqli_num_rows($conmes);
		$conmes = mysqli_query($conectar,"select * from cuenta_cobro where cuc_ano = '".$ano."' and cuc_mes = 7 and usu_clave_int = '".$clausu."'");
		$num7 = mysqli_num_rows($conmes);
		$conmes = mysqli_query($conectar,"select * from cuenta_cobro where cuc_ano = '".$ano."' and cuc_mes = 8 and usu_clave_int = '".$clausu."'");
		$num8 = mysqli_num_rows($conmes);
		$conmes = mysqli_query($conectar,"select * from cuenta_cobro where cuc_ano = '".$ano."' and cuc_mes = 9 and usu_clave_int = '".$clausu."'");
		$num9 = mysqli_num_rows($conmes);
		$conmes = mysqli_query($conectar,"select * from cuenta_cobro where cuc_ano = '".$ano."' and cuc_mes = 10 and usu_clave_int = '".$clausu."'");
		$num10 = mysqli_num_rows($conmes);
		$conmes = mysqli_query($conectar,"select * from cuenta_cobro where cuc_ano = '".$ano."' and cuc_mes = 11 and usu_clave_int = '".$clausu."'");
		$num11 = mysqli_num_rows($conmes);
		$conmes = mysqli_query($conectar,"select * from cuenta_cobro where cuc_ano = '".$ano."' and cuc_mes = 12 and usu_clave_int = '".$clausu."'");
		$num12 = mysqli_num_rows($conmes);
		?>
		<select name="mes" id="mes" onchange="COBRAR()" class="inputs" style="width: 300px">
		<option value="">-Seleccione-</option>
		<option value="01" style="color:<?php if($num1 <= 0){ echo 'red'; }else{ echo 'green'; } ?>">ENERO - <?php if($num1 <= 0){ echo 'PENDIENTE'; }else{ echo 'PAGADO'; } ?></option>
		<option value="02" style="color:<?php if($num2 <= 0){ echo 'red'; }else{ echo 'green'; } ?>">FEBRERO - <?php if($num2 <= 0){ echo 'PENDIENTE'; }else{ echo 'PAGADO'; } ?></option>
		<option value="03" style="color:<?php if($num3 <= 0){ echo 'red'; }else{ echo 'green'; } ?>">MARZO - <?php if($num3 <= 0){ echo 'PENDIENTE'; }else{ echo 'PAGADO'; } ?></option>
		<option value="04" style="color:<?php if($num4 <= 0){ echo 'red'; }else{ echo 'green'; } ?>">ABRIL - <?php if($num4 <= 0){ echo 'PENDIENTE'; }else{ echo 'PAGADO'; } ?></option>
		<option value="05" style="color:<?php if($num5 <= 0){ echo 'red'; }else{ echo 'green'; } ?>">MAYO - <?php if($num5 <= 0){ echo 'PENDIENTE'; }else{ echo 'PAGADO'; } ?></option>
		<option value="06" style="color:<?php if($num6 <= 0){ echo 'red'; }else{ echo 'green'; } ?>">JUNIO - <?php if($num6 <= 0){ echo 'PENDIENTE'; }else{ echo 'PAGADO'; } ?></option>
		<option value="07" style="color:<?php if($num7 <= 0){ echo 'red'; }else{ echo 'green'; } ?>">JULIO - <?php if($num7 <= 0){ echo 'PENDIENTE'; }else{ echo 'PAGADO'; } ?></option>
		<option value="08" style="color:<?php if($num8 <= 0){ echo 'red'; }else{ echo 'green'; } ?>">AGOSTO - <?php if($num8 <= 0){ echo 'PENDIENTE'; }else{ echo 'PAGADO'; } ?></option>
		<option value="09" style="color:<?php if($num9 <= 0){ echo 'red'; }else{ echo 'green'; } ?>">SEPTIEMBRE - <?php if($num9 <= 0){ echo 'PENDIENTE'; }else{ echo 'PAGADO'; } ?></option>
		<option value="10" style="color:<?php if($num10 <= 0){ echo 'red'; }else{ echo 'green'; } ?>">OCTUBRE - <?php if($num10 <= 0){ echo 'PENDIENTE'; }else{ echo 'PAGADO'; } ?></option>
		<option value="11" style="color:<?php if($num11 <= 0){ echo 'red'; }else{ echo 'green'; } ?>">NOVIEMBRE - <?php if($num11 <= 0){ echo 'PENDIENTE'; }else{ echo 'PAGADO'; } ?></option>
		<option value="12" style="color:<?php if($num12 <= 0){ echo 'red'; }else{ echo 'green'; } ?>">DICIEMBRE - <?php if($num12 <= 0){ echo 'PENDIENTE'; }else{ echo 'PAGADO'; } ?></option>
		</select>
		<?php
		exit();
	}
	if($_GET['pagarmes'] == 'si')
	{
		$fecha=date("Y/m/d H:i:s");
		$clausu = $_GET['clausu'];
		$ano = $_GET['ano'];
		$mes = $_GET['mes']; $mese = $mes;
		$ext = $_GET['ext'];
		if($mes == '01'){ $mes = 1; }elseif($mes == '02'){ $mes = 2; }elseif($mes == '03'){ $mes = 3; }elseif($mes == '04'){ $mes = 4; }elseif($mes == '05'){ $mes = 5; }elseif($mes == '06'){ $mes = 6; }elseif($mes == '07'){ $mes = 7; }elseif($mes == '08'){ $mes = 8; }elseif($mes == '09'){ $mes = 9; }
		$conmes = mysqli_query($conectar,"select * from cuenta_cobro where cuc_ano = '".$ano."' and cuc_mes = '".$mes."' and usu_clave_int = '".$clausu."'");
		$num1 = mysqli_num_rows($conmes);
		if($num1 <= 0)
		{
			$nombre_fichero = 'tmp/'.$ano."".$mese."".$clausu."".$ext;
			
			if (file_exists($nombre_fichero)) 
			{
				$trozos = explode(".", $nombre_fichero); 
				$rutaold = $nombre_fichero;
				$extension = end($trozos);
				
				$rutan = 'firmasjefe/'.$ano."".$mese."".$clausu."".$ext;
				if(rename($rutaold,$rutan))
					{
						//echo "El fichero $nombre_fichero existe";
						$sql = mysqli_query($conectar,"insert into cuenta_cobro(usu_clave_int,cuc_ano,cuc_mes,cuc_usu_actualiz,cuc_fec_actualiz,cuc_firma) values('".$clausu."','".$ano."','".$mes."','".$usuario."','".$fecha."','".$rutan."')");
						if($sql > 0)
						{
						echo '<div class="ok">MES PAGADO</div>';
						}
					}
					else
					{
						echo "<div class='validaciones'>No se movio la firma</div>";
					}
			}
			else 
			{
				echo "<div class='validaciones'>El fichero $nombre_fichero no existe</div>";
			}
		}
		else 
		{
		    $nombre_fichero = 'tmp/'.$ano."".$mese."".$clausu."".$ext;
			
			if (file_exists($nombre_fichero)) 
			{
				$trozos = explode(".", $nombre_fichero); 
				$rutaold = $nombre_fichero;
				$extension = end($trozos);
				
				$rutan = 'firmasjefe/'.$ano."".$mese."".$clausu."".$ext;
				if(rename($rutaold,$rutan))
					{
						$update = mysqli_query($conectar,"UPDATE cuenta_cobro SET cuc_firma = '".$rutan."' where cuc_ano = '".$ano."' and cuc_mes = '".$mes."' and usu_clave_int = '".$clausu."'");
					}
			}
		}
		exit();
	}
	if($_GET['mostrarruta'] == 'si')
	{
		$nomadj = $_GET['nomadj'];
		echo "<div class='ok' style='width: 100%;font-size:8px' align='center'>$nomadj</div>";
		exit();
	}
?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>SEGUIMIENTO CAJEROS</title>
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />

<?php //VALIDACIONES ?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="llamadas1.js?<?php echo time(); ?>"></script>

<script type="text/javascript" src="../../js/jquery.searchabledropdown-1.0.8.min1.js"></script>

<script>
$(document).ready(function() {
	$("select").searchable();
});
function OCULTARSCROLL()
{
	parent.autoResize('iframe8');
	setTimeout("parent.autoResize('iframe8')",500);
	setTimeout("parent.autoResize('iframe8')",1000);
	setTimeout("parent.autoResize('iframe8')",1500);
	setTimeout("parent.autoResize('iframe8')",2000);
	setTimeout("parent.autoResize('iframe8')",2500);
	setTimeout("parent.autoResize('iframe8')",3000);
	setTimeout("parent.autoResize('iframe8')",3500);
	setTimeout("parent.autoResize('iframe8')",4000);
	setTimeout("parent.autoResize('iframe8')",4500);
	setTimeout("parent.autoResize('iframe8')",5000);
	setTimeout("parent.autoResize('iframe8')",5500);
	setTimeout("parent.autoResize('iframe8')",6000);
	setTimeout("parent.autoResize('iframe8')",6500);
	setTimeout("parent.autoResize('iframe8')",7000);
	setTimeout("parent.autoResize('iframe8')",7500);
	setTimeout("parent.autoResize('iframe8')",8000);
	setTimeout("parent.autoResize('iframe8')",8500);
	setTimeout("parent.autoResize('iframe8')",9000);
	setTimeout("parent.autoResize('iframe8')",9500);
	setTimeout("parent.autoResize('iframe8')",10000);
}
parent.autoResize('iframe8');
setTimeout("parent.autoResize('iframe8')",500);
setTimeout("parent.autoResize('iframe8')",1000);
setTimeout("parent.autoResize('iframe8')",1500);
setTimeout("parent.autoResize('iframe8')",2000);
setTimeout("parent.autoResize('iframe8')",2500);
setTimeout("parent.autoResize('iframe8')",3000);
setTimeout("parent.autoResize('iframe8')",3500);
setTimeout("parent.autoResize('iframe8')",4000);
setTimeout("parent.autoResize('iframe8')",4500);
setTimeout("parent.autoResize('iframe8')",5000);
setTimeout("parent.autoResize('iframe8')",5500);
setTimeout("parent.autoResize('iframe8')",6000);
setTimeout("parent.autoResize('iframe8')",6500);
setTimeout("parent.autoResize('iframe8')",7000);
setTimeout("parent.autoResize('iframe8')",7500);
setTimeout("parent.autoResize('iframe8')",8000);
setTimeout("parent.autoResize('iframe8')",8500);
setTimeout("parent.autoResize('iframe8')",9000);
setTimeout("parent.autoResize('iframe8')",9500);
setTimeout("parent.autoResize('iframe8')",10000);
</script>
<script type="text/javascript">
function ADJUNTAR(clausu,act)
{	
	
	var archivo = $('#adjunto3').val();	
	var ano = $('#ano').val();
	var mes = $('#mes').val();	
	var extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
	
	if(archivo=='' || archivo==null)
	{
		var num = 0;
		$("#form1").on('submit',(function(e)
		{			
			e.preventDefault();
			if(num==0)
			{
		    alert("Adjuntar la firma del jefe inmediato");	
			num = 1;
			}
		}));
	}
	else
	if(archivo != '' && extension.toUpperCase() != '.png'.toUpperCase() && extension.toUpperCase() != '.jpg'.toUpperCase() && extension.toUpperCase() != '.gif'.toUpperCase() && extension.toUpperCase() != '.BMP'.toUpperCase() && extension.toUpperCase() != '.JPEG'.toUpperCase() && extension.toUpperCase() != '.TIF'.toUpperCase())
	{
		var num = 0;
		$("#form1").on('submit',(function(e)
		{	
			
			e.preventDefault();
			if(num==0)
			{
		    alert("Solo se permiten imagenes");
			num=1;
			}
			
		}));
	}
	else
	{
		var num = 0;
		$("#form1").on('submit',(function(e)
		{
			
			e.preventDefault();
			if(num == 0)
			{
				$.ajax({
		        	url: "upload3.php?clausu="+clausu+"&act="+act+"&ano="+ano+"&mes="+mes,
					type: "POST",
					data:  new FormData(this),
					contentType: false,
		    	    cache: false,
					processData:false,
					success: function(data)
				    {
				    	jQuery("#resultadoadjunto").html("<div class='ok'>CORRECTO</div>");
						setTimeout(PAGAR(extension),1000);
				    },
				  	error: function() 
			    	{
			    		//alert("ERROR");
			    	} 	        
			   });
			   num = 1;
			}
		}));
	}
}
</script>
</head>
<body style="background-color:#FDFDFC" id="content">
<form name="form1" id="form1" method="post">
<!--[if lte IE 7]>
<div class="ieWarning">Este navegador no es compatible con el sistema. Por favor, use Chrome, Safari, Firefox o Internet Explorer 8 o superior.</div>
<![endif]-->
	<input name="ocultoestado" id="ocultoestado" value="" type="hidden" />
	<table style="width: 100%">
		<tr>
			<td class="auto-style2" colspan="5" align="center">
			<div id="cajeros">
				<table style="width:100%">
			<tr>
				<td align="center" class="auto-style2">
					<table style="width: 45%" align="center">
						<tr>
							<td style="width: 45px; height: 31px" class="alinearizq">
							<strong>USUARIO:</strong></td>
							<td style="height: 31px; " class="alinearizq">
							<select name="empleado" id="empleado" title="<?php echo $tipoperfil."-".$usuario;?>" onchange="ESTADOMES()" class="inputs" style="width: 300px;background:#5A825A;color:white">
							<?php
								if((strtoupper($perfil) != strtoupper('Administrador') and strtoupper($perfil) != strtoupper('Administradores') and strtoupper($perfil) != strtoupper('Admin')) or ($metodo == 0))
								{
									if($tipoperfil=="4")
									{
								     $con = mysqli_query($conectar,"select usu_clave_int,usu_nombre,usu_cedula from usuario where usu_clave_int  in(select usu_clave_int_asig from usuario u join usuarios_asig a on a.usu_clave_int_asig = u.usu_clave_int where a.usu_clave_int  = '".$claveusu."') or usu_clave_int='".$claveusu."'");
									}
									else
									{
									$con = mysqli_query($conectar,"select usu_clave_int,usu_nombre,usu_cedula from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."' order by u.usu_nombre");
									
									
									}
									
								}
								else
								{
									echo '<option value="">-Seleccione-</option>';
									$con = mysqli_query($conectar,"select usu_clave_int,usu_nombre,usu_cedula from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) order by u.usu_nombre");
								}
								$num = mysqli_num_rows($con);
								for($i = 1; $i <= $num; $i++)
								{
									$dato = mysqli_fetch_array($con);
									$clave = $dato['usu_clave_int'];
									$nombre = $dato['usu_nombre'];
									$ced = $dato['usu_cedula'];
							?>
								<option value="<?php echo $clave; ?>" <?php if(strtoupper($perfil) == strtoupper('Empleado') or strtoupper($perfil) == strtoupper('Empleados')){ echo 'selected="selected"'; } ?>><?php echo $ced." - ".$nombre; ?></option>
							<?php
								}
							?>
							</select>
							</td>
							<td style="height: 31px; " class="alinearizq">&nbsp;
							</td>
							<td style="width: 150px;text-align:center" rowspan="3" align="center">
							<img src="images/cuenta.png" height="90" width="110" align="center">
							</td>
						</tr>
						<tr>
							<td style="width: 45px; height: 31px" class="alinearizq">
							<strong>AÑO:</strong></td>
							<td style="height: 31px; " class="alinearizq">
							<select name="ano" id="ano" onchange="COBRAR();ESTADOMES()" class="inputs" style="width: 300px">
							<?php
							for($i = 2015; $i <= 2030; $i++)
							{
								?>
								<option value="<?php echo $i; ?>" <?php if(date('Y') == $i){ echo 'selected="selected"'; } ?>><?php echo $i; ?></option>
								<?php
							}
							?>
							</select>
							</td>
							<td style="height: 31px; " class="alinearizq">&nbsp;
							</td>
						</tr>
						<tr>
							<td style="width: 45px; height: 31px;" class="alinearizq">
							<strong>MES:</strong></td>
							<td style="height: 31px;" class="alinearizq">
							<div id="estadomes">
							<?php
							$conmes = mysqli_query($conectar,"select * from cuenta_cobro where cuc_ano = ".date("Y")." and cuc_mes = 1 and usu_clave_int = '".$clausu."'");
							$num1 = mysqli_num_rows($conmes);
							$conmes = mysqli_query($conectar,"select * from cuenta_cobro where cuc_ano = ".date("Y")." and cuc_mes = 2 and usu_clave_int = '".$clausu."'");
							$num2 = mysqli_num_rows($conmes);
							$conmes = mysqli_query($conectar,"select * from cuenta_cobro where cuc_ano = ".date("Y")." and cuc_mes = 3 and usu_clave_int = '".$clausu."'");
							$num3 = mysqli_num_rows($conmes);
							$conmes = mysqli_query($conectar,"select * from cuenta_cobro where cuc_ano = ".date("Y")." and cuc_mes = 4 and usu_clave_int = '".$clausu."'");
							$num4 = mysqli_num_rows($conmes);
							$conmes = mysqli_query($conectar,"select * from cuenta_cobro where cuc_ano = ".date("Y")." and cuc_mes = 5 and usu_clave_int = '".$clausu."'");
							$num5 = mysqli_num_rows($conmes);
							$conmes = mysqli_query($conectar,"select * from cuenta_cobro where cuc_ano = ".date("Y")." and cuc_mes = 6 and usu_clave_int = '".$clausu."'");
							$num6 = mysqli_num_rows($conmes);
							$conmes = mysqli_query($conectar,"select * from cuenta_cobro where cuc_ano = ".date("Y")." and cuc_mes = 7 and usu_clave_int = '".$clausu."'");
							$num7 = mysqli_num_rows($conmes);
							$conmes = mysqli_query($conectar,"select * from cuenta_cobro where cuc_ano = ".date("Y")." and cuc_mes = 8 and usu_clave_int = '".$clausu."'");
							$num8 = mysqli_num_rows($conmes);
							$conmes = mysqli_query($conectar,"select * from cuenta_cobro where cuc_ano = ".date("Y")." and cuc_mes = 9 and usu_clave_int = '".$clausu."'");
							$num9 = mysqli_num_rows($conmes);
							$conmes = mysqli_query($conectar,"select * from cuenta_cobro where cuc_ano = ".date("Y")." and cuc_mes = 10 and usu_clave_int = '".$clausu."'");
							$num10 = mysqli_num_rows($conmes);
							$conmes = mysqli_query($conectar,"select * from cuenta_cobro where cuc_ano = ".date("Y")." and cuc_mes = 11 and usu_clave_int = '".$clausu."'");
							$num11 = mysqli_num_rows($conmes);
							$conmes = mysqli_query($conectar,"select * from cuenta_cobro where cuc_ano = ".date("Y")." and cuc_mes = 12 and usu_clave_int = '".$clausu."'");
							$num12 = mysqli_num_rows($conmes);
							?>
							<select name="mes" id="mes" onchange="COBRAR()" class="inputs" style="width: 300px">
							<option value="">-Seleccione-</option>
							<option value="01" style="color:<?php if($num1 <= 0){ echo 'red'; }else{ echo 'green'; } ?>">ENERO - <?php if($num1 <= 0){ echo 'PENDIENTE'; }else{ echo 'PAGADO'; } ?></option>
							<option value="02" style="color:<?php if($num2 <= 0){ echo 'red'; }else{ echo 'green'; } ?>">FEBRERO - <?php if($num2 <= 0){ echo 'PENDIENTE'; }else{ echo 'PAGADO'; } ?></option>
							<option value="03" style="color:<?php if($num3 <= 0){ echo 'red'; }else{ echo 'green'; } ?>">MARZO - <?php if($num3 <= 0){ echo 'PENDIENTE'; }else{ echo 'PAGADO'; } ?></option>
							<option value="04" style="color:<?php if($num4 <= 0){ echo 'red'; }else{ echo 'green'; } ?>">ABRIL - <?php if($num4 <= 0){ echo 'PENDIENTE'; }else{ echo 'PAGADO'; } ?></option>
							<option value="05" style="color:<?php if($num5 <= 0){ echo 'red'; }else{ echo 'green'; } ?>">MAYO - <?php if($num5 <= 0){ echo 'PENDIENTE'; }else{ echo 'PAGADO'; } ?></option>
							<option value="06" style="color:<?php if($num6 <= 0){ echo 'red'; }else{ echo 'green'; } ?>">JUNIO - <?php if($num6 <= 0){ echo 'PENDIENTE'; }else{ echo 'PAGADO'; } ?></option>
							<option value="07" style="color:<?php if($num7 <= 0){ echo 'red'; }else{ echo 'green'; } ?>">JULIO - <?php if($num7 <= 0){ echo 'PENDIENTE'; }else{ echo 'PAGADO'; } ?></option>
							<option value="08" style="color:<?php if($num8 <= 0){ echo 'red'; }else{ echo 'green'; } ?>">AGOSTO - <?php if($num8 <= 0){ echo 'PENDIENTE'; }else{ echo 'PAGADO'; } ?></option>
							<option value="09" style="color:<?php if($num9 <= 0){ echo 'red'; }else{ echo 'green'; } ?>">SEPTIEMBRE - <?php if($num9 <= 0){ echo 'PENDIENTE'; }else{ echo 'PAGADO'; } ?></option>
							<option value="10" style="color:<?php if($num10 <= 0){ echo 'red'; }else{ echo 'green'; } ?>">OCTUBRE - <?php if($num10 <= 0){ echo 'PENDIENTE'; }else{ echo 'PAGADO'; } ?></option>
							<option value="11" style="color:<?php if($num11 <= 0){ echo 'red'; }else{ echo 'green'; } ?>">NOVIEMBRE - <?php if($num11 <= 0){ echo 'PENDIENTE'; }else{ echo 'PAGADO'; } ?></option>
							<option value="12" style="color:<?php if($num12 <= 0){ echo 'red'; }else{ echo 'green'; } ?>">DICIEMBRE - <?php if($num12 <= 0){ echo 'PENDIENTE'; }else{ echo 'PAGADO'; } ?></option>
							</select>
							</div>
							</td>
							<td style="height: 31px;" class="alinearizq">&nbsp;
							</td>
						</tr>
						</table>
				</td>
			</tr>
			<tr>
				<td class="auto-style2">
				<div id="vistaprevia">
				</div>
			</td>
			</tr>
			</table>		
			</div>
			</td>
		</tr>
		<tr>
			<td style="width: 100px">&nbsp;</td>
			<td style="width: 100px">&nbsp;</td>
			<td style="width: 100px">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
</form>
</body>
</html>