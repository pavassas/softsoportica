function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function MODULO(v)
{	
	if(v == 'USUARIOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulousuarios=si",true);
		ajax.send(null);
	}
	else
	if(v == 'CAMBIARCONTRASENA')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulocancambiarcontrasena=si",true);
		ajax.send(null);
	}
	else
	if(v == 'REGISTROACTIVIDADES')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?moduloregistroactividades=si",true);
		ajax.send(null);
	}
	else
	if(v == 'PROYECTOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?moduloproyectos=si",true);
		ajax.send(null);
	}
	else
	if(v == 'TIEMPOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulotiempos=si",true);
		ajax.send(null);
	}
	else
	if(v == 'INFORMES')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?moduloinformetiempos=si",true);
		ajax.send(null);
	}
	else
	if(v == 'FACTORPRESTACIONAL')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulofactorprestacional=si",true);
		ajax.send(null);
	}
	else
	if(v == 'COBROS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulocobros=si",true);
		ajax.send(null);
	}
}
function OPCIONACTIVA(v)
{	
	if(v == 'ADMINISTRADOR')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('tabContent_5').innerHTML=ajax.responseText;
		    }
		}
		//jQuery("#tabContent_5").html("<img alt='cargando' src='img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?opcionadministrador=si",true);
		ajax.send(null);
	}
	else
	if(v == 'TICKETS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('tabContent_1').innerHTML=ajax.responseText;
		    }
		}
		//jQuery("#tabContent_5").html("<img alt='cargando' src='img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?opciontickets=si",true);
		ajax.send(null);
	}
	else
	if(v == 'REPORTES')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('tabContent_2').innerHTML=ajax.responseText;
		    }
		}
		//jQuery("#tabContent_5").html("<img alt='cargando' src='img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?opcionreportes=si",true);
		ajax.send(null);
	}
}
function RECUPERAR()
{	
	var rec = $("#recuperarcontrasena").val();
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('recu').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#recu").html("<img alt='cargando' src='images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?recuperar=si&dat="+rec,true);
	ajax.send(null);
}
function ENVIARNOTIFICACION()
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('').innerHTML=ajax.responseText;
	    }
	}
	//jQuery("#recu").html("<img alt='cargando' src='img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?enviarnotificacion=si",true);
	ajax.send(null);
}
function RESTABLECER(v)
{
	var con1 = $("#contrasena1").val();
	var con2 = $("#contrasena2").val();
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('clear').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#clear").html("<img alt='cargando' src='img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?restablecer=si&cod="+v+"&con1="+con1+"&con2="+con2,true);
	ajax.send(null);
}
function NOPERMISO(v)
{
	alert("Usted no tiene permiso a la opci�n "+v);
}