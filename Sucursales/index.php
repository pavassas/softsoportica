﻿<?php
    session_start();
	error_reporting(0);
	include('data/Conexion.php');
	date_default_timezone_set('America/Bogota');
    $_SESSION['contadorLogin'] = 0;
	if($_GET['varContrasena'] == 1 || $_GET['varContrasena'] == 2)
	{
		echo "<script>alert('Usuario o Contraseña incorrecta');</script>";
	}
	elseif($_GET['varContrasena'] == 3)
	{
		echo "<script>alert('Su cuenta esta inactiva');</script>";
	}
    elseif($_GET['varContrasena'] == 4)
    {
        echo "<script>alert('Código de seguridad incorrecto');</script>";
    }
	if($_GET['recuperar'] == "si")
	{
		header("Cache-Control: no-store, no-cache, must-revalidate");
		sleep(1);
		$dat = $_GET['dat'];
		$con = mysqli_query($conectar,"select * from usuario where usu_usuario = '".$dat."' or usu_email = '".$dat."'");
		$dato = mysqli_fetch_array($con);
		$usucla = $dato['usu_clave_int'];
		$usu = $dato['usu_usuario'];
		$ema = $dato['usu_email'];
		$act = $dato['usu_sw_activo'];
		$clave = $dato['usu_clave'];
		
		$con = mysqli_query($conectar,"select * from recuperar where usu_clave_int = '".$usucla."' and rec_estado = 0");
		$num = mysqli_num_rows($con);
		
		if($num > 0)
		{
			$dato = mysqli_fetch_array($con);
			$random = $dato['rec_codigo'];
		}
		else
		{
			$length = 50;
			$random = "";
			$characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"; // change to whatever characters you want
			while ($length > 0) {
				$random .= $characters[mt_rand(0,strlen($characters)-1)];
				$length -= 1;
			}
			$con = mysqli_query($conectar,"insert into recuperar(rec_codigo,usu_clave_int,rec_estado) values('".$random."','".$usucla."','0')");
		}
		
		if($dat != '')
		{
			if(($usu == $dat) || ($ema == $dat))
			{
				// asunto del email
				$subject = "Recuperación Clave SUCURSALES";
				
				// Cuerpo del mensaje
				$mensaje = "------------------------------------------- \n";
				$mensaje.= "Solicitud de recuperación                  \n";
				$mensaje.= "------------------------------------------- \n\n";
				$mensaje.= "Restablecer Contraseña: http://pavas.com.co/Sucursales/restablecer.php?codigo=".$random."\n";
				$mensaje.= "FECHA: ".date("d/m/Y")."\n";
				$mensaje.= "Enviado desde http://www.pavas.com.co \n";
				
				// headers del email
				$headers = "From: adminpavas@pavas.com.co\r\n";
				
				// Enviamos el mensaje
				if (mail($ema, $subject, $mensaje, $headers)) {
					echo "<div class='ok'>Su contrase&ntilde;a a sido recuperada satisfactoriamente<br> Por favor revise su correo $ema</div>";
					echo "<script> form.contrasena.value = ''; </script>";
				} else {
					echo "<div class='validaciones'>Error de envió</div>";
				}
				$fecha=date("Y/m/d H:i:s");
				mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'',66,'','".$dat."','".$fecha."')");//Tercer campo tia_clave_int. 66=Solicitud Recuperar Contraseña
			}
			else
			{
				echo "<div class='validaciones'>Usuario o Correo incorrecto</div>";
			}
		}
		else
		{
			echo "<div class='validaciones'>Usuario o Correo incorrecto</div>";
		}
		exit();
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <link rel="shortcut icon" href="images/logo.png" />
        <title>Sucursales</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="description" content="Expand, contract, animate forms with jQuery wihtout leaving the page" />
        <meta name="keywords" content="expand, form, css3, jquery, animate, width, height, adapt, unobtrusive javascript"/>
        <link rel="stylesheet" type="text/css" href="css/stylelogin.css" />
        
        <?php //VENTANA EMERGENTE ?>
		<link rel="stylesheet" href="css/reveal.css" />
		<script type="text/javascript" src="llamadas4.js?<?php echo time();?>"></script>
		<script type="text/javascript" src="js/jquery-1.6.min.js"></script>
		<script type="text/javascript" src="js/jquery.reveal.js"></script>
    </head>
    <body>
    <form name="form" action="data/validarUsuarios.php" method="post" autocomplete="off" >
		<div class="wrapper">
			<div class="content">
				<div id="form_wrapper" class="form_wrapper">
					<form class="login active">
						<h3>Sucursales</h3>
						<div>
							<label>Usuario:</label>
							<input type="text" name="usuariosucursal" placeholder="Usuario" required id="usuariosucursal" autocomplete="off"  />
							<span class="error">This is an error</span>
						</div>
						<div>
							<label>Contraseña: <a data-reveal-id="recuperar" data-animation="fade" class="forgot linkform">¿Olvidó su contraseña?</a></label>
							<input type="password" name="contrasenasucursal" placeholder="Contraseña" required id="contrasenasucursal" autocomplete="off"  />
							<span class="error">This is an error</span>
						</div>
                      <!--  <div <?php if($_SESSION['contadorLogin']<=0){ echo "style='display:none'"; } ?>>
                            <img style="position: relative;left: 50%; top: 50%;
    margin-left: -120px;cursor: pointer; align-content: center" src='data/captcha.php' hspace='5' vspace='5' alt='Click para refrescar imagen' title='Click para refrescar imagen' id="imgcaptcha" onclick='cambiar_captcha();'/><br />
                            <label><span class='refre'>Click en la imagen para obtener otra</span>
                            </label>
                            <input type="text"  autocomplete="off" name="captcha" placeholder="Ingrese código de seguridad" id="captcha" />
                            <span class="error" id="mensajescapt">This is an error</span>
                        </div>-->
						<div class="bottom">
							<input type="submit" value="Entrar"/>
							<div class="clear"></div>
						</div>

					</form>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<!-- Opción de recuperar contraseña -->
		<div id="recuperar" class="reveal-modal" style="left: 56%; top: 100px; height: 150px; width: 350px;">
			<table style="width: 100%; text-align:center">
				<tr>
					<td>&nbsp;</td>
					<td class="auto-style1" colspan="2"><p class="style42">Restablecer Contrase&ntilde;a</p></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="auto-style1" colspan="2"><span lang="es-co" class="style44"><strong>Correo electr&oacute;nico o nombre 
					de usuario:</strong></span></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="auto-style1" colspan="2">
					<input name="recuperarcontrasena" id="recuperarcontrasena" class="resplandor" type="text" style="width: 280px; height: 20px;" /></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="auto-style1" colspan="2">
					<input class="boton" name="recuperar" onclick="RECUPERAR()" type="button" value="Enviar" /></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td colspan="2"><div id="recu" class="auto-style1"></div></td>
					<td>&nbsp;</td>
				</tr>
			</table>
			<a class="close-reveal-modal">&#215;</a>
		</div>
	 	</form>
    </body>
</html>