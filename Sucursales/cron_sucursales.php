<?php
	error_reporting(E_ALL);
	include('Sucursales/data/Conexion.php');
	require("Sucursales/Classes/PHPMailer-master/class.phpmailer.php");
	
	function ENVIARALERTA($email,$nomusu,$clasuc,$dtinm,$nominforme,$nomsuc)
	{
		$mail = new PHPMailer();
		$mail->Body = '';
		
		$mail->From = "adminpavas@pavas.com.co";
		$mail->FromName = "Soportica";
		$mail->AddAddress($email, "Destino");
		$mail->Subject = "Alerta entrega archivo - Sucursal Nro. ".$clasuc;

		// Cuerpo del mensaje
		$mail->Body .= "Hola ".$nom."!\n\n";
		$mail->Body .= "SUCURSALES Le informa que faltan ".$dtinm." dias para subir el siguiente informe: ".$nominforme.".\n";
		$mail->Body .= "SUCURSAL: ".$nomsuc."\n";
		$mail->Body .= date("d/m/Y H:m:s")."\n\n";
		$mail->Body .= "Este mensaje es generado automáticamente por SUCURSALES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Sucursales \n";

		if(!$mail->Send())
		{
			//echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
			//echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
		}
		else
		{
			echo "Enviado con exito";
		}
	}
	
	function CALCULARDIAS($fi,$ff)
	{$conectar = mysqli_connect("localhost", "usrpavas", "9A12)WHFy$2p4v4s", "sucursales");
		$condias = mysqli_query($conectar,"select DATEDIFF('".$ff."','".$fi."') dias from usuario LIMIT 1");
		$datodias = mysqli_fetch_array($condias);
		return $datodias['dias'];
	}
	
	$con = mysqli_query($conectar,"select lb.lib_fec_inmobiliaria,lb.lib_fecteo_inmobiliaria,lb.lib_fec_diseno,lb.lib_fecteo_diseno,lb.lib_fec_licencia,lb.lib_fecteo_licencia,lb.lib_fec_interventoria,lb.lib_fecteo_interventoria,lb.lib_fec_constructor,lb.lib_fecteo_constructor,lb.lib_fec_presupuesto,lb.lib_fecteo_presupuesto,s.suc_usuario_creacion,s.suc_clave_int,s.suc_estado,s.suc_codigo,s.suc_nombre,s.suc_sw_busqueda_local,sucinm.sui_sw_requiere_licencia,sucdis.sud_req_diseno from sucursal s inner join linea_base lb on (lb.suc_clave_int = s.suc_clave_int) left outer join sucursal_inmobiliaria sucinm on (sucinm.suc_clave_int = s.suc_clave_int) left outer join sucursal_diseno sucdis on (sucdis.suc_clave_int = s.suc_clave_int) left outer join sucursal_licencia suclic on (suclic.suc_clave_int = s.suc_clave_int) left outer join sucursal_adjudicacion sucadju on (sucadju.suc_clave_int = s.suc_clave_int) left outer join sucursal_constructor succons on (succons.suc_clave_int = s.suc_clave_int) left outer join sucursal_presupuesto sucpre on (sucpre.suc_clave_int = s.suc_clave_int) left outer join sucursal_suministro sucsum on (sucsum.suc_clave_int = s.suc_clave_int) left outer join sucursal_civil succiv on (succiv.suc_clave_int = s.suc_clave_int) left outer join sucursal_electromecanico sucele on (sucele.suc_clave_int = s.suc_clave_int) left outer join sucursal_seguridad sucseg on (sucseg.suc_clave_int = s.suc_clave_int) where suc_estado IN (1,5) and suc_sw_eliminado = 0");
		
	$num = mysqli_num_rows($con);
	
	for($i = 0; $i < $num; $i++)
	{
		$dato = mysqli_fetch_array($con);
		$clasuc = $dato['suc_clave_int'];
		$est = $dato['suc_estado'];
		$cod = $dato['suc_codigo'];
		$nomsuc = $dato['suc_nombre'];
		$swreqloc = $dato['suc_sw_busqueda_local'];
		$swreqlic = $dato['sui_sw_requiere_licencia'];
		$swreqdis = $dato['sud_req_diseno'];
		$usucreacion = $dato['suc_usuario_creacion'];
		
		$fitinm = $dato['lib_fec_inmobiliaria']; 
		$fftinm = $dato['lib_fecteo_inmobiliaria'];
		$fitdis = $dato['lib_fec_diseno'];
		$fftdis = $dato['lib_fecteo_diseno'];
		$fitlic = $dato['lib_fec_licencia'];
		$fftlic = $dato['lib_fecteo_licencia'];
		$fitint = $dato['lib_fec_interventoria'];
		$fftint = $dato['lib_fecteo_interventoria'];
		$fitcons = $dato['lib_fec_constructor'];
		$fftcons = $dato['lib_fecteo_constructor'];
		$fitpre = $dato['lib_fec_presupuesto'];
		$fftpre = $dato['lib_fecteo_presupuesto'];
		$fitsum = $dato['lib_fec_suministro'];
		$fftsum = $dato['lib_fecteo_suministro'];
		$fitciv = $dato['lib_fec_civil'];
		$fftciv = $dato['lib_fecteo_civil'];
		$fitele = $dato['lib_fec_electromecanico'];
		$fftele = $dato['lib_fecteo_electromecanico'];
		$fitseg = $dato['lib_fec_seguridad'];
		$fftseg = $dato['lib_fecteo_seguridad'];
		
		$fecha=date("Y/m/d");
		
		$dtinm = CALCULARDIAS($fecha,$fftinm);
		$dtlic = CALCULARDIAS($fecha,$fftlic);
		$dtdis = CALCULARDIAS($fecha,$fftdis);
		$dtcons = CALCULARDIAS($fecha,$fftcons);
		$dtpre = CALCULARDIAS($fecha,$fftpre);
		$dtsum = CALCULARDIAS($fecha,$fftsum);
		$dtciv = CALCULARDIAS($fecha,$fftciv);
		$dtele = CALCULARDIAS($fecha,$fftele);
		$dtseg = CALCULARDIAS($fecha,$fftseg);
		
		if($dtinm == ""){ $dtinm = 0; }
		if($dtlic == ""){ $dtlic = 0; }
		if($dtdis == ""){ $dtdis = 0; }
		if($dtcons == ""){ $dtcons = 0; }
		if($dtpre == ""){ $dtpre = 0; }
		if($dtsum == ""){ $dtsum = 0; }
		if($dtciv == ""){ $dtciv = 0; }
		if($dtele == ""){ $dtele = 0; }
		if($dtseg == ""){ $dtseg = 0; }
		
		//NOTIFICACIONES
		$connot = mysqli_query($conectar,"select * from notificar");
		$numnot = mysqli_num_rows($connot);
		
		for($j = 0; $j < $numnot; $j++)
		{
			$datonot = mysqli_fetch_array($connot);
			$clanot = $datonot['not_clave_int'];
			$not = $datonot['not_nombre'];
			$diasnot = $datonot['not_entrega_adjunto'];
			
			$conadj = mysqli_query($conectar,"select * from adjunto_actor aa inner join tipo_adjunto ta on (ta.tad_clave_int = aa.tad_clave_int) inner join notificar n on (n.tad_clave_int = ta.tad_clave_int) where aa.suc_clave_int = '".$clasuc."' and n.not_clave_int = '".$clanot."' and ada_sw_eliminado = 0");
			$numadj = mysqli_num_rows($conadj);
			
			//echo "SUC: ".$clasuc." DTI: ".$dtinm." DNOT: ".$diasnot." REQLOC: ".$swreqloc."<br>";
			if($swreqloc == 1)
			{
				//ALERTAS DE INMOBILIARIA
				//ALERTA: Adjunto Informe - Inmobiliaria
				if($clanot == 39 and $dtinm <= $diasnot and $numadj <= 0 and $fftinm != "" and $fftinm != "0000-00-00")
				{
					echo "SUC: ".$clasuc." DTI: ".$dtinm." DNOT: ".$diasnot." REQLOC: ".$swreqloc." CLANOT: ".$clanot."<br>";
					$conusu = mysqli_query($conectar,"select u.usu_email,u.usu_nombre from notificar_usuario nu inner join usuario u on (u.usu_clave_int = nu.usu_clave_int) where nu.not_clave_int = '".$clanot."' UNION select usu_email,usu_nombre from usuario where usu_usuario = '".$usucreacion."'");
					$numusu = mysqli_num_rows($conusu);
					for($k = 0; $k < $numusu; $k++)
					{
						$datousu = mysqli_fetch_array($conusu);
						$email = $datousu['usu_email'];
						$nomusu = $datousu['usu_nombre'];
						$nominforme = "Adjunto Informe";
						
						echo "ENVIARALERTA(".$email.",".$nomusu.",".$clasuc.",".$dtinm.",".$nominforme.",".$nomsuc.")<br>";
						ENVIARALERTA($email,$nomusu,$clasuc,$dtinm,$nominforme,$nomsuc);
					}
				}
			}
			if($swreqlic == 1)
			{
				//ALERTAS DE LICENCIA
				if($clanot == 48 and $dtlic <= $diasnot and $numadj <= 0  and $fftlic != "" and $fftlic != "0000-00-00")//ALERTA: Adjunto Informe - Licencia
				{
					$conusu = mysqli_query($conectar,"select u.usu_email,u.usu_nombre from notificar_usuario nu inner join usuario u on (u.usu_clave_int = nu.usu_clave_int) where nu.not_clave_int = '".$clanot."' UNION select usu_email,usu_nombre from usuario where usu_usuario = '".$usucreacion."'");
					$numusu = mysqli_num_rows($conusu);
					for($k = 0; $k < $numusu; $k++)
					{
						$datousu = mysqli_fetch_array($conusu);
						$email = $datousu['usu_email'];
						$nomusu = $datousu['usu_nombre'];
						$nominforme = "Adjunto Informe";
						echo "ENVIARALERTA(".$email.",".$nomusu.",".$clasuc.",".$dtlic.",".$nominforme.",".$nomsuc.")<br>";
						ENVIARALERTA($email,$nomusu,$clasuc,$dtinm,$nominforme,$nomsuc);
					}
				}
			}
			if($swreqdis == 1)
			{
				//ALERTAS DE DISEÑO
				//ALERTA: Adjunto Esquema - Diseño
				//ALERTA: Adjunto PowerPoint - Diseño
				//ALERTA: Adjunto Retroalimentacion Diseñador - Diseño
				//ALERTA: Adjunto Calificacion al Diseñador - Diseño
				if(($clanot == 40 or $clanot == 43 or $clanot == 44 or $clanot == 45) and $dtdis <= $diasnot and $numadj <= 0 and $fftdis != "" and $fftdis != "0000-00-00")
				{
					echo "SUC: ".$clasuc." DTI: ".$dtinm." DNOT: ".$diasnot." REQLOC: ".$swreqloc." CLANOT: ".$clanot."<br>";
					$conusu = mysqli_query($conectar,"select u.usu_email,u.usu_nombre from notificar_usuario nu inner join usuario u on (u.usu_clave_int = nu.usu_clave_int) where nu.not_clave_int = '".$clanot."' UNION select usu_email,usu_nombre from usuario where usu_usuario = '".$usucreacion."'");
					$numusu = mysqli_num_rows($conusu);
					for($k = 0; $k < $numusu; $k++)
					{
						$datousu = mysqli_fetch_array($conusu);
						$email = $datousu['usu_email'];
						$nomusu = $datousu['usu_nombre'];
						
						if($clanot == 40){ 
							$nominforme = "Adjunto Esquema"; 
						}elseif($clanot == 43){ 
							$nominforme = "Adjunto PowerPoint"; 
						}elseif($clanot == 44){ 
							$nominforme = "Adjunto Retroalimentacion Disenador"; 
						}elseif($clanot == 45){ 
							$nominforme = "Adjunto Calificacion al Disenador"; 
						}
						
						echo "ENVIARALERTA(".$email.",".$nomusu.",".$clasuc.",".$dtdis.",".$nominforme.",".$nomsuc.")<br>";
						ENVIARALERTA($email,$nomusu,$clasuc,$dtinm,$nominforme,$nomsuc);
					}
				}
			}
			
			//ALERTAS DE CONSTRUCTOR
			//ALERTA: Adjunto Actualización Presupuestal - Constructor
			//ALERTA: Adjunto Control de cambios - Constructor
			//ALERTA: Adjunto Planillas de seguridad social  - Constructor
			//ALERTA: Adjunto Polizas de Seguros - Constructor
			//ALERTA: Adjunto Plan de Manejo Ambiental - Constructor
			//ALERTA: Adjunto Plan de Calidad - Constructor
			//ALERTA: Adjunto Liquidación  - Constructor
			//ALERTA: Adjunto Ficha Técnica - Constructor
			//ALERTA: Adjunto Registro Fotografico - Constructor
			//ALERTA: Adjunto Certificado de Escombros - Constructor
			//ALERTA: Adjunto Certificado Retie - Constructor
			//ALERTA: Adjunto Prueba Arranque Equipos - Constructor
			//ALERTA: Adjunto Evaluación Final - Constructor
			//ALERTA: Adjunto Manuales de operacion de equipos tecnicos - Constructor
			//ALERTA: Adjunto Lista de chequeo aire acondicionado - Constructor
			if(($clanot == 2 or $clanot == 3 or $clanot == 4 or $clanot == 15 or $clanot == 16 or $clanot == 17 or $clanot == 18 or $clanot == 19 or $clanot == 20 or $clanot == 21 or $clanot == 22 or $clanot == 23 or $clanot == 24 or $clanot == 25 or $clanot == 26) and $dtcons <= $diasnot and $numadj <= 0 and $fftcons != "" and $fftcons != "0000-00-00")
			{
				$conusu = mysqli_query($conectar,"select u.usu_email,u.usu_nombre from notificar_usuario nu inner join usuario u on (u.usu_clave_int = nu.usu_clave_int) where nu.not_clave_int = '".$clanot."' UNION select usu_email,usu_nombre from usuario where usu_usuario = '".$usucreacion."'");
				$numusu = mysqli_num_rows($conusu);
				for($k = 0; $k < $numusu; $k++)
				{
					$datousu = mysqli_fetch_array($conusu);
					$email = $datousu['usu_email'];
					$nomusu = $datousu['usu_nombre'];
					
					if($clanot == 2){ 
						$nominforme = "Adjunto Actualizacion Presupuestal"; 
					}
					elseif($clanot == 3){ 
						$nominforme = "Adjunto Control de cambios"; 
					}
					elseif($clanot == 4){ 
						$nominforme = "Adjunto Planillas de seguridad social"; 
					}
					elseif($clanot == 15){ 
						$nominforme = "Adjunto Polizas de Seguros"; 
					}
					elseif($clanot == 16){ 
						$nominforme = "Adjunto Plan de Manejo Ambiental"; 
					}
					elseif($clanot == 17){ 
						$nominforme = "Adjunto Plan de Calidad"; 
					}
					elseif($clanot == 18){ 
						$nominforme = "Adjunto Liquidación"; 
					}
					elseif($clanot == 19){ 
						$nominforme = "Adjunto Ficha Técnica"; 
					}
					elseif($clanot == 20){ 
						$nominforme = "Adjunto Registro Fotografico"; 
					}
					elseif($clanot == 21){ 
						$nominforme = "Adjunto Certificado de Escombros"; 
					}
					elseif($clanot == 22){ 
						$nominforme = "Adjunto Certificado Retie"; 
					}
					elseif($clanot == 23){ 
						$nominforme = "Adjunto Prueba Arranque Equipos"; 
					}
					elseif($clanot == 24){ 
						$nominforme = "Adjunto Evaluación Final"; 
					}
					elseif($clanot == 25){ 
						$nominforme = "Adjunto Manuales de operacion de equipos tecnicos"; 
					}
					elseif($clanot == 26){ 
						$nominforme = "Adjunto Lista de chequeo aire acondicionado"; 
					}
					echo "ENVIARALERTA(".$email.",".$nomusu.",".$clasuc.",".$dtcons.",".$nominforme.",".$nomsuc.")<br>";
					ENVIARALERTA($email,$nomusu,$clasuc,$dtinm,$nominforme,$nomsuc);
				}
			}
			
			//ALERTAS DE SEGURIDAD
			//ALERTA: Adjunto Informe seguridad - Seguridad
			//ALERTA: Adjunto Planos Record - Seguridad
			//ALERTA: Adjunto Calificación Proveedores - Seguridad
			//ALERTA: Adjunto Liquidación Proveedores - Seguridad
			if(($clanot == 31 or $clanot == 60 or $clanot == 61 or $clanot == 62) and $dtseg <= $diasnot and $numadj <= 0 and $fftseg != "" and $fftseg != "0000-00-00")
			{
				$conusu = mysqli_query($conectar,"select u.usu_email,u.usu_nombre from notificar_usuario nu inner join usuario u on (u.usu_clave_int = nu.usu_clave_int) where nu.not_clave_int = '".$clanot."' UNION select usu_email,usu_nombre from usuario where usu_usuario = '".$usucreacion."'");
				$numusu = mysqli_num_rows($conusu);
				for($k = 0; $k < $numusu; $k++)
				{
					$datousu = mysqli_fetch_array($conusu);
					$email = $datousu['usu_email'];
					$nomusu = $datousu['usu_nombre'];
					
					if($clanot == 31){ 
						$nominforme = "Adjunto Informe seguridad"; 
					}elseif($clanot == 60){ 
						$nominforme = "Adjunto Planos Record"; 
					}elseif($clanot == 61){ 
						$nominforme = "Adjunto Calificacion Proveedores"; 
					}elseif($clanot == 62){ 
						$nominforme = "Adjunto Liquidacion Proveedores"; 
					}
					
					echo "ENVIARALERTA(".$email.",".$nomusu.",".$clasuc.",".$dtseg.",".$nominforme.",".$nomsuc.")<br>";
					ENVIARALERTA($email,$nomusu,$clasuc,$dtinm,$nominforme,$nomsuc);
				}
			}
			
			//ALERTAS DE ELECTROMECANICO
			//ALERTA: Adjunto Informe Check List - Electromecanico
			//ALERTA: Adjunto Documentos Retie - Electromecanico
			//ALERTA: Adjunto Certificado Retie - Electromecanico
			//ALERTA: Adjunto Liquidacion Aire Acondicionado - Electromecanico
			//ALERTA: Adjunto Liquidacion Obra Electrica - Electromecanico
			if(($clanot == 54 or $clanot == 55 or $clanot == 56 or $clanot == 57 or $clanot == 58) and $dtele <= $diasnot and $numadj <= 0 and $fftele != "" and $fftele != "0000-00-00")
			{
				$conusu = mysqli_query($conectar,"select u.usu_email,u.usu_nombre from notificar_usuario nu inner join usuario u on (u.usu_clave_int = nu.usu_clave_int) where nu.not_clave_int = '".$clanot."' UNION select usu_email,usu_nombre from usuario where usu_usuario = '".$usucreacion."'");
				$numusu = mysqli_num_rows($conusu);
				for($k = 0; $k < $numusu; $k++)
				{
					$datousu = mysqli_fetch_array($conusu);
					$email = $datousu['usu_email'];
					$nomusu = $datousu['usu_nombre'];
					
					if($clanot == 54){ 
						$nominforme = "Adjunto Informe Check List"; 
					}elseif($clanot == 55){ 
						$nominforme = "Adjunto Documentos Retie"; 
					}elseif($clanot == 56){ 
						$nominforme = "Adjunto Certificado Retie"; 
					}elseif($clanot == 57){ 
						$nominforme = "Adjunto Liquidacion Aire Acondicionado"; 
					}elseif($clanot == 58){ 
						$nominforme = "Adjunto Liquidacion Obra Electrica"; 
					}
					
					echo "ENVIARALERTA(".$email.",".$nomusu.",".$clasuc.",".$dtele.",".$nominforme.",".$nomsuc.")<br>";
					ENVIARALERTA($email,$nomusu,$clasuc,$dtinm,$nominforme,$nomsuc);
				}
			}
			
			//ALERTAS DE CIVIL
			//ALERTA: Adjunto Entrega Sodexo - Civil
			//ALERTA: Adjunto Acta Inicio Real  - Civil
			//ALERTA: Adjunto Acta Entrega Real - Civil
			//ALERTA: Adjunto Informe - Civil
			if(($clanot == 12 or $clanot == 52 or $clanot == 53 or $clanot == 64) and $dtciv <= $diasnot and $numadj <= 0 and $fftciv != "" and $fftciv != "0000-00-00")
			{
				$conusu = mysqli_query($conectar,"select u.usu_email,u.usu_nombre from notificar_usuario nu inner join usuario u on (u.usu_clave_int = nu.usu_clave_int) where nu.not_clave_int = '".$clanot."' UNION select usu_email,usu_nombre from usuario where usu_usuario = '".$usucreacion."'");
				$numusu = mysqli_num_rows($conusu);
				for($k = 0; $k < $numusu; $k++)
				{
					$datousu = mysqli_fetch_array($conusu);
					$email = $datousu['usu_email'];
					$nomusu = $datousu['usu_nombre'];
					
					if($clanot == 12){ 
						$nominforme = "Adjunto Entrega Sodexo"; 
					}elseif($clanot == 52){ 
						$nominforme = "Adjunto Acta Inicio Real"; 
					}elseif($clanot == 53){ 
						$nominforme = "Adjunto Acta Entrega Real"; 
					}elseif($clanot == 64){ 
						$nominforme = "Adjunto Informe"; 
					}
					
					echo "ENVIARALERTA(".$email.",".$nomusu.",".$clasuc.",".$dtciv.",".$nominforme.",".$nomsuc.")<br>";
					ENVIARALERTA($email,$nomusu,$clasuc,$dtinm,$nominforme,$nomsuc);
				}
			}
			
			//ALERTAS DE SUMINISTROS
			//ALERTA: Adjunto Pedidos - Suministros
			//ALERTA: Adjunto Plaqueo activos - Suministros
			if(($clanot == 35 or $clanot == 36) and $dtsum <= $diasnot and $numadj <= 0 and $fftsum != "" and $fftsum != "0000-00-00")
			{
				$conusu = mysqli_query($conectar,"select u.usu_email,u.usu_nombre from notificar_usuario nu inner join usuario u on (u.usu_clave_int = nu.usu_clave_int) where nu.not_clave_int = '".$clanot."' UNION select usu_email,usu_nombre from usuario where usu_usuario = '".$usucreacion."'");
				$numusu = mysqli_num_rows($conusu);
				for($k = 0; $k < $numusu; $k++)
				{
					$datousu = mysqli_fetch_array($conusu);
					$email = $datousu['usu_email'];
					$nomusu = $datousu['usu_nombre'];
					
					if($clanot == 35){ 
						$nominforme = "Adjunto Pedidos"; 
					}elseif($clanot == 36){ 
						$nominforme = "Adjunto Plaqueo activos"; 
					}
					
					echo "ENVIARALERTA(".$email.",".$nomusu.",".$clasuc.",".$dtsum.",".$nominforme.",".$nomsuc.")<br>";
					ENVIARALERTA($email,$nomusu,$clasuc,$dtinm,$nominforme,$nomsuc);
				}
			}
			
			//ALERTAS DE PRESUPUESTOS
			//ALERTA: Adjunto Comparativo Propuestas - Presupuesto
			//ALERTA: Adjunto Casos de Negocio - Presupuesto
			//ALERTA: Adjunto Informe final - Presupuesto
			if(($clanot == 6 or $clanot == 50 or $clanot == 51) and $dtpre <= $diasnot and $numadj <= 0 and $fftpre != "" and $fftpre != "0000-00-00")
			{
				$conusu = mysqli_query($conectar,"select u.usu_email,u.usu_nombre from notificar_usuario nu inner join usuario u on (u.usu_clave_int = nu.usu_clave_int) where nu.not_clave_int = '".$clanot."' UNION select usu_email,usu_nombre from usuario where usu_usuario = '".$usucreacion."'");
				$numusu = mysqli_num_rows($conusu);
				for($k = 0; $k < $numusu; $k++)
				{
					$datousu = mysqli_fetch_array($conusu);
					$email = $datousu['usu_email'];
					$nomusu = $datousu['usu_nombre'];
					
					if($clanot == 6){ 
						$nominforme = "Adjunto Comparativo Propuestas"; 
					}elseif($clanot == 50){ 
						$nominforme = "Adjunto Casos de Negocio"; 
					}elseif($clanot == 51){ 
						$nominforme = "Adjunto Informe final"; 
					}
					
					echo "ENVIARALERTA(".$email.",".$nomusu.",".$clasuc.",".$dtpre.",".$nominforme.",".$nomsuc.")<br>";
					ENVIARALERTA($email,$nomusu,$clasuc,$dtinm,$nominforme,$nomsuc);
				}
			}
		}
	}
?>