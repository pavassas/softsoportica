<?php
	error_reporting(0);
	include('../../../data/Conexion.php');	
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_SESSION['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];
		
	// verifica si no se ha loggeado
	if(!isset($_SESSION["persona"]))
	{
	  session_destroy();
	  header("LOCATION:index.php");
	}else{
	}
	$clasuc = $_GET['clasuc'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<link href="../../../css/multiple-select.css" rel="stylesheet" />
<script src="../../../js/jquery.multiple.select.js"></script>
</head>
<body>
<br><br><br><br><br><br><br><br><br><br><br>
<select multiple="multiple" name="hijos" id="hijos" onchange="parent.HIJOSSELECCIONADOS1();parent.OCULTARSCROLL()" style="width:400px">
<?php
	$con = mysqli_query($conectar,"select * from cajeros.cajero where caj_sw_eliminado = 0 and (suc_clave_int <=0 or suc_clave_int ='".$clasuc."' or suc_clave_int = '' or suc_clave_int IS NULL) order by caj_nombre");
	$num = mysqli_num_rows($con);
	for($i = 0; $i < $num; $i++)
	{
		$dato = mysqli_fetch_array($con);
		$clave = $dato['caj_clave_int'];
		$nombre = $dato['caj_nombre'];
		$codigocaj = $dato['caj_codigo_cajero'];
		$succla = $dato['suc_clave_int'];
?>
	<option value="<?php echo $clave; ?>" <?php if($succla == $clasuc){ echo 'selected="selected"'; } ?>><?php echo $clave." - ".$nombre; ?></option>
<?php
	}
?>
</select>
<script>
    jQuery("#hijos").multipleSelect({
        filter: true
    });
</script>
</body>
</html>