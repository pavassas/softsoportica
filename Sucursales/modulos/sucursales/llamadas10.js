function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function MODULO(v,e)
{	
	if(v == 'SUCURSALES')
	{
		window.location.href = "sucursales.php";
	}
	else
	if(v == 'TODOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('sucursales').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#sucursales").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?todos=si&est="+e,true);
		ajax.send(null);
		setTimeout("REFRESCARLISTAS()",1000);
	}
	OCULTARSCROLL();
}
function GUARDAR()
{		
	//INFO BASICA
	//***********
	var cod = $('#codigo').val();
	var nom = $('#nombre').val();
	var dir = $('#direccion').val();
	var reg = $('#region').val();
	var dep = $('#departamento').val();
	var mun = $('#municipio').val();
	var ima = $('#imagen').val();
	var are = $('#area').val();
	var tii = $('#tipointervencion').val();
	var form = $('#formato').val();
	var fecini = $('#fechainicio').val();
	var mod = $('#modalidad').val();
	var anocon = $('#anocontable').val();
	var est = $('#estadosucursal').val();
	
	//INFO ADICIONAL
	//**************
	var pger = $('#gerente').val();
	var pdir = $('#director').val();
	var peje = $('#ejecutivo').val();
	var pase = $('#asesor').val();
	var pcaj = $('#cajas').val();
	var potro = $('#otros').val();
	
	//INMOBILIARIA
	//************
	var fitinm = $('#fechainicioteoricainmobiliaria').val();
	var fftinm = $('#fechafinteoricainmobiliaria').val();
	var firinm = $('#fechainiciorealinmobiliaria').val();
	var ffrinm = $('#fechafinrealinmobiliaria').val();
	var reqinm = $('#requiereinmobiliaria').val();
	var nominm = $('#nombreinmobiliaria').val();
	
	//DISE�O
	//******
	var fitdis = $('#fechainicioteoricadiseno').val();
	var fftdis = $('#fechafinteoricadiseno').val();
	var firdis = $('#fechainiciorealdiseno').val();
	var ffrdis = $('#fechafinrealdiseno').val();
	var reqdis = $('#requierediseno').val();
	var nomdis = $('#nombrediseno').val();
	
	//LICENCIA
	//********
	var fitlic = $('#fechainicioteoricalicencia').val();
	var fftlic = $('#fechafinteoricalicencia').val();
	var firlic = $('#fechainicioreallicencia').val();
	var ffrlic = $('#fechafinreallicencia').val();
	var reqlic = $('#requierelicencia').val();
	var nomlic = $('#nombrelicencia').val();
	
	//INTERVENCI�N
	//************
	var fitint = $('#fechainicioteoricainterventor').val();
	var fftint = $('#fechafinteoricainterventor').val();
	var firint = $('#fechainiciorealinterventor').val();
	var ffrint = $('#fechafinrealinterventor').val();
	var nomcons = $('#nombreconstructor').val();
	var avaobr = $('#avanceobra').val();
	var nomciv = $('#nombreintcivil').val();
	var nomele = $('#nombreintelectromecanico').val();
	var nomseg = $('#nombreintseguridad').val();
	
	//CONSTRUCTOR
	var fitcons = $('#fechainicioteoricaconstructor').val();
	var fftcons = $('#fechafinteoricaconstructor').val();
	
	//PRESUPUESTO
	var fitpre = $('#fechainicioteoricapresupuesto').val();
	var fftpre = $('#fechafinteoricapresupuesto').val();
	
	//SUMINISTRO
	var fitsum = $('#fechainicioteoricasuministro').val();
	var fftsum = $('#fechafinteoricasuministro').val();
	
	//INT.CIVIL
	var fitciv = $('#fechainicioteoricacivil').val();
	var fftciv = $('#fechafinteoricacivil').val();
	
	//INT.ELECTROMECANICO
	var fitele = $('#fechainicioteoricaelectromecanico').val();
	var fftele = $('#fechafinteoricaelectromecanico').val();
	
	//SEGURIDAD
	var fitseg = $('#fechainicioteoricaseguridad').val();
	var fftseg = $('#fechafinteoricaseguridad').val();
	
	var nuevonombre = CORREGIRTEXTO(nom);
	var nuevadireccion = CORREGIRTEXTO(dir);

	if(nom == '')
	{
		alert("Debe ingresar el nombre");
	}
	else
	if(dir == '')
	{
		alert("Debe ingresar la direccion");
	}
	else
	if(reg == '')
	{
		alert("Debe alegir la region");
	}
	else
	if(dep == '')
	{
		alert("Debe elegir el departamento");
	}
	else
	if(mun == '')
	{
		alert("Debe elegir el municipio");
	}
	else
	if(ima == '')
	{
		alert("Debe elegir la imagen");
	}
	else
	if(tii == '')
	{
		alert("Debe elegir el tipo de intervencion");
	}
	else
	if(form == '')
	{
		alert("Debe elegir el formato");
	}
	else
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('resultadoguardar').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#resultadoguardar").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?guardardatos=si&cod="+cod+"&nom="+nuevonombre+"&dir="+nuevadireccion+"&reg="+reg+"&dep="+dep+"&mun="+mun+"&ima="+ima+"&are="+are+"&tii="+tii+"&form="+form+"&fecini="+fecini+"&mod="+mod+"&anocon="+anocon+"&est="+est+"&fitinm="+fitinm+"&fftinm="+fftinm+"&firinm="+firinm+"&ffrinm="+ffrinm+"&reqinm="+reqinm+"&nominm="+nominm+"&fitdis="+fitdis+"&fftdis="+fftdis+"&firdis="+firdis+"&ffrdis="+ffrdis+"&reqdis="+reqdis+"&nomdis="+nomdis+"&fitlic="+fitlic+"&fftlic="+fftlic+"&firlic="+firlic+"&ffrlic="+ffrlic+"&reqlic="+reqlic+"&nomlic="+nomlic+"&fitint="+fitint+"&fftint="+fftint+"&firint="+firint+"&ffrint="+ffrint+"&nomcons="+nomcons+"&avaobr="+avaobr+"&nomciv="+nomciv+"&nomele="+nomele+"&nomseg="+nomseg+"&fitcons="+fitcons+"&fftcons="+fftcons+"&fitpre="+fitpre+"&fftpre="+fftpre+"&fitsum="+fitsum+"&fftsum="+fftsum+"&fitciv="+fitciv+"&fftciv="+fftciv+"&fitele="+fitele+"&fftele="+fftele+"&fitseg="+fitseg+"&fftseg="+fftseg+"&pger="+pger+"&pdir="+pdir+"&peje="+peje+"&pase="+pase+"&pcaj="+pcaj+"&potro="+potro,true);
		ajax.send(null);
		REFRESCARACTIVOS();
	}
}
function ACTUALIZARINFORMACION(v)
{		
	//INFO BASICA
	//***********
	var cod = $('#codigo').val();
	var nom = $('#nombre').val();
	var dir = $('#direccion').val();
	var reg = $('#region').val();
	var dep = $('#departamento').val();
	var mun = $('#municipio').val();
	var ima = $('#imagen').val();
	var are = $('#area').val();
	var tii = $('#tipointervencion').val();
	var form = $('#formato').val();
	var fecini = $('#fechainicio').val();
	var mod = $('#modalidad').val();
	var anocon = $('#anocontable').val();
	var est = $('#estadosucursal').val();
	
	//INFO ADICIONAL
	//**************
	var pger = $('#gerente').val();
	var pdir = $('#director').val();
	var peje = $('#ejecutivo').val();
	var pase = $('#asesor').val();
	var pcaj = $('#cajas').val();
	var potro = $('#otros').val();
	
	//INMOBILIARIA
	//************
	var fitinm = $('#fechainicioteoricainmobiliaria').val();
	var fftinm = $('#fechafinteoricainmobiliaria').val();
	var firinm = $('#fechainiciorealinmobiliaria').val();
	var ffrinm = $('#fechafinrealinmobiliaria').val();
	var reqinm = $('#requiereinmobiliaria').val();
	var nominm = $('#nombreinmobiliaria').val();
	
	//DISE�O
	//******
	var fitdis = $('#fechainicioteoricadiseno').val();
	var fftdis = $('#fechafinteoricadiseno').val();
	var firdis = $('#fechainiciorealdiseno').val();
	var ffrdis = $('#fechafinrealdiseno').val();
	var reqdis = $('#requierediseno').val();
	var nomdis = $('#nombrediseno').val();
	
	//LICENCIA
	//********
	var fitlic = $('#fechainicioteoricalicencia').val();
	var fftlic = $('#fechafinteoricalicencia').val();
	var firlic = $('#fechainicioreallicencia').val();
	var ffrlic = $('#fechafinreallicencia').val();
	var reqlic = $('#requierelicencia').val();
	var nomlic = $('#nombrelicencia').val();
	
	//INTERVENCI�N
	//************
	var fitint = $('#fechainicioteoricainterventor').val();
	var fftint = $('#fechafinteoricainterventor').val();
	var firint = $('#fechainiciorealinterventor').val();
	var ffrint = $('#fechafinrealinterventor').val();
	var nomcons = $('#nombreconstructor').val();
	var avaobr = $('#avanceobra').val();
	var nomciv = $('#nombreintcivil').val();
	var nomele = $('#nombreintelectromecanico').val();
	var nomseg = $('#nombreintseguridad').val();
	
	//CONSTRUCTOR
	var fitcons = $('#fechainicioteoricaconstructor').val();
	var fftcons = $('#fechafinteoricaconstructor').val();
	
	//PRESUPUESTO
	var fitpre = $('#fechainicioteoricapresupuesto').val();
	var fftpre = $('#fechafinteoricapresupuesto').val();
	
	//SUMINISTRO
	var fitsum = $('#fechainicioteoricasuministro').val();
	var fftsum = $('#fechafinteoricasuministro').val();
	
	//INT.CIVIL
	var fitciv = $('#fechainicioteoricacivil').val();
	var fftciv = $('#fechafinteoricacivil').val();
	
	//INT.ELECTROMECANICO
	var fitele = $('#fechainicioteoricaelectromecanico').val();
	var fftele = $('#fechafinteoricaelectromecanico').val();
	
	//SEGURIDAD
	var fitseg = $('#fechainicioteoricaseguridad').val();
	var fftseg = $('#fechafinteoricaseguridad').val();
	
	var nuevonombre = CORREGIRTEXTO(nom);
	var nuevadireccion = CORREGIRTEXTO(dir);

	if(nom == '')
	{
		alert("Debe ingresar el nombre");
	}
	else
	if(dir == '')
	{
		alert("Debe ingresar la direccion");
	}
	else
	if(reg == '')
	{
		alert("Debe alegir la region");
	}
	else
	if(dep == '')
	{
		alert("Debe elegir el departamento");
	}
	else
	if(mun == '')
	{
		alert("Debe elegir el municipio");
	}
	else
	if(ima == '')
	{
		alert("Debe elegir la imagen");
	}
	else
	if(tii == '')
	{
		alert("Debe elegir el tipo de intervencion");
	}
	else
	if(form == '')
	{
		alert("Debe elegir el formato");
	}
	else
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('estadoregistro').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#estadoregistro").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?actualizardatos=si&cod="+cod+"&nom="+nuevonombre+"&dir="+nuevadireccion+"&reg="+reg+"&dep="+dep+"&mun="+mun+"&ima="+ima+"&are="+are+"&tii="+tii+"&form="+form+"&fecini="+fecini+"&mod="+mod+"&anocon="+anocon+"&est="+est+"&fitinm="+fitinm+"&fftinm="+fftinm+"&firinm="+firinm+"&ffrinm="+ffrinm+"&reqinm="+reqinm+"&nominm="+nominm+"&fitdis="+fitdis+"&fftdis="+fftdis+"&firdis="+firdis+"&ffrdis="+ffrdis+"&reqdis="+reqdis+"&nomdis="+nomdis+"&fitlic="+fitlic+"&fftlic="+fftlic+"&firlic="+firlic+"&ffrlic="+ffrlic+"&reqlic="+reqlic+"&nomlic="+nomlic+"&fitint="+fitint+"&fftint="+fftint+"&firint="+firint+"&ffrint="+ffrint+"&nomcons="+nomcons+"&avaobr="+avaobr+"&nomciv="+nomciv+"&nomele="+nomele+"&nomseg="+nomseg+"&fitcons="+fitcons+"&fftcons="+fftcons+"&fitpre="+fitpre+"&fftpre="+fftpre+"&fitsum="+fitsum+"&fftsum="+fftsum+"&fitciv="+fitciv+"&fftciv="+fftciv+"&fitele="+fitele+"&fftele="+fftele+"&fitseg="+fitseg+"&fftseg="+fftseg+"&pger="+pger+"&pdir="+pdir+"&peje="+peje+"&pase="+pase+"&pcaj="+pcaj+"&potro="+potro+"&suc="+v,true);
		ajax.send(null);
	}
}

//DB->Eliminar linea base (Solicitar motivo)
function ELIMINARLINEABASE(v)
{
	var rec = $("#motivo").val();
	if (v == "")
		v = $("#clavecajero").val();

	if (stringTrim(rec) != "")
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
			{
				document.getElementById('recu').innerHTML=ajax.responseText;
			}
			else
			{
				var closeButton = $(".close-reveal-modal");
				if (closeButton.length > 0)
				{
					closeButton[0].click();
				}
			}
		}
		jQuery("#recu").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?eliminarlineabase=si&motivo="+rec+"&clacaj="+v, true);
		ajax.send(null);
	}
	else
	{
		document.getElementById('recu').innerHTML = "<div class='validaciones' style='width: 100%' align='center'>Debe ingresar el motivo</div>";
	}
}

function CORREGIRTEXTO(v)
{
	var res = v.replace('#','REEMPLAZARNUMERAL');
	var res = res.replace('+','REEMPLAZARMAS');
	
	return res;
}
function REFRESCARACTIVOS()
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('activos').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#activos").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?refrescaractivos=si",true);
	ajax.send(null);
}
function CALCULARMODALIDAD()
{
	var moda = form1.modalidad.value;
	var fec = form1.fechainicio.value;
	
	if(moda != '')
	{
		$.post('calcularfechas.php', { moda: moda,fecini: fec }, function(data){
			if(data != "")
			{
				for(var j=0;j<data.length;j++)
				{
					var swinm = data[j].swinm;
					var swdis = data[j].swdis;
					var swlic = data[j].swlic;
					var swint = data[j].swint;
					var swcons = data[j].swcons;
					var swpre = data[j].swpre;
					var swsum = data[j].swsum;
					var swciv = data[j].swciv;
					var swele = data[j].swele;
					var swseg = data[j].swseg;
					
					var fiinm = data[j].fiinm;
					var ffinm = data[j].ffinm;
					var dinm = data[j].dinm;
					
					var fidis = data[j].fidis;
					var ffdis = data[j].ffdis;
					var ddis = data[j].ddis;
					
					var filic = data[j].filic;
					var fflic = data[j].fflic;
					var dlic = data[j].dlic;
					
					var fiint = data[j].fiint;
					var ffint = data[j].ffint;
					var dint = data[j].dint;
					
					var ficons = data[j].ficons;
					var ffcons = data[j].ffcons;
					var dcons = data[j].dcons;					
										
					var fipre = data[j].fipre;
					var ffpre = data[j].ffpre;
					var dpre = data[j].dpre;
					
					var fisum = data[j].fisum;
					var ffsum = data[j].ffsum;
					var dsum = data[j].dsum;
					
					var ficiv = data[j].ficiv;
					var ffciv = data[j].ffciv;
					var dciv = data[j].dciv;
					
					var fiele = data[j].fiele;
					var ffele = data[j].ffele;
					var dele = data[j].dele;
					
					var fiseg = data[j].fiseg;
					var ffseg = data[j].ffseg;
					var dseg = data[j].dseg;
					
					if(swinm == 0)
					{
						form1.requiereinmobiliaria[0].checked = false;
						form1.requiereinmobiliaria[1].checked = true;
						form1.nombreinmobiliaria.disabled = true;
						form1.nombreinmobiliaria.value = "";
					}
					else
					{
						form1.requiereinmobiliaria[0].checked = true;
						form1.requiereinmobiliaria[1].checked = false;
						form1.nombreinmobiliaria.disabled = false;
						form1.fechainicioteoricainmobiliaria.value = fiinm;
						form1.fechafinteoricainmobiliaria.value = ffinm;
						form1.totaldiasteoricainmobiliaria.value = dinm;		
					}
					
					if(swdis == 0)
					{
						form1.requierediseno[0].checked = false;
						form1.requierediseno[1].checked = true;
						form1.nombrediseno.disabled = true;
						form1.nombrediseno.value = "";
					}
					else
					{
						form1.requierediseno[0].checked = true;
						form1.requierediseno[1].checked = false;
						form1.nombrediseno.disabled = false;	
						form1.fechainicioteoricadiseno.value = fidis;
						form1.fechafinteoricadiseno.value = ffdis;
						form1.totaldiasteoricadiseno.value = ddis;				
					}
					
					if(swlic == 0)
					{
						form1.requierelicencia[0].checked = false;
						form1.requierelicencia[1].checked = true;
						form1.nombrelicencia.disabled = true;
						form1.nombrelicencia.value = "";
					}
					else
					{
						form1.requierelicencia[0].checked = true;
						form1.requierelicencia[1].checked = false;
						form1.nombrelicencia.disabled = false;
						form1.fechainicioteoricalicencia.value = filic;
						form1.fechafinteoricalicencia.value = fflic;
						form1.totaldiasteoricalicencia.value = dlic;
					}
					
					if(swint == 0)
					{
						/*form1.requiereinmobiliaria[0].checked = false;
						form1.requiereinmobiliaria[1].checked = true;
						form1.nombreinmobiliaria.disabled = true;
						form1.nombreinmobiliaria.value = "";*/
					}
					else
					{
						/*form1.requiereinmobiliaria[0].checked = true;
						form1.requiereinmobiliaria[1].checked = false;
						form1.nombreinmobiliaria.disabled = false;*/
						form1.fechainicioteoricainterventor.value = fiint;
						form1.fechafinteoricainterventor.value = ffint;
						form1.totaldiasteoricainterventor.value = dint;			
					}
					
					if(swcons == 0)
					{
						/*form1.requiereinmobiliaria[0].checked = false;
						form1.requiereinmobiliaria[1].checked = true;
						form1.nombreinmobiliaria.disabled = true;
						form1.nombreinmobiliaria.value = "";*/
					}
					else
					{
						/*form1.requiereinmobiliaria[0].checked = true;
						form1.requiereinmobiliaria[1].checked = false;
						form1.nombreinmobiliaria.disabled = false;*/
						form1.fechainicioteoricaconstructor.value = ficons;
						form1.fechafinteoricaconstructor.value = ffcons;
					}
					
					if(swpre == 0)
					{
						/*form1.requiereinmobiliaria[0].checked = false;
						form1.requiereinmobiliaria[1].checked = true;
						form1.nombreinmobiliaria.disabled = true;
						form1.nombreinmobiliaria.value = "";*/
					}
					else
					{
						/*form1.requiereinmobiliaria[0].checked = true;
						form1.requiereinmobiliaria[1].checked = false;
						form1.nombreinmobiliaria.disabled = false;*/
						form1.fechainicioteoricapresupuesto.value = fipre;
						form1.fechafinteoricapresupuesto.value = ffpre;
					}
					
					if(swsum == 0)
					{
						/*form1.requiereinmobiliaria[0].checked = false;
						form1.requiereinmobiliaria[1].checked = true;
						form1.nombreinmobiliaria.disabled = true;
						form1.nombreinmobiliaria.value = "";*/
					}
					else
					{
						/*form1.requiereinmobiliaria[0].checked = true;
						form1.requiereinmobiliaria[1].checked = false;
						form1.nombreinmobiliaria.disabled = false;*/
						form1.fechainicioteoricasuministro.value = fisum;
						form1.fechafinteoricasuministro.value = ffsum;
					}
					
					if(swciv == 0)
					{
						/*form1.requiereinmobiliaria[0].checked = false;
						form1.requiereinmobiliaria[1].checked = true;
						form1.nombreinmobiliaria.disabled = true;
						form1.nombreinmobiliaria.value = "";*/
					}
					else
					{
						/*form1.requiereinmobiliaria[0].checked = true;
						form1.requiereinmobiliaria[1].checked = false;
						form1.nombreinmobiliaria.disabled = false;*/
						form1.fechainicioteoricacivil.value = ficiv;
						form1.fechafinteoricacivil.value = ffciv;
					}
					
					if(swele == 0)
					{
						/*form1.requiereinmobiliaria[0].checked = false;
						form1.requiereinmobiliaria[1].checked = true;
						form1.nombreinmobiliaria.disabled = true;
						form1.nombreinmobiliaria.value = "";*/
					}
					else
					{
						/*form1.requiereinmobiliaria[0].checked = true;
						form1.requiereinmobiliaria[1].checked = false;
						form1.nombreinmobiliaria.disabled = false;*/
						form1.fechainicioteoricaelectromecanico.value = fiele;
						form1.fechafinteoricaelectromecanico.value = ffele;
					}
					
					if(swseg == 0)
					{
						/*form1.requiereinmobiliaria[0].checked = false;
						form1.requiereinmobiliaria[1].checked = true;
						form1.nombreinmobiliaria.disabled = true;
						form1.nombreinmobiliaria.value = "";*/
					}
					else
					{
						/*form1.requiereinmobiliaria[0].checked = true;
						form1.requiereinmobiliaria[1].checked = false;
						form1.nombreinmobiliaria.disabled = false;*/
						form1.fechainicioteoricaseguridad.value = fiseg;
						form1.fechafinteoricaseguridad.value = ffseg;
					}
				}
			}
		},"json");
	}
}
function CALCULARFECHAS(o)
{
	var moda = form1.modalidad.value;
	var modinm = form1.modainm.value;
	var modvis = form1.modavis.value;
	var moddis = form1.modadis.value;
	var modlic = form1.modalic.value;
	var modint = form1.modaint.value;
	
	var fecini = $("#fechainiciocajero").val();
	$.post('calcularfechas.php', { moda: moda,fecini: fecini }, function(data){
		if(data != "")
		{
			for(var j=0;j<data.length;j++)
			{
				alert("Inmobiliaria: "+data[j].fiinm+" "+data[j].ffinm+" "+data[j].dinm);
				alert("Visita: "+data[j].fivis+" "+data[j].ffvis+" "+data[j].dvis);
				alert("Comite: "+data[j].ficom+" "+data[j].ffcom+" "+data[j].dcom);
				alert("Contrato: "+data[j].ficon+" "+data[j].ffcon+" "+data[j].dcon);
				alert("Dise�o: "+data[j].fidis+" "+data[j].ffdis+" "+data[j].ddis);
				alert("Prefactibilidad: "+data[j].fiprefact+" "+data[j].ffprefact+" "+data[j].dprefact);
				alert("Pedido maquina: "+data[j].fiped+" "+data[j].ffped+" "+data[j].dped);
				alert("Licencia: "+data[j].filic+" "+data[j].fflic+" "+data[j].dlic);
				alert("Canal: "+data[j].fican+" "+data[j].ffcan+" "+data[j].dcan);
				alert("Proyeccion: "+data[j].fipro+" "+data[j].ffpro+" "+data[j].dpro);
				alert("Preliminar: "+data[j].fipre+" "+data[j].ffpre+" "+data[j].dpre);
				alert("Constructor: "+data[j].ficons+" "+data[j].ffcons+" "+data[j].dcons);
			}
		}
	},"json");

	if(modinm == 1)
	{
		$("#fechainicioinmobiliaria").val(fecini);
	}
	else
	if(modinm == 0 && modvis == 1)
	{
		$("#fechainiciovisita").val(fecini);
	}
	else
	if(modinm == 0 && modvis == 0 && moddis == 1)
	{
		$("#fechainiciodiseno").val(fecini);
	}
	else
	if(modinm == 0 && modvis == 0 && moddis == 0 && modlic == 1)
	{
		$("#fechainiciolicencia").val(fecini);
	}
	
	setTimeout("CALCULARFECHATEORICA('teoricainmobiliaria','INICAJ')",500);
	setTimeout("CALCULARTOTALDIAS('diastotalinmobiliaria','INICAJ')",500);
	setTimeout("CALCULARDIASACTOR('diasinmobiliaria')",500);
	
	setTimeout("CALCULARFECHATEORICA('teoricavisita','INICAJ')",500);
	setTimeout("CALCULARTOTALDIAS('diastotalvisita','INICAJ')",500);
	setTimeout("CALCULARDIASACTOR('diasvisita')",500);
	
	setTimeout("CALCULARFECHATEORICA('teoricadiseno','INICAJ')",500);
	setTimeout("CALCULARTOTALDIAS('diastotaldiseno','INICAJ')",500);
	setTimeout("CALCULARDIASACTOR('diasdiseno')",500);
	
	setTimeout("CALCULARFECHATEORICA('teoricalicencia','INICAJ')",500);
	setTimeout("CALCULARTOTALDIAS('diastotallicencia','INICAJ')",500);
	setTimeout("CALCULARDIASACTOR('diaslicencia')",500);
	
	setTimeout("CALCULARFECHATEORICA('teoricainterventoria','INICAJ')",500);
	
	if(modinm == 0){ form1.fechainicioinmobiliaria.value = ''; form1.teoricainmobiliaria.value = ''; form1.totaldiasinmobiliaria.value = ''; }
	if(modvis == 0){ form1.fechainiciovisita.value = ''; form1.teoricavisita.value = ''; form1.totaldiasvisita.value = ''; }
	if(moddis == 0){ form1.fechainiciodiseno.value = ''; form1.teoricadiseno.value = ''; form1.totaldiasdiseno.value = ''; }
	if(modlic == 0){ form1.fechainiciolicencia.value = ''; form1.teoricalicencia.value = ''; form1.totaldiaslicencia.value = ''; }
	if(modint == 0){ form1.teoricainterventoria.value = ''; }

	if(moda == '' || fecini == '')
	{
		$("#fechainicioinmobiliaria").val("");
		$("#teoricainmobiliaria").val("");
		$("#totaldiasinmobiliaria").val("");
		$("#fechainiciovisita").val("");
		$("#teoricavisita").val("");
		$("#totaldiasvisita").val("");
		$("#fechainiciodiseno").val("");
		$("#teoricadiseno").val("");
		$("#totaldiasdiseno").val("");
		$("#fechainiciolicencia").val("");
		$("#teoricalicencia").val("");
		$("#totaldiaslicencia").val("");
		$("#teoricainterventoria").val("");
	}
}
function CALCULARFECHATEORICA(o,c)
{
	var moda = form1.modalidad.value;
	
	var modinm = form1.modainm.value;
	var modvis = form1.modavis.value;
	var moddis = form1.modadis.value;
	var modlic = form1.modalic.value;
	var modint = form1.modaint.value;
	var modcons = form1.modacons.value;
	var modcom = form1.modacom.value;
	var modcon = form1.modacon.value;
	var modpre = form1.modapre.value;
	
	if(c == 'INICAJ')
	{
		var feciniinm = $("#fechainiciocajero").val();
		
		if(modinm == 1)
		{
			$("#fechainicioinmobiliaria").val(feciniinm);
		}
		else
		if(modinm == 0 && modvis == 1)
		{
			$("#fechainiciovisita").val(feciniinm);
		}
		else
		if(modinm == 0 && modvis == 0 && moddis == 1)
		{
			$("#fechainiciodiseno").val(feciniinm);
		}
		else
		if(modinm == 0 && modvis == 0 && moddis == 0 && modlic == 1)
		{
			$("#fechainiciolicencia").val(feciniinm);
		}
	}
	
	var dinm = form1.diasinm.value;
	var dvis = form1.diasvis.value;
	var ddis = form1.diasdis.value;
	var dlic = form1.diaslic.value;
	var dcons = form1.diascons.value;
	var dcom = form1.diascom.value;
	var dcon = form1.diascon.value;
	var dpre = form1.diaspre.value;
	
	if(o == 'teoricainmobiliaria')
	{
		if(modinm == 1)
		{
			var fecentinmob = $("#fechaentregainfoinmobiliaria").val();
			
			var dias = dinm;
			if(c == 'INICAJ')
			{
				var fecini = $("#fechainiciocajero").val();
			}
			else
			{
				var fecini = $("#fechainicioinmobiliaria").val();
			}
			
			if(c == 'INICAJ')
			{
				if(fecentinmob != '' && fecentinmob != '0000-00-00')//Si ya se entrego la informacion
				{
					var d = restaFechas(fecini,fecentinmob);
					
					var fechafin = sumaFecha(parseInt(d),fecini);
					if(modvis == 1)
					{
						$("#fechainiciovisita").val(fechafin);//Inicio visita: Es la final de inmobiliaria
					}
					else
					{
						$("#fechainiciovisita").val('');
					}
				}
				else
				{
					var fechafin = sumaFecha(parseInt(dias),fecini);
					if(modvis == 1)
					{
						$("#fechainiciovisita").val(fechafin);//Inicio visita: Es la final de inmobiliaria
					}
					else
					{
						$("#fechainiciovisita").val('');
					}
				}
			}
		}
		else
		{
			var dias = 0;
			var fecini = '';
		}
	}
	else
	if(o == 'teoricavisita')
	{
		if(modvis == 1)
		{
			var fecentinmob = $("#fechaentregainfoinmobiliaria").val();
			var fecentvis = $("#fechaentregavisita").val();
			
			if(c == 'INICAJ')
			{
				var fecini = $("#fechainiciocajero").val();
			}
			else
			{
				var fecini = $("#fechainicioinmobiliaria").val();
			}
			
			var dias = dvis;
			var fecini = $("#fechainiciovisita").val();//Fecha inicio de visita
			if(c == 'INICAJ')
			{
				if(fecentvis != '' && fecentvis != '0000-00-00')//Si ya se entrego la informacion
				{
					var d = restaFechas(fecini,fecentvis);
					
					var fechafin = sumaFecha(parseInt(d)+parseInt(dcom),fecini);
					if(moddis == 1)
					{
						$("#fechainiciodiseno").val(fechafin);//Inicio visita: Es la final de inmobiliaria
					}
					else
					{
						$("#fechainiciodiseno").val('');
					}
				}
				else
				{
					var fechafin = sumaFecha(parseInt(dias)+parseInt(dcom),fecini);
					if(moddis == 1)
					{
						$("#fechainiciodiseno").val(fechafin);//Inicio visita: Es la final de inmobiliaria
					}
					else
					{
						$("#fechainiciodiseno").val('');
					}
				}
			}
		}
		else
		{
			var dias = 0;
			var fecini = '';
		}
	}
	else
	if(o == 'teoricadiseno')
	{
		if(moddis == 1)
		{
			var fecentvis = $("#fechaentregavisita").val();
			var fecentdis = $("#fechaentregainfodiseno").val();
			if(c == 'INICAJ')
			{
				var fecini = $("#fechainiciocajero").val();
			}
			else
			{
				var fecini = $("#fechainicioinmobiliaria").val();
			}
			
			var fechafin = sumaFecha(parseInt(dinm),fecini);//final de inmobiliaria
			
			var dias = ddis;
			var fecini = $("#fechainiciodiseno").val();
			if(c == 'INICAJ')
			{
				if(fecentdis != '' && fecentdis != '0000-00-00')//Si ya se entrego la informacion
				{
					if(modlic == 1)
					{
						var d = restaFechas(fecini,fecentdis);
						$("#fechainiciolicencia").val(fecentdis);
					}
					else
					{
						$("#fechainiciolicencia").val('');
					}
				}
				else
				{
					var fechafin = sumaFecha(parseInt(ddis),fecini);
					if(modlic == 1)
					{
						$("#fechainiciolicencia").val(fechafin);//Inicio visita: Es la final de inmobiliaria
					}
					else
					{
						$("#fechainiciolicencia").val('');
					}
				}
			}
		}
		else
		{
			var dias = 0;
			var fecini = '';
		}
	}
	else
	if(o == 'teoricalicencia')
	{
		if(modlic == 1)
		{
			var fecentdis = $("#fechaentregainfodiseno").val();
			var fecentlic = $("#fechaentregainfolicencia").val();
			if(c == 'INICAJ')
			{
				var fecini = $("#fechainiciocajero").val();
			}
			else
			{
				var fecini = $("#fechainicioinmobiliaria").val();
			}
			var fechafin = sumaFecha(parseInt(dinm),fecini);//final de inmobiliaria
			
			var dias = dlic;
			var fecini = $("#fechainiciolicencia").val();
			
			if(fecentlic != '' && fecentlic != '0000-00-00')//Si ya se entrego la informacion
			{
				var d = restaFechas(fecini,fecentlic);
			}
		}
		else
		{
			var dias = 0;
			var fecini = '';
		}
	}
	else
	if(o == 'teoricainterventoria' && modint == 1)
	{
		var fecentcons = $("#fechaentregaatm").val();
		//Cuando no hay licencia
		if(modlic == 0)
		{
			if(c == 'INICAJ')
			{
				var fecini = $("#fechainiciocajero").val();
			}
			else
			{
				var fecini = $("#fechainicioinmobiliaria").val();
			}
			var fechafin = sumaFecha(parseInt(dinm),fecini);//final de inmobiliaria
			if(modvis == 1)
			{
				var fechafin = sumaFecha(parseInt(dvis),fechafin);
			}
			var fechafin = sumaFecha(parseInt(dcom)+parseInt(dcon)+parseInt(dpre)+parseInt(dcons),fechafin);//Final de dise�o + 5 dias del banco + 30 dias banco + 25 dias de ejecucion
			$("#teoricainterventoria").val(fechafin);
		}
		else
		{
			//Cuando hay licencia
			if(c == 'INICAJ')
			{
				var fecini = $("#fechainiciocajero").val();
			}
			else
			{
				var fecini = $("#fechainicioinmobiliaria").val();
			}
			var fechafin = sumaFecha(parseInt(dinm),fecini);//final de inmobiliaria
			if(modvis == 1)
			{
				var fechafin = sumaFecha(parseInt(dvis),fechafin);
			}
			if(moddis == 1)
			{
				var fechafin = sumaFecha(parseInt(ddis)+parseInt(dcom),fechafin);
			}
			var fechafin = sumaFecha(parseInt(dlic)+parseInt(dcom)+parseInt(dcons),fechafin);//Final de licencia + 25 dias de ejecucion
			$("#teoricainterventoria").val(fechafin);
		}
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById(o).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#"+o).html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarfechateorica=si&fec="+fecini+"&dias="+dias+"&opc="+o,true);
	ajax.send(null);
	
	if(modinm == 0)
	{
		form1.requiereinmobiliaria[0].checked = false;
		form1.requiereinmobiliaria[1].checked = true;
		form1.nombreinmobiliaria.disabled = true;
		form1.nombreinmobiliaria.value = "";
		form1.fechainicioinmobiliaria.disabled = true;
		form1.fechainicioinmobiliaria.value = "";
	}
	else
	{ 
		form1.requiereinmobiliaria[0].checked = true;
		form1.requiereinmobiliaria[1].checked = false;
		form1.nombreinmobiliaria.disabled = false;
		form1.fechainicioinmobiliaria.disabled = false;
	}
	
	if(modvis == 0)
	{ 
		form1.requierevisita[0].checked = false;
		form1.requierevisita[1].checked = true;
		form1.nombrevisita.disabled = true;
		form1.nombrevisita.value = "";
	}
	else
	{ 
		form1.requierevisita[0].checked = true; 
		form1.requierevisita[1].checked = false; 
		form1.nombrevisita.disabled = false;
	}
	
	if(moddis == 0)
	{ 
		form1.requierediseno[0].checked = false;
		form1.requierediseno[1].checked = true;
		form1.nombrediseno.disabled = true;
		form1.nombrediseno.value = "";
	}
	else
	{ 
		form1.requierediseno[0].checked = true; 
		form1.requierediseno[1].checked = false; 
		form1.nombrediseno.disabled = false;
	}
	
	if(modlic == 0)
	{ 
		form1.requierelicencia[0].checked = false;
		form1.requierelicencia[1].checked = true;
		form1.nombregestionador.disabled = true;
		form1.nombregestionador.value = "";
	}
	else
	{ 
		form1.requierelicencia[0].checked = true; 
		form1.requierelicencia[1].checked = false; 
		form1.nombregestionador.disabled = false;
	}
	
	if(moda == '')
	{
		$("#fechainicioinmobiliaria").val("");
		$("#teoricainmobiliaria").val("");
		$("#totaldiasinmobiliaria").val("");
		$("#fechainiciovisita").val("");
		$("#teoricavisita").val("");
		$("#totaldiasvisita").val("");
		$("#fechainiciodiseno").val("");
		$("#teoricadiseno").val("");
		$("#totaldiasdiseno").val("");
		$("#fechainiciolicencia").val("");
		$("#teoricalicencia").val("");
		$("#totaldiaslicencia").val("");
		$("#teoricainterventoria").val("");
	}
}
function restaFechas(f1,f2)
{
	var aFecha1 = f1.split('-'); 
	var aFecha2 = f2.split('-'); 
	var fFecha1 = Date.UTC(aFecha1[0],aFecha1[1]-1,aFecha1[2]); 
	var fFecha2 = Date.UTC(aFecha2[0],aFecha2[1]-1,aFecha2[2]); 
	var dif = fFecha2 - fFecha1;
	var dias = Math.floor(dif / (1000 * 60 * 60 * 24)); 
	return dias;
}
function sumaFecha(d, fecha)
{
	fecha=fecha.replace("-", "/").replace("-", "/");	  
	
	fecha= new Date(fecha);
	fecha.setDate(fecha.getDate()+d);
	
	var anio=fecha.getFullYear();
	var mes= fecha.getMonth()+1;
	var dia= fecha.getDate();
	
	if(mes.toString().length<2)
	{
		mes="0".concat(mes);        
	}    
	
	if(dia.toString().length<2)
	{
		dia="0".concat(dia);        
	}
	return anio+"-"+mes+"-"+dia;
}
function CALCULARTOTALDIAS(o,c)
{
	var modinm = form1.modainm.value;
	var modvis = form1.modavis.value;
	var moddis = form1.modadis.value;
	var modlic = form1.modalic.value;
	var modcons = form1.modacons.value;
	var modcom = form1.modacom.value;
	var modcon = form1.modacon.value;
	var modpre = form1.modapre.value;
	
	var dinm = form1.diasinm.value;
	var dvis = form1.diasvis.value;
	var ddis = form1.diasdis.value;
	var dlic = form1.diaslic.value;
	var dcons = form1.diascons.value;
	var dcom = form1.diascom.value;
	var dcon = form1.diascon.value;
	var dpre = form1.diaspre.value;
	
	if(o == 'diastotalinmobiliaria' && modinm == 1)
	{
		var dias = dinm;
		if(c == 'INICAJ')
		{
			var fecini = $("#fechainiciocajero").val();
		}
		else
		{
			var fecini = $("#fechainicioinmobiliaria").val();
		}
		var fecini = $("#fechainiciocajero").val();
		var fecent = $("#fechaentregainfoinmobiliaria").val();
	}
	else
	if(o == 'diastotalvisita' && modvis == 1)
	{
		var dias = dvis;
		var fecini = $("#fechainiciovisita").val();
		var fecent = $("#fechaentregavisita").val();
	}
	else
	if(o == 'diastotaldiseno' && moddis == 1)
	{
		var dias = ddis;
		var fecini = $("#fechainiciodiseno").val();
		var fecent = $("#fechaentregainfodiseno").val();
	}
	else
	if(o == 'diastotallicencia' && modlic == 1)
	{
		var dias = dlic;
		var fecini = $("#fechainiciolicencia").val();
		var fecent = $("#fechaentregainfolicencia").val();
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById(o).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#"+o).html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrartotaldias=si&fec="+fecini+"&dias="+dias+"&fecent="+fecent+"&campo="+o,true);
	ajax.send(null);
}
function CALCULARDIASACTOR(o)
{
	var dinm = form1.diasinm.value;
	var dvis = form1.diasvis.value;
	var ddis = form1.diasdis.value;
	var dlic = form1.diaslic.value;
	var dcons = form1.diascons.value;
	var dcom = form1.diascom.value;
	var dcon = form1.diascon.value;
	var dpre = form1.diaspre.value;
	
	if(o == 'diasinmobiliaria')
	{
		var fec = dinm;
	}
	else
	if(o == 'diasvisita')
	{
		var fec = dvis;
	}
	else
	if(o == 'diasdiseno')
	{
		var fec = ddis;
	}
	else
	if(o == 'diaslicencia')
	{
		var fec = dlic;
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById(o).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#"+o).html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrardiasactores=si&fec="+fec,true);
	ajax.send(null);
}
function EDITAR(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('sucursales').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#sucursales").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?editardatos=si&suc="+v,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTAS()",500);
	//setTimeout("MODALIDADACTOR("+m+")",500);
	OCULTARSCROLL();
}
function BUSCAR(p)
{	
	var consec = form1.busconsecutivo.value;
	var nom = form1.busnombre.value;
	var anocon = form1.busanocontable.value;
	var cod = form1.buscodigo.value;
	var reg = form1.busregion.value;
	var mun = form1.busmunicipio.value;
	var tipint = form1.bustipointervencion.value;
	var moda = form1.busmodalidad.value;
	var est = form1.ocultoestado.value;
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('busqueda').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#busqueda").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='50' width='50' />"); //loading gif will be overwrited when ajax have success
	if(p > 0)
	{
		ajax.open("GET","?buscar=si&consec="+consec+"&nom="+nom+"&anocon="+anocon+"&cod="+cod+"&reg="+reg+"&mun="+mun+"&tipint="+tipint+"&moda="+moda+"&est="+est+"&page="+p,true);
	}
	else
	{
		ajax.open("GET","?buscar=si&consec="+consec+"&nom="+nom+"&anocon="+anocon+"&cod="+cod+"&reg="+reg+"&mun="+mun+"&tipint="+tipint+"&moda="+moda+"&est="+est,true);
	}
	ajax.send(null);
	OCULTARSCROLL();
}
function VERDEPARTAMENTOS(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('departamentos').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#departamentos").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?verdepartamentos=si&reg="+v,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTADEPARTAMENTOS()",800);
}
function VERCIUDADES(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('ciudades').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#ciudades").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?verciudades=si&dep="+v,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTACIUDADES()",800);
}
function VERCIUDADES1(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('ciudades1').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#ciudades1").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?verciudades1=si&reg="+v,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTACIUDADES1()",1000);
}
function VERMODALIDADES(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('modalidades').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#modalidades").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?vermodalidades=si&tii="+v,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTAMODALIDADES()",800);
}
function VERMODALIDADES1(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('modalidades1').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#modalidades1").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?vermodalidades1=si&tii="+v,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTAMODALIDADES1()",800);
}
function MODALIDADACTOR(v)
{
	var fecini = $("#fechainicioinmobiliaria").val();	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('modalidadactor').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#modalidadactor").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?modalidadactor=si&mod="+v,true);
	ajax.send(null);
	setTimeout("CALCULARFECHAS('INMOBILIARIA')",800);
	if(v == '' || fecini == '')
	{
		$("#fechainicioinmobiliaria").val("");
		$("#teoricainmobiliaria").val("");
		$("#totaldiasinmobiliaria").val("");
		$("#fechainiciovisita").val("");
		$("#teoricavisita").val("");
		$("#totaldiasvisita").val("");
		$("#fechainiciodiseno").val("");
		$("#teoricadiseno").val("");
		$("#totaldiasdiseno").val("");
		$("#fechainiciolicencia").val("");
		$("#teoricalicencia").val("");
		$("#totaldiaslicencia").val("");
		$("#teoricainterventoria").val("");
	}
}
function LIMPIAR()
{
	//INFO BASICA
	//***********
	$('#codigo').val('');
	$('#nombre').val('');
	$('#direccion').val('');
	$('#region').val('');
	$('#departamento').val('');
	$('#municipio').val('');
	$('#imagen').val('');
	$('#area').val('');
	$('#tipointervencion').val('');
	$('#formato').val('');
	$('#fechainicio').val('');
	$('#modalidad').val('');
	$('#anocontable').val('');
	$('#estado').val(1);
	
	//INFO ADICIONAL
	//**************
	$('#gerente').val('');
	$('#director').val('');
	$('#ejecutivo').val('');
	$('#asesor').val('');
	$('#cajas').val('');
	$('#otros').val('');
	
	//INMOBILIARIA
	//************
	$('#fechainicioteoricainmobiliaria').val('');
	$('#fechafinteoricainmobiliaria').val('');
	$('#fechainiciorealinmobiliaria').val('');
	$('#fechafinrealinmobiliaria').val('');
	$('#requiereinmobiliaria').val('');
	$('#nombreinmobiliaria').val('');
	
	//DISE�O
	//******
	$('#fechainicioteoricadiseno').val('');
	$('#fechafinteoricadiseno').val('');
	$('#fechainiciorealdiseno').val('');
	$('#fechafinrealdiseno').val('');
	$('#requierediseno').val('');
	$('#nombrediseno').val('');
	
	//LICENCIA
	//********
	$('#fechainicioteoricalicencia').val('');
	$('#fechafinteoricalicencia').val('');
	$('#fechainicioreallicencia').val('');
	$('#fechafinreallicencia').val('');
	$('#requierelicencia').val('');
	$('#nombrelicencia').val('');
	
	//INTERVENCI�N
	//************
	$('#fechainicioteoricainterventor').val('');
	$('#fechafinteoricainterventor').val('');
	$('#fechainiciorealinterventor').val('');
	$('#fechafinrealinterventor').val('');
	$('#nombreconstructor').val('');
	$('#avanceobra').val('');
	$('#nombreintcivil').val('');
	$('#nombreintelectromecanico').val('');
	$('#nombreintseguridad').val('');
	
	//CONSTRUCTOR
	$('#fechainicioteoricaconstructor').val('');
	$('#fechafinteoricaconstructor').val('');
	
	//PRESUPUESTO
	$('#fechainicioteoricapresupuesto').val('');
	$('#fechafinteoricapresupuesto').val('');
	
	//SUMINISTRO
	$('#fechainicioteoricasuministro').val('');
	$('#fechafinteoricasuministro').val('');
	
	//INT.CIVIL
	$('#fechainicioteoricacivil').val('');
	$('#fechafinteoricacivil').val('');
	
	//INT.ELECTROMECANICO
	$('#fechainicioteoricaelectromecanico').val('');
	$('#fechafinteoricaelectromecanico').val('');
	
	//SEGURIDAD
	$('#fechainicioteoricaseguridad').val('');
	$('#fechafinteoricaseguridad').val('');
	document.location.href = "#top";
}
function RESULTADOADJUNTO(v,cs)
{
	if(v == 'FORMATODISENO')
	{
		var div = 'resultadoadjuntoformatodiseno';
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById(div).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#"+div).html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?estadoadjunto=si&actor="+v+"&clasuc="+cs,true);
	ajax.send(null);
	OCULTARSCROLL();
}
function MOSTRARRUTA(v)
{
	if(v == 'INMOBILIARIA')
	{
		var div = 'resultadoadjuntoinm';
		var nomadj = form1.adjuntoinm.value;
	}
	else
	if(v == 'VISITA')
	{
		var div = 'resultadoadjuntovis';
		var nomadj = form1.adjuntovis.value;
	}
	else
	if(v == 'DISENO')
	{
		var div = 'resultadoadjuntodis';
		var nomadj = form1.adjuntodis.value;
	}
	else
	if(v == 'LICENCIA')
	{
		var div = 'resultadoadjuntolic';
		var nomadj = form1.adjuntolic.value;
	}
	else
	if(v == 'ADJUNTO')
	{
		var div = 'resultadoadjunto';
		var nomadj = $('#archivoadjunto').val();
	}
	else
	if(v == 'COMITE')
	{
		var div = 'resultadoadjuntocomite';
		var nomadj = form1.adjuntocomite.value;
	}
	else
	if(v == 'CONTRATO')
	{
		var div = 'resultadoadjuntocontrato';
		var nomadj = form1.adjuntocontrato.value;
	}
	else
	if(v == 'PREFACTIBILIDAD')
	{
		var div = 'resultadoadjuntoprefactibilidad';
		var nomadj = form1.adjuntoprefactibilidad.value;
	}
	else
	if(v == 'PEDIDOMAQUINA')
	{
		var div = 'resultadoadjuntopedidomaquina';
		var nomadj = form1.adjuntopedidomaquina.value;
	}
	else
	if(v == 'PROYECCION')
	{
		var div = 'resultadoadjuntoproyeccion';
		var nomadj = form1.adjuntoproyeccion.value;
	}
	else
	if(v == 'CANAL')
	{
		var div = 'resultadoadjuntocanal';
		var nomadj = form1.adjuntocanal.value;
	}
	else
	if(v == 'FORMATODISENO')
	{
		var div = 'resultadoadjuntoformatodiseno';
		var nomadj = form1.adjuntoformatodiseno.value;
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById(div).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#"+div).html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarruta=si&nomadj="+nomadj,true);
	ajax.send(null);
	OCULTARSCROLL();
}
function MOSTRARADJUNTOS(c,a)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('adjuntoarchivos').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#adjuntoarchivos").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostraradjuntos=si&clasuc="+c+"&actor="+a,true);
	ajax.send(null);
}
function VERIFICAROTRO(v)
{
	if(v == 'IMAGEN')
	{
		var ima = form1.imagen.value;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('cualima').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#cualima").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?verificarotro=si&ima="+ima+"&otro="+v,true);
		ajax.send(null);
	}
	else
	if(v == 'FORMATO')
	{
		var form = form1.formato.value;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('cualfor').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#cualfor").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?verificarotro=si&form="+form+"&otro="+v,true);
		ajax.send(null);
	}
	else
	if(v == 'TIPOINTERVENCION')
	{
		var tipint = form1.tipointervencion.value;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('cualtipint').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#cualtipint").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?verificarotro=si&tipint="+tipint+"&otro="+v,true);
		ajax.send(null);
	}
}
function AGREGAR(v)
{
	if(v == 'IMAGEN')
	{
		var ima = form1.cualima.value;
		
		document.getElementById('cualima').style.display = 'none';
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('miimagen').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#miimagen").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?agregarima=si&ima="+ima,true);
		ajax.send(null);
	}
	else
	if(v == 'FORMATO')
	{
		var form = form1.cualfor.value;
		
		document.getElementById('cualfor').style.display = 'none';
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('miformato').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#miformato").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?agregarform=si&form="+form,true);
		ajax.send(null);
	}
	else
	if(v == 'TIPOINTERVENCION')
	{
		var tipint = form1.cualtipint.value;
		
		document.getElementById('cualtipint').style.display = 'none';
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('mitipointervencion').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#mitipointervencion").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?agregartipint=si&tipint="+tipint,true);
		ajax.send(null);
	}
}
function ELIMINARSUCURSAL(v)
{	
	if(confirm('Esta seguro/a de Eliminar esta sucursal?'))
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('estadoregistro').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#estadoregistro").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?eliminarsucursal=si&suc="+v,true);
		ajax.send(null);
		setTimeout("window.location.href = 'sucursales.php';",500);
	}
}
function AGREGARNOTA(c)
{
	var not = $('#nota').val();
	
	var nuevanota = CORREGIRTEXTO(not);
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('agregados').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#agregados").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?agregarnota=si&not="+nuevanota+"&caj="+c,true);
	ajax.send(null);
}
function MOSTRARNOTAS(c)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todaslasnotas').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todaslasnotas").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarnotas=si&clacaj="+c,true);
	ajax.send(null);
}
function MOSTRARBITACORA(c)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todaslasnotas').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todaslasnotas").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarbitacora=si&clasuc="+c,true);
	ajax.send(null);
}
function MOSTRARBITACORAADJUNTOS(c)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todoslosadjuntosbitacora').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todoslosadjuntosbitacora").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarbitacoraadjuntos=si&clasuc="+c,true);
	ajax.send(null);
}
function EDITARNOTA(c,cn)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('editarnota').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#editarnota").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?editarnota=si&clanot="+cn+"&caj="+c,true);
	ajax.send(null);
}
function ACTUALIZARNOTA(n,c)
{
	var not = $('#notaedi').val();
	var nuevanota = CORREGIRTEXTO(not);
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todaslasnotas').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todaslasnotas").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?actualizarnota=si&not="+nuevanota+"&clanot="+n+"&caj="+c,true);
	ajax.send(null);
}
function ELIMINARNOTA(c,caj)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todaslasnotas').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todaslasnotas").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?eliminarnota=si&clanot="+c+"&caj="+caj,true);
	ajax.send(null);
}
function EDITARADJUNTO(c,a)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('editaradjunto').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#editaradjunto").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?editaradjunto=si&claada="+c+"&act="+a,true);
	ajax.send(null);
}
function RESULTADOADJUNTOINFORMACION(ca)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('resultadoadjunto').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#resultadoadjunto").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?estadoadjuntoinformacion=si&claada="+ca,true);
	ajax.send(null);
}
function ELIMINARADJUNTO(v)
{
	if(confirm('Esta seguro/a de Eliminar este archivo?'))
	{
		var dataString = 'id='+v;
		
		$.ajax({
	        type: "POST",
	        url: "deletearchivo.php",
	        data: dataString,
	        success: function() {
				//$('#delete-ok').empty();
				//$('#delete-ok').append('<div class="correcto">Se ha eliminado correctamente la entrada con id='+v+'.</div>').fadeIn("slow");
				$('#service'+v).fadeOut("slow");
				//$('#'+v).remove();
	        }
	    });
	}
}
function HIJOSSELECCIONADOS()
{
	var che = form1.padre.checked;
	var hijos = "";
	var objCBarray = document.getElementsByName('selectItemhijos');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	hijos += objCBarray[i].value + ",";
	    }
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('mostrarhijos').innerHTML=ajax.responseText;
	    }
	}
	//jQuery("#resultadoadjunto").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarhijos=si&hij="+hijos+"&che="+che,true);
	ajax.send(null);
}
function HIJOSSELECCIONADOS1()
{
	var che = form1.padre.checked;
	var hijos = "";
	var objCBarray = iframe2.document.getElementsByName('selectItemhijos');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	hijos += objCBarray[i].value + ",";
	    }
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('mostrarhijos').innerHTML=ajax.responseText;
	    }
	}
	//jQuery("#resultadoadjunto").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarhijos=si&hij="+hijos+"&che="+che,true);
	ajax.send(null);
}
function VERPEDIDOMAQUINA(cc)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('pedirmaquina').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#pedirmaquina").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?verpedidomaquina=si&cc="+cc,true);
	ajax.send(null);
}
function stringTrim(x) {
	return x.replace(/^\s+|\s+$/gm,'');
}
function VERINFO(opc)
{
	if(opc == "INFORMACIONBASICA")
	{
		var ocu = $('#OCUINFORMACIONBASICA').val();
		if(ocu == 0)
		{
			document.getElementById('IMGINFORMACIONBASICA').src = "../../images/Minus.png";
			$('#OCUINFORMACIONBASICA').val(1);
			document.getElementById("VERINFORMACIONBASICA").style.display = "block";
		}
		else
		{
			document.getElementById('IMGINFORMACIONBASICA').src = "../../images/Plus.png";
			$('#OCUINFORMACIONBASICA').val(0);
			document.getElementById("VERINFORMACIONBASICA").style.display = "none";
		}
	}
	if(opc == "INFORMACIONADICIONAL")
	{
		var ocu = $('#OCUINFORMACIONADICIONAL').val();
		if(ocu == 0)
		{
			document.getElementById('IMGINFORMACIONADICIONAL').src = "../../images/Minus.png";
			$('#OCUINFORMACIONADICIONAL').val(1);
			document.getElementById("VERINFORMACIONADICIONAL").style.display = "block";
		}
		else
		{
			document.getElementById('IMGINFORMACIONADICIONAL').src = "../../images/Plus.png";
			$('#OCUINFORMACIONADICIONAL').val(0);
			document.getElementById("VERINFORMACIONADICIONAL").style.display = "none";
		}
	}
	else
	if(opc == "INMOBILIARIA")
	{
		var ocu = $('#OCUINMOBILIARIA').val();
		if(ocu == 0)
		{
			document.getElementById('IMGINMOBILIARIA').src = "../../images/Minus.png";
			$('#OCUINMOBILIARIA').val(1);
			document.getElementById("VERINMOBILIARIA").style.display = "block";
		}
		else
		{
			document.getElementById('IMGINMOBILIARIA').src = "../../images/Plus.png";
			$('#OCUINMOBILIARIA').val(0);
			document.getElementById("VERINMOBILIARIA").style.display = "none";
		}
	}
	else
	if(opc == "DISENO")
	{
		var ocu = $('#OCUDISENO').val();
		if(ocu == 0)
		{
			document.getElementById('IMGDISENO').src = "../../images/Minus.png";
			$('#OCUDISENO').val(1);
			document.getElementById("VERDISENO").style.display = "block";
		}
		else
		{
			document.getElementById('IMGDISENO').src = "../../images/Plus.png";
			$('#OCUDISENO').val(0);
			document.getElementById("VERDISENO").style.display = "none";
		}
	}
	else
	if(opc == "LICENCIA")
	{
		var ocu = $('#OCULICENCIA').val();
		if(ocu == 0)
		{
			document.getElementById('IMGLICENCIA').src = "../../images/Minus.png";
			$('#OCULICENCIA').val(1);
			document.getElementById("VERLICENCIA").style.display = "block";
		}
		else
		{
			document.getElementById('IMGLICENCIA').src = "../../images/Plus.png";
			$('#OCULICENCIA').val(0);
			document.getElementById("VERLICENCIA").style.display = "none";
		}
	}
	else
	if(opc == "INTERVENTORIA")
	{
		var ocu = $('#OCUINTERVENTORIA').val();
		if(ocu == 0)
		{
			document.getElementById('IMGINTERVENTORIA').src = "../../images/Minus.png";
			$('#OCUINTERVENTORIA').val(1);
			document.getElementById("VERINTERVENTORIA").style.display = "block";
		}
		else
		{
			document.getElementById('IMGINTERVENTORIA').src = "../../images/Plus.png";
			$('#OCUINTERVENTORIA').val(0);
			document.getElementById("VERINTERVENTORIA").style.display = "none";
		}
	}
	else
	if(opc == "INFORMACIONSUCURSAL")
	{
		var ocu = $('#OCUINFORMACIONSUCURSAL').val();
		if(ocu == 0)
		{
			document.getElementById('IMGINFORMACIONSUCURSAL').src = "../../images/Minus.png";
			$('#OCUINFORMACIONSUCURSAL').val(1);
			
			document.getElementById("VERINFORMACIONBASICA").style.display = "block";
			document.getElementById("VERINFORMACIONADICIONAL").style.display = "block";
			document.getElementById("VERINMOBILIARIA").style.display = "block";
			document.getElementById("VERDISENO").style.display = "block";
			document.getElementById("VERLICENCIA").style.display = "block";
			document.getElementById("VERINTERVENTORIA").style.display = "block";
			
			document.getElementById('IMGINFORMACIONBASICA').src = "../../images/Minus.png";
			document.getElementById('IMGINFORMACIONADICIONAL').src = "../../images/Minus.png";
			document.getElementById('IMGINMOBILIARIA').src = "../../images/Minus.png";
			document.getElementById('IMGDISENO').src = "../../images/Minus.png";
			document.getElementById('IMGLICENCIA').src = "../../images/Minus.png";
			document.getElementById('IMGINTERVENTORIA').src = "../../images/Minus.png";
			
			$('#OCUINFORMACIONBASICA').val(1);
			$('#OCUINFORMACIONADICIONAL').val(1);
			$('#OCUINMOBILIARIA').val(1);
			$('#OCUDISENO').val(1);
			$('#OCULICENCIA').val(1);
			$('#OCUINTERVENTORIA').val(1);
		}
		else
		{
			document.getElementById('IMGINFORMACIONSUCURSAL').src = "../../images/Plus.png";
			$('#OCUINFORMACIONSUCURSAL').val(0);
			
			document.getElementById("VERINFORMACIONBASICA").style.display = "none";
			document.getElementById("VERINFORMACIONADICIONAL").style.display = "none";
			document.getElementById("VERINMOBILIARIA").style.display = "none";
			document.getElementById("VERDISENO").style.display = "none";
			document.getElementById("VERLICENCIA").style.display = "none";
			document.getElementById("VERINTERVENTORIA").style.display = "none";
			
			document.getElementById('IMGINFORMACIONBASICA').src = "../../images/Plus.png";
			document.getElementById('IMGINFORMACIONADICIONAL').src = "../../images/Plus.png";
			document.getElementById('IMGINMOBILIARIA').src = "../../images/Plus.png";
			document.getElementById('IMGDISENO').src = "../../images/Plus.png";
			document.getElementById('IMGLICENCIA').src = "../../images/Plus.png";
			document.getElementById('IMGINTERVENTORIA').src = "../../images/Plus.png";
			
			$('#OCUINFORMACIONBASICA').val(0);
			$('#OCUINFORMACIONADICIONAL').val(0);
			$('#OCUINMOBILIARIA').val(0);
			$('#OCUDISENO').val(0);
			$('#OCULICENCIA').val(0);
			$('#OCUINTERVENTORIA').val(0);
		}
	}
	OCULTARSCROLL();
}