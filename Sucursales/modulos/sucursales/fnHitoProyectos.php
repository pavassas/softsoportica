<?php
error_reporting(0);
include('../../data/Conexion.php');
session_start();
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_SESSION['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");

//CALCULO DE PORCENTAJE DE EJECUCION

/*
Activos
select * from sucursal where suc_estado = 1 and suc_sw_eliminado = 0
Programados
select * from sucursal where suc_estado = 5 and suc_sw_eliminado = 0
Suspendidos
select * from sucursal where suc_estado = 2 and suc_sw_eliminado = 0
Entregados
select * from sucursal where suc_estado = 3 and suc_sw_eliminado = 0
*/

$con = mysqli_query($conectar,"select s.suc_sw_busqueda_local,sucinm.sui_sw_requiere_licencia,sucdis.sud_req_diseno,succons.sco_constructor,succiv.sci_int_civil,sucele.sue_int_electromecanico,sucseg.sse_instalador,s.suc_clave_int,s.suc_estado,s.suc_codigo,s.suc_nombre,lb.lib_fec_inmobiliaria,lb.lib_fecteo_inmobiliaria,lb.lib_fec_diseno,lb.lib_fecteo_diseno,lb.lib_fec_licencia,lb.lib_fecteo_licencia,lb.lib_fec_interventoria,lb.lib_fecteo_interventoria,lb.lib_fec_constructor,lb.lib_fecteo_constructor,lb.lib_fec_presupuesto,lb.lib_fecteo_presupuesto from sucursal s inner join linea_base lb on (lb.suc_clave_int = s.suc_clave_int) left outer join sucursal_inmobiliaria sucinm on (sucinm.suc_clave_int = s.suc_clave_int) left outer join sucursal_diseno sucdis on (sucdis.suc_clave_int = s.suc_clave_int) left outer join sucursal_licencia suclic on (suclic.suc_clave_int = s.suc_clave_int) left outer join sucursal_adjudicacion sucadju on (sucadju.suc_clave_int = s.suc_clave_int) left outer join sucursal_constructor succons on (succons.suc_clave_int = s.suc_clave_int) left outer join sucursal_presupuesto sucpre on (sucpre.suc_clave_int = s.suc_clave_int) left outer join sucursal_suministro sucsum on (sucsum.suc_clave_int = s.suc_clave_int) left outer join sucursal_civil succiv on (succiv.suc_clave_int = s.suc_clave_int) left outer join sucursal_electromecanico sucele on (sucele.suc_clave_int = s.suc_clave_int) left outer join sucursal_seguridad sucseg on (sucseg.suc_clave_int = s.suc_clave_int) left outer join sucursal_alarma sucala on (sucala.suc_clave_int = s.suc_clave_int) where suc_estado = 1 and suc_sw_eliminado = 0");
$num = mysqli_num_rows($con);
$sumporejecucion = 0;

for($i = 0; $i < $num; $i++)
{
	$cantejecutada = 0;
	$dato = mysqli_fetch_array($con);
	$inm = $dato['suc_sw_busqueda_local'];
	$dis = $dato['sud_req_diseno'];
	$lic = $dato['sui_sw_requiere_licencia'];
	$cons = $dato['sco_constructor'];
	$civ = $dato['sci_int_civil'];
	$ele = $dato['sue_int_electromecanico'];
	$seg = $dato['sse_instalador'];
    $ala = $dato['sia_instalador'];
	
	$fitinm = $dato['lib_fec_inmobiliaria']; 
	$fftinm = $dato['lib_fecteo_inmobiliaria'];
	$fitdis = $dato['lib_fec_diseno'];
	$fftdis = $dato['lib_fecteo_diseno'];
	$fitlic = $dato['lib_fec_licencia'];
	$fftlic = $dato['lib_fecteo_licencia'];
	$fitint = $dato['lib_fec_interventoria'];
	$fftint = $dato['lib_fecteo_interventoria'];
	$fitcons = $dato['lib_fec_constructor'];
	$fftcons = $dato['lib_fecteo_constructor'];
	$fitpre = $dato['lib_fec_presupuesto'];
	$fftpre = $dato['lib_fecteo_presupuesto'];
	$fitsum = $dato['lib_fec_suministro'];
	$fftsum = $dato['lib_fecteo_suministro'];
	$fitciv = $dato['lib_fec_civil'];
	$fftciv = $dato['lib_fecteo_civil'];
	$fitele = $dato['lib_fec_electromecanico'];
	$fftele = $dato['lib_fecteo_electromecanico'];
	$fitseg = $dato['lib_fec_seguridad'];
	$fftseg = $dato['lib_fecteo_seguridad'];
	
	if($inm == 1){ $cantejecutada = $cantejecutada+1; }
	if($dis == 1){ $cantejecutada = $cantejecutada+1; }
	if($lic == 1){ $cantejecutada = $cantejecutada+1; }
	if($cons > 0){ $cantejecutada = $cantejecutada+1; }
	if($civ > 0){ $cantejecutada = $cantejecutada+1; }
	if($ele > 0){ $cantejecutada = $cantejecutada+1; }
	if($seg > 0){ $cantejecutada = $cantejecutada+1; }
    if($ala > 0){ $cantejecutada = $cantejecutada+1; }
	if($fftsum != "" and $fftsum != "0000-00-00"){ $cantejecutada = $cantejecutada+1; }
	if($fftpre != "" and $fftpre != "0000-00-00"){ $cantejecutada = $cantejecutada+1; }
	if($fftint != "" and $fftint != "0000-00-00"){ $cantejecutada = $cantejecutada+1; }
	$porcentaje = 10*$cantejecutada;
	$sumporejecucion = $sumporejecucion+$porcentaje;
	//echo "Porcentaje: ".$porcentaje." Sumporejecucion: ".$sumporejecucion."<br>";
}

$con = mysqli_query($conectar,"select s.suc_sw_busqueda_local,sucinm.sui_sw_requiere_licencia,sucdis.sud_req_diseno,succons.sco_constructor,succiv.sci_int_civil,sucele.sue_int_electromecanico,sucseg.sse_instalador,sucala.sia_instalador,s.suc_clave_int,s.suc_estado,s.suc_codigo,s.suc_nombre,lb.lib_fec_inmobiliaria,lb.lib_fecteo_inmobiliaria,lb.lib_fec_diseno,lb.lib_fecteo_diseno,lb.lib_fec_licencia,lb.lib_fecteo_licencia,lb.lib_fec_interventoria,lb.lib_fecteo_interventoria,lb.lib_fec_constructor,lb.lib_fecteo_constructor,lb.lib_fec_presupuesto,lb.lib_fecteo_presupuesto from sucursal s inner join linea_base lb on (lb.suc_clave_int = s.suc_clave_int) left outer join sucursal_inmobiliaria sucinm on (sucinm.suc_clave_int = s.suc_clave_int) left outer join sucursal_diseno sucdis on (sucdis.suc_clave_int = s.suc_clave_int) left outer join sucursal_licencia suclic on (suclic.suc_clave_int = s.suc_clave_int) left outer join sucursal_adjudicacion sucadju on (sucadju.suc_clave_int = s.suc_clave_int) left outer join sucursal_constructor succons on (succons.suc_clave_int = s.suc_clave_int) left outer join sucursal_presupuesto sucpre on (sucpre.suc_clave_int = s.suc_clave_int) left outer join sucursal_suministro sucsum on (sucsum.suc_clave_int = s.suc_clave_int) left outer join sucursal_civil succiv on (succiv.suc_clave_int = s.suc_clave_int) left outer join sucursal_electromecanico sucele on (sucele.suc_clave_int = s.suc_clave_int) left outer join sucursal_seguridad sucseg on (sucseg.suc_clave_int = s.suc_clave_int)
  left outer join sucursal_alarama sucala on (sucala.suc_clave_int = s.suc_clave_int)where suc_estado = 5 and suc_sw_eliminado = 0");
$num1 = mysqli_num_rows($con);
$sumporejecucion1 = 0;

for($i = 0; $i < $num1; $i++)
{
	$cantejecutada = 0;
	$dato = mysqli_fetch_array($con);
	$inm = $dato['suc_sw_busqueda_local'];
	$dis = $dato['sud_req_diseno'];
	$lic = $dato['sui_sw_requiere_licencia'];
	$cons = $dato['sco_constructor'];
	$civ = $dato['sci_int_civil'];
	$ele = $dato['sue_int_electromecanico'];
	$seg = $dato['sse_instalador'];
    $ala = $dato['sia_instalador'];
	
	$fitinm = $dato['lib_fec_inmobiliaria']; 
	$fftinm = $dato['lib_fecteo_inmobiliaria'];
	$fitdis = $dato['lib_fec_diseno'];
	$fftdis = $dato['lib_fecteo_diseno'];
	$fitlic = $dato['lib_fec_licencia'];
	$fftlic = $dato['lib_fecteo_licencia'];
	$fitint = $dato['lib_fec_interventoria'];
	$fftint = $dato['lib_fecteo_interventoria'];
	$fitcons = $dato['lib_fec_constructor'];
	$fftcons = $dato['lib_fecteo_constructor'];
	$fitpre = $dato['lib_fec_presupuesto'];
	$fftpre = $dato['lib_fecteo_presupuesto'];
	$fitsum = $dato['lib_fec_suministro'];
	$fftsum = $dato['lib_fecteo_suministro'];
	$fitciv = $dato['lib_fec_civil'];
	$fftciv = $dato['lib_fecteo_civil'];
	$fitele = $dato['lib_fec_electromecanico'];
	$fftele = $dato['lib_fecteo_electromecanico'];
	$fitseg = $dato['lib_fec_seguridad'];
	$fftseg = $dato['lib_fecteo_seguridad'];
	
	if($inm == 1){ $cantejecutada = $cantejecutada+1; }
	if($dis == 1){ $cantejecutada = $cantejecutada+1; }
	if($lic == 1){ $cantejecutada = $cantejecutada+1; }
	if($cons > 0){ $cantejecutada = $cantejecutada+1; }
	if($civ > 0){ $cantejecutada = $cantejecutada+1; }
	if($ele > 0){ $cantejecutada = $cantejecutada+1; }
	if($seg > 0){ $cantejecutada = $cantejecutada+1; }
    if($ala > 0){ $cantejecutada = $cantejecutada+1; }
	if($fftsum != "" and $fftsum != "0000-00-00"){ $cantejecutada = $cantejecutada+1; }
	if($fftpre != "" and $fftpre != "0000-00-00"){ $cantejecutada = $cantejecutada+1; }
	if($fftint != "" and $fftint != "0000-00-00"){ $cantejecutada = $cantejecutada+1; }
	$porcentaje = 10*$cantejecutada;
	$sumporejecucion1 = $sumporejecucion1+$porcentaje;
	//echo "Porcentaje: ".$porcentaje." Sumporejecucion: ".$sumporejecucion1."<br>";
}

$con = mysqli_query($conectar,"select s.suc_sw_busqueda_local,sucinm.sui_sw_requiere_licencia,sucdis.sud_req_diseno,succons.sco_constructor,succiv.sci_int_civil,sucele.sue_int_electromecanico,sucseg.sse_instalador,sucala.sia_instalador,s.suc_clave_int,s.suc_estado,s.suc_codigo,s.suc_nombre,lb.lib_fec_inmobiliaria,lb.lib_fecteo_inmobiliaria,lb.lib_fec_diseno,lb.lib_fecteo_diseno,lb.lib_fec_licencia,lb.lib_fecteo_licencia,lb.lib_fec_interventoria,lb.lib_fecteo_interventoria,lb.lib_fec_constructor,lb.lib_fecteo_constructor,lb.lib_fec_presupuesto,lb.lib_fecteo_presupuesto from sucursal s inner join linea_base lb on (lb.suc_clave_int = s.suc_clave_int) left outer join sucursal_inmobiliaria sucinm on (sucinm.suc_clave_int = s.suc_clave_int) left outer join sucursal_diseno sucdis on (sucdis.suc_clave_int = s.suc_clave_int) left outer join sucursal_licencia suclic on (suclic.suc_clave_int = s.suc_clave_int) left outer join sucursal_adjudicacion sucadju on (sucadju.suc_clave_int = s.suc_clave_int) left outer join sucursal_constructor succons on (succons.suc_clave_int = s.suc_clave_int) left outer join sucursal_presupuesto sucpre on (sucpre.suc_clave_int = s.suc_clave_int) left outer join sucursal_suministro sucsum on (sucsum.suc_clave_int = s.suc_clave_int) left outer join sucursal_civil succiv on (succiv.suc_clave_int = s.suc_clave_int) left outer join sucursal_electromecanico sucele on (sucele.suc_clave_int = s.suc_clave_int) left outer join sucursal_seguridad sucseg on (sucseg.suc_clave_int = s.suc_clave_int) left outer join sucursal_alarma sucala on (sucala.suc_clave_int = s.suc_clave_int) where suc_estado = 2 and suc_sw_eliminado = 0");
$num2 = mysqli_num_rows($con);
$sumporejecucion2 = 0;

for($i = 0; $i < $num2; $i++)
{
	$cantejecutada = 0;
	$dato = mysqli_fetch_array($con);
	$inm = $dato['suc_sw_busqueda_local'];
	$dis = $dato['sud_req_diseno'];
	$lic = $dato['sui_sw_requiere_licencia'];
	$cons = $dato['sco_constructor'];
	$civ = $dato['sci_int_civil'];
	$ele = $dato['sue_int_electromecanico'];
	$seg = $dato['sse_instalador'];
    $ala = $dato['sia_instalador'];
	
	$fitinm = $dato['lib_fec_inmobiliaria']; 
	$fftinm = $dato['lib_fecteo_inmobiliaria'];
	$fitdis = $dato['lib_fec_diseno'];
	$fftdis = $dato['lib_fecteo_diseno'];
	$fitlic = $dato['lib_fec_licencia'];
	$fftlic = $dato['lib_fecteo_licencia'];
	$fitint = $dato['lib_fec_interventoria'];
	$fftint = $dato['lib_fecteo_interventoria'];
	$fitcons = $dato['lib_fec_constructor'];
	$fftcons = $dato['lib_fecteo_constructor'];
	$fitpre = $dato['lib_fec_presupuesto'];
	$fftpre = $dato['lib_fecteo_presupuesto'];
	$fitsum = $dato['lib_fec_suministro'];
	$fftsum = $dato['lib_fecteo_suministro'];
	$fitciv = $dato['lib_fec_civil'];
	$fftciv = $dato['lib_fecteo_civil'];
	$fitele = $dato['lib_fec_electromecanico'];
	$fftele = $dato['lib_fecteo_electromecanico'];
	$fitseg = $dato['lib_fec_seguridad'];
	$fftseg = $dato['lib_fecteo_seguridad'];
	
	if($inm == 1){ $cantejecutada = $cantejecutada+1; }
	if($dis == 1){ $cantejecutada = $cantejecutada+1; }
	if($lic == 1){ $cantejecutada = $cantejecutada+1; }
	if($cons > 0){ $cantejecutada = $cantejecutada+1; }
	if($civ > 0){ $cantejecutada = $cantejecutada+1; }
	if($ele > 0){ $cantejecutada = $cantejecutada+1; }
	if($seg > 0){ $cantejecutada = $cantejecutada+1; }
    if($ala > 0){ $cantejecutada = $cantejecutada+1; }
	if($fftsum != "" and $fftsum != "0000-00-00"){ $cantejecutada = $cantejecutada+1; }
	if($fftpre != "" and $fftpre != "0000-00-00"){ $cantejecutada = $cantejecutada+1; }
	if($fftint != "" and $fftint != "0000-00-00"){ $cantejecutada = $cantejecutada+1; }
	$porcentaje = 10*$cantejecutada;
	$sumporejecucion2 = $sumporejecucion2+$porcentaje;
	//echo "Porcentaje: ".$porcentaje." Sumporejecucion: ".$sumporejecucion1."<br>";
}

$con = mysqli_query($conectar,"select s.suc_sw_busqueda_local,sucinm.sui_sw_requiere_licencia,sucdis.sud_req_diseno,succons.sco_constructor,succiv.sci_int_civil,sucele.sue_int_electromecanico,sucseg.sse_instalador,sucala.sia_instalador,s.suc_clave_int,s.suc_estado,s.suc_codigo,s.suc_nombre,lb.lib_fec_inmobiliaria,lb.lib_fecteo_inmobiliaria,lb.lib_fec_diseno,lb.lib_fecteo_diseno,lb.lib_fec_licencia,lb.lib_fecteo_licencia,lb.lib_fec_interventoria,lb.lib_fecteo_interventoria,lb.lib_fec_constructor,lb.lib_fecteo_constructor,lb.lib_fec_presupuesto,lb.lib_fecteo_presupuesto from sucursal s inner join linea_base lb on (lb.suc_clave_int = s.suc_clave_int) left outer join sucursal_inmobiliaria sucinm on (sucinm.suc_clave_int = s.suc_clave_int) left outer join sucursal_diseno sucdis on (sucdis.suc_clave_int = s.suc_clave_int) left outer join sucursal_licencia suclic on (suclic.suc_clave_int = s.suc_clave_int) left outer join sucursal_adjudicacion sucadju on (sucadju.suc_clave_int = s.suc_clave_int) left outer join sucursal_constructor succons on (succons.suc_clave_int = s.suc_clave_int) left outer join sucursal_presupuesto sucpre on (sucpre.suc_clave_int = s.suc_clave_int) left outer join sucursal_suministro sucsum on (sucsum.suc_clave_int = s.suc_clave_int) left outer join sucursal_civil succiv on (succiv.suc_clave_int = s.suc_clave_int) left outer join sucursal_electromecanico sucele on (sucele.suc_clave_int = s.suc_clave_int) left outer join sucursal_seguridad sucseg on (sucseg.suc_clave_int = s.suc_clave_int) left outer join sucursal_alarma sucala on (sucala.suc_clave_int = s.suc_clave_int) where suc_estado = 3 and suc_sw_eliminado = 0");
$num3 = mysqli_num_rows($con);
$sumporejecucion3 = 0;

for($i = 0; $i < $num3; $i++)
{
	$cantejecutada = 0;
	$dato = mysqli_fetch_array($con);
	$inm = $dato['suc_sw_busqueda_local'];
	$dis = $dato['sud_req_diseno'];
	$lic = $dato['sui_sw_requiere_licencia'];
	$cons = $dato['sco_constructor'];
	$civ = $dato['sci_int_civil'];
	$ele = $dato['sue_int_electromecanico'];
	$seg = $dato['sse_instalador'];
    $ala = $dato['sia_instalador'];
	
	$fitinm = $dato['lib_fec_inmobiliaria']; 
	$fftinm = $dato['lib_fecteo_inmobiliaria'];
	$fitdis = $dato['lib_fec_diseno'];
	$fftdis = $dato['lib_fecteo_diseno'];
	$fitlic = $dato['lib_fec_licencia'];
	$fftlic = $dato['lib_fecteo_licencia'];
	$fitint = $dato['lib_fec_interventoria'];
	$fftint = $dato['lib_fecteo_interventoria'];
	$fitcons = $dato['lib_fec_constructor'];
	$fftcons = $dato['lib_fecteo_constructor'];
	$fitpre = $dato['lib_fec_presupuesto'];
	$fftpre = $dato['lib_fecteo_presupuesto'];
	$fitsum = $dato['lib_fec_suministro'];
	$fftsum = $dato['lib_fecteo_suministro'];
	$fitciv = $dato['lib_fec_civil'];
	$fftciv = $dato['lib_fecteo_civil'];
	$fitele = $dato['lib_fec_electromecanico'];
	$fftele = $dato['lib_fecteo_electromecanico'];
	$fitseg = $dato['lib_fec_seguridad'];
	$fftseg = $dato['lib_fecteo_seguridad'];
	
	if($inm == 1){ $cantejecutada = $cantejecutada+1; }
	if($dis == 1){ $cantejecutada = $cantejecutada+1; }
	if($lic == 1){ $cantejecutada = $cantejecutada+1; }
	if($cons > 0){ $cantejecutada = $cantejecutada+1; }
	if($civ > 0){ $cantejecutada = $cantejecutada+1; }
	if($ele > 0){ $cantejecutada = $cantejecutada+1; }
	if($seg > 0){ $cantejecutada = $cantejecutada+1; }
    if($ala > 0){ $cantejecutada = $cantejecutada+1; }
	if($fftsum != "" and $fftsum != "0000-00-00"){ $cantejecutada = $cantejecutada+1; }
	if($fftpre != "" and $fftpre != "0000-00-00"){ $cantejecutada = $cantejecutada+1; }
	if($fftint != "" and $fftint != "0000-00-00"){ $cantejecutada = $cantejecutada+1; }
	$porcentaje = 10*$cantejecutada;
	$sumporejecucion3 = $sumporejecucion3+$porcentaje;
	//echo "Porcentaje: ".$porcentaje." Sumporejecucion: ".$sumporejecucion1."<br>";
}

$totalporcentaje = ($sumporejecucion*100)/($num*100);
echo "<a style='font-weight:bold'>TOTAL SUCURSALES ACTIVAS: ".$num." / PORCENTAJE DE EJECUCION: %".round($totalporcentaje,2)."<br><progress max='100' value='".$totalporcentaje."'></progress><br>";

$totalporcentaje1 = ($sumporejecucion1*100)/($num1*100);
echo "TOTAL SUCURSALES PROGRAMADAS: ".$num1." / PORCENTAJE DE EJECUCION: %".round($totalporcentaje1,2)."<br><progress max='100' value='".$totalporcentaje1."'></progress><br>";

$totalporcentaje2 = ($sumporejecucion2*100)/($num2*100);
echo "TOTAL SUCURSALES SUSPENDIDAS: ".$num2." / PORCENTAJE DE EJECUCION: %".round($totalporcentaje2,2)."<br><progress max='100' value='".$totalporcentaje2."'></progress><br>";

$totalporcentaje3 = ($sumporejecucion3*100)/($num3*100);
echo "TOTAL SUCURSALES ENTREGADAS: ".$num3." / PORCENTAJE DE EJECUCION: %".round($totalporcentaje3,2)."<br><progress max='100' value='".$totalporcentaje3."'></progress><br>";


?>