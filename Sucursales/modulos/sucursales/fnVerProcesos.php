<?php
error_reporting(0);
include('../../data/Conexion.php');
session_start();
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_SESSION['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");
$clasuc = $_POST['clasuc'];
$est = $_POST['est'];
$opc = $_POST['opc'];
$reqdis = $_POST['reqdis'];


//CALCULO DE PORCENTAJE DE EJECUCION
$con = mysqli_query($conectar,"select s.suc_clave_int,s.suc_nombre,s.suc_sw_busqueda_local,sucinm.sui_sw_requiere_licencia,sucdis.sud_req_diseno,succons.sco_constructor,succiv.sci_int_civil,sucele.sue_int_electromecanico,sucseg.sse_instalador,s.suc_clave_int,s.suc_estado,s.suc_codigo,s.suc_nombre,lb.lib_fec_inmobiliaria,lb.lib_fecteo_inmobiliaria,lb.lib_fec_diseno,lb.lib_fecteo_diseno,lb.lib_fec_licencia,lb.lib_fecteo_licencia,lb.lib_fec_interventoria,lb.lib_fecteo_interventoria,lb.lib_fec_constructor,lb.lib_fecteo_constructor,lb.lib_fec_presupuesto,lb.lib_fecteo_presupuesto,sucala.sia_instalador from sucursal s inner join linea_base lb on (lb.suc_clave_int = s.suc_clave_int) left outer join sucursal_inmobiliaria sucinm on (sucinm.suc_clave_int = s.suc_clave_int) left outer join sucursal_diseno sucdis on (sucdis.suc_clave_int = s.suc_clave_int) left outer join sucursal_licencia suclic on (suclic.suc_clave_int = s.suc_clave_int) left outer join sucursal_adjudicacion sucadju on (sucadju.suc_clave_int = s.suc_clave_int) left outer join sucursal_constructor succons on (succons.suc_clave_int = s.suc_clave_int) left outer join sucursal_presupuesto sucpre on (sucpre.suc_clave_int = s.suc_clave_int) left outer join sucursal_suministro sucsum on (sucsum.suc_clave_int = s.suc_clave_int) left outer join sucursal_civil succiv on (succiv.suc_clave_int = s.suc_clave_int) left outer join sucursal_electromecanico sucele on (sucele.suc_clave_int = s.suc_clave_int) left outer join sucursal_seguridad sucseg on (sucseg.suc_clave_int = s.suc_clave_int) left outer join sucursal_alarma sucala on (sucala.suc_clave_int = s.suc_clave_int) where suc_estado = '".$est."' and suc_sw_eliminado = 0");
$num = mysqli_num_rows($con);
$sumporindeje = 0;
$sumporejecucion = 0;
$procesosejecutados = "";
$procesosporejecutar = "";
for($i = 0; $i < $num; $i++)
{
	$cantejecutada = 0;
	$dato = mysqli_fetch_array($con);
	$clave = $dato['suc_clave_int'];
	$inm = $dato['suc_sw_busqueda_local'];
	$dis = $dato['sud_req_diseno'];
	$lic = $dato['sui_sw_requiere_licencia'];
	$cons = $dato['sco_constructor'];
	$civ = $dato['sci_int_civil'];
	$ele = $dato['sue_int_electromecanico'];
	$seg = $dato['sse_instalador'];
	$ala = $dato['sia_instalador'];
	
	$fitinm = $dato['lib_fec_inmobiliaria']; 
	$fftinm = $dato['lib_fecteo_inmobiliaria'];
	$fitdis = $dato['lib_fec_diseno'];
	$fftdis = $dato['lib_fecteo_diseno'];
	$fitlic = $dato['lib_fec_licencia'];
	$fftlic = $dato['lib_fecteo_licencia'];
	$fitint = $dato['lib_fec_interventoria'];
	$fftint = $dato['lib_fecteo_interventoria'];
	$fitcons = $dato['lib_fec_constructor'];
	$fftcons = $dato['lib_fecteo_constructor'];
	$fitpre = $dato['lib_fec_presupuesto'];
	$fftpre = $dato['lib_fecteo_presupuesto'];
	$fitsum = $dato['lib_fec_suministro'];
	$fftsum = $dato['lib_fecteo_suministro'];
	$fitciv = $dato['lib_fec_civil'];
	$fftciv = $dato['lib_fecteo_civil'];
	$fitele = $dato['lib_fec_electromecanico'];
	$fftele = $dato['lib_fecteo_electromecanico'];
	$fitseg = $dato['lib_fec_seguridad'];
	$fftseg = $dato['lib_fecteo_seguridad'];
	
	if($inm == 1){ $cantejecutada = $cantejecutada+1; if($clave == $clasuc){ $procesosejecutados.= "Inmobiliaria, "; } }else{ if($clave == $clasuc){ $procesosporejecutar .= "Inmobiliaria, "; } }

	if($dis == 1){ $cantejecutada = $cantejecutada+1; if($clave == $clasuc){ $procesosejecutados .= "Diseño, "; }else{ if($clave==$clasuc  and $reqdis != 2){$procesosporejecutar.="Diseño, ";} } }else{ if($clave == $clasuc ){ $procesosporejecutar .= "Diseño, "; } }
	if($lic == 1){ $cantejecutada = $cantejecutada+1; if($clave == $clasuc){ $procesosejecutados .= "Licencia, "; } }else{ if($clave == $clasuc){ $procesosporejecutar .= "Licencia, "; } }
	if($cons > 0){ $cantejecutada = $cantejecutada+1; if($clave == $clasuc){ $procesosejecutados .= "Constructor, "; } }else{ if($clave == $clasuc){ $procesosporejecutar .= "Constructor, "; } }
	if($civ > 0){ $cantejecutada = $cantejecutada+1; if($clave == $clasuc){ $procesosejecutados .= "Int. Civil, "; } }else{ if($clave == $clasuc){ $procesosporejecutar .= "Int. Civil, "; } }
	if($ele > 0){ $cantejecutada = $cantejecutada+1; if($clave == $clasuc){ $procesosejecutados .= "Int. Electromecanico, "; } }else{ if($clave == $clasuc){ $procesosporejecutar .= "Int. Electromecanico, "; } }
	if($seg > 0){ $cantejecutada = $cantejecutada+1; if($clave == $clasuc){ $procesosejecutados .= "Int. Seguridad, "; } }else{ if($clave == $clasuc){ $procesosporejecutar .= "Int. Seguridad, "; } }

    if($ala > 0){ $cantejecutada = $cantejecutada+1; if($clave == $clasuc){ $procesosejecutados .= "Int. Alarma, "; } }else{ if($clave == $clasuc){ $procesosporejecutar .= "Int. alarma, "; } }

	if($fftsum != "" and $fftsum != "0000-00-00"){ $cantejecutada = $cantejecutada+1; if($clave == $clasuc){ $procesosejecutados .= "Suministros, "; } }else{ if($clave == $clasuc){ $procesosporejecutar .= "Suministros, "; } }
	if($fftpre != "" and $fftpre != "0000-00-00"){ $cantejecutada = $cantejecutada+1; if($clave == $clasuc){ $procesosejecutados .= "Presupuesto, "; } }else{ if($clave == $clasuc){ $procesosporejecutar .= "Presupuesto, "; } }
	if($fftint != "" and $fftint != "0000-00-00"){ $cantejecutada = $cantejecutada+1; if($clave == $clasuc){ $procesosejecutados .= "Interventoria "; } }else{ if($clave == $clasuc){ $procesosporejecutar .= "Interventoria "; } }
	$porcentaje = 10*$cantejecutada;
	$sumporejecucion = $sumporejecucion+$porcentaje;
	if($clave == $clasuc){ $sumporindeje = $sumporindeje+$porcentaje; $nomsuc = $dato['suc_nombre']; }
}

$totalporcentaje = ($sumporejecucion*100)/($num*100);
$totalporcentajeindividual = ($sumporindeje*100)/100;

if($est == 1){ $nomest = "ACTIVAS"; }elseif($est == 5){ $nomest = "PROGRAMADAS"; }elseif($est == 2){ $nomest = "SUSPENDIDAS"; }elseif($est == 3){ $nomest = "ENTREGADAS"; }elseif($est == 4){ $nomest = "CANCELADAS"; }

if($opc == "VERPROCESOS")
{
	echo "<a onClick='quitarDiv(".$clasuc.")' style='font-weight:bold;font-size:12px;color:red;cursor:pointer'>CERRAR</a><br><br><a style='font-weight:bold;font-size:12px;'>".$nomsuc."</a><br><a style='font-weight:bold;font-size:12px'>TOTAL SUCURSALES ".$nomest.": ".$num."<br>PORCENTAJE INDIVIDUAL DE EJECUCION: %".round($totalporcentajeindividual,2)."<br><progress max='100' value='".$totalporcentajeindividual."'></progress>PORCENTAJE TOTAL DE EJECUCION: %".round($totalporcentaje,2)."<br><progress max='100' value='".$totalporcentaje."'></progress><br>";

	echo "<a style='font-weight:bold;font-size:12px;color:green'>PROCESOS EJECUTADOS: <br>".$procesosejecutados."</a><br><br>";
	echo "<a style='font-weight:bold;font-size:12px;color:red'>PROCESOS POR EJECUTAR: <br>".$procesosporejecutar."</a><br>";
}
else
if($opc == "COMPROBARESTADO" and $_POST['est'] == 3)
{
	$porcentaje = round($totalporcentajeindividual,2);
	$conest = mysqli_query($conectar,"select * from sucursal where suc_clave_int = '".$clasuc."'");
	$datoest = mysqli_fetch_array($conest);
	$estsuc = $datoest['suc_aprobado'];
	
	if($estsuc == 1 and $porcentaje >= 100)
	{
		$res =  1;
	}
	else if($estsuc!=1){
	    $res = 0;
    }
    else if($porcentaje>0)
	{
		$res =  1;
	}
	else
    {
        $res = 1;
    }
	$datos[] = array("res"=>$res,"pore"=>$porcentaje,"procesosporejecutar"=>$procesosporejecutar,"procesosejecutados"=>$procesosejecutados);
	echo json_encode($datos);
}
else
{
	echo 0;
}
?>