<?php
ini_set('max_execution_time', 600);
ini_set('upload_max_filesize', '300M');
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
include('../../data/Conexion.php');
require_once('../../Classes/PHPMailer-master/class.phpmailer.php');
header("Cache-Control: no-store, no-cache, must-revalidate");
session_start();
error_reporting(0);
date_default_timezone_set('America/Bogota');
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_SESSION['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];


$fecha=date("Ymd");
$fechaact=date("Y/m/d H:i:s");
$clasuc = $_POST['clasuc'];
$actor = $_POST['actor'];
$arc = $_POST['archivo'];
$claada = $_POST['claada'];
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
{

    if($clasuc != '' and $clasuc != 0 and $actor != '')
    {
        if(is_array($_FILES))
        {
            $con = mysqli_query($conectar,"select MAX(ada_clave_int) max from adjunto_actor where suc_clave_int = '".$clasuc."'");
            $dato = mysqli_fetch_array($con);
            $num = $dato['max'];

            if($num == 0 || $num == ''){ $num = 1; }else{ $num = $num+1; }

            if($actor == 'FORMATODISENO')
            {
                if(is_uploaded_file($_FILES['adjuntoformatodiseno']['tmp_name']))
                {
                    $sourcePath = $_FILES['adjuntoformatodiseno']['tmp_name'];
                    $archivo = basename($_FILES['adjuntoformatodiseno']['name']);

                    $array_nombre = explode('.',$archivo);
                    $cuenta_arr_nombre = count($array_nombre);
                    $extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
                    /*if($extension == 'jpg')
                    {
                        $extension = 'png';
                    }*/
                    $nombrearchivo = $array_nombre[0];
                    $anio = substr($nombrearchivo,0,4);
                    $mes = substr($nombrearchivo,4,2);
                    $dia = substr($nombrearchivo,6,2);
                    $fech = $anio."-".$mes."-".$dia;

                    $prefijo = $anio.$mes.$dia." FORMATO - DISENO";
                    $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '34' and ada_sw_eliminado = 0");
                    $numv = mysqli_num_rows($veri);
                    if($numv>0){
                        echo "errorv";
                    }
                    else
                    if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                    {
                        echo "error1";
                    }
                    else
                    {
                            $destino = "../../adjuntos/formatodiseno/" . $clasuc . "-" . $prefijo . "" . $num . "." . $extension;

                            if (move_uploaded_file($sourcePath, $destino)) {
                                $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,suc_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clasuc . "','" . $fechaact . "','" . $destino . "','" . $archivo . "',34,'" . $usuario . "','" . $fechaact . "')");
                                $sql1 = mysqli_query($conectar,"update sucursal set suc_fec_fsd = '" . $fechaact . "' where suc_clave_int = '" . $clasuc . "'");


                                $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario u inner join notificar_usuario nu on (nu.usu_clave_int = u.usu_clave_int) where not_clave_int = 37 and usu_email != ''");
                                $num = mysqli_num_rows($conema);
                                for($i = 0; $i < $num; $i++)
                                {
                                    $mail = new PHPMailer();

                                    $datoema = mysqli_fetch_array($conema);
                                    $mail->Body = '';
                                    $ema = '';
                                    $nom = $datoema['usu_nombre'];
                                    $ema = $datoema['usu_email'];

                                    $concaj = mysqli_query($conectar,"select suc_nombre from sucursal where suc_clave_int = '".$clasuc."'");
                                    $datocaj = mysqli_fetch_array($concaj);
                                    $nomsuc = $datocaj['suc_nombre'];

                                    $conact = mysqli_query($conectar,"select usu_nombre from usuario where usu_usuario = '".$usuario."'");
                                    $datoact = mysqli_fetch_array($conact);
                                    $nomact = $datoact['usu_nombre'];

                                    $mail->From = "adminpavas@pavas.com.co";
                                    $mail->FromName = "Soportica";
                                    $mail->AddAddress($ema, "Destino");
                                    $mail->Subject = "Adjunto Reporte Diseño - Sucursal Nro. ".$clasuc;

                                    // Cuerpo del mensaje
                                    $mail->Body .= "Hola ".$nom."!\n\n";
                                    $mail->Body .= "SUCURSALES Le informa que han adjuntado el reporte de diseño.\n";
                                    $mail->Body .= "SUCURSAL: ".$nomsuc."\n";
                                    $mail->Body .= "USUARIO QUE ADJUNTA: ".$nomact."\n";
                                    $mail->Body .= date("d/m/Y H:m:s")."\n\n";
                                    $mail->Body .= "Este mensaje es generado automáticamente por SUCURSALES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Sucursales \n";

                                    if(!$mail->Send())
                                    {
                                        //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                                        //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                                    }
                                    else
                                    {
                                    }
                                }


                                echo "ok";
                            }
                            else
                            {
                                echo "error4";
                            }
                        }
                }
                else{
                    echo "error3";
                }
            }
            else
                if($actor == 'REPORTEPREFACTIBILIDAD')
                {
                    if(is_uploaded_file($_FILES['adjuntoreporteprefactibilidad']['tmp_name']))
                    {
                        $sourcePath = $_FILES['adjuntoreporteprefactibilidad']['tmp_name'];
                        $archivo = basename($_FILES['adjuntoreporteprefactibilidad']['name']);

                        $array_nombre = explode('.',$archivo);
                        $cuenta_arr_nombre = count($array_nombre);
                        $extension = strtolower($array_nombre[--$cuenta_arr_nombre]);

                        /*if($extension == 'jpg')
                        {
                            $extension = 'png';
                        }*/

                        $prefijo = "FORMATO - REPORTE PREFACTIBILIDAD";
                        $nombrearchivo = $array_nombre[0];
                        $anio = substr($nombrearchivo,0,4);
                        $mes = substr($nombrearchivo,4,2);
                        $dia = substr($nombrearchivo,6,2);
                        $fech = $anio."-".$mes."-".$dia;



                        $destino =  "../../adjuntos/formatodiseno/".$clasuc."-".$prefijo."".$num.".".$extension;
                        $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '68' and ada_sw_eliminado = 0");
                        $numv = mysqli_num_rows($veri);
                        if($numv>0){
                            echo "errorv";
                        }
                        else
                        if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                        {
                            echo "error1";
                        }
                        else
                        {
                                if (move_uploaded_file($sourcePath, $destino)) {
                                    $sql = mysqli_query($conectar, "insert into adjunto_actor(ada_clave_int,suc_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clasuc . "','" . $fechaact . "','" . $destino . "','" . $archivo . "',68,'" . $usuario . "','" . $fechaact . "')");

                                    $conema = mysqli_query($conectar, "select usu_nombre,usu_email from usuario u inner join notificar_usuario nu on (nu.usu_clave_int = u.usu_clave_int) where not_clave_int = 1 and usu_email != ''");
                                    $num = mysqli_num_rows($conema);
                                    for ($i = 0; $i < $num; $i++) {
                                        $mail = new PHPMailer();

                                        $datoema = mysqli_fetch_array($conema);
                                        $mail->Body = '';
                                        $ema = '';
                                        $nom = $datoema['usu_nombre'];
                                        $ema = $datoema['usu_email'];

                                        $concaj = mysqli_query($conectar, "select suc_nombre from sucursal where suc_clave_int = '" . $clasuc . "'");
                                        $datocaj = mysqli_fetch_array($concaj);
                                        $nomsuc = $datocaj['suc_nombre'];

                                        $conact = mysqli_query($conectar, "select usu_nombre from usuario where usu_usuario = '" . $usuario . "'");
                                        $datoact = mysqli_fetch_array($conact);
                                        $nomact = $datoact['usu_nombre'];

                                        $mail->From = "adminpavas@pavas.com.co";
                                        $mail->FromName = "Soportica";
                                        $mail->AddAddress($ema, "Destino");
                                        $mail->Subject = "Adjunto Reporte Prefactibilidad - Sucursal Nro. " . $clasuc;

                                        // Cuerpo del mensaje
                                        $mail->Body .= "Hola " . $nom . "!\n\n";
                                        $mail->Body .= "SUCURSALES Le informa que han adjuntado el reporte de prefactibilidad de inmobiliaria.\n";
                                        $mail->Body .= "SUCURSAL: " . $nomsuc . "\n";
                                        $mail->Body .= "USUARIO QUE ADJUNTA: " . $nomact . "\n";
                                        $mail->Body .= date("d/m/Y H:m:s") . "\n\n";
                                        $mail->Body .= "Este mensaje es generado automáticamente por SUCURSALES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Sucursales \n";

                                        if (!$mail->Send()) {
                                            //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                                            //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                                        } else {
                                        }
                                    }
                                    echo "ok";
                                }
                                else
                                {
                                    echo "error4";
                                }
                            }
                    }
                    else
                    {
                        echo "error3";
                    }
                }
                else
                    if($actor == 'DOCUMENTOS')
                    {
                        if(is_uploaded_file($_FILES['adjuntodocumentos']['tmp_name']))
                        {
                            $sourcePath = $_FILES['adjuntodocumentos']['tmp_name'];
                            $archivo = basename($_FILES['adjuntodocumentos']['name']);

                            $array_nombre = explode('.',$archivo);
                            $cuenta_arr_nombre = count($array_nombre);
                            $extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
                            /*if($extension == 'jpg')
                            {
                                $extension = 'png';
                            }*/
                            $nombrearchivo = $array_nombre[0];
                            $anio = substr($nombrearchivo,0,4);
                            $mes = substr($nombrearchivo,4,2);
                            $dia = substr($nombrearchivo,6,2);
                            $fech = $anio."-".$mes."-".$dia;

                            $prefijo = $nombrearchivo;// $anio.$mes.$dia." FORMATO - DISENO";
                            $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '79' and ada_sw_eliminado = 0");
                            $numv = mysqli_num_rows($veri);
                            if($numv>0){
                                echo "errorv";
                            }
                            else
                            if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                            {
                                echo "error1";
                            }
                            else
                            {
                                $destino = "../../adjuntos/sucdocumentos/" . $clasuc . "-" . $prefijo . "" . $num . "." . $extension;

                                if (move_uploaded_file($sourcePath, $destino)) {
                                    $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,suc_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clasuc . "','" . $fechaact . "','" . $destino . "','" . $archivo . "',79,'" . $usuario . "','" . $fechaact . "')");
                                    $sql1 = mysqli_query($conectar,"update sucursal set suc_fec_fsd = '" . $fechaact . "' where suc_clave_int = '" . $clasuc . "'");


                                    $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario u inner join notificar_usuario nu on (nu.usu_clave_int = u.usu_clave_int) where not_clave_int = 65 and usu_email != ''");
                                    $num = mysqli_num_rows($conema);
                                    for($i = 0; $i < $num; $i++)
                                    {
                                        $mail = new PHPMailer();

                                        $datoema = mysqli_fetch_array($conema);
                                        $mail->Body = '';
                                        $ema = '';
                                        $nom = $datoema['usu_nombre'];
                                        $ema = $datoema['usu_email'];

                                        $concaj = mysqli_query($conectar,"select suc_nombre from sucursal where suc_clave_int = '".$clasuc."'");
                                        $datocaj = mysqli_fetch_array($concaj);
                                        $nomsuc = $datocaj['suc_nombre'];

                                        $conact = mysqli_query($conectar,"select usu_nombre from usuario where usu_usuario = '".$usuario."'");
                                        $datoact = mysqli_fetch_array($conact);
                                        $nomact = $datoact['usu_nombre'];

                                        $mail->From = "adminpavas@pavas.com.co";
                                        $mail->FromName = "Soportica";
                                        $mail->AddAddress($ema, "Destino");
                                        $mail->Subject = "Adjunto Documento - Sucursal Nro. ".$clasuc;

                                        // Cuerpo del mensaje
                                        $mail->Body .= "Hola ".$nom."!\n\n";
                                        $mail->Body .= "SUCURSALES Le informa que han adjuntado un nuevo documento.\n";
                                        $mail->Body .= "SUCURSAL: ".$nomsuc."\n";
                                        $mail->Body .= "USUARIO QUE ADJUNTA: ".$nomact."\n";
                                        $mail->Body .= date("d/m/Y H:m:s")."\n\n";
                                        $mail->Body .= "Este mensaje es generado automáticamente por SUCURSALES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Sucursales \n";

                                        if(!$mail->Send())
                                        {
                                            //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                                            //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                                        }
                                        else
                                        {
                                        }
                                    }


                                echo "ok";
                                }
                                else
                                {
                                    echo "error4";
                                }
                            }
                        }
                        else{
                            echo "error3";
                        }
                    }
                    else
                    if($actor == 'ACTAVISITA')
                    {
                        if(is_uploaded_file($_FILES['adjuntoactavisita']['tmp_name']))
                        {
                                $sourcePath = $_FILES['adjuntoactavisita']['tmp_name'];
                                $archivo = basename($_FILES['adjuntoactavisita']['name']);

                                $array_nombre = explode('.',$archivo);
                                $cuenta_arr_nombre = count($array_nombre);
                                $extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
                                /*if($extension == 'jpg')
                                {
                                    $extension = 'png';
                                }*/
                                $nombrearchivo = $array_nombre[0];
                                $anio = substr($nombrearchivo,0,4);
                                $mes = substr($nombrearchivo,4,2);
                                $dia = substr($nombrearchivo,6,2);
                                $fech = $anio."-".$mes."-".$dia;

                                $prefijo = $anio.$mes.$dia." FORMATO - ACTA DE VISITA ACLARATORIA";
                                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '69' and ada_sw_eliminado = 0");
                                $numv = mysqli_num_rows($veri);
                                if($numv>0){
                                    echo "errorv";
                                }
                                else
                                if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                                {
                                    echo "error1";
                                }
                                else
                                {
                                        $destino = "../../adjuntos/formatodiseno/" . $clasuc . "-" . $prefijo . "" . $num . "." . $extension;

                                        if (move_uploaded_file($sourcePath, $destino)) {
                                            $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,suc_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clasuc . "','" . $fechaact . "','" . $destino . "','" . $archivo . "',69,'" . $usuario . "','" . $fechaact . "')");
                                            $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario u inner join notificar_usuario nu on (nu.usu_clave_int = u.usu_clave_int) where not_clave_int = 38 and usu_email != ''");
                                            $num = mysqli_num_rows($conema);
                                            for($i = 0; $i < $num; $i++)
                                            {
                                                $mail = new PHPMailer();

                                                $datoema = mysqli_fetch_array($conema);
                                                $mail->Body = '';
                                                $ema = '';
                                                $nom = $datoema['usu_nombre'];
                                                $ema = $datoema['usu_email'];

                                                $concaj = mysqli_query($conectar,"select suc_nombre from sucursal where suc_clave_int = '".$clasuc."'");
                                                $datocaj = mysqli_fetch_array($concaj);
                                                $nomsuc = $datocaj['suc_nombre'];

                                                $conact = mysqli_query($conectar,"select usu_nombre from usuario where usu_usuario = '".$usuario."'");
                                                $datoact = mysqli_fetch_array($conact);
                                                $nomact = $datoact['usu_nombre'];

                                                $mail->From = "adminpavas@pavas.com.co";
                                                $mail->FromName = "Soportica";
                                                $mail->AddAddress($ema, "Destino");
                                                $mail->Subject = "Adjunto Acta Visita Aclaratoria - Sucursal Nro. ".$clasuc;

                                                // Cuerpo del mensaje
                                                $mail->Body .= "Hola ".$nom."!\n\n";
                                                $mail->Body .= "SUCURSALES Le informa que han adjuntado el reporte de acta visita aclaratoria.\n";
                                                $mail->Body .= "SUCURSAL: ".$nomsuc."\n";
                                                $mail->Body .= "USUARIO QUE ADJUNTA: ".$nomact."\n";
                                                $mail->Body .= date("d/m/Y H:m:s")."\n\n";
                                                $mail->Body .= "Este mensaje es generado automáticamente por SUCURSALES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Sucursales \n";

                                                if(!$mail->Send())
                                                {
                                                    //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                                                    //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                                                }
                                                else
                                                {
                                                }
                                            }

                                            echo "ok";
                                        }
                                        else
                                        {
                                            echo "error4";
                                        }
                                    }
                            }
                            else
                            {
                                echo "error3";
                            }
                        }
                        else
                            if($actor == 'ACTUALIZAARCHIVO')
                            {
                                if(is_uploaded_file($_FILES['archivoadjunto']['tmp_name']))
                                {
                                    $sourcePath = $_FILES['archivoadjunto']['tmp_name'];
                                    $archivo = basename($_FILES['archivoadjunto']['name']);

                                    $array_nombre = explode('.',$archivo);
                                    $cuenta_arr_nombre = count($array_nombre);
                                    $extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
                                    $con = mysqli_query($conectar,"select * from adjunto_actor where ada_clave_int = '".$claada."'");
                                    $dato = mysqli_fetch_array($con);
                                    $adj = $dato['ada_adjunto'];
                                    $nombrearchivo = $array_nombre[0];
                                    $anio = substr($nombrearchivo,0,4);
                                    $mes = substr($nombrearchivo,4,2);
                                    $dia = substr($nombrearchivo,6,2);
                                    $fech = $anio."-".$mes."-".$dia;

                                    if($arc == 34)
                                    {
                                        $prefijo =  $anio.$mes.$dia." FORMATO - DISENO";
                                        $error =  "error1";
                                    }
                                    else if($arc== 69)
                                    {
                                        $prefijo = $anio.$mes.$dia." FORMATO - ACTA DE VISITA ACLARATORIA";
                                        $error =  "error2";
                                    }
                                    else if($arc==79)
                                    {
                                        $prefijo = $nombrearchivo;
                                    }
                                    $destino =  "../../adjuntos/formatodiseno/".$clasuc."-".$prefijo."".$num.".".$extension;


                                    $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '".$arc."' and ada_sw_eliminado = 0 and ada_clave_int !='".$claada."'");
                                    $numv = mysqli_num_rows($veri);
                                    if($numv>0){
                                        echo "errorv";
                                    }
                                    else
                                    if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                                    {
                                        echo "error1";
                                    }
                                    else
                                    {

                                        if (move_uploaded_file($sourcePath, $destino)) {
                                            $sql = mysqli_query($conectar,"update adjunto_actor set ada_usu_actualiz = '" . $usuario . "',ada_fec_actualiz = '" . $fechaact . "', ada_adjunto = '" . $destino . "', ada_nombre_adjunto = '" . $archivo . "' where ada_clave_int = '" . $claada . "'");
                                            echo "ok";
                                        }
                                        else
                                        {

                                            echo "error4";
                                        }
                                    }
                                }
                                else
                                {
                                    echo "error3";
                                }
                            }
        }
    }
    sleep(3);
}
else
{
    throw new Exception("Error Processing Request", 1);
}