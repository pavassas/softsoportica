function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function MODULO(v)
{	
	if(v == 'USUARIOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='img/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulousuarios=si",true);
		ajax.send(null);
	}
}
function EDITAR(v,m)
{	
	if(m == 'ACTIVIDAD')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('editarposicion').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#editarposicion").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?editaract=si&actedi="+v,true);
		ajax.send(null);
	}
	else
	if(m == 'NIVEL1')
	{
		var nom = edinivel1.value;
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('editarnivel1').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#editarnivel1").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?editaractn1=si&actedi="+v+"&nom="+nom,true);
		ajax.send(null);
		setTimeout("CONSULTAMODULO('LISTAN1');",1000);
		setTimeout("CONSULTAMODULO('LISTAN2');",1000);
		setTimeout("CONSULTAMODULO('LISTAN3');",1000);
		setTimeout("CONSULTAMODULO('NIVELN1');",1000);
		setTimeout("CONSULTAMODULO('NIVELN11');",1000);
		setTimeout("CONSULTAMODULO('TODOS');",1000);
	}
	else
	if(m == 'NIVEL2')
	{
		var pad = edinivel21.value;
		var nom = edinivel2.value;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('editarnivel2').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#editarnivel2").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?editaractn2=si&actedi="+v+"&pad="+pad+"&nom="+nom,true);
		ajax.send(null);
		setTimeout("CONSULTAMODULO('LISTAN2');",1000);
		setTimeout("CONSULTAMODULO('LISTAN3');",1000);
		setTimeout("CONSULTAMODULO('NIVELN2');",1000);
		setTimeout("CONSULTAMODULO('TODOS');",1000);
	}
	else
	if(m == 'NIVEL3')
	{
		var pad = edinivel31.value;
		var hij = edinivel32.value;
		var nom = edinivel3.value;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('editarnivel3').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#editarnivel3").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?editaractn3=si&actedi="+v+"&pad="+pad+"&hij="+hij+"&nom="+nom,true);
		ajax.send(null);
		setTimeout("CONSULTAMODULO('LISTAN3');",1000);
		setTimeout("CONSULTAMODULO('TODOS');",1000);
	}
}
function ELIMINAR(v,m)
{	
	if(m == 'NIVEL1')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('editarnivel1').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#editarnivel1").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?eliminaractn1=si&actedi="+v,true);
		ajax.send(null);
		setTimeout("CONSULTAMODULO('LISTAN1');",1000);
		setTimeout("CONSULTAMODULO('NIVELN1');",1000);
		setTimeout("CONSULTAMODULO('NIVELN11');",1000);
	}
	else
	if(m == 'NIVEL2')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('editarnivel2').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#editarnivel2").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?eliminaractn2=si&actedi="+v,true);
		ajax.send(null);
		setTimeout("CONSULTAMODULO('LISTAN2');",1000);
		setTimeout("CONSULTAMODULO('NIVELN2');",1000);
	}
	else
	if(m == 'NIVEL3')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('editarnivel3').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#editarnivel3").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?eliminaractn3=si&actedi="+v,true);
		ajax.send(null);
		setTimeout("CONSULTAMODULO('LISTAN3');",1000);
	}
	setTimeout("CONSULTAMODULO('TODOS');",1000);
}
function GUARDAR(v,id)
{
	if(v == 'POSICION')
	{
		var nom = form1.nombre1.value;
		var la = nom.length;
		var act = form1.activo1.checked;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('datos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#datos").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?guardaract=si&nom="+nom+"&act="+act+"&la="+la+"&a="+id,true);
		ajax.send(null);
		if(nom != '' && la >= 6)
		{
			setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
		}
	}
}
function GUARDARNIVEL(v)
{
	if(v == 'NIVEL1')
	{
		var niv1 = form1.nivel1.value;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('editarnivel1').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#editarnivel1").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?guardarnivel1=si&niv1="+niv1,true);
		ajax.send(null);
		setTimeout("CONSULTAMODULO('LISTAN1');",1000);
		setTimeout("CONSULTAMODULO('NIVELN1');",1000);
		setTimeout("CONSULTAMODULO('NIVELN11');",1000);
	}
	else
	if(v == 'NIVEL2')
	{
		var niv1 = form1.nivel21.value;
		var niv2 = form1.nivel2.value;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('editarnivel2').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#editarnivel2").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?guardarnivel2=si&niv1="+niv1+"&niv2="+niv2,true);
		ajax.send(null);
		setTimeout("CONSULTAMODULO('LISTAN2');",1000);
		setTimeout("CONSULTAMODULO('NIVELN2');",1000);
	}
	else
	if(v == 'NIVEL3')
	{
		var niv1 = form1.nivel31.value;
		var niv2 = form1.nivel32.value;
		var niv3 = form1.nivel3.value;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('editarnivel3').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#editarnivel3").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?guardarnivel3=si&niv1="+niv1+"&niv2="+niv2+"&niv3="+niv3,true);
		ajax.send(null);
		setTimeout("CONSULTAMODULO('LISTAN3');",1000);
	}
	setTimeout("CONSULTAMODULO('TODOS');",1000);
}
function NUEVO(v)
{
	if(v == 'ACTIVIDAD')
	{
		var nom = form1.nombre.value;
		var la = nom.length;
		var act = form1.activo.checked;
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('datos1').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#datos1").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?nuevaposicion=si&nom="+nom+"&act="+act+"&la="+la,true);
		ajax.send(null);
		if(nom != '' && la >= 6)
		{
			form1.nombre.value = '';
			setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
		}
	}
}
function CONSULTAMODULO(v)
{
	if(v == 'TODOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('posiciones').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#posiciones").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?todos=si",true);
		ajax.send(null);
	}
	else
	if(v == 'LISTAN1')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('listan1').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#listan1").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?listan1=si",true);
		ajax.send(null);
	}
	else
	if(v == 'LISTAN2')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('listan2').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#listan2").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?listan2=si",true);
		ajax.send(null);
	}
	else
	if(v == 'LISTAN3')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('listan3').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#listan3").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?listan3=si",true);
		ajax.send(null);
	}
	else
	if(v == 'NIVELN1')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('n1').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#n1").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?niveln1=si",true);
		ajax.send(null);
	}
	else
	if(v == 'NIVELN11')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('n11').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#n11").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?niveln11=si",true);
		ajax.send(null);
	}
	else
	if(v == 'NIVELN2')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('n2').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#n2").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?niveln2=si",true);
		ajax.send(null);
	}
}
function BUSCAR(m)
{	
	if(m == 'GEOGRAFIA')
	{
		var pad = form1.nombrepad.value;
		var hij = form1.nombrehij.value;
		var nie = form1.nombrenie.value;
		var act = form1.buscaractivos.value;
				
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('posiciones').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#posiciones").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?buscarpos=si&pad="+pad+"&hij="+hij+"&nie="+nie+"&act="+act,true);
		ajax.send(null);
	}
}
function EDITARNIVEL(v,c)
{
	if(v == 'NIVEL1')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('editarnivel1').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#editarnivel1").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?editarnivel1=si&niv1="+c,true);
		ajax.send(null);
	}
	else
	if(v == 'NIVEL2')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('editarnivel2').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#editarnivel2").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?editarnivel2=si&niv2="+c,true);
		ajax.send(null);
	}
	else
	if(v == 'NIVEL3')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('editarnivel3').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#editarnivel3").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?editarnivel3=si&niv3="+c,true);
		ajax.send(null);
	}
}
function MOSTRARDEPARTAMENTOS(v)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('n2').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#n2").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrardepartamentos=si&reg="+v,true);
	ajax.send(null);
}
function DEPARTAMENTOSCIUDADES(c)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
			document.getElementById('depciu'+c).innerHTML=ajax.responseText;
	    }
	}
	//jQuery("#agregados").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?departamentosciudades=si&clan1="+c,true);
	ajax.send(null);
	setTimeout("OCULTARSCROLL();",500);
}