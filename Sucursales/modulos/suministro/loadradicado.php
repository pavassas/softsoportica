<?php     
error_reporting(E_ALL);

/*
 * examples/mysql/loaddata.php
 * 
 * This file is part of EditableGrid.
 * http://editablegrid.net
 *
 * Copyright (c) 2011 Webismymind SPRL
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://editablegrid.net/license
 */
                              


/**
 * This script loads data from the database and returns it to the js
 *
 */
       
require_once('config.php');      
require_once('EditableGrid.php');            

/**
 * fetch_pairs is a simple method that transforms a mysqli_result object in an array.
 * It will be used to generate possible values for some columns.
*/
function fetch_pairs($mysqli,$query){
	if (!($res = $mysqli->query($query)))return FALSE;
	$rows = array();
	while ($row = $res->fetch_assoc()) {
		$first = true;
		$key = $value = null;
		foreach ($row as $val) {
			if ($first) { $key = $val; $first = false; }
			else { $value = $val; break; } 
		}
		$rows[$key] = $value;
	}
	return $rows;
}


// Database connection
$mysqli = mysqli_init();
$mysqli->options(MYSQLI_OPT_CONNECT_TIMEOUT, 5);
$mysqli->real_connect($config['db_host'],$config['db_user'],$config['db_password'],$config['db_name']); 
                    
// create a new EditableGrid object
$grid = new EditableGrid();
/* 
*  Add columns. The first argument of addColumn is the name of the field in the databse. 
*  The second argument is the label that will be displayed in the header
*/
$grid->addColumn('delete', 'Eliminar', 'html', NULL, false, 'id'); 
$grid->addColumn('id', 'Consec.', 'integer', NULL, false); 
$grid->addColumn('sus_fec_solicitud', 'FecSolicitud', 'date');
$grid->addColumn('sus_num_solped', 'N° Solped', 'integer');
$grid->addColumn('sus_num_pedido', 'N° Pedido', 'integer');
$grid->addColumn('sus_clave_proyecto', 'Nombre Proyecto', 'string', fetch_pairs($mysqli,"SELECT concat('C-',caj_clave_int), concat(caj_nombre,' - CAJERO') pro FROM cajeros.cajero UNION SELECT concat('S-',suc_clave_int), concat(suc_nombre,' - SUCURSAL') pro FROM sucursales.sucursal"),true); 
$grid->addColumn('sus_cencos', 'Cencos', 'string');
$grid->addColumn('pro_clave_int', 'Proyecto', 'string' , fetch_pairs($mysqli,'SELECT pro_clave_int, pro_nombre FROM proyecto'),true);
$grid->addColumn('sum_clave_int', 'Suministro','string' , fetch_pairs($mysqli,'SELECT sum_clave_int, sum_nombre FROM suministro'),true);
$grid->addColumn('sus_referencia', 'Referencia', 'string');  
$grid->addColumn('sus_cantidad', 'Cantidad', 'integer');
$grid->addColumn('mat_clave_int','Cod.Material','string' , fetch_pairs($mysqli,'SELECT mat_clave_int,mat_codigo FROM materiales'),true);
$grid->addColumn('mat_nombre', 'Descripción Material SAP', 'string',NULL,false); 
$grid->addColumn('sus_mascara', 'Mascara', 'string'); 
$grid->addColumn('prv_clave_int', 'Proveedor', 'string', fetch_pairs($mysqli,'SELECT prv_clave_int, prv_nombre FROM proveedores'),true);
$grid->addColumn('sus_val_unitario', 'Valor Unitario', 'integer');
$grid->addColumn('sus_moneda', 'Moneda', 'string', array("COP" => "COP ", "USD" => "USD"));
$grid->addColumn('sus_fec_entrega', 'Fecha Entrega', 'date');
$grid->addColumn('', 'Entregado en sitio', 'string',NULL,false,'');
$grid->addColumn('sus_recepcionado', 'Recepcionado', 'string',array("SI" => "SI ", "NO" => "NO"));
$grid->addColumn('sus_fec_factura', 'FecFactura', 'date'); 
$grid->addColumn('sus_facturado', 'Facturado', 'string',array("SI" => "SI ", "NO" => "NO"));
$grid->addColumn('sus_notas', 'Notas', 'string'); 
$grid->addColumn('sus_observaciones', 'Observaciones', 'string'); 
/* The column id_country and id_continent will show a list of all available countries and continents. So, we select all rows from the tables */                                           

 

$mydb_tablename = (isset($_GET['db_tablename'])) ? stripslashes($_GET['db_tablename']) : 'suministro_radicado';
                                                                       
$result = $mysqli->query("SELECT *, date_format(sus_fec_solicitud, '%d/%m/%Y') as sus_fec_solicitud, date_format(sus_fec_entrega, '%d/%m/%Y') as sus_fec_entrega, date_format(sus_fec_factura, '%d/%m/%Y') as sus_fec_factura FROM ".$mydb_tablename." left outer join materiales on materiales.mat_clave_int = ".$mydb_tablename.".mat_clave_int WHERE suc_clave_int = '".$_GET['suc']."'" );
$mysqli->close();

// send data to the browser
$grid->renderJSON($result);
?>