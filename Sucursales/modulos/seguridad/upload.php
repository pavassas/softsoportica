<?php

include('../../data/Conexion.php');
require_once('../../Classes/PHPMailer-master/class.phpmailer.php');
header("Cache-Control: no-store, no-cache, must-revalidate");
session_start();
error_reporting(0);

ini_set('max_execution_time', 600);
ini_set('upload_max_filesize', '300M');
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_SESSION['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];

$fecha=date("Ymd");
$fechaact=date("Y/m/d H:i:s");
$clasuc = $_POST['clasuc'];
$actor = $_POST['actor'];
$arc = $_POST['archivo'];
$claada = $_POST['claada'];

if($clasuc != '' and $clasuc != 0 and $actor != '')
{
	if(is_array($_FILES)) 
	{
		mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'13',5,'".$clasuc."','".$usuario."','".$fechaact."')");//Tercer campo tia_clave_int. 5=Adjuntar archivo

		$con = mysqli_query($conectar,"select MAX(ada_clave_int) max from adjunto_actor where suc_clave_int = '".$clasuc."'");
		$dato = mysqli_fetch_array($con);
		$num = $dato['max'];
		
		if($num == 0 || $num == ''){ $num = 1; }else{ $num = $num+1; }
		
		if($actor == 'INFORME')
		{
			if(is_uploaded_file($_FILES['adjuntoinf']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntoinf']['tmp_name'];
				$archivo = basename($_FILES['adjuntoinf']['name']);
						
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
                $nombrearchivo = $array_nombre[0];
                $anio = substr($nombrearchivo,0,4);
                $mes = substr($nombrearchivo,4,2);
                $dia = substr($nombrearchivo,6,2);
                $fech = $anio."-".$mes."-".$dia;
                $prefijo = $nombrearchivo;
				//$prefijo =  $anio.$mes.$dia." SEGURIDAD - INFORMACION";
				$destino =  "../../adjuntos/seguridad/".$clasuc."-".$prefijo."".$num.".".$extension;
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '19' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "errorv";
                }
                else
                if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                {
                    echo "error1";
                }
                else
                {
                    if (move_uploaded_file($sourcePath, $destino))
                    {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,suc_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clasuc . "','" . $fechaact . "','" . $destino . "','" . $archivo . "',19,'" . $usuario . "','" . $fechaact . "')");
                        echo "ok";
                    }
                    else
                    {
                        echo "error4";
                    }
                }
			}
			else
            {
			    echo "error3";
            }
		}
		else
		if($actor == 'PLANORECORDSEGURIDAD')
		{
			if(is_uploaded_file($_FILES['adjuntoplanorecordseguridad']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntoplanorecordseguridad']['tmp_name'];
				$archivo = basename($_FILES['adjuntoplanorecordseguridad']['name']);
						
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
                $nombrearchivo = $array_nombre[0];
                $anio = substr($nombrearchivo,0,4);
                $mes = substr($nombrearchivo,4,2);
                $dia = substr($nombrearchivo,6,2);
                $fech = $anio."-".$mes."-".$dia;
				$prefijo =  $anio.$mes.$dia." SEGURIDAD - PLANOS RECORD SEGURIDAD";
				$destino =  "../../adjuntos/seguridad/".$clasuc."-".$prefijo."".$num.".".$extension;
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '65' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "errorv";
                }
                else
                if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                {
                    echo "error1";
                }
                else
                {
                    if (move_uploaded_file($sourcePath, $destino)) {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,suc_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clasuc . "','" . $fechaact . "','" . $destino . "','" . $archivo . "',65,'" . $usuario . "','" . $fechaact . "')");
                        echo "ok";
                    } else {
                        echo "error4";
                    }
                }
			}
			else{
			    echo "error3";
            }
		}
		else
		if($actor == 'CALIFICACIONPROVEEDORES')
		{
			if(is_uploaded_file($_FILES['adjuntocalificacionproveedores']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntocalificacionproveedores']['tmp_name'];
				$archivo = basename($_FILES['adjuntocalificacionproveedores']['name']);
						
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
                $nombrearchivo = $array_nombre[0];
                $anio = substr($nombrearchivo,0,4);
                $mes = substr($nombrearchivo,4,2);
                $dia = substr($nombrearchivo,6,2);
                $fech = $anio."-".$mes."-".$dia;
				$prefijo =  $anio.$mes.$dia." SEGURIDAD - CALIFICACION PROVEEDORES";
				$destino =  "../../adjuntos/seguridad/".$clasuc."-".$prefijo."".$num.".".$extension;
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '66' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "errorv";
                }
                else
                if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                {
                    echo "error1";
                }
                else
                {
                    if (move_uploaded_file($sourcePath, $destino)) {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,suc_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clasuc . "','" . $fechaact . "','" . $destino . "','" . $archivo . "',66,'" . $usuario . "','" . $fechaact . "')");
                        echo "ok";
                    } else {
                        echo "error4";
                    }
                }
			}
			else{
			    echo "error3";
            }
		}
		else
		if($actor == 'LIQUIDACIONPROVEEDORES')
		{
			if(is_uploaded_file($_FILES['adjuntoliquidacionproveedores']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntoliquidacionproveedores']['tmp_name'];
				$archivo = basename($_FILES['adjuntoliquidacionproveedores']['name']);
						
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
                $nombrearchivo = $array_nombre[0];
                $anio = substr($nombrearchivo,0,4);
                $mes = substr($nombrearchivo,4,2);
                $dia = substr($nombrearchivo,6,2);
                $fech = $anio."-".$mes."-".$dia;
				$prefijo =  $anio.$mes.$dia." SEGURIDAD - LIQUIDACION PROVEEDORES";
				$destino =  "../../adjuntos/seguridad/".$clasuc."-".$prefijo."".$num.".".$extension;
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '67' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "errorv";
                }
                else
                if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                {
                    echo "error1";
                }
                else
                {
                    if (move_uploaded_file($sourcePath, $destino))
                    {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,suc_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clasuc . "','" . $fechaact . "','" . $destino . "','" . $archivo . "',67,'" . $usuario . "','" . $fechaact . "')");
                        echo "ok";
                    }
                    else
                    {
                        echo "error4";
                    }
                }
			}
			else{
			    echo "error3";
            }
		}
		else
		if($actor == 'ACTUALIZAARCHIVO')
		{
			if(is_uploaded_file($_FILES['archivoadjunto']['tmp_name'])) 
			{
				$sourcePath = $_FILES['archivoadjunto']['tmp_name'];
				$archivo = basename($_FILES['archivoadjunto']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				$con = mysqli_query($conectar,"select * from adjunto_actor where ada_clave_int = '".$claada."'");
				$dato = mysqli_fetch_array($con);
				$adj = $dato['ada_adjunto'];
                $nombrearchivo = $array_nombre[0];
                $anio = substr($nombrearchivo,0,4);
                $mes = substr($nombrearchivo,4,2);
                $dia = substr($nombrearchivo,6,2);
                $fech = $anio."-".$mes."-".$dia;
                if($arc==19)
                {
                    $prefijo = $anio . $mes . $dia . " SEGURIDAD - INFORMACION";
                }else
                if($arc==65)
                {
                    $prefijo = $anio . $mes . $dia . " SEGURIDAD - PLANOS RECORD SEGURIDAD";
                }else
                if($arc==66)
                {
                    $prefijo = $anio . $mes . $dia . " SEGURIDAD - CALIFICACION PROVEEDORES";
                }else
                if($arc==67)
                {
                    $prefijo = $anio . $mes . $dia . " SEGURIDAD - LIQUIDACION PROVEEDORES";
                }
				$destino =  "../../adjuntos/seguridad/".$clasuc."-".$prefijo."".$claada.".".$extension;
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '".$arc."' and ada_sw_eliminado = 0 and ada_clave_int!='".$claada."'");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "errorv";
                }
                else
                if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                {
                    echo "error1";
                }
                else
                {
                    if (move_uploaded_file($sourcePath, $destino)) {
                        $sql = mysqli_query($conectar,"update adjunto_actor set ada_usu_actualiz = '" . $usuario . "',ada_fec_actualiz = '" . $fechaact . "', ada_adjunto = '" . $destino . "', ada_nombre_adjunto = '" . $archivo . "' where ada_clave_int = '" . $claada . "'");
                        echo "ok";
                    } else {
                        echo "error4";
                    }
                }
			}
			else{
			    echo "error3";
            }
		}
	}
}
?>