function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function MODULO(v,e)
{	
	if(v == 'CAJEROS')
	{
		window.location.href = "cajeros.php";
	}
	else
	if(v == 'TODOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('cajeros').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#cajeros").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?todos=si&est="+e,true);
		ajax.send(null);
		setTimeout("REFRESCARLISTAS()",500);
	}
	OCULTARSCROLL();
}
function BUSCAR(p)
{	
	var sucursales = "";
	var objCBarray = document.getElementsByName('multiselect_bussucursal');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	sucursales += objCBarray[i].value + ",";
	    }
	}
	
	var anocon = form1.busanocontable.value;
	var cod = form1.buscodigo.value;
	var reg = form1.busregion.value;
	var mun = form1.busmunicipio.value;
	var tipint = form1.bustipointervencion.value;
	var moda = form1.busmodalidad.value;
	var actor = form1.busactor.value;
	var est = form1.busestado.value;
	var fii = form1.fechainicio.value;
	var ffi = form1.fechafin.value;
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('busqueda').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#busqueda").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='50' width='50' />"); //loading gif will be overwrited when ajax have success
	if(p > 0)
	{
		ajax.open("GET","?buscar=si&anocon="+anocon+"&suc="+sucursales+"&cod="+cod+"&reg="+reg+"&mun="+mun+"&tipint="+tipint+"&moda="+moda+"&est="+est+"&actor="+actor+"&fii="+fii+"&ffi="+ffi+"&page="+p,true);
	}
	else
	{
		ajax.open("GET","?buscar=si&anocon="+anocon+"&suc="+sucursales+"&cod="+cod+"&reg="+reg+"&mun="+mun+"&tipint="+tipint+"&moda="+moda+"&est="+est+"&fii="+fii+"&ffi="+ffi+"&actor="+actor,true);
	}
	ajax.send(null);
	OCULTARSCROLL();
}
function EXPORTAR()
{
	var sucursales = "";
	var objCBarray = document.getElementsByName('multiselect_bussucursal');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	sucursales += objCBarray[i].value + ",";
	    }
	}
	
	var anocon = form1.busanocontable.value;
	var cod = form1.buscodigo.value;
	var reg = form1.busregion.value;
	var mun = form1.busmunicipio.value;
	var tipint = form1.bustipointervencion.value;
	var moda = form1.busmodalidad.value;
	var actor = form1.busactor.value;
	var est = form1.busestado.value;
	var fii = form1.fechainicio.value;
	var ffi = form1.fechafin.value;
	
	window.location.href = "informes/informeexcel.php?anocon="+anocon+"&suc="+sucursales+"&cod="+cod+"&reg="+reg+"&mun="+mun+"&tipint="+tipint+"&moda="+moda+"&est="+est+"&fii="+fii+"&ffi="+ffi+"&actor="+actor;
}
function VERCIUDADES(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('ciudades').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#ciudades").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?verciudades=si&reg="+v,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTACIUDADES()",800);
}
function VERMODALIDADES1(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('modalidades1').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#modalidades1").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?vermodalidades1=si&tii="+v,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTAMODALIDADES1()",800);
}