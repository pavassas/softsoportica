<?php
error_reporting(0);
include('../../../data/Conexion.php');
require_once('../../../Classes/PHPExcel.php');
date_default_timezone_set('America/Bogota');
session_start();
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_SESSION['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];
	
// verifica si no se ha loggeado
if(!isset($_SESSION["persona"]))
{
  session_destroy();
  header("LOCATION:index.php");
}else{
}
$suc = $_GET['suc'];
$anocon = $_GET['anocon'];
$cod = $_GET['cod'];
$reg = $_GET['reg'];
$mun = $_GET['mun'];
$tipint = $_GET['tipint'];
$moda = $_GET['moda'];
$actor = $_GET['actor'];
$est = $_GET['est'];
$fii = $_GET['fii'];
$ffi = $_GET['ffi'];

$seleccionados = explode(',',$suc);
$num = count($seleccionados);
$sucursales = array();
for($i = 0; $i < $num; $i++)
{
	if($seleccionados[$i] != '')
	{
		$sucursales[$i]=$seleccionados[$i];
	}
}
$listasucursales=implode(',',$sucursales);

$fecha=date("d/m/Y");
$fechaact=date("Y/m/d H:i:s");
//************ESTILOS******************

$styleA1 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => '000000'),
    'size'  => 12,
    'name'  => 'Calibri'
));
$styleA2 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => 'C83000'),
    'size'  => 10,
    'name'  => 'Arial'
));
$styleA3 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => '000000'),
    'size'  => 10,
    'name'  => 'Arial'
));
$styleA4 = array(
'font'  => array(
    'bold'  => false,
    'color' => array('rgb' => '000000'),
    'size'  => 10,
    'name'  => 'Arial'
));
$styleA3p1 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => '000000'),
    'size'  => 9,
    'name'  => 'Arial'
));
$styleA4p1 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => '000000'),
    'size'  => 9,
    'name'  => 'Arial'
));

$borders = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '000000'),
		)
	),
);

//*************************************

$objPHPExcel = new PHPExcel();
$archivo = 'PAVAS - Informe sucursales eliminadas.xls';

//Propiedades de la hoja de excel
$objPHPExcel->getProperties()
		->setCreator("PAVAS TECNOLOGIA")
		->setLastModifiedBy("PAVAS TECNOLOGIA")
		->setTitle("Informe sucursales eliminadas")
		->setSubject("Informe sucursales eliminadas")
		->setDescription("Documento generado con el software Sucursales")
		->setKeywords("Sucursales")
		->setCategory("Reportes");
		
//Ancho de las Columnas
//Info basica
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
//Info secundaria
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(23);
//Inmobiliaria
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(20);
//Visita
$objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setWidth(20);
//Diseño
$objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AN')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AO')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AP')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AQ')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AR')->setWidth(20);
//Licencia
$objPHPExcel->getActiveSheet()->getColumnDimension('AS')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AT')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AU')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AV')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AW')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AX')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AY')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AZ')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BA')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BB')->setWidth(20);
//INTERVENTORIA
$objPHPExcel->getActiveSheet()->getColumnDimension('BC')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BD')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BE')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BF')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BG')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BH')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BI')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BJ')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BK')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BL')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BM')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BN')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BO')->setWidth(20);
//CONSTRUCTOR
$objPHPExcel->getActiveSheet()->getColumnDimension('BP')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BQ')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BR')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BS')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BT')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BU')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BV')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BW')->setWidth(20);
//SEGURIDAD
$objPHPExcel->getActiveSheet()->getColumnDimension('BX')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BY')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BZ')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('CA')->setWidth(20);
//FACTURAZION
$objPHPExcel->getActiveSheet()->getColumnDimension('CB')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CC')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CD')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CE')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CF')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CG')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CH')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CI')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CJ')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CK')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CL')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CM')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CN')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CO')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CP')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CQ')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CR')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CS')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CT')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CU')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CV')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CW')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CX')->setWidth(30);

//************A1**************
$objPHPExcel->getActiveSheet()->getStyle('A1')-> applyFromArray($styleA1);//
$objPHPExcel->getActiveSheet()->getCell('A1')->setValue("SUCURSALES ELIMINADAS");
$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setWrapText(true); //Crea un enter entre palabras
$objPHPExcel->getActiveSheet()->mergeCells('A1:T1');//Conbinar celdas
$objPHPExcel->getActiveSheet()->getStyle('A1:T1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('00D8D8D8');//COLOR DE FONDO
//****************************

/**************** COLUMNAS ****************/

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('A2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('A2')->setValue("No.");
$objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('B2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('B2')->setValue("Nombre");
$objPHPExcel->getActiveSheet()->getStyle('B2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('C2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('C2')->setValue("Dirección");
$objPHPExcel->getActiveSheet()->getStyle('C2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('D2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('D2')->setValue("Año Contable");
$objPHPExcel->getActiveSheet()->getStyle('D2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('E2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('E2')->setValue("Región");
$objPHPExcel->getActiveSheet()->getStyle('E2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('F2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('F2')->setValue("Departamento");
$objPHPExcel->getActiveSheet()->getStyle('F2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('G2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('G2')->setValue("Municipío");
$objPHPExcel->getActiveSheet()->getStyle('G2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('H2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('H2')->setValue("Tipo Intervención");
$objPHPExcel->getActiveSheet()->getStyle('H2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('I2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('I2')->setValue("Modalidad");
$objPHPExcel->getActiveSheet()->getStyle('I2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('J2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('J2')->setValue("Estado");
$objPHPExcel->getActiveSheet()->getStyle('J2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('K2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('K2')->setValue("Código");
$objPHPExcel->getActiveSheet()->getStyle('K2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('L2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('L2')->setValue("Imagen");
$objPHPExcel->getActiveSheet()->getStyle('L2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('M2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('M2')->setValue("Area");
$objPHPExcel->getActiveSheet()->getStyle('M2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('N2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('N2')->setValue("Formato");
$objPHPExcel->getActiveSheet()->getStyle('N2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('O2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('O2')->setValue("Puesto Gerente");
$objPHPExcel->getActiveSheet()->getStyle('O2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('P2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('P2')->setValue("Puesto Director");
$objPHPExcel->getActiveSheet()->getStyle('P2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('Q2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('Q2')->setValue("Puesto Ejecutivo");
$objPHPExcel->getActiveSheet()->getStyle('Q2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('R2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('R2')->setValue("Puesto Asesor");
$objPHPExcel->getActiveSheet()->getStyle('R2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('S2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('S2')->setValue("Puesto Cajas");
$objPHPExcel->getActiveSheet()->getStyle('S2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('T2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('T2')->setValue("Puesto Otros");
$objPHPExcel->getActiveSheet()->getStyle('T2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

$sql = '';
if($listasucursales == '')
{
	$sql = "(s.suc_clave_int = '".$consec."' OR '".$consec."' IS NULL OR '".$consec."' = '') and (suc_nombre LIKE REPLACE('%".$nom."%',' ','%') OR '".$nom."' IS NULL OR '".$nom."' = '') and (suc_ano_contable = '".$anocon."' OR '".$anocon."' IS NULL OR '".$anocon."' = '') and (suc_codigo LIKE REPLACE('%".$cod."%',' ','%') OR '".$cod."' IS NULL OR '".$cod."' = '') and (suc_region = '".$reg."' OR '".$reg."' IS NULL OR '".$reg."' = '') and (suc_municipio = '".$mun."' OR '".$mun."' IS NULL OR '".$mun."' = '') and (tii_clave_int = '".$tipint."' OR '".$tipint."' IS NULL OR '".$tipint."' = '') and (mod_clave_int = '".$moda."' OR '".$moda."' IS NULL OR '".$moda."' = '') and (suc_estado = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') and (sui_inmobiliaria = '".$actor."' or sud_disenador = '".$actor."' or sul_gestionador = '".$actor."' or sco_constructor = '".$actor."' or sci_int_civil = '".$actor."' or sue_int_electromecanico = '".$actor."' or sse_instalador = '".$actor."' or '".$actor."' IS NULL or '".$actor."' = '') and ((s.suc_fecha_inicio BETWEEN '".$fii." 00:00:00' AND '".$ffi." 23:59:59') or ('".$fii."' Is Null and '".$ffi."' Is Null) or ('".$fii."' = '' and '".$ffi."' = '')) and s.suc_sw_eliminado = 1";
}
else
{
	$sql = "(s.suc_clave_int = '".$consec."' OR '".$consec."' IS NULL OR '".$consec."' = '') and (suc_nombre LIKE REPLACE('%".$nom."%',' ','%') OR '".$nom."' IS NULL OR '".$nom."' = '') and (suc_ano_contable = '".$anocon."' OR '".$anocon."' IS NULL OR '".$anocon."' = '') and (suc_codigo LIKE REPLACE('%".$cod."%',' ','%') OR '".$cod."' IS NULL OR '".$cod."' = '') and (suc_region = '".$reg."' OR '".$reg."' IS NULL OR '".$reg."' = '') and (suc_municipio = '".$mun."' OR '".$mun."' IS NULL OR '".$mun."' = '') and (tii_clave_int = '".$tipint."' OR '".$tipint."' IS NULL OR '".$tipint."' = '') and (mod_clave_int = '".$moda."' OR '".$moda."' IS NULL OR '".$moda."' = '') and (suc_estado = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') and (sui_inmobiliaria = '".$actor."' or sud_disenador = '".$actor."' or sul_gestionador = '".$actor."' or sco_constructor = '".$actor."' or sci_int_civil = '".$actor."' or sue_int_electromecanico = '".$actor."' or sse_instalador = '".$actor."' or '".$actor."' IS NULL or '".$actor."' = '') and ((s.suc_fecha_inicio BETWEEN '".$fii." 00:00:00' AND '".$ffi." 23:59:59') or ('".$fii."' Is Null and '".$ffi."' Is Null) or ('".$fii."' = '' and '".$ffi."' = '')) and s.suc_sw_eliminado = 1 and s.suc_clave_int in (".$listasucursales.")";
}

$con = mysqli_query($conectar,"select * from sucursal s inner join sucursal_inmobiliaria sinm on (sinm.suc_clave_int = s.suc_clave_int) inner join sucursal_diseno sdis on (sdis.suc_clave_int = s.suc_clave_int) inner join sucursal_licencia slic on (slic.suc_clave_int = s.suc_clave_int) inner join sucursal_constructor scon on (scon.suc_clave_int = s.suc_clave_int) inner join sucursal_presupuesto spre on (spre.suc_clave_int = s.suc_clave_int) inner join sucursal_suministro ssum on (ssum.suc_clave_int = s.suc_clave_int) inner join sucursal_civil sciv on (sciv.suc_clave_int = s.suc_clave_int) inner join sucursal_electromecanico sele on (sele.suc_clave_int = s.suc_clave_int) inner join sucursal_seguridad sseg on (sseg.suc_clave_int = s.suc_clave_int) where ".$sql."");
$num = mysqli_num_rows($con);

$j = 3;
for($i = 0; $i < $num; $i++)
{
	$dato = mysqli_fetch_array($con);
	$clasuc = $dato['suc_clave_int'];
	$nom = $dato['suc_nombre'];
	$dir = $dato['suc_direccion'];
	$anocon = $dato['suc_ano_contable'];
	$reg = $dato['suc_region'];
	$dep = $dato['suc_departamento'];
	$mun = $dato['suc_municipio'];
	$tipint = $dato['tii_clave_int'];
	$mod = $dato['mod_clave_int'];
	$estpro = $dato['suc_estado'];
	$cod = $dato['suc_codigo'];
	$ima = $dato['ima_clave_int'];
	$are = $dato['suc_area'];
	$for = $dato['for_clave_int'];
	$pger = $dato['suc_puesto_gerente'];
	$pdir = $dato['suc_puesto_director'];
	$peje = $dato['suc_puesto_ejecutivo'];
	$pase = $dato['suc_puesto_asesor'];
	$pcaj = $dato['suc_puesto_cajas'];
	$potro = $dato['suc_puesto_otros'];
	
	$sql = mysqli_query($conectar,"select pog_nombre from posicion_geografica where pog_clave_int = '".$reg."'");
	$datosql = mysqli_fetch_array($sql);
	$nomreg = $datosql['pog_nombre'];
	
	$sql = mysqli_query($conectar,"select pog_nombre from posicion_geografica where pog_clave_int = '".$dep."'");
	$datosql = mysqli_fetch_array($sql);
	$nomdep = $datosql['pog_nombre'];
	
	$sql = mysqli_query($conectar,"select pog_nombre from posicion_geografica where pog_clave_int = '".$mun."'");
	$datosql = mysqli_fetch_array($sql);
	$nommun = $datosql['pog_nombre'];
	
	$sql = mysqli_query($conectar,"select tii_nombre from tipointervencion where tii_clave_int = '".$tipint."'");
	$datosql = mysqli_fetch_array($sql);
	$nomtii = $datosql['tii_nombre'];
	
	$sql = mysqli_query($conectar,"select mod_nombre from modalidad where mod_clave_int = '".$mod."'");
	$datosql = mysqli_fetch_array($sql);
	$moda = $datosql['mod_nombre'];
	
	if($estpro == 1){ $est = "ACTIVO"; }elseif($estpro == 2){ $est = "SUSPENDIDO"; }elseif($estpro == 3){ $est = "ENTREGADO"; }elseif($estpro == 4){ $est = "CANCELADO"; }elseif($estpro == 5){ $est = "PROGRAMADO"; }
	
	$sql = mysqli_query($conectar,"select ima_nombre from imagen where ima_clave_int = '".$ima."'");
	$datosql = mysqli_fetch_array($sql);
	$nomima = $datosql['ima_nombre'];
	
	$sql = mysqli_query($conectar,"select for_nombre from formato where for_clave_int = '".$for."'");
	$datosql = mysqli_fetch_array($sql);
	$nomfor = $datosql['for_nombre'];
											
	/**************** DATOS DE LAS COLUMNAS ****************/ 

	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('A'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("A".$j, $clasuc);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('B'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("B".$j, $nom);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('C'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("C".$j, $dir);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('D'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("D".$j, $anocon);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('E'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("E".$j, $nomreg);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('F'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("F".$j, $nomdep);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('G'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("G".$j, $nommun);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('H'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("H".$j, $nomtii);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('I'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("I".$j, $moda);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('J'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("J".$j, $est);
	//***************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('K'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("K".$j, $cod);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('L'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("L".$j, $nomima);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('M'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("M".$j, $are);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('N'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("N".$j, $nomfor);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('O'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("O".$j, $pger);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('P'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("P".$j, $pdir);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('Q'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("Q".$j, $peje);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('R'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("R".$j, $pase);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('S'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("S".$j, $pcaj);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('T'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("T".$j, $potro);
	//****************************
	
	$j++;
}
//DATOS DE SALIDA DEL EXCEL
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$archivo.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
?>