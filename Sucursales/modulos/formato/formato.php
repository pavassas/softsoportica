<?php
	error_reporting(0);
	include('../../data/Conexion.php');
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_SESSION['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];
		
	// verifica si no se ha loggeado
	if(!isset($_SESSION["persona"]))
	{
	  session_destroy();
	  header("LOCATION:index.php");
	}else{
	}
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");
	
	$con = mysqli_query($conectar,"select u.usu_clave_int,p.prf_clave_int,p.prf_descripcion from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
	$dato = mysqli_fetch_array($con);
	$claprf = $dato['prf_clave_int'];
	
	$con = mysqli_query($conectar,"select per_metodo from permiso where prf_clave_int = '".$claprf."' and ven_clave_int = 17");
	$dato = mysqli_fetch_array($con);
	$metodo = $dato['per_metodo'];
	
	if($_GET['editarform'] == 'si')
	{
		$formedi = $_GET['formedi'];
		$con = mysqli_query($conectar,"select * from formato where for_clave_int = '".$formedi."'");
		$dato = mysqli_fetch_array($con);
		$form = $dato['for_nombre'];
		$act = $dato['for_sw_activo'];
?>
		<table style="width: 38%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Formato:</td>
			<td class="auto-style3">
			<input name="formato1" id="formato1" value="<?php echo $form; ?>" class="inputs" type="text" style="width: 200px" />&nbsp;
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3" colspan="2">Activa:<input class="inputs" <?php if($act == 1){ echo 'checked="checked"'; } ?> name="activo1" type="checkbox" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4">
			<input name="submit" type="button" value="Guardar" onclick="GUARDAR('<?php echo $formedi; ?>')"  style="width: 348px; height: 25px; cursor:pointer" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">
			<div id="datos">
			</div>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
<?php
		exit();
	}
	
	if($_GET['guardarform'] == 'si')
	{
		sleep(1);
		$fecha=date("Y/m/d H:i:s");
		$form = $_GET['form'];
		$act = $_GET['act'];
		$lf = $_GET['lf'];
		$f = $_GET['f'];
		
		$sql = mysqli_query($conectar,"select * from formato where (UPPER(for_nombre) = UPPER('".$form."')) AND for_clave_int <> '".$f."'");
		$dato = mysqli_fetch_array($sql);
		$conform = $dato['for_nombre'];
		
		if($form == '')
		{
			echo "<div class='validaciones'>Debe ingresar el formato</div>";
		}
		else
		if(STRTOUPPER($conform) == STRTOUPPER($form))
		{
			echo "<div class='validaciones'>El formato ingresado ya existe</div>";
		}
		else
		if($lf < 3)
		{
			echo "<div class='validaciones'>El formato debe ser mí­nimo de 3 dijitos</div>";
		}
		else
		{			
			if($act == 'false'){ $swact = 0; }elseif($act == 'true'){ $swact = 1; }
			
			$con = mysqli_query($conectar,"update formato set for_nombre = '".$form."', for_sw_activo = '".$swact."', for_usu_actualiz = '".$usuario."', for_fec_actualiz = '".$fecha."' where for_clave_int = '".$f."'");
			
			if($con >= 1)
			{
				echo "<div class='ok'>Datos grabados correctamente</div>";
				mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'29',74,'".$a."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 11=Actualización área
			}
			else
			{
				echo "<div class='validaciones'>No se han podido guardar los datos</div>";
			}	
		}
		exit();
	}
	
	if($_GET['nuevoformato'] == 'si')
	{
		sleep(1);
		$fecha=date("Y/m/d H:i:s");
		$form = $_GET['form'];
		$act = $_GET['act'];
		$lf = $_GET['lf'];
		
		$sql = mysqli_query($conectar,"select * from formato where (UPPER(for_nombre) = UPPER('".$form."'))");
		$dato = mysqli_fetch_array($sql);
		$conform = $dato['for_nombre'];
		
		if($form == '')
		{
			echo "<div class='validaciones'>Debe ingresar el formato</div>";
		}
		else
		if(STRTOUPPER($conform) == STRTOUPPER($form))
		{
			echo "<div class='validaciones'>El formato ingresado ya existe</div>";
		}
		else
		if($lf < 3)
		{
			echo "<div class='validaciones'>El formato debe ser mí­nimo de 3 dijitos</div>";
		}
		else
		{
			if($act == 'false'){ $swact = 0; }elseif($act == 'true'){ $swact = 1; }
			$con = mysqli_query($conectar,"insert into formato(for_nombre,for_sw_activo,for_usu_actualiz,for_fec_actualiz) values('".$form."','".$swact."','".$usuario."','".$fecha."')");
			
			if($con >= 1)
			{
				echo "<div class='ok'>Datos grabados correctamente</div>";
				$con = mysqli_query($conectar,"select max(for_clave_int) max from formato");
				$dato = mysqli_fetch_array($con);
				$max = $dato['max'];
				mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'29',73,'".$max."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 10=Creación área
			}
			else
			{
				echo "<div class='validaciones'>No se han podido guardar los datos</div>";
			}
		}
		exit();
	}
	if($_GET['todos'] == 'si')
	{
		sleep(1);
		$rows=mysqli_query($conectar,"select * from formato");
		$total=mysqli_num_rows($rows);
?>
		<table style="width: 100%">
		<tr>
			<td class="auto-style3" style="width: 27px">
				<input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<?php echo $total;?>,this);" class="auto-style6" /><span class="auto-style6">
				</span>
			</td>
			<td class="auto-style3" colspan="5">
			<?php
			if($metodo == 1)
			{
			?>
			<table style="width: 30%">
				<tr>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/activo.png" alt="" class="auto-style6" /><input type="submit" value="Activar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/inactivo.png" alt="" class="auto-style6" /><input type="submit" value="Inactivar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/eliminar.png" alt="" class="auto-style6" /><input type="submit" value="Eliminar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
				</tr>
			</table>
			<?php
			}
			?>
			</td>
		</tr>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px"><strong>Formato</strong></td>
			<td class="auto-style5" style="width: 98px"><strong>Usuario</strong></td>
			<td class="auto-style5" style="width: 127px"><strong>
				Actualización</strong></td>
			<td class="auto-style5" style="width: 29px"><strong>
			Activo</strong></td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
		<?php
			$contador=0;
			$con = mysqli_query($conectar,"select * from formato order by for_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$claform = $dato['for_clave_int'];
				$form = $dato['for_nombre'];
				$act = $dato['for_sw_activo'];
				$usuact = $dato['for_usu_actualiz'];
				$fecact = $dato['for_fec_actualiz'];
				$contador=$contador+1;
		?>
		<tr>
			<td class="auto-style3" style="width: 27px">
				<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<?php echo $contador;?>" value="<?php echo $dato['for_clave_int'];?>" class="auto-style6" /></td>
			<td class="auto-style5" style="width: 200px"><?php echo $form; ?></td>
			<td class="auto-style5" style="width: 98px"><?php echo $usuact; ?></td>
			<td class="auto-style5" style="width: 127px"><?php echo $fecact; ?></td>
			<td class="auto-style1" style="width: 30px">
			<input name="activarinactivar" id="activarinactivar" type="checkbox" <?php if($act == 1){ echo 'checked="checked"'; } ?> disabled="disabled" class="auto-style6" ></td>
			<td class="auto-style5">
			<?php
			if($metodo == 1)
			{
			?>
			<a data-reveal-id="editararea" data-animation="fade" style="cursor:pointer" onclick="EDITAR('<?php echo $dato['for_clave_int']; ?>')"><img src="../../img/editar.png" alt="" height="22" width="21" /></a>
			<?php
			}
			?>
			</td>
		</tr>
		<?php
			}
		?>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 98px">&nbsp;</td>
			<td class="auto-style5" style="width: 127px">&nbsp;</td>
			<td class="auto-style5" style="width: 29px">&nbsp;</td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
	</table>
<?php
		exit();
	}
?>
<?php
	if($_GET['buscarform'] == 'si')
	{
		$form = $_GET['form'];
		$act = $_GET['act'];

		$rows=mysqli_query($conectar,"select * from formato where (for_nombre LIKE REPLACE('%".$form."%',' ','%') OR '".$form."' IS NULL OR '".$form."' = '') and (for_sw_activo = '".$act."' OR '".$act."' IS NULL OR '".$act."' = '')");
		$total=mysqli_num_rows($rows);
?>
		<table style="width: 100%">
		<tr>
			<td class="auto-style3" style="width: 27px">
				<input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<?php echo $total;?>,this);" class="auto-style6" /><span class="auto-style6">
				</span>
			</td>
			<td class="auto-style3" colspan="5">
			<?php
			if($metodo == 1)
			{
			?>
			<table style="width: 30%">
				<tr>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/activo.png" alt="" class="auto-style6" /><input type="submit" value="Activar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/inactivo.png" alt="" class="auto-style6" /><input type="submit" value="Inactivar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/eliminar.png" alt="" class="auto-style6" /><input type="submit" value="Eliminar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
				</tr>
			</table>
			<?php
			}
			?>
			</td>
		</tr>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px"><strong>Formato</strong></td>
			<td class="auto-style5" style="width: 98px"><strong>Usuario</strong></td>
			<td class="auto-style5" style="width: 127px"><strong>
				Actualización</strong></td>
			<td class="auto-style5" style="width: 29px"><strong>
			Activo</strong></td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
		<?php
			$contador=0;
			$con = mysqli_query($conectar,"select * from formato where (for_nombre LIKE REPLACE('%".$form."%',' ','%') OR '".$form."' IS NULL OR '".$form."' = '') and (for_sw_activo = '".$act."' OR '".$act."' IS NULL OR '".$act."' = '') order by for_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$claform = $dato['for_clave_int'];
				$form = $dato['for_nombre'];
				$act = $dato['for_sw_activo'];
				$usuact = $dato['for_usu_actualiz'];
				$fecact = $dato['for_fec_actualiz'];
				$contador=$contador+1;
		?>
		<tr>
			<td class="auto-style3" style="width: 27px">
				<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<?php echo $contador;?>" value="<?php echo $dato['for_clave_int'];?>" class="auto-style6" /></td>
			<td class="auto-style5" style="width: 200px"><?php echo $form; ?></td>
			<td class="auto-style5" style="width: 98px"><?php echo $usuact; ?></td>
			<td class="auto-style5" style="width: 127px"><?php echo $fecact; ?></td>
			<td class="auto-style1" style="width: 30px">
			<input name="activarinactivar" id="activarinactivar" type="checkbox" <?php if($act == 1){ echo 'checked="checked"'; } ?> disabled="disabled" class="auto-style6" ></td>
			<td class="auto-style5">
			<?php
			if($metodo == 1)
			{
			?>
			<a data-reveal-id="editararea" data-animation="fade" style="cursor:pointer" onclick="EDITAR('<?php echo $dato['for_clave_int']; ?>')"><img src="../../img/editar.png" alt="" height="22" width="21" /></a>
			<?php
			}
			?>
			</td>
		</tr>
		<?php
			}
		?>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 98px">&nbsp;</td>
			<td class="auto-style5" style="width: 127px">&nbsp;</td>
			<td class="auto-style5" style="width: 29px">&nbsp;</td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
	</table>
<?php
		exit();
	}
?>
<!DOCTYPE HTML>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

	
<title>CONTROL DE OBRAS</title>
<meta name="description" content="Service Desk">
<meta name="author" content="InvGate S.R.L.">

<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
<!--[if lte IE 7]>
<link rel="stylesheet" href="css/ie.c32bc18afe0e2883ee4912a51f86c119.css" type="text/css" />
<![endif]-->

<style type="text/css">
.auto-style1 {
	text-align: center;
}
.auto-style2 {
	font-size: 12px;
	font-family: Arial, helvetica;
	outline: none;
/*transition: all 0.75s ease-in-out;*/ /*-webkit-transition: all 0.75s ease-in-out;*/ /*-moz-transition: all 0.75s ease-in-out;*/border-radius: 3px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 3px;
	border: 1px solid rgba(0,0,0, 0.2);
	background-color: #eee;
	padding: 3px;
	box-shadow: 0 0 10px #aaa;
	-webkit-box-shadow: 0 0 10px #aaa;
	-moz-box-shadow: 0 0 10px #aaa;
	border: 1px solid #999;
	background-color: white;
	text-align: center;
}
.auto-style3 {
	text-align: left;
}
.inputs{
	font-size:14px;
    font-family: Arial, helvetica;
    outline:none;
    border-radius:3px;
    -webkit-border-radius:6px;
    -moz-border-radius:3px;
    border:1px solid rgba(0,0,0, 0.5);
    padding: 3px;
}
.validaciones{
	font-size:14px;
    font-family: Arial, helvetica;
    outline:none;
    border-radius:3px;
    -webkit-border-radius:6px;
    -moz-border-radius:3px;
    border:1px solid rgba(0,0,0, 0.2);
    color:maroon;
    background-color:#eee;
    padding: 3px;
    text-align:center;
}
.ok{
	font-size:14px;
    font-family: Arial, helvetica;
    outline:none;
    border-radius:3px;
    -webkit-border-radius:6px;
    -moz-border-radius:3px;
    border:1px solid rgba(0,0,0, 0.2);
    color:green;
    background-color:#eee;
    padding: 3px;
    text-align:center;
}
.auto-style5 {
	text-align: left;
	font-family: Arial, Helvetica, sans-serif;
}
.auto-style6 {
	font-family: Arial, Helvetica, sans-serif;
}
.auto-style7 {
	text-align: center;
	font-size: large;
}
</style>

<script type="text/javascript" language="javascript">
	selecteds=0;
	
	function CheckUncheck(total,check){
		checkbox=null;
		for(i=1;i<=total;i++){
			checkbox=document.getElementById("idcat"+i);
			//alert(checkbox.value);
			checkbox.checked=check.checked;
		}
		
		if(check.checked){
			selecteds=total;
		}else{
			selecteds=0;
		}
		
	}
	
	function contadorVals(check){
		if(check.checked){
			selecteds=selecteds+1;
		}else{
			selecteds=selecteds-1;
		}
	}
	
	function selectedVals(){
		if(selecteds==0){
			alert("Seleccione al menos un registro.");
			return false;
		}else{
			return true;
		}
	}
</script>
<?php //VALIDACIONES ?>
<script type="text/javascript" src="llamadas.js"></script>
<script type="text/javascript" src="../../js/jquery-1.6.min.js"></script>

<?php //VENTANA EMERGENTE ?>
<link rel="stylesheet" href="../../css/reveal.css" />
<script type="text/javascript" src="../../js/jquery-1.6.min.js"></script>
<script type="text/javascript" src="../../js/jquery.reveal.js"></script>

</head>


<body>
<?php
$rows=mysqli_query($conectar,"select * from formato");
$total=mysqli_num_rows($rows);
?>
<form name="form1" id="form1" action="confirmar.php" method="post" onsubmit="return selectedVals();">
<!--[if lte IE 7]>
<div class="ieWarning">Este navegador no es compatible con el sistema. Por favor, use Chrome, Safari, Firefox o Internet Explorer 8 o superior.</div>
<![endif]-->
<table style="width: 100%">
	<tr>
		<td class="auto-style2" onclick="CONSULTAMODULO('TODOS')" onmouseover="this.style.backgroundColor='#445B74';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="width: 60px; cursor:pointer">
		Todos
		<?php
			$con = mysqli_query($conectar,"select COUNT(*) cant from formato");
			$dato = mysqli_fetch_array($con);
			echo $dato['cant'];
		?>
		</td>
		<td class="auto-style7" style="cursor:pointer" colspan="4">
		MAESTRA FORMATO</td>
		<td class="auto-style2" onmouseover="this.style.backgroundColor='#CCCCCC';this.style.color='#0F213C';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="width: 55px; cursor:pointer">
		<?php
		if($metodo == 1)
		{
		?>
		<a data-reveal-id="nuevaarea" data-animation="fade" style="cursor:pointer"><table style="width: 100%">
			<tr>
				<td style="width: 14px"><a data-reveal-id="nuevaarea" data-animation="fade" style="cursor:pointer"><img alt="" src="../../img/add2.png"></a></td>
				<td>Añadir</td>
			</tr>
		</table></a>
		<?php
		}
		?>
		</td>
	</tr>
	<tr>
		<td class="auto-style2" colspan="6">
		<div id="filtro">
			<table style="width: 33%">
				<tr>
					<td class="auto-style1"><strong>Filtro:<img src="../../img/buscar.png" alt="" height="18" width="15" /></strong></td>
					<td class="auto-style1">
			<input class="inputs" onkeyup="BUSCAR()" name="formato2" maxlength="70" type="text" placeholder="Formato" style="width: 150px" /></td>
					<td class="auto-style1">
					<strong>Activo:</strong></td>
					<td class="auto-style1">
					<select name="buscaractivos" class="inputs" onchange="BUSCAR()">
					<option value="">Todos</option>
					<option value="1">Activos</option>
					<option value="0">Inactivo</option>
					</select></td>
				</tr>
			</table>
		</div>
		</td>
	</tr>
	<tr>
		<td colspan="6" class="auto-style2">
		<div id="editararea" class="reveal-modal" style="left: 57%; top: 50px; height: 180px; width: 350px;">
			
			<a class="close-reveal-modal">&#215;</a>
		</div>
		<div id="areas">
		<table style="width: 100%">
			<tr>
				<td class="auto-style3" style="width: 27px">
					<input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<?php echo $total;?>,this);" />
				</td>
				<td class="auto-style3" colspan="5">
				<?php
				if($metodo == 1)
				{
				?>
				<table style="width: 30%">
					<tr>
						<td class="auto-style1"><p style="cursor:pointer"><img src="../../img/activo.png" alt="" /><input type="submit" value="Activar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" /></p></td>
						<td class="auto-style1"><p style="cursor:pointer"><img src="../../img/inactivo.png" alt="" /><input type="submit" value="Inactivar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" /></p></td>
						<td class="auto-style1"><p style="cursor:pointer"><img src="../../img/eliminar.png" alt="" /><input type="submit" value="Eliminar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" /></p></td>
					</tr>
				</table>
				<?php
				}
				?>
				</td>
			</tr>
			<tr>
				<td class="auto-style3" style="width: 27px">&nbsp;</td>
				<td class="auto-style3" style="width: 200px" ><strong>Formato</strong></td>
				<td class="auto-style3" style="width: 98px"><strong>Usuario</strong></td>
				<td class="auto-style3" style="width: 127px"><strong>
				Actualización</strong></td>
				<td class="auto-style3" style="width: 29px"><strong>
				Activo</strong></td>
				<td class="auto-style3">&nbsp;</td>
			</tr>
			<?php
				$contador=0;
				$con = mysqli_query($conectar,"select * from formato order by for_nombre");
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$claform = $dato['for_clave_int'];
					$form = $dato['for_nombre'];
					$act = $dato['for_sw_activo'];
					$usuact = $dato['for_usu_actualiz'];
					$fecact = $dato['for_fec_actualiz'];
					$contador=$contador+1;
			?>
			<tr>
				<td class="auto-style3" style="width: 27px">
					<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<?php echo $contador;?>" value="<?php echo $dato['for_clave_int'];?>" /></td>
				<td class="auto-style3" style="width: 200px" ><?php echo $form; ?></td>
				<td class="auto-style3" style="width: 98px"><?php echo $usuact; ?></td>
				<td class="auto-style3" style="width: 127px"><?php echo $fecact; ?></td>
				<td class="auto-style1" style="width: 30px">
				<input name="activarinactivar" id="activarinactivar" type="checkbox" <?php if($act == 1){ echo 'checked="checked"'; } ?> disabled="disabled" ></td>
				<td class="auto-style3">
				<?php
				if($metodo == 1)
				{
				?>
				<a data-reveal-id="editararea" data-animation="fade" style="cursor:pointer" onclick="EDITAR('<?php echo $dato['for_clave_int']; ?>')"><img src="../../img/editar.png" alt="" height="22" width="21" /></a>
				<?php
				}
				?>
				</td>
				<?php
					}
				?>
			</tr>
			<tr>
				<td class="auto-style3" style="width: 27px">&nbsp;</td>
				<td class="auto-style3" style="width: 200px" >&nbsp;</td>
				<td class="auto-style3" style="width: 98px">&nbsp;</td>
				<td class="auto-style3" style="width: 127px">&nbsp;</td>
				<td class="auto-style3" style="width: 29px">&nbsp;</td>
				<td class="auto-style3">&nbsp;</td>
			</tr>
		</table>
		</div>
<div id="nuevaarea" class="reveal-modal" style="left: 57%; top: 50px; height: 180px; width: 350px;">
	<table style="width: 38%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Formato:</td>
			<td class="auto-style3">
			<input name="formato" id="formato"  type="text" style="width: 200px" class="inputs" />
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Activo:</td>
			<td class="auto-style3"><input class="inputs" name="activo" checked="checked" type="checkbox" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4">
			<input name="nuevo1" type="button" value="Guardar" onclick="NUEVO()"  style="width: 348px; height: 25px; cursor:pointer" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">
			<div id="datos1">
			</div>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
</div>
		</td>
	</tr>
	<tr>
		<td style="width: 60px">&nbsp;</td>
		<td style="width: 117px">&nbsp;</td>
		<td style="width: 65px">&nbsp;</td>
		<td style="width: 70px">&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td style="width: 60px">&nbsp;</td>
		<td style="width: 117px">&nbsp;</td>
		<td style="width: 65px">&nbsp;</td>
		<td style="width: 70px">&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>
</form>
</body>
</html>