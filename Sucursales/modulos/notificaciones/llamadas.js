function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function EDITAR(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('editararea').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#editararea").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?editarform=si&formedi="+v,true);
	ajax.send(null);
}
function GUARDAR(id)
{
	var form = form1.formato1.value;
	var lf = form.length;
	var diasnot = form1.diasnot1.value;
	var act = form1.activo1.checked;
	
	var usuarios = "";
	var objCBarray = document.getElementsByName('multiselect_usuarios');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	usuarios += objCBarray[i].value + ",";
	    }
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('datos').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#datos").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?guardarform=si&form="+form+"&usuarios="+usuarios+"&diasnot="+diasnot+"&act="+act+"&lf="+lf+"&f="+id,true);
	ajax.send(null);
	if(form != '' && lf >= 3)
	{
		setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
	}
}
function NUEVO()
{
	var ima = form1.imagen.value;
	var li = ima.length;
	var act = form1.activo.checked;
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('datos1').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#datos1").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?nuevaimagen=si&ima="+ima+"&act="+act+"&li="+li,true);
	ajax.send(null);
	if(ima != '' && li >= 3)
	{
		form1.imagen.value = '';
		setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
	}
}
function CONSULTAMODULO(v)
{
	if(v == 'TODOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('areas').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#areas").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?todos=si",true);
		ajax.send(null);
	}
}
function BUSCAR()
{	
	var form = form1.formato2.value;
	var act = form1.buscaractivos.value;
			
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('areas').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#areas").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?buscarform=si&form="+form+"&act="+act,true);
	ajax.send(null);
}