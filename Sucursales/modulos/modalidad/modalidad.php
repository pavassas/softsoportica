<?php
	error_reporting(0);
	include('../../data/Conexion.php');
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_SESSION['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];
	
	// verifica si no se ha loggeado
	if(!isset($_SESSION["persona"]))
	{
	  session_destroy();
	  header("LOCATION:index.php");
	}else{
	}
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");
	
	$con = mysqli_query($conectar,"select u.usu_clave_int,p.prf_clave_int,p.prf_descripcion from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
	$dato = mysqli_fetch_array($con);
	$claprf = $dato['prf_clave_int'];
	
	$con = mysqli_query($conectar,"select per_metodo from permiso where prf_clave_int = '".$claprf."' and ven_clave_int = 15");
	$dato = mysqli_fetch_array($con);
	$metodo = $dato['per_metodo'];
	
	if($_GET['editarmod'] == 'si')
	{
		$modedi = $_GET['modedi'];
		$con = mysqli_query($conectar,"select * from modalidad where mod_clave_int = '".$modedi."'");
		$dato = mysqli_fetch_array($con);
		$mod = $dato['mod_nombre'];
		$act = $dato['mod_sw_activo'];
		$inm = $dato['mod_sw_inmobiliaria'];
		$dinm = $dato['mod_dias_inmobiliaria'];
		$dis = $dato['mod_sw_diseno'];
		$ddis = $dato['mod_dias_diseno'];
		$lic = $dato['mod_sw_licencia'];
		$dlic = $dato['mod_dias_licencia'];
		$pptosum = $dato['mod_sw_pptosuministro'];
		$dpptosum = $dato['mod_dias_pptosuministro'];
		$obra = $dato['mod_sw_obra'];
		$dobra = $dato['mod_dias_obra'];
?>
		<table style="width: 38%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Modalidad:</td>
			<td class="auto-style3">
			<input name="modalidad1" id="modalidad1" value="<?php echo $mod; ?>" class="inputs" type="text" style="width: 200px" />&nbsp;
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3" colspan="2">
			<table style="width: 100%">
				<tr>
					<td>
					<table style="width: 100%">
						<tr>
							<td style="width: 10px"><input name="inmobiliaria1" id="inmobiliaria1" onclick="ACTIVARDIAS1()" <?php if($inm == 1){ echo 'checked="checked"'; } ?> type="checkbox" /></td>
							<td style="width: 70px"><label for="inmobiliaria1" style="cursor:pointer">Inmobiliaria</label></td>
							<td style="width: 30px"><input name="diasinmobiliaria1" id="diasinmobiliaria1" value="<?php echo $dinm; ?>" <?php if($inm == 0){ echo 'disabled="disabled"'; } ?> type="text" style="width: 36px" class="inputs" /></td>
							<td style="width: 10px"><input name="diseno1" id="diseno1" onclick="ACTIVARDIAS1()" <?php if($dis == 1){ echo 'checked="checked"'; } ?> type="checkbox" /></td>
							<td style="width: 60px"><label for="diseno1" style="cursor:pointer">Diseñador</label></td>
							<td><input name="diasdiseno1" id="diasdiseno1" value="<?php echo $ddis; ?>" <?php if($dis == 0){ echo 'disabled="disabled"'; } ?> type="text" style="width: 36px" class="inputs" /></td>
						</tr>
						<tr>
							<td style="width: 10px"><input name="licencia1" id="licencia1" onclick="ACTIVARDIAS1()" <?php if($lic == 1){ echo 'checked="checked"'; } ?> type="checkbox" /></td>
							<td style="width: 70px"><label for="licencia1" style="cursor:pointer">Licencia</label></td>
							<td style="width: 30px"><input name="diaslicencia1" id="diaslicencia1" value="<?php echo $dlic; ?>" <?php if($lic == 0){ echo 'disabled="disabled"'; } ?> type="text" style="width: 36px" class="inputs" /></td>
							<td style="width: 10px"><input name="pptosum1" id="pptosum1" onclick="ACTIVARDIAS1()" <?php if($pptosum == 1){ echo 'checked="checked"'; } ?> type="checkbox" /></td>
							<td style="width: 60px"><label for="pptosum1" style="cursor:pointer">Ppto. y Suministro</label></td>
							<td><input name="diaspptosum1" id="diaspptosum1" value="<?php echo $dpptosum; ?>" <?php if($pptosum == 0){ echo 'disabled="disabled"'; } ?> type="text" style="width: 36px" class="inputs" /></td>
						</tr>
						<tr>
							<td style="width: 10px"><input name="obra1" id="obra1" onclick="ACTIVARDIAS1()" <?php if($obra == 1){ echo 'checked="checked"'; } ?> type="checkbox" /></td>
							<td style="width: 70px"><label for="obra1" style="cursor:pointer">Obra</label></td>
							<td style="width: 30px"><input name="diasobra1" id="diasobra1" value="<?php echo $dobra; ?>" <?php if($obra == 0){ echo 'disabled="disabled"'; } ?> type="text" style="width: 36px" class="inputs" /></td>
							<td style="width: 10px">&nbsp;</td>
							<td style="width: 60px">&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						</table>
					</td>
				</tr>
				</table>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3" colspan="2">
			<table style="width: 100%">
				<tr>
					<td style="width: 29px">Activa:</td>
					<td style="width: 80px"><input class="inputs" <?php if($act == 1){ echo 'checked="checked"'; } ?> name="activo1" type="checkbox" /></td>
					<td style="width: 80px">&nbsp;</td>
					<td style="width: 2px">
					&nbsp;</td>
					<td style="width: 3px">&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4">
			<input name="submit" type="button" value="Guardar" onclick="GUARDAR('MODALIDAD','<?php echo $modedi; ?>')"  style="width: 348px; height: 25px; cursor:pointer" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">
			<div id="datos">
			</div>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
<?php
		exit();
	}
	
	if($_GET['guardarmod'] == 'si')
	{
		sleep(1);
		$mod = $_GET['mod'];
		$act = $_GET['act'];
		$lm = $_GET['lm'];
		$m = $_GET['m'];
		
		$inm = $_GET['inm'];
		$dinm = $_GET['dinm'];
		$dis = $_GET['dis'];
		$ddis = $_GET['ddis'];		
		$lic = $_GET['lic'];
		$dlic = $_GET['dlic'];
		$pptosum = $_GET['pptosum'];
		$dpptosum = $_GET['dpptosum'];
		$obra = $_GET['obra'];
		$dobra = $_GET['dobra'];
		
		$sql = mysqli_query($conectar,"select * from modalidad where (UPPER(mod_nombre) = UPPER('".$mod."')) AND mod_clave_int <> '".$m."'");
		$dato = mysqli_fetch_array($sql);
		$conmod = $dato['mod_nombre'];
		
		if($mod == '')
		{
			echo "<div class='validaciones'>Debe ingresar la modalidad</div>";
		}
		else
		if(STRTOUPPER($conmod) == STRTOUPPER($mod))
		{
			echo "<div class='validaciones'>La modalidad ingresada ya existe</div>";
		}
		else
		if($lm < 3)
		{
			echo "<div class='validaciones'>La modalidad debe ser mí­nimo de 3 dijitos</div>";
		}
		else
		{			
			if($act == 'false'){ $swact = 0; }elseif($act == 'true'){ $swact = 1; }
			if($inm == 'false'){ $inm = 0; }elseif($inm == 'true'){ $inm = 1; }
			if($dis == 'false'){ $dis = 0; }elseif($dis == 'true'){ $dis = 1; }
			if($lic == 'false'){ $lic = 0; }elseif($lic == 'true'){ $lic = 1; }
			if($pptosum == 'false'){ $pptosum = 0; }elseif($pptosum == 'true'){ $pptosum = 1; }
			if($obra == 'false'){ $obra = 0; }elseif($obra == 'true'){ $obra = 1; }
						
			$con = mysqli_query($conectar,"update modalidad set mod_nombre = '".$mod."', mod_sw_activo = '".$swact."', mod_sw_inmobiliaria = '".$inm."', mod_dias_inmobiliaria = '".$dinm."', mod_sw_diseno = '".$dis."', mod_dias_diseno = '".$ddis."', mod_sw_licencia = '".$lic."', mod_dias_licencia = '".$dlic."', mod_sw_pptosuministro = '".$pptosum."', mod_dias_pptosuministro = '".$dpptosum."', mod_sw_obra = '".$obra."', mod_dias_obra = '".$dobra."', mod_usu_actualiz = '".$usuario."', mod_fec_actualiz = '".$fecha."' where mod_clave_int = '".$m."'");
			
			if($con >= 1)
			{
				echo "<div class='ok'>Datos grabados correctamente</div>";
				mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'24',72,'".$m."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 16=Actualización atiende
			}
			else
			{
				echo "<div class='validaciones'>No se han podido guardar los datos</div>";
			}	
		}
		exit();
	}
	
	if($_GET['nuevamodalidad'] == 'si')
	{
		sleep(1);
		$fecha=date("Y/m/d H:i:s");
		$mod = $_GET['mod'];
		$act = $_GET['act'];
		$lm = $_GET['lm'];
		$inm = $_GET['inm'];
		$dinm = $_GET['dinm'];
		$dis = $_GET['dis'];
		$ddis = $_GET['ddis'];		
		$lic = $_GET['lic'];
		$dlic = $_GET['dlic'];
		$pptosum = $_GET['pptosum'];
		$dpptosum = $_GET['dpptosum'];
		$obra = $_GET['obra'];
		$dobra = $_GET['dobra'];
		
		$sql = mysqli_query($conectar,"select * from modalidad where (UPPER(mod_nombre) = UPPER('".$mod."'))");
		$dato = mysqli_fetch_array($sql);
		$conmod = $dato['mod_nombre'];
		
		if($mod == '')
		{
			echo "<div class='validaciones'>Debe ingresar la modalidad</div>";
		}
		else
		if(STRTOUPPER($conmod) == STRTOUPPER($mod))
		{
			echo "<div class='validaciones'>La modalidad ingresada ya existe</div>";
		}
		else
		if($lm < 3)
		{
			echo "<div class='validaciones'>La modalidad debe ser mí­nimo de 3 dijitos</div>";
		}
		else
		{
			if($act == 'false'){ $swact = 0; }elseif($act == 'true'){ $swact = 1; }
			if($inm == 'false'){ $inm = 0; }elseif($inm == 'true'){ $inm = 1; }
			if($dis == 'false'){ $dis = 0; }elseif($dis == 'true'){ $dis = 1; }
			if($lic == 'false'){ $lic = 0; }elseif($lic == 'true'){ $lic = 1; }
			if($pptosum == 'false'){ $pptosum = 0; }elseif($pptosum == 'true'){ $pptosum = 1; }
			if($obra == 'false'){ $obra = 0; }elseif($obra == 'true'){ $obra = 1; }
			
			$con = mysqli_query($conectar,"insert into modalidad(mod_nombre,mod_sw_activo,mod_sw_inmobiliaria,mod_dias_inmobiliaria,mod_sw_diseno,mod_dias_diseno,mod_sw_licencia,mod_dias_licencia,mod_sw_pptosuministro,mod_dias_pptosuministro,mod_sw_obra,mod_dias_obra,mod_usu_actualiz,mod_fec_actualiz) values('".$mod."','".$swact."','".$inm."','".$dinm."','".$dis."','".$ddis."','".$lic."','".$dlic."','".$pptosum."','".$dpptosum."','".$obra."','".$dobra."','".$usuario."','".$fecha."')");
			
			if($con >= 1)
			{
				echo "<div class='ok'>Datos grabados correctamente</div>";
				$con = mysqli_query($conectar,"select max(mod_clave_int) max from modalidad");
				$dato = mysqli_fetch_array($con);
				$max = $dato['max'];
				mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'24',71,'".$max."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 15=Creación atiende
			}
			else
			{
				echo "<div class='validaciones'>No se han podido guardar los datos</div>";
			}
		}
		exit();
	}
	if($_GET['todos'] == 'si')
	{
		sleep(1);
		$rows=mysqli_query($conectar,"select * from modalidad");
		$total=mysqli_num_rows($rows);
?>
		<table style="width: 80%">
		<tr>
			<td class="auto-style3" style="width: 27px">
				<input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<?php echo $total;?>,this);" class="auto-style6" /><span class="auto-style6">
				</span>
			</td>
			<td class="auto-style3" colspan="8">
			<?php
			if($metodo == 1)
			{
			?>
			<table style="width: 30%">
				<tr>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/activo.png" alt="" class="auto-style6" /><input type="submit" value="Activar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/inactivo.png" alt="" class="auto-style6" /><input type="submit" value="Inactivar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/eliminar.png" alt="" class="auto-style6" /><input type="submit" value="Eliminar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
				</tr>
			</table>
			<?php
			}
			?>
			</td>
		</tr>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px"><strong>Modalidad</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Inmob.</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Diseño</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Licencia</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Ppto. y Suministro</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Obra</strong></td>
			<td class="auto-style5" style="width: 29px"><strong>
			Activo</strong></td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
		<?php
			$contador=0;
			$con = mysqli_query($conectar,"select * from modalidad order by mod_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$mod = $dato['mod_nombre'];
				$act = $dato['mod_sw_activo'];
				
				$inm = $dato['mod_sw_inmobiliaria'];
				$dinm = $dato['mod_dias_inmobiliaria'];
				$dis = $dato['mod_sw_diseno'];
				$ddis = $dato['mod_dias_diseno'];
				$lic = $dato['mod_sw_licencia'];
				$dlic = $dato['mod_dias_licencia'];
				$pptosum = $dato['mod_sw_pptosuministro'];
				$dpptosum = $dato['mod_dias_pptosuministro'];
				$obra = $dato['mod_sw_obra'];
				$dobra = $dato['mod_dias_obra'];
				$usuact = $dato['mod_usu_actualiz'];
				$fecact = $dato['mod_fec_actualiz'];
				$contador=$contador+1;
		?>
		<tr>
			<td class="auto-style3" style="width: 27px">
				<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<?php echo $contador;?>" value="<?php echo $dato['mod_clave_int'];?>" class="auto-style6" /></td>
				<td class="auto-style5" style="width: 200px"><?php echo $mod; ?></td>
				<td class="auto-style1" style="width: 200px" >
				<?php if($inm == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dinm; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" >
				<?php if($dis == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $ddis; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" >
				<?php if($lic == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dlic; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" >
				<?php if($pptosum == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dpptosum; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" >
				<?php if($obra == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dobra; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
			<td class="auto-style1" style="width: 30px">
			<input name="activarinactivar" id="activarinactivar" type="checkbox" <?php if($act == 1){ echo 'checked="checked"'; } ?> disabled="disabled" class="auto-style6" ></td>
			<td class="auto-style5">
			<?php
			if($metodo == 1)
			{
			?>
			<a data-reveal-id="editarmodalidad" data-animation="fade" style="cursor:pointer" onclick="EDITAR('<?php echo $dato['mod_clave_int']; ?>','MODALIDAD')"><img src="../../img/editar.png" alt="" height="22" width="21" /></a>
			<?php
			}
			?>
			</td>
		</tr>
		<?php
			}
		?>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 29px">&nbsp;</td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
	</table>
<?php
		exit();
	}
?>
<?php
	if($_GET['buscarmod'] == 'si')
	{
		$mod = $_GET['mod'];
		$act = $_GET['act'];

		$rows=mysqli_query($conectar,"select * from modalidad where (mod_nombre LIKE REPLACE('%".$mod."%',' ','%') OR '".$mod."' IS NULL OR '".$mod."' = '') and (mod_sw_activo = '".$act."' OR '".$act."' IS NULL OR '".$act."' = '')");
		$total=mysqli_num_rows($rows);
?>
		<table style="width: 80%">
		<tr>
			<td class="auto-style3" style="width: 27px">
				<input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<?php echo $total;?>,this);" class="auto-style6" /><span class="auto-style6">
				</span>
			</td>
			<td class="auto-style3" colspan="8">
			<?php
			if($metodo == 1)
			{
			?>
			<table style="width: 30%">
				<tr>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/activo.png" alt="" class="auto-style6" /><input type="submit" value="Activar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/inactivo.png" alt="" class="auto-style6" /><input type="submit" value="Inactivar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/eliminar.png" alt="" class="auto-style6" /><input type="submit" value="Eliminar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
				</tr>
			</table>
			<?php
			}
			?>
			</td>
		</tr>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px"><strong>Modalidad</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Inmob.</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Diseño</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Licencia</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Ppto. y Suministro</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Obra</strong></td>
			<td class="auto-style5" style="width: 29px"><strong>
			Activo</strong></td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
		<?php
			$contador=0;
			$con = mysqli_query($conectar,"select * from modalidad where (mod_nombre LIKE REPLACE('%".$mod."%',' ','%') OR '".$mod."' IS NULL OR '".$mod."' = '') and (mod_sw_activo = '".$act."' OR '".$act."' IS NULL OR '".$act."' = '') order by mod_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$mod = $dato['mod_nombre'];
				$act = $dato['mod_sw_activo'];
				
				$inm = $dato['mod_sw_inmobiliaria'];
				$dinm = $dato['mod_dias_inmobiliaria'];
				$dis = $dato['mod_sw_diseno'];
				$ddis = $dato['mod_dias_diseno'];
				$lic = $dato['mod_sw_licencia'];
				$dlic = $dato['mod_dias_licencia'];
				$pptosum = $dato['mod_sw_pptosuministro'];
				$dpptosum = $dato['mod_dias_pptosuministro'];
				$obra = $dato['mod_sw_obra'];
				$dobra = $dato['mod_dias_obra'];
					
				$usuact = $dato['mod_usu_actualiz'];
				$fecact = $dato['mod_fec_actualiz'];
				$contador=$contador+1;
		?>
		<tr>
			<td class="auto-style3" style="width: 27px">
				<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<?php echo $contador;?>" value="<?php echo $dato['mod_clave_int'];?>" class="auto-style6" /></td>
			<td class="auto-style5" style="width: 200px"><?php echo $mod; ?></td>
				<td class="auto-style1" style="width: 200px" ><?php if($inm == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dinm; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" ><?php if($dis == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $ddis; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" ><?php if($lic == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dlic; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" >
				<?php if($pptosum == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dpptosum; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" >
				<?php if($obra == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dobra; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
			<td class="auto-style1" style="width: 30px">
			<input name="activarinactivar" id="activarinactivar" type="checkbox" <?php if($act == 1){ echo 'checked="checked"'; } ?> disabled="disabled" class="auto-style6" ></td>
			<td class="auto-style5">
			<?php
			if($metodo == 1)
			{
			?>
			<a data-reveal-id="editarmodalidad" data-animation="fade" style="cursor:pointer" onclick="EDITAR('<?php echo $dato['mod_clave_int']; ?>','MODALIDAD')"><img src="../../img/editar.png" alt="" height="22" width="21" /></a>
			<?php
			}
			?>
			</td>
		</tr>
		<?php
			}
		?>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 29px">&nbsp;</td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
	</table>
<?php
		exit();
	}
?>
<!DOCTYPE HTML>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

	
<title>CONTROL DE OBRAS</title>
<meta name="description" content="Service Desk">
<meta name="author" content="InvGate S.R.L.">

<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
<!--[if lte IE 7]>
<link rel="stylesheet" href="css/ie.c32bc18afe0e2883ee4912a51f86c119.css" type="text/css" />
<![endif]-->

<style type="text/css">
.auto-style1 {
	text-align: center;
}
.auto-style2 {
	font-size: 12px;
	font-family: Arial, helvetica;
	outline: none;
/*transition: all 0.75s ease-in-out;*/ /*-webkit-transition: all 0.75s ease-in-out;*/ /*-moz-transition: all 0.75s ease-in-out;*/border-radius: 3px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 3px;
	border: 1px solid rgba(0,0,0, 0.2);
	background-color: #eee;
	padding: 3px;
	box-shadow: 0 0 10px #aaa;
	-webkit-box-shadow: 0 0 10px #aaa;
	-moz-box-shadow: 0 0 10px #aaa;
	border: 1px solid #999;
	background-color: white;
	text-align: center;
}
.auto-style3 {
	text-align: left;
}
.inputs{
	font-size:14px;
    font-family: Arial, helvetica;
    outline:none;
    border-radius:3px;
    -webkit-border-radius:6px;
    -moz-border-radius:3px;
    border:1px solid rgba(0,0,0, 0.5);
    padding: 3px;
}
.validaciones{
	font-size:14px;
    font-family: Arial, helvetica;
    outline:none;
    border-radius:3px;
    -webkit-border-radius:6px;
    -moz-border-radius:3px;
    border:1px solid rgba(0,0,0, 0.2);
    color:maroon;
    background-color:#eee;
    padding: 3px;
    text-align:center;
}
.ok{
	font-size:14px;
    font-family: Arial, helvetica;
    outline:none;
    border-radius:3px;
    -webkit-border-radius:6px;
    -moz-border-radius:3px;
    border:1px solid rgba(0,0,0, 0.2);
    color:green;
    background-color:#eee;
    padding: 3px;
    text-align:center;
}
.auto-style5 {
	text-align: left;
	font-family: Arial, Helvetica, sans-serif;
}
.auto-style6 {
	font-family: Arial, Helvetica, sans-serif;
}
.auto-style7 {
	text-align: center;
	font-size: large;
}
.auto-style8 {
	text-align: center;
	font-family: Arial, Helvetica, sans-serif;
}
</style>

<script type="text/javascript" language="javascript">
	selecteds=0;
	
	function CheckUncheck(total,check){
		checkbox=null;
		for(i=1;i<=total;i++){
			checkbox=document.getElementById("idcat"+i);
			//alert(checkbox.value);
			checkbox.checked=check.checked;
		}
		
		if(check.checked){
			selecteds=total;
		}else{
			selecteds=0;
		}
		
	}
	
	function contadorVals(check){
		if(check.checked){
			selecteds=selecteds+1;
		}else{
			selecteds=selecteds-1;
		}
	}
	
	function selectedVals(){
		if(selecteds==0){
			alert("Seleccione al menos un registro.");
			return false;
		}else{
			return true;
		}
	}
	function ACTIVARDIAS1()
	{
		var inm = form1.inmobiliaria1.checked;
		var dis = form1.diseno1.checked;
		var lic = form1.licencia1.checked;
		var pptosum = form1.pptosum1.checked;
		var obra = form1.obra1.checked;
				
		if(inm == true){ form1.diasinmobiliaria1.disabled = false; }else{ form1.diasinmobiliaria1.disabled = true; form1.diasinmobiliaria1.value = 0; }
		if(dis == true){ form1.diasdiseno1.disabled = false; }else{ form1.diasdiseno1.disabled = true; form1.diasdiseno1.value = 0; }
		if(lic == true){ form1.diaslicencia1.disabled = false; }else{ form1.diaslicencia1.disabled = true; form1.diaslicencia1.value = 0; }
		if(pptosum == true){ form1.diaspptosum1.disabled = false; }else{ form1.diaspptosum1.disabled = true; form1.diaspptosum1.value = 0; }
		if(obra == true){ form1.diasobra1.disabled = false; }else{ form1.diasobra1.disabled = true; form1.diasobra1.value = 0; }
	}
	function ACTIVARDIAS()
	{
		var inm = form1.inmobiliaria.checked;
		var dis = form1.diseno.checked;
		var lic = form1.licencia.checked;
		var pptosum = form1.pptosum.checked;
		var obra = form1.obra.checked;
						
		if(inm == true){ form1.diasinmobiliaria.disabled = false; }else{ form1.diasinmobiliaria.disabled = true; form1.diasinmobiliaria.value = 0; }
		if(dis == true){ form1.diasdiseno.disabled = false; }else{ form1.diasdiseno.disabled = true; form1.diasdiseno.value = 0; }
		if(lic == true){ form1.diaslicencia.disabled = false; }else{ form1.diaslicencia.disabled = true; form1.diaslicencia.value = 0; }
		if(pptosum == true){ form1.diaspptosum.disabled = false; }else{ form1.diaspptosum.disabled = true; form1.diaspptosum.value = 0; }
		if(obra == true){ form1.diasobra.disabled = false; }else{ form1.diasobra.disabled = true; form1.diasobra.value = 0; }
	}
</script>
<?php //VALIDACIONES ?>
<script type="text/javascript" src="llamadas2.js"></script>
<script type="text/javascript" src="../../js/jquery-1.6.min.js"></script>

<?php //VENTANA EMERGENTE ?>
<link rel="stylesheet" href="../../css/reveal.css" />
<script type="text/javascript" src="../../js/jquery.reveal.js"></script>

</head>


<body>
<?php
$rows=mysqli_query($conectar,"select * from modalidad");
$total=mysqli_num_rows($rows);
?>
<form name="form1" id="form1" action="confirmar.php" method="post" onsubmit="return selectedVals();">
<!--[if lte IE 7]>
<div class="ieWarning">Este navegador no es compatible con el sistema. Por favor, use Chrome, Safari, Firefox o Internet Explorer 8 o superior.</div>
<![endif]-->
<table style="width: 100%">
	<tr>
		<td class="auto-style2" onclick="CONSULTAMODULO('TODOS')" onmouseover="this.style.backgroundColor='#445B74';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="width: 60px; cursor:pointer">
		Todos
		<?php
			$con = mysqli_query($conectar,"select COUNT(*) cant from modalidad");
			$dato = mysqli_fetch_array($con);
			echo $dato['cant'];
		?>
		</td>
		<td class="auto-style7" style="cursor:pointer" colspan="4">
		MAESTRA MODALIDAD</td>
		<td class="auto-style2" onmouseover="this.style.backgroundColor='#CCCCCC';this.style.color='#0F213C';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="width: 55px; cursor:pointer">
		<?php
		if($metodo == 1)
		{
		?>
		<a data-reveal-id="nuevamodalidad" data-animation="fade" style="cursor:pointer"><table style="width: 100%">
			<tr>
				<td style="width: 14px"><a data-reveal-id="nuevamodalidad" data-animation="fade" style="cursor:pointer"><img alt="" src="../../img/add2.png"></a></td>
				<td>Añadir</td>
			</tr>
		</table></a>
		<?php
		}
		?>
		</td>
	</tr>
	<tr>
		<td class="auto-style2" colspan="6">
		<div id="filtro">
			<table style="width: 33%">
				<tr>
					<td class="auto-style1"><strong>Filtro:<img src="../../img/buscar.png" alt="" height="18" width="15" /></strong></td>
					<td class="auto-style1">
			<input class="inputs" onkeyup="BUSCAR('MODALIDAD')" name="modalidad2" maxlength="70" type="text" placeholder="Modalidad" style="width: 150px" /></td>
					<td class="auto-style1">
					<strong>Activo:</strong></td>
					<td class="auto-style1">
					<select name="buscaractivos" class="inputs" onchange="BUSCAR('MODALIDAD')">
					<option value="">Todos</option>
					<option value="1">Activos</option>
					<option value="0">Inactivo</option>
					</select></td>
				</tr>
			</table>
		</div>
		</td>
	</tr>
	<tr>
		<td colspan="6" class="auto-style2">
		<div id="editarmodalidad" class="reveal-modal" style="left: 57%; top: 50px; height: 300px; width: 350px;">
			<table style="width: 38%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Nombre:</td>
			<td class="auto-style3"><input class="inputs" name="nombre1" maxlength="50" value="<?php echo $nom; ?>" type="text" style="width: 200px" />
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Activo:</td>
			<td class="auto-style3"><input class="inputs" <?php if($act == 1){ echo 'checked="checked"'; } ?> name="activo1" type="checkbox" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4">
			<input name="submit" type="button" value="Guardar" onclick="GUARDAR('MODALIDAD','<?php echo $ubiedi; ?>')"  style="width: 348px; height: 25px; cursor:pointer" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">
			<div id="datos">
			</div>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
			<a class="close-reveal-modal">&#215;</a>
		</div>
		<div id="modalidad">
		<table style="width: 80%">
			<tr>
				<td class="auto-style3" style="width: 27px">
					<input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<?php echo $total;?>,this);" />
				</td>
				<td class="auto-style3" colspan="8">
				<?php
				if($metodo == 1)
				{
				?>
				<table style="width: 30%">
					<tr>
						<td class="auto-style1"><p style="cursor:pointer"><img src="../../img/activo.png" alt="" /><input type="submit" value="Activar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" /></p></td>
						<td class="auto-style1"><p style="cursor:pointer"><img src="../../img/inactivo.png" alt="" /><input type="submit" value="Inactivar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" /></p></td>
						<td class="auto-style1"><p style="cursor:pointer"><img src="../../img/eliminar.png" alt="" /><input type="submit" value="Eliminar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" /></p></td>
					</tr>
				</table>
				<?php
				}
				?>
				</td>
			</tr>
			<tr>
				<td class="auto-style3" style="width: 27px">&nbsp;</td>
				<td class="auto-style3" style="width: 200px" ><strong>Modalidad</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Inmob.</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Diseño</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Licencia</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Ppto. y Suministro</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Obra</strong></td>
				<td class="auto-style3" style="width: 29px"><strong>
				Act.</strong></td>
				<td class="auto-style3">&nbsp;</td>
			</tr>
			<?php
				$contador=0;
				$con = mysqli_query($conectar,"select * from modalidad order by mod_nombre");
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$clamod = $dato['mod_clave_int'];
					$mod = $dato['mod_nombre'];
					$act = $dato['mod_sw_activo'];
					
					$inm = $dato['mod_sw_inmobiliaria'];
					$dinm = $dato['mod_dias_inmobiliaria'];
					$dis = $dato['mod_sw_diseno'];
					$ddis = $dato['mod_dias_diseno'];
					$lic = $dato['mod_sw_licencia'];
					$dlic = $dato['mod_dias_licencia'];
					$pptosum = $dato['mod_sw_pptosuministro'];
					$dpptosum = $dato['mod_dias_pptosuministro'];
					$obra = $dato['mod_sw_obra'];
					$dobra = $dato['mod_dias_obra'];
					$usuact = $dato['mod_usu_actualiz'];
					$fecact = $dato['mod_fec_actualiz'];
					$contador=$contador+1;
			?>
			<tr>
				<td class="auto-style3" style="width: 27px">
					<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<?php echo $contador;?>" value="<?php echo $dato['mod_clave_int'];?>" /></td>
				<td class="auto-style3" style="width: 200px" ><?php echo $mod; ?></td>
				<td class="auto-style1" style="width: 200px" >
				<?php if($inm == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dinm; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" /></td>
				<td class="auto-style1" style="width: 200px" >
				<?php if($dis == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $ddis; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" >
				<?php if($lic == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dlic; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" >
				<?php if($pptosum == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dpptosum ; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" >
				<?php if($obra == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dobra ; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 30px">
				<input name="activarinactivar" id="activarinactivar" type="checkbox" <?php if($act == 1){ echo 'checked="checked"'; } ?> disabled="disabled" ></td>
				<td class="auto-style3">
				<?php
				if($metodo == 1)
				{
				?>
				<a data-reveal-id="editarmodalidad" data-animation="fade" style="cursor:pointer" onclick="EDITAR('<?php echo $dato['mod_clave_int']; ?>','MODALIDAD')"><img src="../../img/editar.png" alt="" height="22" width="21" /></a>
				<?php
				}
				?>
				</td>
				<?php
					}
				?>
			</tr>
			<tr>
				<td class="auto-style3" style="width: 27px">&nbsp;</td>
				<td class="auto-style3" style="width: 200px" >&nbsp;</td>
				<td class="auto-style3" style="width: 200px" >&nbsp;</td>
				<td class="auto-style3" style="width: 200px" >&nbsp;</td>
				<td class="auto-style3" style="width: 200px" >&nbsp;</td>
				<td class="auto-style3" style="width: 200px" >&nbsp;</td>
				<td class="auto-style3" style="width: 200px" >&nbsp;</td>
				<td class="auto-style3" style="width: 29px">&nbsp;</td>
				<td class="auto-style3">&nbsp;</td>
			</tr>
		</table>
		</div>
<div id="nuevamodalidad" class="reveal-modal" style="left: 57%; top: 50px; height: 300px; width: 350px;">
	<table style="width: 38%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td>Modalidad:</td>
			<td>
			<input name="modalidad" id="modalidad"  type="text" style="width: 200px" class="inputs" />
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">
			<table style="width: 100%">
				<tr>
					<td>
					<table style="width: 100%">
						<tr>
							<td style="width: 10px"><input name="inmobiliaria" id="inmobiliaria" onclick="ACTIVARDIAS()" type="checkbox" /></td>
							<td style="width: 70px" class="auto-style3"><label for="inmobiliaria" style="cursor:pointer">Inmobiliaria</label></td>
							<td style="width: 30px"><input name="diasinmobiliaria" id="diasinmobiliaria" value="0" disabled="disabled" type="text" style="width: 36px" class="inputs" /></td>
							<td style="width: 10px"><input name="diseno" id="diseno" onclick="ACTIVARDIAS()" type="checkbox" /></td>
							<td style="width: 60px" class="auto-style3"><label for="diseno" style="cursor:pointer">Diseñador</label></td>
							<td><input name="diasdiseno" id="diasdiseno" value="0" disabled="disabled" type="text" style="width: 36px" class="inputs" /></td>
						</tr>
						<tr>
							<td style="width: 10px"><input name="licencia" id="licencia" onclick="ACTIVARDIAS()" type="checkbox" /></td>
							<td style="width: 70px" class="auto-style3"><label for="licencia" style="cursor:pointer">Licencia</label></td>
							<td style="width: 30px"><input name="diaslicencia" id="diaslicencia" value="0" disabled="disabled" type="text" style="width: 36px" class="inputs" /></td>
							<td style="width: 10px"><input name="pptosum" id="pptosum" onclick="ACTIVARDIAS()" type="checkbox" /></td>
							<td style="width: 60px" class="auto-style3"><label for="pptosum" style="cursor:pointer">
							Ppto. y Suministro</label></td>
							<td><input name="diaspptosum" id="diaspptosum" value="0" disabled="disabled" type="text" style="width: 36px" class="inputs" /></td>
						</tr>
						<tr>
							<td style="width: 10px"><input name="obra" id="obra" onclick="ACTIVARDIAS()" type="checkbox" /></td>
							<td style="width: 70px" class="auto-style3"><label for="obra" style="cursor:pointer">
							Obra</label></td>
							<td style="width: 30px"><input name="diasobra" id="diasobra" value="0" disabled="disabled" type="text" style="width: 36px" class="inputs" /></td>
							<td style="width: 10px">&nbsp;</td>
							<td style="width: 60px" class="auto-style3">&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						</table>
					</td>
				</tr>
				</table>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Activo: <input class="inputs" name="activo" checked="checked" type="checkbox" /></td>
			<td>
			&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4">
			<input name="nuevo1" type="button" value="Guardar" onclick="NUEVO('MODALIDAD')"  style="width: 348px; height: 25px; cursor:pointer" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">
			<div id="datos1">
			</div>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
</div>
		</td>
	</tr>
	<tr>
		<td style="width: 60px">&nbsp;</td>
		<td style="width: 117px">&nbsp;</td>
		<td style="width: 65px">&nbsp;</td>
		<td style="width: 70px">&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td style="width: 60px">&nbsp;</td>
		<td style="width: 117px">&nbsp;</td>
		<td style="width: 65px">&nbsp;</td>
		<td style="width: 70px">&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>
</form>
</body>
</html>