function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function EDITAR(v,m)
{	
	if(m == 'MODALIDAD')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('editarmodalidad').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#editarmodalidad").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?editarmod=si&modedi="+v,true);
		ajax.send(null);
	}
}
function GUARDAR(v,id)
{
	if(v == 'MODALIDAD')
	{
		var mod = form1.modalidad1.value;
		var lm = mod.length;
		var act = form1.activo1.checked;
		
		var inm = form1.inmobiliaria1.checked;
		var dinm = form1.diasinmobiliaria1.value;
		var dis = form1.diseno1.checked;
		var ddis = form1.diasdiseno1.value;
		var lic = form1.licencia1.checked;
		var dlic = form1.diaslicencia1.value;
		var inter = form1.interventor1.checked;
		var dinter = form1.diasinterventor1.value;
		var cons = form1.constructor1.checked;
		var dcons = form1.diasconstructor1.value;
		var pre = form1.presupuesto1.checked;
		var dpre = form1.diaspresupuesto1.value;
		var sum = form1.suministro1.checked;
		var dsum = form1.diassuministro1.value;
		var civ = form1.civil1.checked;
		var dciv = form1.diascivil1.value;
		var ele = form1.electromecanico1.checked;
		var dele = form1.diaselectromecanico1.value;
		var seg = form1.seguridad1.checked;
		var dseg = form1.diasseguridad1.value;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('datos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#datos").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?guardarmod=si&mod="+mod+"&act="+act+"&inm="+inm+"&dinm="+dinm+"&dis="+dis+"&ddis="+ddis+"&lic="+lic+"&dlic="+dlic+"&inter="+inter+"&dinter="+dinter+"&cons="+cons+"&dcons="+dcons+"&pre="+pre+"&dpre="+dpre+"&sum="+sum+"&dsum="+dsum+"&civ="+civ+"&dciv="+dciv+"&ele="+ele+"&dele="+dele+"&seg="+seg+"&dseg="+dseg+"&lm="+lm+"&m="+id,true);
		ajax.send(null);
		if(mod != '' && lm >= 3)
		{
			setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
		}
	}
}
function NUEVO(v)
{
	if(v == 'MODALIDAD')
	{
		var mod = form1.modalidad.value;
		var lm = mod.length;		
		var act = form1.activo.checked;
		var inm = form1.inmobiliaria.checked;
		var dinm = form1.diasinmobiliaria.value;
		var dis = form1.diseno.checked;
		var ddis = form1.diasdiseno.value;
		var lic = form1.licencia.checked;
		var dlic = form1.diaslicencia.value;
		var inter = form1.interventor.checked;
		var dinter = form1.diasinterventor.value;
		var cons = form1.constructor.checked;
		var dcons = form1.diasconstructor.value;
		var pre = form1.presupuesto.checked;
		var dpre = form1.diaspresupuesto.value;
		var sum = form1.suministro.checked;
		var dsum = form1.diassuministro.value;
		var civ = form1.civil.checked;
		var dciv = form1.diascivil.value;
		var ele = form1.electromecanico.checked;
		var dele = form1.diaselectromecanico.value;
		var seg = form1.seguridad.checked;
		var dseg = form1.diasseguridad.value;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('datos1').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#datos1").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?nuevamodalidad=si&mod="+mod+"&act="+act+"&inm="+inm+"&dinm="+dinm+"&dis="+dis+"&ddis="+ddis+"&lic="+lic+"&dlic="+dlic+"&inter="+inter+"&dinter="+dinter+"&cons="+cons+"&dcons="+dcons+"&pre="+pre+"&dpre="+dpre+"&sum="+sum+"&dsum="+dsum+"&civ="+civ+"&dciv="+dciv+"&ele="+ele+"&dele="+dele+"&seg="+seg+"&dseg="+dseg+"&lm="+lm,true);
		ajax.send(null);
		if(mod != '' && lm >= 3)
		{
			form1.modalidad.value = '';
			setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
		}
	}
}
function CONSULTAMODULO(v)
{
	if(v == 'TODOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modalidad').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modalidad").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?todos=si",true);
		ajax.send(null);
	}
}
function BUSCAR(m)
{	
	if(m == 'MODALIDAD')
	{
		var mod = form1.modalidad2.value;
		var act = form1.buscaractivos.value;
				
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modalidad').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modalidad").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?buscarmod=si&mod="+mod+"&act="+act,true);
		ajax.send(null);
	}
}