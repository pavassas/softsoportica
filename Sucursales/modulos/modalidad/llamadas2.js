function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function EDITAR(v,m)
{	
	if(m == 'MODALIDAD')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('editarmodalidad').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#editarmodalidad").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?editarmod=si&modedi="+v,true);
		ajax.send(null);
	}
}
function GUARDAR(v,id)
{
	if(v == 'MODALIDAD')
	{
		var mod = form1.modalidad1.value;
		var lm = mod.length;
		var act = form1.activo1.checked;
		
		var inm = form1.inmobiliaria1.checked;
		var dinm = form1.diasinmobiliaria1.value;
		var dis = form1.diseno1.checked;
		var ddis = form1.diasdiseno1.value;
		var lic = form1.licencia1.checked;
		var dlic = form1.diaslicencia1.value;
		var pptosum = form1.pptosum1.checked;
		var dpptosum = form1.diaspptosum1.value;
		var obra = form1.obra1.checked;
		var dobra = form1.diasobra1.value;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('datos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#datos").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?guardarmod=si&mod="+mod+"&act="+act+"&inm="+inm+"&dinm="+dinm+"&dis="+dis+"&ddis="+ddis+"&lic="+lic+"&dlic="+dlic+"&pptosum="+pptosum+"&dpptosum="+dpptosum+"&obra="+obra+"&dobra="+dobra+"&lm="+lm+"&m="+id,true);
		ajax.send(null);
		if(mod != '' && lm >= 3)
		{
			setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
		}
	}
}
function NUEVO(v)
{
	if(v == 'MODALIDAD')
	{
		var mod = form1.modalidad.value;
		var lm = mod.length;		
		var act = form1.activo.checked;
		var inm = form1.inmobiliaria.checked;
		var dinm = form1.diasinmobiliaria.value;
		var dis = form1.diseno.checked;
		var ddis = form1.diasdiseno.value;
		var lic = form1.licencia.checked;
		var dlic = form1.diaslicencia.value;
		var pptosum = form1.pptosum.checked;
		var dpptosum = form1.diaspptosum.value;
		var obra = form1.obra.checked;
		var dobra = form1.diasobra.value;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('datos1').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#datos1").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?nuevamodalidad=si&mod="+mod+"&act="+act+"&inm="+inm+"&dinm="+dinm+"&dis="+dis+"&ddis="+ddis+"&lic="+lic+"&dlic="+dlic+"&pptosum="+pptosum+"&dpptosum="+dpptosum+"&obra="+obra+"&dobra="+dobra+"&lm="+lm,true);
		ajax.send(null);
		if(mod != '' && lm >= 3)
		{
			form1.modalidad.value = '';
			setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
		}
	}
}
function CONSULTAMODULO(v)
{
	if(v == 'TODOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modalidad').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modalidad").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?todos=si",true);
		ajax.send(null);
	}
}
function BUSCAR(m)
{	
	if(m == 'MODALIDAD')
	{
		var mod = form1.modalidad2.value;
		var act = form1.buscaractivos.value;
				
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modalidad').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modalidad").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?buscarmod=si&mod="+mod+"&act="+act,true);
		ajax.send(null);
	}
}