function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function MODULO(v,e)
{
    var ale =  document.getElementById('swregistro').value;
    var ire = 0;
    if (ale == 1 || ale == 2) {
        var conf = confirm("¿Quieres salir de este sitio web?\n\nEs posible que los cambios no se guarden.")
        if(conf){
            ire = 1;
        }
        else
        {
            ire = 0;
        }
    }
    else
    {
        ire = 1;
    }
    if(ire==1) {
        if (v == 'DISENO') {
            window.location.href = "diseno.php";
        }
        else if (v == 'TODOS') {
            var ajax;
            ajax = new ajaxFunction();
            ajax.onreadystatechange = function () {
                if (ajax.readyState == 4) {
                    document.getElementById('cajeros').innerHTML = ajax.responseText;
                }
            }
            jQuery("#cajeros").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
            ajax.open("GET", "?todos=si&est=" + e, true);
            ajax.send(null);
            setTimeout("REFRESCARLISTAS()", 500);
           // OCULTARSCROLL();
        }
    }
}
function BUSCAR(p)
{	
	var consec = form1.busconsecutivo.value;
	var nom = form1.busnombre.value;
	var anocon = form1.busanocontable.value;
	var cod = form1.buscodigo.value;
	var reg = form1.busregion.value;
	var mun = form1.busmunicipio.value;
	var tipint = form1.bustipointervencion.value;
	var moda = form1.busmodalidad.value;
	var actor = form1.busactor.value;
	var est = form1.ocultoestado.value;
	var pro = form1.busproyecto.value;
	var estpro = form1.estadosucursal.value;
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('busqueda').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#busqueda").html("<img alt='cargando' src='img/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	if(p > 0)
	{
		ajax.open("GET","?buscar=si&consec="+consec+"&nom="+nom+"&anocon="+anocon+"&cod="+cod+"&reg="+reg+"&mun="+mun+"&tipint="+tipint+"&moda="+moda+"&est="+est+"&actor="+actor+"&pro="+pro+"&estpro="+estpro+"&page="+p,true);
	}
	else
	{
		ajax.open("GET","?buscar=si&consec="+consec+"&nom="+nom+"&anocon="+anocon+"&cod="+cod+"&reg="+reg+"&mun="+mun+"&tipint="+tipint+"&moda="+moda+"&est="+est+"&actor="+actor+"&pro="+pro+"&estpro="+estpro,true);
	}		
	ajax.send(null);
	//OCULTARSCROLL();
}
function VERREGISTRO(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('cajeros').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#cajeros").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?verregistro=si&clasuc="+v,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTAESTADO()",500);
	OCULTARSCROLL();
}
function AGREGARNOTA(c,t)
{
	var nota = "";
	if(t==1)
	{ 
	var	not = $('#notaestado');
	}
	else if(t==2)
	{
	var	not = $('#notaesquema');
	}
	else if(t==3)
	{
	var	not = $('#notapowerpoint');
	}
	else if(t==4)
	{
	var	not = $('#notared');
	}
	else if(t==5)
	{
	var	not = $('#notaentregadiseno');
	}
	else if(t==6)
	{
	var	not = $('#notacambios');
	}
	nota = not.val();
	var nuevanota = CORREGIRTEXTO(nota);
   
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('agregados'+t).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#agregados"+t).html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?agregarnota=si&not="+nuevanota+"&suc="+c+"&tin="+t,true);
	ajax.send(null);
	not.val('');
	
}
function CORREGIRTEXTO(v)
{
	var res = v.replace('#','REEMPLAZARNUMERAL');
	var res = res.replace('+','REEMPLAZARMAS');
	
	return res;
}
function EDITARNOTA(c,cn)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('editarnota').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#editarnota").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?editarnota=si&clanot="+cn+"&suc="+c,true);
	ajax.send(null);
}
function ACTUALIZARNOTA(n,c)
{
	var not = $('#notaedi').val();
	var nuevanota = CORREGIRTEXTO(not);
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todaslasnotas').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todaslasnotas").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?actualizarnota=si&not="+nuevanota+"&clanot="+n+"&suc="+c,true);
	ajax.send(null);
}
function ELIMINARNOTA(c,suc)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todaslasnotas').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todaslasnotas").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?eliminarnota=si&clanot="+c+"&suc="+suc,true);
	ajax.send(null);
}
function MOSTRARNOTAS(c)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todaslasnotas').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todaslasnotas").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarnotas=si&clasuc="+c,true);
	ajax.send(null);
}
function AVISO()
{
	alert("Porfavor seleccione un cajero para agregar notas");
}
function ACTUALIZAR(v)
{
	var disa = $('#disarquitectonico').val();
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('fecenvio').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#fecenvio").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?actualizardatos=si&disa="+disa+"&suc="+v,true);
	ajax.send(null);	
}
function ACTUALIZARFECESQ(v)
{
	var ape = $('#ape').val();
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('divfecesq').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#divfecesq").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?actualizarfecesq=si&ape="+ape+"&suc="+v,true);
	ajax.send(null);	
}
function ACTUALIZARFECDIS(v)
{
	var aed = $('#aed').val();
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('fecentdis').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#fecentdis").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?actualizarentdis=si&aed="+aed+"&suc="+v,true);
	ajax.send(null);	
}
function ACTUALIZARFECTALL(v)
{
	var ftd = $('#ftd').val();
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('divfecpow').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#divfecpow").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?actualizarfectal=si&ftd="+ftd+"&suc="+v,true);
	ajax.send(null);	
}
function ACTUALIZARFTP(v)
{
	var far = $('#far').val();
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('divfecftp').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#divfecftp").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?actualizarfecftp=si&far="+far+"&suc="+v,true);
	ajax.send(null);	
}
function ACTUALIZARESQ(suc)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('entesqreal').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#entesqreal").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?actualizaresq=si&suc="+suc,true);
	ajax.send(null);
	
}
function GUARDAR(v)
{
	var fir = $('#fir').val();
	var ffr = $('#ffr').val();
	var disarquitectonico = $('#disarquitectonico').val();
	var est = $('#estadodis').val();
	var ape = $('#ape').val();
	var ftd = $('#ftd').val();
	var app = $('#app').val();
	var far = $('#far').val();
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('estadoregistro').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#estadoregistro").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?guardardatos=si&est="+est+"&ape="+ape+"&ftd="+ftd+"&app="+app+"&far="+far+"&fir="+fir+"&ffr="+ffr+"&suc="+v,true);
	ajax.send(null);
	
	setTimeout("REFRESCARESTADO('1')",500);
	setTimeout("REFRESCARESTADO('2')",500);
	setTimeout("REFRESCARESTADO('3')",500);
	setTimeout("REFRESCARESTADO('4')",500);
	setTimeout("REFRESCARESTADO('5')",500);
	setTimeout("REFRESCARESTADO('6')",500);
	setTimeout("REFRESCARESTADO('')",500);
}
function REFRESCARESTADO(v)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
	    	document.getElementById('estado'+v).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#estado"+v).html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?refrescarestado=si&est="+v,true);
	ajax.send(null);
}
function RESULTADOADJUNTO(v,cs)
{
	if(v == 'INFORME')
	{
		var div = 'resultadoadjuntoinf';
	}
	else
	if(v == 'ESQUEMA')
	{
		var div = 'resultadoadjuntoesq';
	}
	else
	if(v == 'POWERPOINT')
	{
		var div = 'resultadoadjuntopow';
	}
	else
	if(v == 'RETROALIMENTACION')
	{
		var div = 'resultadoadjuntoret';
	}
	else
	if(v == 'CALIFICACION')
	{
		var div = 'resultadoadjuntocal';
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById(div).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#"+div).html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?estadoadjunto=si&actor="+v+"&clasuc="+cs,true);
	ajax.send(null);
	OCULTARSCROLL();
}
function MOSTRARRUTA(v)
{
	if(v == 'INFORME')
	{
		var div = 'resultadoadjuntoinf';
		var nomadj = form1.adjuntoinf.value;
	}
	else
	if(v == 'ESQUEMA')
	{
		var div = 'resultadoadjuntoesq';
		var nomadj = form1.adjuntoesq.value;
	}
	else
	if(v == 'POWERPOINT')
	{
		var div = 'resultadoadjuntopow';
		var nomadj = form1.adjuntopow.value;
	}
	else
	if(v == 'RETROALIMENTACION')
	{
		var div = 'resultadoadjuntoret';
		var nomadj = form1.adjuntoret.value;
	}
	else
	if(v == 'CALIFICACION')
	{
		var div = 'resultadoadjuntocal';
		var nomadj = form1.adjuntocal.value;
	}
	else
	if(v == 'FACTURACION')
	{
		var div = 'resultadoadjuntofac';
		var nomadj = form1.adjuntofac.value;
	}
	else
	if(v == 'ADJUNTO')
	{
		var div = 'resultadoadjunto';
		var nomadj = $('#archivoadjunto').val();
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById(div).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#"+div).html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarruta=si&nomadj="+nomadj,true);
	ajax.send(null);
	OCULTARSCROLL();
}
function CALCULARTOTALDIAS()
{
	var fi = $('#fir').val();
	var ff = $('#ffr').val();
	var d = restaFechas(fi,ff);
	if(parseInt(d) >= 0){ $('#diasr').val(d); }
}
function restaFechas(f1,f2)
{
	var aFecha1 = f1.split('-'); 
	var aFecha2 = f2.split('-'); 
	var fFecha1 = Date.UTC(aFecha1[0],aFecha1[1]-1,aFecha1[2]); 
	var fFecha2 = Date.UTC(aFecha2[0],aFecha2[1]-1,aFecha2[2]); 
	var dif = fFecha2 - fFecha1;
	var dias = Math.floor(dif / (1000 * 60 * 60 * 24)); 
	return dias;
}
function MOSTRARBITACORA(c)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todaslasnotas').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todaslasnotas").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarbitacora=si&clasuc="+c,true);
	ajax.send(null);
}
function MOSTRARBITACORAADJUNTOS(c)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todoslosadjuntosbitacora').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todoslosadjuntosbitacora").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarbitacoraadjuntos=si&clasuc="+c,true);
	ajax.send(null);
}
function GUARDARCALIFICACION(n)
{
    var pos = $('#pos'+n).val();
    var ajax;
    ajax=new ajaxFunction();
    ajax.onreadystatechange=function()
    {
        if(ajax.readyState==4)
        {
            document.getElementById('msnnota').innerHTML=ajax.responseText;
        }
    }
    jQuery("#msnnota").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
    ajax.open("GET","?guardarcalificacion=si&clanot="+n+"&cal=" + pos,true);
    ajax.send(null);

}
function MOSTRARADJUNTOS(c,a)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('adjuntoarchivos').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#adjuntoarchivos").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostraradjuntos=si&clasuc="+c+"&actor="+a,true);
	ajax.send(null);
}
function EDITARADJUNTO(c,a)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('editaradjunto').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#editaradjunto").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?editaradjunto=si&claada="+c+"&act="+a,true);
	ajax.send(null);
}
function RESULTADOADJUNTOINFORMACION(ca)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('resultadoadjunto').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#resultadoadjunto").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?estadoadjuntoinformacion=si&claada="+ca,true);
	ajax.send(null);
}
function ELIMINARADJUNTO(v)
{
	if(confirm('Esta seguro/a de Eliminar este archivo?'))
	{
		var dataString = 'id='+v;
		
		$.ajax({
	        type: "POST",
	        url: "deletearchivo.php",
	        data: dataString,
	        success: function() {
				//$('#delete-ok').empty();
				//$('#delete-ok').append('<div class="correcto">Se ha eliminado correctamente la entrada con id='+v+'.</div>').fadeIn("slow");
				$('#service'+v).fadeOut("slow");
				//$('#'+v).remove();
	        }
	    });
	}
}
function VERMODALIDADES1(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('modalidades1').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#modalidades1").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?vermodalidades1=si&tii="+v,true);
	ajax.send(null);
	//setTimeout("REFRESCARLISTAMODALIDADES1()",800);
}