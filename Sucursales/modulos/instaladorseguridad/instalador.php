<?php
	error_reporting(0);
	include('../../data/Conexion.php');
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_SESSION['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];
		
	// verifica si no se ha loggeado
	if(!isset($_SESSION["persona"]))
	{
	  session_destroy();
	  header("LOCATION:index.php");
	}else{
	}
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");
	$anocont = date("Y");
	require_once("lib/Zebra_Pagination.php");
	
	$con = mysqli_query($conectar,"select u.usu_clave_int,p.prf_clave_int,p.prf_descripcion,u.usu_sw_inmobiliaria,u.usu_sw_visita,u.usu_sw_diseno,u.usu_sw_licencia,u.usu_sw_interventoria,u.usu_sw_constructor,u.usu_sw_seguridad,u.usu_sw_otro from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
	$dato = mysqli_fetch_array($con);
	$clausu = $dato['usu_clave_int'];
	$claprf = $dato['prf_clave_int'];
	$perfil = $dato['prf_descripcion'];
	
	$swinm = $dato['usu_sw_inmobiliaria'];
	$swvis = $dato['usu_sw_visita'];
	$swdis = $dato['usu_sw_diseno'];
	$swlic = $dato['usu_sw_licencia'];
	$swint = $dato['usu_sw_interventoria'];
	$swcon = $dato['usu_sw_constructor'];
	$swseg = $dato['usu_sw_seguridad'];
	$swotr = $dato['usu_sw_otro'];
	
	$mostrarcajeros = 0;
	if($swinm == 0 and $swvis == 0 and $swdis == 0 and $swlic == 0 and $swint == 0 and $swcon == 0 and $swseg == 0 and $swotr == 1){ $mostrarcajeros = 1; }
	
	$con = mysqli_query($conectar,"select per_metodo from permiso where prf_clave_int = '".$claprf."' and ven_clave_int = 10");
	$dato = mysqli_fetch_array($con);
	$metodo = $dato['per_metodo'];
	
	if($_GET['guardardatos'] == 'si')
	{
		$fecha=date("Y/m/d H:i:s");
		$caj = $_GET['caj'];
		$fecingobr = $_GET['fecingobr'];
		$codmon = $_GET['codmon'];
		
		$conpad = mysqli_query($conectar,"select caj_padre,(select c.caj_nombre from cajero c where c.caj_clave_int = cajero.caj_padre) nomcaj from cajero where caj_clave_int = ".$caj."");
		$datopad = mysqli_fetch_array($conpad);
		$mipad = $datopad['caj_padre'];
		$nomcaj = $datopad['nomcaj'];
		
		if($mipad != 0 and $mipad != '')
		{
			echo "<div class='validaciones'>No puede editar porque Este cajero es hijo del cajero: $mipad - $nomcaj</div>";
		}
		else
		{
			$con = mysqli_query($conectar,"update cajero_seguridad set cas_fecha_ingreso_obra_inst = '".$fecingobr."', cas_codigo_monitoreo = '".$codmon."' where caj_clave_int = '".$caj."'");
			
			if($con > 0)
			{
				echo "<div class='ok' style='width: 100%' align='center'>Datos grabados correctamente</div>";
				$con = mysqli_query($conectar,"select caj_clave_int from cajero where caj_padre = ".$caj."");
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$clahij = $dato['caj_clave_int'];
					$con1 = mysqli_query($conectar,"update cajero_seguridad set cas_fecha_ingreso_obra_inst = '".$fecingobr."', cas_codigo_monitoreo = '".$codmon."' where caj_clave_int = '".$clahij."'");
				}
				
				mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'10',3,'".$caj."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 3=Actualización cajero
			}
			else
			{
				echo "<div class='validaciones' style='width: 100%' align='center'>No se han podido guardar los datos, porfavor intentelo nuevamente</div>";
			}
		}
		exit();
	}
	if($_GET['todos'] == 'si')
	{
		$est = $_GET['est'];
?>
	<table>
	<tr>
		<td align="left" class="auto-style2">
		<table style="width: 50%">
				<tr>
					<td style="width: 60px" rowspan="2"><strong>
					<span class="auto-style13">Filtro:</span><img src="../../img/buscar.png" alt="" height="18" width="15" /></strong></td>
					<td style="width: 150px; height: 31px; display:none">
					<input class="inputs" onkeyup="BUSCAR('CAJERO','')" name="busconsecutivo" id="busconsecutivo" maxlength="70" type="text" placeholder="Consecutivo" style="width: 150px" />
					</td>
					<td style="width: 150px; height: 31px;">
					<input class="inputs" onkeyup="BUSCAR('CAJERO','')" name="busnombre" id="busnombre" maxlength="70" type="text" placeholder="Nombre Cajero" style="width: 150px" />
					</td>
					<td style="width: 150px; height: 31px;">
					<input class="inputs" onkeyup="BUSCAR('CAJERO','')" name="busanocontable" id="busanocontable" maxlength="70" type="text" placeholder="Año Contable" style="width: 150px" />
					</td>
					<td style="width: 150px; height: 31px;" class="alinearizq">
					<select name="buscentrocostos" id="buscentrocostos" onchange="BUSCAR('CAJERO','')" tabindex="4" class="inputs" style="width: 158px">
					<option value="">-Centro Costos-</option>
					<?php
						$con = mysqli_query($conectar,"select * from centrocostos where cco_sw_activo = 1 order by cco_nombre");
						$num = mysqli_num_rows($con);
						for($i = 0; $i < $num; $i++)
						{
							$dato = mysqli_fetch_array($con);
							$clave = $dato['cco_clave_int'];
							$nombre = $dato['cco_nombre'];
					?>
						<option value="<?php echo $clave; ?>" <?php if($clave == $cencos){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
					<?php
						}
					?>
					</select>
					</td>
					<td align="left" style="height: 31px">
					<input class="auto-style5" onkeyup="BUSCAR('CAJERO','')" name="buscodigo" id="buscodigo" maxlength="70" type="text" placeholder="Código Cajero" style="width: 150px" />
					</td>
                    <td class="alinearizq">
                        <select name="busproyecto" id="busproyecto" onchange="BUSCAR('CAJERO','')" tabindex="6" class="inputs" style="width: 230px">
                            <option value="">-Proyecto-</option>
                            <?php
                            $con = mysqli_query($conectar,"select * from proyecto where pro_sw_activo = 1 order by pro_nombre");
                            $num = mysqli_num_rows($con);
                            for($i = 0; $i < $num; $i++)
                            {
                                $dato = mysqli_fetch_array($con);
                                $clave = $dato['pro_clave_int'];
                                $nombre = $dato['pro_nombre'];
                                ?>
                                <option value="<?php echo $clave; ?>" <?php if($clave == $proyec){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
                                <?php
                            }
                            ?>
                        </select>

                    </td>
				</tr>
				<tr>
					<td style="width: 150px; height: 31px;">
					<select name="busregion" id="busregion" onchange="BUSCAR('CAJERO','')" tabindex="6" class="inputs" style="width: 148px">
						<option value="">-Región-</option>
						<?php
							$con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo IS NULL and pog_nieto IS NULL order by pog_nombre");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$clave = $dato['pog_clave_int'];
								$nombre = $dato['pog_nombre'];
						?>
							<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
						<?php
							}
						?>
						</select></td>
					<td style="width: 150px; height: 31px;">
						<select name="busmunicipio" id="busmunicipio" onchange="BUSCAR('CAJERO','')" tabindex="8" class="inputs" style="width: 148px">
						<option value="">-Municipio-</option>
						<?php
							$con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo IS NULL and pog_nieto IS NOT NULL order by pog_nombre");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$clave = $dato['pog_clave_int'];
								$nombre = $dato['pog_nombre'];
						?>
							<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
						<?php
							}
						?>
						</select></td>
					<td style="width: 150px; height: 31px;">
						<select name="bustipologia" id="bustipologia" onchange="BUSCAR('CAJERO','')" tabindex="9" class="inputs" style="width: 148px">
						<option value="">-Tipología-</option>
						<?php
							$con = mysqli_query($conectar,"select * from tipologia where tip_sw_activo = 1 order by tip_nombre");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$clave = $dato['tip_clave_int'];
								$nombre = $dato['tip_nombre'];
						?>
							<option value="<?php echo $clave; ?>" <?php if($clave == $tip){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
						<?php
							}
						?>
						</select></td>
					<td style="width: 150px; height: 31px;" class="alinearizq">
					<select name="bustipointervencion" id="bustipointervencion" onchange="BUSCAR('CAJERO','');VERMODALIDADES1(this.value)" tabindex="10" class="inputs" style="width: 158px">
					<option value="">Tipo Intervención</option>
					<?php
						$con = mysqli_query($conectar,"select * from tipointervencion order by tii_nombre");
						$num = mysqli_num_rows($con);
						for($i = 0; $i < $num; $i++)
						{
							$dato = mysqli_fetch_array($con);
							$clave = $dato['tii_clave_int'];
							$nombre = $dato['tii_nombre'];
					?>
						<option value="<?php echo $clave; ?>" <?php if($clave == $tipint){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
					<?php
						}
					?>
					</select></td>
					<td align="left" style="height: 31px">
					<div id="modalidades1" style="float:left">
					<select name="busmodalidad" id="busmodalidad" onchange="BUSCAR('CAJERO','')" tabindex="8" class="inputs" data-placeholder="-Seleccione-" style="width: 160px">
					<option value="">-Modalidad-</option>
					<?php
						$con = mysqli_query($conectar,"select * from modalidad order by mod_nombre");
						$num = mysqli_num_rows($con);
						for($i = 0; $i < $num; $i++)
						{
							$dato = mysqli_fetch_array($con);
							$clave = $dato['mod_clave_int'];
							$nombre = $dato['mod_nombre'];
					?>
						<option value="<?php echo $clave; ?>" <?php if($moda == $clave){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
					<?php
						}
					?>
					</select>
					</div>
					</td>
					<td align="left" style="height: 31px">
					<select name="busactor" id="busactor" onchange="BUSCAR('CAJERO','')" tabindex="20" class="inputs" style="width: 150px">
					<option value="">-Actor-</option>
					<?php
						if($claprf == 1)
						{
							$con = mysqli_query($conectar,"select * from usuario where usu_sw_inmobiliaria = 1 or usu_sw_visita = 1 or usu_sw_diseno = 1 or usu_sw_licencia = 1 or usu_sw_interventoria = 1 or usu_sw_constructor = 1 or usu_sw_seguridad = 1 order by usu_nombre");
						}
						else
						{
							$con = mysqli_query($conectar,"select * from usuario where (usu_categoria = 2 and usu_sw_seguridad = 1 and usu_sw_activo = 1) OR (usu_clave_int NOT IN (select usu_clave_int from usuario_actor) and usu_sw_seguridad = 1 and usu_sw_activo = 1) order by usu_nombre");
						}
						$num = mysqli_num_rows($con);
						for($i = 0; $i < $num; $i++)
						{
							$dato = mysqli_fetch_array($con);
							$clave = $dato['usu_clave_int'];
							$nombre = $dato['usu_nombre'];
					?>
						<option value="<?php echo $clave; ?>" <?php if($clave == $inmob){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
					<?php
						}
					?>
					</select>
					</td>
				</tr>
				</table>
		</td>
	</tr>
	<tr>
		<td class="auto-style2">
		<div id="busqueda" style="overflow:auto;width: 1226px">
		<table style="width: 100%">
		<tr>
			<td class="alinearizq">&nbsp;</td>
			<td class="alinearizq"><strong>Consecutivo</strong></td>
			<td class="alinearizq"><strong>Código Cajero</strong></td>
			<td class="alinearizq"><strong>Nombre</strong></td>
			<td class="alinearizq"><strong>Padre/Hijo</strong></td>
			<td class="alinearizq"><strong>Región</strong></td>
			<td class="alinearizq"><strong>Tipología</strong></td>
			<td class="alinearizq"><strong>Tip. Intervención</strong></td>
			<td class="alinearizq"><strong>Estado</strong></td>
		</tr>
		<?php
		if($claprf == 1 || $mostrarcajeros == 1)
		{
			$query = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int)  inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where c.caj_estado_proyecto = '".$est."' and c.caj_sw_eliminado = 0");
			//$res = $con->query($query);
			$num_registros = mysqli_num_rows($query);
	
			$resul_x_pagina = 50;
			//Paginar:
			$paginacion = new Zebra_Pagination();
			$paginacion->records($num_registros);
			$paginacion->records_per_page($resul_x_pagina);
			
			$con = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int)  inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where c.caj_estado_proyecto = '".$est."' and c.caj_sw_eliminado = 0 LIMIT ".(($paginacion->get_page() - 1) * $resul_x_pagina). ',' .$resul_x_pagina);
		}
		else
		{
			$query = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int)  inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where c.caj_estado_proyecto = '".$est."' and (cajseg.cas_instalador = '".$clausu."' OR cajseg.cas_instalador IN (select usa_actor from usuario_actor where usu_clave_int = '".$clausu."')) and c.caj_sw_eliminado = 0");
			//$res = $con->query($query);
			$num_registros = mysqli_num_rows($query);
	
			$resul_x_pagina = 50;
			//Paginar:
			$paginacion = new Zebra_Pagination();
			$paginacion->records($num_registros);
			$paginacion->records_per_page($resul_x_pagina);
			
			$con = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int)  inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where c.caj_estado_proyecto = '".$est."' and (cajseg.cas_instalador = '".$clausu."' OR cajseg.cas_instalador IN (select usa_actor from usuario_actor where usu_clave_int = '".$clausu."')) and c.caj_sw_eliminado = 0 LIMIT ".(($paginacion->get_page() - 1) * $resul_x_pagina). ',' .$resul_x_pagina);
		}		
		$num = mysqli_num_rows($con);
		for($i = 0; $i < $num; $i++)
		{
			$dato = mysqli_fetch_array($con);
			//Info Base
			$clacaj = $dato['clacaj'];
			$nomcaj = $dato['caj_nombre'];
			$dir = $dato['caj_direccion'];
			$anocon = $dato['caj_ano_contable'];
			$reg = $dato['caj_region'];
			$dep = $dato['caj_departamento'];
			$mun = $dato['caj_municipio'];
			$tip = $dato['tip_clave_int'];
			$tipint = $dato['tii_clave_int'];
			$mod = $dato['mod_clave_int'];
			$conmod = mysqli_query($conectar,"select mod_nombre from modalidad where mod_clave_int = '".$mod."'");
			$datomod = mysqli_fetch_array($conmod);
			$moda = $datomod['mod_nombre'];
			//Info Secun
			$codcaj = $dato['caj_codigo_cajero'];
			$cencos = $dato['cco_clave_int'];
			$refmaq = $dato['rem_clave_int'];
			$ubi = $dato['ubi_clave_int'];
			$codsuc = $dato['caj_codigo_suc'];
			$ubiamt = $dato['caj_ubicacion_atm'];
			$ati = $dato['ati_clave_int'];
			$rie = $dato['caj_riesgo'];
			$are = $dato['are_clave_int'];
			$codrec = $dato['caj_cod_recibido_monto'];
			$codrec = $dato['caj_fecha_creacion'];
			//Info Inmobiliaria
			$reqinm = $dato['cai_req_inmobiliaria'];
			$inmob = $dato['cai_inmobiliaria'];
			$feciniinmob = $dato['cai_fecha_ini_inmobiliaria'];
			$estinmob = $dato['esi_clave_int'];
			$fecentinmob = $dato['cai_fecha_entrega_info_inmobiliaria'];
			//Info Visita Local
			$reqvis = $dato['cav_req_visita_local'];
			$vis = $dato['cav_visitante'];
			$fecvis = $dato['cav_fecha_visita'];
			//Info Diseño
			$reqdis = $dato['cad_req_diseno'];
			$dis = $dato['cad_disenador'];
			$fecinidis = $dato['cad_fecha_inicio_diseno'];
			$estdis = $dato['esd_clave_int'];
			$fecentdis = $dato['cad_fecha_entrega_info_diseno'];
			//Info Licencia
			$reqlic = $dato['cal_req_licencia'];
			$lic = $dato['cal_gestionador'];
			$fecinilic = $dato['cal_fecha_inicio_licencia'];
			$estlic = $dato['esl_clave_int'];
			$fecentlic = $dato['cal_fecha_entrega_info_licencia'];
			//Info Interventoria
			$int = $dato['cin_interventor'];
			$fecteoent = $dato['cin_fecha_teorica_entrega'];
			$feciniobra = $dato['cin_fecha_inicio_obra'];
			$fecpedsum = $dato['cin_fecha_pedido_suministro'];
			$aprovcotiz = $dato['cin_sw_aprov_cotizacion'];
			$cancom = $dato['can_clave_int'];
			$aprovliqui = $dato['cin_sw_aprov_liquidacion'];
			$swimgnex = $dato['cin_sw_img_nexos'];
			$swfot = $dato['cin_sw_fotos'];
			$swact = $dato['cin_sw_actas'];
			$estpro = $dato['caj_estado_proyecto'];
			//Info Constructor
			$cons = $dato['cac_constructor'];
			$cons = $dato['cac_fecha_entrega_atm'];
			$cons = $dato['cac_porcentaje_avance'];
			$cons = $dato['cac_fecha_entrega_cotizacion'];
			$cons = $dato['cac_fecha_entrega_liquidacion'];
			//Info Instalador Seguridad
			$seg = $dato['cas_instalador'];
			$fecingseg = $dato['cas_fecha_ingreso_obra_inst'];
			$codmon = $dato['cas_codigo_monitoreo'];
			
			$swpad = $dato['caj_sw_padre'];
			$hijo = $dato['caj_padre'];
		?>
		<tr <?php if($i % 2 == 0){ echo 'style="background-color:#eeeeee;"'; } ?>>
			<td class="alinearizq"><img src="../../img/editar.png" onclick="VERREGISTRO('<?php echo $clacaj; ?>')" alt="" height="25" width="25" style="cursor:pointer" /></td>
			<td class="alinearizq"><?php echo $clacaj; ?></td>
			<td class="alinearizq"><?php echo $codcaj; ?></td>
			<td class="alinearizq"><?php echo $nomcaj; ?></td>
			<td class="alinearizq">
			<?php
			if($swpad == 1 and ($hijo == 0 or $hijo == ''))
			{
				$conhij = mysqli_query($conectar,"select caj_clave_int,caj_nombre from cajero where caj_padre = ".$clacaj."");
				$numhij = mysqli_num_rows($conhij);
			?>
				<div style="cursor:pointer" onmouseover="javascript: VentanaFlotante('<?php for($j = 0; $j < $numhij; $j++){ $datohij = mysqli_fetch_array($conhij); echo $datohij['caj_clave_int']." - ".$datohij['caj_nombre']."<br>"; } ?>', 300, <?php echo 35*$numhij; ?>)" onmouseout="javascript: quitarDiv();">Padre</div>
			<?php
			}
			else
			if($hijo > 0)
			{
				$conpad = mysqli_query($conectar,"select caj_clave_int,caj_nombre from cajero where caj_clave_int = ".$hijo."");
				$datopad = mysqli_fetch_array($conpad);
			?>
				<div style="cursor:pointer" onmouseover="javascript: VentanaFlotante('<?php echo $datopad['caj_clave_int']." - ".$datopad['caj_nombre']."<br>"; ?>', 200, 35)" onmouseout="javascript: quitarDiv();">Hijo</div>
			<?php
			}
			else
			{
				echo "<div>Ninguno</div>";
			}
			?>
			</td>
			<td class="alinearizq">
			<?php 
				$sql = mysqli_query($conectar,"select pog_nombre from posicion_geografica where pog_clave_int = '".$reg."'");
				$datosql = mysqli_fetch_array($sql);
				$nomreg = $datosql['pog_nombre'];
				echo $nomreg; 
			?>
			</td>

			<td class="alinearizq">
			<?php 
				$sql = mysqli_query($conectar,"select tip_nombre from tipologia where tip_clave_int = '".$tip."'");
				$datosql = mysqli_fetch_array($sql);
				$nomtip = $datosql['tip_nombre'];
				echo $nomtip;
			?>
			</td>
			<td class="alinearizq">
			<?php 
				$sql = mysqli_query($conectar,"select tii_nombre from tipointervencion where tii_clave_int = '".$tipint."'");
				$datosql = mysqli_fetch_array($sql);
				$nomtii = $datosql['tii_nombre'];
				echo $nomtii;
			?>
			</td>

			<td class="alinearizq"><?php if($estpro == 1){ echo "ACTIVO"; }elseif($estpro == 2){ echo "SUSPENDIDO"; }elseif($estpro == 3){ echo "ENTREGADO"; }elseif($estpro == 4){ echo "CANCELADO"; }elseif($estpro == 5){ echo "PROGRAMADO"; } ?></td>
		</tr>
		<?php
		}
		?>
		</table>
		<?php $paginacion->render(); ?>
		</div>
	</td>
	</tr>
	</table>
<?php
		exit();
	}
	if($_GET['buscar'] == 'si')
	{
		$consec = $_GET['consec'];
		$nom = $_GET['nom'];
		$anocon = $_GET['anocon'];
		$cencos = $_GET['cencos'];
		$cod = $_GET['cod'];
		$reg = $_GET['reg'];
		$mun = $_GET['mun'];
		$tip = $_GET['tip'];
		$tipint = $_GET['tipint'];
		$moda = $_GET['moda'];
		$actor = $_GET['actor'];
		$est = $_GET['est'];
		$pro = $_GET['pro'];
?>
		<table style="width: 100%">
		<tr>
			<td class="alinearizq">&nbsp;</td>
            <td class="alinearizq"><strong>Proyecto</strong></td>
			<td class="alinearizq" style="display: none;"><strong>Consecutivo</strong></td>
			<td class="alinearizq"><strong>Código Cajero</strong></td>
			<td class="alinearizq"><strong>Nombre</strong></td>
			<td class="alinearizq"><strong>Padre/Hijo</strong></td>
			<td class="alinearizq"><strong>Región</strong></td>
			<td class="alinearizq"><strong>Tipología</strong></td>
			<td class="alinearizq"><strong>Tip. Intervención</strong></td>
			<td class="alinearizq"><strong>Estado</strong></td>
		</tr>
		<?php
		if($claprf == 1 || $mostrarcajeros == 1)
		{
			$query = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int) inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where (c.caj_clave_int = '".$consec."' OR '".$consec."' IS NULL OR '".$consec."' = '') and (caj_nombre LIKE REPLACE('%".$nom."%',' ','%') OR '".$nom."' IS NULL OR '".$nom."' = '') and (caj_ano_contable = '".$anocon."' OR '".$anocon."' IS NULL OR '".$anocon."' = '') and (cco_clave_int = '".$cencos."' OR '".$cencos."' IS NULL OR '".$cencos."' = '') and (caj_direccion LIKE REPLACE('%".$dir."%',' ','%') OR '".$dir."' IS NULL OR '".$dir."' = '') and (caj_region = '".$reg."' OR '".$reg."' IS NULL OR '".$reg."' = '') and (caj_departamento = '".$dep."' OR '".$dep."' IS NULL OR '".$dep."' = '') and (caj_municipio = '".$mun."' OR '".$mun."' IS NULL OR '".$mun."' = '') and (tip_clave_int = '".$tip."' OR '".$tip."' IS NULL OR '".$tip."' = '') and (tii_clave_int = '".$tipint."' OR '".$tipint."' IS NULL OR '".$tipint."' = '') and (mod_clave_int = '".$moda."' OR '".$moda."' IS NULL OR '".$moda."' = '') and (c.caj_estado_proyecto = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') and (cai_inmobiliaria = '".$actor."' or cav_visitante = '".$actor."' or cad_disenador = '".$actor."' or cal_gestionador = '".$actor."' or cin_interventor = '".$actor."' or cac_constructor = '".$actor."' or cas_instalador = '".$actor."' or '".$actor."' IS NULL or '".$actor."' = '') and (c.pro_clave_int = '".$pro."' OR '".$pro."' IS NULL OR '".$pro."' = '') and c.caj_sw_eliminado = 0");
			//$res = $con->query($query);
			$num_registros = mysqli_num_rows($query);
	
			$resul_x_pagina = 50;
			//Paginar:
			$paginacion = new Zebra_Pagination();
			$paginacion->records($num_registros);
			$paginacion->records_per_page($resul_x_pagina);
			
			$con = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int) inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where (c.caj_clave_int = '".$consec."' OR '".$consec."' IS NULL OR '".$consec."' = '') and (caj_nombre LIKE REPLACE('%".$nom."%',' ','%') OR '".$nom."' IS NULL OR '".$nom."' = '') and (caj_ano_contable = '".$anocon."' OR '".$anocon."' IS NULL OR '".$anocon."' = '') and (cco_clave_int = '".$cencos."' OR '".$cencos."' IS NULL OR '".$cencos."' = '') and (caj_direccion LIKE REPLACE('%".$dir."%',' ','%') OR '".$dir."' IS NULL OR '".$dir."' = '') and (caj_region = '".$reg."' OR '".$reg."' IS NULL OR '".$reg."' = '') and (caj_departamento = '".$dep."' OR '".$dep."' IS NULL OR '".$dep."' = '') and (caj_municipio = '".$mun."' OR '".$mun."' IS NULL OR '".$mun."' = '') and (tip_clave_int = '".$tip."' OR '".$tip."' IS NULL OR '".$tip."' = '') and (tii_clave_int = '".$tipint."' OR '".$tipint."' IS NULL OR '".$tipint."' = '') and (mod_clave_int = '".$moda."' OR '".$moda."' IS NULL OR '".$moda."' = '') and (c.caj_estado_proyecto = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') and (cai_inmobiliaria = '".$actor."' or cav_visitante = '".$actor."' or cad_disenador = '".$actor."' or cal_gestionador = '".$actor."' or cin_interventor = '".$actor."' or cac_constructor = '".$actor."' or cas_instalador = '".$actor."' or '".$actor."' IS NULL or '".$actor."' = '') and c.caj_sw_eliminado = 0 LIMIT ".(($paginacion->get_page() - 1) * $resul_x_pagina). ',' .$resul_x_pagina);
		}
		else
		{
			$query = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int) inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where (c.caj_clave_int = '".$consec."' OR '".$consec."' IS NULL OR '".$consec."' = '') and (caj_nombre LIKE REPLACE('%".$nom."%',' ','%') OR '".$nom."' IS NULL OR '".$nom."' = '') and (caj_ano_contable = '".$anocon."' OR '".$anocon."' IS NULL OR '".$anocon."' = '') and (cco_clave_int = '".$cencos."' OR '".$cencos."' IS NULL OR '".$cencos."' = '') and (caj_direccion LIKE REPLACE('%".$dir."%',' ','%') OR '".$dir."' IS NULL OR '".$dir."' = '') and (caj_region = '".$reg."' OR '".$reg."' IS NULL OR '".$reg."' = '') and (caj_departamento = '".$dep."' OR '".$dep."' IS NULL OR '".$dep."' = '') and (caj_municipio = '".$mun."' OR '".$mun."' IS NULL OR '".$mun."' = '') and (tip_clave_int = '".$tip."' OR '".$tip."' IS NULL OR '".$tip."' = '') and (tii_clave_int = '".$tipint."' OR '".$tipint."' IS NULL OR '".$tipint."' = '') and (mod_clave_int = '".$moda."' OR '".$moda."' IS NULL OR '".$moda."' = '') and (c.caj_estado_proyecto = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') and (cajseg.cas_instalador = '".$clausu."' OR cajseg.cas_instalador IN (select usa_actor from usuario_actor where usu_clave_int = '".$clausu."')) and (cai_inmobiliaria = '".$actor."' or cav_visitante = '".$actor."' or cad_disenador = '".$actor."' or cal_gestionador = '".$actor."' or cin_interventor = '".$actor."' or cac_constructor = '".$actor."' or cas_instalador = '".$actor."' or '".$actor."' IS NULL or '".$actor."' = '') and c.caj_sw_eliminado = 0");
			//$res = $con->query($query);
			$num_registros = mysqli_num_rows($query);
	
			$resul_x_pagina = 50;
			//Paginar:
			$paginacion = new Zebra_Pagination();
			$paginacion->records($num_registros);
			$paginacion->records_per_page($resul_x_pagina);
			
			$con = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int) inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where (c.caj_clave_int = '".$consec."' OR '".$consec."' IS NULL OR '".$consec."' = '') and (caj_nombre LIKE REPLACE('%".$nom."%',' ','%') OR '".$nom."' IS NULL OR '".$nom."' = '') and (caj_ano_contable = '".$anocon."' OR '".$anocon."' IS NULL OR '".$anocon."' = '') and (cco_clave_int = '".$cencos."' OR '".$cencos."' IS NULL OR '".$cencos."' = '') and (caj_direccion LIKE REPLACE('%".$dir."%',' ','%') OR '".$dir."' IS NULL OR '".$dir."' = '') and (caj_region = '".$reg."' OR '".$reg."' IS NULL OR '".$reg."' = '') and (caj_departamento = '".$dep."' OR '".$dep."' IS NULL OR '".$dep."' = '') and (caj_municipio = '".$mun."' OR '".$mun."' IS NULL OR '".$mun."' = '') and (tip_clave_int = '".$tip."' OR '".$tip."' IS NULL OR '".$tip."' = '') and (tii_clave_int = '".$tipint."' OR '".$tipint."' IS NULL OR '".$tipint."' = '') and (mod_clave_int = '".$moda."' OR '".$moda."' IS NULL OR '".$moda."' = '') and (c.caj_estado_proyecto = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') and (cajseg.cas_instalador = '".$clausu."' OR cajseg.cas_instalador IN (select usa_actor from usuario_actor where usu_clave_int = '".$clausu."')) and (cai_inmobiliaria = '".$actor."' or cav_visitante = '".$actor."' or cad_disenador = '".$actor."' or cal_gestionador = '".$actor."' or cin_interventor = '".$actor."' or cac_constructor = '".$actor."' or cas_instalador = '".$actor."' or '".$actor."' IS NULL or '".$actor."' = '') and c.caj_sw_eliminado = 0 LIMIT ".(($paginacion->get_page() - 1) * $resul_x_pagina). ',' .$resul_x_pagina);
		}
		$num = mysqli_num_rows($con);
		for($i = 0; $i < $num; $i++)
		{
			$dato = mysqli_fetch_array($con);
			//Info Base
			$clacaj = $dato['clacaj'];
			$nomcaj = $dato['caj_nombre'];
			$dir = $dato['caj_direccion'];
			$anocon = $dato['caj_ano_contable'];
			$reg = $dato['caj_region'];
			$dep = $dato['caj_departamento'];
			$mun = $dato['caj_municipio'];
			$tip = $dato['tip_clave_int'];
			$tipint = $dato['tii_clave_int'];
			$mod = $dato['mod_clave_int'];
			$conmod = mysqli_query($conectar,"select mod_nombre from modalidad where mod_clave_int = '".$mod."'");
			$datomod = mysqli_fetch_array($conmod);
			$moda = $datomod['mod_nombre'];
			//Info Secun
			$codcaj = $dato['caj_codigo_cajero'];
			$cencos = $dato['cco_clave_int'];
			$refmaq = $dato['rem_clave_int'];
			$ubi = $dato['ubi_clave_int'];
			$codsuc = $dato['caj_codigo_suc'];
			$ubiamt = $dato['caj_ubicacion_atm'];
			$ati = $dato['ati_clave_int'];
			$rie = $dato['caj_riesgo'];
			$are = $dato['are_clave_int'];
			$codrec = $dato['caj_cod_recibido_monto'];
			$codrec = $dato['caj_fecha_creacion'];
			//Info Inmobiliaria
			$reqinm = $dato['cai_req_inmobiliaria'];
			$inmob = $dato['cai_inmobiliaria'];
			$feciniinmob = $dato['cai_fecha_ini_inmobiliaria'];
			$estinmob = $dato['esi_clave_int'];
			$fecentinmob = $dato['cai_fecha_entrega_info_inmobiliaria'];
			//Info Visita Local
			$reqvis = $dato['cav_req_visita_local'];
			$vis = $dato['cav_visitante'];
			$fecvis = $dato['cav_fecha_visita'];
			//Info Diseño
			$reqdis = $dato['cad_req_diseno'];
			$dis = $dato['cad_disenador'];
			$fecinidis = $dato['cad_fecha_inicio_diseno'];
			$estdis = $dato['esd_clave_int'];
			$fecentdis = $dato['cad_fecha_entrega_info_diseno'];
			//Info Licencia
			$reqlic = $dato['cal_req_licencia'];
			$lic = $dato['cal_gestionador'];
			$fecinilic = $dato['cal_fecha_inicio_licencia'];
			$estlic = $dato['esl_clave_int'];
			$fecentlic = $dato['cal_fecha_entrega_info_licencia'];
			//Info Interventoria
			$int = $dato['cin_interventor'];
			$fecteoent = $dato['cin_fecha_teorica_entrega'];
			$feciniobra = $dato['cin_fecha_inicio_obra'];
			$fecpedsum = $dato['cin_fecha_pedido_suministro'];
			$aprovcotiz = $dato['cin_sw_aprov_cotizacion'];
			$cancom = $dato['can_clave_int'];
			$aprovliqui = $dato['cin_sw_aprov_liquidacion'];
			$swimgnex = $dato['cin_sw_img_nexos'];
			$swfot = $dato['cin_sw_fotos'];
			$swact = $dato['cin_sw_actas'];
			$estpro = $dato['caj_estado_proyecto'];
			//Info Constructor
			$cons = $dato['cac_constructor'];
			$cons = $dato['cac_fecha_entrega_atm'];
			$cons = $dato['cac_porcentaje_avance'];
			$cons = $dato['cac_fecha_entrega_cotizacion'];
			$cons = $dato['cac_fecha_entrega_liquidacion'];
			//Info Instalador Seguridad
			$seg = $dato['cas_instalador'];
			$fecingseg = $dato['cas_fecha_ingreso_obra_inst'];
			$codmon = $dato['cas_codigo_monitoreo'];
			
			$swpad = $dato['caj_sw_padre'];
			$hijo = $dato['caj_padre'];
		?>
		<tr <?php if($i % 2 == 0){ echo 'style="background-color:#eeeeee;"'; } ?>>
			<td class="alinearizq"><img src="../../img/editar.png" onclick="VERREGISTRO('<?php echo $clacaj; ?>')" alt="" height="25" width="25" style="cursor:pointer" /></td>
			<td class="alinearizq"><?php echo $clacaj; ?></td>
			<td class="alinearizq"><?php echo $codcaj; ?></td>
			<td class="alinearizq"><?php echo $nomcaj; ?></td>
			<td class="alinearizq">
			<?php
			if($swpad == 1 and ($hijo == 0 or $hijo == ''))
			{
				$conhij = mysqli_query($conectar,"select caj_clave_int,caj_nombre from cajero where caj_padre = ".$clacaj."");
				$numhij = mysqli_num_rows($conhij);
			?>
				<div style="cursor:pointer" onmouseover="javascript: VentanaFlotante('<?php for($j = 0; $j < $numhij; $j++){ $datohij = mysqli_fetch_array($conhij); echo $datohij['caj_clave_int']." - ".$datohij['caj_nombre']."<br>"; } ?>', 300, <?php echo 35*$numhij; ?>)" onmouseout="javascript: quitarDiv();">Padre</div>
			<?php
			}
			else
			if($hijo > 0)
			{
				$conpad = mysqli_query($conectar,"select caj_clave_int,caj_nombre from cajero where caj_clave_int = ".$hijo."");
				$datopad = mysqli_fetch_array($conpad);
			?>
				<div style="cursor:pointer" onmouseover="javascript: VentanaFlotante('<?php echo $datopad['caj_clave_int']." - ".$datopad['caj_nombre']."<br>"; ?>', 200, 35)" onmouseout="javascript: quitarDiv();">Hijo</div>
			<?php
			}
			else
			{
				echo "<div>Ninguno</div>";
			}
			?>
			</td>
			<td class="alinearizq">
			<?php 
				$sql = mysqli_query($conectar,"select pog_nombre from posicion_geografica where pog_clave_int = '".$reg."'");
				$datosql = mysqli_fetch_array($sql);
				$nomreg = $datosql['pog_nombre'];
				echo $nomreg; 
			?>
			</td>

			<td class="alinearizq">
			<?php 
				$sql = mysqli_query($conectar,"select tip_nombre from tipologia where tip_clave_int = '".$tip."'");
				$datosql = mysqli_fetch_array($sql);
				$nomtip = $datosql['tip_nombre'];
				echo $nomtip;
			?>
			</td>
			<td class="alinearizq">
			<?php 
				$sql = mysqli_query($conectar,"select tii_nombre from tipointervencion where tii_clave_int = '".$tipint."'");
				$datosql = mysqli_fetch_array($sql);
				$nomtii = $datosql['tii_nombre'];
				echo $nomtii;
			?>
			</td>
			<td class="alinearizq">
			<?php if($estpro == 1){ echo "ACTIVO"; }elseif($estpro == 2){ echo "SUSPENDIDO"; }elseif($estpro == 3){ echo "ENTREGADO"; }elseif($estpro == 4){ echo "CANCELADO"; }elseif($estpro == 5){ echo "PROGRAMADO"; } ?>
			</td>
		</tr>
		<?php
		}
		?>
		</table>
<?php
		$paginacion->render();
		exit();
	}
	if($_GET['verdepartamentos'] == 'si')
	{
		$reg = $_GET['reg'];
?>
		<select name="departamento" id="departamento" onchange="VERCIUDADES(this.value)" tabindex="7" class="chosen-select" data-placeholder="-Seleccione-" style="width:148px">
		<option value="">-Seleccione-</option>
		<?php
			$con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo IS NOT NULL and pog_nieto IS NULL and pog_hijo = '".$reg."' order by pog_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clave = $dato['pog_clave_int'];
				$nombre = $dato['pog_nombre'];
		?>
			<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
		<?php
			}
		?>
		</select>
<?php
		exit();
	}
	if($_GET['verciudades'] == 'si')
	{
		$dep = $_GET['dep'];
?>
		<select name="ciudad" id="ciudad" tabindex="8" class="chosen-select" data-placeholder="-Seleccione-" style="width:148px">
		<option value="">-Seleccione-</option>
		<?php
			$con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo IS NULL and pog_nieto IS NOT NULL and pog_nieto = '".$dep."' order by pog_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clave = $dato['pog_clave_int'];
				$nombre = $dato['pog_nombre'];
		?>
			<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
		<?php
			}
		?>
		</select>
<?php
		exit();
	}
	if($_GET['verregistro'] == 'si')
	{
		$clacaj = $_GET['clacaj'];
		$con = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) where c.caj_clave_int = '".$clacaj."'");
		$dato = mysqli_fetch_array($con);
		//Info Base
		$clacaj = $dato['clacaj'];
		$nomcaj = $dato['caj_nombre'];
		$dir = $dato['caj_direccion'];
		$anocon = $dato['caj_ano_contable'];
		$reg = $dato['caj_region'];
		$dep = $dato['caj_departamento'];
		$mun = $dato['caj_municipio'];
		$tip = $dato['tip_clave_int'];
		$tipint = $dato['tii_clave_int'];
		$mod = $dato['mod_clave_int'];
		//Info Secun
		$codcaj = $dato['caj_codigo_cajero'];
		//Info Interventoria
		$feciniobra = $dato['cin_fecha_inicio_obra'];
		$fecteoent = $dato['cin_fecha_teorica_entrega'];
		//Info Constructor
		$cons = $dato['cac_constructor'];
		$porava = $dato['cac_porcentaje_avance'];
		//Info Instalador Seguridad
		$seg = $dato['cas_instalador'];
		$fecingseg = $dato['cas_fecha_ingreso_obra_inst'];
		$codmon = $dato['cas_codigo_monitoreo'];
		
		$conpad = mysqli_query($conectar,"select caj_padre,(select c.caj_nombre from cajero c where c.caj_clave_int = cajero.caj_padre) nomcaj from cajero where caj_clave_int = ".$clacaj."");
		$datopad = mysqli_fetch_array($conpad);
		$mipad = $datopad['caj_padre'];
		$nomcajpad = $datopad['nomcaj'];
		
		if($feciniobra == '0000-00-00'){ $feciniobra = ''; }
		if($fecingseg == '0000-00-00'){ $fecingseg = ''; }
?>
		<fieldset name="Group1" class="auto-style3">
		<legend class="auto-style3"><strong>INFORMACIÓN INSTALADOR SEGURIDAD</strong></legend>
		<input name="clavecajero" id="clavecajero" value="<?php echo $clacaj; ?>" type="hidden" />
		<table style="width: 90%" align="center">
			<tr>
				<td class="alinearizq" colspan="7">
				<fieldset name="Group1">
					<legend align="center">INFORMACIÓN BÁSICA CAJERO</legend>
					<table style="width: 100%">
					<tr>
						<td>Nombre Cajero:</td>
						<td colspan="3">
				<input name="nombrecajero1" id="nombrecajero" value="<?php echo $nomcaj; ?>" disabled="disabled" tabindex="1" class="auto-style5" type="text" style="width: 360px" size="20"></td>
						<td>
						Dirección:</td>
						<td colspan="3">
				<input name="direccion" id="direccion" value="<?php echo $dir; ?>" disabled="disabled" tabindex="5" class="inputs" type="text" style="width: 345px"></td>
					</tr>
					<tr>
						<td>Año Contable:</td>
						<td>
				<input name="codigocajero" id="codigocajero" value="<?php echo $anocon; ?>" disabled="disabled" tabindex="2" class="inputs" type="text" style="width: 140px"></td>
						<td>Cod. Cajero</td>
						<td>
						<input name="codigocajero" id="codigocajero" disabled="disabled" value="<?php echo $codcaj; ?>" tabindex="2" class="inputs" type="text" style="width: 115px">
						</td>
						<td>
						Región:</td>
						<td>
				<select name="region" id="region" disabled="disabled" tabindex="6" class="inputs" style="width: 140px">
				<option value="">-Seleccione-</option>
				<?php
					$con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo IS NULL and pog_nieto IS NULL order by pog_nombre");
					$num = mysqli_num_rows($con);
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$clave = $dato['pog_clave_int'];
						$nombre = $dato['pog_nombre'];
				?>
					<option value="<?php echo $clave; ?>" <?php if($clave == $reg){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
				<?php
					}
				?>
				</select></td>
						<td>
						Municipío:</td>
						<td>
						<select name="ciudad0" id="ciudad0" disabled="disabled" tabindex="8" class="inputs" style="width: 130px">
						<option value="">-Seleccione-</option>
						<?php
							$con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo IS NULL and pog_nieto IS NOT NULL order by pog_nombre");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$clave = $dato['pog_clave_int'];
								$nombre = $dato['pog_nombre'];
						?>
							<option value="<?php echo $clave; ?>" <?php if($clave == $mun){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
						<?php
							}
						?>
						</select></td>
					</tr>
					<tr>
						<td>Tipología:</td>
						<td colspan="3">
						<select name="tipologia" id="tipologia" disabled="disabled" tabindex="9" class="inputs" style="width: 365px">
						<option value="">-Seleccione-</option>
						<?php
							$con = mysqli_query($conectar,"select * from tipologia where tip_sw_activo = 1 order by tip_nombre");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$clave = $dato['tip_clave_int'];
								$nombre = $dato['tip_nombre'];
						?>
							<option value="<?php echo $clave; ?>" <?php if($clave == $tip){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
						<?php
							}
						?>
						</select>
						</td>
						<td>
						Tipo 
				Intervención:</td>
						<td>
						<select name="tipointervencion2" id="tipointervencion0" disabled="disabled" tabindex="17" class="inputs" style="width: 140px">
						<option value="">-Seleccione-</option>
						<?php
							$con = mysqli_query($conectar,"select * from tipointervencion order by tii_nombre");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$clave = $dato['tii_clave_int'];
								$nombre = $dato['tii_nombre'];
						?>
							<option value="<?php echo $clave; ?>" <?php if($clave == $tipint){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
						<?php
							}
						?>
						</select>
						</td>
						<td>
						Modalidad:</td>
						<td>
						<select name="modalidad" id="modalidad" disabled="disabled" tabindex="8" class="inputs" data-placeholder="-Seleccione-" style="width: 130px">
						<option value="">-Seleccione-</option>
						<?php
							$con = mysqli_query($conectar,"select * from modalidad where mod_clave_int in (select mod_clave_int from intervencion_modalidad where tii_clave_int = ".$tipint.") order by mod_nombre");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$clave = $dato['mod_clave_int'];
								$nombre = $dato['mod_nombre'];
						?>
							<option value="<?php echo $clave; ?>" <?php if($mod == $clave){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
						<?php
							}
						?>
						</select>
						</td>
					</tr>
					</table>
				</fieldset>
				</td>
			</tr>
			<tr>
				<td class="alinearizq" colspan="7">
				<table style="width: 90%" align="center">
					<tr>
				<td class="alinearizq" style="width: 140px">Fecha Inicio Obra:</td>
				<td class="alinearizq" style="width: 62px">
				
				<input name="fechainicioobra9" id="fechainicioobra8" value="<?php echo $feciniobra; ?>" disabled="disabled" tabindex="34" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px" size="20"></td>
				<td class="alinearizq" style="width: 53px">
				
				% Avance 
				Obra:</td>
				<td class="alinearizq" style="width: 100px">
				
				<select name="porcentajeavance0" id="porcentajeavance0" disabled="disabled" tabindex="33" class="inputs" data-placeholder="-Seleccione-" style="width: 140px">
				<option value="">-Seleccione-</option>
				<?php
					for($i = 10; $i <= 100; $i = $i+10)
					{
				?>	
						<option value="<?php echo $i; ?>" <?php if($i == $porava){ echo 'selected="selected"'; } ?>><?php echo $i; ?>%</option>
				<?php
					}
				?>
				</select></td>
				<td class="alinearizq" style="width: 119px">
				
				Adjuntar</td>
				<td class="alinearizq" colspan="2">
				<?php
				if($metodo == 1 and ($mipad == 0 or $mipad == ''))
				{
				?>
				<div class="file-wrapper">
				<input name="adjuntoinf" type="file" onchange="MOSTRARRUTA('INFORME')" class="file" style="cursor:pointer; right: 0; top: 0;" />
				<span class="button">Adjuntar Archivo</span>
				</div>
				<input type="submit" onclick="ADJUNTAR('INFORME')" value="Subir" class="btnSubmit" />
				<?php
				}
				?>
				</td>
				<td class="alinearizq">
				<div id="resultadoadjuntoinf">
				<?php
					$con = mysqli_query($conectar,"select * from adjunto_actor where caj_clave_int = '".$clacaj."' and tad_clave_int = 19 and ada_sw_eliminado = 0");
					$numadj = mysqli_fetch_array($con);
					
					if($numadj != '')
					{
				?>
				<div class='ok' style='width: 105px' align='center'>
				<img src='../../images/veradjuntos.png' onclick="VERADJUNTOS('top');MOSTRARADJUNTOS('<?php echo $clacaj; ?>','19')" style='width:100px; height:30px; cursor: pointer' />
				</div>
				<?php
					}
					else
					{
				?>
						<div class='validaciones' style='width: 50%' align='center'>Sin Adjunto</div>
				<?php
					}
				?>
				</div>
				</td>
					</tr>
					<tr>
				<td class="alinearizq" style="width: 140px">Fecha Teorica 
				Entrega:</td>
				<td class="alinearizq" style="width: 62px">
				<input name="teoricainterventoria" id="teoricainterventoria" disabled="disabled" value="<?php echo $fecteoent; ?>" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px">
				</td>
				<td class="alinearizq" style="width: 53px">
				Constructor:
				</td>
				<td class="alinearizq" style="width: 100px">
				<?php
				$con = mysqli_query($conectar,"select * from usuario where (usu_categoria = 2 and usu_sw_constructor = 1 and usu_sw_activo = 1) OR (usu_clave_int NOT IN (select usu_clave_int from usuario_actor) and usu_sw_constructor = 1 and usu_sw_activo = 1) and usu_clave_int = ".$cons." order by usu_nombre");
				$dato = mysqli_fetch_array($con);
				echo $dato['usu_nombre'];
				?>
				</td>
				<td class="alinearizq" style="width: 119px">
				
				&nbsp;</td>
				<td class="alinearizq" colspan="2">
				&nbsp;</td>
				<td class="alinearizq">
				&nbsp;</td>
					</tr>
					<tr>
				<td class="alinearizq" style="width: 140px">Fecha 
				Ingreso Obra:</td>
				<td class="alinearizq" style="width: 62px">
				
				<input name="fechaingresoobra" id="fechaingresoobra" value="<?php echo $fecingseg; ?>" tabindex="34" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px" size="20"></td>
				<td class="alinearizq" style="width: 53px">
				
				Cod. 
				Monitoreo:</td>
				<td class="alinearizq" style="width: 100px">
				
				<input name="codigomonitoreo" id="codigomonitoreo" value="<?php echo $codmon; ?>" tabindex="34" class="inputs" type="text" style="width: 140px" size="20"></td>
				<td class="alinearizq" style="width: 119px">
				
				Adjunto Facturación</td>
				<td class="alinearizq" colspan="2">
				<?php
				if($metodo == 1 and ($mipad == 0 or $mipad == ''))
				{
				?>
				<div class="file-wrapper">
				<input name="adjuntofac" type="file" onchange="MOSTRARRUTA('FACTURACION')" class="file" style="cursor:pointer" />
				<span class="button">Adjuntar Archivo</span>
				</div>
				<input type="submit" onclick="ADJUNTAR('FACTURACION')" value="Subir" class="btnSubmit" />
				<?php
				}
				?>
				</td>
				<td class="alinearizq">
				<div id="resultadoadjuntofac">
				<?php
					$con = mysqli_query($conectar,"select * from adjunto_actor where caj_clave_int = '".$clacaj."' and tad_clave_int = 20 and ada_sw_eliminado = 0");
					$numadj = mysqli_fetch_array($con);
					
					if($numadj != '')
					{
				?>
				<div class='ok' style='width: 105px' align='center'>
				<img src='../../images/veradjuntos.png' onclick="VERADJUNTOS('top');MOSTRARADJUNTOS('<?php echo $clacaj; ?>','20')" style='width:100px; height:30px; cursor: pointer' />
				</div>
				<?php
					}
					else
					{
				?>
						<div class='validaciones' style='width: 50%' align='center'>Sin Adjunto</div>
				<?php
					}
				?>
				</div>
				</td>
					</tr>
					<tr>
						<td>Notas:</td>
						<td colspan="5">
						<textarea name="nota" id="nota" class="inputs" style="width: 650px; height: 60px;" rows="1"></textarea></td>
						<td>
						<?php
						if($metodo == 1 and ($mipad == 0 or $mipad == ''))
						{
						?>
						<img src="../../img/agg.png" title="Agregar Nota" onclick="AGREGARNOTA('<?php echo $clacaj; ?>')" onmouseover="this.src = '../../img/agg1.png'" onmouseout="this.src = '../../img/agg.png'" onclick="AVISO()" height="20" width="20" style="cursor:pointer" />
						<?php
						}
						?>
						</td>
						<td>
						&nbsp;</td>
					</tr>
					<tr>
						<td>Última nota:</td>
						<td colspan="2">
						<div id="agregados" style="overflow:auto; width: 300px;">
						<?php
							$con = mysqli_query($conectar,"select * from notausuario where caj_clave_int = '".$clacaj."' and ven_clave_int = 10 ORDER BY nou_clave_int DESC LIMIT 1");
							$dato = mysqli_fetch_array($con);
							echo $dato['nou_nota']." ".$dato['nou_fecha_creacion'];
						?>
						</div>
						</td>
						<td colspan="3" align="center">
				<img src="../../img/vernotas.png" onclick="VERNOTAS();MOSTRARNOTAS('<?php echo $clacaj; ?>')" style="cursor:pointer" height="35" width="130" /></td>
						<td align="center">
						&nbsp;</td>
						<td align="center">
						&nbsp;</td>
					</tr>
					<tr>
						<td colspan="6" class="alinearizq">
						<a onclick="VERNOTAS();MOSTRARBITACORA('<?php echo $clacaj; ?>')" style="cursor:pointer" class="boton"><span class="colorboton">BITACORA DE NOTAS</span></a>
				&nbsp;&nbsp;<a onclick="VERADJUNTOSBITACORA();MOSTRARBITACORAADJUNTOS('<?php echo $clacaj; ?>')" style="cursor:pointer" class="boton"><span class="colorboton">BITACORA DE ADJUNTOS</span></a>
						</td>
						<td align="center">
						&nbsp;</td>
						<td align="center">
						&nbsp;</td>
					</tr>
					<tr>
						<td colspan="6" align="center">
						<?php
						if($metodo == 1)
						{
						?>
						<br>
						<a onclick="GUARDAR('<?php echo $clacaj; ?>')" style="cursor:pointer" class="boton"><span class="colorboton">GUARDAR INFORMACIÓN</span></a>
						<?php
						}
						?>
						</td>
						<td align="center">
						&nbsp;</td>
						<td align="center">
						&nbsp;</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td class="alinearizq" style="width: 141px">&nbsp;</td>
				<td class="alinearizq" style="width: 141px">&nbsp;</td>
				<td class="alinearizq">
				&nbsp;</td>
				<td class="alinearizq">
				&nbsp;</td>
				<td class="alinearizq">
				&nbsp;</td>
				<td class="alinearizq">
				&nbsp;</td>
				<td style="width: 160px" align="right">
				&nbsp;</td>
			</tr>
			</table>
		<div id="estadoregistro"></div>
	</fieldset>
<?php
		exit();
	}
	if($_GET['agregarnota'] == 'si')
	{
		$fecha = date('Y/m/d H:i:s');
		$not = $_GET['not'];
		$suc = $_GET['suc'];
		
		$not = str_replace("REEMPLAZARNUMERAL","#",$not);
		$not = str_replace("REEMPLAZARMAS","+",$not);
		
		$con = mysqli_query($conectar,"select * from notausuario where usu_clave_int = '".$clausu."' and suc_clave_int = '".$caj."' and UPPER(nou_nota) = '".strtoupper($not)."'");
		$num = mysqli_num_rows($con);
		if($num <= 0)
		{
			mysqli_query($conectar,"insert into notausuario(nou_clave_int,usu_clave_int,suc_clave_int,nou_nota,ven_clave_int,nou_fecha_creacion,nou_fec_actualiz) values(null,'".$clausu."','".$caj."','".$not."','10','".$fecha."','".$fecha."')");
			/*$con = mysqli_query($conectar,"select suc_clave_int from cajero where caj_padre = ".$caj."");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clahij = $dato['caj_clave_int'];
				mysqli_query($conectar,"insert into notausuario(nou_clave_int,usu_clave_int,caj_clave_int,nou_nota,ven_clave_int,nou_fecha_creacion,nou_fec_actualiz) values(null,'".$clausu."','".$clahij."','".$not."','10','".$fecha."','".$fecha."')");
			}*/
		}
		$con = mysqli_query($conectar,"select * from notausuario where usu_clave_int = '".$clausu."' and suc_clave_int = '".$caj."' ORDER BY nou_clave_int DESC LIMIT 1");
		$dato = mysqli_fetch_array($con);
		echo $dato['nou_nota']." ".$dato['nou_fecha_creacion'];
		exit();
	}
	if($_GET['editarnota'] == 'si')
	{
		$clanot = $_GET['clanot'];
		$suc = $_GET['suc'];
		
		$con = mysqli_query($conectar,"select * from notausuario where nou_clave_int = '".$clanot."'");
		$dato = mysqli_fetch_array($con);
		$not = $dato['nou_nota'];
?>
		<table style="width: 100%">
			<tr>
				<td><textarea name="notaedi" id="notaedi" class="inputs" cols="20" rows="2"><?php echo $not; ?></textarea></td>
				<td><input name="Button1" onclick="ACTUALIZARNOTA('<?php echo $clanot; ?>','<?php echo $suc; ?>')" type="button" value="Actualizar" /></td>
				<td><input name="Button1" onclick="ELIMINARNOTA('<?php echo $clanot; ?>','<?php echo $suc; ?>')" type="button" value="Eliminar" /></td>
			</tr>
		</table>
<?php
		exit();
	}
	if($_GET['actualizarnota'] == 'si')
	{
		$fecha = date('Y/m/d H:i:s');
		$not = $_GET['not'];
		$suc = $_GET['caj'];
		$clanot = $_GET['clanot'];
		
		$not = str_replace("REEMPLAZARNUMERAL","#",$not);
		$not = str_replace("REEMPLAZARMAS","+",$not);
		
		$con = mysqli_query($conectar,"select * from notausuario where suc_clave_int = '".$caj."' and ven_clave_int = 10 and UPPER(nou_nota) = '".strtoupper($not)."'");
		$num = mysqli_num_rows($con);
		if($num <= 0)
		{
			mysqli_query($conectar,"update notausuario set nou_nota = '".$not."' where nou_clave_int = '".$clanot."'");
			mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'10',8,'".$caj."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 8=Actualización nota
		}
		$con = mysqli_query($conectar,"select * from notausuario where suc_clave_int = '".$caj."' and ven_clave_int = 10 ORDER BY nou_clave_int");
		$num = mysqli_num_rows($con);
		for($i = 0; $i < $num; $i++)
		{
			$dato = mysqli_fetch_array($con);
			$clanot = $dato['nou_clave_int'];
			if($i % 2 == 0)
			{
				echo '<div style="width:100%; border:2px; font-size: 15px; outline:none; border-radius:3px; -webkit-border-radius:6px; -moz-border-radius:3px; border:1px solid rgba(0,0,0, 0.5); padding: 3px; background-color: #CCCCCC"><strong>Nota:</strong> '.$dato['nou_nota']." <strong>Fecha:</strong> ".$dato['nou_fecha_creacion']."<img src='../../img/editar.png' style='cursor:pointer' onclick=EDITARNOTA('$caj','$clanot') height='25' width='25' /><br></div>";
			}
			else
			{
				echo '<div style="width:100%; border:2px; font-size: 15px; outline:none; border-radius:3px; -webkit-border-radius:6px; -moz-border-radius:3px; border:1px solid rgba(0,0,0, 0.5); padding: 3px; background-color: #F0F0F0"><strong>Nota:</strong> '.$dato['nou_nota']." ".$dato['nou_fecha_creacion']."<img src='../../img/editar.png' style='cursor:pointer' onclick=EDITARNOTA('$caj','$clanot') height='25' width='25' /><br></div>";
			}
		}
		echo '<div id="editarnota"></div>';
		exit();
	}
	if($_GET['eliminarnota'] == 'si')
	{
		$clanot = $_GET['clanot'];
		$suc = $_GET['suc'];
		$fecha=date("Y/m/d H:i:s");
		mysqli_query($conectar,"delete from notausuario where nou_clave_int = '".$clanot."'");
		mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'10',68,'".$caj."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 68=Eliminación nota

		$con = mysqli_query($conectar,"select * from notausuario where suc_clave_int = '".$suc."' and ven_clave_int = 10 ORDER BY nou_clave_int");
		$num = mysqli_num_rows($con);
		for($i = 0; $i < $num; $i++)
		{
			$dato = mysqli_fetch_array($con);
			$clanot = $dato['nou_clave_int'];
			if($i % 2 == 0)
			{
				echo '<div style="width:100%; border:2px; font-size: 15px; outline:none; border-radius:3px; -webkit-border-radius:6px; -moz-border-radius:3px; border:1px solid rgba(0,0,0, 0.5); padding: 3px; background-color: #CCCCCC"><strong>Nota:</strong> '.$dato['nou_nota']." <strong>Fecha:</strong> ".$dato['nou_fecha_creacion']."<img src='../../img/editar.png' style='cursor:pointer' onclick=EDITARNOTA('$caj','$clanot') height='25' width='25' /><br></div>";
			}
			else
			{
				echo '<div style="width:100%; border:2px; font-size: 15px; outline:none; border-radius:3px; -webkit-border-radius:6px; -moz-border-radius:3px; border:1px solid rgba(0,0,0, 0.5); padding: 3px; background-color: #F0F0F0"><strong>Nota:</strong> '.$dato['nou_nota']." <strong>Fecha:</strong> ".$dato['nou_fecha_creacion']."<img src='../../img/editar.png' style='cursor:pointer' onclick=EDITARNOTA('$caj','$clanot') height='25' width='25' /><br></div>";
			}
		}
		echo '<div id="editarnota"></div>';
		
		exit();
	}
	if($_GET['mostrarnotas'] == 'si')
	{
		$clasuc = $_GET['clasuc'];
		$con = mysqli_query($conectar,"select * from notausuario where suc_clave_int = '".$clasuc."' and ven_clave_int = 10 ORDER BY nou_clave_int");
		$num = mysqli_num_rows($con);
		for($i = 0; $i < $num; $i++)
		{
			$dato = mysqli_fetch_array($con);
			$clanot = $dato['nou_clave_int'];
			if($i % 2 == 0)
			{
				echo '<div style="width:100%; border:2px; font-size: 15px; outline:none; border-radius:3px; -webkit-border-radius:6px; -moz-border-radius:3px; border:1px solid rgba(0,0,0, 0.5); padding: 3px; background-color: #CCCCCC"><strong>Nota:</strong> '.$dato['nou_nota']." <strong>Fecha:</strong> ".$dato['nou_fecha_creacion']."<img src='../../img/editar.png' style='cursor:pointer' onclick=EDITARNOTA('$clacaj','$clanot') height='25' width='25' /><br></div>";
			}
			else
			{
				echo '<div style="width:100%; border:2px; font-size: 15px; outline:none; border-radius:3px; -webkit-border-radius:6px; -moz-border-radius:3px; border:1px solid rgba(0,0,0, 0.5); padding: 3px; background-color: #F0F0F0"><strong>Nota:</strong> '.$dato['nou_nota']." <strong>Fecha:</strong> ".$dato['nou_fecha_creacion']."<img src='../../img/editar.png' style='cursor:pointer' onclick=EDITARNOTA('$clacaj','$clanot') height='25' width='25' /><br></div>";
			}
		}
		echo '<div id="editarnota"></div>';
		exit();
	}
	if($_GET['estadoadjunto'] == 'si')
	{
		$actor = $_GET['actor'];
		$clacaj = $_GET['clacaj'];
		
		if($actor == 'INFORME')
		{
			$con = mysqli_query($conectar,"select * from adjunto_actor where caj_clave_int = '".$clacaj."' and tad_clave_int = 19 and ada_sw_eliminado = 0");
			$numadj = mysqli_fetch_array($con);
			
			if($numadj != '')
			{
		?>
				<div class='ok' style='width: 105px' align='center'>
				<img src='../../images/veradjuntos.png' onclick="VERADJUNTOS('top');MOSTRARADJUNTOS('<?php echo $clacaj; ?>','19')" style='width:100px; height:30px; cursor: pointer' />
				</div>
		<?php
			}
		}
		else
		if($actor == 'FACTURACION')
		{
			$con = mysqli_query($conectar,"select * from adjunto_actor where caj_clave_int = '".$clacaj."' and tad_clave_int = 20 and ada_sw_eliminado = 0");
			$numadj = mysqli_fetch_array($con);
			
			if($numadj != '')
			{
		?>
				<div class='ok' style='width: 105px' align='center'>
				<img src='../../images/veradjuntos.png' onclick="VERADJUNTOS('top');MOSTRARADJUNTOS('<?php echo $clacaj; ?>','20')" style='width:100px; height:30px; cursor: pointer' />
				</div>
		<?php
			}
		}
		
		exit();
	}
	if($_GET['mostrarruta'] == 'si')
	{
		$nomadj = $_GET['nomadj'];
		echo "<div class='ok1' style='width: 100%' align='center'>$nomadj</div>";
		exit();
	}
	if($_GET['mostrarbitacora'] == 'si')
	{
		$clasuc = $_GET['clasuc'];
		
		$con = mysqli_query($conectar,"select * from permiso_actor where pea_actor = 'Seguridad'");
		$dato = mysqli_fetch_array($con);
		$notsuc = $dato['pea_sw_nota_suc'];
		$notinm = $dato['pea_sw_nota_inm'];
		$notvis = $dato['pea_sw_nota_vis'];
		$notdis = $dato['pea_sw_nota_dis'];
		$notlic = $dato['pea_sw_nota_lic'];
		$notint = $dato['pea_sw_nota_int'];
		$notcon = $dato['pea_sw_nota_con'];
		$notseg = $dato['pea_sw_nota_seg'];
		$notfac = $dato['pea_sw_nota_fac'];
		
		if($notsuc == 1){ $ven1 = 4; }else{ $ven1 = 0; }
		if($notinm == 1){ $ven2 = 7; }else{ $ven2 = 0; }
		if($notvis == 1){ $ven3 = 18; }else{ $ven3 = 0; }
		if($notdis == 1){ $ven4 = 8; }else{ $ven4 = 0; }
		if($notlic == 1){ $ven5 = 19; }else{ $ven5 = 0; }
		if($notint == 1){ $ven6 = 9; }else{ $ven6 = 0; }
		if($notcon == 1){ $ven7 = 6; }else{ $ven7 = 0; }
		if($notseg == 1){ $ven8 = 10; }else{ $ven8 = 0; }
		if($notfac == 1){ $ven9 = 20; }else{ $ven9 = 0; }
		
		$con = mysqli_query($conectar,"select * from notausuario where suc_clave_int = '".$clasuc."' and ven_clave_int IN (".$ven1.",".$ven2.",".$ven3.",".$ven4.",".$ven5.",".$ven6.",".$ven7.",".$ven8.",".$ven9.") ORDER BY nou_clave_int");
		$num = mysqli_num_rows($con);
		for($i = 0; $i < $num; $i++)
		{
			$dato = mysqli_fetch_array($con);
			$clanot = $dato['nou_clave_int'];
			$ventana = $dato['ven_clave_int'];
			$claveusu = $dato['usu_clave_int'];
			$conusu = mysqli_query($conectar,"select usu_nombre from usuario where usu_clave_int = '".$claveusu."'");
			$datousu = mysqli_fetch_array($conusu);
			$nomusu = $datousu['usu_nombre'];
			
			if($ventana == 4){ $actor = 'Sucursales'; }
			if($ventana == 7){ $actor = 'Inmobiliaria'; }
			if($ventana == 18){ $actor = 'Visita'; }
			if($ventana == 8){ $actor = 'Diseño'; }
			if($ventana == 19){ $actor = 'Licencia'; }
			if($ventana == 9){ $actor = 'Interventoria'; }
			if($ventana == 6){ $actor = 'Constructor'; }
			if($ventana == 10){ $actor = 'Seguridad'; }
			if($ventana == 20){ $actor = 'Facturación'; }
			
			if($i % 2 == 0)
			{
				echo '<div style="width:100%; border:2px; font-size: 14px; outline:none; border-radius:3px; -webkit-border-radius:6px; -moz-border-radius:3px; border:1px solid rgba(0,0,0, 0.5); padding: 3px; background-color: #CCCCCC"><strong>Nota:</strong> '.$dato['nou_nota']." <strong>Fecha:</strong> ".$dato['nou_fecha_creacion']." <strong>Actor:</strong> ".$actor." <strong>Usuario:</strong> ".$nomusu."<br></div>";
			}
			else
			{
				echo '<div style="width:100%; border:2px; font-size: 14px; outline:none; border-radius:3px; -webkit-border-radius:6px; -moz-border-radius:3px; border:1px solid rgba(0,0,0, 0.5); padding: 3px; background-color: #F0F0F0"><strong>Nota:</strong> '.$dato['nou_nota']." <strong>Fecha:</strong> ".$dato['nou_fecha_creacion']." <strong>Actor:</strong> ".$actor." <strong>Usuario:</strong> ".$nomusu."<br></div>";
			}
		}
		echo '<div id="editarnota"></div>';
		exit();
	}
	if($_GET['mostrarbitacoraadjuntos'] == 'si')
	{
		$clasuc = $_GET['clasuc'];
		
		$con = mysqli_query($conectar,"select * from permiso_actor where pea_actor = 'Seguridad'");
		$dato = mysqli_fetch_array($con);
		$adjsuc = $dato['pea_sw_adjunto_suc'];
		$adjinm = $dato['pea_sw_adjunto_inm'];
		$adjvis = $dato['pea_sw_adjunto_vis'];
		$adjdis = $dato['pea_sw_adjunto_dis'];
		$adjlic = $dato['pea_sw_adjunto_lic'];
		$adjint = $dato['pea_sw_adjunto_int'];
		$adjcon = $dato['pea_sw_adjunto_con'];
		$adjseg = $dato['pea_sw_adjunto_seg'];
		$adjfac = $dato['pea_sw_adjunto_fac'];
		
		if($adjsuc == 1){ $act1 = 1; }else{ $act1 = ''; }
		if($adjinm == 1){ $act2 = 2; }else{ $act2 = ''; }
		if($adjvis == 1){ $act3 = 3; }else{ $act3 = ''; }
		if($adjdis == 1){ $act4 = 4; }else{ $act4 = ''; }
		if($adjlic == 1){ $act5 = 5; }else{ $act5 = ''; }
		if($adjint == 1){ $act6 = 6; }else{ $act6 = ''; }
		if($adjcon == 1){ $act7 = 7; }else{ $act7 = ''; }
		if($adjseg == 1){ $act8 = 8; }else{ $act8 = ''; }
		if($adjfac == 1){ $act9 = 9; }else{ $act9 = ''; }
		
		$con = mysqli_query($conectar,"select * from adjunto_actor aa inner join tipo_adjunto ta on (aa.tad_clave_int = ta.tad_clave_int) inner join actor a on (a.act_clave_int = ta.act_clave_int) where aa.suc_clave_int = '".$clasuc."' and aa.ada_sw_eliminado = 0 and a.act_clave_int IN ('".$act1."','".$act2."','".$act3."','".$act4."','".$act5."','".$act6."','".$act7."','".$act8."','".$act9."') ORDER BY aa.ada_fecha_creacion DESC");
		$num = mysqli_num_rows($con);
		for($i = 0; $i < $num; $i++)
		{
			$dato = mysqli_fetch_array($con);
			$claada = $dato['ada_clave_int'];
			$nomadj = $dato['ada_nombre_adjunto'];
			$usuact = $dato['ada_usu_actualiz'];
			
			$conusu = mysqli_query($conectar,"select usu_nombre from usuario where usu_usuario = '".$usuact."'");
			$datousu = mysqli_fetch_array($conusu);
			$nomusu = $datousu['usu_nombre'];
			if($i % 2 == 0)
			{
			?>
				<div style="width:100%; border:2px; float:left; outline:none; border-radius:3px; -webkit-border-radius:6px; -moz-border-radius:3px; border:1px solid rgba(0,0,0, 0.5); padding: 3px; background-color: #CCCCCC;">
				
					<table style="width: 100%; height: 19px; font-size:14px">
						<tr>
							<td><strong>Adjunto: </strong><?php echo $nomadj; ?> <strong>Fec. Creación: </strong><?php echo $dato['ada_fecha_creacion']; ?><strong> Fec. Actualización: </strong><?php echo $dato['ada_fec_actualiz']; ?> <strong> Actor: </strong><?php echo $dato['act_nombre']; ?><strong> Usuario: </strong><?php echo $nomusu; ?></td>
							<td>
							<div style="float:left">
							<div class='ok' style='width: 50px; height:30px; float:left' align='center'>
							<a href='descargar.php?claada=<?php echo $claada; ?>&clacaj=<?php echo $clacaj; ?>' target='_blank'>
							<img src='../../images/descargar.png' style='width:25px; height:25px; cursor: pointer' /></a>
							</div>
							</div>
							</td>
							<?php
							if($actor < 5)
							{
							?>
							<?php 
							}
							?>
						</tr>
					</table>
				</div>
			<?php
			}
			else
			{
			?>
				<div style="width:100%; border:2px; outline:none; border-radius:3px; -webkit-border-radius:6px; -moz-border-radius:3px; border:1px solid rgba(0,0,0, 0.5); padding: 3px; background-color: #F0F0F0;">
				<table style="width: 100%; height: 19px; font-size:14px">
					<tr>
						<td><strong>Adjunto: </strong><?php echo $nomadj; ?> <strong>Fec. Creación: </strong><?php echo $dato['ada_fecha_creacion']; ?><strong> Fec. Actualización: </strong><?php echo $dato['ada_fec_actualiz']; ?><strong> Actor: </strong><?php echo $dato['act_nombre']; ?><strong> Usuario: </strong><?php echo $nomusu; ?></td>
						<td>
						<div style="float:left">
						<div class='ok' style='width: 50px; height:30px; float:left' align='center'>
						<a href='descargar.php?claada=<?php echo $claada; ?>&clacaj=<?php echo $clacaj; ?>' target='_blank'>
						<img src='../../images/descargar.png' style='width:25px; height:25px; cursor: pointer' /></a>
						</div>
						</div>
						</td>
						<?php
						if($actor < 5)
						{
						?>
						<?php 
						}
						?>
					</tr>
				</table>
			</div>
			<?php
			}
		}
		
		exit();
	}
	if($_GET['mostraradjuntos'] == 'si')
	{
		$clasuc = $_GET['clasuc'];
		$actor = $_GET['actor'];
		$con = mysqli_query($conectar,"select * from adjunto_actor where suc_clave_int = '".$clasuc."' and tad_clave_int = '".$actor."' and ada_sw_eliminado = 0 ORDER BY ada_fecha_creacion ASC");
		$num = mysqli_num_rows($con);
		if($actor == 19){ $tit = 'Adjunto información'; }elseif($actor == 20){ $tit = 'Adjunto facturación'; }
		echo "<h3>".$tit."</h3><hr>";
		for($i = 0; $i < $num; $i++)
		{
			$dato = mysqli_fetch_array($con);
			$claada = $dato['ada_clave_int'];
			$nomadj = $dato['ada_nombre_adjunto'];
			if($i % 2 == 0)
			{
			?>
				<div id="service<?php echo $claada; ?>" style="width:100%; border:2px; float:left; outline:none; border-radius:3px; -webkit-border-radius:6px; -moz-border-radius:3px; border:1px solid rgba(0,0,0, 0.5); padding: 3px; background-color: #CCCCCC;">
				
					<table style="width: 100%; height: 19px; font-size:14px">
						<tr>
							<td><strong>Adjunto: </strong><?php echo $nomadj; ?> <strong>Fec. Creación: </strong><?php echo $dato['ada_fecha_creacion']; ?><strong> Fec. Actualización: </strong><?php echo $dato['ada_fec_actualiz']; ?></td>
							<td>
							<div style="float:left">
							<div class='ok' style='width: 50px; height:30px; float:left' align='center'>
							<a href='descargar.php?claada=<?php echo $claada; ?>&clacaj=<?php echo $clacaj; ?>' target='_blank'>
							<img src='../../images/descargar.png' style='width:25px; height:25px; cursor: pointer' /></a>
							</div>
							</div>
							</td>
							<td>
							<img src="../../img/editar.png" onclick="EDITARADJUNTO('<?php echo $claada; ?>','<?php echo $actor; ?>')" alt="" height="25" width="25" style="cursor:pointer" />
							</td>
							<td>
							<button name="accion" onclick="ELIMINARADJUNTO('<?php echo $claada; ?>')" value="imprimir" type="button" style="cursor:pointer;">
							<img src="../../images/delete.png" height="15" width="15" class="auto-style13"></button>
							</td>
						</tr>
					</table>
				</div>
			<?php
			}
			else
			{
			?>
				<div id="service<?php echo $claada; ?>" style="width:100%; border:2px; outline:none; border-radius:3px; -webkit-border-radius:6px; -moz-border-radius:3px; border:1px solid rgba(0,0,0, 0.5); padding: 3px; background-color: #F0F0F0;">
				<table style="width: 100%; height: 19px; font-size:14px">
					<tr>
						<td><strong>Adjunto: </strong><?php echo $nomadj; ?> <strong>Fec. Creación: </strong><?php echo $dato['ada_fecha_creacion']; ?><strong> Fec. Actualización: </strong><?php echo $dato['ada_fec_actualiz']; ?></td>
						<td>
						<div style="float:left">
						<div class='ok' style='width: 50px; height:30px; float:left' align='center'>
						<a href='descargar.php?claada=<?php echo $claada; ?>&clacaj=<?php echo $clacaj; ?>' target='_blank'>
						<img src='../../images/descargar.png' style='width:25px; height:25px; cursor: pointer' /></a>
						</div>
						</div>
						</td>
						<td>
						<img src="../../img/editar.png" onclick="EDITARADJUNTO('<?php echo $claada; ?>','<?php echo $actor; ?>')" alt="" height="25" width="25" style="cursor:pointer" />
						</td>
						<td>
						<button name="accion" onclick="ELIMINARADJUNTO('<?php echo $claada; ?>')" value="imprimir" type="button" style="cursor:pointer;">
						<img src="../../images/delete.png" height="15" width="15" class="auto-style13"></button>
						</td>
					</tr>
				</table>
			</div>
			<?php
			}
		}
		?>
		<div id="editaradjunto"></div>
		<?php
		exit();
	}
	if($_GET['editaradjunto'] == 'si')
	{
		$claada = $_GET['claada'];
		$actor = $_GET['act'];
	?>
		<br>
		<div class="file-wrapper" style="float:left">
		<input name="archivoadjunto" id="archivoadjunto" type="file" onchange="MOSTRARRUTA('ADJUNTO')" class="file" style="cursor:pointer; right: 0; top: 0;" />
		<span class="button">Adjuntar Archivo</span>
		</div>
		<div style="float:left">
		<input name="subir" type="button" id="botonSubidor" onclick="ADJUNTARINFORMACION('ACTUALIZAARCHIVO','<?php echo $claada; ?>','<?php echo $actor; ?>');" value="Subir" class="btnSubmit" />
		</div>
		<div id="resultadoadjunto" style="float:left"></div>
	<?php
		exit();
	}
	if($_GET['estadoadjuntoinformacion'] == 'si')
	{
		$claada = $_GET['claada'];
		?>
			<div class='ok' style='width: 105px' align='center'>
			<?php
				$con = mysqli_query($conectar,"select ada_adjunto from adjunto_actor where ada_clave_int = '".$claada."'");
				$dato = mysqli_fetch_array($con);
				$adj = $dato['ada_adjunto'];
				
				if($adj != '')
				{
			?>
			<div class='ok' style='width: 25%' align='center'>
			<a href='descargar.php?claada=<?php echo $claada; ?>' target='_blank'>
			<img src='../../images/descargar.png' style='width:25px; height:25px; cursor: pointer' /></a>
			</div>
			<?php
				}
			?>
			</div>
		<?php
		exit();
	}
	if($_GET['vermodalidades1'] == 'si')
	{
		$tii = $_GET['tii'];
?>
		<select name="busmodalidad" id="busmodalidad" onchange="BUSCAR('CAJERO','')" tabindex="8" class="inputs" data-placeholder="-Seleccione-" style="width: 140px">
		<option value="">-Seleccione-</option>
		<?php
			$con = mysqli_query($conectar,"select * from modalidad where mod_clave_int in (select mod_clave_int from intervencion_modalidad where tii_clave_int = ".$tii.") order by mod_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clave = $dato['mod_clave_int'];
				$nombre = $dato['mod_nombre'];
		?>
			<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
		<?php
			}
		?>
		</select>

<?php
		exit();
	}
?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>SEGUIMIENTO CAJEROS</title>
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />

<?php //VALIDACIONES ?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="llamadas.js?<?php echo time();?>"></script>

<script type="text/javascript" src="../../js/jquery.searchabledropdown-1.0.8.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("select").searchable();
});
</script>
<?php //CALENDARIO ?>
<link type="text/css" rel="stylesheet" href="../../css/dhtmlgoodies_calendar.css?random=20051112" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../../js/dhtmlgoodies_calendar.js?random=20060118"></script>

<link rel="stylesheet" href="../../css/jquery-ui.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

<?php //ESTO ME PERMITE SUBIR ARCHIVOS DE TODO TIPO ?>	
<script language="javascript">
function ADJUNTAR(v)
{
	var clacaj = form1.clavecajero.value;
	var num = 0;
	$("#form1").on('submit',(function(e) {
		e.preventDefault();
		if(num == 0)
		{
			if(clacaj == '')
			{
				alert("Debe guardar los datos del cajero para poder adjuntar este archivo");
			}
			else
			{
				$.ajax({
		        	url: "upload.php?clacaj="+clacaj+"&actor="+v,
					type: "POST",
					data:  new FormData(this),
					contentType: false,
		    	    cache: false,
					processData:false,
					success: function(data)
				    {
					//$("#targetLayer").html(data);
						RESULTADOADJUNTO(v,clacaj);
				    },
				  	error: function() 
			    	{
			    	} 	        
			   });
			}
			num = 1;
		}
	}));
}
function ADJUNTARINFORMACION(v,ca,a)
{
	var clacaj = form1.clavecajero.value;
	
	var inputFileImage = document.getElementById("archivoadjunto");
	var file = inputFileImage.files[0];
	var data = new FormData();
	data.append('archivoadjunto',file);
	
	if(clacaj == '')
	{
		alert("Debe guardar los datos del cajero para poder adjuntar este archivo");
	}
	else
	{
		$.ajax({
        	url: "upload.php?clacaj="+clacaj+"&actor="+v+"&claada="+ca+"&archivo="+a,
			type: "POST",
			data: data,
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
			//$("#targetLayer").html(data);
				RESULTADOADJUNTOINFORMACION(ca);
		    },
		  	error: function() 
	    	{
	    	} 	        
	   });
	}
}
</script>
<script language="javascript1.2" type="text/javascript">
var IE = document.all ? true : false;
if (!IE) {
    document.captureEvents(Event.MOUSEMOVE);
}
document.onmousemove = getMouseXY;
var tempX = 0;
var tempY = 0;
//esta funcion no necesitas entender solo asigna la posicion del raton a tempX y tempY
function getMouseXY(e){
    if (IE) { //para IE
        tempX = event.clientX + document.body.scrollLeft;
        tempY = event.clientY + document.body.scrollTop;
    }
    else { //para netscape
        tempX = e.pageX;
        tempY = e.pageY;
    }
    if (tempX < 0) {
        tempX = 0;
    }
    if (tempY < 0) {
        tempY = 0;
    }
    return true;
}
function VentanaFlotante(mensaje, x, y){
        //creo el objeto div
        var div_fl = document.createElement('DIV');
        //le asigno que su posicion sera abosoluta
        div_fl.style.position = 'absolute';
        //le asigno el ide a la ventana
        div_fl.id = 'Miventana';
        //digo en que posicion left y top se creara a partir de la posicion del raton tempx tempy
        div_fl.style.left = tempX + 'px';
        div_fl.style.top = tempY + 'px';
        //asigno el ancho del div pasado por parametro
        div_fl.style.width = x + 'px';
        //asigno el alto del div pasado por parametro
        div_fl.style.height = y + 'px';
        //digo con css que el borde sera de grosor 1px solido y de color negro
        div_fl.style.border = "1px solid #000000";
        //asigno el color de fondo
        div_fl.style.backgroundColor = "#cccccc";
        //el objeto añado a la estrutura principal el document.body
        document.body.appendChild(div_fl);
        //el mensaje pasado por parametro muestro dentro del div
        div_fl.innerHTML = mensaje;
}
function quitarDiv()
{
//creo el objeto del div
var mv = document.getElementById('Miventana');
//elimino el objeto
document.body.removeChild(mv);
}
</script>

<script>
function OCULTARSCROLL()
{
	parent.autoResize('iframe10');
	setTimeout("parent.autoResize('iframe10')",500);
	setTimeout("parent.autoResize('iframe10')",1000);
	setTimeout("parent.autoResize('iframe10')",1500);
	setTimeout("parent.autoResize('iframe10')",2000);
	setTimeout("parent.autoResize('iframe10')",2500);
	setTimeout("parent.autoResize('iframe10')",3000);
	setTimeout("parent.autoResize('iframe10')",3500);
	setTimeout("parent.autoResize('iframe10')",4000);
	setTimeout("parent.autoResize('iframe10')",4500);
	setTimeout("parent.autoResize('iframe10')",5000);
	setTimeout("parent.autoResize('iframe10')",5500);
	setTimeout("parent.autoResize('iframe10')",6000);
	setTimeout("parent.autoResize('iframe10')",6500);
	setTimeout("parent.autoResize('iframe10')",7000);
	setTimeout("parent.autoResize('iframe10')",7500);
	setTimeout("parent.autoResize('iframe10')",8000);
	setTimeout("parent.autoResize('iframe10')",8500);
	setTimeout("parent.autoResize('iframe10')",9000);
	setTimeout("parent.autoResize('iframe10')",9500);
	setTimeout("parent.autoResize('iframe10')",10000);
}
parent.autoResize('iframe10');
setTimeout("parent.autoResize('iframe10')",500);
setTimeout("parent.autoResize('iframe10')",1000);
setTimeout("parent.autoResize('iframe10')",1500);
setTimeout("parent.autoResize('iframe10')",2000);
setTimeout("parent.autoResize('iframe10')",2500);
setTimeout("parent.autoResize('iframe10')",3000);
setTimeout("parent.autoResize('iframe10')",3500);
setTimeout("parent.autoResize('iframe10')",4000);
setTimeout("parent.autoResize('iframe10')",4500);
setTimeout("parent.autoResize('iframe10')",5000);
setTimeout("parent.autoResize('iframe10')",5500);
setTimeout("parent.autoResize('iframe10')",6000);
setTimeout("parent.autoResize('iframe10')",6500);
setTimeout("parent.autoResize('iframe10')",7000);
setTimeout("parent.autoResize('iframe10')",7500);
setTimeout("parent.autoResize('iframe10')",8000);
setTimeout("parent.autoResize('iframe10')",8500);
setTimeout("parent.autoResize('iframe10')",9000);
setTimeout("parent.autoResize('iframe10')",9500);
setTimeout("parent.autoResize('iframe10')",10000);
function CAMBIARESTADO(v)
{
	form1.ocultoestado.value = v;
}
</script>

</head>
<body>
<form name="form1" id="form1" method="post">
<!--[if lte IE 7]>
<div class="ieWarning">Este navegador no es compatible con el sistema. Por favor, use Chrome, Safari, Firefox o Internet Explorer 8 o superior.</div>
<![endif]-->
	<input name="ocultoestado" id="ocultoestado" value="1" type="hidden" />
	<table style="width: 100%">
		<tr>
			<td class="auto-style1" style="cursor:pointer" colspan="2" onclick="MODULO('TODOS','1'),CAMBIARESTADO('1')" onmouseover="this.style.backgroundColor='#5a825a';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';">Ver Activos
			<?php
			if($claprf == 1 || $mostrarcajeros == 1)
			{
				$con = mysqli_query($conectar,"select * from cajero where caj_estado_proyecto = 1 and caj_sw_eliminado = 0");
			}
			else
			{
				$con = mysqli_query($conectar,"select * from cajero c inner join cajero_interventoria ci on (ci.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cs on (cs.caj_clave_int = c.caj_clave_int) where (cs.cas_instalador = '".$clausu."' OR cs.cas_instalador IN (select usa_actor from usuario_actor where usu_clave_int = '".$clausu."')) and caj_estado_proyecto = 1 and c.caj_sw_eliminado = 0");
			}
			
			$num = mysqli_num_rows($con);
			echo $num;
			?>
			</td>
			<td class="auto-style2" style="width: 100px; cursor:pointer" onclick="MODULO('TODOS','5'),CAMBIARESTADO('5')" onmouseover="this.style.backgroundColor='#5a825a';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';">
			<div id="programados">
			Ver Programados
			<?php
			if($claprf == 1 || $mostrarcajeros == 1)
			{
				$con = mysqli_query($conectar,"select * from cajero where caj_estado_proyecto = 5 and caj_sw_eliminado = 0");
			}
			else
			{
				$con = mysqli_query($conectar,"select * from cajero c inner join cajero_interventoria ci on (ci.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cs on (cs.caj_clave_int = c.caj_clave_int) where (cs.cas_instalador = '".$clausu."' OR cs.cas_instalador IN (select usa_actor from usuario_actor where usu_clave_int = '".$clausu."')) and caj_estado_proyecto = 5 and c.caj_sw_eliminado = 0");
			}
			$num = mysqli_num_rows($con);
			echo $num;
			?>
			</div>
			</td>
			<td class="auto-style2" style="width: 100px; cursor:pointer" onclick="MODULO('TODOS','2'),CAMBIARESTADO('2')" onmouseover="this.style.backgroundColor='#5a825a';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';">
			Ver Suspendidos
			<?php
			if($claprf == 1 || $mostrarcajeros == 1)
			{
				$con = mysqli_query($conectar,"select * from cajero where caj_estado_proyecto = 2 and caj_sw_eliminado = 0");
			}
			else
			{
				$con = mysqli_query($conectar,"select * from cajero c inner join cajero_interventoria ci on (ci.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cs on (cs.caj_clave_int = c.caj_clave_int) where (cs.cas_instalador = '".$clausu."' OR cs.cas_instalador IN (select usa_actor from usuario_actor where usu_clave_int = '".$clausu."')) and caj_estado_proyecto = 2 and c.caj_sw_eliminado = 0");
			}
			
			$num = mysqli_num_rows($con);
			echo $num;
			?>
			</td>
			<td class="auto-style2" style="width: 100px; cursor:pointer" onclick="MODULO('TODOS','3'),CAMBIARESTADO('3')" onmouseover="this.style.backgroundColor='#5a825a';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';">
			Entregados
			<?php
			if($claprf == 1 || $mostrarcajeros == 1)
			{
				$con = mysqli_query($conectar,"select * from cajero where caj_estado_proyecto = 3 and caj_sw_eliminado = 0");
			}
			else
			{
				$con = mysqli_query($conectar,"select * from cajero c inner join cajero_interventoria ci on (ci.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cs on (cs.caj_clave_int = c.caj_clave_int) where (cs.cas_instalador = '".$clausu."' OR cs.cas_instalador IN (select usa_actor from usuario_actor where usu_clave_int = '".$clausu."')) and caj_estado_proyecto = 3 and c.caj_sw_eliminado = 0");
			}
			
			$num = mysqli_num_rows($con);
			echo $num;
			?>
			</td>
			<td class="auto-style2" style="width: 100px; cursor:pointer" onclick="MODULO('TODOS','4'),CAMBIARESTADO('4')" onmouseover="this.style.backgroundColor='#5a825a';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';">
			Cancelados
			<?php
			if($claprf == 1 || $mostrarcajeros == 1)
			{
				$con = mysqli_query($conectar,"select * from cajero where caj_estado_proyecto = 4 and caj_sw_eliminado = 0");
			}
			else
			{
				$con = mysqli_query($conectar,"select * from cajero c inner join cajero_interventoria ci on (ci.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cs on (cs.caj_clave_int = c.caj_clave_int) where (cs.cas_instalador = '".$clausu."' OR cs.cas_instalador IN (select usa_actor from usuario_actor where usu_clave_int = '".$clausu."')) and caj_estado_proyecto = 4 and c.caj_sw_eliminado = 0");
			}
						
			$num = mysqli_num_rows($con);
			echo $num;
			?>
			</td>
		</tr>
		<tr>
			<td class="auto-style2" colspan="6" align="center">
			<div id="cajeros">
			<?php
			$est = 1;
			?>
			<table>
			<tr>
				<td align="left" class="auto-style2">
				<table style="width: 50%">
						<tr>
							<td style="width: 60px" rowspan="2"><strong>
							<span class="auto-style13">Filtro:</span><img src="../../img/buscar.png" alt="" height="18" width="15" /></strong></td>
							<td style="width: 150px; height: 31px; display:none">
							<input class="inputs" onkeyup="BUSCAR('CAJERO','')" name="busconsecutivo" id="busconsecutivo" maxlength="70" type="text" placeholder="Consecutivo" style="width: 150px" />
							</td>
							<td style="width: 150px; height: 31px;">
							<input class="inputs" onkeyup="BUSCAR('CAJERO','')" name="busnombre" id="busnombre" maxlength="70" type="text" placeholder="Nombre Cajero" style="width: 150px" />
							</td>
							<td style="width: 150px; height: 31px;">
							<input class="inputs" onkeyup="BUSCAR('CAJERO','')" name="busanocontable" id="busanocontable" maxlength="70" type="text" placeholder="Año Contable" style="width: 150px" />
							</td>
							<td style="width: 150px; height: 31px;" class="alinearizq">
							<select name="buscentrocostos" id="buscentrocostos" onchange="BUSCAR('CAJERO','')" tabindex="4" class="inputs" style="width: 158px">
							<option value="">-Centro Costos-</option>
							<?php
								$con = mysqli_query($conectar,"select * from centrocostos where cco_sw_activo = 1 order by cco_nombre");
								$num = mysqli_num_rows($con);
								for($i = 0; $i < $num; $i++)
								{
									$dato = mysqli_fetch_array($con);
									$clave = $dato['cco_clave_int'];
									$nombre = $dato['cco_nombre'];
							?>
								<option value="<?php echo $clave; ?>" <?php if($clave == $cencos){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
							<?php
								}
							?>
							</select>
							</td>
							<td align="left" style="height: 31px">
							<input class="auto-style5" onkeyup="BUSCAR('CAJERO','')" name="buscodigo" id="buscodigo" maxlength="70" type="text" placeholder="Código Cajero" style="width: 150px" />
							</td>
                            <td class="alinearizq">
                                <select name="busproyecto" id="busproyecto" onchange="BUSCAR('CAJERO','')" tabindex="6" class="inputs" style="width: 230px">
                                    <option value="">-Proyecto-</option>
                                    <?php
                                    $con = mysqli_query($conectar,"select * from proyecto where pro_sw_activo = 1 order by pro_nombre");
                                    $num = mysqli_num_rows($con);
                                    for($i = 0; $i < $num; $i++)
                                    {
                                        $dato = mysqli_fetch_array($con);
                                        $clave = $dato['pro_clave_int'];
                                        $nombre = $dato['pro_nombre'];
                                        ?>
                                        <option value="<?php echo $clave; ?>" <?php if($clave == $proyec){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>

                            </td>
						</tr>
						<tr>
							<td style="width: 150px; height: 31px;">
							<select name="busregion" id="busregion" onchange="BUSCAR('CAJERO','')" tabindex="6" class="inputs" style="width: 148px">
								<option value="">-Región-</option>
								<?php
									$con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo IS NULL and pog_nieto IS NULL order by pog_nombre");
									$num = mysqli_num_rows($con);
									for($i = 0; $i < $num; $i++)
									{
										$dato = mysqli_fetch_array($con);
										$clave = $dato['pog_clave_int'];
										$nombre = $dato['pog_nombre'];
								?>
									<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
								<?php
									}
								?>
								</select></td>
							<td style="width: 150px; height: 31px;">
								<select name="busmunicipio" id="busmunicipio" onchange="BUSCAR('CAJERO','')" tabindex="8" class="inputs" style="width: 148px">
								<option value="">-Municipio-</option>
								<?php
									$con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo IS NULL and pog_nieto IS NOT NULL order by pog_nombre");
									$num = mysqli_num_rows($con);
									for($i = 0; $i < $num; $i++)
									{
										$dato = mysqli_fetch_array($con);
										$clave = $dato['pog_clave_int'];
										$nombre = $dato['pog_nombre'];
								?>
									<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
								<?php
									}
								?>
								</select></td>
							<td style="width: 150px; height: 31px;">
								<select name="bustipologia" id="bustipologia" onchange="BUSCAR('CAJERO','')" tabindex="9" class="inputs" style="width: 148px">
								<option value="">-Tipología-</option>
								<?php
									$con = mysqli_query($conectar,"select * from tipologia where tip_sw_activo = 1 order by tip_nombre");
									$num = mysqli_num_rows($con);
									for($i = 0; $i < $num; $i++)
									{
										$dato = mysqli_fetch_array($con);
										$clave = $dato['tip_clave_int'];
										$nombre = $dato['tip_nombre'];
								?>
									<option value="<?php echo $clave; ?>" <?php if($clave == $tip){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
								<?php
									}
								?>
								</select></td>
							<td style="width: 150px; height: 31px;" class="alinearizq">
							<select name="bustipointervencion" id="bustipointervencion" onchange="BUSCAR('CAJERO','');VERMODALIDADES1(this.value)" tabindex="10" class="inputs" style="width: 158px">
							<option value="">Tipo Intervención</option>
							<?php
								$con = mysqli_query($conectar,"select * from tipointervencion order by tii_nombre");
								$num = mysqli_num_rows($con);
								for($i = 0; $i < $num; $i++)
								{
									$dato = mysqli_fetch_array($con);
									$clave = $dato['tii_clave_int'];
									$nombre = $dato['tii_nombre'];
							?>
								<option value="<?php echo $clave; ?>" <?php if($clave == $tipint){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
							<?php
								}
							?>
							</select></td>
							<td align="left" style="height: 31px">
							<div id="modalidades1" style="float:left">
							<select name="busmodalidad" id="busmodalidad" onchange="BUSCAR('CAJERO','')" tabindex="8" class="inputs" data-placeholder="-Seleccione-" style="width: 160px">
							<option value="">-Modalidad-</option>
							<?php
								$con = mysqli_query($conectar,"select * from modalidad order by mod_nombre");
								$num = mysqli_num_rows($con);
								for($i = 0; $i < $num; $i++)
								{
									$dato = mysqli_fetch_array($con);
									$clave = $dato['mod_clave_int'];
									$nombre = $dato['mod_nombre'];
							?>
								<option value="<?php echo $clave; ?>" <?php if($moda == $clave){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
							<?php
								}
							?>
							</select>
							</div>
							</td>
							<td align="left" style="height: 31px">
							<select name="busactor" id="busactor" onchange="BUSCAR('CAJERO','')" tabindex="20" class="inputs" style="width: 150px">
							<option value="">-Actor-</option>
							<?php
								if($claprf == 1)
								{
									$con = mysqli_query($conectar,"select * from usuario where usu_sw_inmobiliaria = 1 or usu_sw_visita = 1 or usu_sw_diseno = 1 or usu_sw_licencia = 1 or usu_sw_interventoria = 1 or usu_sw_constructor = 1 or usu_sw_seguridad = 1 order by usu_nombre");
								}
								else
								{
									$con = mysqli_query($conectar,"select * from usuario where (usu_categoria = 2 and usu_sw_seguridad = 1 and usu_sw_activo = 1) OR (usu_clave_int NOT IN (select usu_clave_int from usuario_actor) and usu_sw_seguridad = 1 and usu_sw_activo = 1) order by usu_nombre");
								}
								$num = mysqli_num_rows($con);
								for($i = 0; $i < $num; $i++)
								{
									$dato = mysqli_fetch_array($con);
									$clave = $dato['usu_clave_int'];
									$nombre = $dato['usu_nombre'];
							?>
								<option value="<?php echo $clave; ?>" <?php if($clave == $inmob){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
							<?php
								}
							?>
							</select>
							</td>
						</tr>
						</table>
				</td>
			</tr>
			<tr>
				<td class="auto-style2">
				<div id="busqueda" style="overflow:auto;width: 1226px">
				<table style="width: 100%">
				<tr>
					<td class="alinearizq">&nbsp;</td>
					<td class="alinearizq"><strong>Consecutivo</strong></td>
					<td class="alinearizq"><strong>Código Cajero</strong></td>
					<td class="alinearizq"><strong>Nombre</strong></td>
					<td class="alinearizq"><strong>Padre/Hijo</strong></td>
					<td class="alinearizq"><strong>Región</strong></td>
					<td class="alinearizq"><strong>Tipología</strong></td>
					<td class="alinearizq"><strong>Tip. Intervención</strong></td>
					<td class="alinearizq"><strong>Estado</strong></td>
				</tr>
				<?php
				if($claprf == 1 || $mostrarcajeros == 1)
				{
					$query = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int)  inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where c.caj_estado_proyecto = '".$est."' and c.caj_sw_eliminado = 0");
					//$res = $con->query($query);
					$num_registros = mysqli_num_rows($query);
			
					$resul_x_pagina = 50;
					//Paginar:
					$paginacion = new Zebra_Pagination();
					$paginacion->records($num_registros);
					$paginacion->records_per_page($resul_x_pagina);
					
					$con = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int)  inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where c.caj_estado_proyecto = '".$est."' and c.caj_sw_eliminado = 0 LIMIT ".(($paginacion->get_page() - 1) * $resul_x_pagina). ',' .$resul_x_pagina);
				}
				else
				{
					$query = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int)  inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where c.caj_estado_proyecto = '".$est."' and (cajseg.cas_instalador = '".$clausu."' OR cajseg.cas_instalador IN (select usa_actor from usuario_actor where usu_clave_int = '".$clausu."')) and c.caj_sw_eliminado = 0");
					//$res = $con->query($query);
					$num_registros = mysqli_num_rows($query);
			
					$resul_x_pagina = 50;
					//Paginar:
					$paginacion = new Zebra_Pagination();
					$paginacion->records($num_registros);
					$paginacion->records_per_page($resul_x_pagina);
					
					$con = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int)  inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where c.caj_estado_proyecto = '".$est."' and (cajseg.cas_instalador = '".$clausu."' OR cajseg.cas_instalador IN (select usa_actor from usuario_actor where usu_clave_int = '".$clausu."')) and c.caj_sw_eliminado = 0 LIMIT ".(($paginacion->get_page() - 1) * $resul_x_pagina). ',' .$resul_x_pagina);
				}		
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					//Info Base
					$clacaj = $dato['clacaj'];
					$nomcaj = $dato['caj_nombre'];
					$dir = $dato['caj_direccion'];
					$anocon = $dato['caj_ano_contable'];
					$reg = $dato['caj_region'];
					$dep = $dato['caj_departamento'];
					$mun = $dato['caj_municipio'];
					$tip = $dato['tip_clave_int'];
					$tipint = $dato['tii_clave_int'];
					$mod = $dato['mod_clave_int'];
					$conmod = mysqli_query($conectar,"select mod_nombre from modalidad where mod_clave_int = '".$mod."'");
					$datomod = mysqli_fetch_array($conmod);
					$moda = $datomod['mod_nombre'];
					//Info Secun
					$codcaj = $dato['caj_codigo_cajero'];
					$cencos = $dato['cco_clave_int'];
					$refmaq = $dato['rem_clave_int'];
					$ubi = $dato['ubi_clave_int'];
					$codsuc = $dato['caj_codigo_suc'];
					$ubiamt = $dato['caj_ubicacion_atm'];
					$ati = $dato['ati_clave_int'];
					$rie = $dato['caj_riesgo'];
					$are = $dato['are_clave_int'];
					$codrec = $dato['caj_cod_recibido_monto'];
					$codrec = $dato['caj_fecha_creacion'];
					//Info Inmobiliaria
					$reqinm = $dato['cai_req_inmobiliaria'];
					$inmob = $dato['cai_inmobiliaria'];
					$feciniinmob = $dato['cai_fecha_ini_inmobiliaria'];
					$estinmob = $dato['esi_clave_int'];
					$fecentinmob = $dato['cai_fecha_entrega_info_inmobiliaria'];
					//Info Visita Local
					$reqvis = $dato['cav_req_visita_local'];
					$vis = $dato['cav_visitante'];
					$fecvis = $dato['cav_fecha_visita'];
					//Info Diseño
					$reqdis = $dato['cad_req_diseno'];
					$dis = $dato['cad_disenador'];
					$fecinidis = $dato['cad_fecha_inicio_diseno'];
					$estdis = $dato['esd_clave_int'];
					$fecentdis = $dato['cad_fecha_entrega_info_diseno'];
					//Info Licencia
					$reqlic = $dato['cal_req_licencia'];
					$lic = $dato['cal_gestionador'];
					$fecinilic = $dato['cal_fecha_inicio_licencia'];
					$estlic = $dato['esl_clave_int'];
					$fecentlic = $dato['cal_fecha_entrega_info_licencia'];
					//Info Interventoria
					$int = $dato['cin_interventor'];
					$fecteoent = $dato['cin_fecha_teorica_entrega'];
					$feciniobra = $dato['cin_fecha_inicio_obra'];
					$fecpedsum = $dato['cin_fecha_pedido_suministro'];
					$aprovcotiz = $dato['cin_sw_aprov_cotizacion'];
					$cancom = $dato['can_clave_int'];
					$aprovliqui = $dato['cin_sw_aprov_liquidacion'];
					$swimgnex = $dato['cin_sw_img_nexos'];
					$swfot = $dato['cin_sw_fotos'];
					$swact = $dato['cin_sw_actas'];
					$estpro = $dato['caj_estado_proyecto'];
					//Info Constructor
					$cons = $dato['cac_constructor'];
					$cons = $dato['cac_fecha_entrega_atm'];
					$cons = $dato['cac_porcentaje_avance'];
					$cons = $dato['cac_fecha_entrega_cotizacion'];
					$cons = $dato['cac_fecha_entrega_liquidacion'];
					//Info Instalador Seguridad
					$seg = $dato['cas_instalador'];
					$fecingseg = $dato['cas_fecha_ingreso_obra_inst'];
					$codmon = $dato['cas_codigo_monitoreo'];
					
					$swpad = $dato['caj_sw_padre'];
					$hijo = $dato['caj_padre'];
				?>
				<tr <?php if($i % 2 == 0){ echo 'style="background-color:#eeeeee;"'; } ?>>
					<td class="alinearizq"><img src="../../img/editar.png" onclick="VERREGISTRO('<?php echo $clacaj; ?>')" alt="" height="25" width="25" style="cursor:pointer" /></td>
					<td class="alinearizq"><?php echo $clacaj; ?></td>
					<td class="alinearizq"><?php echo $codcaj; ?></td>
					<td class="alinearizq"><?php echo $nomcaj; ?></td>
					<td class="alinearizq">
					<?php
					if($swpad == 1 and ($hijo == 0 or $hijo == ''))
					{
						$conhij = mysqli_query($conectar,"select caj_clave_int,caj_nombre from cajero where caj_padre = ".$clacaj."");
						$numhij = mysqli_num_rows($conhij);
					?>
						<div style="cursor:pointer" onmouseover="javascript: VentanaFlotante('<?php for($j = 0; $j < $numhij; $j++){ $datohij = mysqli_fetch_array($conhij); echo $datohij['caj_clave_int']." - ".$datohij['caj_nombre']."<br>"; } ?>', 300, <?php echo 35*$numhij; ?>)" onmouseout="javascript: quitarDiv();">Padre</div>
					<?php
					}
					else
					if($hijo > 0)
					{
						$conpad = mysqli_query($conectar,"select caj_clave_int,caj_nombre from cajero where caj_clave_int = ".$hijo."");
						$datopad = mysqli_fetch_array($conpad);
					?>
						<div style="cursor:pointer" onmouseover="javascript: VentanaFlotante('<?php echo $datopad['caj_clave_int']." - ".$datopad['caj_nombre']."<br>"; ?>', 200, 35)" onmouseout="javascript: quitarDiv();">Hijo</div>
					<?php
					}
					else
					{
						echo "<div>Ninguno</div>";
					}
					?>
					</td>
					<td class="alinearizq">
					<?php 
						$sql = mysqli_query($conectar,"select pog_nombre from posicion_geografica where pog_clave_int = '".$reg."'");
						$datosql = mysqli_fetch_array($sql);
						$nomreg = $datosql['pog_nombre'];
						echo $nomreg; 
					?>
					</td>

					<td class="alinearizq">
					<?php 
						$sql = mysqli_query($conectar,"select tip_nombre from tipologia where tip_clave_int = '".$tip."'");
						$datosql = mysqli_fetch_array($sql);
						$nomtip = $datosql['tip_nombre'];
						echo $nomtip;
					?>
					</td>
					<td class="alinearizq">
					<?php 
						$sql = mysqli_query($conectar,"select tii_nombre from tipointervencion where tii_clave_int = '".$tipint."'");
						$datosql = mysqli_fetch_array($sql);
						$nomtii = $datosql['tii_nombre'];
						echo $nomtii;
					?>
					</td>

					<td class="alinearizq"><?php if($estpro == 1){ echo "ACTIVO"; }elseif($estpro == 2){ echo "SUSPENDIDO"; }elseif($estpro == 3){ echo "ENTREGADO"; }elseif($estpro == 4){ echo "CANCELADO"; } ?></td>
				</tr>
				<?php
				}
				?>
				</table>
				<?php $paginacion->render(); ?>
				</div>
			</td>
			</tr>
			</table>
			</div>
			</td>
		</tr>
		<tr>
			<td style="width: 100px">&nbsp;</td>
			<td style="width: 10px">&nbsp;</td>
			<td style="width: 100px">&nbsp;</td>
			<td style="width: 43px">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
	<script>
	function VERADJUNTOS(p)
	{
		$("#adjuntoarchivos").dialog({ <!--  ------> muestra la ventana  -->
			width: 650,  <!-- -------------> ancho de la ventana -->
			height: 350,<!--  -------------> altura de la ventana -->
			show: "scale", <!-- -----------> animación de la ventana al aparecer -->
			hide: "scale", <!-- -----------> animación al cerrar la ventana -->
			resizable: "false", <!-- ------> fija o redimensionable si ponemos este valor a "true" -->
			position: p,<!--  ------> posicion de la ventana en la pantalla (left, top, right...) -->
			modal: "true" <!-- ------------> si esta en true bloquea el contenido de la web mientras la ventana esta activa (muy elegante) -->
		});
	}
	</script>
	<div name="adjuntoarchivos" id="adjuntoarchivos" class="ventana" title="Adjuntos agregados">
	<br>
		<div class="file-wrapper" style="float:left">
		<input name="archivoadjunto" id="archivoadjunto" type="file" onchange="MOSTRARRUTA('ADJUNTO')" class="file" style="cursor:pointer; right: 0; top: 0;" />
		<span class="button">Adjuntar Archivo</span>
		</div>
		<div style="float:left">
		<input name="subir" type="button" id="botonSubidor" onclick="ADJUNTARINFORMACION('ACTUALIZAARCHIVO','<?php echo $claada; ?>','<?php echo $actor; ?>');" value="Subir" class="btnSubmit" />
		</div>
		<div id="resultadoadjunto" style="float:left"></div>
	</div>
	<script type="text/javascript" language="javascript">
	function VERNOTAS()
	{
		$("#todaslasnotas").dialog({ <!--  ------> muestra la ventana  -->
			width: 590,  <!-- -------------> ancho de la ventana -->
			height: 350,<!--  -------------> altura de la ventana -->
			show: "scale", <!-- -----------> animación de la ventana al aparecer -->
			hide: "scale", <!-- -----------> animación al cerrar la ventana -->
			resizable: "false", <!-- ------> fija o redimensionable si ponemos este valor a "true" -->
			position: "top",<!--  ------> posicion de la ventana en la pantalla (left, top, right...) -->
			modal: "true" <!-- ------------> si esta en true bloquea el contenido de la web mientras la ventana esta activa (muy elegante) -->
		});
	}
    </script>
    <div name="todaslasnotas" id="todaslasnotas" class="ventana" title="Notas Agregadas">	
	</div>
	<script type="text/javascript" language="javascript">
	function VERADJUNTOSBITACORA()
	{
		$("#todoslosadjuntosbitacora").dialog({ <!--  ------> muestra la ventana  -->
			width: 590,  <!-- -------------> ancho de la ventana -->
			height: 350,<!--  -------------> altura de la ventana -->
			show: "scale", <!-- -----------> animación de la ventana al aparecer -->
			hide: "scale", <!-- -----------> animación al cerrar la ventana -->
			resizable: "false", <!-- ------> fija o redimensionable si ponemos este valor a "true" -->
			position: "top",<!--  ------> posicion de la ventana en la pantalla (left, top, right...) -->
			modal: "true" <!-- ------------> si esta en true bloquea el contenido de la web mientras la ventana esta activa (muy elegante) -->
		});
	}
    </script>
    <div name="todoslosadjuntosbitacora" id="todoslosadjuntosbitacora" class="ventana" title="Bitácora de adjuntos">	
	</div>
</form>
</body>
</html>