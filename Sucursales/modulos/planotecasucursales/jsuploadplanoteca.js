	function subirArchivos() 
	{
		//$('#progreso').css('display','none');
		//$('#publishform').html('');

		
			var formData = new FormData($(".formulario1")[0]);
			var message = ""; 			
			//hacemos la petición ajax  
			$.ajax({
				url: 'montar_archivo.php',
				type: 'POST',
				// Form data
				//datos del formulario
				data: formData,
				//necesario para subir archivos via ajax
				cache: false,
				contentType: false,
				processData: false,
				//mientras enviamos el archivo
				beforeSend: function(){
				$('#resultadoadjuntoformatoplano').html('<div class="alert alert-info">Guardando archivo, por favor espere...</div>');
				//message = "Guardando Gestión, por favor espere...";
				//showMessage(message)         
				},
				//una vez finalizado correctamente
					success: function(data){

					if(data=="error3")
					{
						message = "Formato De Archivo de antes no Permitido .Verificar";
						mostrarRespuesta(message, false);
					}
					else
					if(data=="error4")
					{
						message = "Seleccionar el archivo a importar.Verificar";
						mostrarRespuesta(message, false);
					}
					else
					if(data!="")
					{
						
						$('#resultadoadjuntoformatoplano').html("Analizando Archivo Por favor Espere...");
						//$('#progreso').focus();
						console.log(data);
						ejecutarArchivos(data);
						//mostrarArchivos();
					}			
					//mensaje de error adicionales
				},
				//si ha ocurrido un error
				error: function(){
					message = "Ha ocurrido un error";
					mostrarRespuesta(message, false);
				}
			});
	   }
	function eliminarArchivos(archivo) 
	{
         $.ajax({
			url: 'eliminar_archivo.php',
			type: 'POST',
			timeout: 10000,
			data: {archivo: archivo},
			error: function() {
				mostrarRespuesta('Error al intentar eliminar el archivo.', false);
                },
				success: function(respuesta) {
				if (respuesta == 1) 
				{
					mostrarRespuesta('El archivo ha sido eliminado.', true);
				} 
				else 
				{
					mostrarRespuesta('Error al intentar eliminar el archivo.', false);                            
				}
                   // mostrarArchivos();
               }
             });
            }
			function ejecutarArchivos(archivo) 
			{
				//$('#progreso').css('display','block');
				$.post('ejecutar_archivo.php',{archivo:archivo},function(data)
					{
						//console.log("Nombre de Archivo: "+archivo);
						$('#resultadoadjuntoformatoplano').html(data);
					}
				)
			}
			function CAMBIARPROGESO(e)
			{
				if(e=="error")
				{
					$('#resultadoadjuntoformatoplano').html("Surgieron Errores En el archivo a Importar. Verificar");
				}
				else if(e=="ok")
				{
					$('#progreso span').html("Guardando Archivo.Por Favor Espere...");
					$('#archivoplano').replaceWith( $('#archivo').val('').clone( true ) );

					//$('#resultadoadjuntocomparativopropuestas').hide(10000);

					var message = "Archivo Montado exitosamente";
					setTimeout(mostrarRespuesta(message, true),1000);
				}
			}
			function OCULTARPROGRESO()
			{
						

			}
            function mostrarArchivos() {
                $.ajax({
                    url: 'mostrar_archivos.php',
                    dataType: 'JSON',
                    success: function(respuesta) {
                        if (respuesta) {
                            var html = '';
                            for (var i = 0; i < respuesta.length; i++) {
                                if (respuesta[i] != undefined) {
                                    html += '<div class="row"><span class="col-md-4">'+respuesta[i]+'</span> <div class="col-md-1"> <a class="eliminar_archivo btn btn-danger" role="buttom" href="javascript:void(0);"><i class="glyphicon glyphicon-trash"></i> </a> </div></div><hr class="col-md-12"/>';
                                }
                            }
                            $("#archivos_subidos").html(html);
                        }
                    }
                });
            }
            function mostrarRespuesta(mensaje, ok1){
                //$("#respuesta").removeClass('alert-success').removeClass('alert-danger').html(mensaje);
                if(ok1){
					$('#resultadoadjuntoformatoplano').html(mensaje);
					//$('#cargando').html("");
                   // $("#respuesta").addClass('alert-success');
					//setTimeout(OCULTARPROGRESO(),1000);
                }else{
					alert(mensaje);
                    //$("#respuesta").addClass('alert-danger');
					//$('#cargando').html("");
					//$('#textcarga').html('0/100 %');
                }
				//setTimeout("$('#respuesta').html('').removeClass('alert-success').removeClass('alert-danger')",10000);
            }
            $(document).ready(function() {
                //mostrarArchivos();
               
                $("#archivos_subidos").on('click', '.eliminar_archivo', function() {					
                    var archivo = $(this).parents('.row').eq(0).find('span').text();
                    archivo = $.trim(archivo);
                    eliminarArchivos(archivo);
                });				
            });