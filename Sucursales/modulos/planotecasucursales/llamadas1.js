function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }

function MOSTRARRUTA(v) {
    var div = 'resultadoadjuntoformatoplano';
    var nomadj = form1.archivoplano.value;//$('#adjuntocasonegocio').val();
    var ajax;
    ajax=new ajaxFunction();
    ajax.onreadystatechange=function()
    {
        if(ajax.readyState==4)
        {
            document.getElementById(div).innerHTML=ajax.responseText;
        }
    }
    jQuery("#"+div).html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
    ajax.open("GET","?mostrarruta=si&nomadj="+nomadj,true);
    ajax.send(null);
}
function BUSCAR(p)
{	
	var d1 = form1.planoteca.value;
	var d2 = form1.tipologia.value;
	var d3 = form1.codigo.value;
	var d4 = form1.nombre.value;
	var d5 = form1.direccion.value;
	var d6 = form1.departamento.value;
	var d7 = form1.municipio.value;
	var d8 = form1.region.value;
	var d9 = form1.propiedad.value;
	var d10 = form1.estado.value;
	var d11 = form1.planos.value;
	var d12 = form1.anointerv.value;
	var d13 = form1.cumpleaccesibilidad.value;
	var d14 = form1.elementosaccesibilidad.value;
	var d15 = form1.notasaccesibilidad.value;
	var d16 = form1.apertura.value;
	var d17 = form1.hallautoservicio.value;
	var d18 = form1.arealote.value;
	var d19 = form1.sotano.value;
	var d20 = form1.parqueadero.value;
	var d21 = form1.piso1.value;
	var d22 = form1.piso2.value;
	var d23 = form1.piso3.value;
	var d24 = form1.piso4.value;
	var d25 = form1.piso5.value;
	var d26 = form1.otros.value;
	var d27 = form1.areatotal.value;
	var d28 = form1.areacons.value;
	var d29 = form1.areaconsporcentaje.value;
	var d30 = form1.totalareapublico.value;
	var d31 = form1.totalareapublicoporcentaje.value;
	var d32 = form1.autoservicios.value;
	var d33 = form1.autoserviciosporcentaje.value;
	var d34 = form1.clienteasesoria.value;
	var d35 = form1.clienteasesoriaporcentaje.value;
	var d36 = form1.clientecajas.value;
	var d37 = form1.clientecajasporcentaje.value;
	var d38 = form1.puestodetrabajo.value;
	var d39 = form1.puestodetrabajoporcentaje.value;
	var d40 = form1.informadora.value;
	var d41 = form1.informadoraporcentaje.value;
	var d42 = form1.director.value;
	var d43 = form1.directorporcentaje.value;
	var d44 = form1.cajas.value;
	var d45 = form1.cajasporcentaje.value;
	var d46 = form1.cajasaccesibilidad.value;
	var d47 = form1.nivelcajas.value;
	var d48 = form1.gerente.value;
	var d49 = form1.gerenteporcentaje.value;
	var d50 = form1.asesores.value;
	var d51 = form1.asesoresporcentaje.value;
	var d52 = form1.internos.value;
	var d53 = form1.internosporcentaje.value;
	var d54 = form1.totalbackoffice.value;
	var d55 = form1.totalbackofficeporcentaje.value;
	var d56 = form1.servicios.value;
	var d57 = form1.serviciosporcentaje.value;
	var d58 = form1.areastecnicas.value;
	var d59 = form1.areastecnicasporcentaje.value;
	var d60 = form1.averticya.value;
	var d61 = form1.terrazapatio.value;
	var d62 = form1.notasbackoffice.value;
	var d63 = form1.imagen.value;
	var d64 = form1.formato.value;
	var d65 = form1.suc.value;
	var d66 = form1.pn.value;
	var d67 = form1.caj.value;
	var d68 = form1.precord.value;
	var d69 = form1.nopuestos.value;
	var d70 = form1.banco.value;
	var d71 = form1.enlaceopr.value;
	var d72 = form1.valores.value;
	var d73 = form1.fiduciaria.value;
	var d74 = form1.leasing.value;
	var d75 = form1.factoring.value;
	var d76 = form1.sufi.value;
	var d77 = form1.monextranj.value;
	var d78 = form1.nocajeros.value;
	var d79 = form1.licenciaconst.value;
	var d80 = form1.tipolicencia.value;
	var d81 = form1.notaslicencia.value;
	var d82 = form1.registroaviso.value;
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('busqueda').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#busqueda").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='50' width='50' />"); //loading gif will be overwrited when ajax have success
	if(p > 0)
	{
		ajax.open("GET","?buscar=si&d1="+d1+"&d2="+d2+"&d3="+d3+"&d4="+d4+"&d5="+d5+"&d6="+d6+"&d7="+d7+"&d8="+d8+"&d9="+d9+"&d10="+d10+"&d11="+d11+"&d12="+d12+"&d13="+d13+"&d14="+d14+"&d15="+d15+"&d16="+d16+"&d17="+d17+"&d18="+d18+"&d19="+d19+"&d20="+d20+"&d21="+d21+"&d22="+d22+"&d23="+d23+"&d24="+d24+"&d25="+d25+"&d26="+d26+"&d27="+d27+"&d28="+d28+"&d29="+d29+"&d30="+d30+"&d31="+d31+"&d32="+d32+"&d33="+d33+"&d34="+d34+"&d35="+d35+"&d36="+d36+"&d37="+d37+"&d38="+d38+"&d39="+d39+"&d40="+d40+"&d41="+d41+"&d42="+d42+"&d43="+d43+"&d44="+d44+"&d45="+d45+"&d46="+d46+"&d47="+d47+"&d48="+d48+"&d49="+d49+"&d50="+d50+"&d51="+d51+"&d52="+d52+"&d53="+d53+"&d54="+d54+"&d55="+d55+"&d56="+d56+"&d57="+d57+"&d58="+d58+"&d59="+d59+"&d60="+d60+"&d61="+d61+"&d62="+d62+"&d63="+d63+"&d64="+d64+"&d65="+d65+"&d66="+d66+"&d67="+d67+"&d68="+d68+"&d69="+d69+"&d70="+d70+"&d71="+d71+"&d72="+d72+"&d73="+d73+"&d74="+d74+"&d75="+d75+"&d76="+d76+"&d77="+d77+"&d78="+d78+"&d79="+d79+"&d80="+d80+"&d81="+d81+"&d82="+d82+"&page="+p,true);
	}
	else
	{
		ajax.open("GET","?buscar=si&d1="+d1+"&d2="+d2+"&d3="+d3+"&d4="+d4+"&d5="+d5+"&d6="+d6+"&d7="+d7+"&d8="+d8+"&d9="+d9+"&d10="+d10+"&d11="+d11+"&d12="+d12+"&d13="+d13+"&d14="+d14+"&d15="+d15+"&d16="+d16+"&d17="+d17+"&d18="+d18+"&d19="+d19+"&d20="+d20+"&d21="+d21+"&d22="+d22+"&d23="+d23+"&d24="+d24+"&d25="+d25+"&d26="+d26+"&d27="+d27+"&d28="+d28+"&d29="+d29+"&d30="+d30+"&d31="+d31+"&d32="+d32+"&d33="+d33+"&d34="+d34+"&d35="+d35+"&d36="+d36+"&d37="+d37+"&d38="+d38+"&d39="+d39+"&d40="+d40+"&d41="+d41+"&d42="+d42+"&d43="+d43+"&d44="+d44+"&d45="+d45+"&d46="+d46+"&d47="+d47+"&d48="+d48+"&d49="+d49+"&d50="+d50+"&d51="+d51+"&d52="+d52+"&d53="+d53+"&d54="+d54+"&d55="+d55+"&d56="+d56+"&d57="+d57+"&d58="+d58+"&d59="+d59+"&d60="+d60+"&d61="+d61+"&d62="+d62+"&d63="+d63+"&d64="+d64+"&d65="+d65+"&d66="+d66+"&d67="+d67+"&d68="+d68+"&d69="+d69+"&d70="+d70+"&d71="+d71+"&d72="+d72+"&d73="+d73+"&d74="+d74+"&d75="+d75+"&d76="+d76+"&d77="+d77+"&d78="+d78+"&d79="+d79+"&d80="+d80+"&d81="+d81+"&d82="+d82,true);
	}
	ajax.send(null);
	OCULTARSCROLL();
}
function EXPORTAR()
{	
	var d1 = form1.planoteca.value;
	var d2 = form1.tipologia.value;
	var d3 = form1.codigo.value;
	var d4 = form1.nombre.value;
	var d5 = form1.direccion.value;
	var d6 = form1.departamento.value;
	var d7 = form1.municipio.value;
	var d8 = form1.region.value;
	var d9 = form1.propiedad.value;
	var d10 = form1.estado.value;
	var d11 = form1.planos.value;
	var d12 = form1.anointerv.value;
	var d13 = form1.cumpleaccesibilidad.value;
	var d14 = form1.elementosaccesibilidad.value;
	var d15 = form1.notasaccesibilidad.value;
	var d16 = form1.apertura.value;
	var d17 = form1.hallautoservicio.value;
	var d18 = form1.arealote.value;
	var d19 = form1.sotano.value;
	var d20 = form1.parqueadero.value;
	var d21 = form1.piso1.value;
	var d22 = form1.piso2.value;
	var d23 = form1.piso3.value;
	var d24 = form1.piso4.value;
	var d25 = form1.piso5.value;
	var d26 = form1.otros.value;
	var d27 = form1.areatotal.value;
	var d28 = form1.areacons.value;
	var d29 = form1.areaconsporcentaje.value;
	var d30 = form1.totalareapublico.value;
	var d31 = form1.totalareapublicoporcentaje.value;
	var d32 = form1.autoservicios.value;
	var d33 = form1.autoserviciosporcentaje.value;
	var d34 = form1.clienteasesoria.value;
	var d35 = form1.clienteasesoriaporcentaje.value;
	var d36 = form1.clientecajas.value;
	var d37 = form1.clientecajasporcentaje.value;
	var d38 = form1.puestodetrabajo.value;
	var d39 = form1.puestodetrabajoporcentaje.value;
	var d40 = form1.informadora.value;
	var d41 = form1.informadoraporcentaje.value;
	var d42 = form1.director.value;
	var d43 = form1.directorporcentaje.value;
	var d44 = form1.cajas.value;
	var d45 = form1.cajasporcentaje.value;
	var d46 = form1.cajasaccesibilidad.value;
	var d47 = form1.nivelcajas.value;
	var d48 = form1.gerente.value;
	var d49 = form1.gerenteporcentaje.value;
	var d50 = form1.asesores.value;
	var d51 = form1.asesoresporcentaje.value;
	var d52 = form1.internos.value;
	var d53 = form1.internosporcentaje.value;
	var d54 = form1.totalbackoffice.value;
	var d55 = form1.totalbackofficeporcentaje.value;
	var d56 = form1.servicios.value;
	var d57 = form1.serviciosporcentaje.value;
	var d58 = form1.areastecnicas.value;
	var d59 = form1.areastecnicasporcentaje.value;
	var d60 = form1.averticya.value;
	var d61 = form1.terrazapatio.value;
	var d62 = form1.notasbackoffice.value;
	var d63 = form1.imagen.value;
	var d64 = form1.formato.value;
	var d65 = form1.suc.value;
	var d66 = form1.pn.value;
	var d67 = form1.caj.value;
	var d68 = form1.precord.value;
	var d69 = form1.nopuestos.value;
	var d70 = form1.banco.value;
	var d71 = form1.enlaceopr.value;
	var d72 = form1.valores.value;
	var d73 = form1.fiduciaria.value;
	var d74 = form1.leasing.value;
	var d75 = form1.factoring.value;
	var d76 = form1.sufi.value;
	var d77 = form1.monextranj.value;
	var d78 = form1.nocajeros.value;
	var d79 = form1.licenciaconst.value;
	var d80 = form1.tipolicencia.value;
	var d81 = form1.notaslicencia.value;
	var d82 = form1.registroaviso.value;
	
	window.location.href = "informes/informeexcel.php?d1="+d1+"&d2="+d2+"&d3="+d3+"&d4="+d4+"&d5="+d5+"&d6="+d6+"&d7="+d7+"&d8="+d8+"&d9="+d9+"&d10="+d10+"&d11="+d11+"&d12="+d12+"&d13="+d13+"&d14="+d14+"&d15="+d15+"&d16="+d16+"&d17="+d17+"&d18="+d18+"&d19="+d19+"&d20="+d20+"&d21="+d21+"&d22="+d22+"&d23="+d23+"&d24="+d24+"&d25="+d25+"&d26="+d26+"&d27="+d27+"&d28="+d28+"&d29="+d29+"&d30="+d30+"&d31="+d31+"&d32="+d32+"&d33="+d33+"&d34="+d34+"&d35="+d35+"&d36="+d36+"&d37="+d37+"&d38="+d38+"&d39="+d39+"&d40="+d40+"&d41="+d41+"&d42="+d42+"&d43="+d43+"&d44="+d44+"&d45="+d45+"&d46="+d46+"&d47="+d47+"&d48="+d48+"&d49="+d49+"&d50="+d50+"&d51="+d51+"&d52="+d52+"&d53="+d53+"&d54="+d54+"&d55="+d55+"&d56="+d56+"&d57="+d57+"&d58="+d58+"&d59="+d59+"&d60="+d60+"&d61="+d61+"&d62="+d62+"&d63="+d63+"&d64="+d64+"&d65="+d65+"&d66="+d66+"&d67="+d67+"&d68="+d68+"&d69="+d69+"&d70="+d70+"&d71="+d71+"&d72="+d72+"&d73="+d73+"&d74="+d74+"&d75="+d75+"&d76="+d76+"&d77="+d77+"&d78="+d78+"&d79="+d79+"&d80="+d80+"&d81="+d81+"&d82="+d82;
}
function VERCIUDADES(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('ciudades').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#ciudades").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?verciudades=si&dep="+v,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTACIUDADES()",800);
}
function GUARDAR()
{
	var d1 = form1.planoteca.value;
	var d2 = form1.tipologia.value;
	var d3 = form1.codigo.value;
	var d4 = form1.nombre.value;
	var d5 = form1.direccion.value;
	var d6 = form1.departamento.value;
	var d7 = form1.municipio.value;
	var d8 = form1.region.value;
	var d9 = form1.propiedad.value;
	var d10 = form1.estado.value;
	var d11 = form1.planos.value;
	var d12 = form1.anointerv.value;
	var d13 = form1.cumpleaccesibilidad.value;
	var d14 = form1.elementosaccesibilidad.value;
	var d15 = form1.notasaccesibilidad.value;
	var d16 = form1.apertura.value;
	var d17 = form1.hallautoservicio.value;
	var d18 = form1.arealote.value;
	var d19 = form1.sotano.value;
	var d20 = form1.parqueadero.value;
	var d21 = form1.piso1.value;
	var d22 = form1.piso2.value;
	var d23 = form1.piso3.value;
	var d24 = form1.piso4.value;
	var d25 = form1.piso5.value;
	var d26 = form1.otros.value;
	var d27 = form1.areatotal.value;
	var d28 = form1.areacons.value;
	var d29 = form1.areaconsporcentaje.value;
	var d30 = form1.totalareapublico.value;
	var d31 = form1.totalareapublicoporcentaje.value;
	var d32 = form1.autoservicios.value;
	var d33 = form1.autoserviciosporcentaje.value;
	var d34 = form1.clienteasesoria.value;
	var d35 = form1.clienteasesoriaporcentaje.value;
	var d36 = form1.clientecajas.value;
	var d37 = form1.clientecajasporcentaje.value;
	var d38 = form1.puestodetrabajo.value;
	var d39 = form1.puestodetrabajoporcentaje.value;
	var d40 = form1.informadora.value;
	var d41 = form1.informadoraporcentaje.value;
	var d42 = form1.director.value;
	var d43 = form1.directorporcentaje.value;
	var d44 = form1.cajas.value;
	var d45 = form1.cajasporcentaje.value;
	var d46 = form1.cajasaccesibilidad.value;
	var d47 = form1.nivelcajas.value;
	var d48 = form1.gerente.value;
	var d49 = form1.gerenteporcentaje.value;
	var d50 = form1.asesores.value;
	var d51 = form1.asesoresporcentaje.value;
	var d52 = form1.internos.value;
	var d53 = form1.internosporcentaje.value;
	var d54 = form1.totalbackoffice.value;
	var d55 = form1.totalbackofficeporcentaje.value;
	var d56 = form1.servicios.value;
	var d57 = form1.serviciosporcentaje.value;
	var d58 = form1.areastecnicas.value;
	var d59 = form1.areastecnicasporcentaje.value;
	var d60 = form1.averticya.value;
	var d61 = form1.terrazapatio.value;
	var d62 = form1.notasbackoffice.value;
	var d63 = form1.imagen.value;
	var d64 = form1.formato.value;
	var d65 = form1.suc.value;
	var d66 = form1.pn.value;
	var d67 = form1.caj.value;
	var d68 = form1.precord.value;
	var d69 = form1.nopuestos.value;
	var d70 = form1.banco.value;
	var d71 = form1.enlaceopr.value;
	var d72 = form1.valores.value;
	var d73 = form1.fiduciaria.value;
	var d74 = form1.leasing.value;
	var d75 = form1.factoring.value;
	var d76 = form1.sufi.value;
	var d77 = form1.monextranj.value;
	var d78 = form1.nocajeros.value;
	var d79 = form1.licenciaconst.value;
	var d80 = form1.tipolicencia.value;
	var d81 = form1.notaslicencia.value;
	var d82 = form1.registroaviso.value;
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('resultadoplanoteca').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#resultadoplanoteca").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?guardar=si&d1="+d1+"&d2="+d2+"&d3="+d3+"&d4="+d4+"&d5="+d5+"&d6="+d6+"&d7="+d7+"&d8="+d8+"&d9="+d9+"&d10="+d10+"&d11="+d11+"&d12="+d12+"&d13="+d13+"&d14="+d14+"&d15="+d15+"&d16="+d16+"&d17="+d17+"&d18="+d18+"&d19="+d19+"&d20="+d20+"&d21="+d21+"&d22="+d22+"&d23="+d23+"&d24="+d24+"&d25="+d25+"&d26="+d26+"&d27="+d27+"&d28="+d28+"&d29="+d29+"&d30="+d30+"&d31="+d31+"&d32="+d32+"&d33="+d33+"&d34="+d34+"&d35="+d35+"&d36="+d36+"&d37="+d37+"&d38="+d38+"&d39="+d39+"&d40="+d40+"&d41="+d41+"&d42="+d42+"&d43="+d43+"&d44="+d44+"&d45="+d45+"&d46="+d46+"&d47="+d47+"&d48="+d48+"&d49="+d49+"&d50="+d50+"&d51="+d51+"&d52="+d52+"&d53="+d53+"&d54="+d54+"&d55="+d55+"&d56="+d56+"&d57="+d57+"&d58="+d58+"&d59="+d59+"&d60="+d60+"&d61="+d61+"&d62="+d62+"&d63="+d63+"&d64="+d64+"&d65="+d65+"&d66="+d66+"&d67="+d67+"&d68="+d68+"&d69="+d69+"&d70="+d70+"&d71="+d71+"&d72="+d72+"&d73="+d73+"&d74="+d74+"&d75="+d75+"&d76="+d76+"&d77="+d77+"&d78="+d78+"&d79="+d79+"&d80="+d80+"&d81="+d81+"&d82="+d82,true);
	ajax.send(null);
}
function VALIDARCODIGO(v)
{
	$.post('validarcodigo.php', { cod: v }, function(data){
		if(data != "")
		{
			for(var j=0;j<data.length;j++)
			{
				var d1 = data[j].d1;
				var d2 = data[j].d2;
				var d3 = data[j].d3;
				var d4 = data[j].d4;
				var d5 = data[j].d5;
				var d6 = data[j].d6;
				var d7 = data[j].d7;
				var d8 = data[j].d8;
				var d9 = data[j].d9;
				var d10 = data[j].d10;
				var d11 = data[j].d11;
				var d12 = data[j].d12;
				var d13 = data[j].d13;
				var d14 = data[j].d14;
				var d15 = data[j].d15;
				var d16 = data[j].d16;
				var d17 = data[j].d17;
				var d18 = data[j].d18;
				var d19 = data[j].d19;
				var d20 = data[j].d20;
				var d21 = data[j].d21;
				var d22 = data[j].d22;
				var d23 = data[j].d23;
				var d24 = data[j].d24;
				var d25 = data[j].d25;
				var d26 = data[j].d26;
				var d27 = data[j].d27;
				var d28 = data[j].d28;
				var d29 = data[j].d29;
				var d30 = data[j].d30;
				var d31 = data[j].d31;
				var d32 = data[j].d32;
				var d33 = data[j].d33;
				var d34 = data[j].d34;
				var d35 = data[j].d35;
				var d36 = data[j].d36;
				var d37 = data[j].d37;
				var d38 = data[j].d38;
				var d39 = data[j].d39;
				var d40 = data[j].d40;
				var d41 = data[j].d41;
				var d42 = data[j].d42;
				var d43 = data[j].d43;
				var d44 = data[j].d44;
				var d45 = data[j].d45;
				var d46 = data[j].d46;
				var d47 = data[j].d47;
				var d48 = data[j].d48;
				var d49 = data[j].d49;
				var d50 = data[j].d50;
				var d51 = data[j].d51;
				var d52 = data[j].d52;
				var d53 = data[j].d53;
				var d54 = data[j].d54;
				var d55 = data[j].d55;
				var d56 = data[j].d56;
				var d57 = data[j].d57;
				var d58 = data[j].d58;
				var d59 = data[j].d59;
				var d60 = data[j].d60;
				var d61 = data[j].d61;
				var d62 = data[j].d62;
				var d63 = data[j].d63;
				var d64 = data[j].d64;
				var d65 = data[j].d65;
				var d66 = data[j].d66;
				var d67 = data[j].d67;
				var d68 = data[j].d68;
				var d69 = data[j].d69;
				var d70 = data[j].d70;
				var d71 = data[j].d71;
				var d72 = data[j].d72;
				var d73 = data[j].d73;
				var d74 = data[j].d74;
				var d75 = data[j].d75;
				var d76 = data[j].d76;
				var d77 = data[j].d77;
				var d78 = data[j].d78;
				var d79 = data[j].d79;
				var d80 = data[j].d80;
				var d81 = data[j].d81;
				var d82 = data[j].d82;
				
				form1.planoteca.value = d1;
				form1.tipologia.value = d2;
				if(d3 != '' && d3 != null && d3 != 'null')
				{
					form1.codigo.value = d3;
				}
				form1.nombre.value = d4;
				form1.direccion.value = d5;
				form1.departamento.value = d6;
				form1.municipio.value = d7;
				form1.region.value = d8;
				form1.propiedad.value = d9;
				form1.estado.value = d10;
				form1.planos.value = d11;
				form1.anointerv.value = d12;
				form1.cumpleaccesibilidad.value = d13;
				form1.elementosaccesibilidad.value = d14;
				form1.notasaccesibilidad.value = d15;
				form1.apertura.value = d16;
				form1.hallautoservicio.value = d17;
				form1.arealote.value = d18;
				form1.sotano.value = d19;
				form1.parqueadero.value = d20;
				form1.piso1.value = d21;
				form1.piso2.value = d22;
				form1.piso3.value = d23;
				form1.piso4.value = d24;
				form1.piso5.value = d25;
				form1.otros.value = d26;
				form1.areatotal.value = d27;
				form1.areacons.value = d28;
				form1.areaconsporcentaje.value = d29;
				form1.totalareapublico.value = d30;
				form1.totalareapublicoporcentaje.value = d31;
				form1.autoservicios.value = d32;
				form1.autoserviciosporcentaje.value = d33;
				form1.clienteasesoria.value = d34;
				form1.clienteasesoriaporcentaje.value = d35;
				form1.clientecajas.value = d36;
				form1.clientecajasporcentaje.value = d37;
				form1.puestodetrabajo.value = d38;
				form1.puestodetrabajoporcentaje.value = d39;
				form1.informadora.value = d40;
				form1.informadoraporcentaje.value = d41;
				form1.director.value = d42;
				form1.directorporcentaje.value = d43;
				form1.cajas.value = d44;
				form1.cajasporcentaje.value = d45;
				form1.cajasaccesibilidad.value = d46;
				form1.nivelcajas.value = d47;
				form1.gerente.value = d48;
				form1.gerenteporcentaje.value = d49;
				form1.asesores.value = d50;
				form1.asesoresporcentaje.value = d51;
				form1.internos.value = d52;
				form1.internosporcentaje.value = d53;
				form1.totalbackoffice.value = d54;
				form1.totalbackofficeporcentaje.value = d55;
				form1.servicios.value = d56;
				form1.serviciosporcentaje.value = d57;
				form1.areastecnicas.value = d58;
				form1.areastecnicasporcentaje.value = d59;
				form1.averticya.value = d60;
				form1.terrazapatio.value = d61;
				form1.notasbackoffice.value = d62;
				form1.imagen.value = d63;
				form1.formato.value = d64;
				form1.suc.value = d65;
				form1.pn.value = d66;
				form1.caj.value = d67;
				form1.precord.value = d68;
				form1.nopuestos.value = d69;
				form1.banco.value = d70;
				form1.enlaceopr.value = d71;
				form1.valores.value = d72;
				form1.fiduciaria.value = d73;
				form1.leasing.value = d74;
				form1.factoring.value = d75;
				form1.sufi.value = d76;
				form1.monextranj.value = d77;
				form1.nocajeros.value = d78;
				form1.licenciaconst.value = d79;
				form1.tipolicencia.value = d80;
				form1.notaslicencia.value = d81;
				form1.registroaviso.value = d82;
				
				document.location.href = "#top";
			}
		}
	},"json");
}
function VALIDARAREATOTAL()
{
	var d19 = form1.sotano.value;
	var d20 = form1.parqueadero.value;
	var d21 = form1.piso1.value;
	var d22 = form1.piso2.value;
	var d23 = form1.piso3.value;
	var d24 = form1.piso4.value;
	var d25 = form1.piso5.value;
	var d26 = form1.otros.value;
	
	if(d19 == ''){ d19 = 0; }
	if(d20 == ''){ d20 = 0; }
	if(d21 == ''){ d21 = 0; }
	if(d22 == ''){ d22 = 0; }
	if(d23 == ''){ d23 = 0; }
	if(d24 == ''){ d24 = 0; }
	if(d25 == ''){ d25 = 0; }
	if(d26 == ''){ d26 = 0; }
	
	form1.areatotal.value = parseFloat(d19)+parseFloat(d20)+parseFloat(d21)+parseFloat(d22)+parseFloat(d23)+parseFloat(d24)+parseFloat(d25)+parseFloat(d26);
}
function LIMPIAR()
{
	form1.planoteca.value = "";
	form1.tipologia.value = "";
	form1.codigo.value = "";
	form1.nombre.value = "";
	form1.direccion.value = "";
	form1.departamento.value = "";
	form1.municipio.value = "";
	form1.region.value = "";
	form1.propiedad.value = "";
	form1.estado.value = "";
	form1.planos.value = "";
	form1.anointerv.value = "";
	form1.cumpleaccesibilidad.value = "";
	form1.elementosaccesibilidad.value = "";
	form1.notasaccesibilidad.value = "";
	form1.apertura.value = "";
	form1.hallautoservicio.value = "";
	form1.arealote.value = "";
	form1.sotano.value = "";
	form1.parqueadero.value = "";
	form1.piso1.value = "";
	form1.piso2.value = "";
	form1.piso3.value = "";
	form1.piso4.value = "";
	form1.piso5.value = "";
	form1.otros.value = "";
	form1.areatotal.value = "";
	form1.areacons.value = "";
	form1.areaconsporcentaje.value = "";
	form1.totalareapublico.value = "";
	form1.totalareapublicoporcentaje.value = "";
	form1.autoservicios.value = "";
	form1.autoserviciosporcentaje.value = "";
	form1.clienteasesoria.value = "";
	form1.clienteasesoriaporcentaje.value = "";
	form1.clientecajas.value = "";
	form1.clientecajasporcentaje.value = "";
	form1.puestodetrabajo.value = "";
	form1.puestodetrabajoporcentaje.value = "";
	form1.informadora.value = "";
	form1.informadoraporcentaje.value = "";
	form1.director.value = "";
	form1.directorporcentaje.value = "";
	form1.cajas.value = "";
	form1.cajasporcentaje.value = "";
	form1.cajasaccesibilidad.value = "";
	form1.nivelcajas.value = "";
	form1.gerente.value = "";
	form1.gerenteporcentaje.value = "";
	form1.asesores.value = "";
	form1.asesoresporcentaje.value = "";
	form1.internos.value = "";
	form1.internosporcentaje.value = "";
	form1.totalbackoffice.value = "";
	form1.totalbackofficeporcentaje.value = "";
	form1.servicios.value = "";
	form1.serviciosporcentaje.value = "";
	form1.areastecnicas.value = "";
	form1.areastecnicasporcentaje.value = "";
	form1.averticya.value = "";
	form1.terrazapatio.value = "";
	form1.notasbackoffice.value = "";
	form1.imagen.value = "";
	form1.formato.value = "";
	form1.suc.value = "";
	form1.pn.value = "";
	form1.caj.value = "";
	form1.precord.value = "";
	form1.nopuestos.value = "";
	form1.banco.value = "";
	form1.enlaceopr.value = "";
	form1.valores.value = "";
	form1.fiduciaria.value = "";
	form1.leasing.value = "";
	form1.factoring.value = "";
	form1.sufi.value = "";
	form1.monextranj.value = "";
	form1.nocajeros.value = "";
	form1.licenciaconst.value = "";
	form1.tipolicencia.value = "";
	form1.notaslicencia.value = "";
	form1.registroaviso.value = "";
}
function ELIMINAR(c)
{
	var msn = confirm("Realmente Desea Eliminar Esta Planoteca");
	if(msn==true)
	{
		$.post("delete.php",{ide:c},
		function(data)
		{
			if(data==1)
			{
				$('#planoteca'+c).fadeOut("slow");
				ENVIAREMAIL(c);
			}
			else
			{
			   alert("Ocurrio un error al eliminar");
			}
		});
	}
	else
	{
	  return false
	}
}