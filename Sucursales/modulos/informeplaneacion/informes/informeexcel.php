<?php
include('../../../data/Conexion.php');
require_once('../../../Classes/PHPExcel.php');
date_default_timezone_set('America/Bogota');
session_start();
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_SESSION['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];
	
// verifica si no se ha loggeado
if(!isset($_SESSION["persona"]))
{
  session_destroy();
  header("LOCATION:index.php");
}else{
}

$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
$dato = mysqli_fetch_array($con);
$ultimaobra = $dato['obr_clave_int'];

$caj = $_GET['caj'];
$anocon = $_GET['anocon'];
$cencos = $_GET['cencos'];
$cod = $_GET['cod'];
$reg = $_GET['reg'];
$mun = $_GET['mun'];
$tip = $_GET['tip'];
$tipint = $_GET['tipint'];
$actor = $_GET['actor'];
$est = $_GET['est'];

$seleccionados = explode(',',$caj);
$num = count($seleccionados);
$cajeros = array();
for($i = 0; $i < $num; $i++)
{
	if($seleccionados[$i] != '')
	{
		$cajeros[$i]=$seleccionados[$i];
	}
}
$listacajeros=implode(',',$cajeros);

$fecha=date("d/m/Y");
$fechaact=date("Y/m/d H:i:s");
mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,obr_clave_int,loa_usu_actualiz,loa_fec_actualiz) values(null,29,71,'".$ultimaobra."','".$usuario."','".$fechaact."')");//Tercer campo tia_clave_int. 71=Documento Impreso
//************ESTILOS******************

$styleA1 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => '000000'),
    'size'  => 12,
    'name'  => 'Calibri'
));
$styleA2 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => 'C83000'),
    'size'  => 10,
    'name'  => 'Arial'
));
$styleA3 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => '000000'),
    'size'  => 10,
    'name'  => 'Arial'
));
$styleA4 = array(
'font'  => array(
    'bold'  => false,
    'color' => array('rgb' => '000000'),
    'size'  => 10,
    'name'  => 'Arial'
));
$styleA3p1 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => '000000'),
    'size'  => 9,
    'name'  => 'Arial'
));
$styleA4p1 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => '000000'),
    'size'  => 9,
    'name'  => 'Arial'
));

$borders = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '000000'),
		)
	),
);

//*************************************

$objPHPExcel = new PHPExcel();
$archivo = 'PAVAS - Informe cajeros eliminados.xls';

//Propiedades de la hoja de excel
$objPHPExcel->getProperties()
		->setCreator("PAVAS TECNOLOGIA")
		->setLastModifiedBy("PAVAS TECNOLOGIA")
		->setTitle("Informe cajeros eliminados")
		->setSubject("Informe cajeros eliminados")
		->setDescription("Documento generado con el software Control de Materiales")
		->setKeywords("Control de Materiales")
		->setCategory("Reportes");
		
//Ancho de las Columnas
//Info basica
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
//Info secundaria
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(23);
//Inmobiliaria
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(20);
//Visita
$objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setWidth(20);
//Diseño
$objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AN')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AO')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AP')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AQ')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AR')->setWidth(20);
//Licencia
$objPHPExcel->getActiveSheet()->getColumnDimension('AS')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AT')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AU')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AV')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AW')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AX')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AY')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AZ')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BA')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BB')->setWidth(20);
//INTERVENTORIA
$objPHPExcel->getActiveSheet()->getColumnDimension('BC')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BD')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BE')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BF')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BG')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BH')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BI')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BJ')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BK')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BL')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BM')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BN')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BO')->setWidth(20);
//CONSTRUCTOR
$objPHPExcel->getActiveSheet()->getColumnDimension('BP')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BQ')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BR')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BS')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BT')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BU')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BV')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BW')->setWidth(20);
//SEGURIDAD
$objPHPExcel->getActiveSheet()->getColumnDimension('BX')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BY')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BZ')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('CA')->setWidth(20);
//FACTURAZION
$objPHPExcel->getActiveSheet()->getColumnDimension('CB')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CC')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CD')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CE')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CF')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CG')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CH')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CI')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CJ')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CK')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CL')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CM')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CN')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CO')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CP')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CQ')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CR')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CS')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CT')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CU')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CV')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CW')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CX')->setWidth(30);

//************A1**************
$objPHPExcel->getActiveSheet()->getStyle('A1')-> applyFromArray($styleA1);//
$objPHPExcel->getActiveSheet()->getCell('A1')->setValue("LISTADO GENERAL DE CAJEROS");
$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setWrapText(true); //Crea un enter entre palabras
$objPHPExcel->getActiveSheet()->mergeCells('A1:CX1');//Conbinar celdas
$objPHPExcel->getActiveSheet()->getStyle('A1:CX1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('00D8D8D8');//COLOR DE FONDO
//****************************

/**************** COLUMNAS ****************/

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('A2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('A2')->setValue("Cajero");
$objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('B2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('B2')->setValue("Nombre");
$objPHPExcel->getActiveSheet()->getStyle('B2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('C2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('C2')->setValue("Dirección");
$objPHPExcel->getActiveSheet()->getStyle('C2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('D2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('D2')->setValue("Año Contable");
$objPHPExcel->getActiveSheet()->getStyle('D2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('E2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('E2')->setValue("Región");
$objPHPExcel->getActiveSheet()->getStyle('E2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('F2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('F2')->setValue("Departamento");
$objPHPExcel->getActiveSheet()->getStyle('F2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('G2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('G2')->setValue("Municipío");
$objPHPExcel->getActiveSheet()->getStyle('G2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('H2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('H2')->setValue("Tipología");
$objPHPExcel->getActiveSheet()->getStyle('H2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('I2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('I2')->setValue("Tipo Intervención");
$objPHPExcel->getActiveSheet()->getStyle('I2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('J2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('J2')->setValue("Modalidad");
$objPHPExcel->getActiveSheet()->getStyle('J2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('K2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('K2')->setValue("Estado Proyecto");
$objPHPExcel->getActiveSheet()->getStyle('K2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('L2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('L2')->setValue("Código Cajero");
$objPHPExcel->getActiveSheet()->getStyle('L2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('M2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('M2')->setValue("Centro Costos");
$objPHPExcel->getActiveSheet()->getStyle('M2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('N2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('N2')->setValue("Referencia Maquina");
$objPHPExcel->getActiveSheet()->getStyle('N2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('O2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('O2')->setValue("Ubicación (SUC, CC, REMOTO)");
$objPHPExcel->getActiveSheet()->getStyle('O2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('P2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('P2')->setValue("Código de Suc");
$objPHPExcel->getActiveSheet()->getStyle('P2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('Q2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('Q2')->setValue("Ubicación ATM");
$objPHPExcel->getActiveSheet()->getStyle('Q2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('R2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('R2')->setValue("Atiende");
$objPHPExcel->getActiveSheet()->getStyle('R2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('S2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('S2')->setValue("Riesgo");
$objPHPExcel->getActiveSheet()->getStyle('S2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('T2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('T2')->setValue("Area");
$objPHPExcel->getActiveSheet()->getStyle('T2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('U2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('U2')->setValue("Fecha Apagado ATM");
$objPHPExcel->getActiveSheet()->getStyle('U2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('V2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('V2')->setValue("Requiere Inmobiliaria");
$objPHPExcel->getActiveSheet()->getStyle('V2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('W2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('W2')->setValue("Nombre Inmobiliaria");
$objPHPExcel->getActiveSheet()->getStyle('W2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('X2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('X2')->setValue("Fecha Inicio");
$objPHPExcel->getActiveSheet()->getStyle('X2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('Y2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('Y2')->setValue("Fecha Teorica Entrega (45d)");
$objPHPExcel->getActiveSheet()->getStyle('Y2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('Z2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('Z2')->setValue("Fecha Entrega Info");
$objPHPExcel->getActiveSheet()->getStyle('Z2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AA2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AA2')->setValue("Total Días");
$objPHPExcel->getActiveSheet()->getStyle('AA2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AB2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AB2')->setValue("Estado");
$objPHPExcel->getActiveSheet()->getStyle('AB2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AC2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AC2')->setValue("Notas");
$objPHPExcel->getActiveSheet()->getStyle('AC2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AD2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AD2')->setValue("Requiere Visita Local");
$objPHPExcel->getActiveSheet()->getStyle('AD2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AE2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AE2')->setValue("Nombre Visitante");
$objPHPExcel->getActiveSheet()->getStyle('AE2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AF2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AF2')->setValue("Fecha Inicio");
$objPHPExcel->getActiveSheet()->getStyle('AF2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AG2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AG2')->setValue("Fecha Teorica Entrega (7d)");
$objPHPExcel->getActiveSheet()->getStyle('AG2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AH2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AH2')->setValue("Fecha Entrega Info");
$objPHPExcel->getActiveSheet()->getStyle('AH2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AI2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AI2')->setValue("Total Días");
$objPHPExcel->getActiveSheet()->getStyle('AI2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AJ2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AJ2')->setValue("Estado");
$objPHPExcel->getActiveSheet()->getStyle('AJ2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AK2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AK2')->setValue("Notas");
$objPHPExcel->getActiveSheet()->getStyle('AK2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AL2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AL2')->setValue("Requiere Diseño");
$objPHPExcel->getActiveSheet()->getStyle('AL2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AM2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AM2')->setValue("Nombre Diseñador");
$objPHPExcel->getActiveSheet()->getStyle('AM2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AN2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AN2')->setValue("Fecha Inicio");
$objPHPExcel->getActiveSheet()->getStyle('AN2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AO2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AO2')->setValue("Fecha Teorica Entrega (7d)");
$objPHPExcel->getActiveSheet()->getStyle('AO2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AP2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AP2')->setValue("Fecha Entrega Info");
$objPHPExcel->getActiveSheet()->getStyle('AP2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AQ2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AQ2')->setValue("Total Días");
$objPHPExcel->getActiveSheet()->getStyle('AQ2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AR2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AR2')->setValue("Estado");
$objPHPExcel->getActiveSheet()->getStyle('AR2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AS2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AS2')->setValue("Notas");
$objPHPExcel->getActiveSheet()->getStyle('AS2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AT2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AT2')->setValue("Requiere Licencia");
$objPHPExcel->getActiveSheet()->getStyle('AT2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AU2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AU2')->setValue("Nombre Gestionador");
$objPHPExcel->getActiveSheet()->getStyle('AU2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AV2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AV2')->setValue("Fecha Inicio");
$objPHPExcel->getActiveSheet()->getStyle('AV2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AW2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AW2')->setValue("Fecha Teorica Entrega (90d)");
$objPHPExcel->getActiveSheet()->getStyle('AW2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AX2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AX2')->setValue("Fecha Entrega Info");
$objPHPExcel->getActiveSheet()->getStyle('AX2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AY2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AY2')->setValue("Total Días");
$objPHPExcel->getActiveSheet()->getStyle('AY2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AZ2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AZ2')->setValue("Estado");
$objPHPExcel->getActiveSheet()->getStyle('AZ2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BA2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BA2')->setValue("Curaduria");
$objPHPExcel->getActiveSheet()->getStyle('BA2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BB2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BB2')->setValue("N°Radicado");
$objPHPExcel->getActiveSheet()->getStyle('BB2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BC2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BC2')->setValue("Notas");
$objPHPExcel->getActiveSheet()->getStyle('BC2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BD2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BD2')->setValue("Nombre Interventor");
$objPHPExcel->getActiveSheet()->getStyle('BD2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BE2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BE2')->setValue("Fecha Teorica Entrega");
$objPHPExcel->getActiveSheet()->getStyle('BE2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BF2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BF2')->setValue("Fecha Inicio Obra");
$objPHPExcel->getActiveSheet()->getStyle('BF2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BG2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BG2')->setValue("Fecha Pedido Suministro");
$objPHPExcel->getActiveSheet()->getStyle('BG2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BH2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BH2')->setValue("Fecha Aprobación Comité");
$objPHPExcel->getActiveSheet()->getStyle('BH2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BI2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BI2')->setValue("Aprobar Cotización");
$objPHPExcel->getActiveSheet()->getStyle('BI2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BJ2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BJ2')->setValue("Operador Canal Comunicaciones");
$objPHPExcel->getActiveSheet()->getStyle('BJ2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BK2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BK2')->setValue("Fecha Entrega Canal");
$objPHPExcel->getActiveSheet()->getStyle('BK2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BL2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BL2')->setValue("Aprobar Liquidación");
$objPHPExcel->getActiveSheet()->getStyle('BL2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BM2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BM2')->setValue("Image Nexos");
$objPHPExcel->getActiveSheet()->getStyle('BM2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BN2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BN2')->setValue("Fotos");
$objPHPExcel->getActiveSheet()->getStyle('BN2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BO2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BO2')->setValue("Actas");
$objPHPExcel->getActiveSheet()->getStyle('BO2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BP2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BP2')->setValue("Notas");
$objPHPExcel->getActiveSheet()->getStyle('BP2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BQ2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BQ2')->setValue("Nombre Constructor");
$objPHPExcel->getActiveSheet()->getStyle('BQ2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BR2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BR2')->setValue("Fecha Inicio Obra");
$objPHPExcel->getActiveSheet()->getStyle('BR2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BS2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BS2')->setValue("Fecha Teorica Entrega");
$objPHPExcel->getActiveSheet()->getStyle('BS2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BT2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BT2')->setValue("Fecha Real Entrega ATM");
$objPHPExcel->getActiveSheet()->getStyle('BT2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BU2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BU2')->setValue("% Avance");
$objPHPExcel->getActiveSheet()->getStyle('BU2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BV2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BV2')->setValue("Fecha Entrega Cotización (3d)");
$objPHPExcel->getActiveSheet()->getStyle('BV2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BW2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BW2')->setValue("Fecha Entrega Liquidación (7d)");
$objPHPExcel->getActiveSheet()->getStyle('BW2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BX2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BX2')->setValue("Notas");
$objPHPExcel->getActiveSheet()->getStyle('BX2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BY2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BY2')->setValue("Nombre Instalador");
$objPHPExcel->getActiveSheet()->getStyle('BY2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BZ2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BZ2')->setValue("Fecha Ingreso Obra");
$objPHPExcel->getActiveSheet()->getStyle('BZ2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CA2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CA2')->setValue("Código Monitoreo");
$objPHPExcel->getActiveSheet()->getStyle('CA2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CB2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CB2')->setValue("Notas");
$objPHPExcel->getActiveSheet()->getStyle('CB2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CC2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CC2')->setValue("Número OT. Inmobiliaria");
$objPHPExcel->getActiveSheet()->getStyle('CC2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CD2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CD2')->setValue("Valor Inmobiliaria");
$objPHPExcel->getActiveSheet()->getStyle('CD2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CE2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CE2')->setValue("Liquidación o Cotización Inmobiliaria");
$objPHPExcel->getActiveSheet()->getStyle('CE2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CF2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CF2')->setValue("Número OT. Visita Local");
$objPHPExcel->getActiveSheet()->getStyle('CF2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CG2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CG2')->setValue("Valor Visita Local");
$objPHPExcel->getActiveSheet()->getStyle('CG2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CH2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CH2')->setValue("Liquidación o Cotización Visita Local");
$objPHPExcel->getActiveSheet()->getStyle('CH2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CI2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CI2')->setValue("Número OT. Diseño");
$objPHPExcel->getActiveSheet()->getStyle('CI2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CJ2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CJ2')->setValue("Valor Diseño");
$objPHPExcel->getActiveSheet()->getStyle('CJ2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CK2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CK2')->setValue("Liquidación o Cotización Diseño");
$objPHPExcel->getActiveSheet()->getStyle('CK2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CL2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CL2')->setValue("Número OT. Licencia");
$objPHPExcel->getActiveSheet()->getStyle('CL2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CM2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CM2')->setValue("Valor Licencia");
$objPHPExcel->getActiveSheet()->getStyle('CM2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CN2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CN2')->setValue("Liquidación o Cotización Licencia");
$objPHPExcel->getActiveSheet()->getStyle('CN2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CO2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CO2')->setValue("Número OT. Interventoria");
$objPHPExcel->getActiveSheet()->getStyle('CO2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CP2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CP2')->setValue("Valor Interventoria");
$objPHPExcel->getActiveSheet()->getStyle('CP2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CQ2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CQ2')->setValue("Liquidación o Cotización Interventoria");
$objPHPExcel->getActiveSheet()->getStyle('CQ2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CR2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CR2')->setValue("Número OT. Construcción");
$objPHPExcel->getActiveSheet()->getStyle('CR2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CS2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CS2')->setValue("Valor Construcción");
$objPHPExcel->getActiveSheet()->getStyle('CS2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CT2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CT2')->setValue("Liquidación o Cotización Construcción");
$objPHPExcel->getActiveSheet()->getStyle('CT2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CU2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CU2')->setValue("Número OT. Instalador Seguridad");
$objPHPExcel->getActiveSheet()->getStyle('CU2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CV2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CV2')->setValue("Valor Instalador Seguridad");
$objPHPExcel->getActiveSheet()->getStyle('CV2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CW2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CW2')->setValue("Liquidación o Cotización Instalador Seguridad");
$objPHPExcel->getActiveSheet()->getStyle('CW2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CX2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CX2')->setValue("$Total Cajero");
$objPHPExcel->getActiveSheet()->getStyle('CX2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

if($listacajeros == '')
{
	$sql = "(c.caj_clave_int = '".$consec."' OR '".$consec."' IS NULL OR '".$consec."' = '') and (caj_nombre LIKE REPLACE('%".$nom."%',' ','%') OR '".$nom."' IS NULL OR '".$nom."' = '') and (caj_ano_contable = '".$anocon."' OR '".$anocon."' IS NULL OR '".$anocon."' = '') and (cco_clave_int = '".$cencos."' OR '".$cencos."' IS NULL OR '".$cencos."' = '') and (caj_codigo_cajero LIKE REPLACE('%".$cod."%',' ','%') OR '".$cod."' IS NULL OR '".$cod."' = '') and (caj_region = '".$reg."' OR '".$reg."' IS NULL OR '".$reg."' = '') and (caj_municipio = '".$mun."' OR '".$mun."' IS NULL OR '".$mun."' = '') and (tip_clave_int = '".$tip."' OR '".$tip."' IS NULL OR '".$tip."' = '') and (tii_clave_int = '".$tipint."' OR '".$tipint."' IS NULL OR '".$tipint."' = '') and (caj_estado_proyecto = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') and (cai_inmobiliaria = '".$actor."' or cav_visitante = '".$actor."' or cad_disenador = '".$actor."' or cal_gestionador = '".$actor."' or cin_interventor = '".$actor."' or cac_constructor = '".$actor."' or cas_instalador = '".$actor."' or '".$actor."' IS NULL or '".$actor."' = '') and c.caj_sw_eliminado = 0";
}
else
{
	$sql = "(c.caj_clave_int = '".$consec."' OR '".$consec."' IS NULL OR '".$consec."' = '') and (caj_nombre LIKE REPLACE('%".$nom."%',' ','%') OR '".$nom."' IS NULL OR '".$nom."' = '') and (caj_ano_contable = '".$anocon."' OR '".$anocon."' IS NULL OR '".$anocon."' = '') and (cco_clave_int = '".$cencos."' OR '".$cencos."' IS NULL OR '".$cencos."' = '') and (caj_codigo_cajero LIKE REPLACE('%".$cod."%',' ','%') OR '".$cod."' IS NULL OR '".$cod."' = '') and (caj_region = '".$reg."' OR '".$reg."' IS NULL OR '".$reg."' = '') and (caj_municipio = '".$mun."' OR '".$mun."' IS NULL OR '".$mun."' = '') and (tip_clave_int = '".$tip."' OR '".$tip."' IS NULL OR '".$tip."' = '') and (tii_clave_int = '".$tipint."' OR '".$tipint."' IS NULL OR '".$tipint."' = '') and (caj_estado_proyecto = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') and (cai_inmobiliaria = '".$actor."' or cav_visitante = '".$actor."' or cad_disenador = '".$actor."' or cal_gestionador = '".$actor."' or cin_interventor = '".$actor."' or cac_constructor = '".$actor."' or cas_instalador = '".$actor."' or '".$actor."' IS NULL or '".$actor."' = '') and c.caj_sw_eliminado = 0 and c.caj_clave_int in (".$listacajeros.")";
}

$con = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int) inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where ".$sql."");
$num = mysqli_num_rows($con);

$j = 3;
for($i = 0; $i < $num; $i++)
{
	$dato = mysqli_fetch_array($con);
	//Info Base
	$clacaj = $dato['clacaj']; 
	$nomcaj = $dato['caj_nombre']; //Nombre Cajero
	$dir = $dato['caj_direccion']; //Dirección
	$anocon = $dato['caj_ano_contable']; //Año Contable
	$reg = $dato['caj_region']; //Región
	$dep = $dato['caj_departamento']; //Departamento
	$mun = $dato['caj_municipio']; //Municipío
	$tip = $dato['tip_clave_int']; //Tipología
	$tipint = $dato['tii_clave_int']; //Tipo Intervención
	$moda = $dato['mod_clave_int']; //Modalidad
	$estpro = $dato['caj_estado_proyecto']; //Estado Proyecto
	if($estpro == 1){ $estpro = 'Activo'; }elseif($estpro == 2){ $estpro = 'Suspendido'; }elseif($estpro == 3){ $estpro = 'Entregado'; }elseif($estpro == 4){ $estpro = 'Cancelado'; }elseif($estpro == 5){ $estpro = "Programado"; }
	$con1 = mysqli_query($conectar,"select * from posicion_geografica where pog_clave_int = '".$reg."'");
	$dato1 = mysqli_fetch_array($con1);
	$reg = $dato1['pog_nombre'];
	$con1 = mysqli_query($conectar,"select * from posicion_geografica where pog_clave_int = '".$dep."'");
	$dato1 = mysqli_fetch_array($con1);
	$dep = $dato1['pog_nombre'];
	$con1 = mysqli_query($conectar,"select * from posicion_geografica where pog_clave_int = '".$mun."'");
	$dato1 = mysqli_fetch_array($con1);
	$mun = $dato1['pog_nombre'];
	$con1 = mysqli_query($conectar,"select * from tipologia where tip_clave_int = '".$tip."'");
	$dato1 = mysqli_fetch_array($con1);
	$tip = $dato1['tip_nombre'];
	$con1 = mysqli_query($conectar,"select * from tipointervencion where tii_clave_int = '".$tipint."'");
	$dato1 = mysqli_fetch_array($con1);
	$tipint = $dato1['tii_nombre'];
	$con1 = mysqli_query($conectar,"select * from modalidad where mod_clave_int = '".$moda."'");
	$dato1 = mysqli_fetch_array($con1);
	$moda = $dato1['mod_nombre'];
	//Info Secun
	$codcaj = $dato['caj_codigo_cajero']; //Código Cajero
	$cencos = $dato['cco_clave_int']; //Centro Costos
	$refmaq = $dato['rem_clave_int']; //Referencia Maquina
	$ubi = $dato['ubi_clave_int']; //Ubicación (SUC, CC, REMOTO)
	$codsuc = $dato['caj_codigo_suc']; //Código de Suc
	$ubiamt = $dato['caj_ubicacion_atm']; //Ubicación ATM
	$ati = $dato['ati_clave_int']; //Atiende
	$rie = $dato['caj_riesgo']; //Riesgo
	$are = $dato['are_clave_int']; //Area
	$fecapagadoatm = $dato['caj_fec_apagado_atm']; //Código Recibido Monto
	$con1 = mysqli_query($conectar,"select * from centrocostos where cco_clave_int = '".$cencos."'");
	$dato1 = mysqli_fetch_array($con1);
	$cencos = $dato1['cco_nombre'];
	$con1 = mysqli_query($conectar,"select * from referenciamaquina where rem_clave_int = '".$refmaq."'");
	$dato1 = mysqli_fetch_array($con1);
	$refmaq = $dato1['rem_nombre'];
	$con1 = mysqli_query($conectar,"select * from ubicacion where ubi_clave_int = '".$ubi."'");
	$dato1 = mysqli_fetch_array($con1);
	$ubi = $dato1['ubi_descripcion'];
	if($ubiamt == 1){ $ubiamt = 'EXTERNO'; }elseif($ubiamt == 2){ $ubiamt = 'INTERNO'; }elseif($ubiamt == 0){ $ubiamt = 'N/A'; }
	$con1 = mysqli_query($conectar,"select * from atiende where ati_clave_int = '".$ati."'");
	$dato1 = mysqli_fetch_array($con1);
	$ati = $dato1['ati_nombre'];
	if($rie == 1){ $rie = 'ALTO'; }elseif($rie == 2){ $rie = 'BAJO'; }elseif($rie == 0){ $rie = 'N/A'; }
	$con1 = mysqli_query($conectar,"select * from area where are_clave_int = '".$are."'");
	$dato1 = mysqli_fetch_array($con1);
	$are = $dato1['are_nombre'];
	//Info Inmobiliaria
	$reqinm = $dato['cai_req_inmobiliaria']; //Requiere Inmobiliaria
	$inmob = $dato['cai_inmobiliaria']; //Nombre Inmobiliaria
	$feciniinmob = $dato['cai_fecha_ini_inmobiliaria']; //Fecha Inicio
	$estinmob = $dato['esi_clave_int']; //Estado
	$fecentinmob = $dato['cai_fecha_entrega_info_inmobiliaria']; //Fecha Entrega Info
	if($reqinm == 1){ $reqinm = 'Si'; }elseif($reqinm == 2){ $reqinm = 'No'; }
	$con1 = mysqli_query($conectar,"select * from usuario where usu_clave_int = '".$inmob."'");
	$dato1 = mysqli_fetch_array($con1);
	$inmob = $dato1['usu_nombre'];
	$con1 = mysqli_query($conectar,"select * from estado_inmobiliaria where esi_clave_int = '".$estinmob."'");
	$dato1 = mysqli_fetch_array($con1);
	$estinmob = $dato1['esi_nombre'];
	
	//Fecha teorica y total días inmobiliaria
	$confecteoinm = mysqli_query($conectar,"select ADDDATE('".$feciniinmob."', INTERVAL 45 DAY) dias from usuario LIMIT 1");
	$datofecteoinm = mysqli_fetch_array($confecteoinm);
	$fecteoinm = $datofecteoinm['dias'];
	if($fecentinmob != '' and $fecentinmob != '0000-00-00')
	{
		$condiasinm = mysqli_query($conectar,"select DATEDIFF('".$fecentinmob."','".$feciniinmob."') dias from usuario LIMIT 1");
	}
	else
	{
		$condiasinm = mysqli_query($conectar,"select DATEDIFF('".$fecteoinm."','".$feciniinmob."') dias from usuario LIMIT 1");
	}
	$datodiasinm = mysqli_fetch_array($condiasinm);
	$diasinm = $datodiasinm['dias'];
	
	$con1 = mysqli_query($conectar,"select * from notausuario where ven_clave_int = 7 and caj_clave_int = '".$clacaj."'");
	$num1 = mysqli_num_rows($con1);
	for($k = 1; $k <= $num1; $k++)
	{
		$dato1 = mysqli_fetch_array($con1);
		$not = $dato1['nou_nota'];
		$notasinm .= $k.". ".$not.". \n";
	}
	
	//Info Visita Local
	$reqvis = $dato['cav_req_visita_local']; //Requiere Visita Local
	$vis = $dato['cav_visitante']; //Nombre Visitante
	$fecvis = $dato['cav_fecha_visita']; //Fecha Inicio
	$fecentvis = $dato['cav_fecha_entrega_informe']; //Fecha Entrega Info
	$estvis = $dato['cav_estado']; //Estado
	if($reqvis == 1){ $reqvis = 'Si'; }elseif($reqvis == 2){ $reqvis = 'No'; }
	$con1 = mysqli_query($conectar,"select * from usuario where usu_clave_int = '".$vis."'");
	$dato1 = mysqli_fetch_array($con1);
	$vis = $dato1['usu_nombre'];
	if($estvis == 1){ $estvis = 'Activo'; }elseif($estvis == 2){ $estvis = 'Entregado'; }

	//Fecha teorica y total días visita
	$confecteovis = mysqli_query($conectar,"select ADDDATE('".$fecvis."', INTERVAL 7 DAY) dias from usuario LIMIT 1");
	$datofecteovis = mysqli_fetch_array($confecteovis);
	$fecteovis = $datofecteovis['dias'];
	if($fecentvis != '' and $fecentvis != '0000-00-00')
	{
		$condiasvis = mysqli_query($conectar,"select DATEDIFF('".$fecentvis."','".$fecvis."') dias from usuario LIMIT 1");
	}
	else
	{
		$condiasvis = mysqli_query($conectar,"select DATEDIFF('".$fecteovis."','".$fecvis."') dias from usuario LIMIT 1");
	}
	$datodiasvis = mysqli_fetch_array($condiasvis);
	$diasvis = $datodiasvis['dias'];
	
	$con1 = mysqli_query($conectar,"select * from notausuario where ven_clave_int = 18 and caj_clave_int = '".$clacaj."'");
	$num1 = mysqli_num_rows($con1);
	for($k = 1; $k <= $num1; $k++)
	{
		$dato1 = mysqli_fetch_array($con1);
		$not = $dato1['nou_nota'];
		$notasvis .= $k.". ".$not.". \n";
	}
	
	//Info Diseño
	$reqdis = $dato['cad_req_diseno']; //Requiere Diseño
	$dis = $dato['cad_disenador']; //Nombre Visitante
	$fecinidis = $dato['cad_fecha_inicio_diseno']; //Fecha Inicio
	$estdis = $dato['esd_clave_int']; //Estado
	$fecentdis = $dato['cad_fecha_entrega_info_diseno']; //Fecha Entrega Info
	if($reqdis == 1){ $reqdis = 'Si'; }elseif($reqdis == 2){ $reqdis = 'No'; }
	$con1 = mysqli_query($conectar,"select * from usuario where usu_clave_int = '".$dis."'");
	$dato1 = mysqli_fetch_array($con1);
	$dis = $dato1['usu_nombre'];
	$con1 = mysqli_query($conectar,"select * from estado_diseno where esd_clave_int = '".$estdis."'");
	$dato1 = mysqli_fetch_array($con1);
	$estdis = $dato1['esd_nombre'];
	
	//Fecha teorica y total días diseno
	$confecteodis = mysqli_query($conectar,"select ADDDATE('".$fecinidis."', INTERVAL 7 DAY) dias from usuario LIMIT 1");
	$datofecteodis = mysqli_fetch_array($confecteodis);
	$fecteodis = $datofecteodis['dias'];
	if($fecentdis != '' and $fecentdis != '0000-00-00')
	{
		$condiasdis = mysqli_query($conectar,"select DATEDIFF('".$fecentdis."','".$fecinidis."') dias from usuario LIMIT 1");
	}
	else
	{
		$condiasdis = mysqli_query($conectar,"select DATEDIFF('".$fecteodis."','".$fecinidis."') dias from usuario LIMIT 1");
	}
	$datodiasdis = mysqli_fetch_array($condiasdis);
	$diasdis = $datodiasdis['dias'];
	
	$con1 = mysqli_query($conectar,"select * from notausuario where ven_clave_int = 8 and caj_clave_int = '".$clacaj."'");
	$num1 = mysqli_num_rows($con1);
	for($k = 1; $k <= $num1; $k++)
	{
		$dato1 = mysqli_fetch_array($con1);
		$not = $dato1['nou_nota'];
		$notasdis .= $k.". ".$not.". \n";
	}
	
	//Info Licencia
	$reqlic = $dato['cal_req_licencia']; //Requiere Licencia
	$lic = $dato['cal_gestionador']; //Nombre Gestionador
	$fecinilic = $dato['cal_fecha_inicio_licencia']; //Fecha Inicio
	$fecentlic = $dato['cal_fecha_entrega_info_licencia'];
	$estlic = $dato['esl_clave_int']; //Estado
	$cura = $dato['cal_curaduria']; //Curaduria
	$radi = $dato['cal_radicado']; //N°Radicado
	if($reqlic == 1){ $reqlic = 'Si'; }elseif($reqlic == 2){ $reqlic = 'No'; }
	$con1 = mysqli_query($conectar,"select * from usuario where usu_clave_int = '".$lic."'");
	$dato1 = mysqli_fetch_array($con1);
	$lic = $dato1['usu_nombre'];
	$con1 = mysqli_query($conectar,"select * from estado_licencia where esl_clave_int = '".$estlic."'");
	$dato1 = mysqli_fetch_array($con1);
	$estlic = $dato1['esl_nombre'];
	
	//Fecha teorica y total días licencia
	$confecteolic = mysqli_query($conectar,"select ADDDATE('".$fecinilic."', INTERVAL 90 DAY) dias from usuario LIMIT 1");
	$datofecteolic = mysqli_fetch_array($confecteolic);
	$fecteolic = $datofecteolic['dias'];
	if($fecentlic != '' and $fecentlic != '0000-00-00')
	{
		$condiaslic = mysqli_query($conectar,"select DATEDIFF('".$fecentlic."','".$fecinilic."') dias from usuario LIMIT 1");
	}
	else
	{
		$condiaslic = mysqli_query($conectar,"select DATEDIFF('".$fecteolic."','".$fecinilic."') dias from usuario LIMIT 1");
	}
	$datodiaslic = mysqli_fetch_array($condiaslic);
	$diaslic = $datodiaslic['dias'];
	
	$con1 = mysqli_query($conectar,"select * from notausuario where ven_clave_int = 19 and caj_clave_int = '".$clacaj."'");
	$num1 = mysqli_num_rows($con1);
	for($k = 1; $k <= $num1; $k++)
	{
		$dato1 = mysqli_fetch_array($con1);
		$not = $dato1['nou_nota'];
		$notaslic .= $k.". ".$not.". \n";
	}
	
	//Info Interventoria
	$int = $dato['cin_interventor']; //Interventor
	$fecteoent = $dato['cin_fecha_teorica_entrega']; //Fecha Teorica Entrega
	$feciniobra = $dato['cin_fecha_inicio_obra']; //Fecha Inicio Obra
	$fecpedsum = $dato['cin_fecha_pedido_suministro']; //Fecha Pedido Suministro
	$fecaprocom = $dato['cin_fecha_aprov_comite']; //Fecha Aprobación Comité
	$aprovcotiz = $dato['cin_sw_aprov_cotizacion']; //Aprobar Cotización
	$cancom = $dato['can_clave_int']; //Operador Canal Comunicaciones
	$aprovliqui = $dato['cin_sw_aprov_liquidacion']; //Aprobar Liquidación
	$fecentcan = $dato['cin_fecha_entrega_canal']; //Fecha Entrega Canal
	$swimgnex = $dato['cin_sw_img_nexos']; //Image Nexos
	$swfot = $dato['cin_sw_fotos']; //Fotos
	$swact = $dato['cin_sw_actas']; //Actas
	if($aprovcotiz == 1){ $aprovcotiz = 'Si'; }elseif($aprovcotiz == 2){ $aprovcotiz = 'No'; }else{ $aprovcotiz = 'Sin definir'; }
	if($aprovliqui == 1){ $aprovliqui = 'Si'; }elseif($aprovliqui == 2){ $aprovliqui = 'No'; }else{ $aprovliqui = 'Sin definir'; }
	if($swimgnex == 1){ $swimgnex = 'Si'; }elseif($swimgnex == 2){ $swimgnex = 'No'; }else{ $swimgnex = 'Sin definir'; }
	if($swfot == 1){ $swfot = 'Si'; }elseif($swfot == 2){ $swfot = 'No'; }else{ $swfot = 'Sin definir'; }
	if($swact == 1){ $swact = 'Si'; }elseif($swact == 2){ $swact = 'No'; }else{ $swact = 'Sin definir'; }
	$con1 = mysqli_query($conectar,"select * from usuario where usu_clave_int = '".$int."'");
	$dato1 = mysqli_fetch_array($con1);
	$int = $dato1['usu_nombre'];
	$con1 = mysqli_query($conectar,"select * from canal_comunicacion where can_clave_int = '".$cancom ."'");
	$dato1 = mysqli_fetch_array($con1);
	$cancom = $dato1['can_nombre'];
	
	$con1 = mysqli_query($conectar,"select * from notausuario where ven_clave_int = 9 and caj_clave_int = '".$clacaj."'");
	$num1 = mysqli_num_rows($con1);
	for($k = 1; $k <= $num1; $k++)
	{
		$dato1 = mysqli_fetch_array($con1);
		$not = $dato1['nou_nota'];
		$notasint .= $k.". ".$not.". \n";
	}
	
	//Info Constructor
	$cons = $dato['cac_constructor']; //Constructor
	$feciniobra = $dato['cin_fecha_inicio_obra']; //Fecha Inicio Obra
	$fecteoent = $dato['cin_fecha_teorica_entrega'];
	$fecentatm = $dato['cac_fecha_entrega_atm']; //Fecha Entrega ATM
	$porava = $dato['cac_porcentaje_avance']; //%Avance
	$fecentcotiz = $dato['cac_fecha_entrega_cotizacion']; //Fecha Entrega Cotización
	$fecentliq = $dato['cac_fecha_entrega_liquidacion']; //Fecha Entrega Liquidación 
	$con1 = mysqli_query($conectar,"select * from usuario where usu_clave_int = '".$cons."'");
	$dato1 = mysqli_fetch_array($con1);
	$cons = $dato1['usu_nombre'];
	
	$con1 = mysqli_query($conectar,"select * from notausuario where ven_clave_int = 6 and caj_clave_int = '".$clacaj."'");
	$num1 = mysqli_num_rows($con1);
	for($k = 1; $k <= $num1; $k++)
	{
		$dato1 = mysqli_fetch_array($con1);
		$not = $dato1['nou_nota'];
		$notascons .= $k.". ".$not.". \n";
	}
	
	//Info Instalador Seguridad
	$seg = $dato['cas_instalador'];
	$fecingseg = $dato['cas_fecha_ingreso_obra_inst'];
	$codmon = $dato['cas_codigo_monitoreo'];
	$con1 = mysqli_query($conectar,"select * from usuario where usu_clave_int = '".$seg."'");
	$dato1 = mysqli_fetch_array($con1);
	$seg = $dato1['usu_nombre'];
	
	$con1 = mysqli_query($conectar,"select * from notausuario where ven_clave_int = 10 and caj_clave_int = '".$clacaj."'");
	$num1 = mysqli_num_rows($con1);
	for($k = 1; $k <= $num1; $k++)
	{
		$dato1 = mysqli_fetch_array($con1);
		$not = $dato1['nou_nota'];
		$notasseg .= $k.". ".$not.". \n";
	}
	
	//Info Facturación
	$numotinm = $dato['caf_num_ot_inmobiliaria'];
	$vrinm = $dato['caf_valor_inmobiliaria'];
	$swcotizliquiinm = $dato['caf_sw_cotiz_liqui_inmobiliaria'];
	$numotvis = $dato['caf_num_ot_visita'];
	$vrvis = $dato['caf_valor_visita'];
	$swcotizliquivis = $dato['caf_sw_cotiz_liqui_visita'];
	$numotdis = $dato['caf_num_ot_diseno'];
	$vrdis = $dato['caf_valor_diseno'];
	$swcotizliquidis = $dato['caf_sw_cotiz_liqui_diseno'];
	$numotlic = $dato['caf_num_ot_licencia'];
	$vrlic = $dato['caf_valor_licencia'];
	$swcotizliquilic = $dato['caf_sw_cotiz_liqui_licencia'];
	$numotint = $dato['caf_num_ot_interventoria'];
	$vrint = $dato['caf_valor_interventoria'];
	$swcotizliquiint = $dato['caf_sw_cotiz_liqui_interventoria'];
	$numotcons = $dato['caf_num_ot_constructor'];
	$vrcons = $dato['caf_valor_constructor'];
	$swcotizliquicons = $dato['caf_sw_cotiz_liqui_constructor'];
	$numotseg = $dato['caf_num_ot_seguridad'];
	$vrseg = $dato['caf_valor_seguridad'];
	$swcotizliquiseg = $dato['caf_sw_cotiz_liqui_seguridad'];
	
	if($swcotizliquiinm == 1){ $swcotizliquiinm = 'COTIZACIÓN'; }elseif($swcotizliquiinm == 2){ $swcotizliquiinm = 'LIQUIDACIÓN'; }else{ $swcotizliquiinm = 'SIN DEFINIR'; }
	if($swcotizliquivis == 1){ $swcotizliquivis = 'COTIZACIÓN'; }elseif($swcotizliquivis == 2){ $swcotizliquivis = 'LIQUIDACIÓN'; }else{ $swcotizliquivis = 'SIN DEFINIR'; }
	if($swcotizliquidis == 1){ $swcotizliquidis = 'COTIZACIÓN'; }elseif($swcotizliquidis == 2){ $swcotizliquidis = 'LIQUIDACIÓN'; }else{ $swcotizliquidis = 'SIN DEFINIR'; }
	if($swcotizliquilic == 1){ $swcotizliquilic = 'COTIZACIÓN'; }elseif($swcotizliquilic == 2){ $swcotizliquilic = 'LIQUIDACIÓN'; }else{ $swcotizliquilic = 'SIN DEFINIR'; }
	if($swcotizliquiint == 1){ $swcotizliquiint = 'COTIZACIÓN'; }elseif($swcotizliquiint == 2){ $swcotizliquiint = 'LIQUIDACIÓN'; }else{ $swcotizliquiint = 'SIN DEFINIR'; }
	if($swcotizliquicons == 1){ $swcotizliquicons = 'COTIZACIÓN'; }elseif($swcotizliquicons == 2){ $swcotizliquicons = 'LIQUIDACIÓN'; }else{ $swcotizliquicons = 'SIN DEFINIR'; }
	if($swcotizliquiseg == 1){ $swcotizliquiseg = 'COTIZACIÓN'; }elseif($swcotizliquiseg == 2){ $swcotizliquiseg = 'LIQUIDACIÓN'; }else{ $swcotizliquiseg = 'SIN DEFINIR'; }
	
	if($feciniinmob == '0000-00-00'){ $feciniinmob = ''; }
	if($fecentinmob == '0000-00-00'){ $fecentinmob = ''; }
	if($fecvis == '0000-00-00'){ $fecvis = ''; }
	if($fecentvis == '0000-00-00'){ $fecentvis = ''; }
	if($fecinidis == '0000-00-00'){ $fecinidis = ''; }
	if($fecentdis == '0000-00-00'){ $fecentdis = ''; }
	if($fecinilic == '0000-00-00'){ $fecinilic = ''; }
	if($fecentlic == '0000-00-00'){ $fecentlic = ''; }
	if($fecteoent == '0000-00-00'){ $fecteoent = ''; }
	if($feciniobra == '0000-00-00'){ $feciniobra = ''; }
	if($fecpedsum == '0000-00-00'){ $fecpedsum = ''; }
	if($fecentatm == '0000-00-00'){ $fecentatm = ''; }
	if($fecentcotiz == '0000-00-00'){ $fecentcotiz = ''; }
	if($fecentliq == '0000-00-00'){ $fecentliq = ''; }
	if($fecingseg == '0000-00-00'){ $fecingseg = ''; }
	if($fecteoinm == '0000-00-00'){ $fecteoinm = ''; }
	if($fecteovis == '0000-00-00'){ $fecteovis = ''; }
	if($fecteodis == '0000-00-00'){ $fecteodis = ''; }
	if($fecteolic == '0000-00-00'){ $fecteolic = ''; }
											
	/**************** DATOS DE LAS COLUMNAS ****************/ 

	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('A'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("A".$j, $clacaj);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('B'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("B".$j, $nomcaj);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('C'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("C".$j, $dir);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('D'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("D".$j, $anocon);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('E'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("E".$j, $reg);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('F'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("F".$j, $dep);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('G'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("G".$j, $mun);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('H'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("H".$j, $tip);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('I'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("I".$j, $tipint);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('J'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("J".$j, $moda);
	//***************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('K'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("K".$j, $estpro);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('L'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("L".$j, $codcaj);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('M'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("M".$j, $cencos);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('N'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("N".$j, $refmaq);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('O'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("O".$j, $ubi);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('P'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("P".$j, $codsuc);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('Q'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("Q".$j, $ubiamt);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('R'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("R".$j, $ati);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('S'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("S".$j, $rie);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('T'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("T".$j, $are);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('U'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("U".$j, $fecapagadoatm);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('V'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("V".$j, $reqinm);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('W'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("W".$j, $inmob);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('X'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("X".$j, $feciniinmob);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('Y'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("Y".$j, $fecteoinm);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('Z'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("Z".$j, $fecentinmob);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AA'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AA".$j, $diasinm);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AB'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AB".$j, $estinmob);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AC'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AC".$j, $notasinm);
	//****************************

	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AD'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AD".$j, $reqvis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AE'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AE".$j, $vis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AF'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AF".$j, $fecvis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AG'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AG".$j, $fecteovis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AH'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AH".$j, $fecentvis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AI'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AI".$j, $diasvis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AJ'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AJ".$j, $estvis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AK'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AK".$j, $notasvis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AL'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AL".$j, $reqdis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AM'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AM".$j, $dis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AN'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AN".$j, $fecinidis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AO'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AO".$j, $fecteodis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AP'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AP".$j, $fecentdis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AQ'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AQ".$j, $diasdis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AR'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AR".$j, $estdis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AS'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AS".$j, $notasdis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AT'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AT".$j, $reqlic);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AU'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AU".$j, $lic);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AV'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AV".$j, $fecinilic);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AW'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AW".$j, $fecteolic);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AX'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AX".$j, $fecentlic);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AY'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AY".$j, $diaslic);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AZ'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AZ".$j, $estlic);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BA'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BA".$j, $cura);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BB'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BB".$j, $radi);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BC'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BC".$j, $notaslic);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BD'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BD".$j, $int);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BE'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BE".$j, $fecteoent);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BF'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BF".$j, $feciniobra);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BG'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BG".$j, $fecpedsum);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BH'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BH".$j, $fecaprocom);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BI'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BI".$j, $aprovcotiz);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BJ'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BJ".$j, $cancom);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BK'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BK".$j, $fecentcan);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BL'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BL".$j, $aprovliqui);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BM'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BM".$j, $swimgnex);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BN'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BN".$j, $swfot);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BO'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BO".$j, $swact);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BP'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BP".$j, $notasint);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BQ'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BQ".$j, $cons);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BR'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BR".$j, $feciniobra);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BS'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BS".$j, $fecteoent);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BT'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BT".$j, $fecentatm);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BU'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BU".$j, $porava);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BV'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BV".$j, $fecentcotiz);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BW'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BW".$j, $fecentliq);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BX'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BX".$j, $notascons);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BY'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BY".$j, $seg);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BZ'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BZ".$j, $fecingseg);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CA'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CA".$j, $codmon);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CB'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CB".$j, $notasseg);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CC'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CC".$j, $numotinm);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CD'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CD".$j, $vrinm);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CE'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CE".$j, $swcotizliquiinm);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CF'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CF".$j, $numotvis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CG'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CG".$j, $vrvis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CH'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CH".$j, $swcotizliquivis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CI'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CI".$j, $numotdis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CJ'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CJ".$j, $vrdis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CK'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CK".$j, $swcotizliquidis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CL'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CL".$j, $numotlic);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CM'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CM".$j, $vrlic);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CN'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CN".$j, $swcotizliquilic);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CO'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CO".$j, $numotint);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CP'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CP".$j, $vrint);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CQ'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CQ".$j, $swcotizliquiint);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CR'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CR".$j, $numotcons);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CS'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CS".$j, $vrcons);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CT'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CT".$j, $swcotizliquicons);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CU'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CU".$j, $numotseg);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CV'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CV".$j, $vrseg);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CW'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CW".$j, $swcotizliquiseg);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CX'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CX".$j, $vrinm+$vrvis+$vrdis+$vrlic+$vrint+$vrcons+$vrseg);
	//****************************

	$j++;
	$objPHPExcel->getActiveSheet()->getStyle('A'.$j)-> applyFromArray($styleA3);//
	$objPHPExcel->getActiveSheet()->getCell('A'.$j)->setValue("NOMBRE TAREA");
	$objPHPExcel->getActiveSheet()->getStyle('A'.$j)->getFill()->getStartColor()->setARGB('00D8D8D8');//COLOR DE FONDO
	$objPHPExcel->getActiveSheet()->getStyle('B'.$j)-> applyFromArray($styleA3);//
	$objPHPExcel->getActiveSheet()->getCell('B'.$j)->setValue("DURACIÓN");
	$objPHPExcel->getActiveSheet()->getStyle('B'.$j)->getFill()->getStartColor()->setARGB('00D8D8D8');//COLOR DE FONDO
	$objPHPExcel->getActiveSheet()->getStyle('C'.$j)-> applyFromArray($styleA3);//
	$objPHPExcel->getActiveSheet()->getCell('C'.$j)->setValue("COMIENZO");
	$objPHPExcel->getActiveSheet()->getStyle('C'.$j)->getFill()->getStartColor()->setARGB('00D8D8D8');//COLOR DE FONDO
	$objPHPExcel->getActiveSheet()->getStyle('D'.$j)-> applyFromArray($styleA3);//
	$objPHPExcel->getActiveSheet()->getCell('D'.$j)->setValue("FIN");
	$objPHPExcel->getActiveSheet()->getStyle('D'.$j)->getFill()->getStartColor()->setARGB('00D8D8D8');//COLOR DE FONDO
	$j++;
	//echo "select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int)  inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where c.caj_clave_int = ".$clacaj."";
	$con1 = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int)  inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where c.caj_clave_int = ".$clacaj."");
	$dato1 = mysqli_fetch_array($con1);
	$mod = $dato1['mod_clave_int'];
	$conmod = mysqli_query($conectar,"select * from modalidad where mod_clave_int = ".$mod."");
	$datomod = mysqli_fetch_array($conmod);
	$modinm = $datomod['mod_sw_inmobiliaria'];
	$dinm = $datomod['mod_dias_inmobiliaria'];
	$modvis = $datomod['mod_sw_visita'];
	$dvis = $datomod['mod_dias_visita'];
	$modcom = $datomod['mod_sw_aprocomite'];
	$dcom = $datomod['mod_dias_aprocomite'];
	$modcon = $datomod['mod_sw_contrato'];
	$dcon = $datomod['mod_dias_contrato'];
	$moddis = $datomod['mod_sw_diseno'];
	$ddis = $datomod['mod_dias_diseno'];
	$modlic = $datomod['mod_sw_licencia'];
	$dlic = $datomod['mod_dias_licencia'];
	$modpre = $datomod['mod_sw_preliminar'];
	$dpre = $datomod['mod_dias_preliminar'];
	$dcons = $datomod['mod_dias_constructor'];
	$modint = $datomod['mod_sw_interventoria'];
	
	//Info Inmobiliaria
	$feciniinmob = $dato1['cai_fecha_ini_inmobiliaria'];
	//Info Visita Local
	$fecvis = $dato1['cav_fecha_visita'];
	//Info Diseño
	$fecinidis = $dato1['cad_fecha_inicio_diseno'];
	//Info Licencia
	$fecinilic = $dato1['cal_fecha_inicio_licencia'];
	//Info Interventoria
	$fecteoent = $dato1['cin_fecha_teorica_entrega'];
	
	$conlin = mysqli_query($conectar,"select * from linea_base where caj_clave_int = ".$clacaj."");
	$datolin = mysqli_fetch_array($conlin);
	$fii = $datolin['lib_fec_inmobiliaria'];
	$fti = $datolin['lib_fecteo_inmobiliaria'];
	$fiv = $datolin['lib_fec_visita'];
	$ftv = $datolin['lib_fecteo_visita'];
	$ficom = $datolin['lib_fec_comite'];
	$ftcom = $datolin['lib_fecteo_comite'];
	$ficon = $datolin['lib_fec_contrato'];
	$ftcon = $datolin['lib_fecteo_contrato'];
	$fid = $datolin['lib_fec_diseno'];
	$ftd = $datolin['lib_fecteo_diseno'];
	$fil = $datolin['lib_fec_licencia'];
	$ftl = $datolin['lib_fecteo_licencia'];
	$fipre = $datolin['lib_fec_preliminar'];
	$ftpre = $datolin['lib_fecteo_preliminar'];
	$fiint = $datolin['lib_fec_interventoria'];
	$ftint = $datolin['lib_fecteo_interventoria'];
	
	if(strtotime($fecteoent) < strtotime($ftint)){ $fecteodias = $ftint; }else{ $fecteodias = $fecteoent; }
	if(strtotime($feciniinmob) < strtotime($fii)){ $fecinidias = $feciniinmob; }else{ $fecinidias = $fii; }
	
	$con1 = mysqli_query($conectar,"select DATEDIFF('".$fecteodias."','".$fecinidias."') dias from usuario limit 1");
	$dato1 = mysqli_fetch_array($con1);
	$diasreales = $dato1['dias'];
	
	for($k = 0; $k <= $diasreales; $k++)
	{
		$con1 = mysqli_query($conectar,"select ADDDATE('".$fecinidias."', INTERVAL $k DAY) fec from usuario limit 1");
		$dato1 = mysqli_fetch_array($con1);
		$fec = $dato1['fec'];
						
		$vecfec[$k] = $fec;
	}
	
	$tamano = sizeof($vecfec);
	$semcom = abs(date("W",strtotime($feciniinmob))-date("W",strtotime($fecteoent)));
			
	$auxsemcor = 0;
	$auxsemlin = 0;
	$swcor = 1;
	$swlin = 1;
	for($n = 0; $n < 8; $n++)
	{
		if($n == 0)
		{
			$nomtar = "Inmobiliaria";
			$con1 = mysqli_query($conectar,"select ADDDATE('".$feciniinmob."', INTERVAL ".$dinm." DAY) dias from usuario LIMIT 1");
			$dato1 = mysqli_fetch_array($con1);
			$fecteo = $dato1['dias'];
			$fecini = $feciniinmob;
			$fi = $fii;
			$ft = $fti;
		}
		else
		if($n == 1)
		{
			$nomtar = "Visita";
			$con1 = mysqli_query($conectar,"select ADDDATE('".$fecvis."', INTERVAL ".$dvis." DAY) dias from usuario LIMIT 1");
			$dato1 = mysqli_fetch_array($con1);
			$fecteo = $dato1['dias'];
			$fecini = $fecvis;
			$fi = $fiv;
			$ft = $ftv;
		}
		else
		if($n == 2)
		{
			$nomtar = "Aprob. comité";
			$con1 = mysqli_query($conectar,"select ADDDATE('".$fecvis."', INTERVAL ".$dvis." DAY) dias from usuario LIMIT 1");
			$dato1 = mysqli_fetch_array($con1);
			$fecteovis = $dato1['dias'];
			
			$con1 = mysqli_query($conectar,"select ADDDATE('".$fecteovis."', INTERVAL ".$dcom." DAY) dias from usuario LIMIT 1");
			$dato1 = mysqli_fetch_array($con1);
			$fecteo = $dato1['dias'];
			$fecini = $fecteovis;
			$fi = $ficom;
			$ft = $ftcom;
		}
		else
		if($n == 3)
		{
			$nomtar = "Contrato";						
			$con1 = mysqli_query($conectar,"select ADDDATE('".$fecvis."', INTERVAL ".$dvis." DAY) dias from usuario LIMIT 1");
			$dato1 = mysqli_fetch_array($con1);
			$fecteovis = $dato1['dias'];
			
			$con1 = mysqli_query($conectar,"select ADDDATE('".$fecteovis."', INTERVAL ".$dcom." DAY) dias from usuario LIMIT 1");
			$dato1 = mysqli_fetch_array($con1);
			$fecteocom = $dato1['dias'];
			
			if($modlic == 1)
			{
				$con1 = mysqli_query($conectar,"select ADDDATE('".$fecteocom."', INTERVAL ".$dcon." DAY) dias from usuario LIMIT 1");
				$dato1 = mysqli_fetch_array($con1);
				$fecteo = $dato1['dias'];
			}
			else
			{
				$con1 = mysqli_query($conectar,"select ADDDATE('".$fecteocom."', INTERVAL ".$dcon."+".$ddis." DAY) dias from usuario LIMIT 1");
				$dato1 = mysqli_fetch_array($con1);
				$fecteo = $dato1['dias'];
			}
			
			$fecini = $fecteocom;
			$fi = $ficon;
			$ft = $ftcon;
		}
		else
		if($n == 4)
		{
			$nomtar = "Diseño";
			$con1 = mysqli_query($conectar,"select ADDDATE('".$fecinidis."', INTERVAL ".$ddis." DAY) dias from usuario LIMIT 1");
			$dato1 = mysqli_fetch_array($con1);
			$fecteo = $dato1['dias'];
			$fecini = $fecinidis;
			$fi = $fid;
			$ft = $ftd;
		}
		else
		if($n == 5)
		{
			$nomtar = "Licencia";
			$con1 = mysqli_query($conectar,"select ADDDATE('".$fecinilic."', INTERVAL ".$dlic." DAY) dias from usuario LIMIT 1");
			$dato1 = mysqli_fetch_array($con1);
			$fecteo = $dato1['dias'];
			$fecini = $fecinilic;
			$fi = $fil;
			$ft = $ftl;
		}
		else
		if($n == 6)
		{
			$nomtar = "Preliminar";
			if($modlic == 1)
			{
				$con1 = mysqli_query($conectar,"select ADDDATE('".$fecinilic."', INTERVAL ".$dlic." DAY) dias from usuario LIMIT 1");
				$dato1 = mysqli_fetch_array($con1);
				$fecteolic = $dato1['dias'];
				
				$con1 = mysqli_query($conectar,"select ADDDATE('".$fecteolic."', INTERVAL ".$dpre." DAY) dias from usuario LIMIT 1");
				$dato1 = mysqli_fetch_array($con1);
				$fecteo = $dato1['dias'];
				$fecini = $fecteolic;
			}
			else
			{
				$con1 = mysqli_query($conectar,"select ADDDATE('".$fecvis."', INTERVAL ".$dvis." DAY) dias from usuario LIMIT 1");
				$dato1 = mysqli_fetch_array($con1);
				$fecteovis = $dato1['dias'];
				
				$con1 = mysqli_query($conectar,"select ADDDATE('".$fecteovis."', INTERVAL ".$dcom." DAY) dias from usuario LIMIT 1");
				$dato1 = mysqli_fetch_array($con1);
				$fecteocom = $dato1['dias'];
										
				$con1 = mysqli_query($conectar,"select ADDDATE('".$fecteocom."', INTERVAL ".$dcon." DAY) dias from usuario LIMIT 1");
				$dato1 = mysqli_fetch_array($con1);
				$fecteocon = $dato1['dias'];
				
				$con1 = mysqli_query($conectar,"select ADDDATE('".$fecteocon."', INTERVAL ".$dpre." DAY) dias from usuario LIMIT 1");
				$dato1 = mysqli_fetch_array($con1);
				$fecteo = $dato1['dias'];
				$fecini = $fecteocon;
			}
			$fi = $fipre;
			$ft = $ftpre;
		}
		else
		if($n == 7)
		{
			$nomtar = "Interventoria";
			$fecteo = $fecteoent;
			$con1 = mysqli_query($conectar,"select ADDDATE('".$fecteo."', INTERVAL -".$dcons." DAY) dias from usuario LIMIT 1");
			$dato1 = mysqli_fetch_array($con1);
			$fecini = $dato1['dias'];
			
			//Linea base
			$fi = $fiint;
			$ft = $ftint;
		}
		
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle('A'.$j)-> applyFromArray($styleA4);//
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("A".$j, $nomtar);
		//****************************
		
		$condur = mysqli_query($conectar,"select DATEDIFF('".$fecteo."','".$fecini."') duracion from usuario limit 1");
		$datodur = mysqli_fetch_array($condur);
		if($datodur['duracion'] != ''){ $dur = $datodur['duracion']." Días"; }else{ $dur = "0 Días"; }
		
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle('B'.$j)-> applyFromArray($styleA4);//
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("B".$j, $dur);
		//****************************
		
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle('C'.$j)-> applyFromArray($styleA4);//
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("C".$j, $fecini);
		//****************************
		
		if($fecteo != ''){ $fete = $fecteo; }else{ $fete = '0000-00-00'; }
		
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle('D'.$j)-> applyFromArray($styleA4);//
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("D".$j, $fete);
		//****************************
		/*if($fecini <> '0000-00-00')
		{
			$con1 = mysqli_query($conectar,"select DATEDIFF('".$fecteo."','".$fecini."') dias from usuario limit 1");
			$dato1 = mysqli_fetch_array($con1);
			$dias = $dato1['dias'];
			for($p = 0; $p <= $dias; $p++)
			{
				$con1 = mysqli_query($conectar,"select ADDDATE('".$fecini."', INTERVAL $p DAY) fec from usuario limit 1");
				$dato1 = mysqli_fetch_array($con1);
				$fec = $dato1['fec'];
				for($q = 0; $q < $tamano; $q++)
				{
					if($q == 0){ $letra = 'E'; }elseif($q == 1){ $letra = 'F'; }elseif($q == 2){ $letra = 'G'; }elseif($q == 3){ $letra = 'H'; }elseif($q == 4){ $letra = 'I'; }elseif($q == 5){ $letra = 'J'; }elseif($q == 6){ $letra = 'K'; }elseif($q == 7){ $letra = 'L'; }elseif($q == 8){ $letra = 'M'; }elseif($q == 9){ $letra = 'N'; }elseif($q == 10){ $letra = 'O'; }elseif($q == 11){ $letra = 'P'; }elseif($q == 12){ $letra = 'Q'; }elseif($q == 13){ $letra = 'R'; }elseif($q == 14){ $letra = 'S'; }elseif($q == 15){ $letra = 'T'; }elseif($q == 16){ $letra = 'U'; }elseif($q == 17){ $letra = 'V'; }elseif($q == 18){ $letra = 'W'; }elseif($q == 19){ $letra = 'X'; }elseif($q == 20){ $letra = 'Y'; }elseif($q == 21){ $letra = 'Z'; }elseif($q == 22){ $letra = 'AA'; }elseif($q == 23){ $letra = 'AB'; }elseif($q == 24){ $letra = 'AC'; }elseif($q == 25){ $letra = 'AD'; }elseif($q == 26){ $letra = 'AE'; }elseif($q == 27){ $letra = 'AF'; }elseif($q == 28){ $letra = 'AG'; }elseif($q == 29){ $letra = 'AH'; }elseif($q == 30){ $letra = 'AI'; }elseif($q == 31){ $letra = 'AJ'; }elseif($q == 32){ $letra = 'AK'; }elseif($q == 33){ $letra = 'AL'; }elseif($q == 34){ $letra = 'AM'; }elseif($q == 35){ $letra = 'AN'; }elseif($q == 36){ $letra = 'AO'; }elseif($q == 37){ $letra = 'AP'; }elseif($q == 38){ $letra = 'AQ'; }elseif($q == 39){ $letra = 'AR'; }elseif($q == 40){ $letra = 'AS'; }elseif($q == 41){ $letra = 'AT'; }elseif($q == 42){ $letra = 'AU'; }elseif($q == 43){ $letra = 'AV'; }elseif($q == 44){ $letra = 'AW'; }elseif($q == 45){ $letra = 'AX'; }elseif($q == 46){ $letra = 'AY'; }elseif($q == 47){ $letra = 'AZ'; }elseif($q == 48){ $letra = 'BA'; }elseif($q == 49){ $letra = 'BB'; }elseif($q == 50){ $letra = 'BC'; }elseif($q == 51){ $letra = 'BD'; }elseif($q == 52){ $letra = 'BE'; }elseif($q == 53){ $letra = 'BF'; }elseif($q == 54){ $letra = 'BG'; }elseif($q == 55){ $letra = 'BH'; }elseif($q == 56){ $letra = 'BI'; }elseif($q == 57){ $letra = 'BJ'; }elseif($q == 58){ $letra = 'BK'; }elseif($q == 59){ $letra = 'BL'; }elseif($q == 60){ $letra = 'BM'; }elseif($q == 61){ $letra = 'BN'; }elseif($q == 62){ $letra = 'BO'; }elseif($q == 63){ $letra = 'BP'; }elseif($q == 64){ $letra = 'BQ'; }elseif($q == 65){ $letra = 'BR'; }elseif($q == 66){ $letra = 'BS'; }elseif($q == 67){ $letra = 'BT'; }elseif($q == 68){ $letra = 'BU'; }elseif($q == 69){ $letra = 'BV'; }elseif($q == 70){ $letra = 'BW'; }elseif($q == 71){ $letra = 'BX'; }elseif($q == 72){ $letra = 'BY'; }elseif($q == 73){ $letra = 'BZ'; }elseif($q == 74){ $letra = 'CA'; }elseif($q == 75){ $letra = 'CB'; }elseif($q == 76){ $letra = 'CC'; }elseif($q == 77){ $letra = 'CD'; }elseif($q == 78){ $letra = 'CE'; }elseif($q == 79){ $letra = 'CF'; }elseif($q == 80){ $letra = 'CG'; }elseif($q == 81){ $letra = 'CH'; }elseif($q == 82){ $letra = 'CI'; }elseif($q == 83){ $letra = 'CJ'; }elseif($q == 84){ $letra = 'CK'; }elseif($q == 85){ $letra = 'CL'; }elseif($q == 86){ $letra = 'CM'; }elseif($q == 87){ $letra = 'CN'; }elseif($q == 88){ $letra = 'CO'; }elseif($q == 89){ $letra = 'CP'; }elseif($q == 90){ $letra = 'CQ'; }elseif($q == 91){ $letra = 'CR'; }elseif($q == 92){ $letra = 'CS'; }elseif($q == 93){ $letra = 'CT'; }elseif($q == 94){ $letra = 'CU'; }elseif($q == 95){ $letra = 'CV'; }elseif($q == 96){ $letra = 'CW'; }elseif($q == 97){ $letra = 'CX'; }elseif($q == 98){ $letra = 'CY'; }elseif($q == 99){ $letra = 'CZ'; }elseif($q == 100){ $letra = 'DA'; }elseif($q == 101){ $letra = 'DB'; }elseif($q == 102){ $letra = 'DC'; }elseif($q == 103){ $letra = 'DD'; }elseif($q == 104){ $letra = 'DE'; }elseif($q == 105){ $letra = 'DF'; }elseif($q == 106){ $letra = 'DG'; }elseif($q == 107){ $letra = 'DH'; }elseif($q == 108){ $letra = 'DI'; }elseif($q == 109){ $letra = 'DJ'; }elseif($q == 110){ $letra = 'DK'; }elseif($q == 111){ $letra = 'DL'; }elseif($q == 112){ $letra = 'DM'; }elseif($q == 113){ $letra = 'DN'; }elseif($q == 114){ $letra = 'DO'; }elseif($q == 115){ $letra = 'DP'; }elseif($q == 116){ $letra = 'DQ'; }elseif($q == 117){ $letra = 'DR'; }elseif($q == 118){ $letra = 'DS'; }elseif($q == 119){ $letra = 'DT'; }elseif($q == 120){ $letra = 'DU'; }elseif($q == 121){ $letra = 'DV'; }elseif($q == 122){ $letra = 'DW'; }elseif($q == 123){ $letra = 'DX'; }elseif($q == 124){ $letra = 'DY'; }elseif($q == 125){ $letra = 'DZ'; }elseif($q == 126){ $letra = 'EA'; }elseif($q == 127){ $letra = 'EB'; }elseif($q == 128){ $letra = 'EC'; }elseif($q == 129){ $letra = 'ED'; }elseif($q == 130){ $letra = 'EE'; }elseif($q == 131){ $letra = 'EF'; }elseif($q == 132){ $letra = 'EG'; }elseif($q == 133){ $letra = 'EH'; }elseif($q == 134){ $letra = 'EI'; }elseif($q == 135){ $letra = 'EJ'; }elseif($q == 136){ $letra = 'EK'; }elseif($q == 137){ $letra = 'EL'; }elseif($q == 138){ $letra = 'EM'; }elseif($q == 139){ $letra = 'EN'; }elseif($q == 140){ $letra = 'EO'; }elseif($q == 141){ $letra = 'EP'; }elseif($q == 142){ $letra = 'EQ'; }elseif($q == 143){ $letra = 'ER'; }elseif($q == 144){ $letra = 'ES'; }elseif($q == 145){ $letra = 'ET'; }elseif($q == 146){ $letra = 'EU'; }elseif($q == 147){ $letra = 'EV'; }elseif($q == 148){ $letra = 'EW'; }elseif($q == 149){ $letra = 'EX'; }elseif($q == 150){ $letra = 'EY'; }elseif($q == 151){ $letra = 'EZ'; }elseif($q == 152){ $letra = 'FA'; }elseif($q == 153){ $letra = 'FB'; }elseif($q == 154){ $letra = 'FC'; }elseif($q == 155){ $letra = 'FD'; }elseif($q == 156){ $letra = 'FE'; }elseif($q == 157){ $letra = 'FF'; }elseif($q == 158){ $letra = 'FG'; }elseif($q == 159){ $letra = 'FH'; }elseif($q == 160){ $letra = 'FI'; }elseif($q == 161){ $letra = 'FJ'; }elseif($q == 162){ $letra = 'FK'; }elseif($q == 163){ $letra = 'FL'; }elseif($q == 164){ $letra = 'FM'; }elseif($q == 165){ $letra = 'FN'; }elseif($q == 166){ $letra = 'FO'; }elseif($q == 167){ $letra = 'FP'; }elseif($q == 168){ $letra = 'FQ'; }elseif($q == 169){ $letra = 'FR'; }elseif($q == 170){ $letra = 'FS'; }elseif($q == 171){ $letra = 'FT'; }elseif($q == 172){ $letra = 'FU'; }elseif($q == 173){ $letra = 'FV'; }elseif($q == 174){ $letra = 'FW'; }elseif($q == 175){ $letra = 'FX'; }elseif($q == 176){ $letra = 'FY'; }elseif($q == 177){ $letra = 'FZ'; }elseif($q == 178){ $letra = 'GA'; }elseif($q == 179){ $letra = 'GB'; }elseif($q == 180){ $letra = 'GC'; }elseif($q == 181){ $letra = 'GD'; }elseif($q == 182){ $letra = 'GE'; }elseif($q == 183){ $letra = 'GF'; }elseif($q == 184){ $letra = 'GG'; }elseif($q == 185){ $letra = 'GH'; }elseif($q == 186){ $letra = 'GI'; }elseif($q == 187){ $letra = 'GJ'; }elseif($q == 188){ $letra = 'GK'; }elseif($q == 189){ $letra = 'GL'; }elseif($q == 190){ $letra = 'GM'; }elseif($q == 191){ $letra = 'GN'; }elseif($q == 192){ $letra = 'GO'; }elseif($q == 193){ $letra = 'GP'; }elseif($q == 194){ $letra = 'GQ'; }elseif($q == 195){ $letra = 'GR'; }elseif($q == 196){ $letra = 'GS'; }elseif($q == 197){ $letra = 'GT'; }elseif($q == 198){ $letra = 'GU'; }elseif($q == 199){ $letra = 'GV'; }elseif($q == 200){ $letra = 'GW'; }elseif($q == 201){ $letra = 'GX'; }elseif($q == 202){ $letra = 'GY'; }elseif($q == 203){ $letra = 'GZ'; }
					if($vecfec[$q] == $fec)
					{
						$objPHPExcel->getActiveSheet()->getStyle($letra.$j)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
						$objPHPExcel->getActiveSheet()->getStyle($letra.$j)->getFill()->getStartColor()->setARGB('00D8D8D8');//COLOR DE FONDO
						break;
					}
				}
			}
		}*/
		
		$j++;
		
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle('A'.$j)-> applyFromArray($styleA3);//
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("A".$j, $nomtar);
		//****************************
		
		$condur = mysqli_query($conectar,"select DATEDIFF('".$ft."','".$fi."') duracion from usuario limit 1");
		$datodur = mysqli_fetch_array($condur);
		if($datodur['duracion'] != ''){ $dur = $datodur['duracion']." Días"; }else{ $dur = "0 Días"; }
		
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle('B'.$j)-> applyFromArray($styleA3);//
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("B".$j, $dur);
		//****************************
		
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle('C'.$j)-> applyFromArray($styleA3);//
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("C".$j, $fi);
		//****************************
		
		if($ft != ''){ $fete = $ft; }else{ $fete = '0000-00-00'; }
		
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle('D'.$j)-> applyFromArray($styleA3);//
		$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("D".$j, $fete);
		//****************************
		
		/*if($fi <> '0000-00-00')
		{
			$con1 = mysqli_query($conectar,"select DATEDIFF('".$ft."','".$fi."') dias from usuario limit 1");
			$dato1 = mysqli_fetch_array($con1);
			$dias = $dato1['dias'];
			for($p = 0; $p <= $dias; $p++)
			{
				$con1 = mysqli_query($conectar,"select ADDDATE('".$fi."', INTERVAL $p DAY) fec from usuario limit 1");
				$dato1 = mysqli_fetch_array($con1);
				$fec = $dato1['fec'];
				for($q = 0; $q < $tamano; $q++)
				{
					if($q == 0){ $letra = 'E'; }elseif($q == 1){ $letra = 'F'; }elseif($q == 2){ $letra = 'G'; }elseif($q == 3){ $letra = 'H'; }elseif($q == 4){ $letra = 'I'; }elseif($q == 5){ $letra = 'J'; }elseif($q == 6){ $letra = 'K'; }elseif($q == 7){ $letra = 'L'; }elseif($q == 8){ $letra = 'M'; }elseif($q == 9){ $letra = 'N'; }elseif($q == 10){ $letra = 'O'; }elseif($q == 11){ $letra = 'P'; }elseif($q == 12){ $letra = 'Q'; }elseif($q == 13){ $letra = 'R'; }elseif($q == 14){ $letra = 'S'; }elseif($q == 15){ $letra = 'T'; }elseif($q == 16){ $letra = 'U'; }elseif($q == 17){ $letra = 'V'; }elseif($q == 18){ $letra = 'W'; }elseif($q == 19){ $letra = 'X'; }elseif($q == 20){ $letra = 'Y'; }elseif($q == 21){ $letra = 'Z'; }elseif($q == 22){ $letra = 'AA'; }elseif($q == 23){ $letra = 'AB'; }elseif($q == 24){ $letra = 'AC'; }elseif($q == 25){ $letra = 'AD'; }elseif($q == 26){ $letra = 'AE'; }elseif($q == 27){ $letra = 'AF'; }elseif($q == 28){ $letra = 'AG'; }elseif($q == 29){ $letra = 'AH'; }elseif($q == 30){ $letra = 'AI'; }elseif($q == 31){ $letra = 'AJ'; }elseif($q == 32){ $letra = 'AK'; }elseif($q == 33){ $letra = 'AL'; }elseif($q == 34){ $letra = 'AM'; }elseif($q == 35){ $letra = 'AN'; }elseif($q == 36){ $letra = 'AO'; }elseif($q == 37){ $letra = 'AP'; }elseif($q == 38){ $letra = 'AQ'; }elseif($q == 39){ $letra = 'AR'; }elseif($q == 40){ $letra = 'AS'; }elseif($q == 41){ $letra = 'AT'; }elseif($q == 42){ $letra = 'AU'; }elseif($q == 43){ $letra = 'AV'; }elseif($q == 44){ $letra = 'AW'; }elseif($q == 45){ $letra = 'AX'; }elseif($q == 46){ $letra = 'AY'; }elseif($q == 47){ $letra = 'AZ'; }elseif($q == 48){ $letra = 'BA'; }elseif($q == 49){ $letra = 'BB'; }elseif($q == 50){ $letra = 'BC'; }elseif($q == 51){ $letra = 'BD'; }elseif($q == 52){ $letra = 'BE'; }elseif($q == 53){ $letra = 'BF'; }elseif($q == 54){ $letra = 'BG'; }elseif($q == 55){ $letra = 'BH'; }elseif($q == 56){ $letra = 'BI'; }elseif($q == 57){ $letra = 'BJ'; }elseif($q == 58){ $letra = 'BK'; }elseif($q == 59){ $letra = 'BL'; }elseif($q == 60){ $letra = 'BM'; }elseif($q == 61){ $letra = 'BN'; }elseif($q == 62){ $letra = 'BO'; }elseif($q == 63){ $letra = 'BP'; }elseif($q == 64){ $letra = 'BQ'; }elseif($q == 65){ $letra = 'BR'; }elseif($q == 66){ $letra = 'BS'; }elseif($q == 67){ $letra = 'BT'; }elseif($q == 68){ $letra = 'BU'; }elseif($q == 69){ $letra = 'BV'; }elseif($q == 70){ $letra = 'BW'; }elseif($q == 71){ $letra = 'BX'; }elseif($q == 72){ $letra = 'BY'; }elseif($q == 73){ $letra = 'BZ'; }elseif($q == 74){ $letra = 'CA'; }elseif($q == 75){ $letra = 'CB'; }elseif($q == 76){ $letra = 'CC'; }elseif($q == 77){ $letra = 'CD'; }elseif($q == 78){ $letra = 'CE'; }elseif($q == 79){ $letra = 'CF'; }elseif($q == 80){ $letra = 'CG'; }elseif($q == 81){ $letra = 'CH'; }elseif($q == 82){ $letra = 'CI'; }elseif($q == 83){ $letra = 'CJ'; }elseif($q == 84){ $letra = 'CK'; }elseif($q == 85){ $letra = 'CL'; }elseif($q == 86){ $letra = 'CM'; }elseif($q == 87){ $letra = 'CN'; }elseif($q == 88){ $letra = 'CO'; }elseif($q == 89){ $letra = 'CP'; }elseif($q == 90){ $letra = 'CQ'; }elseif($q == 91){ $letra = 'CR'; }elseif($q == 92){ $letra = 'CS'; }elseif($q == 93){ $letra = 'CT'; }elseif($q == 94){ $letra = 'CU'; }elseif($q == 95){ $letra = 'CV'; }elseif($q == 96){ $letra = 'CW'; }elseif($q == 97){ $letra = 'CX'; }elseif($q == 98){ $letra = 'CY'; }elseif($q == 99){ $letra = 'CZ'; }elseif($q == 100){ $letra = 'DA'; }elseif($q == 101){ $letra = 'DB'; }elseif($q == 102){ $letra = 'DC'; }elseif($q == 103){ $letra = 'DD'; }elseif($q == 104){ $letra = 'DE'; }elseif($q == 105){ $letra = 'DF'; }elseif($q == 106){ $letra = 'DG'; }elseif($q == 107){ $letra = 'DH'; }elseif($q == 108){ $letra = 'DI'; }elseif($q == 109){ $letra = 'DJ'; }elseif($q == 110){ $letra = 'DK'; }elseif($q == 111){ $letra = 'DL'; }elseif($q == 112){ $letra = 'DM'; }elseif($q == 113){ $letra = 'DN'; }elseif($q == 114){ $letra = 'DO'; }elseif($q == 115){ $letra = 'DP'; }elseif($q == 116){ $letra = 'DQ'; }elseif($q == 117){ $letra = 'DR'; }elseif($q == 118){ $letra = 'DS'; }elseif($q == 119){ $letra = 'DT'; }elseif($q == 120){ $letra = 'DU'; }elseif($q == 121){ $letra = 'DV'; }elseif($q == 122){ $letra = 'DW'; }elseif($q == 123){ $letra = 'DX'; }elseif($q == 124){ $letra = 'DY'; }elseif($q == 125){ $letra = 'DZ'; }elseif($q == 126){ $letra = 'EA'; }elseif($q == 127){ $letra = 'EB'; }elseif($q == 128){ $letra = 'EC'; }elseif($q == 129){ $letra = 'ED'; }elseif($q == 130){ $letra = 'EE'; }elseif($q == 131){ $letra = 'EF'; }elseif($q == 132){ $letra = 'EG'; }elseif($q == 133){ $letra = 'EH'; }elseif($q == 134){ $letra = 'EI'; }elseif($q == 135){ $letra = 'EJ'; }elseif($q == 136){ $letra = 'EK'; }elseif($q == 137){ $letra = 'EL'; }elseif($q == 138){ $letra = 'EM'; }elseif($q == 139){ $letra = 'EN'; }elseif($q == 140){ $letra = 'EO'; }elseif($q == 141){ $letra = 'EP'; }elseif($q == 142){ $letra = 'EQ'; }elseif($q == 143){ $letra = 'ER'; }elseif($q == 144){ $letra = 'ES'; }elseif($q == 145){ $letra = 'ET'; }elseif($q == 146){ $letra = 'EU'; }elseif($q == 147){ $letra = 'EV'; }elseif($q == 148){ $letra = 'EW'; }elseif($q == 149){ $letra = 'EX'; }elseif($q == 150){ $letra = 'EY'; }elseif($q == 151){ $letra = 'EZ'; }elseif($q == 152){ $letra = 'FA'; }elseif($q == 153){ $letra = 'FB'; }elseif($q == 154){ $letra = 'FC'; }elseif($q == 155){ $letra = 'FD'; }elseif($q == 156){ $letra = 'FE'; }elseif($q == 157){ $letra = 'FF'; }elseif($q == 158){ $letra = 'FG'; }elseif($q == 159){ $letra = 'FH'; }elseif($q == 160){ $letra = 'FI'; }elseif($q == 161){ $letra = 'FJ'; }elseif($q == 162){ $letra = 'FK'; }elseif($q == 163){ $letra = 'FL'; }elseif($q == 164){ $letra = 'FM'; }elseif($q == 165){ $letra = 'FN'; }elseif($q == 166){ $letra = 'FO'; }elseif($q == 167){ $letra = 'FP'; }elseif($q == 168){ $letra = 'FQ'; }elseif($q == 169){ $letra = 'FR'; }elseif($q == 170){ $letra = 'FS'; }elseif($q == 171){ $letra = 'FT'; }elseif($q == 172){ $letra = 'FU'; }elseif($q == 173){ $letra = 'FV'; }elseif($q == 174){ $letra = 'FW'; }elseif($q == 175){ $letra = 'FX'; }elseif($q == 176){ $letra = 'FY'; }elseif($q == 177){ $letra = 'FZ'; }elseif($q == 178){ $letra = 'GA'; }elseif($q == 179){ $letra = 'GB'; }elseif($q == 180){ $letra = 'GC'; }elseif($q == 181){ $letra = 'GD'; }elseif($q == 182){ $letra = 'GE'; }elseif($q == 183){ $letra = 'GF'; }elseif($q == 184){ $letra = 'GG'; }elseif($q == 185){ $letra = 'GH'; }elseif($q == 186){ $letra = 'GI'; }elseif($q == 187){ $letra = 'GJ'; }elseif($q == 188){ $letra = 'GK'; }elseif($q == 189){ $letra = 'GL'; }elseif($q == 190){ $letra = 'GM'; }elseif($q == 191){ $letra = 'GN'; }elseif($q == 192){ $letra = 'GO'; }elseif($q == 193){ $letra = 'GP'; }elseif($q == 194){ $letra = 'GQ'; }elseif($q == 195){ $letra = 'GR'; }elseif($q == 196){ $letra = 'GS'; }elseif($q == 197){ $letra = 'GT'; }elseif($q == 198){ $letra = 'GU'; }elseif($q == 199){ $letra = 'GV'; }elseif($q == 200){ $letra = 'GW'; }elseif($q == 201){ $letra = 'GX'; }elseif($q == 202){ $letra = 'GY'; }elseif($q == 203){ $letra = 'GZ'; }
					if($vecfec[$q] == $fec)
					{
						$objPHPExcel->getActiveSheet()->getStyle($letra.$j)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
						$objPHPExcel->getActiveSheet()->getStyle($letra.$j)->getFill()->getStartColor()->setARGB('00D8D8D8');//COLOR DE FONDO
						break;
					}
				}
			}
		}*/
		
		$j++;
	}
	$j++;
}
/*
for($i = 0; $i <= $diasreales; $i++)
{
	$con1 = mysqli_query($conectar,"select ADDDATE('".$fecinidias."', INTERVAL $i DAY) fec,DATE_FORMAT(ADDDATE('".$fecinidias."', INTERVAL $i DAY), '%d') dia,DATE_FORMAT(ADDDATE('".$fecinidias."', INTERVAL $i DAY), '%b') mes from usuario limit 1");
	$dato1 = mysqli_fetch_array($con1);
	$fec = $dato1['fec'];
	$dia = $dato1['dia'];
	$mes = $dato1['mes'];
	
	if($mes == 'Jan'){ $mes = 'En'; }elseif($mes == 'Feb'){ $mes = 'Fb'; }elseif($mes == 'Mar'){ $mes = 'Mr'; }elseif($mes == 'Apr'){ $mes = 'Ab'; }elseif($mes == 'May'){ $mes = 'My'; }elseif($mes == 'Jun'){ $mes = 'Jn'; }elseif($mes == 'Jul'){ $mes = 'Jl'; }elseif($mes == 'Aug'){ $mes = 'Ag'; }elseif($mes == 'Sep'){ $mes = 'St'; }elseif($mes == 'Oct'){ $mes = 'Ot'; }elseif($mes == 'Nov'){ $mes = 'Nv'; }elseif($mes == 'Dec'){ $mes = 'Dc'; }
	
	$con1 = mysqli_query($conectar,"SELECT DATE_FORMAT('".$fec."', '%W') sem from usuario limit 1");
	$dato1 = mysqli_fetch_array($con1);
	$sem = $dato1['sem'];
	
	if($sem == 'Monday'){ $sem = 'L'; }elseif($sem == 'Tuesday'){ $sem = 'M'; }elseif($sem == 'Wednesday'){ $sem = 'X'; }elseif($sem == 'Thursday'){ $sem = 'J'; }elseif($sem == 'Friday'){ $sem = 'V'; }elseif($sem == 'Saturday'){ $sem = 'S'; }elseif($sem == 'Sunday'){ $sem = 'D'; }
	
	if($i == 0){ $letra = 'E'; }elseif($i == 1){ $letra = 'F'; }elseif($i == 2){ $letra = 'G'; }elseif($i == 3){ $letra = 'H'; }elseif($i == 4){ $letra = 'I'; }elseif($i == 5){ $letra = 'J'; }elseif($i == 6){ $letra = 'K'; }elseif($i == 7){ $letra = 'L'; }elseif($i == 8){ $letra = 'M'; }elseif($i == 9){ $letra = 'N'; }elseif($i == 10){ $letra = 'O'; }elseif($i == 11){ $letra = 'P'; }elseif($i == 12){ $letra = 'Q'; }elseif($i == 13){ $letra = 'R'; }elseif($i == 14){ $letra = 'S'; }elseif($i == 15){ $letra = 'T'; }elseif($i == 16){ $letra = 'U'; }elseif($i == 17){ $letra = 'V'; }elseif($i == 18){ $letra = 'W'; }elseif($i == 19){ $letra = 'X'; }elseif($i == 20){ $letra = 'Y'; }elseif($i == 21){ $letra = 'Z'; }elseif($i == 22){ $letra = 'AA'; }elseif($i == 23){ $letra = 'AB'; }elseif($i == 24){ $letra = 'AC'; }elseif($i == 25){ $letra = 'AD'; }elseif($i == 26){ $letra = 'AE'; }elseif($i == 27){ $letra = 'AF'; }elseif($i == 28){ $letra = 'AG'; }elseif($i == 29){ $letra = 'AH'; }elseif($i == 30){ $letra = 'AI'; }elseif($i == 31){ $letra = 'AJ'; }elseif($i == 32){ $letra = 'AK'; }elseif($i == 33){ $letra = 'AL'; }elseif($i == 34){ $letra = 'AM'; }elseif($i == 35){ $letra = 'AN'; }elseif($i == 36){ $letra = 'AO'; }elseif($i == 37){ $letra = 'AP'; }elseif($i == 38){ $letra = 'AQ'; }elseif($i == 39){ $letra = 'AR'; }elseif($i == 40){ $letra = 'AS'; }elseif($i == 41){ $letra = 'AT'; }elseif($i == 42){ $letra = 'AU'; }elseif($i == 43){ $letra = 'AV'; }elseif($i == 44){ $letra = 'AW'; }elseif($i == 45){ $letra = 'AX'; }elseif($i == 46){ $letra = 'AY'; }elseif($i == 47){ $letra = 'AZ'; }elseif($i == 48){ $letra = 'BA'; }elseif($i == 49){ $letra = 'BB'; }elseif($i == 50){ $letra = 'BC'; }elseif($i == 51){ $letra = 'BD'; }elseif($i == 52){ $letra = 'BE'; }elseif($i == 53){ $letra = 'BF'; }elseif($i == 54){ $letra = 'BG'; }elseif($i == 55){ $letra = 'BH'; }elseif($i == 56){ $letra = 'BI'; }elseif($i == 57){ $letra = 'BJ'; }elseif($i == 58){ $letra = 'BK'; }elseif($i == 59){ $letra = 'BL'; }elseif($i == 60){ $letra = 'BM'; }elseif($i == 61){ $letra = 'BN'; }elseif($i == 62){ $letra = 'BO'; }elseif($i == 63){ $letra = 'BP'; }elseif($i == 64){ $letra = 'BQ'; }elseif($i == 65){ $letra = 'BR'; }elseif($i == 66){ $letra = 'BS'; }elseif($i == 67){ $letra = 'BT'; }elseif($i == 68){ $letra = 'BU'; }elseif($i == 69){ $letra = 'BV'; }elseif($i == 70){ $letra = 'BW'; }elseif($i == 71){ $letra = 'BX'; }elseif($i == 72){ $letra = 'BY'; }elseif($i == 73){ $letra = 'BZ'; }elseif($i == 74){ $letra = 'CA'; }elseif($i == 75){ $letra = 'CB'; }elseif($i == 76){ $letra = 'CC'; }elseif($i == 77){ $letra = 'CD'; }elseif($i == 78){ $letra = 'CE'; }elseif($i == 79){ $letra = 'CF'; }elseif($i == 80){ $letra = 'CG'; }elseif($i == 81){ $letra = 'CH'; }elseif($i == 82){ $letra = 'CI'; }elseif($i == 83){ $letra = 'CJ'; }elseif($i == 84){ $letra = 'CK'; }elseif($i == 85){ $letra = 'CL'; }elseif($i == 86){ $letra = 'CM'; }elseif($i == 87){ $letra = 'CN'; }elseif($i == 88){ $letra = 'CO'; }elseif($i == 89){ $letra = 'CP'; }elseif($i == 90){ $letra = 'CQ'; }elseif($i == 91){ $letra = 'CR'; }elseif($i == 92){ $letra = 'CS'; }elseif($i == 93){ $letra = 'CT'; }elseif($i == 94){ $letra = 'CU'; }elseif($i == 95){ $letra = 'CV'; }elseif($i == 96){ $letra = 'CW'; }elseif($i == 97){ $letra = 'CX'; }elseif($i == 98){ $letra = 'CY'; }elseif($i == 99){ $letra = 'CZ'; }elseif($i == 100){ $letra = 'DA'; }elseif($i == 101){ $letra = 'DB'; }elseif($i == 102){ $letra = 'DC'; }elseif($i == 103){ $letra = 'DD'; }elseif($i == 104){ $letra = 'DE'; }elseif($i == 105){ $letra = 'DF'; }elseif($i == 106){ $letra = 'DG'; }elseif($i == 107){ $letra = 'DH'; }elseif($i == 108){ $letra = 'DI'; }elseif($i == 109){ $letra = 'DJ'; }elseif($i == 110){ $letra = 'DK'; }elseif($i == 111){ $letra = 'DL'; }elseif($i == 112){ $letra = 'DM'; }elseif($i == 113){ $letra = 'DN'; }elseif($i == 114){ $letra = 'DO'; }elseif($i == 115){ $letra = 'DP'; }elseif($i == 116){ $letra = 'DQ'; }elseif($i == 117){ $letra = 'DR'; }elseif($i == 118){ $letra = 'DS'; }elseif($i == 119){ $letra = 'DT'; }elseif($i == 120){ $letra = 'DU'; }elseif($i == 121){ $letra = 'DV'; }elseif($i == 122){ $letra = 'DW'; }elseif($i == 123){ $letra = 'DX'; }elseif($i == 124){ $letra = 'DY'; }elseif($i == 125){ $letra = 'DZ'; }elseif($i == 126){ $letra = 'EA'; }elseif($i == 127){ $letra = 'EB'; }elseif($i == 128){ $letra = 'EC'; }elseif($i == 129){ $letra = 'ED'; }elseif($i == 130){ $letra = 'EE'; }elseif($i == 131){ $letra = 'EF'; }elseif($i == 132){ $letra = 'EG'; }elseif($i == 133){ $letra = 'EH'; }elseif($i == 134){ $letra = 'EI'; }elseif($i == 135){ $letra = 'EJ'; }elseif($i == 136){ $letra = 'EK'; }elseif($i == 137){ $letra = 'EL'; }elseif($i == 138){ $letra = 'EM'; }elseif($i == 139){ $letra = 'EN'; }elseif($i == 140){ $letra = 'EO'; }elseif($i == 141){ $letra = 'EP'; }elseif($i == 142){ $letra = 'EQ'; }elseif($i == 143){ $letra = 'ER'; }elseif($i == 144){ $letra = 'ES'; }elseif($i == 145){ $letra = 'ET'; }elseif($i == 146){ $letra = 'EU'; }elseif($i == 147){ $letra = 'EV'; }elseif($i == 148){ $letra = 'EW'; }elseif($i == 149){ $letra = 'EX'; }elseif($i == 150){ $letra = 'EY'; }elseif($i == 151){ $letra = 'EZ'; }elseif($i == 152){ $letra = 'FA'; }elseif($i == 153){ $letra = 'FB'; }elseif($i == 154){ $letra = 'FC'; }elseif($i == 155){ $letra = 'FD'; }elseif($i == 156){ $letra = 'FE'; }elseif($i == 157){ $letra = 'FF'; }elseif($i == 158){ $letra = 'FG'; }elseif($i == 159){ $letra = 'FH'; }elseif($i == 160){ $letra = 'FI'; }elseif($i == 161){ $letra = 'FJ'; }elseif($i == 162){ $letra = 'FK'; }elseif($i == 163){ $letra = 'FL'; }elseif($i == 164){ $letra = 'FM'; }elseif($i == 165){ $letra = 'FN'; }elseif($i == 166){ $letra = 'FO'; }elseif($i == 167){ $letra = 'FP'; }elseif($i == 168){ $letra = 'FQ'; }elseif($i == 169){ $letra = 'FR'; }elseif($i == 170){ $letra = 'FS'; }elseif($i == 171){ $letra = 'FT'; }elseif($i == 172){ $letra = 'FU'; }elseif($i == 173){ $letra = 'FV'; }elseif($i == 174){ $letra = 'FW'; }elseif($i == 175){ $letra = 'FX'; }elseif($i == 176){ $letra = 'FY'; }elseif($i == 177){ $letra = 'FZ'; }elseif($i == 178){ $letra = 'GA'; }elseif($i == 179){ $letra = 'GB'; }elseif($i == 180){ $letra = 'GC'; }elseif($i == 181){ $letra = 'GD'; }elseif($i == 182){ $letra = 'GE'; }elseif($i == 183){ $letra = 'GF'; }elseif($i == 184){ $letra = 'GG'; }elseif($i == 185){ $letra = 'GH'; }elseif($i == 186){ $letra = 'GI'; }elseif($i == 187){ $letra = 'GJ'; }elseif($i == 188){ $letra = 'GK'; }elseif($i == 189){ $letra = 'GL'; }elseif($i == 190){ $letra = 'GM'; }elseif($i == 191){ $letra = 'GN'; }elseif($i == 192){ $letra = 'GO'; }elseif($i == 193){ $letra = 'GP'; }elseif($i == 194){ $letra = 'GQ'; }elseif($i == 195){ $letra = 'GR'; }elseif($i == 196){ $letra = 'GS'; }elseif($i == 197){ $letra = 'GT'; }elseif($i == 198){ $letra = 'GU'; }elseif($i == 199){ $letra = 'GV'; }elseif($i == 200){ $letra = 'GW'; }elseif($i == 201){ $letra = 'GX'; }elseif($i == 202){ $letra = 'GY'; }elseif($i == 203){ $letra = 'GZ'; }
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle($letra.'4')-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.'4', $sem."".$dia."".$mes);
	$objPHPExcel->getActiveSheet()->getStyle($letra.'4')->getAlignment()->setTextRotation(90);
	//****************************
}
*/
//DATOS DE SALIDA DEL EXCEL
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$archivo.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
?>