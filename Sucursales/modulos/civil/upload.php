<?php
session_start();
ini_set('max_execution_time', 600);
ini_set('upload_max_filesize', '300M');
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
include('../../data/Conexion.php');
require_once('../../Classes/PHPMailer-master/class.phpmailer.php');
header("Cache-Control: no-store, no-cache, must-revalidate");

error_reporting(0);
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_SESSION['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];

//date_default_timezone_set('America/Bogota');
$fecha=date("Ymd");
$fechaactual=date('Y/m/d H:i:s');
$clasuc = $_POST['clasuc'];
$actor = $_POST['actor'];
$arc = $_POST['archivo'];
$claada = $_POST['claada'];

if($clasuc != '' and $clasuc != 0 and $actor != '')
{
	if(is_array($_FILES)) 
	{
		mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'11',5,'".$clasuc."','".$usuario."','".$fechaact."')");//Tercer campo tia_clave_int. 5=Adjuntar archivo

		$con = mysqli_query($conectar,"select MAX(ada_clave_int) max from adjunto_actor where suc_clave_int = '".$clasuc."'");
		$dato = mysqli_fetch_array($con);
		$num = $dato['max'];
		
		if($num == 0 || $num == ''){ $num = 1; }else{ $num = $num+1; }
				
		if($actor == 'INFORMEFINAL')
		{
			if(is_uploaded_file($_FILES['adjuntoinf']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntoinf']['tmp_name'];
				$archivo = basename($_FILES['adjuntoinf']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
                $nombrearchivo = $array_nombre[0];
                $anio = substr($nombrearchivo,0,4);
                $mes = substr($nombrearchivo,4,2);
                $dia = substr($nombrearchivo,6,2);
                $fech = $anio."-".$mes."-".$dia;
                $prefijo = $nombrearchivo;

				//$prefijo = $anio.$mes.$dia." CIVIL - INFORME";
				$destino =  "../../adjuntos/civil/".$clasuc."-".$prefijo."".$num.".".$extension;
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '37' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "errorv";
                }
                else
                if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                {
                    echo "error1";
                }
                else
                {
                    if (move_uploaded_file($sourcePath, $destino)) 
                    {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,suc_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clasuc . "','" . $fechaactual . "','" . $destino . "','" . $archivo . "',37,'" . $usuario . "','" . $fechaactual . "')");
                         $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario u inner join notificar_usuario nu on (nu.usu_clave_int = u.usu_clave_int) where not_clave_int = 64 and usu_email != ''");
                            $num = mysqli_num_rows($conema);
                            for ($i = 0; $i < $num; $i++) 
                            {
                                $mail = new PHPMailer();

                                $datoema = mysqli_fetch_array($conema);
                                $mail->Body = '';
                                $ema = '';
                                $nom = $datoema['usu_nombre'];
                                $ema = $datoema['usu_email'];

                                $concaj = mysqli_query($conectar,"select suc_nombre from sucursal where suc_clave_int = '" . $clasuc . "'");
                                $datocaj = mysqli_fetch_array($concaj);
                                $nomsuc = $datocaj['suc_nombre'];

                                $conact = mysqli_query($conectar,"select usu_nombre from usuario where usu_usuario = '" . $usuario . "'");
                                $datoact = mysqli_fetch_array($conact);
                                $nomact = $datoact['usu_nombre'];

                                $mail->From = "adminpavas@pavas.com.co";
                                $mail->FromName = "Soportica";
                                $mail->AddAddress($ema, "Destino");
                                $mail->Subject = "Adjunto Informe Final - Sucursal Nro. " . $clasuc;

                                // Cuerpo del mensaje
                                $mail->Body .= "Hola " . $nom . "!\n\n";
                                $mail->Body .= "SUCURSALES Le informa que han adjuntado informe final.\n";
                                $mail->Body .= "SUCURSAL: " . $nomsuc . "\n";
                                $mail->Body .= "USUARIO QUE ADJUNTA: " . $nomact . "\n";
                                $mail->Body .= date("d/m/Y H:m:s") . "\n\n";
                                $mail->Body .= "Este mensaje es generado automáticamente por SUCURSALES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Sucursales \n";

                                if (!$mail->Send()) {
                                    //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                                    //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                                } else {
                                }
                            }
                        echo "ok";
                    } else {
                        echo "error4";
                    }
                }

			}
			else{
			    echo "error3";
            }
		}
		else
		if($actor == 'ACTAINICIO')
		{
			if(is_uploaded_file($_FILES['adjuntoactainicio']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntoactainicio']['tmp_name'];
				$archivo = basename($_FILES['adjuntoactainicio']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
                $nombrearchivo = $array_nombre[0];
                $anio = substr($nombrearchivo,0,4);
                $mes = substr($nombrearchivo,4,2);
                $dia = substr($nombrearchivo,6,2);
                $fech = $anio."-".$mes."-".$dia;
				$prefijo = $anio.$mes.$dia." CIVIL - ACTA DE INICIO REAL";
				$destino =  "../../adjuntos/civil/".$clasuc."-".$prefijo."".$num.".".$extension;
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '59' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "errorv";
                }
                else
                if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                {
                    echo "error1";
                }
                else
                {
                    if (move_uploaded_file($sourcePath, $destino)) {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,suc_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clasuc . "','" . $fechaactual . "','" . $destino . "','" . $archivo . "',59,'" . $usuario . "','" . $fechaactual . "')");

                        $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario u inner join notificar_usuario nu on (nu.usu_clave_int = u.usu_clave_int) where not_clave_int = 52 and usu_email != ''");
                            $num = mysqli_num_rows($conema);
                            for ($i = 0; $i < $num; $i++) 
                            {
                                $mail = new PHPMailer();

                                $datoema = mysqli_fetch_array($conema);
                                $mail->Body = '';
                                $ema = '';
                                $nom = $datoema['usu_nombre'];
                                $ema = $datoema['usu_email'];

                                $concaj = mysqli_query($conectar,"select suc_nombre from sucursal where suc_clave_int = '" . $clasuc . "'");
                                $datocaj = mysqli_fetch_array($concaj);
                                $nomsuc = $datocaj['suc_nombre'];

                                $conact = mysqli_query($conectar,"select usu_nombre from usuario where usu_usuario = '" . $usuario . "'");
                                $datoact = mysqli_fetch_array($conact);
                                $nomact = $datoact['usu_nombre'];

                                $mail->From = "adminpavas@pavas.com.co";
                                $mail->FromName = "Soportica";
                                $mail->AddAddress($ema, "Destino");
                                $mail->Subject = "Adjunto Acta Inicio Real - Sucursal Nro. " . $clasuc;

                                // Cuerpo del mensaje
                                $mail->Body .= "Hola " . $nom . "!\n\n";
                                $mail->Body .= "SUCURSALES Le informa que ha adjuntado acta inicio real.\n";
                                $mail->Body .= "SUCURSAL: " . $nomsuc . "\n";
                                $mail->Body .= "USUARIO QUE ADJUNTA: " . $nomact . "\n";
                                $mail->Body .= date("d/m/Y H:m:s") . "\n\n";
                                $mail->Body .= "Este mensaje es generado automáticamente por SUCURSALES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Sucursales \n";

                                if (!$mail->Send()) {
                                    //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                                    //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                                } else {
                                }
                            }
                        echo "ok";
                    } else {
                        echo "error4";
                    }
                }
			}
			else{
			    echo "error3";
            }
		}
		else
            if($actor == 'ENTREGASODEXO')
            {
                if(is_uploaded_file($_FILES['adjuntoentregasodexo']['tmp_name']))
                {
                    $sourcePath = $_FILES['adjuntoentregasodexo']['tmp_name'];
                    $archivo = basename($_FILES['adjuntoentregasodexo']['name']);

                    $array_nombre = explode('.',$archivo);
                    $cuenta_arr_nombre = count($array_nombre);
                    $extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
                    /*if($extension == 'jpg')
                    {
                        $extension = 'png';
                    }*/
                    $nombrearchivo = $array_nombre[0];
                    $anio = substr($nombrearchivo,0,4);
                    $mes = substr($nombrearchivo,4,2);
                    $dia = substr($nombrearchivo,6,2);
                    $fech = $anio."-".$mes."-".$dia;
                    $prefijo = $anio.$mes.$dia." CIVIL - ENTREGA SODEXO";
                    $destino =  "../../adjuntos/civil/".$clasuc."-".$prefijo."".$num.".".$extension;
                    $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '53' and ada_sw_eliminado = 0");
                    $numv = mysqli_num_rows($veri);
                    if($numv>0){
                        echo "errorv";
                    }
                    else
                        if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                        {
                            echo "error1";
                        }
                        else
                        {
                        if (move_uploaded_file($sourcePath, $destino)) {
                            $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,suc_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clasuc . "','" . $fechaactual . "','" . $destino . "','" . $archivo . "',53,'" . $usuario . "','" . $fechaactual . "')");

                            $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario u inner join notificar_usuario nu on (nu.usu_clave_int = u.usu_clave_int) where not_clave_int = 12 and usu_email != ''");
                            $num = mysqli_num_rows($conema);
                            for ($i = 0; $i < $num; $i++) {
                                $mail = new PHPMailer();

                                $datoema = mysqli_fetch_array($conema);
                                $mail->Body = '';
                                $ema = '';
                                $nom = $datoema['usu_nombre'];
                                $ema = $datoema['usu_email'];

                                $concaj = mysqli_query($conectar,"select suc_nombre from sucursal where suc_clave_int = '" . $clasuc . "'");
                                $datocaj = mysqli_fetch_array($concaj);
                                $nomsuc = $datocaj['suc_nombre'];

                                $conact = mysqli_query($conectar,"select usu_nombre from usuario where usu_usuario = '" . $usuario . "'");
                                $datoact = mysqli_fetch_array($conact);
                                $nomact = $datoact['usu_nombre'];

                                $mail->From = "adminpavas@pavas.com.co";
                                $mail->FromName = "Soportica";
                                $mail->AddAddress($ema, "Destino");
                                $mail->Subject = "Adjunto Entrega a Sodexo - Sucursal Nro. " . $clasuc;

                                // Cuerpo del mensaje
                                $mail->Body .= "Hola " . $nom . "!\n\n";
                                $mail->Body .= "SUCURSALES Le informa que han adjuntado la entrega a sodexo.\n";
                                $mail->Body .= "SUCURSAL: " . $nomsuc . "\n";
                                $mail->Body .= "USUARIO QUE ADJUNTA: " . $nomact . "\n";
                                $mail->Body .= date("d/m/Y H:m:s") . "\n\n";
                                $mail->Body .= "Este mensaje es generado automáticamente por SUCURSALES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Sucursales \n";

                                if (!$mail->Send()) {
                                    //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                                    //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                                } else {
                                }
                            }
                            echo "ok";
                    } else {
                            echo "error4";
                        }
                    }
                }
                else{
                    echo "error3";
                }
            }
            else
		if($actor == 'ACTAENTREGA')
		{
			if(is_uploaded_file($_FILES['adjuntoactaentrega']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntoactaentrega']['tmp_name'];
				$archivo = basename($_FILES['adjuntoactaentrega']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
                $nombrearchivo = $array_nombre[0];
                $anio = substr($nombrearchivo,0,4);
                $mes = substr($nombrearchivo,4,2);
                $dia = substr($nombrearchivo,6,2);
                $fech = $anio."-".$mes."-".$dia;
				$prefijo = $anio.$mes.$dia." CIVIL - ACTA DE ENTREGA REAL";
				$destino =  "../../adjuntos/civil/".$clasuc."-".$prefijo."".$num.".".$extension;
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '60' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "errorv";
                }
                else
                    if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                    {
                        echo "error1";
                    }
                    else
                    {
                    if (move_uploaded_file($sourcePath, $destino)) {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,suc_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clasuc . "','" . $fechaactual . "','" . $destino . "','" . $archivo . "',60,'" . $usuario . "','" . $fechaactual . "')");

                         $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario u inner join notificar_usuario nu on (nu.usu_clave_int = u.usu_clave_int) where not_clave_int = 53 and usu_email != ''");
                            $num = mysqli_num_rows($conema);
                            for ($i = 0; $i < $num; $i++) 
                            {
                                $mail = new PHPMailer();

                                $datoema = mysqli_fetch_array($conema);
                                $mail->Body = '';
                                $ema = '';
                                $nom = $datoema['usu_nombre'];
                                $ema = $datoema['usu_email'];

                                $concaj = mysqli_query($conectar,"select suc_nombre from sucursal where suc_clave_int = '" . $clasuc . "'");
                                $datocaj = mysqli_fetch_array($concaj);
                                $nomsuc = $datocaj['suc_nombre'];

                                $conact = mysqli_query($conectar,"select usu_nombre from usuario where usu_usuario = '" . $usuario . "'");
                                $datoact = mysqli_fetch_array($conact);
                                $nomact = $datoact['usu_nombre'];

                                $mail->From = "adminpavas@pavas.com.co";
                                $mail->FromName = "Soportica";
                                $mail->AddAddress($ema, "Destino");
                                $mail->Subject = "Adjunto Acta Entrega Real - Sucursal Nro. " . $clasuc;

                                // Cuerpo del mensaje
                                $mail->Body .= "Hola " . $nom . "!\n\n";
                                $mail->Body .= "SUCURSALES Le informa que ha adjuntado acta entrega real.\n";
                                $mail->Body .= "SUCURSAL: " . $nomsuc . "\n";
                                $mail->Body .= "USUARIO QUE ADJUNTA: " . $nomact . "\n";
                                $mail->Body .= date("d/m/Y H:m:s") . "\n\n";
                                $mail->Body .= "Este mensaje es generado automáticamente por SUCURSALES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Sucursales \n";

                                if (!$mail->Send()) {
                                    //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                                    //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                                } else {
                                }
                            }

                        echo "ok";
                    } else {
                        echo "error4";
                    }
                }
			}
			else{
			    echo "error3";
            }
		}
        else
        if($actor == 'INFORMEPLAQUEO')
        {
            if(is_uploaded_file($_FILES['adjuntoinfp']['tmp_name']))
            {
                $sourcePath = $_FILES['adjuntoinfp']['tmp_name'];
                $archivo = basename($_FILES['adjuntoinfp']['name']);

                $array_nombre = explode('.',$archivo);
                $cuenta_arr_nombre = count($array_nombre);
                $extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
                /*if($extension == 'jpg')
                {
                    $extension = 'png';
                }*/
                $nombrearchivo = $array_nombre[0];
                $anio = substr($nombrearchivo,0,4);
                $mes = substr($nombrearchivo,4,2);
                $dia = substr($nombrearchivo,6,2);
                $fech = $anio."-".$mes."-".$dia;
                $prefijo = $anio.$mes.$dia." SUMINISTRO - INFORME PLAQUEO";
                $destino =  "../../adjuntos/suministro/".$clasuc."-".$prefijo."".$num.".".$extension;
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '78' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "errorv";
                }
                else
                if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                {
                    echo "error1";
                }
                else
                {
                    if (move_uploaded_file($sourcePath, $destino))
                    {
                            $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,suc_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clasuc . "','" . $fechaactual . "','" . $destino . "','" . $archivo . "',78,'" . $usuario . "','" . $fechaactual . "')");

                            $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario u inner join notificar_usuario nu on (nu.usu_clave_int = u.usu_clave_int) where not_clave_int = 36 and usu_email != ''");
                            $num = mysqli_num_rows($conema);
                            for($i = 0; $i < $num; $i++)
                            {
                                $mail = new PHPMailer();

                                $datoema = mysqli_fetch_array($conema);
                                $mail->Body = '';
                                $ema = '';
                                $nom = $datoema['usu_nombre'];
                                $ema = $datoema['usu_email'];

                                $concaj = mysqli_query($conectar,"select suc_nombre from sucursal where suc_clave_int = '".$clasuc."'");
                                $datocaj = mysqli_fetch_array($concaj);
                                $nomsuc = $datocaj['suc_nombre'];

                                $conact = mysqli_query($conectar,"select usu_nombre from usuario where usu_usuario = '".$usuario."'");
                                $datoact = mysqli_fetch_array($conact);
                                $nomact = $datoact['usu_nombre'];

                                $mail->From = "adminpavas@pavas.com.co";
                                $mail->FromName = "Soportica";
                                $mail->AddAddress($ema, "Destino");
                                $mail->Subject = "Adjunto Plaqueo Activos - Sucursal Nro. ".$clasuc;

                                // Cuerpo del mensaje
                                $mail->Body .= "Hola ".$nom."!\n\n";
                                $mail->Body .= "SUCURSALES Le informa que han adjuntado un informe de plaqueo activos.\n";
                                $mail->Body .= "SUCURSAL: ".$nomsuc."\n";
                                $mail->Body .= "USUARIO QUE ADJUNTA: ".$nomact."\n";
                                $mail->Body .= date("d/m/Y H:m:s")."\n\n";
                                $mail->Body .= "Este mensaje es generado automáticamente por SUCURSALES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Sucursales \n";

                                if(!$mail->Send())
                                {
                                    //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                                    //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                                }
                                else
                                {
                                }
                            }
                            echo "ok";
                    }
                    else
                    {
                            echo "error4";
                    }
                }
            }
            else{
                echo "error3";
            }
        }
		else
		if($actor == 'ACTUALIZAARCHIVO')
		{
			if(is_uploaded_file($_FILES['adjuntoava1']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntoava1']['tmp_name'];
				$archivo = basename($_FILES['adjuntoava1']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				$con = mysqli_query($conectar,"select * from adjunto_actor where ada_clave_int = '".$claada."'");
				$dato = mysqli_fetch_array($con);
				$adj = $dato['ada_adjunto'];
                $nombrearchivo = $array_nombre[0];
                $anio = substr($nombrearchivo,0,4);
                $mes = substr($nombrearchivo,4,2);
                $dia = substr($nombrearchivo,6,2);
                $fech = $anio."-".$mes."-".$dia;
				if($arc==37) {
                    $prefijo = $anio . $mes . $dia . " CIVIL - INFORME";
                    $error="error1";

                }else if($arc==59)
                {
                    $prefijo = $anio . $mes . $dia . " CIVIL - ACTA DE INICIO REAL";
                    $error="error2";
                }
                else if($arc==53)
                {
                    $prefijo = $anio . $mes . $dia . " CIVIL - ENTREGA SODEXO";
                    $error="error5";
                }
                else if($arc==60)
                {
                    $prefijo = $anio . $mes . $dia . " CIVIL - ACTA DE ENTREGA REAL";
                    $error="error6";
                }
                else if($arc== 78)
                {
                    $prefijo = $anio.$mes.$dia." SUMINISTRO - INFORME PLAQUEO";
                    $error =  "error2";
                }
				$destino =  "../../adjuntos/civil/".$clasuc."-".$prefijo."".$claada.".".$extension;

                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '".$arc."' and ada_sw_eliminado = 0 and ada_clave_int!='".$claada."'");
                $numv = mysqli_num_rows($veri);
                if($numv>0)
                {
                    echo "errorv";
                }
                else
                if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                {
                    echo "error1";
                }
                else
                {

                    if (move_uploaded_file($sourcePath, $destino)) {
                        $sql = mysqli_query($conectar,"update adjunto_actor set ada_usu_actualiz = '" . $usuario . "',ada_fec_actualiz = '" . $fechaactual . "', ada_adjunto = '" . $destino . "', ada_nombre_adjunto = '" . $archivo . "' where ada_clave_int = '" . $claada . "'");
                    } else {
                        echo "error4";
                    }
                }
			}
			else
            {
			    echo "error3";
            }
		}



	}
}
?>