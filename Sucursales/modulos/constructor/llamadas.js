function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function MODULO(v,e)
{
    var ale =  document.getElementById('swregistro').value;
    var ire = 0;
    if (ale == 1 || ale == 2) {
        var conf = confirm("¿Quieres salir de este sitio web?\n\nEs posible que los cambios no se guarden.")
        if(conf){
            ire = 1;
        }
        else
        {
            ire = 0;
        }
    }
    else
    {
        ire = 1;
    }
    if(ire==1)
    {
        if (v == 'CONSTRUCTOR') {
            window.location.href = "constructor.php";
        }
        else if (v == 'TODOS') {
            var ajax;
            ajax = new ajaxFunction();
            ajax.onreadystatechange = function () {
                if (ajax.readyState == 4) {
                    document.getElementById('cajeros').innerHTML = ajax.responseText;
                }
            }
            jQuery("#cajeros").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
            ajax.open("GET", "?todos=si&est=" + e, true);
            ajax.send(null);
        }
        OCULTARSCROLL();
    }
}
function BUSCAR(p)
{	
	var consec = form1.busconsecutivo.value;
	var nom = form1.busnombre.value;
	var anocon = form1.busanocontable.value;
	var cod = form1.buscodigo.value;
	var reg = form1.busregion.value;
	var mun = form1.busmunicipio.value;
	var tipint = form1.bustipointervencion.value;
	var moda = form1.busmodalidad.value;
	var actor = form1.busactor.value;
	var est = form1.ocultoestado.value;
	var pro = form1.busproyecto.value;
	var estpro = form1.estadosucursal.value;
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('busqueda').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#busqueda").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	if(p > 0)
	{
		ajax.open("GET","?buscar=si&consec="+consec+"&nom="+nom+"&anocon="+anocon+"&cod="+cod+"&reg="+reg+"&mun="+mun+"&tipint="+tipint+"&moda="+moda+"&est="+est+"&actor="+actor+"&pro="+pro+"&estpro="+estpro+"&page="+p,true);
	}
	else
	{
		ajax.open("GET","?buscar=si&consec="+consec+"&nom="+nom+"&anocon="+anocon+"&cod="+cod+"&reg="+reg+"&mun="+mun+"&tipint="+tipint+"&moda="+moda+"&est="+est+"&actor="+actor+"&pro="+pro+"&estpro="+estpro,true);
	}		
	ajax.send(null);
	OCULTARSCROLL();
}
function VERREGISTRO(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('cajeros').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#cajeros").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?verregistro=si&clasuc="+v,true);
	ajax.send(null);
	OCULTARSCROLL();
}
function AGREGARNOTA(c)
{
	var not = $('#nota').val();
	var nuevanota = CORREGIRTEXTO(not);
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('agregados').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#agregados").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?agregarnota=si&not="+nuevanota+"&suc="+c,true);
	ajax.send(null);
}
function CORREGIRTEXTO(v)
{
	var res = v.replace('#','REEMPLAZARNUMERAL');
	var res = res.replace('+','REEMPLAZARMAS');
	
	return res;
}
function EDITARNOTA(c,cn)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('editarnota').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#editarnota").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?editarnota=si&clanot="+cn+"&suc="+c,true);
	ajax.send(null);
}
function ACTUALIZARNOTA(n,c)
{
	var not = $('#notaedi').val();
	var nuevanota = CORREGIRTEXTO(not);
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todaslasnotas').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todaslasnotas").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?actualizarnota=si&not="+nuevanota+"&clanot="+n+"&suc="+c,true);
	ajax.send(null);
}
function ELIMINARNOTA(c,suc)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todaslasnotas').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todaslasnotas").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?eliminarnota=si&clanot="+c+"&suc="+suc,true);
	ajax.send(null);
}
function MOSTRARNOTAS(c)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todaslasnotas').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todaslasnotas").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarnotas=si&clasuc="+c,true);
	ajax.send(null);
}
function AVISO()
{
	alert("Porfavor seleccione un cajero para agregar notas");
}
function GUARDAR(v)
{
	var fit = $('#fit').val();
	var fft = $('#fft').val();
	var aireacon = $('#aireacondicionado').val();
	var contraseg = $('#contratistaseguridad').val();
	var contraele = $('#contratistaelectrico').val();
	var subcontraaire = $('#subcontraaire').val();
	var subcontraele = $('#subcontraele').val();
	var planosrec = $('#planosrecord').val();
	var fir = $('#fir').val();
	var ffr = $('#ffr').val();
	var feccot = $('#feccot').val();
	var fecliq = $('#fecliq').val();
	var avance = $('#avance').val();
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('estadoregistro').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#estadoregistro").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?guardardatos=si&aireacon="+aireacon+"&contraseg="+contraseg+"&contraele="+contraele+"&planosrec="+planosrec+"&fir="+fir+"&ffr="+ffr+"&fit="+fit+"&fft="+fft+"&feccot="+feccot+"&fecliq="+fecliq+"&avance="+avance+"&suc="+v+"&subcontraele="+subcontraele+"&subcontraaire="+subcontraaire,true);
	ajax.send(null);
}
function RESULTADOADJUNTO(v,cs)
{
	if(v == 'POLIZAS')
	{
		var div = 'resultadoadjuntopolizas';
	}
	else
	if(v == 'ACTUALIZACIONPRESUPUESTAL1')
	{
		var div = 'resultadoadjuntoactualizacionpresupuestal1';
	}
	else
	if(v == 'CONTROLCAMBIOS')
	{
		var div = 'resultadoadjuntocontrolcambios';
	}
	else
	if(v == 'SEGURIDADSOCIAL')
	{
		var div = 'resultadoadjuntoseguridadsocial';
	}
	else
	if(v == 'MANEJOAMBIENTAL')
	{
		var div = 'resultadoadjuntomanejoambiental';
	}
	else
	if(v == 'PLANCALIDAD')
	{
		var div = 'resultadoadjuntoplancalidad';
	}
	else
	if(v == 'ACTUALIZACIONPRESUPUESTAL2')
	{
		var div = 'resultadoadjuntoactualizacionpresupuestal2';
	}
	else
	if(v == 'LIQUIDACION')
	{
		var div = 'resultadoadjuntoliquidacion';
	}
	else
	if(v == 'FICHATECNICA')
	{
		var div = 'resultadoadjuntofichatecnica';
	}
	else
	if(v == 'REGISTROFOTOGRAFICO')
	{
		var div = 'resultadoadjuntoregistrofotografico';
	}
	else
	if(v == 'CERTIFICADOESCOMBROS')
	{
		var div = 'resultadoadjuntocertificadoescombros';
	}
	else
	if(v == 'CERTIFICADORETIE')
	{
		var div = 'resultadoadjuntocertificadoretie';
	}
	else
	if(v == 'PRUEBAARRANQUE')
	{
		var div = 'resultadoadjuntopruebaarranque';
	}
	else
	if(v == 'EVALUACIONFINAL')
	{
		var div = 'resultadoadjuntoevaluacionfinal';
	}

	else
	if(v == 'EQUIPOSTECNICOS')
	{
		var div = 'resultadoadjuntoequipostecnicos';
	}
	else
	if(v == 'LISTACHEQUEO')
	{
		var div = 'resultadoadjuntolistachequeo';
	}
	else
	if(v == 'INFORMEFINAL')
	{
		var div = 'resultadoadjuntoinf';
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById(div).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#"+div).html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?estadoadjunto=si&actor="+v+"&clasuc="+cs,true);
	ajax.send(null);
	OCULTARSCROLL();
}
function MOSTRARRUTA(v)
{
	if(v == 'COTIZACION')
	{
		var div = 'resultadoadjuntocot';
		var nomadj = form1.adjuntocot.value;
	}
	else
	if(v == 'INFORMEFINAL')
	{
		var div = 'resultadoadjuntoinf';
		var nomadj = form1.adjuntoinf.value;
	}
	else
	if(v == 'FACTURACION')
	{
		var div = 'resultadoadjuntofac';
		var nomadj = form1.adjuntofac.value;
	}
	else
	if(v == 'AVANCE')
	{
		var div = 'resultadoadjuntoava';
		var nomadj = form1.adjuntoava.value;
	}
	else
	if(v == 'ACTUALIZAARCHIVO')
	{
		var div = 'resultadoadjuntoava1';
		var nomadj = $('#adjuntoava1').val();
	}
	else
	if(v == 'POLIZAS')
	{
		var div = 'resultadoadjuntopolizas';
		var nomadj = form1.adjuntopolizas.value;//$('#adjuntopolizas').val();
	}
	else
	if(v == 'ACTUALIZACIONPRESUPUESTAL1')
	{
		var div = 'resultadoadjuntoactualizacionpresupuestal1';
		var nomadj = form1.adjuntoactualizacionpresupuestal1.value;//$('#adjuntoactualizacionpresupuestal1').val();
	}
	else
	if(v == 'CONTROLCAMBIOS')
	{
		var div = 'resultadoadjuntocontrolcambios';
		var nomadj = form1.adjuntocontrolcambios.value;//$('#adjuntocontrolcambios').val();
	}
	else
	if(v == 'SEGURIDADSOCIAL')
	{
		var div = 'resultadoadjuntoseguridadsocial';
		var nomadj = form1.adjuntoseguridadsocial.value;//$('#adjuntoseguridadsocial').val();
	}
	else
	if(v == 'MANEJOAMBIENTAL')
	{
		var div = 'resultadoadjuntomanejoambiental';
		var nomadj = form1.adjuntomanejoambiental.value;//$('#adjuntomanejoambiental').val();
	}
	else
	if(v == 'PLANCALIDAD')
	{
		var div = 'resultadoadjuntoplancalidad';
		var nomadj = form1.adjuntoplancalidad.value;//$('#adjuntoplancalidad').val();
	}
	else
	if(v == 'ACTUALIZACIONPRESUPUESTAL2')
	{
		var div = 'resultadoadjuntoactualizacionpresupuestal2';
		var nomadj = form1.adjuntoactualizacionpresupuestal2.value;//$('#adjuntoactualizacionpresupuestal2').val();
	}
	else
	if(v == 'LIQUIDACION')
	{
		var div = 'resultadoadjuntoliquidacion';
		var nomadj = form1.adjuntoliquidacion.value;//$('#adjuntoliquidacion').val();
	}
	else
	if(v == 'FICHATECNICA')
	{
		var div = 'resultadoadjuntofichatecnica';
		var nomadj = form1.adjuntofichatecnica.value;//$('#adjuntofichatecnica').val();
	}
	else
	if(v == 'REGISTROFOTOGRAFICO')
	{
		var div = 'resultadoadjuntoregistrofotografico';
		var nomadj = form1.adjuntoregistrofotografico.value;//$('#adjuntoregistrofotografico').val();
	}
	else
	if(v == 'CERTIFICADOESCOMBROS')
	{
		var div = 'resultadoadjuntocertificadoescombros';
		var nomadj = form1.adjuntocertificadoescombros.value;//$('#adjuntocertificadoescombros').val();
	}
	else
	if(v == 'CERTIFICADORETIE')
	{
		var div = 'resultadoadjuntocertificadoretie';
		var nomadj = form1.adjuntocertificadoretie.value;//$('#adjuntocertificadoretie').val();
	}
	else
	if(v == 'PRUEBAARRANQUE')
	{
		var div = 'resultadoadjuntopruebaarranque';
		var nomadj = form1.adjuntopruebaarranque.value;//$('#adjuntopruebaarranque').val();
	}
	else
	if(v == 'EVALUACIONFINAL')
	{
		var div = 'resultadoadjuntoevaluacionfinal';
		var nomadj = form1.adjuntoevaluacionfinal.value;//$('#adjuntoevaluacionfinal').val();
	}
	/*else
	if(v == 'ENTREGASODEXO')
	{
		var div = 'resultadoadjuntoentregasodexo';
		var nomadj = form1.adjuntoentregasodexo.value;//$('#adjuntoentregasodexo').val();
	}
	else*/
	if(v == 'EQUIPOSTECNICOS')
	{
		var div = 'resultadoadjuntoequipostecnicos';
		var nomadj = form1.adjuntoequipostecnicos.value;//$('#adjuntoequipostecnicos').val();
	}
	else
	if(v == 'LISTACHEQUEO')
	{
		var div = 'resultadoadjuntolistachequeo';
		var nomadj = form1.adjuntolistachequeo.value;//$('#adjuntolistachequeo').val();
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById(div).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#"+div).html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarruta=si&nomadj="+nomadj,true);
	ajax.send(null);
	OCULTARSCROLL();
}
function RESULTADOADJUNTOAVANCE(v,cc,ca)
{
	if(v == 'ACTUALIZAARCHIVO')
	{
		var div = 'resultadoadjuntoava1';
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById(div).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#"+div).html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?estadoadjuntoavance=si&actor="+v+"&clacaj="+cc+"&claada="+ca,true);
	ajax.send(null);
	OCULTARSCROLL();
}
function MOSTRARADJUNTOS(c,a)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todoslosadjuntos').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todoslosadjuntos").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostraradjuntos=si&clasuc="+c+"&actor="+a,true);
	ajax.send(null);
}
function EDITAR(c,a)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('editaradjunto').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#editaradjunto").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?editaradjuntoconstructor=si&claada="+c+"&act="+a,true);
	ajax.send(null);
}
function CALCULARTOTALDIAS()
{
	var fi = $('#fir').val();
	var ff = $('#ffr').val();
	var d = restaFechas(fi,ff);
	if(parseInt(d) >= 0){ $('#diasr').val(d); }
}
function CALCULARTOTALDIASTEORICO()
{
	var fi = $('#fit').val();
	var ff = $('#fft').val();
	var d = restaFechas(fi,ff);
	if(parseInt(d) >= 0){ $('#diast').val(d); }
}
function restaFechas(f1,f2)
{
	var aFecha1 = f1.split('-'); 
	var aFecha2 = f2.split('-'); 
	var fFecha1 = Date.UTC(aFecha1[0],aFecha1[1]-1,aFecha1[2]); 
	var fFecha2 = Date.UTC(aFecha2[0],aFecha2[1]-1,aFecha2[2]); 
	var dif = fFecha2 - fFecha1;
	var dias = Math.floor(dif / (1000 * 60 * 60 * 24)); 
	return dias;
}
function MOSTRARBITACORA(c)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todaslasnotas').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todaslasnotas").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarbitacora=si&clasuc="+c,true);
	ajax.send(null);
}
function GUARDARCALIFICACION(n)
{
    var pos = $('#pos'+n).val();
    var ajax;
    ajax=new ajaxFunction();
    ajax.onreadystatechange=function()
    {
        if(ajax.readyState==4)
        {
            document.getElementById('msnnota').innerHTML=ajax.responseText;
        }
    }
    jQuery("#msnnota").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
    ajax.open("GET","?guardarcalificacion=si&clanot="+n+"&cal=" + pos,true);
    ajax.send(null);

}
function MOSTRARBITACORAADJUNTOS(c)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todoslosadjuntosbitacora').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todoslosadjuntosbitacora").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarbitacoraadjuntos=si&clasuc="+c,true);
	ajax.send(null);
}
function ELIMINARADJUNTO(v)
{
	if(confirm('Esta seguro/a de Eliminar este archivo?'))
	{
		var dataString = 'id='+v;
		
		$.ajax({
	        type: "POST",
	        url: "deletearchivo.php",
	        data: dataString,
	        success: function() {
				//$('#delete-ok').empty();
				//$('#delete-ok').append('<div class="correcto">Se ha eliminado correctamente la entrada con id='+v+'.</div>').fadeIn("slow");
				$('#service'+v).fadeOut("slow");
				//$('#'+v).remove();
	        }
	    });
	}
}
function VERMODALIDADES1(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('modalidades1').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#modalidades1").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?vermodalidades1=si&tii="+v,true);
	ajax.send(null);
	//setTimeout("REFRESCARLISTAMODALIDADES1()",800);
}