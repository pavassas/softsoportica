<?php
ini_set('max_execution_time', 600);
ini_set('upload_max_filesize', '300M');
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
include('../../data/Conexion.php');
require_once('../../Classes/PHPMailer-master/class.phpmailer.php');
header("Cache-Control: no-store, no-cache, must-revalidate");
session_start();
error_reporting(0);
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_SESSION['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];

$fecha=date("Ymd");
$fechaactual=date('Y/m/d H:i:s');
$clasuc = $_POST['clasuc'];
$actor = $_POST['actor'];
$arc = $_POST['archivo'];
$claada = $_POST['claada'];
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
{
if($clasuc != '' and $clasuc != 0 and $actor != '')
{
	if(is_array($_FILES)) 
	{
		mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'8',5,'".$clasuc."','".$usuario."','".$fechaactual."')");//Tercer campo tia_clave_int. 5=Adjuntar archivo

		$con = mysqli_query($conectar,"select MAX(ada_clave_int) max from adjunto_actor where suc_clave_int = '".$clasuc."'");
		$dato = mysqli_fetch_array($con);
		$num = $dato['max'];
		
		if($num == 0 || $num == ''){ $num = 1; }else{ $num = $num+1; }
				
		if($actor == 'INFORMEFINAL')
		{
			if(is_uploaded_file($_FILES['adjuntoinf']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntoinf']['tmp_name'];
				$archivo = basename($_FILES['adjuntoinf']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
				$nombrearchivo = $array_nombre[0];
				//$array_fecha = explode(' ',$nombrearchivo);
				//$fechaarchivo = $array_fecha[0];
				$anio = substr($nombrearchivo,0,4);
				$mes = substr($nombrearchivo,4,2);
				$dia = substr($nombrearchivo,6,2);
				$fech = $anio."-".$mes."-".$dia;

				$prefijo = $nombrearchivo;
				//$prefijo = $anio.$mes.$dia." CONSTRUCTOR - INFORME";
				$destino =  "../../adjuntos/constructor/".$clasuc."-".$prefijo."".$num.".".$extension;
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '16' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "errorv";
                }
                else
                if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                {
                    echo "error1";
                }
                else
                {
                    if (move_uploaded_file($sourcePath, $destino)) {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,suc_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clasuc . "','" . $fechaactual . "','" . $destino . "','" . $archivo . "',16,'" . $usuario . "','" . $fechaactual . "')");
                        echo "ok";
                    } else {
                        echo "error4";
                    }
                }
			}
			else{
			    echo "error3";
            }
		}
		else
		if($actor == 'POLIZAS')
		{
			if(is_uploaded_file($_FILES['adjuntopolizas']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntopolizas']['tmp_name'];
				$archivo = basename($_FILES['adjuntopolizas']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
				$nombrearchivo = $array_nombre[0];
				$anio = substr($nombrearchivo,0,4);
                $mes = substr($nombrearchivo,4,2);
                $dia = substr($nombrearchivo,6,2);
                $fech = $anio."-".$mes."-".$dia;

                $prefijo = $anio.$mes.$dia." CONSTRUCTOR - POLIZAS";
				$destino =  "../../adjuntos/constructor/".$clasuc."-".$prefijo."".$num.".".$extension;
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '39' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "errorv";
                }
                else
                if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                {
                    echo "error1";
                }
                else
                {
                    if (move_uploaded_file($sourcePath, $destino)) {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,suc_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clasuc . "','" . $fechaactual . "','" . $destino . "','" . $archivo . "',39,'" . $usuario . "','" . $fechaactual . "')");

                        $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario u inner join notificar_usuario nu on (nu.usu_clave_int = u.usu_clave_int) where not_clave_int = 15 and usu_email != ''");
                        $num = mysqli_num_rows($conema);
                        for ($i = 0; $i < $num; $i++) {
                            $mail = new PHPMailer();

                            $datoema = mysqli_fetch_array($conema);
                            $mail->Body = '';
                            $ema = '';
                            $nom = $datoema['usu_nombre'];
                            $ema = $datoema['usu_email'];

                            $concaj = mysqli_query($conectar,"select suc_nombre from sucursal where suc_clave_int = '" . $clasuc . "'");
                            $datocaj = mysqli_fetch_array($concaj);
                            $nomsuc = $datocaj['suc_nombre'];

                            $conact = mysqli_query($conectar,"select usu_nombre from usuario where usu_usuario = '" . $usuario . "'");
                            $datoact = mysqli_fetch_array($conact);
                            $nomact = $datoact['usu_nombre'];

                            $mail->From = "adminpavas@pavas.com.co";
                            $mail->FromName = "Soportica";
                            $mail->AddAddress($ema, "Destino");
                            $mail->Subject = "Adjunto Pólizas de Seguros - Sucursal Nro. " . $clasuc;

                            // Cuerpo del mensaje
                            $mail->Body .= "Hola " . $nom . "!\n\n";
                            $mail->Body .= "SUCURSALES Le informa que han adjuntado las pólizas de Seguros.\n";
                            $mail->Body .= "SUCURSAL: " . $nomsuc . "\n";
                            $mail->Body .= "USUARIO QUE ADJUNTA: " . $nomact . "\n";
                            $mail->Body .= date("d/m/Y H:m:s") . "\n\n";
                            $mail->Body .= "Este mensaje es generado automáticamente por SUCURSALES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Sucursales \n";

                            if (!$mail->Send())
                            {
                                //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                                //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                            } else
                            {

                            }
                        }
                        echo "ok";

                    }
                    else
                    {
                        echo "error4";
                    }
                }
			}
			else
            {
			    echo "error3";
            }
		}
		else
		if($actor == 'ACTUALIZACIONPRESUPUESTAL1')
		{
			if(is_uploaded_file($_FILES['adjuntoactualizacionpresupuestal1']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntoactualizacionpresupuestal1']['tmp_name'];
				$archivo = basename($_FILES['adjuntoactualizacionpresupuestal1']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
                $nombrearchivo = $array_nombre[0];
                $anio = substr($nombrearchivo,0,4);
                $mes = substr($nombrearchivo,4,2);
                $dia = substr($nombrearchivo,6,2);
                $fech = $anio."-".$mes."-".$dia;

				$prefijo = $anio.$mes.$dia." CONSTRUCTOR - ACTUALIZACIÓN PRESUPUESTAL";
				$destino =  "../../adjuntos/constructor/".$clasuc."-".$prefijo."".$num.".".$extension;
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '40' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "errorv";
                }
                else
                if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                {
                    echo "error1";
                }
                else
                {
                    if (move_uploaded_file($sourcePath, $destino))
                    {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,suc_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clasuc . "','" . $fechaactual . "','" . $destino . "','" . $archivo . "',40,'" . $usuario . "','" . $fechaactual . "')");

                        $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario u inner join notificar_usuario nu on (nu.usu_clave_int = u.usu_clave_int) where not_clave_int in (2) and usu_email != ''");
                        $num = mysqli_num_rows($conema);
                        for ($i = 0; $i < $num; $i++) {
                            $mail = new PHPMailer();

                            $datoema = mysqli_fetch_array($conema);
                            $mail->Body = '';
                            $ema = '';
                            $nom = $datoema['usu_nombre'];
                            $ema = $datoema['usu_email'];

                            $concaj = mysqli_query($conectar,"select suc_nombre from sucursal where suc_clave_int = '" . $clasuc . "'");
                            $datocaj = mysqli_fetch_array($concaj);
                            $nomsuc = $datocaj['suc_nombre'];

                            $conact = mysqli_query($conectar,"select usu_nombre from usuario where usu_usuario = '" . $usuario . "'");
                            $datoact = mysqli_fetch_array($conact);
                            $nomact = $datoact['usu_nombre'];

                            $mail->From = "adminpavas@pavas.com.co";
                            $mail->FromName = "Soportica";
                            $mail->AddAddress($ema, "Destino");
                            $mail->Subject = "Adjunto Actualización Presupuestal - Sucursal Nro. " . $clasuc;

                            // Cuerpo del mensaje
                            $mail->Body .= "Hola " . $nom . "!\n\n";
                            $mail->Body .= "SUCURSALES Le informa que han adjuntado la actualización presupuestal.\n";
                            $mail->Body .= "SUCURSAL: " . $nomsuc . "\n";
                            $mail->Body .= "USUARIO QUE ADJUNTA: " . $nomact . "\n";
                            $mail->Body .= date("d/m/Y H:m:s") . "\n\n";
                            $mail->Body .= "Este mensaje es generado automáticamente por SUCURSALES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Sucursales \n";

                            if (!$mail->Send()) {
                                //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                                //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                            }
                            else
                            {
                            }
                        }
                        echo "ok";
                    } else
                        {
                        echo "error4";
                        }
                }
			}
			else{
			    echo "error3";
            }
		}
		else
		if($actor == 'CONTROLCAMBIOS')
		{
			if(is_uploaded_file($_FILES['adjuntocontrolcambios']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntocontrolcambios']['tmp_name'];
				$archivo = basename($_FILES['adjuntocontrolcambios']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
                $nombrearchivo = $array_nombre[0];
                $anio = substr($nombrearchivo,0,4);
                $mes = substr($nombrearchivo,4,2);
                $dia = substr($nombrearchivo,6,2);
                $fech = $anio."-".$mes."-".$dia;

				$prefijo = $anio.$mes.$dia." CONSTRUCTOR - CONTROL CAMBIOS";
				$destino =  "../../adjuntos/constructor/".$clasuc."-".$prefijo."".$num.".".$extension;
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '41' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "errorv";
                }
                else
                if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                {
                    echo "error1";
                }
                else
                {
                    if (move_uploaded_file($sourcePath, $destino)) {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,suc_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clasuc . "','" . $fechaactual . "','" . $destino . "','" . $archivo . "',41,'" . $usuario . "','" . $fechaactual . "')");

                        $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario u inner join notificar_usuario nu on (nu.usu_clave_int = u.usu_clave_int) where not_clave_int in (3) and usu_email != ''");
                        $num = mysqli_num_rows($conema);
                        for ($i = 0; $i < $num; $i++) {
                            $mail = new PHPMailer();

                            $datoema = mysqli_fetch_array($conema);
                            $mail->Body = '';
                            $ema = '';
                            $nom = $datoema['usu_nombre'];
                            $ema = $datoema['usu_email'];

                            $concaj = mysqli_query($conectar,"select suc_nombre from sucursal where suc_clave_int = '" . $clasuc . "'");
                            $datocaj = mysqli_fetch_array($concaj);
                            $nomsuc = $datocaj['suc_nombre'];

                            $conact = mysqli_query($conectar,"select usu_nombre from usuario where usu_usuario = '" . $usuario . "'");
                            $datoact = mysqli_fetch_array($conact);
                            $nomact = $datoact['usu_nombre'];

                            $mail->From = "adminpavas@pavas.com.co";
                            $mail->FromName = "Soportica";
                            $mail->AddAddress($ema, "Destino");
                            $mail->Subject = "Adjunto Control de cambios - Sucursal Nro. " . $clasuc;

                            // Cuerpo del mensaje
                            $mail->Body .= "Hola " . $nom . "!\n\n";
                            $mail->Body .= "SUCURSALES Le informa que han adjuntado el control de cambios.\n";
                            $mail->Body .= "SUCURSAL: " . $nomsuc . "\n";
                            $mail->Body .= "USUARIO QUE ADJUNTA: " . $nomact . "\n";
                            $mail->Body .= date("d/m/Y H:m:s") . "\n\n";
                            $mail->Body .= "Este mensaje es generado automáticamente por SUCURSALES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Sucursales \n";

                            if (!$mail->Send()) {
                                //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                                //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                            } else {
                            }
                        }
                        echo "ok";
                    } else {
                        echo "error4";
                    }
                }
			}
			else{
			    echo "error3";
            }
		}
		else
		if($actor == 'SEGURIDADSOCIAL')
		{
			if(is_uploaded_file($_FILES['adjuntoseguridadsocial']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntoseguridadsocial']['tmp_name'];
				$archivo = basename($_FILES['adjuntoseguridadsocial']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
                $nombrearchivo = $array_nombre[0];
                $anio = substr($nombrearchivo,0,4);
                $mes = substr($nombrearchivo,4,2);
                $dia = substr($nombrearchivo,6,2);
                $fech = $anio."-".$mes."-".$dia;

				$prefijo = $anio.$mes.$dia." CONSTRUCTOR - SEGURIDAD SOCIAL";
				$destino =  "../../adjuntos/constructor/".$clasuc."-".$prefijo."".$num.".".$extension;
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '42' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "errorv";
                }
                else
                if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                {
                    echo "error1";
                }
                else
                {
                    if (move_uploaded_file($sourcePath, $destino)) {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,suc_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clasuc . "','" . $fechaactual . "','" . $destino . "','" . $archivo . "',42,'" . $usuario . "','" . $fechaactual . "')");

                        $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario u inner join notificar_usuario nu on (nu.usu_clave_int = u.usu_clave_int) where not_clave_int in (4) and usu_email != ''");
                        $num = mysqli_num_rows($conema);
                        for ($i = 0; $i < $num; $i++) {
                            $mail = new PHPMailer();

                            $datoema = mysqli_fetch_array($conema);
                            $mail->Body = '';
                            $ema = '';
                            $nom = $datoema['usu_nombre'];
                            $ema = $datoema['usu_email'];

                            $concaj = mysqli_query($conectar,"select suc_nombre from sucursal where suc_clave_int = '" . $clasuc . "'");
                            $datocaj = mysqli_fetch_array($concaj);
                            $nomsuc = $datocaj['suc_nombre'];

                            $conact = mysqli_query($conectar,"select usu_nombre from usuario where usu_usuario = '" . $usuario . "'");
                            $datoact = mysqli_fetch_array($conact);
                            $nomact = $datoact['usu_nombre'];

                            $mail->From = "adminpavas@pavas.com.co";
                            $mail->FromName = "Soportica";
                            $mail->AddAddress($ema, "Destino");
                            $mail->Subject = "Adjunto Planillas de seguridad social - Sucursal Nro. " . $clasuc;

                            // Cuerpo del mensaje
                            $mail->Body .= "Hola " . $nom . "!\n\n";
                            $mail->Body .= "SUCURSALES Le informa que han adjuntado la planillas de seguridad social.\n";
                            $mail->Body .= "SUCURSAL: " . $nomsuc . "\n";
                            $mail->Body .= "USUARIO QUE ADJUNTA: " . $nomact . "\n";
                            $mail->Body .= date("d/m/Y H:m:s") . "\n\n";
                            $mail->Body .= "Este mensaje es generado automáticamente por SUCURSALES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Sucursales \n";

                            if (!$mail->Send())
                            {
                                //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                                //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                            }
                            else
                            {
                            }
                        }
                        echo "ok";
                    }
                    else
                    {
                        echo "error4";
                    }
                }
			}
			else{
			    echo "error3";
            }
		}
		else
		if($actor == 'MANEJOAMBIENTAL')
		{
			if(is_uploaded_file($_FILES['adjuntomanejoambiental']['tmp_name'])) {
                $sourcePath = $_FILES['adjuntomanejoambiental']['tmp_name'];
                $archivo = basename($_FILES['adjuntomanejoambiental']['name']);

                $array_nombre = explode('.', $archivo);
                $cuenta_arr_nombre = count($array_nombre);
                $extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
                /*if($extension == 'jpg')
                {
                    $extension = 'png';
                }*/
                $nombrearchivo = $array_nombre[0];
                $anio = substr($nombrearchivo,0,4);
                $mes = substr($nombrearchivo,4,2);
                $dia = substr($nombrearchivo,6,2);
                $fech = $anio."-".$mes."-".$dia;


                $prefijo = $anio.$mes.$dia." CONSTRUCTOR - MANEJO AMBIENTAL";
                $destino = "../../adjuntos/constructor/" . $clasuc . "-" . $prefijo . "" . $num . "." . $extension;
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '43' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "errorv";
                }
                else
                if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                {
                    echo "error1";
                }
                else
                {
                    if (move_uploaded_file($sourcePath, $destino)) {
                    $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,suc_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clasuc . "','" . $fechaactual . "','" . $destino . "','" . $archivo . "',43,'" . $usuario . "','" . $fechaactual . "')");

                    $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario u inner join notificar_usuario nu on (nu.usu_clave_int = u.usu_clave_int) where not_clave_int = 16 and usu_email != ''");
                    $num = mysqli_num_rows($conema);
                    for ($i = 0; $i < $num; $i++) {
                        $mail = new PHPMailer();

                        $datoema = mysqli_fetch_array($conema);
                        $mail->Body = '';
                        $ema = '';
                        $nom = $datoema['usu_nombre'];
                        $ema = $datoema['usu_email'];

                        $concaj = mysqli_query($conectar,"select suc_nombre from sucursal where suc_clave_int = '" . $clasuc . "'");
                        $datocaj = mysqli_fetch_array($concaj);
                        $nomsuc = $datocaj['suc_nombre'];

                        $conact = mysqli_query($conectar,"select usu_nombre from usuario where usu_usuario = '" . $usuario . "'");
                        $datoact = mysqli_fetch_array($conact);
                        $nomact = $datoact['usu_nombre'];

                        $mail->From = "adminpavas@pavas.com.co";
                        $mail->FromName = "Soportica";
                        $mail->AddAddress($ema, "Destino");
                        $mail->Subject = "Adjunto Plan de manejo ambiental - Sucursal Nro. " . $clasuc;

                        // Cuerpo del mensaje
                        $mail->Body .= "Hola " . $nom . "!\n\n";
                        $mail->Body .= "SUCURSALES Le informa que han adjuntado el plan de manejo ambiental.\n";
                        $mail->Body .= "SUCURSAL: " . $nomsuc . "\n";
                        $mail->Body .= "USUARIO QUE ADJUNTA: " . $nomact . "\n";
                        $mail->Body .= date("d/m/Y H:m:s") . "\n\n";
                        $mail->Body .= "Este mensaje es generado automáticamente por SUCURSALES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Sucursales \n";

                        if (!$mail->Send()) {
                            //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                            //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                        } else {
                        }
                    }
                    echo "ok";
                } else
                    {
                    echo "error4";
                }
            }
			}
			else{
			    echo "error3";
            }
		}
		else
		if($actor == 'PLANCALIDAD')
		{
			if(is_uploaded_file($_FILES['adjuntoplancalidad']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntoplancalidad']['tmp_name'];
				$archivo = basename($_FILES['adjuntoplancalidad']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
                $nombrearchivo = $array_nombre[0];
                $anio = substr($nombrearchivo,0,4);
                $mes = substr($nombrearchivo,4,2);
                $dia = substr($nombrearchivo,6,2);
                $fech = $anio."-".$mes."-".$dia;

				$prefijo = $anio.$mes.$dia." CONSTRUCTOR - PLAN CALIDAD";
				$destino =  "../../adjuntos/constructor/".$clasuc."-".$prefijo."".$num.".".$extension;
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '44' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "errorv";
                }
                else
                if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                {
                    echo "error1";
                }
                else
                {
                    if (move_uploaded_file($sourcePath, $destino))
                    {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,suc_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clasuc . "','" . $fechaactual . "','" . $destino . "','" . $archivo . "',44,'" . $usuario . "','" . $fechaactual . "')");

                        $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario u inner join notificar_usuario nu on (nu.usu_clave_int = u.usu_clave_int) where not_clave_int = 17 and usu_email != ''");
                        $num = mysqli_num_rows($conema);
                        for ($i = 0; $i < $num; $i++) {
                            $mail = new PHPMailer();

                            $datoema = mysqli_fetch_array($conema);
                            $mail->Body = '';
                            $ema = '';
                            $nom = $datoema['usu_nombre'];
                            $ema = $datoema['usu_email'];

                            $concaj = mysqli_query($conectar,"select suc_nombre from sucursal where suc_clave_int = '" . $clasuc . "'");
                            $datocaj = mysqli_fetch_array($concaj);
                            $nomsuc = $datocaj['suc_nombre'];

                            $conact = mysqli_query($conectar,"select usu_nombre from usuario where usu_usuario = '" . $usuario . "'");
                            $datoact = mysqli_fetch_array($conact);
                            $nomact = $datoact['usu_nombre'];

                            $mail->From = "adminpavas@pavas.com.co";
                            $mail->FromName = "Soportica";
                            $mail->AddAddress($ema, "Destino");
                            $mail->Subject = "Adjunto Plan de calidad - Sucursal Nro. " . $clasuc;

                            // Cuerpo del mensaje
                            $mail->Body .= "Hola " . $nom . "!\n\n";
                            $mail->Body .= "SUCURSALES Le informa que han adjuntado el Plan de calidad.\n";
                            $mail->Body .= "SUCURSAL: " . $nomsuc . "\n";
                            $mail->Body .= "USUARIO QUE ADJUNTA: " . $nomact . "\n";
                            $mail->Body .= date("d/m/Y H:m:s") . "\n\n";
                            $mail->Body .= "Este mensaje es generado automáticamente por SUCURSALES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Sucursales \n";

                            if (!$mail->Send()) {
                                //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                                //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                            } else {
                            }
                        }
                        echo "ok";
                    }
                    else
                    {
                        echo "error4";
                    }
                }
			}
			else{
			    echo "error3";
            }
		}
		else
		if($actor == 'ACTUALIZACIONPRESUPUESTAL2')
		{
			if(is_uploaded_file($_FILES['adjuntoactualizacionpresupuestal2']['tmp_name'])) {
                $sourcePath = $_FILES['adjuntoactualizacionpresupuestal2']['tmp_name'];
                $archivo = basename($_FILES['adjuntoactualizacionpresupuestal2']['name']);

                $array_nombre = explode('.', $archivo);
                $cuenta_arr_nombre = count($array_nombre);
                $extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
                /*if($extension == 'jpg')
                {
                    $extension = 'png';
                }*/
                $nombrearchivo = $array_nombre[0];
                $anio = substr($nombrearchivo,0,4);
                $mes = substr($nombrearchivo,4,2);
                $dia = substr($nombrearchivo,6,2);
                $fech = $anio."-".$mes."-".$dia;


                $prefijo = $anio.$mes.$dia." CONSTRUCTOR - ACTUALIZACION PRESUPUESTAL";
                $destino = "../../adjuntos/constructor/" . $clasuc . "-" . $prefijo . "" . $num . "." . $extension;
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '45' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "errorv";
                }
                else
                if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                {
                    echo "error1";
                }
                else
                {
                    if (move_uploaded_file($sourcePath, $destino))
                    {
                    $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,suc_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clasuc . "','" . $fechaactual . "','" . $destino . "','" . $archivo . "',45,'" . $usuario . "','" . $fechaactual . "')");

                    $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario u inner join notificar_usuario nu on (nu.usu_clave_int = u.usu_clave_int) where not_clave_int = 2 and usu_email != ''");
                    $num = mysqli_num_rows($conema);
                    for ($i = 0; $i < $num; $i++) {
                        $mail = new PHPMailer();

                        $datoema = mysqli_fetch_array($conema);
                        $mail->Body = '';
                        $ema = '';
                        $nom = $datoema['usu_nombre'];
                        $ema = $datoema['usu_email'];

                        $concaj = mysqli_query($conectar,"select suc_nombre from sucursal where suc_clave_int = '" . $clasuc . "'");
                        $datocaj = mysqli_fetch_array($concaj);
                        $nomsuc = $datocaj['suc_nombre'];

                        $conact = mysqli_query($conectar,"select usu_nombre from usuario where usu_usuario = '" . $usuario . "'");
                        $datoact = mysqli_fetch_array($conact);
                        $nomact = $datoact['usu_nombre'];

                        $mail->From = "adminpavas@pavas.com.co";
                        $mail->FromName = "Soportica";
                        $mail->AddAddress($ema, "Destino");
                        $mail->Subject = "Adjunto Actualización presupuestal - Sucursal Nro. " . $clasuc;

                        // Cuerpo del mensaje
                        $mail->Body .= "Hola " . $nom . "!\n\n";
                        $mail->Body .= "SUCURSALES Le informa que han adjuntado la actualizacion presupuestal.\n";
                        $mail->Body .= "SUCURSAL: " . $nomsuc . "\n";
                        $mail->Body .= "USUARIO QUE ADJUNTA: " . $nomact . "\n";
                        $mail->Body .= date("d/m/Y H:m:s") . "\n\n";
                        $mail->Body .= "Este mensaje es generado automáticamente por SUCURSALES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Sucursales \n";

                            if (!$mail->Send())
                            {
                                //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                                //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                            }
                            else
                            {
                            }
                        }
                        echo "ok";
                    }
                    else
                    {
                        echo "error4";
                    }
                }
			}
			else
            {
                echo "error3";
            }
		}
		else
		if($actor == 'LIQUIDACION')
		{
			if(is_uploaded_file($_FILES['adjuntoliquidacion']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntoliquidacion']['tmp_name'];
				$archivo = basename($_FILES['adjuntoliquidacion']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
                $nombrearchivo = $array_nombre[0];
                $anio = substr($nombrearchivo,0,4);
                $mes = substr($nombrearchivo,4,2);
                $dia = substr($nombrearchivo,6,2);
                $fech = $anio."-".$mes."-".$dia;


				$prefijo = $anio.$mes.$dia." CONSTRUCTOR - LIQUIDACION";
				$destino =  "../../adjuntos/constructor/".$clasuc."-".$prefijo."".$num.".".$extension;
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '46' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "errorv";
                }
                else
                if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                {
                    echo "error1";
                }
                else
                {
                    if (move_uploaded_file($sourcePath, $destino)) {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,suc_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clasuc . "','" . $fechaactual . "','" . $destino . "','" . $archivo . "',46,'" . $usuario . "','" . $fechaactual . "')");

                        $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario u inner join notificar_usuario nu on (nu.usu_clave_int = u.usu_clave_int) where not_clave_int = 18 and usu_email != ''");
                        $num = mysqli_num_rows($conema);
                        for ($i = 0; $i < $num; $i++) {
                            $mail = new PHPMailer();

                            $datoema = mysqli_fetch_array($conema);
                            $mail->Body = '';
                            $ema = '';
                            $nom = $datoema['usu_nombre'];
                            $ema = $datoema['usu_email'];

                            $concaj = mysqli_query($conectar,"select suc_nombre from sucursal where suc_clave_int = '" . $clasuc . "'");
                            $datocaj = mysqli_fetch_array($concaj);
                            $nomsuc = $datocaj['suc_nombre'];

                            $conact = mysqli_query($conectar,"select usu_nombre from usuario where usu_usuario = '" . $usuario . "'");
                            $datoact = mysqli_fetch_array($conact);
                            $nomact = $datoact['usu_nombre'];

                            $mail->From = "adminpavas@pavas.com.co";
                            $mail->FromName = "Soportica";
                            $mail->AddAddress($ema, "Destino");
                            $mail->Subject = "Adjunto Liquidacion - Sucursal Nro. " . $clasuc;

                            // Cuerpo del mensaje
                            $mail->Body .= "Hola " . $nom . "!\n\n";
                            $mail->Body .= "SUCURSALES Le informa que han adjuntado la liquidacion del constructor.\n";
                            $mail->Body .= "SUCURSAL: " . $nomsuc . "\n";
                            $mail->Body .= "USUARIO QUE ADJUNTA: " . $nomact . "\n";
                            $mail->Body .= date("d/m/Y H:m:s") . "\n\n";
                            $mail->Body .= "Este mensaje es generado automáticamente por SUCURSALES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Sucursales \n";

                            if (!$mail->Send()) {
                                //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                                //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                            } else {
                            }
                        }
                        echo "ok";
                    } else {
                        echo "error4";
                    }
                }
			}
			else
            {
                echo "error3";
            }
		}
		else
		if($actor == 'FICHATECNICA')
		{
			if(is_uploaded_file($_FILES['adjuntofichatecnica']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntofichatecnica']['tmp_name'];
				$archivo = basename($_FILES['adjuntofichatecnica']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
                $nombrearchivo = $array_nombre[0];
                $anio = substr($nombrearchivo,0,4);
                $mes = substr($nombrearchivo,4,2);
                $dia = substr($nombrearchivo,6,2);
                $fech = $anio."-".$mes."-".$dia;

				$prefijo = $anio.$mes.$dia." CONSTRUCTOR - FICHA TECNICA";
				$destino =  "../../adjuntos/constructor/".$clasuc."-".$prefijo."".$num.".".$extension;
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '47' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "errorv";
                }
                else
                if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                {
                    echo "error1";
                }
                else
                {
                    if (move_uploaded_file($sourcePath, $destino)) {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,suc_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clasuc . "','" . $fechaactual . "','" . $destino . "','" . $archivo . "',47,'" . $usuario . "','" . $fechaactual . "')");

                        $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario u inner join notificar_usuario nu on (nu.usu_clave_int = u.usu_clave_int) where not_clave_int = 19 and usu_email != ''");
                        $num = mysqli_num_rows($conema);
                        for ($i = 0; $i < $num; $i++) {
                            $mail = new PHPMailer();

                            $datoema = mysqli_fetch_array($conema);
                            $mail->Body = '';
                            $ema = '';
                            $nom = $datoema['usu_nombre'];
                            $ema = $datoema['usu_email'];

                            $concaj = mysqli_query($conectar,"select suc_nombre from sucursal where suc_clave_int = '" . $clasuc . "'");
                            $datocaj = mysqli_fetch_array($concaj);
                            $nomsuc = $datocaj['suc_nombre'];

                            $conact = mysqli_query($conectar,"select usu_nombre from usuario where usu_usuario = '" . $usuario . "'");
                            $datoact = mysqli_fetch_array($conact);
                            $nomact = $datoact['usu_nombre'];

                            $mail->From = "adminpavas@pavas.com.co";
                            $mail->FromName = "Soportica";
                            $mail->AddAddress($ema, "Destino");
                            $mail->Subject = "Adjunto Ficha Tecnica - Sucursal Nro. " . $clasuc;

                            // Cuerpo del mensaje
                            $mail->Body .= "Hola " . $nom . "!\n\n";
                            $mail->Body .= "SUCURSALES Le informa que han adjuntado la ficha tecnica.\n";
                            $mail->Body .= "SUCURSAL: " . $nomsuc . "\n";
                            $mail->Body .= "USUARIO QUE ADJUNTA: " . $nomact . "\n";
                            $mail->Body .= date("d/m/Y H:m:s") . "\n\n";
                            $mail->Body .= "Este mensaje es generado automáticamente por SUCURSALES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Sucursales \n";

                            if (!$mail->Send()) {
                                //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                                //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                            } else {
                            }
                        }
                        echo "ok";
                    } else {
                        echo "error4";
                    }
                }
			}
			else{
			    echo "error3";
            }
		}
		else
		if($actor == 'REGISTROFOTOGRAFICO')
		{
			if(is_uploaded_file($_FILES['adjuntoregistrofotografico']['tmp_name'])) {
                $sourcePath = $_FILES['adjuntoregistrofotografico']['tmp_name'];
                $archivo = basename($_FILES['adjuntoregistrofotografico']['name']);

                $array_nombre = explode('.', $archivo);
                $cuenta_arr_nombre = count($array_nombre);
                $extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
                /*if($extension == 'jpg')
                {
                    $extension = 'png';
                }*/
                $nombrearchivo = $array_nombre[0];
                $anio = substr($nombrearchivo,0,4);
                $mes = substr($nombrearchivo,4,2);
                $dia = substr($nombrearchivo,6,2);
                $fech = $anio."-".$mes."-".$dia;

                $prefijo = $anio.$mes.$dia." CONSTRUCTOR - REGISTRO FOTOGRAFICO";
                $destino = "../../adjuntos/constructor/" . $clasuc . "-" . $prefijo . "" . $num . "." . $extension;
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '48' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "errorv";
                }
                else
                if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                {
                    echo "error1";
                }
                else
                {
                    if (move_uploaded_file($sourcePath, $destino)) {
                    $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,suc_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clasuc . "','" . $fechaactual . "','" . $destino . "','" . $archivo . "',48,'" . $usuario . "','" . $fechaactual . "')");

                    $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario u inner join notificar_usuario nu on (nu.usu_clave_int = u.usu_clave_int) where not_clave_int = 20 and usu_email != ''");
                    $num = mysqli_num_rows($conema);
                    for ($i = 0; $i < $num; $i++) {
                        $mail = new PHPMailer();

                        $datoema = mysqli_fetch_array($conema);
                        $mail->Body = '';
                        $ema = '';
                        $nom = $datoema['usu_nombre'];
                        $ema = $datoema['usu_email'];

                        $concaj = mysqli_query($conectar,"select suc_nombre from sucursal where suc_clave_int = '" . $clasuc . "'");
                        $datocaj = mysqli_fetch_array($concaj);
                        $nomsuc = $datocaj['suc_nombre'];

                        $conact = mysqli_query($conectar,"select usu_nombre from usuario where usu_usuario = '" . $usuario . "'");
                        $datoact = mysqli_fetch_array($conact);
                        $nomact = $datoact['usu_nombre'];

                        $mail->From = "adminpavas@pavas.com.co";
                        $mail->FromName = "Soportica";
                        $mail->AddAddress($ema, "Destino");
                        $mail->Subject = "Adjunto Registro Fotografico - Sucursal Nro. " . $clasuc;

                        // Cuerpo del mensaje
                        $mail->Body .= "Hola " . $nom . "!\n\n";
                        $mail->Body .= "SUCURSALES Le informa que han adjuntado el registro fotografico.\n";
                        $mail->Body .= "SUCURSAL: " . $nomsuc . "\n";
                        $mail->Body .= "USUARIO QUE ADJUNTA: " . $nomact . "\n";
                        $mail->Body .= date("d/m/Y H:m:s") . "\n\n";
                        $mail->Body .= "Este mensaje es generado automáticamente por SUCURSALES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Sucursales \n";

                        if (!$mail->Send()) {
                            //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                            //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                        } else {
                        }
                    }
                    echo "ok";
                } else {
                    echo "error4";
                }
            }
			}
			else{
			    echo "error3";
            }
		}
		else
		if($actor == 'CERTIFICADOESCOMBROS')
		{
			if(is_uploaded_file($_FILES['adjuntocertificadoescombros']['tmp_name'])) {
                $sourcePath = $_FILES['adjuntocertificadoescombros']['tmp_name'];
                $archivo = basename($_FILES['adjuntocertificadoescombros']['name']);

                $array_nombre = explode('.', $archivo);
                $cuenta_arr_nombre = count($array_nombre);
                $extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
                /*if($extension == 'jpg')
                {
                    $extension = 'png';
                }*/
                $nombrearchivo = $array_nombre[0];
                $anio = substr($nombrearchivo,0,4);
                $mes = substr($nombrearchivo,4,2);
                $dia = substr($nombrearchivo,6,2);
                $fech = $anio."-".$mes."-".$dia;

                $prefijo = $anio.$mes.$dia." CONSTRUCTOR - CERTIFICADO ESCOMBROS";
                $destino = "../../adjuntos/constructor/" . $clasuc . "-" . $prefijo . "" . $num . "." . $extension;
                 $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '49' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "errorv";
                }
                else
                if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                {
                    echo "error1";
                }
                else
                {
                if (move_uploaded_file($sourcePath, $destino)) {
                    $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,suc_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clasuc . "','" . $fechaactual . "','" . $destino . "','" . $archivo . "',49,'" . $usuario . "','" . $fechaactual . "')");

                    $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario u inner join notificar_usuario nu on (nu.usu_clave_int = u.usu_clave_int) where not_clave_int = 21 and usu_email != ''");
                    $num = mysqli_num_rows($conema);
                    for ($i = 0; $i < $num; $i++) {
                        $mail = new PHPMailer();

                        $datoema = mysqli_fetch_array($conema);
                        $mail->Body = '';
                        $ema = '';
                        $nom = $datoema['usu_nombre'];
                        $ema = $datoema['usu_email'];

                        $concaj = mysqli_query($conectar,"select suc_nombre from sucursal where suc_clave_int = '" . $clasuc . "'");
                        $datocaj = mysqli_fetch_array($concaj);
                        $nomsuc = $datocaj['suc_nombre'];

                        $conact = mysqli_query($conectar,"select usu_nombre from usuario where usu_usuario = '" . $usuario . "'");
                        $datoact = mysqli_fetch_array($conact);
                        $nomact = $datoact['usu_nombre'];

                        $mail->From = "adminpavas@pavas.com.co";
                        $mail->FromName = "Soportica";
                        $mail->AddAddress($ema, "Destino");
                        $mail->Subject = "Adjunto Certificado de escombros - Sucursal Nro. " . $clasuc;

                        // Cuerpo del mensaje
                        $mail->Body .= "Hola " . $nom . "!\n\n";
                        $mail->Body .= "SUCURSALES Le informa que han adjuntado el certificado de escombros.\n";
                        $mail->Body .= "SUCURSAL: " . $nomsuc . "\n";
                        $mail->Body .= "USUARIO QUE ADJUNTA: " . $nomact . "\n";
                        $mail->Body .= date("d/m/Y H:m:s") . "\n\n";
                        $mail->Body .= "Este mensaje es generado automáticamente por SUCURSALES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Sucursales \n";

                        if (!$mail->Send()) {
                            //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                            //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                        } else {
                        }
                    }
                    echo "ok";
                } else {
                    echo "error4";
                }
            }
			}
			else
            {
                echo "error3";
            }
		}
		else
		if($actor == 'CERTIFICADORETIE')
		{
			if(is_uploaded_file($_FILES['adjuntocertificadoretie']['tmp_name'])) {
                $sourcePath = $_FILES['adjuntocertificadoretie']['tmp_name'];
                $archivo = basename($_FILES['adjuntocertificadoretie']['name']);

                $array_nombre = explode('.', $archivo);
                $cuenta_arr_nombre = count($array_nombre);
                $extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
                /*if($extension == 'jpg')
                {
                    $extension = 'png';
                }*/
                $nombrearchivo = $array_nombre[0];
                $anio = substr($nombrearchivo,0,4);
                $mes = substr($nombrearchivo,4,2);
                $dia = substr($nombrearchivo,6,2);
                $fech = $anio."-".$mes."-".$dia;

                $prefijo = $anio.$mes.$dia." CONSTRUCTOR - CERTIFICADO RETIE";
                $destino = "../../adjuntos/constructor/" . $clasuc . "-" . $prefijo . "" . $num . "." . $extension;
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '50' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "errorv";
                }
                else
                if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                {
                    echo "error1";
                }
                else
                {
                    if (move_uploaded_file($sourcePath, $destino)) {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,suc_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clasuc . "','" . $fechaactual . "','" . $destino . "','" . $archivo . "',50,'" . $usuario . "','" . $fechaactual . "')");

                        $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario u inner join notificar_usuario nu on (nu.usu_clave_int = u.usu_clave_int) where not_clave_int = 22 and usu_email != ''");
                        $num = mysqli_num_rows($conema);
                        for ($i = 0; $i < $num; $i++) {
                            $mail = new PHPMailer();

                            $datoema = mysqli_fetch_array($conema);
                            $mail->Body = '';
                            $ema = '';
                            $nom = $datoema['usu_nombre'];
                            $ema = $datoema['usu_email'];

                            $concaj = mysqli_query($conectar,"select suc_nombre from sucursal where suc_clave_int = '" . $clasuc . "'");
                            $datocaj = mysqli_fetch_array($concaj);
                            $nomsuc = $datocaj['suc_nombre'];

                            $conact = mysqli_query($conectar,"select usu_nombre from usuario where usu_usuario = '" . $usuario . "'");
                            $datoact = mysqli_fetch_array($conact);
                            $nomact = $datoact['usu_nombre'];

                            $mail->From = "adminpavas@pavas.com.co";
                            $mail->FromName = "Soportica";
                            $mail->AddAddress($ema, "Destino");
                            $mail->Subject = "Adjunto Certificado Retie - Sucursal Nro. " . $clasuc;

                            // Cuerpo del mensaje
                            $mail->Body .= "Hola " . $nom . "!\n\n";
                            $mail->Body .= "SUCURSALES Le informa que han adjuntado el certificado retie.\n";
                            $mail->Body .= "SUCURSAL: " . $nomsuc . "\n";
                            $mail->Body .= "USUARIO QUE ADJUNTA: " . $nomact . "\n";
                            $mail->Body .= date("d/m/Y H:m:s") . "\n\n";
                            $mail->Body .= "Este mensaje es generado automáticamente por SUCURSALES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Sucursales \n";

                            if (!$mail->Send()) {
                                //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                                //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                            } else {
                            }
                        }
                        echo "ok";
                    } else {
                        echo "error4";
                    }
                }
			}
			else{
			    echo "error3";
            }
		}
		else
		if($actor == 'PRUEBAARRANQUE')
		{
			if(is_uploaded_file($_FILES['adjuntopruebaarranque']['tmp_name'])) {
                $sourcePath = $_FILES['adjuntopruebaarranque']['tmp_name'];
                $archivo = basename($_FILES['adjuntopruebaarranque']['name']);

                $array_nombre = explode('.', $archivo);
                $cuenta_arr_nombre = count($array_nombre);
                $extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
                /*if($extension == 'jpg')
                {
                    $extension = 'png';
                }*/
                $nombrearchivo = $array_nombre[0];
                $anio = substr($nombrearchivo,0,4);
                $mes = substr($nombrearchivo,4,2);
                $dia = substr($nombrearchivo,6,2);
                $fech = $anio."-".$mes."-".$dia;

                $prefijo = $anio.$mes.$dia." CONSTRUCTOR - PRUEBA ARRANQUE";
                $destino = "../../adjuntos/constructor/" . $clasuc . "-" . $prefijo . "" . $num . "." . $extension;
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '51' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "errorv";
                }
                else
                if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                {
                    echo "error1";
                }
                else
                {
                    if (move_uploaded_file($sourcePath, $destino)) {
                    $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,suc_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clasuc . "','" . $fechaactual . "','" . $destino . "','" . $archivo . "',51,'" . $usuario . "','" . $fechaactual . "')");

                    $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario u inner join notificar_usuario nu on (nu.usu_clave_int = u.usu_clave_int) where not_clave_int = 23 and usu_email != ''");
                    $num = mysqli_num_rows($conema);
                        for ($i = 0; $i < $num; $i++)
                        {
                            $mail = new PHPMailer();

                            $datoema = mysqli_fetch_array($conema);
                            $mail->Body = '';
                            $ema = '';
                            $nom = $datoema['usu_nombre'];
                            $ema = $datoema['usu_email'];

                            $concaj = mysqli_query($conectar,"select suc_nombre from sucursal where suc_clave_int = '" . $clasuc . "'");
                            $datocaj = mysqli_fetch_array($concaj);
                            $nomsuc = $datocaj['suc_nombre'];

                            $conact = mysqli_query($conectar,"select usu_nombre from usuario where usu_usuario = '" . $usuario . "'");
                            $datoact = mysqli_fetch_array($conact);
                            $nomact = $datoact['usu_nombre'];

                            $mail->From = "adminpavas@pavas.com.co";
                            $mail->FromName = "Soportica";
                            $mail->AddAddress($ema, "Destino");
                            $mail->Subject = "Adjunto Prueba Arranque equipos - Sucursal Nro. " . $clasuc;

                            // Cuerpo del mensaje
                            $mail->Body .= "Hola " . $nom . "!\n\n";
                            $mail->Body .= "SUCURSALES Le informa que han adjuntado las pruebas de arranque de equipos.\n";
                            $mail->Body .= "SUCURSAL: " . $nomsuc . "\n";
                            $mail->Body .= "USUARIO QUE ADJUNTA: " . $nomact . "\n";
                            $mail->Body .= date("d/m/Y H:m:s") . "\n\n";
                            $mail->Body .= "Este mensaje es generado automáticamente por SUCURSALES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Sucursales \n";

                            if (!$mail->Send()) {
                                //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                                //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                            }
                            else
                            {
                            }
                        }
                         echo "ok";
                    }
                    else
                    {
                        echo "error4";
                    }
                }
			}
			else
            {
                echo "error3";
            }
		}
		else
		if($actor == 'EVALUACIONFINAL')
		{
			if(is_uploaded_file($_FILES['adjuntoevaluacionfinal']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntoevaluacionfinal']['tmp_name'];
				$archivo = basename($_FILES['adjuntoevaluacionfinal']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
                $nombrearchivo = $array_nombre[0];
                $anio = substr($nombrearchivo,0,4);
                $mes = substr($nombrearchivo,4,2);
                $dia = substr($nombrearchivo,6,2);
                $fech = $anio."-".$mes."-".$dia;

				$prefijo = $anio.$mes.$dia." CONSTRUCTOR - EVALUACION FINAL";
				$destino =  "../../adjuntos/constructor/".$clasuc."-".$prefijo."".$num.".".$extension;
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '52' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "errorv";
                }
                else
                if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                {
                    echo "error1";
                }
                else
                {
                    if (move_uploaded_file($sourcePath, $destino)) {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,suc_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clasuc . "','" . $fechaactual . "','" . $destino . "','" . $archivo . "',52,'" . $usuario . "','" . $fechaactual . "')");

                        $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario u inner join notificar_usuario nu on (nu.usu_clave_int = u.usu_clave_int) where not_clave_int = 24 and usu_email != ''");
                        $num = mysqli_num_rows($conema);
                        for ($i = 0; $i < $num; $i++) {
                            $mail = new PHPMailer();

                            $datoema = mysqli_fetch_array($conema);
                            $mail->Body = '';
                            $ema = '';
                            $nom = $datoema['usu_nombre'];
                            $ema = $datoema['usu_email'];

                            $concaj = mysqli_query($conectar,"select suc_nombre from sucursal where suc_clave_int = '" . $clasuc . "'");
                            $datocaj = mysqli_fetch_array($concaj);
                            $nomsuc = $datocaj['suc_nombre'];

                            $conact = mysqli_query($conectar,"select usu_nombre from usuario where usu_usuario = '" . $usuario . "'");
                            $datoact = mysqli_fetch_array($conact);
                            $nomact = $datoact['usu_nombre'];

                            $mail->From = "adminpavas@pavas.com.co";
                            $mail->FromName = "Soportica";
                            $mail->AddAddress($ema, "Destino");
                            $mail->Subject = "Adjunto Evaluación Final - Sucursal Nro. " . $clasuc;

                            // Cuerpo del mensaje
                            $mail->Body .= "Hola " . $nom . "!\n\n";
                            $mail->Body .= "SUCURSALES Le informa que han adjuntado la evaluacion final.\n";
                            $mail->Body .= "SUCURSAL: " . $nomsuc . "\n";
                            $mail->Body .= "USUARIO QUE ADJUNTA: " . $nomact . "\n";
                            $mail->Body .= date("d/m/Y H:m:s") . "\n\n";
                            $mail->Body .= "Este mensaje es generado automáticamente por SUCURSALES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Sucursales \n";

                            if (!$mail->Send()) {
                                //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                                //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                            } else {
                            }
                        }
                        echo "ok";
                    } else {
                        echo "error4";
                    }
                }
			}
			else{
			    echo "error3";
            }
		}
		else

		if($actor == 'EQUIPOSTECNICOS')
		{
			if(is_uploaded_file($_FILES['adjuntoequipostecnicos']['tmp_name'])) {
                $sourcePath = $_FILES['adjuntoequipostecnicos']['tmp_name'];
                $archivo = basename($_FILES['adjuntoequipostecnicos']['name']);

                $array_nombre = explode('.', $archivo);
                $cuenta_arr_nombre = count($array_nombre);
                $extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
                /*if($extension == 'jpg')
                {
                    $extension = 'png';
                }*/
                $nombrearchivo = $array_nombre[0];
                $anio = substr($nombrearchivo,0,4);
                $mes = substr($nombrearchivo,4,2);
                $dia = substr($nombrearchivo,6,2);
                $fech = $anio."-".$mes."-".$dia;

                $prefijo = $anio.$mes.$dia." CONSTRUCTOR - EQUIPOS TECNICOS";
                $destino = "../../adjuntos/constructor/" . $clasuc . "-" . $prefijo . "" . $num . "." . $extension;
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '54' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "errorv";
                }
                else
                if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                {
                    echo "error1";
                }
                else
                {
                    if (move_uploaded_file($sourcePath, $destino))
                    {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,suc_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clasuc . "','" . $fechaactual . "','" . $destino . "','" . $archivo . "',54,'" . $usuario . "','" . $fechaactual . "')");

                        $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario u inner join notificar_usuario nu on (nu.usu_clave_int = u.usu_clave_int) where not_clave_int = 25 and usu_email != ''");
                        $num = mysqli_num_rows($conema);
                        for ($i = 0; $i < $num; $i++)
                        {
                        $mail = new PHPMailer();

                        $datoema = mysqli_fetch_array($conema);
                        $mail->Body = '';
                        $ema = '';
                        $nom = $datoema['usu_nombre'];
                        $ema = $datoema['usu_email'];

                        $concaj = mysqli_query($conectar,"select suc_nombre from sucursal where suc_clave_int = '" . $clasuc . "'");
                        $datocaj = mysqli_fetch_array($concaj);
                        $nomsuc = $datocaj['suc_nombre'];

                        $conact = mysqli_query($conectar,"select usu_nombre from usuario where usu_usuario = '" . $usuario . "'");
                        $datoact = mysqli_fetch_array($conact);
                        $nomact = $datoact['usu_nombre'];

                        $mail->From = "adminpavas@pavas.com.co";
                        $mail->FromName = "Soportica";
                        $mail->AddAddress($ema, "Destino");
                        $mail->Subject = "Adjunto Manuales de operacion de equipos tecnicos - Sucursal Nro. " . $clasuc;

                        // Cuerpo del mensaje
                        $mail->Body .= "Hola " . $nom . "!\n\n";
                        $mail->Body .= "SUCURSALES Le informa que han adjuntado los manuales de operacion de equipos tecnicos.\n";
                        $mail->Body .= "SUCURSAL: " . $nomsuc . "\n";
                        $mail->Body .= "USUARIO QUE ADJUNTA: " . $nomact . "\n";
                        $mail->Body .= date("d/m/Y H:m:s") . "\n\n";
                        $mail->Body .= "Este mensaje es generado automáticamente por SUCURSALES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Sucursales \n";

                        if (!$mail->Send()) {
                            //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                            //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                        } else {
                        }
                    }
                    echo "ok";
                } else
                    {
                        echo "error4";
                    }
                }
			}
			else{
			    echo "error3";
            }
		}
		else
		if($actor == 'LISTACHEQUEO')
		{
			if(is_uploaded_file($_FILES['adjuntolistachequeo']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntolistachequeo']['tmp_name'];
				$archivo = basename($_FILES['adjuntolistachequeo']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
                $nombrearchivo = $array_nombre[0];
                $anio = substr($nombrearchivo,0,4);
                $mes = substr($nombrearchivo,4,2);
                $dia = substr($nombrearchivo,6,2);
                $fech = $anio."-".$mes."-".$dia;

				$prefijo = $anio.$mes.$dia." CONSTRUCTOR - LISTA CHEQUEO";
				$destino =  "../../adjuntos/constructor/".$clasuc."-".$prefijo."".$num.".".$extension;
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '55' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "errorv";
                }
                else
                if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                {
                    echo "error1";
                }
                else
                {

                    if (move_uploaded_file($sourcePath, $destino)) {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,suc_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clasuc . "','" . $fechaactual . "','" . $destino . "','" . $archivo . "',55,'" . $usuario . "','" . $fechaactual . "')");

                        $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario u inner join notificar_usuario nu on (nu.usu_clave_int = u.usu_clave_int) where not_clave_int = 26 and usu_email != ''");
                        $num = mysqli_num_rows($conema);
                        for ($i = 0; $i < $num; $i++) {
                            $mail = new PHPMailer();

                            $datoema = mysqli_fetch_array($conema);
                            $mail->Body = '';
                            $ema = '';
                            $nom = $datoema['usu_nombre'];
                            $ema = $datoema['usu_email'];

                            $concaj = mysqli_query($conectar,"select suc_nombre from sucursal where suc_clave_int = '" . $clasuc . "'");
                            $datocaj = mysqli_fetch_array($concaj);
                            $nomsuc = $datocaj['suc_nombre'];

                            $conact = mysqli_query($conectar,"select usu_nombre from usuario where usu_usuario = '" . $usuario . "'");
                            $datoact = mysqli_fetch_array($conact);
                            $nomact = $datoact['usu_nombre'];

                            $mail->From = "adminpavas@pavas.com.co";
                            $mail->FromName = "Soportica";
                            $mail->AddAddress($ema, "Destino");
                            $mail->Subject = "Adjunto Lista de chequeo aire acondicionado - Sucursal Nro. " . $clasuc;

                            // Cuerpo del mensaje
                            $mail->Body .= "Hola " . $nom . "!\n\n";
                            $mail->Body .= "SUCURSALES Le informa que han adjuntado la lista de chequeo aire acondicionado.\n";
                            $mail->Body .= "SUCURSAL: " . $nomsuc . "\n";
                            $mail->Body .= "USUARIO QUE ADJUNTA: " . $nomact . "\n";
                            $mail->Body .= date("d/m/Y H:m:s") . "\n\n";
                            $mail->Body .= "Este mensaje es generado automáticamente por SUCURSALES, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Sucursales \n";

                            if (!$mail->Send()) {
                                //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                                //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                            } else {
                            }
                        }
                        echo "ok";
                    } else {
                        echo "error4";
                    }
                }
				}
				else{
				    echo "error3";
                }
			}
		}
		else
		if($actor == 'ACTUALIZAARCHIVO')
		{
			if(is_uploaded_file($_FILES['adjuntoava1']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntoava1']['tmp_name'];
				$archivo = basename($_FILES['adjuntoava1']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				$con = mysqli_query($conectar,"select * from adjunto_actor where ada_clave_int = '".$claada."'");
				$dato = mysqli_fetch_array($con);
				$adj = $dato['ada_adjunto'];
                $tia = $dato['tad_clave_int'];

                $nombrearchivo = $array_nombre[0];
                $anio = substr($nombrearchivo,0,4);
                $mes = substr($nombrearchivo,4,2);
                $dia = substr($nombrearchivo,6,2);
                $fech = $anio."-".$mes."-".$dia;

				if($tia==16) {
                    $prefijo = $anio.$mes.$dia." CONSTRUCTOR - INFORME";
                    $error = "error1";
                    $destino = "../../adjuntos/constructor/" . $clasuc . "-" . $prefijo . "" . $claada . "." . $extension;
                }
                else if($tia==39)
                {
                    $prefijo = $anio.$mes.$dia." CONSTRUCTOR - POLIZAS";
                    $error = "error2";
                    $destino = "../../adjuntos/constructor/" . $clasuc . "-" . $prefijo . "" . $claada . "." . $extension;
                }
                else if($tia==40)
                {
                    $prefijo = $anio.$mes.$dia." CONSTRUCTOR - ACTUALIZACIÓN PRESUPUESTAL";
                    $error = "error5";
                    $destino = "../../adjuntos/constructor/" . $clasuc . "-" . $prefijo . "" . $claada . "." . $extension;
                }
                else if($tia==41)
                {
                    $prefijo = $anio.$mes.$dia." CONSTRUCTOR - CONTROL CAMBIOS";
                    $error = "error6";
                    $destino = "../../adjuntos/constructor/" . $clasuc . "-" . $prefijo . "" . $claada . "." . $extension;
                }
                else if($tia==42)
                {
                    $prefijo = $anio.$mes.$dia." CONSTRUCTOR - SEGURIDAD SOCIAL";
                    $error = "error7";
                    $destino = "../../adjuntos/constructor/" . $clasuc . "-" . $prefijo . "" . $claada . "." . $extension;
                }
                else if($tia==43)
                {
                    $prefijo = $anio.$mes.$dia." CONSTRUCTOR - MANEJO AMBIENTAL";
                    $error = "error8";
                    $destino = "../../adjuntos/constructor/" . $clasuc . "-" . $prefijo . "" . $claada . "." . $extension;
                }
                else if($tia==44)
                {
                    $prefijo = $anio.$mes.$dia." CONSTRUCTOR - PLAN CALIDAD";
                    $error = "error9";
                    $destino = "../../adjuntos/constructor/" . $clasuc . "-" . $prefijo . "" . $claada . "." . $extension;
                }
                else if($tia==45)
                {
                    $prefijo = $anio.$mes.$dia." CONSTRUCTOR - ACTUALIZACION PRESUPUESTAL";
                    $error = "error10";
                    $destino = "../../adjuntos/constructor/" . $clasuc . "-" . $prefijo . "" . $claada . "." . $extension;
                }
                else if($tia==46)
                {
                    $prefijo = $anio.$mes.$dia." CONSTRUCTOR - LIQUIDACION";
                    $error = "error11";
                    $destino = "../../adjuntos/constructor/" . $clasuc . "-" . $prefijo . "" . $claada . "." . $extension;
                }
                else if($tia==47)
                {
                    $prefijo = $anio.$mes.$dia." CONSTRUCTOR - FICHA TECNICA";
                    $error = "error12";
                    $destino = "../../adjuntos/constructor/" . $clasuc . "-" . $prefijo . "" . $claada . "." . $extension;
                }
                else if($tia==48)
                {
                    $prefijo = $fecha." CONSTRUCTOR - REGISTRO FOTOGRAFICO";
                    $error = "error13";
                    $destino = "../../adjuntos/constructor/" . $clasuc . "-" . $prefijo . "" . $claada . "." . $extension;
                }
                else if($tia==49)
                {
                    $prefijo = $anio.$mes.$dia." CONSTRUCTOR - CERTIFICADO ESCOMBROS";
                    $error = "error14";
                    $destino = "../../adjuntos/constructor/" . $clasuc . "-" . $prefijo . "" . $claada . "." . $extension;
                }
                else if($tia==50)
                {
                    $prefijo = $anio.$mes.$dia." CONSTRUCTOR - CERTIFICADO RETIE";
                    $error = "error15";
                    $destino = "../../adjuntos/constructor/" . $clasuc . "-" . $prefijo . "" . $claada . "." . $extension;
                }
                else if($tia==51)
                {
                    $prefijo = $anio.$mes.$dia." CONSTRUCTOR - PRUEBA ARRANQUE";
                    $error = "error16";
                    $destino = "../../adjuntos/constructor/" . $clasuc . "-" . $prefijo . "" . $claada . "." . $extension;
                }
                else if($tia==52)
                {
                    $prefijo = $anio.$mes.$dia." CONSTRUCTOR - EVALUACION FINAL";
                    $error = "error17";
                    $destino = "../../adjuntos/constructor/" . $clasuc . "-" . $prefijo . "" . $claada . "." . $extension;
                }
                else if($tia==54)
                {
                    $prefijo = $anio.$mes.$dia." CONSTRUCTOR - EQUIPOS TECNICOS";
                    $error = "error18";
                    $destino = "../../adjuntos/constructor/" . $clasuc . "-" . $prefijo . "" . $claada . "." . $extension;
                }
                else if($tia==55)
                {
                    $prefijo = $anio.$mes.$dia." CONSTRUCTOR - LISTA CHEQUEO";
                    $error = "error19";
                    $destino = "../../adjuntos/constructor/" . $clasuc . "-" . $prefijo . "" . $claada . "." . $extension;
                }

                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and suc_clave_int = '".$clasuc."' and tad_clave_int = '".$tia."' and ada_sw_eliminado = 0 and ada_clave_int!='".$claada."'");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "errorv";
                }
                else
                if(date('Y',strtotime($fech))!=$anio ||  date('m',strtotime($fech))!=$mes ||  date('d',strtotime($fech))!=$dia)
                {
                    echo "error1";
                }
                else
                {
                    if (move_uploaded_file($sourcePath, $destino)) {
                        $sql = mysqli_query($conectar,"update adjunto_actor set ada_usu_actualiz = '" . $usuario . "',ada_fec_actualiz = '" . $fechaactual . "', ada_adjunto = '" . $destino . "', ada_nombre_adjunto = '" . $archivo . "' where ada_clave_int = '" . $claada . "'");
                     echo "ok";
                    }
                    else{
                        echo "error4";
                    }
                }
			}
			else
            {

                echo "error3";
            }
		}
	}

    sleep(3);
}
else
{

    throw new Exception("Error Processing Request", 1);

}
?>