<?php
 
/*
 * 
 * http://editablegrid.net
 *
 * Copyright (c) 2011 Webismymind SPRL
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://editablegrid.net/license
 */
      
require_once('config.php');  
error_reporting(0);
session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_SESSION['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"]; 
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");      

// Database connection                                   
$mysqli = mysqli_init();
$mysqli->options(MYSQLI_OPT_CONNECT_TIMEOUT, 5);
$mysqli->real_connect($config['db_host'],$config['db_user'],$config['db_password'],$config['db_name']); 

// Get all parameter provided by the javascript
$ano = $mysqli->real_escape_string(strip_tags(date('Y')));
//$firstname = $mysqli->real_escape_string(strip_tags($_POST['firstname']));
$tablename = $mysqli->real_escape_string(strip_tags($_POST['tablename']));
$suc = $mysqli->real_escape_string(strip_tags($_POST['suc']));
$return=false;
if ( $stmt = $mysqli->prepare("INSERT INTO ".$tablename."  (suc_clave_int,con_ano,con_usu_actualiz,con_fec_actualiz) VALUES (?,?,?,?)")) {

	$stmt->bind_param("iiss",$suc, $ano,$usuario,$fecha);
    $return = $stmt->execute();
	$stmt->close();
}             
$mysqli->close();        

echo $return ? "ok" : "error";

      

