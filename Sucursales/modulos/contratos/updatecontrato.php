<?php
 
/*
 * 
 * This file is part of EditableGrid.
 * http://editablegrid.net
 *
 * Copyright (c) 2011 Webismymind SPRL
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://editablegrid.net/license
 */
      
require_once('config.php');  
error_reporting(0);
session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_SESSION['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"]; 
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");         

// Database connection                                   
$mysqli = mysqli_init();
$mysqli->options(MYSQLI_OPT_CONNECT_TIMEOUT, 5);
$mysqli->real_connect($config['db_host'],$config['db_user'],$config['db_password'],$config['db_name']); 
                      
// Get all parameters provided by the javascript
$colname = $mysqli->real_escape_string(strip_tags($_POST['colname']));
$id = $mysqli->real_escape_string(strip_tags($_POST['id']));
$coltype = $mysqli->real_escape_string(strip_tags($_POST['coltype']));
$value = $mysqli->real_escape_string(strip_tags($_POST['newvalue']));
$tablename = $mysqli->real_escape_string(strip_tags($_POST['tablename']));
                                                
// Here, this is a little tips to manage date format before update the table
if ($coltype == 'date') {
   if ($value === "") 
  	 $value = NULL;
   else {
      $date_info = date_parse_from_format('d/m/Y', $value);
      $value = "{$date_info['year']}-{$date_info['month']}-{$date_info['day']}";
   }
}                      

// This very generic. So this script can be used to update several tables.
$return=false;
if($tablename=="contrato")
{
	if ( $stmt = $mysqli->prepare("UPDATE ".$tablename." SET ".$colname." = ?,	con_usu_actualiz='".$usuario."',con_fec_actualiz ='".$fecha."' WHERE id = ?")) 
	{
		$stmt->bind_param("si",$value, $id);
		$return = $stmt->execute();
		$stmt->close();
		
	}          
}
else if($tablename=="ordenes")
{
	if ( $stmt = $mysqli->prepare("UPDATE ".$tablename." SET ".$colname." = ?,	ord_usu_actualiz='".$usuario."',ord_fec_actualiz ='".$fecha."' WHERE id = ?")) 
	{
		$stmt->bind_param("si",$value, $id);
		$return = $stmt->execute();
		$stmt->close();
		
	}     
}
else if($tablename=="seguridad")
{
	if ( $stmt = $mysqli->prepare("UPDATE ".$tablename." SET ".$colname." = ?,	seg_usu_actualiz='".$usuario."',seg_fec_actualiz ='".$fecha."' WHERE id = ?")) 
	{
		$stmt->bind_param("si",$value, $id);
		$return = $stmt->execute();
		$stmt->close();
		
	}     
}
$mysqli->close();        

echo $return ? "ok" : "error";

      
