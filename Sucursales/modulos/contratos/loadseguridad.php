<?php     


/*
 * examples/mysql/loaddata.php
 * 
 * This file is part of EditableGrid.
 * http://editablegrid.net
 *
 * Copyright (c) 2011 Webismymind SPRL
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://editablegrid.net/license
 */
                              


/**
 * This script loads data from the database and returns it to the js
 *
 */
       
require_once('config.php');      
require_once('EditableGrid.php');            

/**
 * fetch_pairs is a simple method that transforms a mysqli_result object in an array.
 * It will be used to generate possible values for some columns.
*/
function fetch_pairs($mysqli,$query){
	if (!($res = $mysqli->query($query)))return FALSE;
	$rows = array();
	while ($row = $res->fetch_assoc()) {
		$first = true;
		$key = $value = null;
		foreach ($row as $val) {
			if ($first) { $key = $val; $first = false; }
			else { $value = $val; break; } 
		}
		$rows[$key] = $value;
	}
	return $rows;
}


// Database connection
$mysqli = mysqli_init();
$mysqli->options(MYSQLI_OPT_CONNECT_TIMEOUT, 5);
$mysqli->real_connect($config['db_host'],$config['db_user'],$config['db_password'],$config['db_name']); 
                    
// create a new EditableGrid object
$grid = new EditableGrid();
/* 
*  Add columns. The first argument of addColumn is the name of the field in the databse. 
*  The second argument is the label that will be displayed in the header
*/
$grid->addColumn('delete', 'Eliminar', 'html', NULL, false, 'id'); 
$grid->addColumn('id', 'Consec.', 'integer', NULL, false); 

$grid->addColumn('seg_ano', 'Año Contable', 'integer');  
$grid->addColumn('pro_clave_int', 'Proyecto', 'string' , fetch_pairs($mysqli,'SELECT pro_clave_int, pro_nombre FROM proyecto'),true);
$grid->addColumn('seg_fec_solicitud', 'FecSolicitud', 'date');  
$grid->addColumn('seg_fec_aprobacion', 'FecAprobacion', 'date'); 
$grid->addColumn('seg_fec_sap', 'Ing.Sap', 'date');  
$grid->addColumn('seg_desc_obra', 'Descripción Obra', 'string'); 
$grid->addColumn('seg_nom_proyecto', 'Nombre Proyecto', 'string'); 
$grid->addColumn('seg_tip_obra', 'Tip.Obra', 'string'); 
$grid->addColumn('seg_cencos', 'Cencos', 'string');
$grid->addColumn('prv_clave_int', 'Razon', 'string' , fetch_pairs($mysqli,'SELECT prv_clave_int, prv_nombre FROM proveedores'),true);
$grid->addColumn('seg_ini_om', 'Ini.OM', 'date');
$grid->addColumn('seg_fin_om', 'Fin.OM', 'date');
$grid->addColumn('seg_valor', 'Valor Sin Iva', 'integer');
$grid->addColumn('seg_num_contrato', 'N°Contrato', 'integer');  
$grid->addColumn('seg_ord_compra', 'Ord.Compra', 'integer');
$grid->addColumn('seg_nota', 'Notas', 'string'); 
$grid->addColumn('seg_fec_factura', 'FecFactura', 'date'); 
$grid->addColumn('seg_recepcionado', 'Recepcionado', 'boolean'); 
$grid->addColumn('seg_pag_factura', 'PagFactura', 'string'); 
$grid->addColumn('seg_tipo', 'Tipo', 'string');
$grid->addColumn('seg_comentario', 'Comentarios', 'string'); 
$grid->addColumn('seg_seguimiento', 'Seguimiento', 'string');    
/* The column id_country and id_continent will show a list of all available countries and continents. So, we select all rows from the tables */                                           

 

$mydb_tablename = (isset($_GET['db_tablename'])) ? stripslashes($_GET['db_tablename']) : 'seguridad';
                                                                       
$result = $mysqli->query("SELECT *, date_format(seg_fec_solicitud, '%d/%m/%Y') as seg_fec_solicitud, date_format(seg_fec_aprobacion, '%d/%m/%Y') as seg_fec_aprobacion, date_format(seg_fec_sap, '%d/%m/%Y') as seg_fec_sap,date_format(seg_ini_om, '%d/%m/%Y') as seg_ini_om, date_format(seg_fin_om, '%d/%m/%Y') as seg_fin_om, date_format(seg_fec_factura, '%d/%m/%Y') as seg_fec_factura FROM ".$mydb_tablename." WHERE suc_clave_int = '".$_GET['suc']."'");
$mysqli->close();

// send data to the browser
$grid->renderJSON($result);
?>