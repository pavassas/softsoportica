<?php
error_reporting(0);
include('../../../data/Conexion.php');
require_once('../../../Classes/PHPExcel.php');
date_default_timezone_set('America/Bogota');
session_start();
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_SESSION['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];
	
// verifica si no se ha loggeado
if(!isset($_SESSION["persona"]))
{
  session_destroy();
  header("LOCATION:index.php");
}else{
}

$d1 = $_GET['d1'];
$d2 = $_GET['d2'];
$d3 = $_GET['d3'];
$d4 = $_GET['d4'];
$d5 = $_GET['d5'];
$d6 = $_GET['d6'];
$d7 = $_GET['d7'];
$d8 = $_GET['d8'];
$d9 = $_GET['d9'];
$d10 = $_GET['d10'];
$d11 = $_GET['d11'];
$d12 = $_GET['d12'];
$d13 = $_GET['d13'];
$d14 = $_GET['d14'];
$d15 = $_GET['d15'];
$d16 = $_GET['d16'];
$d17 = $_GET['d17'];
$d18 = $_GET['d18'];
$d19 = $_GET['d19'];
$d20 = $_GET['d20'];
$d21 = $_GET['d21'];
$d22 = $_GET['d22'];
$d23 = $_GET['d23'];
$d24 = $_GET['d24'];
$d25 = $_GET['d25'];
$d26 = $_GET['d26'];
$d27 = $_GET['d27'];
$d28 = $_GET['d28'];
$d29 = $_GET['d29'];
$d30 = $_GET['d30'];
$d31 = $_GET['d31'];
$d32 = $_GET['d32'];
$d33 = $_GET['d33'];
$d34 = $_GET['d34'];
$d35 = $_GET['d35'];
$d36 = $_GET['d36'];
$d37 = $_GET['d37'];
$d38 = $_GET['d38'];
$d39 = $_GET['d39'];
$d40 = $_GET['d40'];
$d41 = $_GET['d41'];
$d42 = $_GET['d42'];
$d43 = $_GET['d43'];
$d44 = $_GET['d44'];
$d45 = $_GET['d45'];
$d46 = $_GET['d46'];
$d47 = $_GET['d47'];
$d48 = $_GET['d48'];
$d49 = $_GET['d49'];
$d50 = $_GET['d50'];
$d51 = $_GET['d51'];
$d52 = $_GET['d52'];
$d53 = $_GET['d53'];
$d54 = $_GET['d54'];
$d55 = $_GET['d55'];
$d56 = $_GET['d56'];
$d57 = $_GET['d57'];
$d58 = $_GET['d58'];
$d59 = $_GET['d59'];
$d60 = $_GET['d60'];
$d61 = $_GET['d61'];

$fecha=date("d/m/Y");
$fechaact=date("Y/m/d H:i:s");
//************ESTILOS******************

$styleA1 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => '000000'),
    'size'  => 12,
    'name'  => 'Calibri'
));
$styleA2 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => 'C83000'),
    'size'  => 10,
    'name'  => 'Arial'
));
$styleA3 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => '000000'),
    'size'  => 10,
    'name'  => 'Arial'
));
$styleA4 = array(
'font'  => array(
    'bold'  => false,
    'color' => array('rgb' => '000000'),
    'size'  => 10,
    'name'  => 'Arial'
));
$styleA3p1 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => '000000'),
    'size'  => 9,
    'name'  => 'Arial'
));
$styleA4p1 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => '000000'),
    'size'  => 9,
    'name'  => 'Arial'
));

$borders = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '000000'),
		)
	),
);

//*************************************

$objPHPExcel = new PHPExcel();
$archivo = 'PAVAS - Informe Planoteca.xls';

//Propiedades de la hoja de excel
$objPHPExcel->getProperties()
		->setCreator("PAVAS TECNOLOGIA")
		->setLastModifiedBy("PAVAS TECNOLOGIA")
		->setTitle("Informe Planoteca")
		->setSubject("Informe Planoteca")
		->setDescription("Documento generado con el software Sucursales")
		->setKeywords("Planoteca")
		->setCategory("Reportes");
		
//Ancho de las Columnas
//Info basica
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
//Info secundaria
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(23);
//Inmobiliaria
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(20);
//Visita
$objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setWidth(20);
//Diseño
$objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AN')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AO')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AP')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AQ')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AR')->setWidth(20);
//Licencia
$objPHPExcel->getActiveSheet()->getColumnDimension('AS')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AT')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AU')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AV')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AW')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AX')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AY')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AZ')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BA')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BB')->setWidth(20);
//INTERVENTORIA
$objPHPExcel->getActiveSheet()->getColumnDimension('BC')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BD')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BE')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BF')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BG')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BH')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BI')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BJ')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BK')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BL')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BM')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BN')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BO')->setWidth(20);
//CONSTRUCTOR
$objPHPExcel->getActiveSheet()->getColumnDimension('BP')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BQ')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BR')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BS')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BT')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BU')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BV')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BW')->setWidth(20);
//SEGURIDAD
$objPHPExcel->getActiveSheet()->getColumnDimension('BX')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BY')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BZ')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('CA')->setWidth(20);
//FACTURAZION
$objPHPExcel->getActiveSheet()->getColumnDimension('CB')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CC')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CD')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CE')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CF')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CG')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CH')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CI')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CJ')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CK')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CL')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CM')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CN')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CO')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CP')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CQ')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CR')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CS')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CT')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CU')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CV')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CW')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CX')->setWidth(30);

//************A1**************
$objPHPExcel->getActiveSheet()->getStyle('A1')-> applyFromArray($styleA1);//
$objPHPExcel->getActiveSheet()->getCell('A1')->setValue("LISTA DE PLANOTECAS");
$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setWrapText(true); //Crea un enter entre palabras
$objPHPExcel->getActiveSheet()->mergeCells('A1:BI1');//Conbinar celdas
$objPHPExcel->getActiveSheet()->getStyle('A1:BI1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('00D8D8D8');//COLOR DE FONDO
//****************************

/**************** COLUMNAS ****************/

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('A2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('A2')->setValue("Planoteca");
$objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('B2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('B2')->setValue("Tipología");
$objPHPExcel->getActiveSheet()->getStyle('B2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('C2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('C2')->setValue("Código");
$objPHPExcel->getActiveSheet()->getStyle('C2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('D2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('D2')->setValue("Nombre");
$objPHPExcel->getActiveSheet()->getStyle('D2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('E2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('E2')->setValue("Dirección");
$objPHPExcel->getActiveSheet()->getStyle('E2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('F2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('F2')->setValue("Departamento");
$objPHPExcel->getActiveSheet()->getStyle('F2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('G2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('G2')->setValue("Municipio");
$objPHPExcel->getActiveSheet()->getStyle('G2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('H2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('H2')->setValue("Región");
$objPHPExcel->getActiveSheet()->getStyle('H2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('I2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('I2')->setValue("Propiedad");
$objPHPExcel->getActiveSheet()->getStyle('I2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('J2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('J2')->setValue("Estado");
$objPHPExcel->getActiveSheet()->getStyle('J2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('K2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('K2')->setValue("Cant. pisos");
$objPHPExcel->getActiveSheet()->getStyle('K2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('L2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('L2')->setValue("Planos");
$objPHPExcel->getActiveSheet()->getStyle('L2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('M2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('M2')->setValue("Año interv.");
$objPHPExcel->getActiveSheet()->getStyle('M2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('N2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('N2')->setValue("Apertura");
$objPHPExcel->getActiveSheet()->getStyle('N2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('O2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('O2')->setValue("Área lote");
$objPHPExcel->getActiveSheet()->getStyle('O2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('P2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('P2')->setValue("Sótano");
$objPHPExcel->getActiveSheet()->getStyle('P2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('Q2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('Q2')->setValue("Parqueadero");
$objPHPExcel->getActiveSheet()->getStyle('Q2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('R2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('R2')->setValue("Piso 1");
$objPHPExcel->getActiveSheet()->getStyle('R2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('S2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('S2')->setValue("Piso 2");
$objPHPExcel->getActiveSheet()->getStyle('S2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('T2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('T2')->setValue("Piso 3");
$objPHPExcel->getActiveSheet()->getStyle('T2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('U2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('U2')->setValue("Piso 4");
$objPHPExcel->getActiveSheet()->getStyle('U2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('V2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('V2')->setValue("Piso 5");
$objPHPExcel->getActiveSheet()->getStyle('V2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('W2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('W2')->setValue("Piso 6");
$objPHPExcel->getActiveSheet()->getStyle('W2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('X2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('X2')->setValue("Piso 7");
$objPHPExcel->getActiveSheet()->getStyle('X2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('Y2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('Y2')->setValue("Piso 8");
$objPHPExcel->getActiveSheet()->getStyle('Y2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('Z2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('Z2')->setValue("Piso 9");
$objPHPExcel->getActiveSheet()->getStyle('Z2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AA2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AA2')->setValue("Piso 10");
$objPHPExcel->getActiveSheet()->getStyle('AA2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AB2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AB2')->setValue("Piso 11");
$objPHPExcel->getActiveSheet()->getStyle('AB2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AC2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AC2')->setValue("Piso 12");
$objPHPExcel->getActiveSheet()->getStyle('AC2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AD2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AD2')->setValue("Piso 13");
$objPHPExcel->getActiveSheet()->getStyle('AD2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AE2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AE2')->setValue("Piso 14");
$objPHPExcel->getActiveSheet()->getStyle('AE2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AF2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AF2')->setValue("Piso 15");
$objPHPExcel->getActiveSheet()->getStyle('AF2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AG2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AG2')->setValue("Piso 16");
$objPHPExcel->getActiveSheet()->getStyle('AG2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AH2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AH2')->setValue("Piso 17");
$objPHPExcel->getActiveSheet()->getStyle('AH2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AI2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AI2')->setValue("Piso 18");
$objPHPExcel->getActiveSheet()->getStyle('AI2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AJ2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AJ2')->setValue("Piso 19");
$objPHPExcel->getActiveSheet()->getStyle('AJ2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AK2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AK2')->setValue("Piso 20");
$objPHPExcel->getActiveSheet()->getStyle('AK2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AL2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AL2')->setValue("Piso 21");
$objPHPExcel->getActiveSheet()->getStyle('AL2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AM2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AM2')->setValue("Piso 22");
$objPHPExcel->getActiveSheet()->getStyle('AM2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AN2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AN2')->setValue("Piso 23");
$objPHPExcel->getActiveSheet()->getStyle('AN2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AO2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AO2')->setValue("Piso 24");
$objPHPExcel->getActiveSheet()->getStyle('AO2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AP2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AP2')->setValue("Piso 25");
$objPHPExcel->getActiveSheet()->getStyle('AP2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AQ2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AQ2')->setValue("Otros");
$objPHPExcel->getActiveSheet()->getStyle('AQ2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AR2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AR2')->setValue("Área total");
$objPHPExcel->getActiveSheet()->getStyle('AR2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AS2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AS2')->setValue("Actualizados");
$objPHPExcel->getActiveSheet()->getStyle('AS2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AT2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AT2')->setValue("Notas");
$objPHPExcel->getActiveSheet()->getStyle('AT2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AU2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AU2')->setValue("Nº Puestos");
$objPHPExcel->getActiveSheet()->getStyle('AU2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AV2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AV2')->setValue("Banco");
$objPHPExcel->getActiveSheet()->getStyle('AV2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AW2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AW2')->setValue("Enlace Opr.");
$objPHPExcel->getActiveSheet()->getStyle('AW2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AX2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AX2')->setValue("Valores");
$objPHPExcel->getActiveSheet()->getStyle('AX2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AY2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AY2')->setValue("Fiduciaria");
$objPHPExcel->getActiveSheet()->getStyle('AY2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AZ2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AZ2')->setValue("Leasing");
$objPHPExcel->getActiveSheet()->getStyle('AZ2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BA2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BA2')->setValue("Factoring");
$objPHPExcel->getActiveSheet()->getStyle('BA2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BB2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BB2')->setValue("Sufi");
$objPHPExcel->getActiveSheet()->getStyle('BB2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BC2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BC2')->setValue("Mon. Extranj.");
$objPHPExcel->getActiveSheet()->getStyle('BC2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BD2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BD2')->setValue("Nº Cajeros");
$objPHPExcel->getActiveSheet()->getStyle('BD2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BE2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BE2')->setValue("Banca Invers.");
$objPHPExcel->getActiveSheet()->getStyle('BE2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BF2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BF2')->setValue("Licencia const.");
$objPHPExcel->getActiveSheet()->getStyle('BF2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BG2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BG2')->setValue("Tipo licencia");
$objPHPExcel->getActiveSheet()->getStyle('BG2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BH2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BH2')->setValue("Notas licencia");
$objPHPExcel->getActiveSheet()->getStyle('BH2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BI2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BI2')->setValue("Registro aviso");
$objPHPExcel->getActiveSheet()->getStyle('BI2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

$con = mysqli_query($conectar,"select * from planoteca_edificios where (ple_planoteca LIKE REPLACE('%".$d1."%',' ','%') OR '".$d1."' IS NULL OR '".$d1."' = '') and (tip_clave_int LIKE REPLACE('%".$d2."%',' ','%') OR '".$d2."' IS NULL OR '".$d2."' = '') and (ple_codigo LIKE REPLACE('%".$d3."%',' ','%') OR '".$d3."' IS NULL OR '".$d3."' = '') and (ple_nombre LIKE REPLACE('%".$d4."%',' ','%') OR '".$d4."' IS NULL OR '".$d4."' = '') and (ple_direccion LIKE REPLACE('%".$d5."%',' ','%') OR '".$d5."' IS NULL OR '".$d5."' = '') and (ple_departamento LIKE REPLACE('%".$d6."%',' ','%') OR '".$d6."' IS NULL OR '".$d6."' = '') and (ple_municipio LIKE REPLACE('%".$d7."%',' ','%') OR '".$d7."' IS NULL OR '".$d7."' = '') and (ple_region LIKE REPLACE('%".$d8."%',' ','%') OR '".$d8."' IS NULL OR '".$d8."' = '') and (pro_clave_int LIKE REPLACE('%".$d9."%',' ','%') OR '".$d9."' IS NULL OR '".$d9."' = '') and (esp_clave_int LIKE REPLACE('%".$d10."%',' ','%') OR '".$d10."' IS NULL OR '".$d10."' = '') and (ple_cantidadpisos LIKE REPLACE('%".$d11."%',' ','%') OR '".$d11."' IS NULL OR '".$d11."' = '') and (ple_planos LIKE REPLACE('%".$d12."%',' ','%') OR '".$d12."' IS NULL OR '".$d12."' = '') and (ple_anointerv LIKE REPLACE('%".$d13."%',' ','%') OR '".$d13."' IS NULL OR '".$d13."' = '') and (ple_apertura LIKE REPLACE('%".$d14."%',' ','%') OR '".$d14."' IS NULL OR '".$d14."' = '') and (ple_arealote LIKE REPLACE('%".$d15."%',' ','%') OR '".$d15."' IS NULL OR '".$d15."' = '') and (ple_sotano LIKE REPLACE('%".$d16."%',' ','%') OR '".$d16."' IS NULL OR '".$d16."' = '') and (ple_parqueadero LIKE REPLACE('%".$d17."%',' ','%') OR '".$d17."' IS NULL OR '".$d17."' = '') and (ple_piso1 LIKE REPLACE('%".$d18."%',' ','%') OR '".$d18."' IS NULL OR '".$d18."' = '') and (ple_piso2 LIKE REPLACE('%".$d19."%',' ','%') OR '".$d19."' IS NULL OR '".$d19."' = '') and (ple_piso3 LIKE REPLACE('%".$d20."%',' ','%') OR '".$d20."' IS NULL OR '".$d20."' = '') and (ple_piso4 LIKE REPLACE('%".$d21."%',' ','%') OR '".$d21."' IS NULL OR '".$d21."' = '') and (ple_piso5 LIKE REPLACE('%".$d22."%',' ','%') OR '".$d22."' IS NULL OR '".$d22."' = '') and (ple_piso6 LIKE REPLACE('%".$d23."%',' ','%') OR '".$d23."' IS NULL OR '".$d23."' = '') and (ple_piso7 LIKE REPLACE('%".$d24."%',' ','%') OR '".$d24."' IS NULL OR '".$d24."' = '') and (ple_piso8 LIKE REPLACE('%".$d25."%',' ','%') OR '".$d25."' IS NULL OR '".$d25."' = '') and (ple_piso9 LIKE REPLACE('%".$d26."%',' ','%') OR '".$d26."' IS NULL OR '".$d26."' = '') and (ple_piso10 LIKE REPLACE('%".$d27."%',' ','%') OR '".$d27."' IS NULL OR '".$d27."' = '') and (ple_piso11 LIKE REPLACE('%".$d28."%',' ','%') OR '".$d28."' IS NULL OR '".$d28."' = '') and (ple_piso12 LIKE REPLACE('%".$d29."%',' ','%') OR '".$d29."' IS NULL OR '".$d29."' = '') and (ple_piso13 LIKE REPLACE('%".$d30."%',' ','%') OR '".$d30."' IS NULL OR '".$d30."' = '') and (ple_piso14 LIKE REPLACE('%".$d31."%',' ','%') OR '".$d31."' IS NULL OR '".$d31."' = '') and (ple_piso15 LIKE REPLACE('%".$d32."%',' ','%') OR '".$d32."' IS NULL OR '".$d32."' = '') and (ple_piso16 LIKE REPLACE('%".$d33."%',' ','%') OR '".$d33."' IS NULL OR '".$d33."' = '') and (ple_piso17 LIKE REPLACE('%".$d34."%',' ','%') OR '".$d34."' IS NULL OR '".$d34."' = '') and (ple_piso18 LIKE REPLACE('%".$d35."%',' ','%') OR '".$d35."' IS NULL OR '".$d35."' = '') and (ple_piso19 LIKE REPLACE('%".$d36."%',' ','%') OR '".$d36."' IS NULL OR '".$d36."' = '') and (ple_piso20 LIKE REPLACE('%".$d37."%',' ','%') OR '".$d37."' IS NULL OR '".$d37."' = '') and (ple_piso21 LIKE REPLACE('%".$d38."%',' ','%') OR '".$d38."' IS NULL OR '".$d38."' = '') and (ple_piso22 LIKE REPLACE('%".$d39."%',' ','%') OR '".$d39."' IS NULL OR '".$d39."' = '') and (ple_piso23 LIKE REPLACE('%".$d40."%',' ','%') OR '".$d40."' IS NULL OR '".$d40."' = '') and (ple_piso24 LIKE REPLACE('%".$d41."%',' ','%') OR '".$d41."' IS NULL OR '".$d41."' = '') and (ple_piso25 LIKE REPLACE('%".$d42."%',' ','%') OR '".$d42."' IS NULL OR '".$d42."' = '') and (ple_otros LIKE REPLACE('%".$d43."%',' ','%') OR '".$d43."' IS NULL OR '".$d43."' = '') and (ple_areatotal LIKE REPLACE('%".$d44."%',' ','%') OR '".$d44."' IS NULL OR '".$d44."' = '') and (ple_actualizados LIKE REPLACE('%".$d45."%',' ','%') OR '".$d45."' IS NULL OR '".$d45."' = '') and (ple_notasplanoteca LIKE REPLACE('%".$d46."%',' ','%') OR '".$d46."' IS NULL OR '".$d46."' = '') and (ple_numpuestos LIKE REPLACE('%".$d47."%',' ','%') OR '".$d47."' IS NULL OR '".$d47."' = '') and (ple_banco LIKE REPLACE('%".$d48."%',' ','%') OR '".$d48."' IS NULL OR '".$d48."' = '') and (ple_enlaceopr LIKE REPLACE('%".$d49."%',' ','%') OR '".$d49."' IS NULL OR '".$d49."' = '') and (ple_valores LIKE REPLACE('%".$d50."%',' ','%') OR '".$d50."' IS NULL OR '".$d50."' = '') and (ple_fiduciaria LIKE REPLACE('%".$d51."%',' ','%') OR '".$d51."' IS NULL OR '".$d51."' = '') and (ple_leasing LIKE REPLACE('%".$d52."%',' ','%') OR '".$d52."' IS NULL OR '".$d52."' = '') and (ple_factoring LIKE REPLACE('%".$d53."%',' ','%') OR '".$d53."' IS NULL OR '".$d53."' = '') and (ple_sufi LIKE REPLACE('%".$d54."%',' ','%') OR '".$d54."' IS NULL OR '".$d54."' = '') and (ple_monextranj LIKE REPLACE('%".$d55."%',' ','%') OR '".$d55."' IS NULL OR '".$d55."' = '') and (ple_numcajeros LIKE REPLACE('%".$d56."%',' ','%') OR '".$d56."' IS NULL OR '".$d56."' = '') and (ple_bancainvers LIKE REPLACE('%".$d57."%',' ','%') OR '".$d57."' IS NULL OR '".$d57."' = '') and (ple_licenciaconst LIKE REPLACE('%".$d58."%',' ','%') OR '".$d58."' IS NULL OR '".$d58."' = '') and (til_clave_int LIKE REPLACE('%".$d59."%',' ','%') OR '".$d59."' IS NULL OR '".$d59."' = '') and (ple_notaslicencia LIKE REPLACE('%".$d60."%',' ','%') OR '".$d60."' IS NULL OR '".$d60."' = '') and (ple_registroaviso LIKE REPLACE('%".$d61."%',' ','%') OR '".$d61."' IS NULL OR '".$d61."' = '') and ple_sw_eliminado = 0");
$num = mysqli_num_rows($con);

$j = 3;
for($i = 0; $i < $num; $i++)
{
	$dato = mysqli_fetch_array($con);
	$cla = $dato['ple_clave_int'];
	$d1 = $dato['ple_planoteca'];
	
	$d2 = $dato['tip_clave_int'];
	$contip = mysqli_query($conectar,"select * from tipologia where tip_clave_int = '".$d2."'");
	$datotip = mysqli_fetch_array($contip);
	$d2 = $datotip['tip_nombre'];
	
	$d3 = $dato['ple_codigo'];
	$d4 = $dato['ple_nombre'];
	$d5 = $dato['ple_direccion'];
	
	$d6 = $dato['ple_departamento'];
	$sql = mysqli_query($conectar,"select pog_nombre from posicion_geografica where pog_clave_int = '".$d6."'");
	$datosql = mysqli_fetch_array($sql);
	$d6 = $datosql['pog_nombre'];
	
	$d7 = $dato['ple_municipio'];
	$sql = mysqli_query($conectar,"select pog_nombre from posicion_geografica where pog_clave_int = '".$d7."'");
	$datosql = mysqli_fetch_array($sql);
	$d7 = $datosql['pog_nombre'];
	
	$d8 = $dato['ple_region'];
	$sql = mysqli_query($conectar,"select pog_nombre from posicion_geografica where pog_clave_int = '".$d8."'");
	$datosql = mysqli_fetch_array($sql);
	$d8 = $datosql['pog_nombre'];
	
	$d9 = $dato['pro_clave_int'];
	$conpro = mysqli_query($conectar,"select * from propiedad where pro_clave_int = '".$d9."'");
	$datopro = mysqli_fetch_array($conpro);
	$d9 = $datopro['pro_nombre'];
	
	$d10 = $dato['esp_clave_int'];
	$conest = mysqli_query($conectar,"select * from estado_planoteca where esp_clave_int = '".$d10."'");
	$datoest = mysqli_fetch_array($conest);
	$d10 = $datoest['esp_nombre'];
	
	$d11 = $dato['ple_cantidadpisos'];
	$d12 = $dato['ple_planos'];
	$d13 = $dato['ple_anointerv'];
	$d14 = $dato['ple_apertura'];
	$d15 = $dato['ple_arealote'];
	$d16 = $dato['ple_sotano'];
	$d17 = $dato['ple_parqueadero'];
	$d18 = $dato['ple_piso1'];
	$d19 = $dato['ple_piso2'];
	$d20 = $dato['ple_piso3'];
	$d21 = $dato['ple_piso4'];
	$d22 = $dato['ple_piso5'];
	$d23 = $dato['ple_piso6'];
	$d24 = $dato['ple_piso7'];
	$d25 = $dato['ple_piso8'];
	$d26 = $dato['ple_piso9'];
	$d27 = $dato['ple_piso10'];
	$d28 = $dato['ple_piso11'];
	$d29 = $dato['ple_piso12'];
	$d30 = $dato['ple_piso13'];
	$d31 = $dato['ple_piso14'];
	$d32 = $dato['ple_piso15'];
	$d33 = $dato['ple_piso16'];
	$d34 = $dato['ple_piso17'];
	$d35 = $dato['ple_piso18'];
	$d36 = $dato['ple_piso19'];
	$d37 = $dato['ple_piso20'];
	$d38 = $dato['ple_piso21'];
	$d39 = $dato['ple_piso22'];
	$d40 = $dato['ple_piso23'];
	$d41 = $dato['ple_piso24'];
	$d42 = $dato['ple_piso25'];
	$d43 = $dato['ple_otros'];
	$d44 = $dato['ple_areatotal'];
	$d45 = $dato['ple_actualizados'];
	if($d45 == 1){ $d45 = "SI"; }elseif($d45 == 2){ $d45 = "NO"; }
	$d46 = $dato['ple_notasplanoteca'];
	$d47 = $dato['ple_numpuestos'];
	$d48 = $dato['ple_banco'];
	$d49 = $dato['ple_enlaceopr'];
	$d50 = $dato['ple_valores'];
	$d51 = $dato['ple_fiduciaria'];
	$d52 = $dato['ple_leasing'];
	$d53 = $dato['ple_factoring'];
	$d54 = $dato['ple_sufi'];
	$d55 = $dato['ple_monextranj'];
	$d56 = $dato['ple_numcajeros'];
	$d57 = $dato['ple_bancainvers'];
	$d58 = $dato['ple_licenciaconst'];
	if($d58 == 1){ $d58 = "SI"; }elseif($d58 == 2){ $d58 = "NO"; }
	$d59 = $dato['til_clave_int'];
	$d60 = $dato['ple_notaslicencia'];
	$d61 = $dato['ple_registroaviso'];
		
	/**************** DATOS DE LAS COLUMNAS ****************/ 

	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('A'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("A".$j, $d1);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('B'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("B".$j, $d2);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('C'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("C".$j, $d3);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('D'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("D".$j, $d4);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('E'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("E".$j, $d5);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('F'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("F".$j, $d6);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('G'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("G".$j, $d7);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('H'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("H".$j, $d8);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('I'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("I".$j, $d9);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('J'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("J".$j, $d10);
	//***************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('K'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("K".$j, $d11);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('L'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("L".$j, $d12);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('M'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("M".$j, $d13);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('N'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("N".$j, $d14);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('O'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("O".$j, $d15);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('P'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("P".$j, $d16);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('Q'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("Q".$j, $d17);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('R'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("R".$j, $d18);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('S'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("S".$j, $d19);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('T'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("T".$j, $d20);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('U'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("U".$j, $d21);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('V'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("V".$j, $d22);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('W'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("W".$j, $d23);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('X'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("X".$j, $d24);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('Y'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("Y".$j, $d25);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('Z'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("Z".$j, $d26);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AA'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AA".$j, $d27);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AB'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AB".$j, $d28);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AC'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AC".$j, $d29);
	//****************************

	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AD'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AD".$j, $d30);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AE'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AE".$j, $d31);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AF'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AF".$j, $d32);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AG'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AG".$j, $d33);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AH'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AH".$j, $d34);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AI'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AI".$j, $d35);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AJ'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AJ".$j, $d36);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AK'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AK".$j, $d37);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AL'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AL".$j, $d38);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AM'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AM".$j, $d39);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AN'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AN".$j, $d40);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AO'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AO".$j, $d41);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AP'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AP".$j, $d42);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AQ'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AQ".$j, $d43);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AR'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AR".$j, $d44);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AS'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AS".$j, $d45);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AT'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AT".$j, $d46);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AU'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AU".$j, $d47);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AV'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AV".$j, $d48);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AW'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AW".$j, $d49);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AX'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AX".$j, $d50);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AY'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AY".$j, $d51);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AZ'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AZ".$j, $d52);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BA'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BA".$j, $d53);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BB'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BB".$j, $d54);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BC'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BC".$j, $d55);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BD'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BD".$j, $d56);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BE'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BE".$j, $d57);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BF'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BF".$j, $d58);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BG'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BG".$j, $d59);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BH'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BH".$j, $d60);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BI'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BI".$j, $d61);
	//****************************
	
	$j++;
}
//DATOS DE SALIDA DEL EXCEL
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$archivo.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
?>