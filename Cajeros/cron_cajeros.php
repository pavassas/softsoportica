<?php
session_start();
error_reporting(0);
include('Cajeros/data/Conexion.php');
require("Cajeros/Classes/PHPMailer-master/class.phpmailer.php");
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");

$con = mysqli_query($conectar,"select c.caj_clave_int,cai_fecha_ini_inmobiliaria,cai_fecha_entrega_info_inmobiliaria from cajero c inner join modalidad m on (m.mod_clave_int = c.mod_clave_int) inner join cajero_inmobiliaria ci on (ci.caj_clave_int = c.caj_clave_int) where cai_inmobiliaria <> 0 and cai_inmobiliaria <> '' and cai_fecha_ini_inmobiliaria <> '0000-00-00' and ((cai_fecha_entrega_info_inmobiliaria = '0000-00-00' and (CURDATE() >= ADDDATE(cai_fecha_ini_inmobiliaria, INTERVAL mod_dias_inmobiliaria DAY)))) and c.caj_estado_proyecto in (1,5) and c.caj_sw_eliminado = 0 and c.caj_fecha_creacion >= '2015-01-01 00:00:00' and c.caj_clave_int not in (select alerta.caj_clave_int from alerta where c.caj_clave_int = alerta.caj_clave_int and tal_clave_int = 7 and ale_sw_inactivo = 0)");
	$num = mysqli_num_rows($con);
	for($i = 0; $i < $num; $i++)
	{
		$dato = mysqli_fetch_array($con);
		$caj = $dato['caj_clave_int'];
		$fi = $dato['cai_fecha_ini_inmobiliaria'];
		$fe = $dato['cai_fecha_entrega_info_inmobiliaria'];
		
		mysqli_query($conectar,"insert into alerta(ale_clave_int,caj_clave_int,tal_clave_int,ale_fecha_inicio,ale_fecha_entrega,ale_sw_enviado,ale_usu_actualiz,ale_fec_actualiz,ale_sw_inactivo) values(null,'".$caj."',7,'".$fi."','".$fe."',0,'".$usuario."','".$fecha."')");
	}
	
	//VISITA
	$con = mysqli_query($conectar,"select c.caj_clave_int,cav_fecha_visita,cav_fecha_entrega_informe from cajero c inner join modalidad m on (m.mod_clave_int = c.mod_clave_int) inner join cajero_visita cv on (cv.caj_clave_int = c.caj_clave_int) where cav_visitante <> 0 and cav_visitante <> '' and cav_fecha_visita <> '0000-00-00' and ((cav_fecha_entrega_informe = '0000-00-00' and (CURDATE() >= ADDDATE(cav_fecha_visita, INTERVAL mod_dias_visita DAY)))) and c.caj_estado_proyecto in (1,5) and c.caj_sw_eliminado = 0 and c.caj_fecha_creacion >= '2015-01-01 00:00:00' and c.caj_clave_int not in (select alerta.caj_clave_int from alerta where c.caj_clave_int = alerta.caj_clave_int and tal_clave_int = 8 and ale_sw_inactivo = 0)");
	$num = mysqli_num_rows($con);
	for($i = 0; $i < $num; $i++)
	{
		$dato = mysqli_fetch_array($con);
		$caj = $dato['caj_clave_int'];
		$fi = $dato['cav_fecha_visita'];
		$fe = $dato['cav_fecha_entrega_informe'];
		
		mysqli_query($conectar,"insert into alerta(ale_clave_int,caj_clave_int,tal_clave_int,ale_fecha_inicio,ale_fecha_entrega,ale_sw_enviado,ale_usu_actualiz,ale_fec_actualiz,ale_sw_inactivo) values(null,'".$caj."',8,'".$fi."','".$fe."',0,'".$usuario."','".$fecha."','0')");
	}
	
	//DISEï¿½O
	$con = mysqli_query($conectar,"select c.caj_clave_int,cad_fecha_inicio_diseno,cad_fecha_entrega_info_diseno from cajero c inner join modalidad m on (m.mod_clave_int = c.mod_clave_int) inner join cajero_diseno cd on (cd.caj_clave_int = c.caj_clave_int) where cad_disenador <> 0 and cad_disenador <> '' and cad_fecha_inicio_diseno <> '0000-00-00' and ((cad_fecha_entrega_info_diseno = '0000-00-00' and (CURDATE() >= ADDDATE(cad_fecha_inicio_diseno, INTERVAL mod_dias_diseno DAY)))) and c.caj_estado_proyecto in (1,5) and c.caj_sw_eliminado = 0 and c.caj_fecha_creacion >= '2015-01-01 00:00:00' and c.caj_clave_int not in (select alerta.caj_clave_int from alerta where c.caj_clave_int = alerta.caj_clave_int and tal_clave_int = 9 and ale_sw_inactivo = 0)");
	$num = mysqli_num_rows($con);
	for($i = 0; $i < $num; $i++)
	{
		$dato = mysqli_fetch_array($con);
		$caj = $dato['caj_clave_int'];
		$fi = $dato['cad_fecha_inicio_diseno'];
		$fe = $dato['cad_fecha_entrega_info_diseno'];
		
		mysqli_query($conectar,"insert into alerta(ale_clave_int,caj_clave_int,tal_clave_int,ale_fecha_inicio,ale_fecha_entrega,ale_sw_enviado,ale_usu_actualiz,ale_fec_actualiz,ale_sw_inactivo) values(null,'".$caj."',9,'".$fi."','".$fe."',0,'".$usuario."','".$fecha."','0')");
	}
	
	//LICENCIA
	$con = mysqli_query($conectar,"select c.caj_clave_int,cal_fecha_inicio_licencia,cal_fecha_entrega_info_licencia from cajero c inner join modalidad m on (m.mod_clave_int = c.mod_clave_int) inner join cajero_licencia cl on (cl.caj_clave_int = c.caj_clave_int) where cal_gestionador <> 0 and cal_gestionador <> '' and cal_fecha_inicio_licencia <> '0000-00-00' and ((cal_fecha_entrega_info_licencia = '0000-00-00' and (CURDATE() >= ADDDATE(cal_fecha_inicio_licencia, INTERVAL mod_dias_licencia DAY)))) and c.caj_estado_proyecto in (1,5) and c.caj_sw_eliminado = 0 and c.caj_fecha_creacion >= '2015-01-01 00:00:00' and c.caj_clave_int not in (select alerta.caj_clave_int from alerta where c.caj_clave_int = alerta.caj_clave_int and tal_clave_int = 10 and ale_sw_inactivo = 0)");
	$num = mysqli_num_rows($con);
	for($i = 0; $i < $num; $i++)
	{
		$dato = mysqli_fetch_array($con);
		$caj = $dato['caj_clave_int'];
		$fi = $dato['cal_fecha_inicio_licencia'];
		$fe = $dato['cal_fecha_entrega_info_licencia'];
		
		mysqli_query($conectar,"insert into alerta(ale_clave_int,caj_clave_int,tal_clave_int,ale_fecha_inicio,ale_fecha_entrega,ale_sw_enviado,ale_usu_actualiz,ale_fec_actualiz,ale_sw_inactivo) values(null,'".$caj."',10,'".$fi."','".$fe."',0,'".$usuario."','".$fecha."','0')");
	}
	
	//CONSTRUCTOR
	$con = mysqli_query($conectar,"select c.caj_clave_int,ci.cin_fecha_inicio_obra,ci.cin_fecha_teorica_entrega,cc.cac_fecha_entrega_atm from cajero c inner join cajero_interventoria ci on (ci.caj_clave_int = c.caj_clave_int) inner join cajero_constructor cc on (cc.caj_clave_int = c.caj_clave_int) where ci.cin_fecha_teorica_entrega <> '' and ci.cin_fecha_teorica_entrega <> '0000-00-00' and ((cc.cac_fecha_entrega_atm = '0000-00-00' AND DATEDIFF(CURDATE(),ci.cin_fecha_teorica_entrega) = -1)) and c.caj_estado_proyecto in (1,5) and c.caj_sw_eliminado = 0 and c.caj_fecha_creacion >= '2015-01-01 00:00:00' and c.caj_clave_int not in (select alerta.caj_clave_int from alerta where c.caj_clave_int = alerta.caj_clave_int and tal_clave_int = 11 and ale_sw_inactivo = 0)");
	$num = mysqli_num_rows($con);
	for($i = 0; $i < $num; $i++)
	{
		$dato = mysqli_fetch_array($con);
		$caj = $dato['caj_clave_int'];
		$fi = $dato['cin_fecha_inicio_obra'];
		$fte = $dato['cin_fecha_teorica_entrega'];
		$fe = $dato['cac_fecha_entrega_atm'];
		
		mysqli_query($conectar,"insert into alerta(ale_clave_int,caj_clave_int,tal_clave_int,ale_fecha_inicio,ale_fecha_entrega,ale_fecha_entrega_cajero,ale_sw_enviado,ale_usu_actualiz,ale_fec_actualiz,ale_sw_inactivo) values(null,'".$caj."',11,'".$fi."','".$fte."','".$fe."',0,'".$usuario."','".$fecha."','0')");
	}
	
	//Algoritmo para alertas de fechas de incumplimiento por parte de los actores
	
	//INMOBILIARIA
	$con = mysqli_query($conectar,"select c.caj_clave_int,cai_fecha_ini_inmobiliaria,cai_fecha_entrega_info_inmobiliaria from cajero c inner join modalidad m on (m.mod_clave_int = c.mod_clave_int) inner join cajero_inmobiliaria ci on (ci.caj_clave_int = c.caj_clave_int) where cai_inmobiliaria <> 0 and cai_inmobiliaria <> '' and cai_fecha_ini_inmobiliaria <> '0000-00-00' and ((cai_fecha_entrega_info_inmobiliaria > ADDDATE(cai_fecha_ini_inmobiliaria, INTERVAL mod_dias_inmobiliaria DAY)) or (cai_fecha_entrega_info_inmobiliaria = '0000-00-00' and (CURDATE() > ADDDATE(cai_fecha_ini_inmobiliaria, INTERVAL mod_dias_inmobiliaria DAY)))) and c.caj_estado_proyecto in (1,5) and (cai_fecha_entrega_info_inmobiliaria = '0000-00-00' or cai_fecha_entrega_info_inmobiliaria = '') and c.caj_sw_eliminado = 0 and c.caj_fecha_creacion >= '2015-01-01 00:00:00' and c.caj_clave_int not in (select alerta.caj_clave_int from alerta where c.caj_clave_int = alerta.caj_clave_int and tal_clave_int = 1 and ale_sw_inactivo = 0)");
	$num = mysqli_num_rows($con);
	for($i = 0; $i < $num; $i++)
	{
		$dato = mysqli_fetch_array($con);
		$caj = $dato['caj_clave_int'];
		$fi = $dato['cai_fecha_ini_inmobiliaria'];
		$fe = $dato['cai_fecha_entrega_info_inmobiliaria'];
		
		mysqli_query($conectar,"insert into alerta(ale_clave_int,caj_clave_int,tal_clave_int,ale_fecha_inicio,ale_fecha_entrega,ale_sw_enviado,ale_usu_actualiz,ale_fec_actualiz,ale_sw_inactivo) values(null,'".$caj."',1,'".$fi."','".$fe."',0,'".$usuario."','".$fecha."','0')");
	}
	//VISITA
	$con = mysqli_query($conectar,"select c.caj_clave_int,cav_fecha_visita,cav_fecha_entrega_informe from cajero c inner join modalidad m on (m.mod_clave_int = c.mod_clave_int) inner join cajero_visita cv on (cv.caj_clave_int = c.caj_clave_int) where cav_visitante <> 0 and cav_visitante <> '' and cav_fecha_visita <> '0000-00-00' and ((cav_fecha_entrega_informe > ADDDATE(cav_fecha_visita, INTERVAL mod_dias_visita DAY)) or (cav_fecha_entrega_informe = '0000-00-00' and (CURDATE() > ADDDATE(cav_fecha_visita, INTERVAL mod_dias_visita DAY)))) and c.caj_estado_proyecto in (1,5) and (cav_fecha_entrega_informe = '0000-00-00' or cav_fecha_entrega_informe = '')  and c.caj_sw_eliminado = 0 and c.caj_fecha_creacion >= '2015-01-01 00:00:00' and c.caj_clave_int not in (select alerta.caj_clave_int from alerta where c.caj_clave_int = alerta.caj_clave_int and tal_clave_int = 2 and ale_sw_inactivo = 0)");
	$num = mysqli_num_rows($con);
	for($i = 0; $i < $num; $i++)
	{
		$dato = mysqli_fetch_array($con);
		$caj = $dato['caj_clave_int'];
		$fi = $dato['cav_fecha_visita'];
		$fe = $dato['cav_fecha_entrega_informe'];
		
		mysqli_query($conectar,"insert into alerta(ale_clave_int,caj_clave_int,tal_clave_int,ale_fecha_inicio,ale_fecha_entrega,ale_sw_enviado,ale_usu_actualiz,ale_fec_actualiz,ale_sw_inactivo) values(null,'".$caj."',2,'".$fi."','".$fe."',0,'".$usuario."','".$fecha."','0')");
	}
	//DISEï¿½O
	$con = mysqli_query($conectar,"select c.caj_clave_int,cad_fecha_inicio_diseno,cad_fecha_entrega_info_diseno from cajero c inner join modalidad m on (m.mod_clave_int = c.mod_clave_int) inner join cajero_diseno cd on (cd.caj_clave_int = c.caj_clave_int) where cad_disenador <> 0 and cad_disenador <> '' and cad_fecha_inicio_diseno <> '0000-00-00' and ((cad_fecha_entrega_info_diseno > ADDDATE(cad_fecha_inicio_diseno, INTERVAL mod_dias_diseno DAY)) or (cad_fecha_entrega_info_diseno = '0000-00-00' and (CURDATE() > ADDDATE(cad_fecha_inicio_diseno, INTERVAL mod_dias_diseno DAY)))) and c.caj_estado_proyecto in (1,5) and (cad_fecha_entrega_info_diseno = '0000-00-00' or cad_fecha_entrega_info_diseno = '') and c.caj_sw_eliminado = 0 and c.caj_fecha_creacion >= '2015-01-01 00:00:00' and c.caj_clave_int not in (select alerta.caj_clave_int from alerta where c.caj_clave_int = alerta.caj_clave_int and tal_clave_int = 3 and ale_sw_inactivo = 0)");
	$num = mysqli_num_rows($con);
	for($i = 0; $i < $num; $i++)
	{
		$dato = mysqli_fetch_array($con);
		$caj = $dato['caj_clave_int'];
		$fi = $dato['cad_fecha_inicio_diseno'];
		$fe = $dato['cad_fecha_entrega_info_diseno'];
		
		mysqli_query($conectar,"insert into alerta(ale_clave_int,caj_clave_int,tal_clave_int,ale_fecha_inicio,ale_fecha_entrega,ale_sw_enviado,ale_usu_actualiz,ale_fec_actualiz,ale_sw_inactivo) values(null,'".$caj."',3,'".$fi."','".$fe."',0,'".$usuario."','".$fecha."','0')");
	}
	//LICENCIA
	$con = mysqli_query($conectar,"select c.caj_clave_int,cal_fecha_inicio_licencia,cal_fecha_entrega_info_licencia from cajero c inner join modalidad m on (m.mod_clave_int = c.mod_clave_int) inner join cajero_licencia cl on (cl.caj_clave_int = c.caj_clave_int) where cal_gestionador <> 0 and cal_gestionador <> '' and cal_fecha_inicio_licencia <> '0000-00-00' and ((cal_fecha_entrega_info_licencia > ADDDATE(cal_fecha_inicio_licencia, INTERVAL mod_dias_licencia DAY)) or (cal_fecha_entrega_info_licencia = '0000-00-00' and (CURDATE() > ADDDATE(cal_fecha_inicio_licencia, INTERVAL mod_dias_licencia DAY)))) and c.caj_estado_proyecto in (1,5) and (cal_fecha_entrega_info_licencia = '0000-00-00' or cal_fecha_entrega_info_licencia = '') and c.caj_sw_eliminado = 0 and c.caj_fecha_creacion >= '2015-01-01 00:00:00' and c.caj_clave_int not in (select alerta.caj_clave_int from alerta where c.caj_clave_int = alerta.caj_clave_int and tal_clave_int = 4 and ale_sw_inactivo = 0)");
	$num = mysqli_num_rows($con);
	for($i = 0; $i < $num; $i++)
	{
		$dato = mysqli_fetch_array($con);
		$caj = $dato['caj_clave_int'];
		$fi = $dato['cal_fecha_inicio_licencia'];
		$fe = $dato['cal_fecha_entrega_info_licencia'];
		
		mysqli_query($conectar,"insert into alerta(ale_clave_int,caj_clave_int,tal_clave_int,ale_fecha_inicio,ale_fecha_entrega,ale_sw_enviado,ale_usu_actualiz,ale_fec_actualiz,ale_sw_inactivo) values(null,'".$caj."',4,'".$fi."','".$fe."',0,'".$usuario."','".$fecha."','0')");
	}
	//INTERVENTORIA
	$con = mysqli_query($conectar,"select c.caj_clave_int,cin_fecha_inicio_obra,cin_fecha_teorica_entrega from cajero c inner join cajero_interventoria ci on (ci.caj_clave_int = c.caj_clave_int) inner join cajero_constructor cc on (cc.caj_clave_int = c.caj_clave_int) where ci.cin_fecha_teorica_entrega <> '' and ci.cin_fecha_teorica_entrega <> '0000-00-00' and ((ci.cin_fecha_inicio_obra = '0000-00-00' and cc.cac_fecha_entrega_atm = '0000-00-00' and CURDATE() > ci.cin_fecha_teorica_entrega) or (ci.cin_fecha_inicio_obra <> '0000-00-00' and ci.cin_fecha_inicio_obra > ci.cin_fecha_teorica_entrega)) and c.caj_estado_proyecto in (1,5) and c.caj_sw_eliminado = 0 and c.caj_fecha_creacion >= '2015-01-01 00:00:00' and c.caj_clave_int not in (select alerta.caj_clave_int from alerta where c.caj_clave_int = alerta.caj_clave_int and tal_clave_int = 5 and ale_sw_inactivo = 0)");
	$num = mysqli_num_rows($con);
	for($i = 0; $i < $num; $i++)
	{
		$dato = mysqli_fetch_array($con);
		$caj = $dato['caj_clave_int'];
		$fi = $dato['cin_fecha_inicio_obra'];
		$fe = $dato['cin_fecha_teorica_entrega'];
		
		mysqli_query($conectar,"insert into alerta(ale_clave_int,caj_clave_int,tal_clave_int,ale_fecha_inicio,ale_fecha_entrega,ale_sw_enviado,ale_usu_actualiz,ale_fec_actualiz,ale_sw_inactivo) values(null,'".$caj."',5,'".$fi."','".$fe."',0,'".$usuario."','".$fecha."','0')");
	}
	//CONSTRUCTOR
	$con = mysqli_query($conectar,"select c.caj_clave_int,ci.cin_fecha_inicio_obra,ci.cin_fecha_teorica_entrega,cc.cac_fecha_entrega_atm from cajero c inner join cajero_interventoria ci on (ci.caj_clave_int = c.caj_clave_int) inner join cajero_constructor cc on (cc.caj_clave_int = c.caj_clave_int) where ci.cin_fecha_teorica_entrega <> '' and ci.cin_fecha_teorica_entrega <> '0000-00-00' and ((cc.cac_fecha_entrega_atm <> '0000-00-00' and cc.cac_fecha_entrega_atm > ci.cin_fecha_teorica_entrega) OR (cc.cac_fecha_entrega_atm =  '0000-00-00' AND CURDATE( ) > ci.cin_fecha_teorica_entrega)) and (cc.cac_fecha_entrega_atm = '0000-00-00' or cc.cac_fecha_entrega_atm = '') and c.caj_estado_proyecto in (1,5) and c.caj_sw_eliminado = 0 and c.caj_fecha_creacion >= '2015-01-01 00:00:00' and c.caj_clave_int not in (select alerta.caj_clave_int from alerta where c.caj_clave_int = alerta.caj_clave_int and tal_clave_int = 6 and ale_sw_inactivo = 0)");
	$num = mysqli_num_rows($con);
	for($i = 0; $i < $num; $i++)
	{
		$dato = mysqli_fetch_array($con);
		$caj = $dato['caj_clave_int'];
		$fi = $dato['cin_fecha_inicio_obra'];
		$fte = $dato['cin_fecha_teorica_entrega'];
		$fe = $dato['cac_fecha_entrega_atm'];
		
		mysqli_query($conectar,"insert into alerta(ale_clave_int,caj_clave_int,tal_clave_int,ale_fecha_inicio,ale_fecha_entrega,ale_fecha_entrega_cajero,ale_sw_enviado,ale_usu_actualiz,ale_fec_actualiz,ale_sw_inactivo) values(null,'".$caj."',6,'".$fi."','".$fte."','".$fe."',0,'".$usuario."','".$fecha."', 0)");
	}
	
	//Actualizo las alertas que no se han enviado el dia de hoy
	mysqli_query($conectar,"update alerta set ale_sw_enviado = 0 where ale_fecha_enviado <> CURDATE() and tal_clave_int = 1 and caj_clave_int in (select caj_clave_int from cajero_inmobiliaria where caj_clave_int = alerta.caj_clave_int and (cai_fecha_entrega_info_inmobiliaria = '' or cai_fecha_entrega_info_inmobiliaria = '0000-00-00'))");

	mysqli_query($conectar,"update alerta set ale_sw_enviado = 0 where ale_fecha_enviado <> CURDATE() and tal_clave_int = 2 and caj_clave_int in (select caj_clave_int from cajero_visita where caj_clave_int = alerta.caj_clave_int and (cav_fecha_entrega_informe = '' or cav_fecha_entrega_informe = '0000-00-00'))");
	
	mysqli_query($conectar,"update alerta set ale_sw_enviado = 0 where ale_fecha_enviado <> CURDATE() and tal_clave_int = 3 and caj_clave_int in (select caj_clave_int from cajero_diseno where caj_clave_int = alerta.caj_clave_int and (cad_fecha_entrega_info_diseno = '' or cad_fecha_entrega_info_diseno = '0000-00-00'))");
	mysqli_query($conectar,"update alerta set ale_sw_enviado = 0 where ale_fecha_enviado <> CURDATE() and tal_clave_int = 4 and caj_clave_int in (select caj_clave_int from cajero_licencia where caj_clave_int = alerta.caj_clave_int and (cal_fecha_entrega_info_licencia = '' or cal_fecha_entrega_info_licencia = '0000-00-00'))");
	mysqli_query($conectar,"update alerta set ale_sw_enviado = 0 where ale_fecha_enviado <> CURDATE() and tal_clave_int = 5 and caj_clave_int in (select caj_clave_int from cajero_interventoria where caj_clave_int = alerta.caj_clave_int and (cin_fecha_inicio_obra = '' or cin_fecha_inicio_obra = '0000-00-00'))");
	
	mysqli_query($conectar,"update alerta set ale_sw_enviado = 0 where ale_fecha_enviado <> CURDATE() and tal_clave_int = 6 and caj_clave_int in (select caj_clave_int from cajero_constructor where caj_clave_int = alerta.caj_clave_int and (cac_fecha_entrega_atm = '' or cac_fecha_entrega_atm = '0000-00-00'))");


	//Envio correo de alertas a los implicados de cada cajero con alerta
	$con = mysqli_query($conectar,"select a.ale_clave_int,c.caj_clave_int,c.caj_nombre,a.tal_clave_int from alerta a inner join cajero c on (c.caj_clave_int = a.caj_clave_int) where a.ale_sw_enviado = 0 and a.ale_sw_eliminado = 0 and a.ale_fecha_enviado <> CURDATE() and c.caj_fecha_creacion >= '2015-01-01' and c.caj_estado_proyecto in (1,5) and c.caj_sw_eliminado = 0 and a.ale_sw_inactivo=0");
	$num = mysqli_num_rows($con);
	
	for($i = 0; $i < $num; $i++)
	{
		$dato = mysqli_fetch_array($con);
		$ale = $dato['ale_clave_int'];
		$caj = $dato['caj_clave_int'];
		$nomcaj = $dato['caj_nombre'];
		$tip = $dato['tal_clave_int'];
		
		$con1 = mysqli_query($conectar,"select * from cajero_inmobiliaria where caj_clave_int = '".$caj."'");
		$dato1 = mysqli_fetch_array($con1);
		$inm = $dato1['cai_inmobiliaria'];
		$finm = $dato1['cai_fecha_entrega_info_inmobiliaria'];
		$con1 = mysqli_query($conectar,"select * from cajero_visita where caj_clave_int = '".$caj."'");
		$dato1 = mysqli_fetch_array($con1);
		$vis = $dato1['cav_visitante'];
		$fvis = $dato1['cav_fecha_entrega_informe'];
		$con1 = mysqli_query($conectar,"select * from cajero_diseno where caj_clave_int = '".$caj."'");
		$dato1 = mysqli_fetch_array($con1);
		$dis = $dato1['cad_disenador'];
		$fdis = $dato1['cad_fecha_entrega_info_diseno'];
		$con1 = mysqli_query($conectar,"select * from cajero_licencia where caj_clave_int = '".$caj."'");
		$dato1 = mysqli_fetch_array($con1);
		$lic = $dato1['cal_gestionador'];
		$flic = $dato1['cal_fecha_entrega_info_licencia'];
		$con1 = mysqli_query($conectar,"select * from cajero_interventoria where caj_clave_int = '".$caj."'");
		$dato1 = mysqli_fetch_array($con1);
		$int = $dato1['cin_interventor'];
		$fint = $dato1['cin_fecha_inicio_obra'];
		$con1 = mysqli_query($conectar,"select * from cajero_constructor where caj_clave_int = '".$caj."'");
		$dato1 = mysqli_fetch_array($con1);
		$cons = $dato1['cac_constructor'];
		$fcons = $dato1['cac_fecha_entrega_atm'];
		$con1 = mysqli_query($conectar,"select * from cajero_seguridad where caj_clave_int = '".$caj."'");
		$dato1 = mysqli_fetch_array($con1);
		$seg = $dato1['cas_instalador'];
		$fseg = $dato1['cas_fecha_ingreso_obra_inst'];
		
		if($inm <> '' and $inm <> 0 and $tip == 1 and ($finm == '' or $finm == '0000-00-00'))
		{
			enviar_correo($ale,$inm,$caj,$nomcaj);
		}
		if($vis <> '' and $vis <> 0 and $tip == 2 and ($fvis == '' or $fvis == '0000-00-00'))
		{
			enviar_correo($ale,$vis,$caj,$nomcaj);
		}
		if($dis <> '' and $dis <> 0 and $tip == 3 and ($fdis == '' or $fdis == '0000-00-00'))
		{
			enviar_correo($ale,$dis,$caj,$nomcaj);
		}
		if($lic <> '' and $lic <> 0 and $tip == 4 and ($flic == '' or $flic == '0000-00-00'))
		{
			enviar_correo($ale,$lic,$caj,$nomcaj);
		}
		if($int <> '' and $int <> 0 and $tip == 5 and ($fint == '' or $fint == '0000-00-00'))
		{
			enviar_correo($ale,$int,$caj,$nomcaj);
		}
		if($cons <> '' and $cons <> 0 and $tip == 6 and ($fcons == '' or $fcons == '0000-00-00'))
		{
			enviar_correo($ale,$cons,$caj,$nomcaj);
		}
		if($inm <> '' and $inm <> 0 and $tip == 7)
		{
			enviar_correo($ale,$inm,$caj,$nomcaj);
		}
		if($vis <> '' and $vis <> 0 and $tip == 8)
		{
			enviar_correo($ale,$vis,$caj,$nomcaj);
		}

		if($dis <> '' and $dis <> 0 and $tip == 9)
		{
			enviar_correo($ale,$dis,$caj,$nomcaj);
		}
		if($lic <> '' and $lic <> 0 and $tip == 10)
		{
			enviar_correo($ale,$lic,$caj,$nomcaj);
		}
		if($cons <> '' and $cons <> 0 and $tip == 11)
		{
			enviar_correo($ale,$cons,$caj,$nomcaj);
		}
	}
	
	function enviar_correo($a,$u,$c,$n)//$u=Clave Usuario / $a=Actor / $c=Clave Cajero / $n=Nombre cajero
	{
        $conectar = mysqli_connect("localhost", "usrpavas", "9A12)WHFy$2p4v4s", "cajeros");
		//$mail->Body = '';
		$ema = '';
		$con1 = mysqli_query($conectar,"select * from usuario where usu_clave_int = '".$u."'");
		$dato1 = mysqli_fetch_array($con1);
		$nom = $dato1['usu_nombre'];
		$ema = $dato1['usu_email'];
		$cat = $dato1['usu_categoria'];
		
		$con1 = mysqli_query($conectar,"select ale_clave_int,caj_clave_int,tal_nombre,act_nombre,a.tal_clave_int,ale_fecha_inicio,ale_fecha_entrega,ale_fecha_entrega_cajero from alerta a inner join tipo_alerta ta on (ta.tal_clave_int = a.tal_clave_int) inner join actor act on (act.act_clave_int = ta.act_clave_int) where a.ale_clave_int = '".$a."'");
		$dato1 = mysqli_fetch_array($con1);
		$ale = $dato1['tal_nombre'];
		$act = $dato1['act_nombre'];
		$tip = $dato1['tal_clave_int'];
		$fi = $dato1['ale_fecha_inicio'];
		$fe = $dato1['ale_fecha_entrega'];
		$fecaj = $dato1['ale_fecha_entrega_cajero'];
		$caj = $dato1['caj_clave_int'];
		$conmod = mysqli_query($conectar,"select * from cajero c inner join modalidad m on (m.mod_clave_int = c.mod_clave_int) where c.caj_clave_int = ".$caj."");
		$datomod = mysqli_fetch_array($conmod);
		$dinm = $datomod['mod_dias_inmobiliaria'];
		$dvis = $datomod['mod_dias_visita'];
		$ddis = $datomod['mod_dias_diseno'];
		$dlic = $datomod['mod_dias_licencia'];
		
		if($dinm == ''){ $dinm = 0; }
		if($dvis == ''){ $dvis = 0; }
		if($ddis == ''){ $ddis = 0; }
		if($dlic == ''){ $dlic = 0; }
		
		if($tip == 1)
		{
			$con1 = mysqli_query($conectar,"select ADDDATE('".$fi."', INTERVAL ".$dinm." DAY) fecteo from usuario LIMIT 1");
			$dato1 = mysqli_fetch_array($con1);
			$fecteo = $dato1['fecteo'];
		}
		else
		if($tip == 2)
		{
			$con1 = mysqli_query($conectar,"select ADDDATE('".$fi."', INTERVAL ".$dvis." DAY) fecteo from usuario LIMIT 1");
			$dato1 = mysqli_fetch_array($con1);
			$fecteo = $dato1['fecteo'];
		}
		else
		if($tip == 3)
		{
			$con1 = mysqli_query($conectar,"select ADDDATE('".$fi."', INTERVAL ".$ddis." DAY) fecteo from usuario LIMIT 1");
			$dato1 = mysqli_fetch_array($con1);
			$fecteo = $dato1['fecteo'];
		}
		else
		if($tip == 4)
		{
			$con1 = mysqli_query($conectar,"select ADDDATE('".$fi."', INTERVAL ".$dlic." DAY) fecteo from usuario LIMIT 1");
			$dato1 = mysqli_fetch_array($con1);
			$fecteo = $dato1['fecteo'];
		}
		else
		if($tip == 5)
		{
			$fecteo = $fe;
		}
		
		if($tip == 6)
		{
			if($fecaj == '0000-00-00')
			{
				$condias = mysqli_query($conectar,"select DATEDIFF(CURDATE(),'".$fe."') dias from usuario LIMIT 1");
				$datodias = mysqli_fetch_array($condias);
				$dias = $datodias['dias'];
			}
			else
			{
				$condias = mysqli_query($conectar,"select DATEDIFF('".$fecaj."','".$fe."') dias from usuario LIMIT 1");
				$datodias = mysqli_fetch_array($condias);
				$dias = $datodias['dias'];
			}
		}
		else
		if($fe == '0000-00-00' or $tip == 5)
		{	
			$condias = mysqli_query($conectar,"select DATEDIFF(CURDATE(),'".$fecteo."') dias from usuario LIMIT 1");
			$datodias = mysqli_fetch_array($condias);
			$dias = $datodias['dias'];
		}
		else
		{
			$condias = mysqli_query($conectar,"select DATEDIFF('".$fe."','".$fecteo."') dias from usuario LIMIT 1");
			$datodias = mysqli_fetch_array($condias);
			$dias = $datodias['dias'];
		}
		
		if($tip == 6){ $fte = $fe; }else { $fte = $fecteo; }
		if($tip == 6){ $entrega = $fecaj; }else { $entrega = $fe; }

		if($cat == 1)
		{
			$mail = new PHPMailer();

			$mail->From = "adminpavas@pavas.com.co";
			$mail->FromName = "Soportica";
			$mail->AddAddress($ema, "Destino");
			$mail->Subject = "Alerta Cajero Nro. ".$c;

			// Cuerpo del mensaje
			$mail->Body .= "Hola ".$nom."!\n\n";
			$mail->Body .= "CONTROL CAJEROS registra que hay una alerta.\n";
			$mail->Body .= "CAJERO: ".$n."\n";
			$mail->Body .= "ALERTA: ".$ale."\n";
			$mail->Body .= "ACTOR: ".$act."\n";
			$mail->Body .= "FECHA INICIO: ".$fi."\n";
			$mail->Body .= "FECHA TEORICA ENTREGA: ".$fte."\n";
			$mail->Body .= "FECHA ENTREGA: ".$entrega."\n";
			$mail->Body .= "DIAS RETRASO: ".$dias."\n";
			$mail->Body .= date("d/m/Y H:m:s")."\n\n";
			$mail->Body .= "Este mensaje es generado automaticamente por CONTROL CAJEROS, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Cajeros \n";

            if (($tip == 1 || $tip == 2 || $tip == 3 || $tip == 4 || $tip == 5 || $tip == 6) && $dias % 3 == 0) 
            {
                if (!$mail->Send()) {
                    //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                    //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                } else {
                    mysqli_query($conectar,"update alerta set ale_sw_enviado = 1, ale_fecha_enviado = '" . date("Y/m/d") . "' where ale_clave_int = '" . $a . "'");
                }
            } 
            else 
            {
                if (($tip == 7 || $tip == 8 || $tip == 9 || $tip == 10 || $tip == 11) && $dias == -1)
                {
                    if (!$mail->Send()) {
                        //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                        //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                    } else {
                        mysqli_query($conectar,"update alerta set ale_sw_enviado = 1, ale_fecha_enviado = '" . date("Y/m/d") . "' where ale_clave_int = '" . $a . "'");
                    }
                }
            }
		}
		else
		if($cat == 2)
		{
			$con = mysqli_query($conectar,"select * from usuario_actor ua inner join usuario u on (u.usu_clave_int = ua.usu_clave_int) where ua.usa_actor = '".$u."'");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$mail = new PHPMailer();

				$dato1 = mysqli_fetch_array($con);
				$mail->Body = '';
				$ema = '';
				$nom = $dato1['usu_nombre'];
				$ema = $dato1['usu_email'];

				$mail->From = "adminpavas@pavas.com.co";
				$mail->FromName = "Soportica";
				$mail->AddAddress($ema, "Destino");
				$mail->Subject = "Alerta Cajero Nro. ".$c;

				// Cuerpo del mensaje
				$mail->Body .= "Hola ".$nom."!\n\n";
				$mail->Body .= "CONTROL CAJEROS registra que hay una alerta.\n";
				$mail->Body .= "CAJERO: ".$n."\n";
				$mail->Body .= "ALERTA: ".$ale."\n";
				$mail->Body .= "ACTOR: ".$act."\n";
				$mail->Body .= "FECHA INICIO: ".$fi."\n";
				$mail->Body .= "FECHA TEORICA ENTREGA: ".$fte."\n";
				$mail->Body .= "FECHA ENTREGA: ".$entrega."\n";
				$mail->Body .= "DIAS RETRASO: ".$dias."\n";
				$mail->Body .= date("d/m/Y H:m:s")."\n\n";
				$mail->Body .= "Este mensaje es generado automaticamente por CONTROL CAJEROS, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Cajeros \n";

                if (($tip == 1 || $tip == 2 || $tip == 3 || $tip == 4 || $tip == 5 || $tip == 6) && $dias % 3 == 0) {
                    if (!$mail->Send()) {
                        //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                        //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                    } else {
                        mysqli_query($conectar,"update alerta set ale_sw_enviado = 1, ale_fecha_enviado = '" . date("Y/m/d") . "' where ale_clave_int = '" . $a . "'");
                    }
                } 
                else 
                {
                    if (($tip == 7 || $tip == 8 || $tip == 9 || $tip == 10 || $tip == 11) && $dias == -1)
                    {
                        if (!$mail->Send()) {
                            //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                            //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                        } else {
                            mysqli_query($conectar,"update alerta set ale_sw_enviado = 1, ale_fecha_enviado = '" . date("Y/m/d") . "' where ale_clave_int = '" . $a . "'");
                        }
                    }
                }
			}
		}
	}