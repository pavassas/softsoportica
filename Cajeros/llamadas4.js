


function cambiar_captcha(){
    document.getElementById('imgcaptcha').src="data/captcha.php?rnd=" + Math.random();
}
function ALERTALOGUEO()
{

}
function VALIDARCATCHAT(){
    var elcaptcha = $("#captcha").val();
    $.post("data/validarcaptchat.php", {
            ingreso_cap: elcaptcha,},

// Obtenemos la respuesta
        function(respuesta_php){

            if(respuesta_php == 'captchaincorrecto'){
                // alert(respuesta_php); // este alert es solo para cerificar como funciona,
                console.log('Respuesta: ',respuesta_php);
                cambiar_captcha();
                $("#mensajescapt").text('Código de seguridad incorrecto');
                $("#mensajescapt").show(1000);
                return false;
            }
           else{
                // alert(respuesta_php); // este alert es solo para cerificar como funciona,
                console.log('Respuesta: ',respuesta_php);
                return false;
            }

        }); // fin $post
}
function validarpass(id) {

    $('#'+id).keyup(function() {
        // set password variable
        var pswd = $(this).val();
        //validate the length
        if ( pswd.length < 8 ) {
            $('#length').html('Debería tener <strong>8 carácteres</strong> como mínimo');
            $('#length').css('color',"red");
        } else {
            $('#length').html('');
        }

        //validate letter
        if ( pswd.match(/[A-z]/) ) {
            $('#letter').html('');
        } else {
            $('#letter').html('Al menos debería tener <strong>una letra</strong>');
            $('#letter').css('color',"red");

        }

        //validate capital letter
        if ( pswd.match(/[A-Z]/) ) {
            $('#capital').html('');
        } else {
            $('#capital').html('Al menos debería tener <strong>una letra en mayúsculas</strong>');
            $('#capital').css('color',"red");

        }

        //validate number
        if ( pswd.match(/\d/) ) {
            $('#number').html('');
        } else {
            $('#number').html('Al menos debería tener <strong>un número</strong>');
            $('#number').css('color',"red");

        }

        if(/\s/.test(pswd)){
            $('#space').html('No puede contener  <strong>espacios vacios</strong>');
            $('#space').css('color',"red");
		}
		else
		{
            $('#space').html('');
		}

    }).focus(function() {
        $('#pswd_info').show();
    }).blur(function() {
        $('#pswd_info').hide();
    });
}


function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function MODULO(v)
{	
	if(v == 'INICIO')
	{
        document.title = 'Seguimiento Cajeros  - Inicio';
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?moduloinicio=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'USUARIOS')
	{
        document.title = 'Seguimiento Cajeros  - Usuarios';
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulousuarios=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'FESTIVOS')
	{
        document.title = 'Seguimiento Cajeros  - Festivos';
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulofestivos=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'CONFIGURACIONES')
	{
        document.title = 'Seguimiento Cajeros  - Configuraciones';
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?moduloconfiguraciones=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'CAMBIARCONTRASENA')
	{
        document.title = 'Seguimiento Cajeros  - Cambio Contraseña';
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulocancambiarcontrasena=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'CAJEROS')
	{
		document.title = 'Seguimiento Cajeros  - Cajeros'
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulocajeros=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'POSICIONGEOGRAFICA')
	{
        document.title = 'Seguimiento Cajeros  - Posición Geografica';
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?moduloposiciongeografica=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'CONSTRUCTOR')
	{
        document.title = 'Seguimiento Cajeros  - Contructor';
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?moduloconstructor=si",true);
		ajax.send(null);
    	//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'INMOBILIARIA')
	{
        document.title = 'Seguimiento Cajeros  - Inmobiliaria';
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?moduloinmobiliaria=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'DISENO')
	{
        document.title = 'Seguimiento Cajeros  - Diseño';
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulodiseno=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'INTERVENTORIA')
	{
        document.title = 'Seguimiento Cajeros  - Interventoria';
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulointerventoria=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'INSTALADORSEGURIDAD')
	{
        document.title = 'Seguimiento Cajeros  - Seguridad';
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?moduloinstaladorseguridad=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'CENTROCOSTOS')
	{
        document.title = 'Seguimiento Cajeros  - Inicio';
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulocentrocostos=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'TIPOLOGIA')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulotipologia=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'REFERENCIAMAQUINA')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?moduloreferenciamaquina=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'UBICACION')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?moduloubicacion=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'ATIENDE')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?moduloatiende=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'TIPOINTERVENCION')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulotipointervencion=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'AREA')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?moduloarea=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'VISITALOCAL')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulovisitalocal=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'LICENCIA')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulolicencia=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'FACTURACION')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulofacturacion=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'CANALCOMUNICACIONES')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulocanalcomunicaciones=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'CONSULTA')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?moduloconsulta=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'PERMISOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulopermisos=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'REGISTROACTIVIDADES')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?moduloregistroactividades=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'ALERTAS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?moduloalertas=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
    else
    if(v == 'ASIGNARALERTAS')
    {
        var ajax;
        ajax=new ajaxFunction();
        ajax.onreadystatechange=function()
        {
            if(ajax.readyState==4)
            {
                document.getElementById('modulos').innerHTML=ajax.responseText;
            }
        }
        jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
        ajax.open("GET","?moduloasignaralertas=si",true);
        ajax.send(null);
        //setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
    }
	else
	if(v == 'INFORMECAJEROELIMINADO')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulocajeroseliminados=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'INFORMECAJEROSSINELIMINAR')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulocajerossineliminar=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'MODALIDAD')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulomodalidad=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'PLANEACION')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?moduloplaneacion=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'MAQUINAS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulomaquinas=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'INSTRUCTIVO')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?moduloinstructivo=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'APROBARPEDIDOMAQUINA')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?moduloaprobarpedidomaquina=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'LINEATELEFONICA')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulolineatelefonica=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
}
function OPCIONACTIVA(v)
{	
	if(v == 'ADMINISTRADOR')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('tabContent_5').innerHTML=ajax.responseText;
		    }
		}
		//jQuery("#tabContent_5").html("<img alt='cargando' src='img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?opcionadministrador=si",true);
		ajax.send(null);
	}
	else
	if(v == 'TICKETS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('tabContent_1').innerHTML=ajax.responseText;
		    }
		}
		//jQuery("#tabContent_5").html("<img alt='cargando' src='img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?opciontickets=si",true);
		ajax.send(null);
	}
	else
	if(v == 'REPORTES')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('tabContent_2').innerHTML=ajax.responseText;
		    }
		}
		//jQuery("#tabContent_5").html("<img alt='cargando' src='img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?opcionreportes=si",true);
		ajax.send(null);
	}
}
function RECUPERAR()
{	
	var rec = $("#recuperarcontrasena").val();
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('recu').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#recu").html("<img alt='cargando' src='images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?recuperar=si&dat="+rec,true);
	ajax.send(null);
}
function ENVIARNOTIFICACION()
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('').innerHTML=ajax.responseText;
	    }
	}
	//jQuery("#recu").html("<img alt='cargando' src='img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?enviarnotificacion=si",true);
	ajax.send(null);
}

function VALIDARCLAVEDINAMICA(msn,o)
{
    if(o==1)
    {
        var modalLocation = 'modalclave';
        $('#'+modalLocation).reveal();
        $('#msnclave').html(msn);
       // $('#modalclave').foundation('reveal', 'open');

     /*   var clave = prompt(msn, "");
        if (clave == "" || clave == null)
        {
            alert("No ingreso la clave dinamica");
            window.location.href = "data/logout.php";
        }
        else
        {
            $.post('funciones/fnValidaciones.php',{opcion:"VALIDARCLAVEDINAMICA",clave:clave},function(data) {
                    var res = data[0].res;
                    if (res == "ok") {
                        window.location.href = "principal.php";
                    }
                    else if (res == "error1") {
                        alert("Clave ingresada incorrecta");
                        window.location.href = "data/logout.php";
                    }
                    else if (res == "error2") {
                        alert("Surgio un error al actualizar acceso a clave dinamica");
                        window.location.href = "data/logout.php";
                    }
                }
            ,"json");
        }*/
    }
    else if(o==2)
    {
        alert(msn);
        window.location.href = "data/logout.php";
    }
}
function  VERIFICARCLAVEDINAMICA()
{
	var clave = $('#clavedinamica').val();
	var l = clave.length;

    if (clave == "" || clave == null)
    {
        $('#divveri').html("No ingreso la clave dinamica").addClass('validaciones');
        //window.location.href = "data/logout.php";
    }
    else if(l<6)
	{
        $('#divveri').html("La clave debe contener 6 digitos").addClass('validaciones');
	}
    else
    {
        $.post('funciones/fnValidaciones.php',{opcion:"VALIDARCLAVEDINAMICA",clave:clave},function(data) {
                var res = data[0].res;
                if (res == "ok") {
                    window.location.href = "principal.php";
                }
                else if (res == "error1") {
                    $('#divveri').html("Clave ingresada incorrecta").addClass('validaciones');
                    //alert("Clave ingresada incorrecta");
                    //window.location.href = "data/logout.php";
                }
                else if (res == "error2") {
                    $('#divveri').html("Surgio un error al actualizar acceso a clave dinamica").addClass('validaciones');
                    //alert("Surgio un error al actualizar acceso a clave dinamica");
                    //window.location.href = "data/logout.php";
                }
            }
            ,"json");
    }
}
function RESTABLECER(v)
{
	var con1 = $("#contrasena1").val();
	var con2 = $("#contrasena2").val();
    var longitud = false,
        minuscula = false,
        numero = false,
        mayuscula = false, space = false;

    if (con1.length < 8  || con2.length<8) {
        //$('#length').removeClass('valid').addClass('invalid');
        longitud = false;
    } else {
        //$('#length').removeClass('invalid').addClass('valid');
        longitud = true;
    }

    //validate letter
    if (con1.match(/[A-z]/) && con2.match(/[A-z]/)) {
        //$('#letter').removeClass('invalid').addClass('valid');
        minuscula = true;
    } else {
        // $('#letter').removeClass('valid').addClass('invalid');
        minuscula = false;
    }

    //validate capital letter
    if (con1.match(/[A-Z]/) && con2.match(/[A-Z]/)) {
        // $('#capital').removeClass('invalid').addClass('valid');
        mayuscula = true;
    } else {
        //$('#capital').removeClass('valid').addClass('invalid');
        mayuscula = false;
    }

    //validate number
    if (con1.match(/\d/) && con2.match(/\d/)) {
        // $('#number').removeClass('invalid').addClass('valid');
        numero = true;
    } else {
        //$('#number').removeClass('valid').addClass('invalid');
        numero = false;
    }

    if(/\s/.test(con1) || /\s/.test(con2)){
        space = false;
    }
    else
    {
        space = true
    }
    if(longitud==false || minuscula==false || numero==false || mayuscula==false ||space ==false){
        alert("No cumple con los parametros de seguridad de una contraseña");

    }
    else
	{
        var ajax;
        ajax = new ajaxFunction();
        ajax.onreadystatechange = function () {
            if (ajax.readyState == 4) {
                document.getElementById('clear').innerHTML = ajax.responseText;
            }
        }
        jQuery("#clear").html("<img alt='cargando' src='img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
        ajax.open("GET", "?restablecer=si&cod=" + v + "&con1=" + con1 + "&con2=" + con2, true);
        ajax.send(null);
    }
}
function NOPERMISO(v)
{
	alert("Usted no tiene permiso a la opci�n "+v);
}
function MOSTRARVIDEO(v)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
	    	document.getElementById('video').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#video").html("<img alt='cargando' src='img/ajax-loader.gif' height='50' width='50' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarvideo=si&vid="+v,true);
	ajax.send(null);
}