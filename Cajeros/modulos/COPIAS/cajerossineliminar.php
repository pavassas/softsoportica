<style type="text/css">
.auto-style8 {
	text-decoration: underline;
}
</style>
<?php
	include('../../data/Conexion.php');
	require("../../Classes/PHPMailer-master/class.phpmailer.php");
	
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_COOKIE['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];
		
	// verifica si no se ha loggeado
	if(!isset($_SESSION["persona"]))
	{
	  session_destroy();
	  header("LOCATION:index.php");
	}else{
	}
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");
	$anocont = date("Y");
	require_once("lib/Zebra_Pagination.php");
	
	$con = mysqli_query($conectar,"select u.usu_clave_int,p.prf_clave_int,p.prf_descripcion,p.prf_sw_actualiza_info from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
	$dato = mysqli_fetch_array($con);
	$clausu = $dato['usu_clave_int'];
	$claprf = $dato['prf_clave_int'];
	$perfil = $dato['prf_descripcion'];
	$actualizainfo = $dato['prf_sw_actualiza_info'];
	
	$con = mysqli_query($conectar,"select per_metodo from permiso where prf_clave_int = '".$claprf."' and ven_clave_int = 4");
	$dato = mysqli_fetch_array($con);
	$metodo = $dato['per_metodo'];
	
	if($_GET['buscar'] == 'si')
	{
		$caj = $_GET['caj'];
		$seleccionados = explode(',',$caj);
		$num = count($seleccionados);
		$cajeros = array();
		for($i = 0; $i < $num; $i++)
		{
			if($seleccionados[$i] != '')
			{
				$cajeros[$i]=$seleccionados[$i];
			}
		}
		$listacajeros=implode(',',$cajeros);
		
		$fii = $_GET['fii'];
		$ffi = $_GET['ffi'];
		$fie = $_GET['fie'];
		$ffe = $_GET['ffe'];
		$nomcaj = $_GET['nomcaj'];
		$dircaj = $_GET['dircaj'];
		$est = $_GET['est'];
		$anocon = $_GET['anocon'];
		$reg = $_GET['reg'];
		$dep = $_GET['dep'];
		$mun = $_GET['mun'];
		$tip = $_GET['tip'];
		$tipint = $_GET['tipint'];
		$moda = $_GET['moda'];
		
		$cod = $_GET['cod'];
		$cencos = $_GET['cencos'];
		$refmaq = $_GET['refmaq'];
		$ubi = $_GET['ubi'];
		$codsuc = $_GET['codsuc'];
		$ubiatm = $_GET['ubiatm'];
		$ati = $_GET['ati'];
		$rie = $_GET['rie'];
		$are = $_GET['are'];
		$codrec = $_GET['codrec'];
		
		$nominm = $_GET['nominm'];
		$estinm = $_GET['estinm'];
		$feciniinmdesde = $_GET['feciniinmdesde'];
		$feciniinmhasta = $_GET['feciniinmhasta'];
		$fecentinmdesde = $_GET['fecentinmdesde'];
		$fecentinmhasta = $_GET['fecentinmhasta'];
		$diasinm = $_GET['diasinm'];
		$notinm = $_GET['notinm'];
		
		$nomvis = $_GET['nomvis'];
		$estvis = $_GET['estvis'];
		$fecinivisdesde = $_GET['fecinivisdesde'];
		$fecinivishasta = $_GET['fecinivishasta'];
		$fecentvisdesde = $_GET['fecentvisdesde'];
		$fecentvishasta = $_GET['fecentvishasta'];
		$diasvis = $_GET['diasvis'];
		$notvis = $_GET['notvis'];
		
		$nomdis = $_GET['nomdis'];
		$estdis = $_GET['estdis'];
		$fecinidisdesde = $_GET['fecinidisdesde'];
		$fecinidishasta = $_GET['fecinidishasta'];
		$fecentdisdesde = $_GET['fecentdisdesde'];
		$fecentdishasta = $_GET['fecentdishasta'];
		$diasdis = $_GET['diasdis'];
		$notdis = $_GET['notdis'];
		
		$nomges = $_GET['nomges'];
		$estges = $_GET['estges'];
		$fecinigesdesde = $_GET['fecinigesdesde'];
		$fecinigeshasta = $_GET['fecinigeshasta'];
		$fecentgesdesde = $_GET['fecentgesdesde'];
		$fecentgeshasta = $_GET['fecentgeshasta'];
		$diasges = $_GET['diasges'];
		$notges = $_GET['notges'];
		
		$nomint = $_GET['nomint'];
		$cancom = $_GET['cancom'];
		$feciniintdesde = $_GET['feciniintdesde'];
		$feciniinthasta = $_GET['feciniinthasta'];
		$fecentintdesde = $_GET['fecentintdesde'];
		$fecentinthasta = $_GET['fecentinthasta'];
		$fecinipedintdesde = $_GET['fecinipedintdesde'];
		$fecinipedinthasta = $_GET['fecinipedinthasta'];
		$notint = $_GET['notint'];
		
		$nomcons = $_GET['nomcons'];
		$porava = $_GET['porava'];
		$feciniconsdesde = $_GET['feciniconsdesde'];
		$feciniconshasta = $_GET['feciniconshasta'];
		$fecentconsdesde = $_GET['fecentconsdesde'];
		$fecentconshasta = $_GET['fecentconshasta'];
		$diascons = $_GET['diascons'];
		$notcons = $_GET['notcons'];
		
		$swinfobasica = $_GET['swinfobasica'];
		$swinfosecundaria = $_GET['swinfosecundaria'];
		$swinmobiliaria = $_GET['swinmobiliaria'];
		$swvisitalocal = $_GET['swvisitalocal'];
		$swdiseno = $_GET['swdiseno'];
		$swlicencia = $_GET['swlicencia'];
		$swinterventoria = $_GET['swinterventoria'];
		$swconstructor = $_GET['swconstructor'];
		
		$vernombrecajero = $_GET['vernombrecajero'];
		$verdireccion = $_GET['verdireccion'];
		$verestadocajero = $_GET['verestadocajero'];
		$veranocontable = $_GET['veranocontable'];
		$verregion = $_GET['verregion'];
		$verdepartamento = $_GET['verdepartamento'];
		$vermunicipio = $_GET['vermunicipio'];
		$vertipologia = $_GET['vertipologia'];
		$verintervencion = $_GET['verintervencion'];
		$vermodalidad = $_GET['vermodalidad'];
		$vercodigocajero = $_GET['vercodigocajero'];
		$vercentrocostos = $_GET['vercentrocostos'];
		$verreferencia = $_GET['verreferencia'];
		$verubicacion = $_GET['verubicacion'];
		$vercodigosuc = $_GET['vercodigosuc'];
		$verubicacionatm = $_GET['verubicacionatm'];
		$veratiende = $_GET['veratiende'];
		$verriesgo = $_GET['verriesgo'];
		$verarea = $_GET['verarea'];
		$vercodigorecibido = $_GET['vercodigorecibido'];
		$verinmobiliaria = $_GET['verinmobiliaria'];
		$verestadoinm = $_GET['verestadoinm'];
		$verfechainicioinm = $_GET['verfechainicioinm'];
		$verfechaentregainm = $_GET['verfechaentregainm'];
		$vertotaldiasinm = $_GET['vertotaldiasinm'];
		$vernotasinm = $_GET['vernotasinm'];
		$vervisita = $_GET['vervisita'];
		$verestadovis = $_GET['verestadovis'];
		$verfechainiciovis = $_GET['verfechainiciovis'];
		$verfechaentregavis = $_GET['verfechaentregavis'];
		$vertotaldiasvis = $_GET['vertotaldiasvis'];
		$vernotasvis = $_GET['vernotasvis'];
		$verdisenador = $_GET['verdisenador'];
		$verestadodis = $_GET['verestadodis'];
		$verfechainiciodis = $_GET['verfechainiciodis'];
		$verfechaentregadis = $_GET['verfechaentregadis'];
		$vertotaldiasdis = $_GET['vertotaldiasdis'];
		$vernotasdis = $_GET['vernotasdis'];
		$vergestionador = $_GET['vergestionador'];
		$verestadolic = $_GET['verestadolic'];
		$verfechainiciolic = $_GET['verfechainiciolic'];
		$verfechaentregalic = $_GET['verfechaentregalic'];
		$vertotaldiaslic = $_GET['vertotaldiaslic'];
		$vernotaslic = $_GET['vernotaslic'];
		$verinterventor = $_GET['verinterventor'];
		$veroperadorcanal = $_GET['veroperadorcanal'];
		$verfechainicioint = $_GET['verfechainicioint'];
		$verfechaentregaint = $_GET['verfechaentregaint'];
		$verfechapedido = $_GET['verfechapedido'];
		$vernotasint = $_GET['vernotasint'];
		$verconstructor = $_GET['verconstructor'];
		$veravance = $_GET['veravance'];
		$verfechainiciocons = $_GET['verfechainiciocons'];
		$verfechaentregacons = $_GET['verfechaentregacons'];
		$vertotaldiascons = $_GET['vertotaldiascons'];
		$vernotascons = $_GET['vernotascons'];
		
		if($swinfobasica == 0)
		{
			$nomcaj = '';
			$dircaj = '';
			$est = '';
			$anocon = '';
			$reg = '';
			$dep = '';
			$mun = '';
			$tip = '';
			$tipint = '';
			$moda = '';
		}
		if($swinfosecundaria == 0)
		{
			$cod = '';
			$cencos = '';
			$refmaq = '';
			$ubi = '';
			$codsuc = '';
			$ubiatm = '';
			$ati = '';
			$rie = '';
			$are = '';
			$codrec = '';
		}
		if($swinmobiliaria == 0)
		{
			$nominm = '';
			$estinm = '';
			$feciniinmdesde = '';
			$feciniinmhasta = '';
			$fecentinmdesde = '';
			$fecentinmhasta = '';
			$diasinm = '';
			$notinm = '';
		}
		if($swvisitalocal == 0)
		{
			$nomvis = '';
			$estvis = '';
			$fecinivisdesde = '';
			$fecinivishasta = '';
			$fecentvisdesde = '';
			$fecentvishasta = '';
			$diasvis = '';
			$notvis = '';
		}
		if($swdiseno == 0)
		{
			$nomdis = '';
			$estdis = '';
			$fecinidisdesde = '';
			$fecinidishasta = '';
			$fecentdisdesde = '';
			$fecentdishasta = '';
			$diasdis = '';
			$notdis = '';
		}
		if($swlicencia == 0)
		{
			$nomges = '';
			$estges = '';
			$fecinigesdesde = '';
			$fecinigeshasta = '';
			$fecentgesdesde = '';
			$fecentgeshasta = '';
			$diasges = '';
			$notges = '';
		}
		if($swinterventoria == 0)
		{
			$nomint = '';
			$cancom = '';
			$feciniintdesde = '';
			$feciniinthasta = '';
			$fecentintdesde = '';
			$fecentinthasta = '';
			$fecinipedintdesde = '';
			$fecinipedinthasta = '';
			$notint = '';
		}
		if($swconstructor == 0)
		{
			$nomcons = '';
			$porava = '';
			$feciniconsdesde = '';
			$feciniconshasta = '';
			$fecentconsdesde = '';
			$fecentconshasta = '';
			$diascons = '';
			$notcons = '';
		}
?>
		<fieldset name="Group1">
		<legend align="center">LISTADO DE CAJEROS<br>
		<button name="Accion" value="excel" title="Exportar a Excel" onclick="EXPORTAR()" type="button" style="cursor:pointer;">
		<img src="../../images/excel.png" height="25" width="25"></button>
		</legend>
		<div id="informacion" style="overflow:;width:1200px">
		<table style="width: 100%">
		<tr>
			<td class="alinearizq"><strong>Cajero</strong></td>
			<?php if($vernombrecajero == 'true'){ ?>
			<td class="alinearizq"><strong>Nombre</strong></td>
			<?php } ?>
			<?php if($verdireccion == 'true'){ ?>
			<td class="alinearizq"><strong>Dirección</strong></td>
			<?php } ?>
			<?php if($veranocontable == 'true'){ ?>
			<td class="alinearizq"><strong>Año Cont.</strong></td>
			<?php } ?>
			<?php if($verregion == 'true'){ ?>
			<td class="alinearizq"><strong>Región</strong></td>
			<?php } ?>
			<?php if($verdepartamento == 'true'){ ?>
			<td class="alinearizq"><strong>Departamento</strong></td>
			<?php } ?>
			<?php if($vermunicipio == 'true'){ ?>
			<td class="alinearizq"><strong>Municipio</strong></td>
			<?php } ?>
			<?php if($vertipologia == 'true'){ ?>
			<td class="alinearizq"><strong>Tipología</strong></td>
			<?php } ?>
			<?php if($verintervencion == 'true'){ ?>
			<td class="alinearizq"><strong>Tip. Intervención</strong></td>
			<?php } ?>
			<?php if($vermodalidad == 'true'){ ?>
			<td class="alinearizq"><strong>Modalidad</strong></td>
			<?php } ?>
			<?php if($verestadocajero == 'true'){ ?>
			<td class="alinearizq"><strong>Estado</strong></td>
			<?php } ?>
			<?php if($vercodigocajero == 'true'){ ?>
			<td class="alinearizq"><strong>Código</strong></td>
			<?php } ?>
			<?php if($vercentrocostos == 'true'){ ?>
			<td class="alinearizq"><strong>Cen.Cos</strong></td>
			<?php } ?>
			<?php if($verreferencia == 'true'){ ?>
			<td class="alinearizq"><strong>Ref. Maquina</strong></td>
			<?php } ?>
			<?php if($verubicacion == 'true'){ ?>
			<td class="alinearizq"><strong>Ubicación (SUC, CC, REMOTO)</strong></td>
			<?php } ?>
			<?php if($vercodigosuc == 'true'){ ?>
			<td class="alinearizq"><strong>Cód. Suc.</strong></td>
			<?php } ?>
			<?php if($verubicacionatm == 'true'){ ?>
			<td class="alinearizq"><strong>Ubicación ATM.</strong></td>
			<?php } ?>
			<?php if($veratiende == 'true'){ ?>
			<td class="alinearizq"><strong>Atiende</strong></td>
			<?php } ?>
			<?php if($verriesgo == 'true'){ ?>
			<td class="alinearizq"><strong>Riesgo</strong></td>
			<?php } ?>
			<?php if($verarea == 'true'){ ?>
			<td class="alinearizq"><strong>Area</strong></td>
			<?php } ?>
			<?php if($verinmobiliaria == 'true'){ ?>
			<td class="alinearizq"><strong>Inmobiliaria</strong></td>
			<?php } ?>
			<?php if($verestadoinm == 'true'){ ?>
			<td class="alinearizq"><strong>Estado</strong></td>
			<?php } ?>
			<?php if($verfechainicioinm == 'true'){ ?>
			<td class="alinearizq"><strong>Fecha Inicio</strong></td>
			<?php } ?>
			<?php if($verfechaentregainm == 'true'){ ?>
			<td class="alinearizq"><strong>Fecha Entrega</strong></td>
			<?php } ?>
			<?php if($vertotaldiasinm == 'true'){ ?>
			<td class="alinearizq"><strong>Total Días</strong></td>
			<?php } ?>
			<?php if($vernotasinm == 'true'){ ?>
			<td class="alinearizq"><strong>Notas</strong></td>
			<?php } ?>
			<?php if($vervisita == 'true'){ ?>
			<td class="alinearizq"><strong>Visitante</strong></td>
			<?php } ?>
			<?php if($verestadovis == 'true'){ ?>
			<td class="alinearizq"><strong>Estado</strong></td>
			<?php } ?>
			<?php if($verfechainiciovis == 'true'){ ?>
			<td class="alinearizq"><strong>Fecha Inicio</strong></td>
			<?php } ?>
			<?php if($verfechaentregavis == 'true'){ ?>
			<td class="alinearizq"><strong>Fecha Entrega</strong></td>
			<?php } ?>
			<?php if($vertotaldiasvis == 'true'){ ?>
			<td class="alinearizq"><strong>Total Días</strong></td>
			<?php } ?>
			<?php if($vernotasvis == 'true'){ ?>
			<td class="alinearizq"><strong>Notas</strong></td>
			<?php } ?>
			<?php if($verdisenador == 'true'){ ?>
			<td class="alinearizq"><strong>Diseñador</strong></td>
			<?php } ?>
			<?php if($verestadodis == 'true'){ ?>
			<td class="alinearizq"><strong>Estado</strong></td>
			<?php } ?>
			<?php if($verfechainiciodis == 'true'){ ?>
			<td class="alinearizq"><strong>Fecha Inicio</strong></td>
			<?php } ?>
			<?php if($verfechaentregadis == 'true'){ ?>
			<td class="alinearizq"><strong>Fecha Entrega</strong></td>
			<?php } ?>
			<?php if($vertotaldiasdis == 'true'){ ?>
			<td class="alinearizq"><strong>Total Días</strong></td>
			<?php } ?>
			<?php if($vernotasdis == 'true'){ ?>
			<td class="alinearizq"><strong>Notas</strong></td>
			<?php } ?>
			<?php if($vergestionador == 'true'){ ?>
			<td class="alinearizq"><strong>Gestionador</strong></td>
			<?php } ?>
			<?php if($verestadolic == 'true'){ ?>
			<td class="alinearizq"><strong>Estado</strong></td>
			<?php } ?>
			<?php if($verfechainiciolic == 'true'){ ?>
			<td class="alinearizq"><strong>Fecha Inicio</strong></td>
			<?php } ?>
			<?php if($verfechaentregalic == 'true'){ ?>
			<td class="alinearizq"><strong>Fecha Entrega</strong></td>
			<?php } ?>
			<?php if($vertotaldiaslic == 'true'){ ?>
			<td class="alinearizq"><strong>Total Días</strong></td>
			<?php } ?>
			<?php if($vernotaslic == 'true'){ ?>
			<td class="alinearizq"><strong>Notas</strong></td>
			<?php } ?>
			<?php if($verinterventor == 'true'){ ?>
			<td class="alinearizq"><strong>Interventor</strong></td>
			<?php } ?>
			<?php if($veroperadorcanal == 'true'){ ?>
			<td class="alinearizq"><strong>Operador Canal</strong></td>
			<?php } ?>
			<?php if($verfechainicioint == 'true'){ ?>
			<td class="alinearizq"><strong>Fecha Inicio Obra</strong></td>
			<?php } ?>
			<?php if($verfechaentregaint == 'true'){ ?>
			<td class="alinearizq"><strong>Fecha Entrega</strong></td>
			<?php } ?>
			<?php if($verfechapedido == 'true'){ ?>
			<td class="alinearizq"><strong>Fecha Pedido Suministro</strong></td>
			<?php } ?>
			<?php if($vernotasint == 'true'){ ?>
			<td class="alinearizq"><strong>Notas</strong></td>
			<?php } ?>
			<?php if($verconstructor == 'true'){ ?>
			<td class="alinearizq"><strong>Constructor</strong></td>
			<?php } ?>
			<?php if($veravance == 'true'){ ?>
			<td class="alinearizq"><strong>%Avance</strong></td>
			<?php } ?>
			<?php if($verfechainiciocons == 'true'){ ?>
			<td class="alinearizq"><strong>Fecha Inicio</strong></td>
			<?php } ?>
			<?php if($verfechaentregacons == 'true'){ ?>
			<td class="alinearizq"><strong>Fecha Entrega</strong></td>
			<?php } ?>
			<?php if($vertotaldiascons == 'true'){ ?>
			<td class="alinearizq"><strong>Total Días</strong></td>
			<?php } ?>
			<?php if($vernotascons == 'true'){ ?>
			<td class="alinearizq"><strong>Notas</strong></td>
			<?php } ?>
		</tr>
		<?php
		$sql = '';
		if($listacajeros == '')
		{
			$sql = "((c.caj_fecha_inicio = '".$fii."'  OR '".$fii."' IS NULL OR '".$fii."' = '') or ((c.caj_fecha_inicio BETWEEN '".$fii." 00:00:00' AND '".$ffi." 23:59:59') or ('".$fii."' Is Null and '".$ffi."' Is Null) or ('".$fii."' = '' and '".$ffi."' = ''))) and (((cajcons.cac_fecha_entrega_atm = '".$fie."'  OR '".$fie."' IS NULL OR '".$fie."' = '') or ((cajcons.cac_fecha_entrega_atm BETWEEN '".$fie." 00:00:00' AND '".$ffe." 23:59:59') or ('".$fie."' Is Null and '".$ffe."' Is Null) or ('".$fie."' = '' and '".$ffe."' = ''))) or ((cajint.cin_fecha_teorica_entrega = '".$fie."'  OR '".$fie."' IS NULL OR '".$fie."' = '') or ((cajint.cin_fecha_teorica_entrega BETWEEN '".$fie." 00:00:00' AND '".$ffe." 23:59:59') or ('".$fie."' Is Null and '".$ffe."' Is Null) or ('".$fie."' = '' and '".$ffe."' = '')))) and 
			(caj_nombre LIKE REPLACE('%".$nomcaj."%',' ','%') OR '".$nomcaj."' IS NULL OR '".$nomcaj."' = '') and 
			(caj_direccion LIKE REPLACE('%".$dircaj."%',' ','%') OR '".$dircaj."' IS NULL OR '".$dircaj."' = '') and 
			(caj_estado_proyecto = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') and
			(caj_ano_contable = '".$anocon."' OR '".$anocon."' IS NULL OR '".$anocon."' = '') and 
			(caj_region = '".$reg."' OR '".$reg."' IS NULL OR '".$reg."' = '') and 
			(caj_departamento = '".$dep."' OR '".$dep."' IS NULL OR '".$dep."' = '') and 
			(caj_municipio = '".$mun."' OR '".$mun."' IS NULL OR '".$mun."' = '') and 
			(tip_clave_int = '".$tip."' OR '".$tip."' IS NULL OR '".$tip."' = '') and 
			(tii_clave_int = '".$tipint."' OR '".$tipint."' IS NULL OR '".$tipint."' = '') and 
			(mod_clave_int = '".$moda."' OR '".$moda."' IS NULL OR '".$moda."' = '') and 
			
			(caj_codigo_cajero LIKE REPLACE('%".$cod."%',' ','%') OR '".$cod."' IS NULL OR '".$cod."' = '') and 
			(cco_clave_int = '".$cencos."' OR '".$cencos."' IS NULL OR '".$cencos."' = '') and 
			(rem_clave_int = '".$refmaq."' OR '".$refmaq."' IS NULL OR '".$refmaq."' = '') and 
			(ubi_clave_int = '".$ubi."' OR '".$ubi."' IS NULL OR '".$ubi."' = '') and 
			(caj_codigo_suc LIKE REPLACE('%".$codsuc."%',' ','%') OR '".$codsuc."' IS NULL OR '".$codsuc."' = '') and 
			(caj_ubicacion_atm = '".$ubiatm."' OR '".$ubiatm."' IS NULL OR '".$ubiatm."' = '') and 
			(ati_clave_int = '".$ati."' OR '".$ati."' IS NULL OR '".$ati."' = '') and 
			(caj_riesgo = '".$rie."' OR '".$rie."' IS NULL OR '".$rie."' = '') and 
			(are_clave_int = '".$are."' OR '".$are."' IS NULL OR '".$are."' = '') and 
			(caj_codigo_recibido_monto LIKE REPLACE('%".$codrec."%',' ','%') OR '".$codrec."' IS NULL OR '".$codrec."' = '') and 
			
			(cai_inmobiliaria = '".$nominm."' OR '".$nominm."' IS NULL OR '".$nominm."' = '') and 
			(esi_clave_int = '".$estinm."' OR '".$estinm."' IS NULL OR '".$estinm."' = '') and 
			((cai_fecha_ini_inmobiliaria = '".$feciniinmdesde."'  OR '".$feciniinmdesde."' IS NULL OR '".$feciniinmdesde."' = '') or ((cai_fecha_ini_inmobiliaria BETWEEN '".$feciniinmdesde." 00:00:00' AND '".$feciniinmhasta." 23:59:59') or ('".$feciniinmdesde."' Is Null and '".$feciniinmhasta."' Is Null) or ('".$feciniinmdesde."' = '' and '".$feciniinmhasta."' = ''))) and 
			((cai_fecha_entrega_info_inmobiliaria = '".$fecentinmdesde."'  OR '".$fecentinmdesde."' IS NULL OR '".$fecentinmdesde."' = '') or ((cai_fecha_entrega_info_inmobiliaria BETWEEN '".$fecentinmdesde." 00:00:00' AND '".$fecentinmhasta." 23:59:59') or ('".$fecentinmdesde."' Is Null and '".$fecentinmhasta."' Is Null) or ('".$fecentinmdesde."' = '' and '".$fecentinmhasta."' = ''))) and 
			(DATEDIFF(cai_fecha_entrega_info_inmobiliaria,cai_fecha_ini_inmobiliaria) = '".$diasinm."' OR '".$diasinm."' IS NULL OR '".$diasinm."' = '') and 
			
			(cav_visitante = '".$nomvis."' OR '".$nomvis."' IS NULL OR '".$nomvis."' = '') and 
			(cav_estado = '".$estvis."' OR '".$estvis."' IS NULL OR '".$estvis."' = '') and 
			((cav_fecha_visita = '".$fecinivisdesde."'  OR '".$fecinivisdesde."' IS NULL OR '".$fecinivisdesde."' = '') or ((cav_fecha_visita BETWEEN '".$fecinivisdesde." 00:00:00' AND '".$fecinivishasta." 23:59:59') or ('".$fecinivisdesde."' Is Null and '".$fecinivishasta."' Is Null) or ('".$fecinivisdesde."' = '' and '".$fecinivishasta."' = ''))) and 
			((cav_fecha_entrega_informe = '".$fecentvisdesde."'  OR '".$fecentvisdesde."' IS NULL OR '".$fecentvisdesde."' = '') or ((cav_fecha_entrega_informe BETWEEN '".$fecentvisdesde." 00:00:00' AND '".$fecentvishasta." 23:59:59') or ('".$fecentvisdesde."' Is Null and '".$fecentvishasta."' Is Null) or ('".$fecentvisdesde."' = '' and '".$fecentvishasta."' = ''))) and 
			(DATEDIFF(cav_fecha_entrega_informe,cav_fecha_visita) = '".$diasvis."' OR '".$diasvis."' IS NULL OR '".$diasvis."' = '') and 

			(cad_disenador = '".$nomdis."' OR '".$nomdis."' IS NULL OR '".$nomdis."' = '') and 
			(esd_clave_int = '".$estdis."' OR '".$estdis."' IS NULL OR '".$estdis."' = '') and 
			((cad_fecha_inicio_diseno = '".$fecinidisdesde."'  OR '".$fecinidisdesde."' IS NULL OR '".$fecinidisdesde."' = '') or ((cad_fecha_inicio_diseno BETWEEN '".$fecinidisdesde." 00:00:00' AND '".$fecinidishasta." 23:59:59') or ('".$fecinidisdesde."' Is Null and '".$fecinidishasta."' Is Null) or ('".$fecinidisdesde."' = '' and '".$fecinidishasta."' = ''))) and 
			((cad_fecha_entrega_info_diseno = '".$fecentdisdesde."'  OR '".$fecentdisdesde."' IS NULL OR '".$fecentdisdesde."' = '') or ((cad_fecha_entrega_info_diseno BETWEEN '".$fecentdisdesde." 00:00:00' AND '".$fecentdishasta." 23:59:59') or ('".$fecentdisdesde."' Is Null and '".$fecentdishasta."' Is Null) or ('".$fecentdisdesde."' = '' and '".$fecentdishasta."' = ''))) and 
			(DATEDIFF(cad_fecha_entrega_info_diseno,cad_fecha_inicio_diseno) = '".$diasdis."' OR '".$diasdis."' IS NULL OR '".$diasdis."' = '') and 
			
			(cal_gestionador = '".$nomges."' OR '".$nomges."' IS NULL OR '".$nomges."' = '') and 
			(esl_clave_int = '".$estges."' OR '".$estges."' IS NULL OR '".$estges."' = '') and 
			((cal_fecha_inicio_licencia = '".$fecinigesdesde."'  OR '".$fecinigesdesde."' IS NULL OR '".$fecinigesdesde."' = '') or ((cal_fecha_inicio_licencia BETWEEN '".$fecinigesdesde." 00:00:00' AND '".$fecinigeshasta." 23:59:59') or ('".$fecinigesdesde."' Is Null and '".$fecinigeshasta."' Is Null) or ('".$fecinigesdesde."' = '' and '".$fecinigeshasta."' = ''))) and 
			((cal_fecha_entrega_info_licencia = '".$fecentgesdesde."'  OR '".$fecentgesdesde."' IS NULL OR '".$fecentgesdesde."' = '') or ((cal_fecha_entrega_info_licencia BETWEEN '".$fecentgesdesde." 00:00:00' AND '".$fecentgeshasta." 23:59:59') or ('".$fecentgesdesde."' Is Null and '".$fecentgeshasta."' Is Null) or ('".$fecentgesdesde."' = '' and '".$fecentgeshasta."' = ''))) and 
			(DATEDIFF(cal_fecha_entrega_info_licencia,cal_fecha_inicio_licencia) = '".$diasges."' OR '".$diasges."' IS NULL OR '".$diasges."' = '') and 
						
			(cin_interventor = '".$nomint."' OR '".$nomint."' IS NULL OR '".$nomint."' = '') and 
			(can_clave_int = '".$cancom."' OR '".$cancom."' IS NULL OR '".$cancom."' = '') and 
			((cin_fecha_inicio_obra = '".$feciniintdesde."'  OR '".$feciniintdesde."' IS NULL OR '".$feciniintdesde."' = '') or ((cin_fecha_inicio_obra BETWEEN '".$feciniintdesde." 00:00:00' AND '".$feciniinthasta." 23:59:59') or ('".$feciniintdesde."' Is Null and '".$feciniinthasta."' Is Null) or ('".$feciniintdesde."' = '' and '".$feciniinthasta."' = ''))) and 
			((cin_fecha_teorica_entrega = '".$fecentintdesde."'  OR '".$fecentintdesde."' IS NULL OR '".$fecentintdesde."' = '') or ((cin_fecha_teorica_entrega BETWEEN '".$fecentintdesde." 00:00:00' AND '".$fecentinthasta." 23:59:59') or ('".$fecentintdesde."' Is Null and '".$fecentinthasta."' Is Null) or ('".$fecentintdesde."' = '' and '".$fecentinthasta."' = ''))) and 
			((cin_fecha_pedido_suministro = '".$fecinipedintdesde."'  OR '".$fecinipedintdesde."' IS NULL OR '".$fecinipedintdesde."' = '') or ((cin_fecha_pedido_suministro BETWEEN '".$fecinipedintdesde." 00:00:00' AND '".$fecinipedinthasta." 23:59:59') or ('".$fecinipedintdesde."' Is Null and '".$fecinipedinthasta."' Is Null) or ('".$fecinipedintdesde."' = '' and '".$fecinipedinthasta."' = ''))) and 
						
			(cac_constructor = '".$nomcons."' OR '".$nomcons."' IS NULL OR '".$nomcons."' = '') and 
			(cac_porcentaje_avance = '".$porava."' OR '".$porava."' IS NULL OR '".$porava."' = '') and 
			((cin_fecha_inicio_obra = '".$feciniconsdesde."'  OR '".$feciniconsdesde."' IS NULL OR '".$feciniconsdesde."' = '') or ((cin_fecha_inicio_obra BETWEEN '".$feciniconsdesde." 00:00:00' AND '".$feciniconshasta." 23:59:59') or ('".$feciniconsdesde."' Is Null and '".$feciniconshasta."' Is Null) or ('".$feciniconsdesde."' = '' and '".$feciniconshasta."' = ''))) and 
			((cac_fecha_entrega_atm = '".$fecentconsdesde."'  OR '".$fecentconsdesde."' IS NULL OR '".$fecentconsdesde."' = '') or ((cac_fecha_entrega_atm BETWEEN '".$fecentconsdesde." 00:00:00' AND '".$fecentconshasta." 23:59:59') or ('".$fecentconsdesde."' Is Null and '".$fecentconshasta."' Is Null) or ('".$fecentconsdesde."' = '' and '".$fecentconshasta."' = ''))) and 
			(DATEDIFF(cac_fecha_entrega_atm,cin_fecha_inicio_obra) = '".$diascons."' OR '".$diascons."' IS NULL OR '".$diascons."' = '') and 
			
			c.caj_sw_eliminado = 0";
			
			if($notinm != '')
			{
				$sql1 = "and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 7 and nou_nota LIKE REPLACE('%".$notinm."%',' ','%')))";
			}
			if($notvis != '')
			{
				$sql1 = "and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 18 and nou_nota LIKE REPLACE('%".$notvis."%',' ','%')))";
			}
			if($notdis != '')
			{
				$sql1 = "and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 8 and nou_nota LIKE REPLACE('%".$notdis."%',' ','%')))";
			}
			if($notges != '')
			{
				if($sql1 != ''){ $sql1 = $sql1." and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 19 and nou_nota LIKE REPLACE('%".$notges."%',' ','%')))"; }else{ $sql1 = "and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 19 and nou_nota LIKE REPLACE('%".$notges."%',' ','%')))"; }
			}
			if($notint != '')
			{
				if($sql1 != ''){ $sql1 = $sql1." and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 9 and nou_nota LIKE REPLACE('%".$notint."%',' ','%')))"; }else{ $sql1 = "and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 9 and nou_nota LIKE REPLACE('%".$notint."%',' ','%')))"; }
			}
			if($notcons != '')
			{
				if($sql1 != ''){ $sql1 = $sql1." and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 6 and nou_nota LIKE REPLACE('%".$notcons."%',' ','%')))"; }else{ $sql1 = "and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 6 and nou_nota LIKE REPLACE('%".$notcons."%',' ','%')))"; }
			}
			if($notdis == '' and $notges == '' and $notint == '' and $notcons == '')
			{
				$sql1 = "and c.caj_clave_int IS NOT NULL";
			}
		}
		else
		{
			$sql = "c.caj_clave_int in (".$listacajeros.") and ((c.caj_fecha_inicio = '".$fii."'  OR '".$fii."' IS NULL OR '".$fii."' = '') or ((c.caj_fecha_inicio BETWEEN '".$fii." 00:00:00' AND '".$ffi." 23:59:59') or ('".$fii."' Is Null and '".$ffi."' Is Null) or ('".$fii."' = '' and '".$ffi."' = ''))) and (((cajcons.cac_fecha_entrega_atm = '".$fie."'  OR '".$fie."' IS NULL OR '".$fie."' = '') or ((cajcons.cac_fecha_entrega_atm BETWEEN '".$fie." 00:00:00' AND '".$ffe." 23:59:59') or ('".$fie."' Is Null and '".$ffe."' Is Null) or ('".$fie."' = '' and '".$ffe."' = ''))) or ((cajint.cin_fecha_teorica_entrega = '".$fie."'  OR '".$fie."' IS NULL OR '".$fie."' = '') or ((cajint.cin_fecha_teorica_entrega BETWEEN '".$fie." 00:00:00' AND '".$ffe." 23:59:59') or ('".$fie."' Is Null and '".$ffe."' Is Null) or ('".$fie."' = '' and '".$ffe."' = '')))) and 
			(caj_nombre LIKE REPLACE('%".$nomcaj."%',' ','%') OR '".$nomcaj."' IS NULL OR '".$nomcaj."' = '') and 
			(caj_direccion LIKE REPLACE('%".$dircaj."%',' ','%') OR '".$dircaj."' IS NULL OR '".$dircaj."' = '') and 
			(caj_estado_proyecto = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') and
			(caj_ano_contable = '".$anocon."' OR '".$anocon."' IS NULL OR '".$anocon."' = '') and 
			(caj_region = '".$reg."' OR '".$reg."' IS NULL OR '".$reg."' = '') and 
			(caj_departamento = '".$dep."' OR '".$dep."' IS NULL OR '".$dep."' = '') and 
			(caj_municipio = '".$mun."' OR '".$mun."' IS NULL OR '".$mun."' = '') and 
			(tip_clave_int = '".$tip."' OR '".$tip."' IS NULL OR '".$tip."' = '') and 
			(tii_clave_int = '".$tipint."' OR '".$tipint."' IS NULL OR '".$tipint."' = '') and 
			(mod_clave_int = '".$moda."' OR '".$moda."' IS NULL OR '".$moda."' = '') and 
			
			(caj_codigo_cajero LIKE REPLACE('%".$cod."%',' ','%') OR '".$cod."' IS NULL OR '".$cod."' = '') and 
			(cco_clave_int = '".$cencos."' OR '".$cencos."' IS NULL OR '".$cencos."' = '') and 
			(rem_clave_int = '".$refmaq."' OR '".$refmaq."' IS NULL OR '".$refmaq."' = '') and 
			(ubi_clave_int = '".$ubi."' OR '".$ubi."' IS NULL OR '".$ubi."' = '') and 
			(caj_codigo_suc LIKE REPLACE('%".$codsuc."%',' ','%') OR '".$codsuc."' IS NULL OR '".$codsuc."' = '') and 
			(caj_ubicacion_atm = '".$ubiatm."' OR '".$ubiatm."' IS NULL OR '".$ubiatm."' = '') and 
			(ati_clave_int = '".$ati."' OR '".$ati."' IS NULL OR '".$ati."' = '') and 
			(caj_riesgo = '".$rie."' OR '".$rie."' IS NULL OR '".$rie."' = '') and 
			(are_clave_int = '".$are."' OR '".$are."' IS NULL OR '".$are."' = '') and 
			(caj_codigo_recibido_monto LIKE REPLACE('%".$codrec."%',' ','%') OR '".$codrec."' IS NULL OR '".$codrec."' = '') and 
			
			(cai_inmobiliaria = '".$nominm."' OR '".$nominm."' IS NULL OR '".$nominm."' = '') and 
			(esi_clave_int = '".$estinm."' OR '".$estinm."' IS NULL OR '".$estinm."' = '') and 
			((cai_fecha_ini_inmobiliaria = '".$feciniinmdesde."'  OR '".$feciniinmdesde."' IS NULL OR '".$feciniinmdesde."' = '') or ((cai_fecha_ini_inmobiliaria BETWEEN '".$feciniinmdesde." 00:00:00' AND '".$feciniinmhasta." 23:59:59') or ('".$feciniinmdesde."' Is Null and '".$feciniinmhasta."' Is Null) or ('".$feciniinmdesde."' = '' and '".$feciniinmhasta."' = ''))) and 
			((cai_fecha_entrega_info_inmobiliaria = '".$fecentinmdesde."'  OR '".$fecentinmdesde."' IS NULL OR '".$fecentinmdesde."' = '') or ((cai_fecha_entrega_info_inmobiliaria BETWEEN '".$fecentinmdesde." 00:00:00' AND '".$fecentinmhasta." 23:59:59') or ('".$fecentinmdesde."' Is Null and '".$fecentinmhasta."' Is Null) or ('".$fecentinmdesde."' = '' and '".$fecentinmhasta."' = ''))) and 
			(DATEDIFF(cai_fecha_entrega_info_inmobiliaria,cai_fecha_ini_inmobiliaria) = '".$diasinm."' OR '".$diasinm."' IS NULL OR '".$diasinm."' = '') and 
			
			(cav_visitante = '".$nomvis."' OR '".$nomvis."' IS NULL OR '".$nomvis."' = '') and 
			(cav_estado = '".$estvis."' OR '".$estvis."' IS NULL OR '".$estvis."' = '') and 
			((cav_fecha_visita = '".$fecinivisdesde."'  OR '".$fecinivisdesde."' IS NULL OR '".$fecinivisdesde."' = '') or ((cav_fecha_visita BETWEEN '".$fecinivisdesde." 00:00:00' AND '".$fecinivishasta." 23:59:59') or ('".$fecinivisdesde."' Is Null and '".$fecinivishasta."' Is Null) or ('".$fecinivisdesde."' = '' and '".$fecinivishasta."' = ''))) and 
			((cav_fecha_entrega_informe = '".$fecentvisdesde."'  OR '".$fecentvisdesde."' IS NULL OR '".$fecentvisdesde."' = '') or ((cav_fecha_entrega_informe BETWEEN '".$fecentvisdesde." 00:00:00' AND '".$fecentvishasta." 23:59:59') or ('".$fecentvisdesde."' Is Null and '".$fecentvishasta."' Is Null) or ('".$fecentvisdesde."' = '' and '".$fecentvishasta."' = ''))) and 
			(DATEDIFF(cav_fecha_entrega_informe,cav_fecha_visita) = '".$diasvis."' OR '".$diasvis."' IS NULL OR '".$diasvis."' = '') and 
			
			(cad_disenador = '".$nomdis."' OR '".$nomdis."' IS NULL OR '".$nomdis."' = '') and 
			(esd_clave_int = '".$estdis."' OR '".$estdis."' IS NULL OR '".$estdis."' = '') and 
			((cad_fecha_inicio_diseno = '".$fecinidisdesde."'  OR '".$fecinidisdesde."' IS NULL OR '".$fecinidisdesde."' = '') or ((cad_fecha_inicio_diseno BETWEEN '".$fecinidisdesde." 00:00:00' AND '".$fecinidishasta." 23:59:59') or ('".$fecinidisdesde."' Is Null and '".$fecinidishasta."' Is Null) or ('".$fecinidisdesde."' = '' and '".$fecinidishasta."' = ''))) and 
			((cad_fecha_entrega_info_diseno = '".$fecentdisdesde."'  OR '".$fecentdisdesde."' IS NULL OR '".$fecentdisdesde."' = '') or ((cad_fecha_entrega_info_diseno BETWEEN '".$fecentdisdesde." 00:00:00' AND '".$fecentdishasta." 23:59:59') or ('".$fecentdisdesde."' Is Null and '".$fecentdishasta."' Is Null) or ('".$fecentdisdesde."' = '' and '".$fecentdishasta."' = ''))) and 
			(DATEDIFF(cad_fecha_entrega_info_diseno,cad_fecha_inicio_diseno) = '".$diasdis."' OR '".$diasdis."' IS NULL OR '".$diasdis."' = '') and 
			
			(cal_gestionador = '".$nomges."' OR '".$nomges."' IS NULL OR '".$nomges."' = '') and 
			(esl_clave_int = '".$estges."' OR '".$estges."' IS NULL OR '".$estges."' = '') and 
			((cal_fecha_inicio_licencia = '".$fecinigesdesde."'  OR '".$fecinigesdesde."' IS NULL OR '".$fecinigesdesde."' = '') or ((cal_fecha_inicio_licencia BETWEEN '".$fecinigesdesde." 00:00:00' AND '".$fecinigeshasta." 23:59:59') or ('".$fecinigesdesde."' Is Null and '".$fecinigeshasta."' Is Null) or ('".$fecinigesdesde."' = '' and '".$fecinigeshasta."' = ''))) and 
			((cal_fecha_entrega_info_licencia = '".$fecentgesdesde."'  OR '".$fecentgesdesde."' IS NULL OR '".$fecentgesdesde."' = '') or ((cal_fecha_entrega_info_licencia BETWEEN '".$fecentgesdesde." 00:00:00' AND '".$fecentgeshasta." 23:59:59') or ('".$fecentgesdesde."' Is Null and '".$fecentgeshasta."' Is Null) or ('".$fecentgesdesde."' = '' and '".$fecentgeshasta."' = ''))) and 
			(DATEDIFF(cal_fecha_entrega_info_licencia,cal_fecha_inicio_licencia) = '".$diasges."' OR '".$diasges."' IS NULL OR '".$diasges."' = '') and 
						
			(cin_interventor = '".$nomint."' OR '".$nomint."' IS NULL OR '".$nomint."' = '') and 
			(can_clave_int = '".$cancom."' OR '".$cancom."' IS NULL OR '".$cancom."' = '') and 
			((cin_fecha_inicio_obra = '".$feciniintdesde."'  OR '".$feciniintdesde."' IS NULL OR '".$feciniintdesde."' = '') or ((cin_fecha_inicio_obra BETWEEN '".$feciniintdesde." 00:00:00' AND '".$feciniinthasta." 23:59:59') or ('".$feciniintdesde."' Is Null and '".$feciniinthasta."' Is Null) or ('".$feciniintdesde."' = '' and '".$feciniinthasta."' = ''))) and 
			((cin_fecha_teorica_entrega = '".$fecentintdesde."'  OR '".$fecentintdesde."' IS NULL OR '".$fecentintdesde."' = '') or ((cin_fecha_teorica_entrega BETWEEN '".$fecentintdesde." 00:00:00' AND '".$fecentinthasta." 23:59:59') or ('".$fecentintdesde."' Is Null and '".$fecentinthasta."' Is Null) or ('".$fecentintdesde."' = '' and '".$fecentinthasta."' = ''))) and 
			((cin_fecha_pedido_suministro = '".$fecinipedintdesde."'  OR '".$fecinipedintdesde."' IS NULL OR '".$fecinipedintdesde."' = '') or ((cin_fecha_pedido_suministro BETWEEN '".$fecinipedintdesde." 00:00:00' AND '".$fecinipedinthasta." 23:59:59') or ('".$fecinipedintdesde."' Is Null and '".$fecinipedinthasta."' Is Null) or ('".$fecinipedintdesde."' = '' and '".$fecinipedinthasta."' = ''))) and 
						
			(cac_constructor = '".$nomcons."' OR '".$nomcons."' IS NULL OR '".$nomcons."' = '') and 
			(cac_porcentaje_avance = '".$porava."' OR '".$porava."' IS NULL OR '".$porava."' = '') and 
			((cin_fecha_inicio_obra = '".$feciniconsdesde."'  OR '".$feciniconsdesde."' IS NULL OR '".$feciniconsdesde."' = '') or ((cin_fecha_inicio_obra BETWEEN '".$feciniconsdesde." 00:00:00' AND '".$feciniconshasta." 23:59:59') or ('".$feciniconsdesde."' Is Null and '".$feciniconshasta."' Is Null) or ('".$feciniconsdesde."' = '' and '".$feciniconshasta."' = ''))) and 
			((cac_fecha_entrega_atm = '".$fecentconsdesde."'  OR '".$fecentconsdesde."' IS NULL OR '".$fecentconsdesde."' = '') or ((cac_fecha_entrega_atm BETWEEN '".$fecentconsdesde." 00:00:00' AND '".$fecentconshasta." 23:59:59') or ('".$fecentconsdesde."' Is Null and '".$fecentconshasta."' Is Null) or ('".$fecentconsdesde."' = '' and '".$fecentconshasta."' = ''))) and 
			(DATEDIFF(cac_fecha_entrega_atm,cin_fecha_inicio_obra) = '".$diascons."' OR '".$diascons."' IS NULL OR '".$diascons."' = '') and 
			
			c.caj_sw_eliminado = 0";
			
			if($notinm != '')
			{
				$sql1 = "and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 7 and nou_nota LIKE REPLACE('%".$notinm."%',' ','%')))";
			}
			if($notvis != '')
			{
				$sql1 = "and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 18 and nou_nota LIKE REPLACE('%".$notvis."%',' ','%')))";
			}
			if($notdis != '')
			{
				$sql1 = "and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 8 and nou_nota LIKE REPLACE('%".$notdis."%',' ','%')))";
			}
			if($notges != '')
			{
				if($sql1 != ''){ $sql1 = $sql1." and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 19 and nou_nota LIKE REPLACE('%".$notges."%',' ','%')))"; }else{ $sql1 = "and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 19 and nou_nota LIKE REPLACE('%".$notges."%',' ','%')))"; }
			}
			if($notint != '')
			{
				if($sql1 != ''){ $sql1 = $sql1." and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 9 and nou_nota LIKE REPLACE('%".$notint."%',' ','%')))"; }else{ $sql1 = "and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 9 and nou_nota LIKE REPLACE('%".$notint."%',' ','%')))"; }
			}
			if($notcons != '')
			{
				if($sql1 != ''){ $sql1 = $sql1." and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 6 and nou_nota LIKE REPLACE('%".$notcons."%',' ','%')))"; }else{ $sql1 = "and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 6 and nou_nota LIKE REPLACE('%".$notcons."%',' ','%')))"; }
			}
			if($notdis == '' and $notges == '' and $notint == '' and $notcons == '')
			{
				$sql1 = "and c.caj_clave_int IS NOT NULL";
			}
		}
		//echo "select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int) inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where ".$sql."";
		$query = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int) inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where ".$sql." ".$sql1."");
		//$res = $con->query($query);
		$num_registros = mysqli_num_rows($query);

		$resul_x_pagina = 100;
		//Paginar:
		$paginacion = new Zebra_Pagination();
		$paginacion->records($num_registros);
		$paginacion->records_per_page($resul_x_pagina);
		
		$con = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int) inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where ".$sql." ".$sql1." LIMIT ".(($paginacion->get_page() - 1) * $resul_x_pagina). ',' .$resul_x_pagina);
		//echo "select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int) inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where ".$sql." ".$sql1."";
		$num = mysqli_num_rows($con);
		for($i = 0; $i < $num; $i++)
		{
			$dato = mysqli_fetch_array($con);
			//Info Base
			$clacaj = $dato['clacaj'];
			$nomcaj = $dato['caj_nombre'];
			$dir = $dato['caj_direccion'];
			$anocon = $dato['caj_ano_contable'];
			$reg = $dato['caj_region'];
			$dep = $dato['caj_departamento'];
			$mun = $dato['caj_municipio'];
			$tip = $dato['tip_clave_int'];
			$tipint = $dato['tii_clave_int'];
			$mod = $dato['mod_clave_int'];
			$conmod = mysqli_query($conectar,"select * from modalidad where mod_clave_int = '".$mod."'");
			$datomod = mysqli_fetch_array($conmod);
			$moda = $datomod['mod_nombre'];
			$swlic = $datomod['mod_sw_licencia'];
			$dinm = $datomod['mod_dias_inmobiliaria'];
			$dvis = $datomod['mod_dias_visita'];
			$ddis = $datomod['mod_dias_diseno'];
			$dlic = $datomod['mod_dias_licencia'];
			$dcom = $datomod['mod_dias_aprocomite'];
			$dcon = $datomod['mod_dias_contrato'];
			$dpre = $datomod['mod_dias_preliminar'];
			$dcons = $datomod['mod_dias_constructor'];
			if($dinm == ''){ $dinm = 0; }
			if($dvis == ''){ $dvis = 0; }
			if($ddis == ''){ $ddis = 0; }
			if($dlic == ''){ $dlic = 0; }
			if($dcom == ''){ $dcom = 0; }
			if($dcon == ''){ $dcon = 0; }
			if($dpre == ''){ $dpre = 0; }
			if($dcons == ''){ $dcons = 0; }
			//Info Secun
			$codcaj = $dato['caj_codigo_cajero'];
			$cencos = $dato['cco_clave_int'];
			$refmaq = $dato['rem_clave_int'];
			$ubi = $dato['ubi_clave_int'];
			$codsuc = $dato['caj_codigo_suc'];
			$ubiamt = $dato['caj_ubicacion_atm'];
			$ati = $dato['ati_clave_int'];
			$rie = $dato['caj_riesgo'];
			$are = $dato['are_clave_int'];
			$codrec = $dato['caj_cod_recibido_monto'];
			$feccreacion = $dato['caj_fecha_creacion'];
			//Info Inmobiliaria
			$reqinm = $dato['cai_req_inmobiliaria'];
			$inmob = $dato['cai_inmobiliaria'];
			$feciniinmob = $dato['cai_fecha_ini_inmobiliaria'];
			$estinmob = $dato['esi_clave_int'];
			$fecentinmob = $dato['cai_fecha_entrega_info_inmobiliaria'];
			if($fecentinmob != '' and $fecentinmob != '0000-00-00')
			{
				$condiasinm = mysqli_query($conectar,"select DATEDIFF('".$fecentinmob."','".$feciniinmob."') dias from usuario LIMIT 1");
				$datodiasinm = mysqli_fetch_array($condiasinm);
				$diasinm = $datodiasinm['dias'];			
			}
			else
			{
				$confecteoinm = mysqli_query($conectar,"select ADDDATE('".$feciniinmob."', INTERVAL ".$dinm." DAY) dias from usuario LIMIT 1");
				$datofecteoinm = mysqli_fetch_array($confecteoinm);
				$fecteoinm = $datofecteoinm['dias'];
		
				$condiasinm = mysqli_query($conectar,"select DATEDIFF('".$fecteoinm."','".$feciniinmob."') dias from usuario LIMIT 1");
				$datodiasinm = mysqli_fetch_array($condiasinm);
				$diasinm = $datodiasinm['dias'];
			}
			//Info Visita Local
			$reqvis = $dato['cav_req_visita_local'];
			$vis = $dato['cav_visitante'];
			$fecvis = $dato['cav_fecha_visita'];
			$fecentvis = $dato['cav_fecha_entrega_informe'];
			$estvis = $dato['cav_estado'];
			if($fecentvis != '' and $fecentvis != '0000-00-00')
			{
				$condiasvis = mysqli_query($conectar,"select DATEDIFF('".$fecentvis."','".$fecvis."') dias from usuario LIMIT 1");
				$datodiasvis = mysqli_fetch_array($condiasvis);
				$diasvis = $datodiasvis['dias'];
			}
			else
			{
				$confecteovis = mysqli_query($conectar,"select ADDDATE('".$fecvis."', INTERVAL ".$dvis." DAY) dias from usuario LIMIT 1");
				$datofecteovis = mysqli_fetch_array($confecteovis);
				$fecteovis = $datofecteovis['dias'];
		
				$condiasvis = mysqli_query($conectar,"select DATEDIFF('".$fecteovis."','".$fecvis."') dias from usuario LIMIT 1");
				$datodiasvis = mysqli_fetch_array($condiasvis);
				$diasvis = $datodiasvis['dias'];
			}
			
			//Info Diseño
			$reqdis = $dato['cad_req_diseno'];
			$dis = $dato['cad_disenador'];
			$fecinidis = $dato['cad_fecha_inicio_diseno'];
			$estdis = $dato['esd_clave_int'];
			$fecentdis = $dato['cad_fecha_entrega_info_diseno'];
			if($fecentdis != '' and $fecentdis != '0000-00-00')
			{
				$condiasdis = mysqli_query($conectar,"select DATEDIFF('".$fecentdis."','".$fecinidis."') dias from usuario LIMIT 1");
			}
			else
			{
				$condiasdis = mysqli_query($conectar,"select DATEDIFF('".$fecteodis."','".$fecinidis."') dias from usuario LIMIT 1");
			}
			$datodiasdis = mysqli_fetch_array($condiasdis);
			$diasdis = $datodiasdis['dias'];
			//Info Licencia
			$reqlic = $dato['cal_req_licencia'];
			$lic = $dato['cal_gestionador'];
			$fecinilic = $dato['cal_fecha_inicio_licencia'];
			$estlic = $dato['esl_clave_int'];
			$fecentlic = $dato['cal_fecha_entrega_info_licencia'];
			if($fecentlic != '' and $fecentlic != '0000-00-00')
			{
				$condiaslic = mysqli_query($conectar,"select DATEDIFF('".$fecentlic."','".$fecinilic."') dias from usuario LIMIT 1");
			}
			else
			{
				$confecteolic = mysqli_query($conectar,"select ADDDATE('".$fecinilic."', INTERVAL ".$dlic." DAY) dias from usuario LIMIT 1");
				$datofecteolic = mysqli_fetch_array($confecteolic);
				$fecteolic = $datofecteolic['dias'];
		
				$condiaslic = mysqli_query($conectar,"select DATEDIFF('".$fecteolic."','".$fecinilic."') dias from usuario LIMIT 1");
			}
			$datodiaslic = mysqli_fetch_array($condiaslic);
			$diaslic = $datodiaslic['dias'];
			//Info Interventoria
			$int = $dato['cin_interventor'];
			$fecteoent = $dato['cin_fecha_teorica_entrega'];
			$feciniobra = $dato['cin_fecha_inicio_obra'];
			$fecpedsum = $dato['cin_fecha_pedido_suministro'];
			$aprovcotiz = $dato['cin_sw_aprov_cotizacion'];
			$cancom = $dato['can_clave_int'];
			$aprovliqui = $dato['cin_sw_aprov_liquidacion'];
			$swimgnex = $dato['cin_sw_img_nexos'];
			$swfot = $dato['cin_sw_fotos'];
			$swact = $dato['cin_sw_actas'];
			$estpro = $dato['caj_estado_proyecto'];
			//Info Constructor
			$cons = $dato['cac_constructor'];
			$fecentcons = $dato['cac_fecha_entrega_atm'];
			$poravance = $dato['cac_porcentaje_avance'];
			$fecentcot = $dato['cac_fecha_entrega_cotizacion'];
			$fecentliq = $dato['cac_fecha_entrega_liquidacion'];
			if($fecentcons != '' and $fecentcons != '0000-00-00')
			{
				$condiascons = mysqli_query($conectar,"select DATEDIFF('".$fecentcons."','".$feciniobra."') dias from usuario LIMIT 1");
				$datodiascons = mysqli_fetch_array($condiascons);
				$dcons = $datodiascons['dias'];
			}
			//Info Instalador Seguridad
			$seg = $dato['cas_instalador'];
			$fecingseg = $dato['cas_fecha_ingreso_obra_inst'];
			$codmon = $dato['cas_codigo_monitoreo'];
		?>
		<tr>
			<td class="alinearizq"><?php echo $clacaj; ?></td>
			<?php if($vernombrecajero == 'true'){ ?>
			<td class="alinearizq"><?php echo $nomcaj; ?></td>
			<?php } ?>
			<?php if($verdireccion == 'true'){ ?>
			<td class="alinearizq"><?php echo $dir; ?></td>
			<?php } ?>
			<?php if($veranocontable == 'true'){ ?>
			<td class="alinearizq"><?php echo $anocon; ?></td>
			<?php } ?>
			<?php if($verregion == 'true'){ ?>
			<td class="alinearizq">
			<?php 
				$sql = mysqli_query($conectar,"select pog_nombre from posicion_geografica where pog_clave_int = '".$reg."'");
				$datosql = mysqli_fetch_array($sql);
				$nomreg = $datosql['pog_nombre'];
				echo $nomreg; 
			?>
			</td>
			<?php } ?>
			<?php if($verdepartamento == 'true'){ ?>
			<td class="alinearizq">
			<?php 
				$sql = mysqli_query($conectar,"select pog_nombre from posicion_geografica where pog_clave_int = '".$dep."'");
				$datosql = mysqli_fetch_array($sql);
				$nomdep = $datosql['pog_nombre'];
				echo $nomdep;
			?>
			</td>
			<?php } ?>
			<?php if($vermunicipio == 'true'){ ?>
			<td class="alinearizq">
			<?php 
				$sql = mysqli_query($conectar,"select pog_nombre from posicion_geografica where pog_clave_int = '".$mun."'");
				$datosql = mysqli_fetch_array($sql);
				$nommun = $datosql['pog_nombre'];
				echo $nommun;
			?>
			</td>
			<?php } ?>
			<?php if($vertipologia == 'true'){ ?>
			<td class="alinearizq">
			<?php 
				$sql = mysqli_query($conectar,"select tip_nombre from tipologia where tip_clave_int = '".$tip."'");
				$datosql = mysqli_fetch_array($sql);
				$nomtip = $datosql['tip_nombre'];
				echo $nomtip;
			?>
			</td>
			<?php } ?>
			<?php if($verintervencion == 'true'){ ?>
			<td class="alinearizq">
			<?php 
				$sql = mysqli_query($conectar,"select tii_nombre from tipointervencion where tii_clave_int = '".$tipint."'");
				$datosql = mysqli_fetch_array($sql);
				$nomtii = $datosql['tii_nombre'];
				echo $nomtii;
			?>
			</td>
			<?php } ?>
			<?php if($vermodalidad == 'true'){ ?>
			<td class="alinearizq">
			<?php if($mod <= 0){ echo "Sin Definir "; }else{ echo $moda; } ?></td>
			<?php } ?>
			<?php if($verestadocajero == 'true'){ ?>
			<td class="alinearizq"><?php if($estpro == 1){ echo "ACTIVO"; }elseif($estpro == 2){ echo "SUSPENDIDO"; }elseif($estpro == 3){ echo "ENTREGADO"; }elseif($estpro == 4){ echo "CANCELADO"; }elseif($estpro == 5){ echo "PROGRAMADO"; } ?></td>
			<?php } ?>
			<?php if($vercodigocajero == 'true'){ ?>
			<td class="alinearizq"><?php echo $codcaj; ?></td>
			<?php } ?>
			<?php if($vercentrocostos == 'true'){ ?>
			<td class="alinearizq">
			<?php
			$concc = mysqli_query($conectar,"select cco_nombre from centrocostos where cco_clave_int = '".$cencos."'");
			$datocc = mysqli_fetch_array($concc);
			echo $datocc['cco_nombre'];
			?>
			</td>
			<?php } ?>
			<?php if($verreferencia == 'true'){ ?>
			<td class="alinearizq">
			<?php
			$conref = mysqli_query($conectar,"select rem_nombre from referenciamaquina where rem_clave_int = '".$refmaq."'");
			$datoref = mysqli_fetch_array($conref);
			echo $datoref['rem_nombre'];
			?>
			</td>
			<?php } ?>
			<?php if($verubicacion == 'true'){ ?>
			<td class="alinearizq">
			<?php
			$conubi = mysqli_query($conectar,"select ubi_descripcion from ubicacion where ubi_clave_int = '".$ubi."'");
			$datoubi = mysqli_fetch_array($conubi);
			echo $datoubi['ubi_descripcion'];
			?>
			</td>
			<?php } ?>
			<?php if($vercodigosuc == 'true'){ ?>
			<td class="alinearizq"><?php echo $codsuc; ?></td>
			<?php } ?>
			<?php if($verubicacionatm == 'true'){ ?>
			<td class="alinearizq"><?php if($ubiamt == 1){ echo 'EXTERNO'; }elseif($ubiamt == 2){ echo 'INTERNO'; }else{ echo 'N/A'; } ?></td>
			<?php } ?>
			<?php if($veratiende == 'true'){ ?>
			<td class="alinearizq">
			<?php
			$conati = mysqli_query($conectar,"select ati_nombre from atiende where ati_clave_int = '".$ati."'");
			$datoati = mysqli_fetch_array($conati);
			echo $datoati['ati_nombre'];
			?>
			</td>
			<?php } ?>
			<?php if($verriesgo == 'true'){ ?>
			<td class="alinearizq"><?php if($rie == 1){ echo 'ALTO'; }elseif($rie == 2){ echo 'BAJO'; }else{ echo 'N/A'; } ?></td>
			<?php } ?>
			<?php if($verarea == 'true'){ ?>
			<td class="alinearizq">
			<?php
			$conare = mysqli_query($conectar,"select are_nombre from area where are_clave_int = '".$are."'");
			$datoare = mysqli_fetch_array($conare);
			echo $datoare['are_nombre'];
			?>
			</td>
			<?php } ?>
			<?php if($verinmobiliaria == 'true'){ ?>
			<td class="alinearizq">
			<?php
			$coninm = mysqli_query($conectar,"select usu_nombre from usuario where usu_clave_int = '".$inmob."'");
			$datoinm = mysqli_fetch_array($coninm);
			echo $datoinm['usu_nombre'];
			?>
			</td>
			<?php } ?>
			<?php if($verestadoinm == 'true'){ ?>
			<td class="alinearizq">
			<?php
			$conestinmob = mysqli_query($conectar,"select esi_nombre from estado_inmobiliaria where esi_clave_int = '".$estinmob."'");
			$datoestinmob = mysqli_fetch_array($conestinmob);
			echo $datoestinmob['esi_nombre'];
			?>
			</td>
			<?php } ?>
			<?php if($verfechainicioinm == 'true'){ ?>
			<td class="alinearizq"><?php echo $feciniinmob; ?></td>
			<?php } ?>
			<?php if($verfechaentregainm == 'true'){ ?>
			<td class="alinearizq"><?php echo $fecentinmob; ?></td>
			<?php } ?>
			<?php if($vertotaldiasinm == 'true'){ ?>
			<td class="alinearizq"><?php echo $diasinm; ?></td>
			<?php } ?>
			<?php if($vernotasinm == 'true'){ ?>
			<td class="alinearizq">
			<?php
			$connot = mysqli_query($conectar,"select nou_nota from notausuario where ven_clave_int = 7 and caj_clave_int = ".$clacaj."");
			$numnot = mysqli_num_rows($connot);
			?>
			<div style="cursor:pointer" onmouseover="javascript: VentanaFlotante('<?php for($j = 0; $j < $numnot; $j++){ $datonot = mysqli_fetch_array($connot); echo $datonot['nou_nota']."<br>"; } if($numnot == 0){ echo "Sin notas"; } ?>', 300, <?php if($numnot == 0){ echo 35; }else{ echo 35*$numnot; } ?>)" onmouseout="javascript: quitarDiv();"><strong>...</strong></div>
			</td>
			<?php } ?>
			<?php if($vervisita == 'true'){ ?>
			<td class="alinearizq">
			<?php
			$convis = mysqli_query($conectar,"select usu_nombre from usuario where usu_clave_int = '".$vis."'");
			$datovis = mysqli_fetch_array($coninm);
			echo $datovis['usu_nombre'];
			?>
			</td>
			<?php } ?>
			<?php if($verestadovis == 'true'){ ?>
			<td class="alinearizq"><?php if($estvis == 1){ echo 'Activo'; }elseif($estvis == 2){ echo 'Entregado'; } ?></td>
			<?php } ?>
			<?php if($verfechainiciovis == 'true'){ ?>
			<td class="alinearizq"><?php echo $fecvis; ?></td>
			<?php } ?>
			<?php if($verfechaentregavis == 'true'){ ?>
			<td class="alinearizq"><?php echo $fecentvis; ?></td>
			<?php } ?>
			<?php if($vertotaldiasvis == 'true'){ ?>
			<td class="alinearizq"><?php echo $diasvis; ?></td>
			<?php } ?>
			<?php if($vernotasvis == 'true'){ ?>
			<td class="alinearizq">
			<?php
			$connot = mysqli_query($conectar,"select nou_nota from notausuario where ven_clave_int = 18 and caj_clave_int = ".$clacaj."");
			$numnot = mysqli_num_rows($connot);
			?>
			<div style="cursor:pointer" onmouseover="javascript: VentanaFlotante('<?php for($j = 0; $j < $numnot; $j++){ $datonot = mysqli_fetch_array($connot); echo $datonot['nou_nota']."<br>"; } if($numnot == 0){ echo "Sin notas"; } ?>', 300, <?php if($numnot == 0){ echo 35; }else{ echo 35*$numnot; } ?>)" onmouseout="javascript: quitarDiv();"><strong>...</strong></div>
			</td>
			<?php } ?>
			<?php if($verdisenador == 'true'){ ?>
			<td class="alinearizq">
			<?php
			$condis = mysqli_query($conectar,"select usu_nombre from usuario where usu_clave_int = '".$dis."'");
			$datodis = mysqli_fetch_array($condis);
			echo $datodis['usu_nombre'];
			?>
			</td>
			<?php } ?>
			<?php if($verestadodis == 'true'){ ?>
			<td class="alinearizq">
			<?php
			$conestdis = mysqli_query($conectar,"select esd_nombre from estado_diseno where esd_clave_int = '".$estdis."'");
			$datoestdis = mysqli_fetch_array($conestdis);
			echo $datoestdis['esd_nombre'];
			?>
			</td>
			<?php } ?>
			<?php if($verfechainiciodis == 'true'){ ?>
			<td class="alinearizq"><?php echo $fecinidis; ?></td>
			<?php } ?>
			<?php if($verfechaentregadis == 'true'){ ?>
			<td class="alinearizq"><?php echo $fecentdis; ?></td>
			<?php } ?>
			<?php if($vertotaldiasdis == 'true'){ ?>
			<td class="alinearizq"><?php echo $diasdis; ?></td>
			<?php } ?>
			<?php if($vernotasdis == 'true'){ ?>
			<td class="alinearizq">
			<?php
			$connot = mysqli_query($conectar,"select nou_nota from notausuario where ven_clave_int = 8 and caj_clave_int = ".$clacaj."");
			$numnot = mysqli_num_rows($connot);
			?>
			<div style="cursor:pointer" onmouseover="javascript: VentanaFlotante('<?php for($j = 0; $j < $numnot; $j++){ $datonot = mysqli_fetch_array($connot); echo $datonot['nou_nota']."<br>"; } if($numnot == 0){ echo "Sin notas"; } ?>', 300, <?php if($numnot == 0){ echo 35; }else{ echo 35*$numnot; } ?>)" onmouseout="javascript: quitarDiv();"><strong>...</strong></div>
			</td>
			<?php } ?>
			<?php if($vergestionador == 'true'){ ?>
			<td class="alinearizq">
			<?php
			$conlic = mysqli_query($conectar,"select usu_nombre from usuario where usu_clave_int = '".$lic."'");
			$datolic = mysqli_fetch_array($conlic);
			echo $datolic['usu_nombre'];
			?>
			</td>
			<?php } ?>
			<?php if($verestadolic == 'true'){ ?>
			<td class="alinearizq">
			<?php
			$conestlic = mysqli_query($conectar,"select esl_nombre from estado_licencia where esl_clave_int = '".$estlic."'");
			$datoestlic = mysqli_fetch_array($conestlic);
			echo $datoestlic['esl_nombre'];
			?>
			</td>
			<?php } ?>
			<?php if($verfechainiciolic == 'true'){ ?>
			<td class="alinearizq"><?php echo $fecinilic; ?></td>
			<?php } ?>
			<?php if($verfechaentregalic == 'true'){ ?>
			<td class="alinearizq"><?php echo $fecentlic; ?></td>
			<?php } ?>
			<?php if($vertotaldiaslic == 'true'){ ?>
			<td class="alinearizq"><?php echo $diaslic; ?></td>
			<?php } ?>
			<?php if($vernotaslic == 'true'){ ?>
			<td class="alinearizq">
			<?php
			$connot = mysqli_query($conectar,"select nou_nota from notausuario where ven_clave_int = 19 and caj_clave_int = ".$clacaj."");
			$numnot = mysqli_num_rows($connot);
			?>
			<div style="cursor:pointer" onmouseover="javascript: VentanaFlotante('<?php for($j = 0; $j < $numnot; $j++){ $datonot = mysqli_fetch_array($connot); echo $datonot['nou_nota']."<br>"; } if($numnot == 0){ echo "Sin notas"; } ?>', 300, <?php if($numnot == 0){ echo 35; }else{ echo 35*$numnot; } ?>)" onmouseout="javascript: quitarDiv();"><strong>...</strong></div>
			</td>
			<?php } ?>
			<?php if($verinterventor == 'true'){ ?>
			<td class="alinearizq">
			<?php
			$conint = mysqli_query($conectar,"select usu_nombre from usuario where usu_clave_int = '".$int."'");
			$datoint = mysqli_fetch_array($conint);
			echo $datoint['usu_nombre'];
			?>
			</td>
			<?php } ?>
			<?php if($veroperadorcanal == 'true'){ ?>
			<td class="alinearizq">
			<?php
			$concancom = mysqli_query($conectar,"select can_nombre from canal_comunicacion where can_clave_int = '".$cancom."'");
			$datocancom = mysqli_fetch_array($concancom);
			echo $datocancom['can_nombre'];
			?>
			</td>
			<?php } ?>
			<?php if($verfechainicioint == 'true'){ ?>
			<td class="alinearizq"><?php echo $feciniobra; ?></td>
			<?php } ?>
			<?php if($verfechaentregaint == 'true'){ ?>
			<td class="alinearizq"><?php echo $fecteoent; ?></td>
			<?php } ?>
			<?php if($verfechapedido == 'true'){ ?>
			<td class="alinearizq"><?php echo $fecpedsum; ?></td>
			<?php } ?>
			<?php if($vernotasint == 'true'){ ?>
			<td class="alinearizq">
			<?php
			$connot = mysqli_query($conectar,"select nou_nota from notausuario where ven_clave_int = 9 and caj_clave_int = ".$clacaj."");
			$numnot = mysqli_num_rows($connot);
			?>
			<div style="cursor:pointer" onmouseover="javascript: VentanaFlotante('<?php for($j = 0; $j < $numnot; $j++){ $datonot = mysqli_fetch_array($connot); echo $datonot['nou_nota']."<br>"; } if($numnot == 0){ echo "Sin notas"; } ?>', 300, <?php if($numnot == 0){ echo 35; }else{ echo 35*$numnot; } ?>)" onmouseout="javascript: quitarDiv();"><strong>...</strong></div>
			</td>
			<?php } ?>
			<?php if($verconstructor == 'true'){ ?>
			<td class="alinearizq">
			<?php
			$concons = mysqli_query($conectar,"select usu_nombre from usuario where usu_clave_int = '".$cons."'");
			$datocons = mysqli_fetch_array($concons);
			echo $datocons['usu_nombre'];
			?>
			</td>
			<?php } ?>
			<?php if($veravance == 'true'){ ?>
			<td class="alinearizq"><?php echo $poravance; ?></td>
			<?php } ?>
			<?php if($verfechainiciocons == 'true'){ ?>
			<td class="alinearizq"><?php echo $feciniobra; ?></td>
			<?php } ?>
			<?php if($verfechaentregacons == 'true'){ ?>
			<td class="alinearizq"><?php echo $fecentcons; ?></td>
			<?php } ?>
			<?php if($vertotaldiascons == 'true'){ ?>
			<td class="alinearizq"><?php echo $dcons; ?></td>
			<?php } ?>
			<?php if($vernotascons == 'true'){ ?>
			<td class="alinearizq">
			<?php
			$connot = mysqli_query($conectar,"select nou_nota from notausuario where ven_clave_int = 6 and caj_clave_int = ".$clacaj."");
			$numnot = mysqli_num_rows($connot);
			?>
			<div style="cursor:pointer" onmouseover="javascript: VentanaFlotante('<?php for($j = 0; $j < $numnot; $j++){ $datonot = mysqli_fetch_array($connot); echo $datonot['nou_nota']."<br>"; } if($numnot == 0){ echo "Sin notas"; } ?>', 300, <?php if($numnot == 0){ echo 35; }else{ echo 35*$numnot; } ?>)" onmouseout="javascript: quitarDiv();"><strong>...</strong></div>
			</td>
			<?php } ?>
		</tr>
		<tr>
			<td class="alinearizq" colspan="56">
			<hr>
			</td>
		</tr>
		<?php
		}
		?>
		</table>
		</div>
		</fieldset>
<?php
		$paginacion->render();
		exit();
	}
if($_GET['vermodalidades'] == 'si')
	{
		$tii = $_GET['tii'];
?>
		<select name="busmodalidad" id="busmodalidad" onchange="BUSCAR('CAJERO','')" tabindex="8" class="inputs" data-placeholder="-Seleccione-" style="width: 140px">
		<option value="">-Seleccione-</option>
		<?php
			$con = mysqli_query($conectar,"select * from modalidad where mod_clave_int in (select mod_clave_int from intervencion_modalidad where tii_clave_int = ".$tii.") order by mod_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clave = $dato['mod_clave_int'];
				$nombre = $dato['mod_nombre'];
		?>
			<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
		<?php
			}
		?>
		</select>

<?php
		exit();
	}
	if($_GET['verdepartamentos'] == 'si')
	{
		$reg = $_GET['reg'];
?>
		<select name="busdepartamento" id="busdepartamento" onchange="BUSCAR('CAJERO','');VERCIUDADES(this.value)" tabindex="7" data-placeholder="-Seleccione-" class="inputs" style="width: 140px">
		<option value="">-Seleccione-</option>
		<?php
			$con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo IS NOT NULL and pog_nieto IS NULL and pog_hijo = '".$reg."' order by pog_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clave = $dato['pog_clave_int'];
				$nombre = $dato['pog_nombre'];
		?>
			<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
		<?php
			}
		?>
		</select>
<?php
		exit();
	}
	if($_GET['verciudades'] == 'si')
	{
		$dep = $_GET['dep'];
?>
		<select name="busmunicipio" id="busmunicipio" onchange="BUSCAR('CAJERO','')" tabindex="8" data-placeholder="-Seleccione-" class="inputs" style="width: 140px">
		<option value="">-Seleccione-</option>
		<?php
			$con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo IS NULL and pog_nieto IS NOT NULL and pog_nieto = '".$dep."' order by pog_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clave = $dato['pog_clave_int'];
				$nombre = $dato['pog_nombre'];
		?>
			<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
		<?php
			}
		?>
		</select>
<?php
		exit();
	}
?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>SEGUIMIENTO CAJEROS</title>
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />

<?php //VALIDACIONES ?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="llamadas.js"></script>

<script type="text/javascript" src="../../js/jquery.searchabledropdown-1.0.8.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#buscentrocostos").searchable();
	$("#busregion").searchable();
	$("#busmunicipio").searchable();
	$("#bustipologia").searchable();
	$("#bustipointervencion").searchable();
	$("#busmodalidad").searchable();
	$("#busactor").searchable();
	$("#busestado").searchable();
});
function REFRESCARLISTACIUDADES()
{
	$(document).ready(function() {
		$("#busmunicipio").searchable();
	});
}
function REFRESCARLISTAMODALIDADES()
{
	$(document).ready(function() {
		$("#busmodalidad").searchable();
	});
}
function REFRESCARLISTADEPARTAMENTOS()
{
	$(document).ready(function() {
		$("#busdepartamento").searchable();
	});
}
function REFRESCARLISTACIUDADES()
{
	$(document).ready(function() {
		$("#busmunicipio").searchable();
	});
}
function COLORESTADO()
{
	var est = form1.busestado.value;
	if(est == 1 || est == 5)
	{
		document.getElementById("busestado").style.color="green";
		document.getElementById("busestado").style.fontWeight='bold';
	}
	else
	if(est == 2)
	{
		document.getElementById("busestado").style.color="orange";
		document.getElementById("busestado").style.fontWeight='bold';
	}
	else
	if(est == 3)
	{
		document.getElementById("busestado").style.color="#610D03";
		document.getElementById("busestado").style.fontWeight='bold';
	}
	else
	if(est == 4)
	{
		document.getElementById("busestado").style.color="blue";
		document.getElementById("busestado").style.fontWeight='bold';
	}
}
function verfiltro(o)
{
	if(o == 'INFOBASICA')
	{
		document.getElementById('verinfobasica').checked=false;
		$('#tabinfobasica').show(); //muestro mediante id
		$('#verinfobasica').hide(); //oculto mediante id
		$('#ocultarinfobasica').show(); //muestro mediante id
		document.getElementById('ocultarinfobasica').checked=true;
		$('#ocuinfobasica').val('1');
	}
	else
	if(o == 'INFOSECUNDARIA')
	{
		document.getElementById('verinfosecundaria').checked=false;
		$('#tabinfosecundaria').show(); //muestro mediante id
		$('#verinfosecundaria').hide(); //oculto mediante id
		$('#ocultarinfosecundaria').show(); //muestro mediante id
		document.getElementById('ocultarinfosecundaria').checked=true;
		$('#ocuinfosecundaria').val('1');
	}
	else
	if(o == 'INMOBILIARIA')
	{
		document.getElementById('verinmobiliaria').checked=false;
		$('#tabinmobiliaria').show(); //muestro mediante id
		$('#verinmobiliaria').hide(); //oculto mediante id
		$('#ocultarinmobiliaria').show(); //muestro mediante id
		document.getElementById('ocultarinmobiliaria').checked=true;
		$('#ocuinmobiliaria').val('1');
	}
	else
	if(o == 'VISITA')
	{
		document.getElementById('vervisita').checked=false;
		$('#tabvisita').show(); //muestro mediante id
		$('#vervisita').hide(); //oculto mediante id
		$('#ocultarvisita').show(); //muestro mediante id
		document.getElementById('ocultarvisita').checked=true;
		$('#ocuvisitalocal').val('1');
	}
	else
	if(o == 'DISENO')
	{
		document.getElementById('verdiseno').checked=false;
		$('#tabdiseno').show(); //muestro mediante id
		$('#verdiseno').hide(); //oculto mediante id
		$('#ocultardiseno').show(); //muestro mediante id
		document.getElementById('ocultardiseno').checked=true;
		$('#ocudiseno').val('1');
	}
	else
	if(o == 'LICENCIA')
	{
		document.getElementById('verlicencia').checked=false;
		$('#tablicencia').show(); //muestro mediante id
		$('#verlicencia').hide(); //oculto mediante id
		$('#ocultarlicencia').show(); //muestro mediante id
		document.getElementById('ocultarlicencia').checked=true;
		$('#oculicencia').val('1');
	}
	else
	if(o == 'INTERVENTORIA')
	{
		document.getElementById('verinterventoria').checked=false;
		$('#tabinterventoria').show(); //muestro mediante id
		$('#verinterventoria').hide(); //oculto mediante id
		$('#ocultarinterventoria').show(); //muestro mediante id
		document.getElementById('ocultarinterventoria').checked=true;
		$('#ocuinterventoria').val('1');
	}
	else
	if(o == 'CONSTRUCTOR')
	{
		document.getElementById('verconstructor').checked=false;
		$('#tabconstructor').show(); //muestro mediante id
		$('#verconstructor').hide(); //oculto mediante id
		$('#ocultarconstructor').show(); //muestro mediante id
		document.getElementById('ocultarconstructor').checked=true;
		$('#ocuconstructor').val('1');
	}
}
function ocultarfiltro(o)
{
	if(o == 'INFOBASICA')
	{
		$('#tabinfobasica').hide();
		$('#verinfobasica').show();
		$('#ocultarinfobasica').hide();
		document.getElementById('verinfobasica').checked=false;
		document.getElementById('ocultarinfobasica').checked=true;
		$('#ocuinfobasica').val('0');
	}
	else
	if(o == 'INFOSECUNDARIA')
	{
		$('#tabinfosecundaria').hide();
		$('#verinfosecundaria').show();
		$('#ocultarinfosecundaria').hide();
		document.getElementById('verinfosecundaria').checked=false;
		document.getElementById('ocultarinfosecundaria').checked=true;
		$('#ocuinfosecundaria').val('0');
	}
	else
	if(o == 'INMOBILIARIA')
	{
		$('#tabinmobiliaria').hide();
		$('#verinmobiliaria').show();
		$('#ocultarinmobiliaria').hide();
		document.getElementById('verinmobiliaria').checked=false;
		document.getElementById('ocultarinmobiliaria').checked=true;
		$('#ocuinmobiliaria').val('0');
	}
	else
	if(o == 'VISITA')
	{
		$('#tabvisita').hide();
		$('#vervisita').show();
		$('#ocultarvisita').hide();
		document.getElementById('vervisita').checked=false;
		document.getElementById('ocultarvisita').checked=true;
		$('#ocuvisitalocal').val('0');
	}
	else
	if(o == 'DISENO')
	{
		$('#tabdiseno').hide();
		$('#verdiseno').show();
		$('#ocultardiseno').hide();
		document.getElementById('verdiseno').checked=false;
		document.getElementById('ocultardiseno').checked=true;
		$('#ocudiseno').val('0');
	}
	else
	if(o == 'LICENCIA')
	{
		$('#tablicencia').hide();
		$('#verlicencia').show();
		$('#ocultarlicencia').hide();
		document.getElementById('verlicencia').checked=false;
		document.getElementById('ocultarlicencia').checked=true;
		$('#oculicencia').val('0');
	}
	else
	if(o == 'INTERVENTORIA')
	{
		$('#tabinterventoria').hide();
		$('#verinterventoria').show();
		$('#ocultarinterventoria').hide();
		document.getElementById('verinterventoria').checked=false;
		document.getElementById('ocultarinterventoria').checked=true;
		$('#ocuinterventoria').val('0');
	}
	else
	if(o == 'CONSTRUCTOR')
	{
		$('#tabconstructor').hide();
		$('#verconstructor').show();
		$('#ocultarconstructor').hide();
		document.getElementById('verconstructor').checked=false;
		document.getElementById('ocultarconstructor').checked=true;
		$('#ocuconstructor').val('0');
	}
}
var IE = document.all ? true : false;
if (!IE) {
    document.captureEvents(Event.MOUSEMOVE);
}
document.onmousemove = getMouseXY;
var tempX = 0;
var tempY = 0;
//esta funcion no necesitas entender solo asigna la posicion del raton a tempX y tempY
function getMouseXY(e){
    if (IE) { //para IE
        tempX = event.clientX + document.body.scrollLeft;
        tempY = event.clientY + document.body.scrollTop;
    }
    else { //para netscape
        tempX = e.pageX;
        tempY = e.pageY;
    }
    if (tempX < 0) {
        tempX = 0;
    }
    if (tempY < 0) {
        tempY = 0;
    }
    return true;
}
function VentanaFlotante(mensaje, x, y){
    //creo el objeto div
    var div_fl = document.createElement('DIV');
    //le asigno que su posicion sera abosoluta
    div_fl.style.position = 'absolute';
    //le asigno el ide a la ventana
    div_fl.id = 'Miventana';
    //digo en que posicion left y top se creara a partir de la posicion del raton tempx tempy
    div_fl.style.left = tempX + 'px';
    div_fl.style.top = tempY + 'px';
    //asigno el ancho del div pasado por parametro
    div_fl.style.width = x + 'px';
    //asigno el alto del div pasado por parametro
    div_fl.style.height = y + 'px';
    //digo con css que el borde sera de grosor 1px solido y de color negro
    div_fl.style.border = "1px solid #000000";
    //asigno el color de fondo
    div_fl.style.backgroundColor = "#cccccc";
    //el objeto añado a la estrutura principal el document.body
    document.body.appendChild(div_fl);
    //el mensaje pasado por parametro muestro dentro del div
    div_fl.innerHTML = mensaje;
}
function quitarDiv()
{
//creo el objeto del div
var mv = document.getElementById('Miventana');
//elimino el objeto
document.body.removeChild(mv);
}
</script>

<?php //CALENDARIO ?>
<link type="text/css" rel="stylesheet" href="../../css/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<SCRIPT type="text/javascript" src="../../js/dhtmlgoodies_calendar.js?random=20060118"></script>

<link rel="stylesheet" href="../../css/jquery-ui.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

<script>
function OCULTARSCROLL()
{
	parent.autoResize('iframe30');
	setTimeout("parent.autoResize('iframe30')",500);
	setTimeout("parent.autoResize('iframe30')",1000);
	setTimeout("parent.autoResize('iframe30')",1500);
	setTimeout("parent.autoResize('iframe30')",2000);
	setTimeout("parent.autoResize('iframe30')",2500);
	setTimeout("parent.autoResize('iframe30')",3000);
	setTimeout("parent.autoResize('iframe30')",3500);
	setTimeout("parent.autoResize('iframe30')",4000);
	setTimeout("parent.autoResize('iframe30')",4500);
	setTimeout("parent.autoResize('iframe30')",5000);
	setTimeout("parent.autoResize('iframe30')",5500);
	setTimeout("parent.autoResize('iframe30')",6000);
	setTimeout("parent.autoResize('iframe30')",6500);
	setTimeout("parent.autoResize('iframe30')",7000);
	setTimeout("parent.autoResize('iframe30')",7500);
	setTimeout("parent.autoResize('iframe30')",8000);
	setTimeout("parent.autoResize('iframe30')",8500);
	setTimeout("parent.autoResize('iframe30')",9000);
	setTimeout("parent.autoResize('iframe30')",9500);
	setTimeout("parent.autoResize('iframe30')",10000);
}
parent.autoResize('iframe30');
setTimeout("parent.autoResize('iframe30')",500);
setTimeout("parent.autoResize('iframe30')",1000);
setTimeout("parent.autoResize('iframe30')",1500);
setTimeout("parent.autoResize('iframe30')",2000);
setTimeout("parent.autoResize('iframe30')",2500);
setTimeout("parent.autoResize('iframe30')",3000);
setTimeout("parent.autoResize('iframe30')",3500);
setTimeout("parent.autoResize('iframe30')",4000);
setTimeout("parent.autoResize('iframe30')",4500);
setTimeout("parent.autoResize('iframe30')",5000);
setTimeout("parent.autoResize('iframe30')",5500);
setTimeout("parent.autoResize('iframe30')",6000);
setTimeout("parent.autoResize('iframe30')",6500);
setTimeout("parent.autoResize('iframe30')",7000);
setTimeout("parent.autoResize('iframe30')",7500);
setTimeout("parent.autoResize('iframe30')",8000);
setTimeout("parent.autoResize('iframe30')",8500);
setTimeout("parent.autoResize('iframe30')",9000);
setTimeout("parent.autoResize('iframe30')",9500);
setTimeout("parent.autoResize('iframe30')",10000);
</script>
<?php //********ESTAS LIBRERIAS JS Y CSS SIRVEN PARA HACER LA BUSQUEDA DINAMICA CON CHECKLIST************//?>
<link rel="stylesheet" type="text/css" href="../../css/checklist/jquery.multiselect.css" />
<link rel="stylesheet" type="text/css" href="../../css/checklist/jquery.multiselect.filter.css" />
<link rel="stylesheet" type="text/css" href="../../css/checklist/styleselect.css" />
<link rel="stylesheet" type="text/css" href="../../css/checklist/prettify.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script type="text/javascript" src="../../js/checklist/jquery.multiselect.js"></script>
<script type="text/javascript" src="../../js/checklist/jquery.multiselect.filter.js"></script>
<script type="text/javascript" src="../../js/checklist/prettify.js"></script>
<?php //**************************************************************************************************//?>

</head>
<body style="background-color:#FDFDFC" id="content">
<form name="form1" id="form1" method="post">
<!--[if lte IE 7]>
<div class="ieWarning">Este navegador no es compatible con el sistema. Por favor, use Chrome, Safari, Firefox o Internet Explorer 8 o superior.</div>
<![endif]-->
	<input name="ocultoestado" id="ocultoestado" value="" type="hidden" />
	<table style="width: 100%">
		<tr>
			<td class="auto-style2" colspan="5" align="center">
			<div id="cajeros">
				<table style="width:100%">
			<tr>
				<td align="left" class="auto-style2">
				<fieldset name="Group1">
				<legend align="center"><strong>FILTRO</strong></legend>
					<table style="width: 50%">
						<tr>
							<td style="height: 31px;" class="alinearizq" colspan="6">
							
							<table style="width: 90%">
								<tr>
									<td class="alinearizq"><strong>CAJERO:</strong></td>
									<td>
									<select multiple="multiple" onchange="BUSCAR('CAJERO','')" name="buscajero" id="buscajero" style="width:320px">
									<?php
										$con = mysqli_query($conectar,"select * from cajero where caj_sw_eliminado = 0 order by caj_nombre");
										$num = mysqli_num_rows($con);
										for($i = 0; $i < $num; $i++)
										{
											$dato = mysqli_fetch_array($con);
											$clave = $dato['caj_clave_int'];
											$nombre = $dato['caj_nombre'];
									?>
										<option value="<?php echo $clave; ?>"><?php echo $clave." - ".$nombre; ?></option>
									<?php
										}
									?>
									</select>
									</td>
									<td class="alinearizq"><strong>FEC.INICIO:</strong></td>
									<td><input name="fechainicio" id="fechainicio" placeholder="Desde" onchange="BUSCAR('CAJERO','')" readonly="readonly" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px;cursor:pointer"></td>
									<td><input name="fechafin" id="fechafin" placeholder="Hasta" onchange="BUSCAR('CAJERO','')" readonly="readonly" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px;cursor:pointer"></td>
									<td class="alinearizq"><strong>FEC.ENTREGA:</strong></td>
									<td><input name="fechainicioentrega" id="fechainicioentrega" placeholder="Desde" onchange="BUSCAR('CAJERO','')" readonly="readonly" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px;cursor:pointer"></td>
									<td><input name="fechafinentrega" id="fechafinentrega" placeholder="Hasta" onchange="BUSCAR('CAJERO','')" readonly="readonly" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px;cursor:pointer"></td>
								</tr>
							</table>
							</td>
						</tr>
						<tr>
							<td colspan="6">
							<hr>
							</td>
						</tr>
						<tr>
							<td style="height: 31px" class="alinearizq" colspan="6">
							<fieldset name="Group1">
									<legend><strong>INFORMACIÓN BÁSICA<input name="verinfobasica" id="verinfobasica" onclick="verfiltro('INFOBASICA');BUSCAR('CAJERO','')" type="checkbox"><input name="ocultarinfobasica" id="ocultarinfobasica" style="display:none" onclick="ocultarfiltro('INFOBASICA');BUSCAR('CAJERO','')" type="checkbox"><input name="ocuinfobasica" id="ocuinfobasica" value="0" type="hidden" /></strong></legend>
									<table style="width: 100%;display:none" id="tabinfobasica">
										<tr>
											<td style="width: 400px" class="alinearizq">Nombre Cajero:</td>
											<td style="width: 400px"><input name="vernombrecajero" id="vernombrecajero" type="checkbox" onclick="BUSCAR('CAJERO','')" /></td>
											<td colspan="4">
											<input name="busnombre" id="busnombre" onkeyup="BUSCAR('CAJERO','')" tabindex="1" class="auto-style5" type="text" style="width: 325px" size="20"></td>
											<td class="alinearizq">
											Dirección:</td>
											<td>
											<input name="verdireccion" id="verdireccion" type="checkbox" onclick="BUSCAR('CAJERO','')" />
											</td>
											<td colspan="4">
											<input name="busdireccion" id="busdireccion" onkeyup="BUSCAR('CAJERO','')" tabindex="5" class="inputs" type="text" style="width: 390px"></td>
											<td>
											&nbsp;</td>
											<td>
											&nbsp;</td>
											<td>
											&nbsp;</td>
										</tr>
										<tr>
											<td style="width: 400px" class="alinearizq">Estado 
											Proyecto:</td>
											<td style="width: 400px"><input name="verestadocajero" id="verestadocajero" type="checkbox" onclick="BUSCAR('CAJERO','')" /></td>
											<td style="width: 168px">
											<select name="busestado" id="busestado" onchange="BUSCAR('CAJERO','');COLORESTADO()" tabindex="20" class="inputs" data-placeholder="-Seleccione-" style="width: 140px; color:green; font-weight:bold">
											<option value="">-Seleccione-</option>
											<option value="1">Activo</option>
											<option value="5">Programado</option>
											<option value="2">Suspendido</option>
											<option value="3">Entregado</option>
											<option value="4">Cancelado</option>
											</select></td>
											<td style="width: 98px" class="alinearizq">Año Contable:</td>
											<td style="width: 98px"><input name="veranocontable" id="veranocontable" type="checkbox" onclick="BUSCAR('CAJERO','')" /></td>
											<td style="width: 164px">
											<input name="busanocontable" id="busanocontable" onkeyup="BUSCAR('CAJERO','')" tabindex="2" class="inputs" type="text" style="width: 60px" size="20"></td>
											<td class="alinearizq">
											Región:</td>
											<td>
											<input name="verregion" id="verregion" type="checkbox" onclick="BUSCAR('CAJERO','')" />
											</td>
											<td style="width: 159px">
											<select name="busregion" id="busregion" onchange="BUSCAR('CAJERO','');VERDEPARTAMENTOS(this.value)" tabindex="6" data-placeholder="-Seleccione-" class="inputs" style="width: 140px">
											<option value="">-Seleccione-</option>
											<?php
												$con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo IS NULL and pog_nieto IS NULL order by pog_nombre");
												$num = mysqli_num_rows($con);
												for($i = 0; $i < $num; $i++)
												{
													$dato = mysqli_fetch_array($con);
													$clave = $dato['pog_clave_int'];
													$nombre = $dato['pog_nombre'];
											?>
												<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
											<?php
												}
											?>
											</select>
											</td>
											<td style="width: 74px" class="alinearizq">
									Departamento:</td>
											<td style="width: 74px">
											<input name="verdepartamento" id="verdepartamento" type="checkbox" onclick="BUSCAR('CAJERO','')" />
											</td>
											<td>
											<div id="departamentos" style="float:left">
											<select name="busdepartamento" id="busdepartamento" onchange="BUSCAR('CAJERO','');VERCIUDADES(this.value)" tabindex="7" class="inputs" data-placeholder="-Seleccione-" style="width: 145px">
											<option value="">-Seleccione-</option>
											<?php
												$con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo IS NOT NULL and pog_nieto IS NULL order by pog_nombre");
												$num = mysqli_num_rows($con);
												for($i = 0; $i < $num; $i++)
												{
													$dato = mysqli_fetch_array($con);
													$clave = $dato['pog_clave_int'];
													$nombre = $dato['pog_nombre'];
											?>
												<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
											<?php
												}
											?>
											</select>
											</div>
											</td>
											<td class="alinearizq">
											Municipío:</td>
											<td>
											<input name="vermunicipio" id="vermunicipio" type="checkbox" onclick="BUSCAR('CAJERO','')" />
											</td>
											<td>
											<div id="ciudades" style="float:left">
											<select name="busmunicipio" id="busmunicipio" onchange="BUSCAR('CAJERO','')" tabindex="8" class="inputs" data-placeholder="-Seleccione-" style="width: 140px">
											<option value="">-Seleccione-</option>
											<?php
												$con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo IS NULL and pog_nieto IS NOT NULL order by pog_nombre");
												$num = mysqli_num_rows($con);
												for($i = 0; $i < $num; $i++)
												{
													$dato = mysqli_fetch_array($con);
													$clave = $dato['pog_clave_int'];
													$nombre = $dato['pog_nombre'];
											?>
												<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
											<?php
												}
											?>
											</select>
											</div>
											</td>
										</tr>
										<tr>
											<td style="width: 400px" class="alinearizq">Tipología:</td>
											<td style="width: 400px"><input name="vertipologia" id="vertipologia" type="checkbox" onclick="BUSCAR('CAJERO','')" /></td>
											<td colspan="4">
									<div id="mitipologia">
									<select name="bustipologia" id="bustipologia" onchange="BUSCAR('CAJERO','')" tabindex="9" data-placeholder="-Seleccione-" class="inputs" style="width: 320px">
									<option value="">-Seleccione-</option>
									<?php
										$con = mysqli_query($conectar,"select * from tipologia where tip_sw_activo = 1 order by tip_nombre");
										$num = mysqli_num_rows($con);
										for($i = 0; $i < $num; $i++)
										{
											$dato = mysqli_fetch_array($con);
											$clave = $dato['tip_clave_int'];
											$nombre = $dato['tip_nombre'];
									?>
										<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
									<?php
										}
									?>
									</select>
									</div>
									<div id="cualtip"></div>
									</td>
											<td class="alinearizq">
											Tipo 
									Intervención:</td>
											<td>
											<input name="verintervencion" id="verintervencion" type="checkbox" onclick="BUSCAR('CAJERO','')" />
											</td>
											<td colspan="4">
									<div id="mitipointervencion">
									<select name="bustipointervencion" id="bustipointervencion" onchange="BUSCAR('CAJERO','');VERMODALIDADES(this.value)" tabindex="17" data-placeholder="-Seleccione-" class="inputs" style="width: 390px">
									<option value="">-Seleccione-</option>
									<?php
										$con = mysqli_query($conectar,"select * from tipointervencion order by tii_nombre");
										$num = mysqli_num_rows($con);
										for($i = 0; $i < $num; $i++)
										{
											$dato = mysqli_fetch_array($con);
											$clave = $dato['tii_clave_int'];
											$nombre = $dato['tii_nombre'];
									?>
										<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
									<?php
										}
									?>
									</select>
									</div>
									<div id="cualtipint"></div>
									</td>
										<td class="alinearizq">Modalidad:</td>
										<td>
										<input name="vermodalidad" id="vermodalidad" type="checkbox" onclick="BUSCAR('CAJERO','')" />
										</td>
										<td>
										<div id="modalidades" style="float:left">
										<select name="busmodalidad" id="busmodalidad" onchange="BUSCAR('CAJERO','')" tabindex="8" class="inputs" data-placeholder="-Seleccione-" style="width: 140px">
										<option value="">-Seleccione-</option>
										</select>
										</div>
										<div id="modalidadactor">
										</div>
										</td>
									</tr>
									</table>
								</fieldset>
							</td>
						</tr>
						<tr>
							<td colspan="6">
							<hr>
							</td>
						</tr>
						<tr>
							<td style="height: 31px;" class="alinearizq" colspan="6">
							<fieldset name="Group1">
											<legend><strong>INFORMACIÓN SECUNDARIA<input name="verinfosecundaria" id="verinfosecundaria" onclick="verfiltro('INFOSECUNDARIA');BUSCAR('CAJERO','')" type="checkbox"><input name="ocultarinfosecundaria" id="ocultarinfosecundaria" style="display:none" onclick="ocultarfiltro('INFOSECUNDARIA');BUSCAR('CAJERO','')" type="checkbox"><input name="ocuinfosecundaria" id="ocuinfosecundaria" value="0" type="hidden" /></strong></legend>
										<table style="width: 100%;display:none" id="tabinfosecundaria">
											<tr>
										<td class="alinearizq" style="width: 90px">Código Cajero:</td>
										<td class="alinearizq" style="width: 5px">
										<input name="vercodigocajero" id="vercodigocajero" type="checkbox" onclick="BUSCAR('CAJERO','')" />
										</td>
										<td class="alinearizq" style="width: 6px">
										<input name="buscodigo" id="buscodigo" onkeyup="BUSCAR('CAJERO','')" tabindex="2" class="inputs" type="text" style="width: 140px"></td>
										<td class="alinearizq" style="width: 81px">
										Centro Costos:</td>
										<td class="alinearizq" style="width: 5px">
										<input name="vercentrocostos" id="vercentrocostos" type="checkbox" onclick="BUSCAR('CAJERO','')" />
										</td>
										<td class="alinearizq" style="width: 100px">
										<div id="micentrocostos">
										<select name="buscentrocostos" id="buscentrocostos" onchange="BUSCAR('CAJERO','')" tabindex="4" class="inputs" data-placeholder="-Seleccione-" style="width: 140px">
										<option value="">-Seleccione-</option>
										<?php
											$con = mysqli_query($conectar,"select * from centrocostos where cco_sw_activo = 1 order by cco_nombre");
											$num = mysqli_num_rows($con);
											for($i = 0; $i < $num; $i++)
											{
												$dato = mysqli_fetch_array($con);
												$clave = $dato['cco_clave_int'];
												$nombre = $dato['cco_nombre'];
										?>
											<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
										<?php
											}
										?>
										</select>
										</div>
										<div id="cualcc"></div>
										</td>
										<td class="alinearizq" style="width: 88px">
										Referencia 
										Maquina:</td>
										<td class="alinearizq" style="width: 5px">
										<input name="verreferencia" id="verreferencia" type="checkbox" onclick="BUSCAR('CAJERO','')" />
										</td>
										<td class="alinearizq" style="width: 155px">
										<div id="mireferenciamaquina">
										<select name="busreferenciamaquina" id="busreferenciamaquina" onchange="BUSCAR('CAJERO','')" tabindex="10" class="inputs" data-placeholder="-Seleccione-" style="width: 140px">
										<option value="">-Seleccione-</option>
										<?php
											$con = mysqli_query($conectar,"select * from referenciamaquina where rem_sw_activo = 1 order by rem_nombre");
											$num = mysqli_num_rows($con);
											for($i = 0; $i < $num; $i++)
											{
												$dato = mysqli_fetch_array($con);
												$clave = $dato['rem_clave_int'];
												$nombre = $dato['rem_nombre'];
										?>
											<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
										<?php
											}
										?>
										</select>
										</div>
										<div id="cualref"></div>
										</td>
									</tr>
									<tr>
										<td class="alinearizq" style="width: 90px">Ubicación 
										(SUC, CC, REMOTO):</td>
										<td class="alinearizq" style="width: 5px">
										<input name="verubicacion" id="verubicacion" type="checkbox" onclick="BUSCAR('CAJERO','')" />
										</td>
										<td class="alinearizq" style="width: 6px">
										<div id="miubicacion">
										<select name="busubicacion" id="busubicacion" onchange="BUSCAR('CAJERO','')" tabindex="11" class="inputs" data-placeholder="-Seleccione-" style="width: 140px">
										<option value="">-Seleccione-</option>
										<?php
											$con = mysqli_query($conectar,"select * from ubicacion where ubi_sw_activo = 1 order by ubi_descripcion");
											$num = mysqli_num_rows($con);
											for($i = 0; $i < $num; $i++)
											{
												$dato = mysqli_fetch_array($con);
												$clave = $dato['ubi_clave_int'];
												$nombre = $dato['ubi_descripcion'];
										?>
											<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
										<?php
											}
										?>
										</select>
										</div>
										<div id="cualubi">
										</div>
										</td>
										<td class="alinearizq" style="width: 81px">Código de 
										Suc:</td>
										<td class="alinearizq" style="width: 5px">
										<input name="vercodigosuc" id="vercodigosuc" type="checkbox" onclick="BUSCAR('CAJERO','')" />
										</td>
										<td class="alinearizq" style="width: 100px">
										<input name="buscodigosuc" id="buscodigosuc" onkeyup="BUSCAR('CAJERO','')" tabindex="12" class="inputs" type="text" style="width: 140px"></td>
										<td class="alinearizq" style="width: 88px">Ubicación 
										ATM:</td>
										<td class="alinearizq" style="width: 5px">
										<input name="verubicacionatm" id="verubicacionatm" type="checkbox" onclick="BUSCAR('CAJERO','')" />
										</td>
										<td class="alinearizq" style="width: 155px">
										<select name="busubicacionamt" id="busubicacionamt" onchange="BUSCAR('CAJERO','')" tabindex="13" class="inputs" style="width: 140px">
										<option value="">-Seleccione-</option>
										<option value="1">EXTERNO</option>
										<option value="2">INTERNO</option>
										<option value="0">N/A</option>
										</select></td>
									</tr>
									<tr>
										<td class="alinearizq" style="width: 90px">Atiende:</td>
										<td class="alinearizq" style="width: 5px">
										<input name="veratiende" id="veratiende" type="checkbox" onclick="BUSCAR('CAJERO','')" />
										</td>
										<td class="alinearizq" style="width: 6px">
										<div id="miatiende">
										<select name="busatiende" id="busatiende" onchange="BUSCAR('CAJERO','')" tabindex="14" class="inputs" data-placeholder="-Seleccione-" style="width: 140px">
										<option value="">-Seleccione-</option>
										<?php
											$con = mysqli_query($conectar,"select * from atiende where ati_sw_activo = 1 order by ati_nombre");
											$num = mysqli_num_rows($con);
											for($i = 0; $i < $num; $i++)
											{
												$dato = mysqli_fetch_array($con);
												$clave = $dato['ati_clave_int'];
												$nombre = $dato['ati_nombre'];
										?>
											<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
										<?php
											}
										?>
										</select>
										</div>
										<div id="cualati"></div>
										</td>
										<td class="alinearizq" style="width: 81px">Riesgo:</td>
										<td class="alinearizq" style="width: 5px">
										<input name="verriesgo" id="verriesgo" type="checkbox" onclick="BUSCAR('CAJERO','')" />
										</td>
										<td class="alinearizq" style="width: 100px">
										<select name="busriesgo" id="busriesgo" onchange="BUSCAR('CAJERO','')" tabindex="15" class="inputs" style="width: 140px">
										<option value="">-Seleccione-</option>
										<option value="1">ALTO</option>
										<option value="2">BAJO</option>
										<option value="0">N/A</option>
										</select></td>
										<td class="alinearizq" style="width: 88px">Area:</td>
										<td class="alinearizq" style="width: 5px">
										<input name="verarea" id="verarea" type="checkbox" onclick="BUSCAR('CAJERO','')" />
										</td>
										<td class="alinearizq" style="width: 155px">
										<div id="miarea">
										<select name="busarea" id="busarea" onchange="BUSCAR('CAJERO','')" tabindex="16" class="inputs" data-placeholder="-Seleccione-" style="width: 140px">
										<option value="">-Seleccione-</option>
										<?php
											$con = mysqli_query($conectar,"select * from area where are_sw_activo = 1 order by are_nombre");
											$num = mysqli_num_rows($con);
											for($i = 0; $i < $num; $i++)
											{
												$dato = mysqli_fetch_array($con);
												$clave = $dato['are_clave_int'];
												$nombre = $dato['are_nombre'];
										?>
											<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
										<?php
											}
										?>
										</select>
										</div>
										<div id="cualare"></div>
										</td>
									</tr>
									<tr>
										<td class="alinearizq" style="width: 90px">
										Código Recibido Monto:</td>
										<td class="alinearizq" style="width: 5px">
										<input name="vercodigorecibido" id="vercodigorecibido" type="checkbox" onclick="BUSCAR('CAJERO','')" />
										</td>
										<td class="alinearizq" style="width: 6px">
										<input name="buscodigorecibido" id="buscodigorecibido" onkeyup="BUSCAR('CAJERO','')" tabindex="42" class="inputs" type="text" style="width: 140px"></td>
										<td class="alinearizq" style="width: 81px">
										&nbsp;</td>
										<td class="alinearizq" style="width: 5px">
										&nbsp;</td>
										<td class="alinearizq" style="width: 100px">
										&nbsp;</td>
										<td class="alinearizq" style="width: 88px">
										&nbsp;</td>
										<td class="alinearizq" style="width: 5px">
										&nbsp;</td>
										<td class="alinearizq" style="width: 155px">
										&nbsp;</td>
									</tr>
									</table>
								</fieldset>
							</td>
						</tr>
						<tr>
							<td colspan="6">
							<hr>
							</td>
						</tr>
						<tr>
							<td style="height: 31px;" class="alinearizq" colspan="6">
							<fieldset name="Group1">
							<legend><strong>INMOBILIARIA<input name="verinmobiliaria" id="verinmobiliaria" onclick="verfiltro('INMOBILIARIA');BUSCAR('CAJERO','')" type="checkbox"><input name="ocultarinmobiliaria" id="ocultarinmobiliaria" style="display:none" onclick="ocultarfiltro('INMOBILIARIA');BUSCAR('CAJERO','')" type="checkbox"><input name="ocuinmobiliaria" id="ocuinmobiliaria" value="0" type="hidden" /></strong></legend>
							<table style="width: 100%;display:none" id="tabinmobiliaria">
								<tr>
									<td class="alinearizq" style="width: 90px">
									Nombre 
							Inmobiliaria:</td>
									<td class="alinearizq" style="width: 5px">
									<input name="verinmobiliaria" id="verinmobiliaria" type="checkbox" onclick="BUSCAR('CAJERO','')" />
									</td>
									<td class="alinearizq" style="width: 6px">
									<select name="busnombreinmobiliaria" id="busnombreinmobiliaria" onchange="BUSCAR('CAJERO','')" tabindex="20" class="inputs" data-placeholder="-Seleccione-" style="width: 140px">
									<option value="">-Seleccione-</option>
									<?php
										$con = mysqli_query($conectar,"select * from usuario where (usu_categoria = 2 and usu_sw_inmobiliaria = 1 and usu_sw_activo = 1) OR (usu_clave_int NOT IN (select usu_clave_int from usuario_actor) and usu_sw_inmobiliaria = 1 and usu_sw_activo = 1) order by usu_nombre");
										$num = mysqli_num_rows($con);
										for($i = 0; $i < $num; $i++)
										{
											$dato = mysqli_fetch_array($con);
											$clave = $dato['usu_clave_int'];
											$nombre = $dato['usu_nombre'];
									?>
										<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
									<?php
										}
									?>
									</select>
									</td>
									<td class="alinearizq" style="width: 81px">Estado:
									</td>
									<td class="alinearizq" style="width: 5px">
									<input name="verestadoinm" id="verestadoinm" type="checkbox" onclick="BUSCAR('CAJERO','')" />
									</td>
									<td class="alinearizq" style="width: 100px">
									<select name="busestadoinmobiliaria" id="busestadoinmobiliaria" onchange="BUSCAR('CAJERO','')" tabindex="17" class="inputs" style="width:148px">
									<option value="">-Seleccione-</option>
									<?php
										$con = mysqli_query($conectar,"select * from estado_inmobiliaria where esi_sw_activo = 1 order by esi_nombre");
										$num = mysqli_num_rows($con);
										for($i = 0; $i < $num; $i++)
										{
											$dato = mysqli_fetch_array($con);
											$clave = $dato['esi_clave_int'];
											$nombre = $dato['esi_nombre'];
									?>
										<option value="<?php echo $clave; ?>" <?php if($clave == $estinmob){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
									<?php
										}
									?>
									</select>
									</td>
									<td class="alinearizq" style="width: 88px">
									Fecha Inicio:</td>
									<td class="alinearizq" style="width: 5px">
									<input name="verfechainicioinm" id="verfechainicioinm" type="checkbox" onclick="BUSCAR('CAJERO','')" />
									</td>
									<td class="alinearizq" style="width: 155px">
									<input name="busfechainicioinmobiliaria" id="busfechainicioinmobiliaria" onkeyup="BUSCAR('CAJERO','')" onchange="BUSCAR('CAJERO','')" placeholder="Desde" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px">
									<input name="busfechafininmobiliaria" id="busfechafininmobiliaria" onkeyup="BUSCAR('CAJERO','')" onchange="BUSCAR('CAJERO','')" placeholder="Hasta" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px">
									</td>
								</tr>
								<tr>
									<td class="alinearizq" style="width: 88px">
									Fecha Entrega:</td>
									<td class="alinearizq" style="width: 5px">
									<input name="verfechaentregainm" id="verfechaentregainm" type="checkbox" onclick="BUSCAR('CAJERO','')" />
									</td>
									<td class="alinearizq" style="width: 155px">
									<input name="busfechaentregainiinfoinmobiliaria" id="busfechaentregainiinfoinmobiliaria" onkeyup="BUSCAR('CAJERO','')" onchange="BUSCAR('CAJERO','')" placeholder="Desde" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px">
									<input name="busfechaentregafininfoinmobiliaria" id="busfechaentregafininfoinmobiliaria" onkeyup="BUSCAR('CAJERO','')" onchange="BUSCAR('CAJERO','')" placeholder="Hasta" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px">
									</td>
									<td class="alinearizq" style="width: 90px">Total Días:</td>
									<td class="alinearizq" style="width: 5px">
									<input name="vertotaldiasinm" id="vertotaldiasinm" type="checkbox" onclick="BUSCAR('CAJERO','')" />
									</td>
									<td class="alinearizq" style="width: 6px">
									<input name="bustotaldiasinmobiliaria" id="bustotaldiasinmobiliaria" onkeyup="BUSCAR('CAJERO','')" tabindex="31" class="inputs" type="text" style="width: 140px">
									</td>
									<td class="alinearizq" style="width: 88px">
									Notas:</td>
									<td class="alinearizq" style="width: 5px">
									<input name="vernotasinm" id="vernotasinm" type="checkbox" onclick="BUSCAR('CAJERO','')" />
									</td>
									<td class="alinearizq" style="width: 155px">
									<textarea cols="20" class="inputs" name="busnotasinmobiliaria" id="busnotasinmobiliaria" onkeyup="BUSCAR('CAJERO','')" rows="2"></textarea></td>
								</tr>
							</table>
							</fieldset>
							</td>
						</tr>
						<tr>
							<td colspan="6">
							<hr>
							</td>
						</tr>
						<tr>
							<td style="height: 31px;" class="alinearizq" colspan="6">
							<fieldset name="Group1">
							<legend><strong>VISITA LOCAL<input name="vervisita" id="vervisita" onclick="verfiltro('VISITA');BUSCAR('CAJERO','')" type="checkbox"><input name="ocultarvisita" id="ocultarvisita" style="display:none" onclick="ocultarfiltro('VISITA');BUSCAR('CAJERO','')" type="checkbox"><input name="ocuvisitalocal" id="ocuvisitalocal" value="0" type="hidden" /></strong></legend>
							<table style="width: 100%;display:none" id="tabvisita">
								<tr>
									<td class="alinearizq" style="width: 90px">
									Nombre 
							Visitante:</td>
									<td class="alinearizq" style="width: 5px">
									<input name="vervisita" id="vervisita" type="checkbox" class="auto-style8" onclick="BUSCAR('CAJERO','')" />
									</td>
									<td class="alinearizq" style="width: 6px">
									<select name="busnombrevisita" id="busnombrevisita" onchange="BUSCAR('CAJERO','')" tabindex="20" class="inputs" data-placeholder="-Seleccione-" style="width: 140px">
									<option value="">-Seleccione-</option>
									<?php
										$con = mysqli_query($conectar,"select * from usuario where (usu_categoria = 2 and usu_sw_visita = 1 and usu_sw_activo = 1) OR (usu_clave_int NOT IN (select usu_clave_int from usuario_actor) and usu_sw_visita = 1 and usu_sw_activo = 1) order by usu_nombre");
										$num = mysqli_num_rows($con);
										for($i = 0; $i < $num; $i++)
										{
											$dato = mysqli_fetch_array($con);
											$clave = $dato['usu_clave_int'];
											$nombre = $dato['usu_nombre'];
									?>
										<option value="<?php echo $clave; ?>" <?php if($clave == $vis){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
									<?php
										}
									?>
									</select>
									</td>
									<td class="alinearizq" style="width: 81px">Estado:
									</td>
									<td class="alinearizq" style="width: 5px">
									<input name="verestadovis" id="verestadovis" type="checkbox" onclick="BUSCAR('CAJERO','')" />
									</td>
									<td class="alinearizq" style="width: 100px">
									<select name="busestadovisita" id="busestadovisita" onchange="BUSCAR('CAJERO','')" tabindex="17" class="inputs" style="width:148px">
									<option value="">-Seleccione-</option>
									<option value="1">Activo</option>
									<option value="2">Entregado</option>
									</select>
									</td>
									<td class="alinearizq" style="width: 88px">
									Fecha Inicio:</td>
									<td class="alinearizq" style="width: 5px">
									<input name="verfechainiciovis" id="verfechainiciovis" type="checkbox" onclick="BUSCAR('CAJERO','')" />
									</td>
									<td class="alinearizq" style="width: 155px">
									<input name="busfechainiciovisita" id="busfechainiciovisita" onkeyup="BUSCAR('CAJERO','')" onchange="BUSCAR('CAJERO','')" placeholder="Desde" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px">
									<input name="busfechafinvisita" id="busfechafinvisita" onkeyup="BUSCAR('CAJERO','')" onchange="BUSCAR('CAJERO','')" placeholder="Hasta" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px">
									</td>
								</tr>
								<tr>
									<td class="alinearizq" style="width: 88px">
									Fecha Entrega:</td>
									<td class="alinearizq" style="width: 5px">
									<input name="verfechaentregavis" id="verfechaentregavis" type="checkbox" onclick="BUSCAR('CAJERO','')" />
									</td>
									<td class="alinearizq" style="width: 155px">
									<input name="busfechaentregainiinfovisita" id="busfechaentregainiinfovisita" onkeyup="BUSCAR('CAJERO','')" onchange="BUSCAR('CAJERO','')" placeholder="Desde" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px">
									<input name="busfechaentregafininfovisita" id="busfechaentregafininfovisita" onkeyup="BUSCAR('CAJERO','')" onchange="BUSCAR('CAJERO','')" placeholder="Hasta" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px">
									</td>
									<td class="alinearizq" style="width: 90px">Total Días:</td>
									<td class="alinearizq" style="width: 5px">
									<input name="vertotaldiasvis" id="vertotaldiasvis" type="checkbox" onclick="BUSCAR('CAJERO','')" />
									</td>
									<td class="alinearizq" style="width: 6px">
									<input name="bustotaldiasvisita" id="bustotaldiasvisita" onkeyup="BUSCAR('CAJERO','')" tabindex="31" class="inputs" type="text" style="width: 140px">
									</td>
									<td class="alinearizq" style="width: 88px">
									Notas:</td>
									<td class="alinearizq" style="width: 5px">
									<input name="vernotasvis" id="vernotasvis" type="checkbox" onclick="BUSCAR('CAJERO','')" />
									</td>
									<td class="alinearizq" style="width: 155px">
									<textarea cols="20" class="inputs" name="busnotasvisita" id="busnotasvisita" onkeyup="BUSCAR('CAJERO','')" rows="2"></textarea></td>
								</tr>
							</table>
							</fieldset>
							</td>
						</tr>
						<tr>
							<td colspan="6">
							<hr>
							</td>
						</tr>
						<tr>
							<td style="height: 31px;" class="alinearizq" colspan="6">
							<fieldset name="Group1">
							<legend><strong>DISEÑO<input name="verdiseno" id="verdiseno" onclick="verfiltro('DISENO');BUSCAR('CAJERO','')" type="checkbox"><input name="ocultardiseno" id="ocultardiseno" style="display:none" onclick="ocultarfiltro('DISENO');BUSCAR('CAJERO','')" type="checkbox"><input name="ocudiseno" id="ocudiseno" value="0" type="hidden" /></strong></legend>
							<table style="width: 100%;display:none" id="tabdiseno">
											<tr>
										<td class="alinearizq" style="width: 90px">
										Nombre 
								Diseñador:</td>
										<td class="alinearizq" style="width: 5px">
										<input name="verdisenador" id="verdisenador" type="checkbox" onclick="BUSCAR('CAJERO','')" />
										</td>
										<td class="alinearizq" style="width: 6px">
										<select name="busnombrediseno" id="busnombrediseno" onchange="BUSCAR('CAJERO','')" tabindex="20" class="inputs" data-placeholder="-Seleccione-" style="width: 140px">
										<option value="">-Seleccione-</option>
										<?php
											$con = mysqli_query($conectar,"select * from usuario where (usu_categoria = 2 and usu_sw_diseno = 1 and usu_sw_activo = 1) OR (usu_clave_int NOT IN (select usu_clave_int from usuario_actor) and usu_sw_diseno = 1 and usu_sw_activo = 1) order by usu_nombre");
											$num = mysqli_num_rows($con);
											for($i = 0; $i < $num; $i++)
											{
												$dato = mysqli_fetch_array($con);
												$clave = $dato['usu_clave_int'];
												$nombre = $dato['usu_nombre'];
										?>
											<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
										<?php
											}
										?>
										</select>
										</td>
										<td class="alinearizq" style="width: 81px">Estado:
										</td>
										<td class="alinearizq" style="width: 5px">
										<input name="verestadodis" id="verestadodis" type="checkbox" onclick="BUSCAR('CAJERO','')" />
										</td>
										<td class="alinearizq" style="width: 100px">
										<select name="busestadodiseno" id="busestadodiseno" onchange="BUSCAR('CAJERO','')" tabindex="17" class="inputs" style="width:148px">
										<option value="">-Seleccione-</option>
										<?php
											$con = mysqli_query($conectar,"select * from estado_diseno where esd_sw_activo = 1 order by esd_nombre");
											$num = mysqli_num_rows($con);
											for($i = 0; $i < $num; $i++)
											{
												$dato = mysqli_fetch_array($con);
												$clave = $dato['esd_clave_int'];
												$nombre = $dato['esd_nombre'];
										?>
											<option value="<?php echo $clave; ?>" <?php if($clave == $estdis){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
										<?php
											}
										?>
										</select>
										</td>
										<td class="alinearizq" style="width: 88px">
										Fecha Inicio:</td>
										<td class="alinearizq" style="width: 5px">
										<input name="verfechainiciodis" id="verfechainiciodis" type="checkbox" onclick="BUSCAR('CAJERO','')" />
										</td>
										<td class="alinearizq" style="width: 155px">
										<input name="busfechainiciodiseno" id="busfechainiciodiseno" onkeyup="BUSCAR('CAJERO','')" onchange="BUSCAR('CAJERO','')" placeholder="Desde" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px">
										<input name="busfechafindiseno" id="busfechafindiseno" onkeyup="BUSCAR('CAJERO','')" onchange="BUSCAR('CAJERO','')" placeholder="Hasta" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px">
										</td>
									</tr>
									<tr>
										<td class="alinearizq" style="width: 88px">
										Fecha Entrega:</td>
										<td class="alinearizq" style="width: 5px">
										<input name="verfechaentregadis" id="verfechaentregadis" type="checkbox" onclick="BUSCAR('CAJERO','')" />
										</td>
										<td class="alinearizq" style="width: 155px">
										<input name="busfechaentregainiinfodiseno" id="busfechaentregainiinfodiseno" onkeyup="BUSCAR('CAJERO','')" onchange="BUSCAR('CAJERO','')" placeholder="Desde" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px">
										<input name="busfechaentregafininfodiseno" id="busfechaentregafininfodiseno" onkeyup="BUSCAR('CAJERO','')" onchange="BUSCAR('CAJERO','')" placeholder="Hasta" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px">
										</td>
										<td class="alinearizq" style="width: 90px">Total Días:</td>
										<td class="alinearizq" style="width: 5px">
										<input name="vertotaldiasdis" id="vertotaldiasdis" type="checkbox" onclick="BUSCAR('CAJERO','')" />
										</td>
										<td class="alinearizq" style="width: 6px">
										<input name="bustotaldiasdiseno" id="bustotaldiasdiseno" onkeyup="BUSCAR('CAJERO','')" tabindex="31" class="inputs" type="text" style="width: 140px">
										</td>
										<td class="alinearizq" style="width: 88px">
										Notas:</td>
										<td class="alinearizq" style="width: 5px">
										<input name="vernotasdis" id="vernotasdis" type="checkbox" onclick="BUSCAR('CAJERO','')" />
										</td>
										<td class="alinearizq" style="width: 155px">
										<textarea cols="20" class="inputs" name="busnotasdiseno" id="busnotasdiseno" onkeyup="BUSCAR('CAJERO','')" rows="2"></textarea></td>
									</tr>
									</table>
							</fieldset>
							</td>
						</tr>
						<tr>
							<td colspan="6">
							<hr>
							</td>
						</tr>
						<tr>
							<td style="height: 31px;" class="alinearizq" colspan="6">
							<fieldset name="Group1">
							<legend><strong>LICENCIA<input name="verlicencia" id="verlicencia" onclick="verfiltro('LICENCIA');BUSCAR('CAJERO','')" type="checkbox"><input name="ocultarlicencia" id="ocultarlicencia" style="display:none" onclick="ocultarfiltro('LICENCIA');BUSCAR('CAJERO','')" type="checkbox"><input name="oculicencia" id="oculicencia" value="0" type="hidden" /></strong></legend>
							<table style="width: 100%;display:none" id="tablicencia">
											<tr>
										<td class="alinearizq" style="width: 90px">
										Nombre 
								Gestionador:</td>
										<td class="alinearizq" style="width: 5px">
										<input name="vergestionador" id="vergestionador" type="checkbox" onclick="BUSCAR('CAJERO','')" />
										</td>
										<td class="alinearizq" style="width: 6px">
										<select name="busnombregestionador" id="busnombregestionador" onchange="BUSCAR('CAJERO','')" tabindex="20" class="inputs" data-placeholder="-Seleccione-" style="width: 140px">
										<option value="">-Seleccione-</option>
										<?php
											$con = mysqli_query($conectar,"select * from usuario where (usu_categoria = 2 and usu_sw_licencia = 1 and usu_sw_activo = 1) OR (usu_clave_int NOT IN (select usu_clave_int from usuario_actor) and usu_sw_licencia = 1 and usu_sw_activo = 1) order by usu_nombre");
											$num = mysqli_num_rows($con);
											for($i = 0; $i < $num; $i++)
											{
												$dato = mysqli_fetch_array($con);
												$clave = $dato['usu_clave_int'];
												$nombre = $dato['usu_nombre'];
										?>
											<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
										<?php
											}
										?>
										</select>
										</td>
										<td class="alinearizq" style="width: 81px">Estado:
										</td>
										<td class="alinearizq" style="width: 5px">
										<input name="verestadolic" id="verestadolic" type="checkbox" onclick="BUSCAR('CAJERO','')" />
										</td>
										<td class="alinearizq" style="width: 100px">
										<select name="busestadogestionador" id="busestadogestionador" onchange="BUSCAR('CAJERO','')" tabindex="17" class="inputs" style="width:148px">
										<option value="">-Seleccione-</option>
										<?php
											$con = mysqli_query($conectar,"select * from estado_licencia where esl_sw_activo = 1 order by esl_nombre");
											$num = mysqli_num_rows($con);
											for($i = 0; $i < $num; $i++)
											{
												$dato = mysqli_fetch_array($con);
												$clave = $dato['esl_clave_int'];
												$nombre = $dato['esl_nombre'];
										?>
											<option value="<?php echo $clave; ?>" <?php if($clave == $estlic){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
										<?php
											}
										?>
										</select>
										</td>
										<td class="alinearizq" style="width: 88px">
										Fecha Inicio:</td>
										<td class="alinearizq" style="width: 5px">
										<input name="verfechainiciolic" id="verfechainiciolic" type="checkbox" onclick="BUSCAR('CAJERO','')" />
										</td>
										<td class="alinearizq" style="width: 155px">
										<input name="busfechainiciogestionador" id="busfechainiciogestionador" onkeyup="BUSCAR('CAJERO','')" onchange="BUSCAR('CAJERO','')" placeholder="Desde" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px">
										<input name="busfechafingestionador" id="busfechafingestionador" onkeyup="BUSCAR('CAJERO','')" onchange="BUSCAR('CAJERO','')" placeholder="Hasta" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px">
										</td>
									</tr>
									<tr>
										<td class="alinearizq" style="width: 88px">
										Fecha Entrega:</td>
										<td class="alinearizq" style="width: 5px">
										<input name="verfechaentregalic" id="verfechaentregalic" type="checkbox" onclick="BUSCAR('CAJERO','')" />
										</td>
										<td class="alinearizq" style="width: 155px">
										<input name="busfechaentregainiinfogestionador" id="busfechaentregainiinfogestionador" onkeyup="BUSCAR('CAJERO','')" onchange="BUSCAR('CAJERO','')" placeholder="Desde" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px">
										<input name="busfechaentregafininfogestionador" id="busfechaentregafininfogestionador" onkeyup="BUSCAR('CAJERO','')" onchange="BUSCAR('CAJERO','')" placeholder="Hasta" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px">
										</td>
										<td class="alinearizq" style="width: 90px">Total Días:</td>
										<td class="alinearizq" style="width: 5px">
										<input name="vertotaldiaslic" id="vertotaldiaslic" type="checkbox" onclick="BUSCAR('CAJERO','')" />
										</td>
										<td class="alinearizq" style="width: 6px">
										<input name="bustotaldiasgestionador" id="bustotaldiasgestionador" onkeyup="BUSCAR('CAJERO','')" tabindex="31" class="inputs" type="text" style="width: 140px">
										</td>
										<td class="alinearizq" style="width: 88px">
										Notas:</td>
										<td class="alinearizq" style="width: 5px">
										<input name="vernotaslic" id="vernotaslic" type="checkbox" onclick="BUSCAR('CAJERO','')" />
										</td>
										<td class="alinearizq" style="width: 155px">
										<textarea cols="20" class="inputs" name="busnotasgestionador" id="busnotasgestionador" onkeyup="BUSCAR('CAJERO','')" rows="2"></textarea></td>
									</tr>
									</table>
							</fieldset>
							</td>
						</tr>
						<tr>
							<td colspan="6">
							<hr>
							</td>
						</tr>
						<tr>
							<td style="height: 31px;" class="alinearizq" colspan="6">
							<fieldset name="Group1">
							<legend><strong>INTERVENTORIA<input name="verinterventoria" id="verinterventoria" onclick="verfiltro('INTERVENTORIA');BUSCAR('CAJERO','')" type="checkbox"><input name="ocultarinterventoria" id="ocultarinterventoria" style="display:none" onclick="ocultarfiltro('INTERVENTORIA');BUSCAR('CAJERO','')" type="checkbox"><input name="ocuinterventoria" id="ocuinterventoria" value="0" type="hidden" /></strong></legend>
							<table style="width: 100%;display:none" id="tabinterventoria">
									<tr>
								<td class="alinearizq" style="width: 90px">
								Nombre 
						Interventor:</td>
								<td class="alinearizq" style="width: 5px">
								<input name="verinterventor" id="verinterventor" type="checkbox" onclick="BUSCAR('CAJERO','')" />
								</td>
								<td class="alinearizq" style="width: 6px">
								<select name="busnombreinterventor" id="busnombreinterventor" onchange="BUSCAR('CAJERO','')" tabindex="20" class="inputs" data-placeholder="-Seleccione-" style="width: 140px">
								<option value="">-Seleccione-</option>
								<?php
									$con = mysqli_query($conectar,"select * from usuario where (usu_categoria = 2 and usu_sw_interventoria = 1 and usu_sw_activo = 1) OR (usu_clave_int NOT IN (select usu_clave_int from usuario_actor) and usu_sw_interventoria = 1 and usu_sw_activo = 1) order by usu_nombre");
									$num = mysqli_num_rows($con);
									for($i = 0; $i < $num; $i++)
									{
										$dato = mysqli_fetch_array($con);
										$clave = $dato['usu_clave_int'];
										$nombre = $dato['usu_nombre'];
								?>
									<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
								<?php
									}
								?>
								</select>
								</td>
								<td class="alinearizq" style="width: 81px">Operador	Canal:
								</td>
								<td class="alinearizq" style="width: 5px">
								<input name="veroperadorcanal" id="veroperadorcanal" type="checkbox" onclick="BUSCAR('CAJERO','')" />
								</td>
								<td class="alinearizq" style="width: 100px">
								<select name="buscanalcomunicacion" id="buscanalcomunicacion" onchange="BUSCAR('CAJERO','')" tabindex="20" class="inputs" data-placeholder="-Seleccione-" style="width: 140px">
								<option value="">-Seleccione-</option>
								<?php
									$con = mysqli_query($conectar,"select * from canal_comunicacion where can_sw_activo = 1 order by can_nombre");
									$num = mysqli_num_rows($con);
									for($i = 0; $i < $num; $i++)
									{
										$dato = mysqli_fetch_array($con);
										$clave = $dato['can_clave_int'];
										$nombre = $dato['can_nombre'];
								?>
									<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
								<?php
									}
								?>
								</select>
								</td>
								<td class="alinearizq" style="width: 88px">
								Fecha Inicio Obra:</td>
								<td class="alinearizq" style="width: 4px">
								<input name="verfechainicioint" id="verfechainicioint" type="checkbox" onclick="BUSCAR('CAJERO','')" />
								</td>
								<td class="alinearizq" style="width: 155px">
								<input name="busfechainiciointerventor" id="busfechainiciointerventor" onkeyup="BUSCAR('CAJERO','')" onchange="BUSCAR('CAJERO','')" placeholder="Desde" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px">
								<input name="busfechafininterventor" id="busfechafininterventor" onkeyup="BUSCAR('CAJERO','')" onchange="BUSCAR('CAJERO','')" placeholder="Hasta" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px">
								</td>
							</tr>
							<tr>
								<td class="alinearizq" style="width: 88px">
								Fecha Entrega:</td>
								<td class="alinearizq" style="width: 5px">
								<input name="verfechaentregaint" id="verfechaentregaint" type="checkbox" onclick="BUSCAR('CAJERO','')" />
								</td>
								<td class="alinearizq" style="width: 155px">
								<input name="busfechaentregainiinfointerventor" id="busfechaentregainiinfointerventor" onkeyup="BUSCAR('CAJERO','')" onchange="BUSCAR('CAJERO','')" placeholder="Desde" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px">
								<input name="busfechaentregafininfointerventor" id="busfechaentregafininfointerventor" onkeyup="BUSCAR('CAJERO','')" onchange="BUSCAR('CAJERO','')" placeholder="Hasta" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px">
								</td>
								<td class="alinearizq" style="width: 90px">Fecha Pedido Suministro:</td>
								<td class="alinearizq" style="width: 5px">
								<input name="verfechapedido" id="verfechapedido" type="checkbox" onclick="BUSCAR('CAJERO','')" />
								</td>
								<td class="alinearizq" style="width: 6px">
								<input name="busfechainipedidointerventor" id="busfechainipedidointerventor" onkeyup="BUSCAR('CAJERO','')" onchange="BUSCAR('CAJERO','')" placeholder="Desde" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px">
								<input name="busfechafinpedidointerventor" id="busfechafinpedidointerventor" onkeyup="BUSCAR('CAJERO','')" onchange="BUSCAR('CAJERO','')" placeholder="Hasta" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px">
								</td>
								<td class="alinearizq" style="width: 88px">
								Notas:</td>
								<td class="alinearizq" style="width: 4px">
								<input name="vernotasint" id="vernotasint" type="checkbox" onclick="BUSCAR('CAJERO','')" />
								</td>
								<td class="alinearizq" style="width: 155px">
								<textarea cols="20" class="inputs" name="busnotasinterventor" id="busnotasinterventor" onkeyup="BUSCAR('CAJERO','')" rows="2"></textarea></td>
							</tr>
							</table>
							</fieldset>
							</td>
						</tr>
						<tr>
							<td colspan="6">
							<hr>
							</td>
						</tr>
						<tr>
							<td style="height: 31px;" class="alinearizq" colspan="6">
							<fieldset name="Group1">
							<legend><strong>CONSTRUCTOR<input name="verconstructor" id="verconstructor" onclick="verfiltro('CONSTRUCTOR');BUSCAR('CAJERO','')" type="checkbox"><input name="ocultarconstructor" id="ocultarconstructor" style="display:none" onclick="ocultarfiltro('CONSTRUCTOR');BUSCAR('CAJERO','')" type="checkbox"><input name="ocuconstructor" id="ocuconstructor" value="0" type="hidden" /></strong></legend>
							<table style="width: 100%;display:none" id="tabconstructor">
										<tr>
									<td class="alinearizq" style="width: 90px">
									Nombre 
							Constructor:</td>
									<td class="alinearizq" style="width: 5px">
									<input name="verconstructor" id="verconstructor" type="checkbox" onclick="BUSCAR('CAJERO','')" />
									</td>
									<td class="alinearizq" style="width: 6px">
									<select name="busnombreconstructor" id="busnombreconstructor" onchange="BUSCAR('CAJERO','')" tabindex="20" class="inputs" data-placeholder="-Seleccione-" style="width: 140px">
									<option value="">-Seleccione-</option>
									<?php
										$con = mysqli_query($conectar,"select * from usuario where (usu_categoria = 2 and usu_sw_licencia = 1 and usu_sw_activo = 1) OR (usu_clave_int NOT IN (select usu_clave_int from usuario_actor) and usu_sw_licencia = 1 and usu_sw_activo = 1) order by usu_nombre");
										$num = mysqli_num_rows($con);
										for($i = 0; $i < $num; $i++)
										{
											$dato = mysqli_fetch_array($con);
											$clave = $dato['usu_clave_int'];
											$nombre = $dato['usu_nombre'];
									?>
										<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
									<?php
										}
									?>
									</select>
									</td>
									<td class="alinearizq" style="width: 81px">%Avance:
									</td>
									<td class="alinearizq" style="width: 5px"><input name="veravance" id="veravance" type="checkbox" onclick="BUSCAR('CAJERO','')" /></td>
									<td class="alinearizq" style="width: 100px">
									<select name="busporcentajeavance" id="busporcentajeavance" onchange="BUSCAR('CAJERO','')" tabindex="33" class="inputs" data-placeholder="-Seleccione-" style="width: 140px">
									<option value="">-Seleccione-</option>
									<?php
										for($i = 10; $i <= 100; $i = $i+10)
										{
									?>	
											<option value="<?php echo $i; ?>"><?php echo $i; ?>%</option>
									<?php
										}
									?>
									</select>
									</td>
									<td class="alinearizq" style="width: 88px">
									Fecha Inicio:</td>
									<td class="alinearizq" style="width: 5px">
									<input name="verfechainiciocons" id="verfechainiciocons" type="checkbox" onclick="BUSCAR('CAJERO','')" />
									</td>
									<td class="alinearizq" style="width: 155px">
									<input name="busfechainicioconstructor" id="busfechainicioconstructor" onkeyup="BUSCAR('CAJERO','')" onchange="BUSCAR('CAJERO','')" placeholder="Desde" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px">
									<input name="busfechafinconstructor" id="busfechafinconstructor" onkeyup="BUSCAR('CAJERO','')" onchange="BUSCAR('CAJERO','')" placeholder="Hasta" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px">
									</td>
								</tr>
								<tr>
									<td class="alinearizq" style="width: 88px">
									Fecha Entrega:</td>
									<td class="alinearizq" style="width: 5px">
									<input name="verfechaentregacons" id="verfechaentregacons" type="checkbox" onclick="BUSCAR('CAJERO','')" />
									</td>
									<td class="alinearizq" style="width: 155px">
									<input name="busfechaentregainiinfoconstructor" id="busfechaentregainiinfoconstructor" onkeyup="BUSCAR('CAJERO','')" onchange="BUSCAR('CAJERO','')" placeholder="Desde" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px">
									<input name="busfechaentregafininfoconstructor" id="busfechaentregafininfoconstructor" onkeyup="BUSCAR('CAJERO','')" onchange="BUSCAR('CAJERO','')" placeholder="Hasta" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px">
									</td>
									<td class="alinearizq" style="width: 90px">Total Días:</td>
									<td class="alinearizq" style="width: 5px"><input name="vertotaldiascons" id="vertotaldiascons" type="checkbox" onclick="BUSCAR('CAJERO','')" /></td>
									<td class="alinearizq" style="width: 6px">
									<input name="bustotaldiasconstructor" id="bustotaldiasconstructor" onkeyup="BUSCAR('CAJERO','')" tabindex="31" class="inputs" type="text" style="width: 140px">
									</td>
									<td class="alinearizq" style="width: 88px">
									Notas:</td>
									<td class="alinearizq" style="width: 5px">
									<input name="vernotascons" id="vernotascons" type="checkbox" onclick="BUSCAR('CAJERO','')" />
									</td>
									<td class="alinearizq" style="width: 155px">
									<textarea cols="20" class="inputs" name="busnotasconstructor" id="busnotasconstructor" onkeyup="BUSCAR('CAJERO','')" rows="2"></textarea></td>
								</tr>
							</table>
							</fieldset>
							</td>
						</tr>
						<tr>
							<td style="width: 22px; height: 31px;" class="alinearizq">
							
							</td>
							<td style="height: 31px;">
							
							&nbsp;
							
							</td>
							<td style="width: 150px; height: 31px;" class="alinearizq">
							&nbsp;</td>
							<td align="left" style="height: 31px">
							&nbsp;</td>
							<td align="left" style="height: 31px">
							&nbsp;</td>
							<td align="left" style="height: 31px">
							&nbsp;</td>
						</tr>
						</table>
					</fieldset>
				</td>
			</tr>
			<tr>
				<td class="auto-style2">
				<div id="busqueda">
				<fieldset name="Group1">
				<legend align="center">LISTADO DE CAJEROS<br>
				<button name="Accion" value="excel" title="Exportar a Excel" onclick="EXPORTAR()" type="button" style="cursor:pointer;">
				<img src="../../images/excel.png" height="25" width="25"></button>
				</legend>
				<table style="width: 100%">
				<tr>
					<td class="alinearizq"><strong>Cajero</strong></td>
					<td class="alinearizq"><strong>Nombre</strong></td>
					<td class="alinearizq"><strong>Dirección</strong></td>
					<td class="alinearizq"><strong>Año Cont.</strong></td>
					<td class="alinearizq"><strong>Región</strong></td>
					<td class="alinearizq"><strong>Departamento</strong></td>
					<td class="alinearizq"><strong>Municipio</strong></td>
					<td class="alinearizq"><strong>Tipología</strong></td>
					<td class="alinearizq"><strong>Tip. Intervención</strong></td>
					<td class="alinearizq"><strong>Modalidad</strong></td>
					<td class="alinearizq"><strong>Estado</strong></td>
				</tr>
				<?php
				$query = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int)  inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where c.caj_sw_eliminado = 0");
				//$res = $con->query($query);
				$num_registros = mysqli_num_rows($query);
		
				$resul_x_pagina = 100;
				//Paginar:
				$paginacion = new Zebra_Pagination();
				$paginacion->records($num_registros);
				$paginacion->records_per_page($resul_x_pagina);
						
				$con = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int)  inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where c.caj_sw_eliminado = 0 LIMIT ".(($paginacion->get_page() - 1) * $resul_x_pagina). ',' .$resul_x_pagina);
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					//Info Base
					$clacaj = $dato['clacaj'];
					$nomcaj = $dato['caj_nombre'];
					$dir = $dato['caj_direccion'];
					$anocon = $dato['caj_ano_contable'];
					$reg = $dato['caj_region'];
					$dep = $dato['caj_departamento'];
					$mun = $dato['caj_municipio'];
					$tip = $dato['tip_clave_int'];
					$tipint = $dato['tii_clave_int'];
					$mod = $dato['mod_clave_int'];
					$conmod = mysqli_query($conectar,"select mod_nombre from modalidad where mod_clave_int = '".$mod."'");
					$datomod = mysqli_fetch_array($conmod);
					$moda = $datomod['mod_nombre'];
					//Info Secun
					$codcaj = $dato['caj_codigo_cajero'];
					$cencos = $dato['cco_clave_int'];
					$refmaq = $dato['rem_clave_int'];
					$ubi = $dato['ubi_clave_int'];
					$codsuc = $dato['caj_codigo_suc'];
					$ubiamt = $dato['caj_ubicacion_atm'];
					$ati = $dato['ati_clave_int'];
					$rie = $dato['caj_riesgo'];
					$are = $dato['are_clave_int'];
					$codrec = $dato['caj_cod_recibido_monto'];
					$codrec = $dato['caj_fecha_creacion'];
					//Info Inmobiliaria
					$reqinm = $dato['cai_req_inmobiliaria'];
					$inmob = $dato['cai_inmobiliaria'];
					$feciniinmob = $dato['cai_fecha_ini_inmobiliaria'];
					$estinmob = $dato['esi_clave_int'];
					$fecentinmob = $dato['cai_fecha_entrega_info_inmobiliaria'];
					//Info Visita Local
					$reqvis = $dato['cav_req_visita_local'];
					$vis = $dato['cav_visitante'];
					$fecvis = $dato['cav_fecha_visita'];
					//Info Diseño
					$reqdis = $dato['cad_req_diseno'];
					$dis = $dato['cad_disenador'];
					$fecinidis = $dato['cad_fecha_inicio_diseno'];
					$estdis = $dato['esd_clave_int'];
					$fecentdis = $dato['cad_fecha_entrega_info_diseno'];
					//Info Licencia
					$reqlic = $dato['cal_req_licencia'];
					$lic = $dato['cal_gestionador'];
					$fecinilic = $dato['cal_fecha_inicio_licencia'];
					$estlic = $dato['esl_clave_int'];
					$fecentlic = $dato['cal_fecha_entrega_info_licencia'];
					//Info Interventoria
					$int = $dato['cin_interventor'];
					$fecteoent = $dato['cin_fecha_teorica_entrega'];
					$feciniobra = $dato['cin_fecha_inicio_obra'];
					$fecpedsum = $dato['cin_fecha_pedido_suministro'];
					$aprovcotiz = $dato['cin_sw_aprov_cotizacion'];
					$cancom = $dato['can_clave_int'];
					$aprovliqui = $dato['cin_sw_aprov_liquidacion'];
					$swimgnex = $dato['cin_sw_img_nexos'];
					$swfot = $dato['cin_sw_fotos'];
					$swact = $dato['cin_sw_actas'];
					$estpro = $dato['caj_estado_proyecto'];
					//Info Constructor
					$cons = $dato['cac_constructor'];
					$cons = $dato['cac_fecha_entrega_atm'];
					$cons = $dato['cac_porcentaje_avance'];
					$cons = $dato['cac_fecha_entrega_cotizacion'];
					$cons = $dato['cac_fecha_entrega_liquidacion'];
					//Info Instalador Seguridad
					$seg = $dato['cas_instalador'];
					$fecingseg = $dato['cas_fecha_ingreso_obra_inst'];
					$codmon = $dato['cas_codigo_monitoreo'];
				?>
				<tr>
					<td class="alinearizq"><?php echo $clacaj; ?></td>
					<td class="alinearizq"><?php echo $nomcaj; ?></td>
					<td class="alinearizq"><?php echo $dir; ?></td>
					<td class="alinearizq"><?php echo $anocon; ?></td>
					<td class="alinearizq">
					<?php 
						$sql = mysqli_query($conectar,"select pog_nombre from posicion_geografica where pog_clave_int = '".$reg."'");
						$datosql = mysqli_fetch_array($sql);
						$nomreg = $datosql['pog_nombre'];
						echo $nomreg; 
					?>
					</td>
					<td class="alinearizq">
					<?php 
						$sql = mysqli_query($conectar,"select pog_nombre from posicion_geografica where pog_clave_int = '".$dep."'");
						$datosql = mysqli_fetch_array($sql);
						$nomdep = $datosql['pog_nombre'];
						echo $nomdep;
					?>
					</td>
					<td class="alinearizq">
					<?php 
						$sql = mysqli_query($conectar,"select pog_nombre from posicion_geografica where pog_clave_int = '".$mun."'");
						$datosql = mysqli_fetch_array($sql);
						$nommun = $datosql['pog_nombre'];
						echo $nommun;
					?>
					</td>
					<td class="alinearizq">
					<?php 
						$sql = mysqli_query($conectar,"select tip_nombre from tipologia where tip_clave_int = '".$tip."'");
						$datosql = mysqli_fetch_array($sql);
						$nomtip = $datosql['tip_nombre'];
						echo $nomtip;
					?>
					</td>
					<td class="alinearizq">
					<?php 
						$sql = mysqli_query($conectar,"select tii_nombre from tipointervencion where tii_clave_int = '".$tipint."'");
						$datosql = mysqli_fetch_array($sql);
						$nomtii = $datosql['tii_nombre'];
						echo $nomtii;
					?>
					</td>
					<td class="alinearizq">
					<?php if($mod <= 0){ echo "Sin Definir "; }else{ echo $moda; } ?></td>
					<td class="alinearizq"><?php if($estpro == 1){ echo "ACTIVO"; }elseif($estpro == 2){ echo "SUSPENDIDO"; }elseif($estpro == 3){ echo "ENTREGADO"; }elseif($estpro == 4){ echo "CANCELADO"; }elseif($estpro == 5){ echo "PROGRAMADO"; } ?></td>
				</tr>
				<tr>
					<td class="alinearizq" colspan="11"><hr></td>
				</tr>
				<?php
				}
				?>
				</table>
				</fieldset>
				<?php $paginacion->render(); ?>
				</div>
			</td>
			</tr>
			</table>		
			</div>
			</td>
		</tr>
		<tr>
			<td style="width: 100px">&nbsp;</td>
			<td style="width: 100px">&nbsp;</td>
			<td style="width: 100px">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
<script type="text/javascript" language="javascript">
    $("#buscajero").multiselect().multiselectfilter();
</script>
<input name="infobasica" id="infobasica" value="0" type="hidden" />
<input name="infosecundaria" id="infosecundaria" value="0" type="hidden" />
<input name="diseno" id="diseno" value="0" type="hidden" />
<input name="licencia" id="licencia" value="0" type="hidden" />
<input name="interventoria" id="interventoria" value="0" type="hidden" />
<input name="constructor" id="constructor" value="0" type="hidden" />
</form>
</body>
</html>