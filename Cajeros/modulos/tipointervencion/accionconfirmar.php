<?php include('../../data/Conexion.php');
session_start();
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];
	
// verifica si no se ha loggeado
if(!isset($_SESSION["persona"]))
{
  session_destroy();
  header("LOCATION:index.php");
}else{
}
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");
$idcats=$_POST['idcat'];
$contador=0;
if(is_array($idcats)){
        for($i=0;$i<count($idcats);$i++){
        	if($_GET['accion'] == 'Activar')
        	{
				$rows=mysqli_query($conectar,"update tipointervencion set tii_sw_activo = 1, tii_usu_actualiz = '".$usuario."', tii_fec_actualiz = '".$fecha."' where tii_clave_int=".$idcats[$i]);
				if($idcats[$i] <> '' and $idcats[$i] <> 0)
				{
					mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'5',19,'".$idcats[$i]."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 19=Activar proceso
				}
        	}
        	elseif($_GET['accion'] == 'Inactivar')
        	{
        		$rows=mysqli_query($conectar,"update tipointervencion set tii_sw_activo = 0, tii_usu_actualiz = '".$usuario."', tii_fec_actualiz = '".$fecha."' where tii_clave_int=".$idcats[$i]);
				if($idcats[$i] <> '' and $idcats[$i] <> 0)
				{
					mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'5',20,'".$idcats[$i]."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 20=Inactivar proceso
				}
        	}
        	elseif($_GET['accion'] == 'Eliminar')
        	{
        		if($idcats[$i] != '')
        		{
	        		$con = mysqli_query($conectar,"select * from cajero where tii_clave_int = '".$idcats[$i]."'");
	        		$nume = mysqli_num_rows($con);
			
	        		if($nume > 0)
	        		{
	        			for($k = 0; $k < $nume; $k++)
						{
							$dato = mysqli_fetch_array($con); 
							$men = $men."".$dato['caj_clave_int'].",";
						}
	        		}
	        		else
	        		{
	        			$rows1=mysqli_query($conectar,"delete from tipointervencion where tii_clave_int=".$idcats[$i]);
						mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'16',41,'".$t."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 41=Actualización tipo intervención
	        		}
	        	}
        	}
        	$contador++;
		}
		//echo "Se han eliminado $contador Registros de la base de datos";
		if($_GET['accion'] == 'Activar')
        {
        	echo '<script>alert("Los registros seleccionadas han sido Activados correctamente"); window.location.href="tipointervencion.php";</script>';
        }
    	elseif($_GET['accion'] == 'Inactivar')
    	{
    		echo '<script>alert("Los registros seleccionadas han sido Inactivados correctamente"); window.location.href="tipointervencion.php";</script>';
    	}
    	elseif($_GET['accion'] == 'Eliminar')
    	{
    		if($men != '')
    		{
    			echo '<script>alert("Los Tipos de intervencion seleccionadas han sido Eliminados correctamente, Excepto los Tipos de intervencion que estan asignados a los siguientes cajeros: '.$men.'"); window.location.href="tipointervencion.php";</script>';
    		}
    		else
    		{
    			echo '<script>alert("Los registros seleccionadas han sido Eliminados correctamente"); window.location.href="tipointervencion.php";</script>';
    		}
    	}
}else{
	echo '<script>alert("No se enviaron registros para eliminar"); window.location.href="tipointervencion.php";</script>';
}

?>