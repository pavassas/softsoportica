<?php
include("../../data/Conexion.php");
date_default_timezone_set('America/Bogota');

$output = '';
$month = $_GET['month'];
$year = $_GET['year'];
	
if($month == '' && $year == '') { 
	$time = time();
	$month = date('n',$time);
    $year = date('Y',$time);
}

$date = getdate(mktime(0,0,0,$month,1,$year));
$today = getdate();
$hours = $today['hours'];
$mins = $today['minutes'];
$secs = $today['seconds'];

if(strlen($hours)<2) $hours="0".$hours;
if(strlen($mins)<2) $mins="0".$mins;
if(strlen($secs)<2) $secs="0".$secs;

$days=date("t",mktime(0,0,0,$month,1,$year));
$start = $date['wday']+1;
$name = $date['month'];
$year2 = $date['year'];
$offset = $days + $start - 1;
 
if($month==12) { 
	$next=1; 
	$nexty=$year + 1; 
} else { 
	$next=$month + 1; 
	$nexty=$year; 
}

if($month==1) { 
	$prev=12; 
	$prevy=$year - 1; 
} else { 
	$prev=$month - 1; 
	$prevy=$year; 
}

if($offset <= 28) $weeks=28; 
elseif($offset > 35) $weeks = 42; 
else $weeks = 35; 

$output .= "
<table cellspacing='1'>
<tr class='cal'>
	<td colspan='7'>
		<table align='center' class='calhead' style='width: 90%'>
		<tr>
			<td align='center'>
				<a href='javascript:navigate($prev,$prevy)'><img align='left' src='images/10.png'></a>
			</td>
			<td align='center'>
			<div class='mes' onclick='javascript:navigate(\"\",\"\")'>$name $year2</div>
			</td>
			<td align='center'>
			<a href='javascript:navigate($next,$nexty)'><img align='right' src='images/12.png'></a>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr class='dayhead'>
	<td>Sun</td>
	<td>Mon</td>
	<td>Tue</td>
	<td>Wed</td>
	<td>Thu</td>
	<td>Fri</td>
	<td>Sat</td>
</tr>";

$col=1;
$cur=1;
$next=0;
for($i=1;$i<=$weeks;$i++) { 
	if($next==3) $next=0;
	if($col==1) $output.="<tr class='dayrow'>"; 
	
	if($cur == 1){ $cur = '01'; }elseif($cur == 2){ $cur = '02'; }elseif($cur == 3){ $cur = '03'; }elseif($cur == 4){ $cur = '04'; }elseif($cur == 5){ $cur = '05'; }elseif($cur == 6){ $cur = '06'; }elseif($cur == 7){ $cur = '07'; }elseif($cur == 8){ $cur = '08'; }elseif($cur == 9){ $cur = '09'; }
  	$con = mysqli_query($conectar,"select * from festivo where fes_fecha = '$year2/$month/$cur'");
	$num = mysqli_num_rows($con);

	if($num > 0)
	{	
		$output.="<td align='center' onclick=FECHA('$cur','$month','$year2') style=background-image:url('images/14.png');background-repeat:no-repeat valign='top' onMouseOver=\"this.className='dayover'\" onMouseOut=\"this.className='dayout'\">";
	}
	else
	{
		$output.="<td align='center' onclick=FECHA('$cur','$month','$year2') style=background-image:url('images/13.png');background-repeat:no-repeat valign='top' onMouseOver=\"this.className='dayover'\" onMouseOut=\"this.className='dayout'\">";
	}

	if($i <= ($days+($start-1)) && $i >= $start) {
		$output.="<div class='day'><b";

		if(($cur==$today[mday]) && ($name==$today[month])) $output.=" style='color:#C00'";

		$output.=">$cur</b></div></td>";

		$cur++; 
		$col++; 
		
	} else { 
		$output.="&nbsp;</td>"; 
		$col++; 
	}  
	    
    if($col==8) { 
	    $output.="</tr>"; 
	    $col=1; 
    }
}

$output.="</table>";
  
echo $output;

?>