function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function FECHA(d,m,y)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
		{
			 document.getElementById('calendar').innerHTML=ajax.responseText;
		}
	}
	jQuery("#calendar").html("<img alt='cargando' src='images/cargando.gif' height='30' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?guardarfestivo=si&d="+d+"&m="+m+"&y="+y,true);
	ajax.send(null);
}
function EDITARFECHA(id)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
		{
			 document.getElementById('dialogo').innerHTML=ajax.responseText;
		}
	}
	jQuery("#dialogo").html("<img alt='cargando' src='images/cargando.gif' height='30' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?editarfecha=si&id="+id,true);
	ajax.send(null);
}
function GUARDAR(d,m,y)
{	
	var tit = $('#titulo').val();
	var fi = $('#fecini').val();
	var hi = $('#horaini').val();
	var ff = $('#fecfin').val();
	var hf = $('#horafin').val();
	var des = $('#descripcion').val();
	
	if(tit == '')
	{
		alert("Debe ingresar el titulo");
	}
	else
	if(fi == '')
	{
		alert("Debe ingresar la fecha inicial del evento");
	}
	else
	if(hi == '')
	{
		alert("Debe elegir la hora inicial del evento");
	}
	else
	if(ff == '')
	{
		alert("Debe ingresar la fecha final del evento");
	}
	else
	if(hf == '')
	{
		alert("Debe elegir la hora final del evento");
	}
	else
	if(des == '')
	{
		alert("Debe ingresar la descripcion del evento");
	}
	else
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
			{
				 document.getElementById('datos').innerHTML=ajax.responseText;
			}
		}
		jQuery("#datos").html("<img alt='cargando' src='images/cargando.gif' height='30' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?guardarevento=si&tit="+tit+"&fi="+fi+"&hi="+hi+"&ff="+ff+"&hf="+hf+"&des="+des+"&d="+d+"&m="+m+"&y="+y,true);
		ajax.send(null);
		
		setTimeout("FECHA("+d+","+m+","+y+")",1000);
		setTimeout("CALENDAR()",1000);
	}
}
function CALENDAR()
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
		{
			 document.getElementById('calendar').innerHTML=ajax.responseText;
		}
	}
	jQuery("#calendar").html("<img alt='cargando' src='images/cargando.gif' height='30' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarcalendario=si",true);
	ajax.send(null);
}
function CANCELAR(d,m,y)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
		{
			 document.getElementById('datosdia').innerHTML=ajax.responseText;
		}
	}
	jQuery("#datosdia").html("<img alt='cargando' src='images/cargando.gif' height='30' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?modulofecha=si&d="+d+"&m="+m+"&y="+y,true);
	ajax.send(null);
}

function EDITAR(id)
{	
	var tit = titulo1.value;
	var fi = fecini1.value;
	var ff = fecfin1.value;
	var act = fecact1.value;
	var com = comentarios1.value;
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
		{
			 document.getElementById('datos1').innerHTML=ajax.responseText;
		}
	}
	jQuery("#datos1").html("<img alt='cargando' src='images/cargando.gif' height='30' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?editarevento=si&tit="+tit+"&fi="+fi+"&ff="+ff+"&act="+act+"&id="+id+"&com="+com,true);
	ajax.send(null);
}
function NUEVO(d,m,y)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
		{
			 document.getElementById('datosdia').innerHTML=ajax.responseText;
		}
	}
	jQuery("#datosdia").html("<img alt='cargando' src='images/cargando.gif' height='30' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?nuevoevento=si&d="+d+"&m="+m+"&y="+y,true);
	ajax.send(null);
}
function eliminar(v)
{
	if(confirm('Esta seguro/a de Eliminar este registro?'))
	{
		var dataString = 'id='+v;
	
		$.ajax({
	        type: "POST",
	        url: "delete.php",
	        data: dataString,
	        success: function() {
				$('#delete-ok').empty();
				//$('#delete-ok').append('<div class="correcto">Se ha eliminado correctamente la entrada con id='+v+'.</div>').fadeIn("slow");
				$('#service'+v).fadeOut("slow");
				//$('#'+v).remove();
	        }
	    });
	    setTimeout("CALENDAR()",1000);
	}
}