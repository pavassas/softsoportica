<?php
	include('../../data/Conexion.php');
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_COOKIE['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];
		
	// verifica si no se ha loggeado
	if(!isset($_SESSION["persona"]))
	{
	  session_destroy();
	  header("LOCATION:index.php");
	}else{
	}
?>
<!DOCTYPE HTML>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

	
<title>PAVAS TECNOLOGÍA S.A.S.</title>
<meta name="description" content="Service Desk">
<meta name="author" content="InvGate S.R.L.">

<link rel="shortcut icon" href="img/favicon.ico">
<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">

<link rel="stylesheet" href="css/clean.7e4f11af0c688412dd8aa25b28b0db84.css" type="text/css">
<link rel="stylesheet" href="css/fonts.d87620aa29437ec1a4c2364858fa3607.css" type="text/css">
<link rel="stylesheet" href="css/toolbar.e73e3ba4378b1f2b1de350ef323d2c10.css" type="text/css">
<link rel="stylesheet" href="css/forms.7ba5dbaa170d790e06dc3e64d16df6ec.css" type="text/css">
<link rel="stylesheet" href="css/dropdowns.a3e95e26feafcd09b87732e381224c45.css" type="text/css">
<link rel="stylesheet" href="css/tables.d22651b7bc6ede923c3140e803446613.css" type="text/css">
<link rel="stylesheet" href="css/plugins/validationengine/validationengine.2c0953b5103027332146a5934721da3d.css" type="text/css">
<link rel="stylesheet" href="css/plugins/colorbox/colorbox.bf6245d8fbdbd182e63b3d30efb331bc.css" type="text/css">
<link rel="stylesheet" href="css/plugins/gritter/jquery.gritter.9561c37e5099df25b9dbd6b50002fae9.css" type="text/css">
<link rel="stylesheet" href="css/plugins/invgate/popup.bcac3d52c26fe8813f96890668274834.css" type="text/css">
<link rel="stylesheet" href="css/plugins/invgate/inputlist.a4daf29140fb7fd16c87914a472d76b4.css" type="text/css">
<link rel="stylesheet" href="css/plugins/invgate/multipleselector.537b0771c40de9f3a64e193ba36a57c7.css" type="text/css">
<link rel="stylesheet" href="css/plugins.26ab202f1d786ec744d7f587444f18f3.css" type="text/css">
<link rel="stylesheet" href="css/gamification/main.74d81eedeec7466de677ed5cbb818d69.css" type="text/css">
<link rel="stylesheet" href="css/scrollbar.da62bc7ef54aebbdb4e52272528a44d0.css" type="text/css">
<link rel="stylesheet" href="css/main.e41c890c8f72625912855cdc11dffd33.css" type="text/css">

<link href="css/requests/list.b2b381c1e243bbdeb809bdd354fa55a7.css" media="screen" rel="stylesheet" type="text/css" >
<!--[if lte IE 7]>
<link rel="stylesheet" href="css/ie.c32bc18afe0e2883ee4912a51f86c119.css" type="text/css" />
<![endif]-->

<style type="text/css">
.auto-style1 {
	text-align: center;
}
.auto-style2 {
	font-size: 12px;
	font-family: Arial, helvetica;
	outline: none;
/*transition: all 0.75s ease-in-out;*/ /*-webkit-transition: all 0.75s ease-in-out;*/ /*-moz-transition: all 0.75s ease-in-out;*/border-radius: 3px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 3px;
	border: 1px solid rgba(0,0,0, 0.2);
	
	background-color: #eee;
	padding: 3px;
	box-shadow: 0 0 10px #aaa;
	-webkit-box-shadow: 0 0 10px #aaa;
	-moz-box-shadow: 0 0 10px #aaa;
	border: 1px solid #999;
	background-color: white;
	text-align: center;
}
.auto-style3 {
	text-align: left;
}
.inputs{
	font-size:14px;
    font-family: Arial, helvetica;
    outline:none;
    border-radius:3px;
    -webkit-border-radius:6px;
    -moz-border-radius:3px;
    border:1px solid rgba(0,0,0, 0.5);
    padding: 3px;
}
.validaciones{
	font-size:14px;
    font-family: Arial, helvetica;
    outline:none;
    border-radius:3px;
    -webkit-border-radius:6px;
    -moz-border-radius:3px;
    border:1px solid rgba(0,0,0, 0.2);
    color:maroon;
    background-color:#eee;
    padding: 3px;
    text-align:center;
}
.ok{
	font-size:14px;
    font-family: Arial, helvetica;
    outline:none;
    border-radius:3px;
    -webkit-border-radius:6px;
    -moz-border-radius:3px;
    border:1px solid rgba(0,0,0, 0.2);
    color:green;
    background-color:#eee;
    padding: 3px;
    text-align:center;
}
</style>

<script type="text/javascript" language="javascript">
	selecteds=0;
	
	function CheckUncheck(total,check){
		checkbox=null;
		for(i=1;i<=total;i++){
			checkbox=document.getElementById("idcat"+i);
			//alert(checkbox.value);
			checkbox.checked=check.checked;
		}
		
		if(check.checked){
			selecteds=total;
		}else{
			selecteds=0;
		}
		
	}
	
	function contadorVals(check){
		if(check.checked){
			selecteds=selecteds+1;
		}else{
			selecteds=selecteds-1;
		}
	}
	
	function selectedVals(){
		if(selecteds==0){
			alert("Seleccione al menos un registro.");
			return false;
		}else{
			return true;
		}
	}
</script>

<?php //VALIDACIONES ?>
<script type="text/javascript" src="llamadas.js"></script>
<script type="text/javascript" src="js/jquery-1.6.min.js"></script>

<?php //VENTANA EMERGENTE ?>
<link rel="stylesheet" href="css/reveal.css" />
<script type="text/javascript" src="js/jquery-1.6.min.js"></script>
<script type="text/javascript" src="js/jquery.reveal.js"></script>
</head>


<body>
<?php
$rows=mysqli_query($conectar,"select * from canal_comunicacion");
$total=mysqli_num_rows($rows);
$idcats=$_POST['idcat'];
?>
<form action="accionconfirmar.php?accion=<?php echo $_POST['Accion']; ?>" method="post">
					<input  type="hidden" name="idcat[]" id="idcat" value="<?php echo $row['can_clave_int'];?>" />
	<input  type="hidden" name="idcat[]" id="idcat" value="<?php echo $row['can_clave_int'];?>" />
<!--[if lte IE 7]>
<div class="ieWarning">Este navegador no es compatible con el sistema. Por favor, use Chrome, Safari, Firefox o Internet Explorer 8 o superior.</div>
<![endif]-->
<div class="requestListFiltersButton requestListFiltersButtonClear">
	<table style="width: 105%">
		<tr>
			<td class="auto-style2" style="width: 60px; cursor:pointer">
			Todos
			<?php
				$con = mysqli_query($conectar,"select COUNT(*) cant from canal_comunicacion");
				$dato = mysqli_fetch_array($con);
				echo $dato['cant'];
			?>
			</td>
			<td class="auto-style1" style="width: 117px; cursor:pointer">&nbsp;
			</td>
			<td class="auto-style1" style="width: 65px; cursor:pointer">&nbsp;
			</td>
			<td class="auto-style1" style="width: 70px; cursor:pointer">&nbsp;
			</td>
			<td class="auto-style1">&nbsp;</td>
			<td class="auto-style2" style="width: 55px; cursor:pointer">
			<table style="width: 100%">
				<tr>
					<td><a data-reveal-id="nuevousuario" data-animation="fade" style="cursor:pointer"><img alt="" src="../../img/add2.png"></a></td>
					<td><a data-reveal-id="nuevousuario" data-animation="fade" style="cursor:pointer">Añadir</a></td>
				</tr>
			</table>
			</td>
			<td class="auto-style1" style="width: 55px"></td>
		</tr>
		<tr>
			<td colspan="6" class="auto-style2">
			<?php
			if(isset($_POST['Accion']))
			{
				$accion = $_POST['Accion'];
			}
			if($accion <> 'Agregar')
			{
			?>
			<table style="width: 102%">
				<tr>
					<td class="auto-style3" style="width: 221px">&nbsp;</td>
					<td class="auto-style3" style="width: 221px">&nbsp;</td>
					<td class="auto-style3" style="width: 221px">&nbsp;</td>
					<td class="auto-style3" style="width: 3px">&nbsp;</td>
					<td class="auto-style3">&nbsp;</td>
				</tr>
				<tr>
					<td class="auto-style3" style="width: 221px"><strong>Canal de Comunicación</strong></td>
					<td class="auto-style3" style="width: 221px"><strong>
					Usuario</strong></td>
					<td class="auto-style3" style="width: 221px"><strong>
					Actualización</strong></td>
					<td class="auto-style3" style="width: 3px"><strong>Activo</strong></td>
					<td class="auto-style3">&nbsp;</td>
				</tr>
				<?php
					if(isset($_POST['Accion']))
					{
						$accion = $_POST['Accion'];
					}
					if(is_array($idcats)){
					if($accion == 'Activar')
					{
					?>
				    <p class="auto-style3">
				    	
				    	<strong>Esta seguro de Activar los siguientes Canales de Comunicación? 
				    	<input type="submit" value="SI" name="Accion" onmouseover="this.style.backgroundColor='#445B74';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="userNameIcon" /></strong>
				    	<a href="canal.php"><strong>
						<input type="button" value="NO" name="Accion" onmouseover="this.style.backgroundColor='#445B74';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="userNameIcon" /></strong></a>
				    </p>
				    <?php
				    }
				    else
				    if($accion == 'Inactivar')
					{
					
					?>
				    <p class="auto-style3">
				    	
				    	<strong>Esta seguro de Inactivar los siguientes Canales de Comunicación?
				    	<input type="submit" value="SI" name="Accion" onmouseover="this.style.backgroundColor='#445B74';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="userNameIcon" /></strong>
				    	<a href="canal.php"><strong>
						<input type="button" value="NO" name="Accion" onmouseover="this.style.backgroundColor='#445B74';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="userNameIcon" /></strong></a>
				    </p>
				    <?php
				    }
				    else
				    if($accion == 'Eliminar')
					{
					
					?>
				    <p class="auto-style3">
				    	
				    	<strong>Esta seguro de Eliminar los siguientes Canales de Comunicación?
				    	<input type="submit" value="SI" name="Accion" onmouseover="this.style.backgroundColor='#445B74';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="userNameIcon" /></strong>
				    	<a href="canal.php"><strong>
						<input type="button" value="NO" name="Accion" onmouseover="this.style.backgroundColor='#445B74';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="userNameIcon" /></strong></a>
				    </p>
				    <?php
				    }
				    ?>
				    <?php for($i=0;$i<count($idcats);$i++)
			        	{
							$rows=mysqli_query($conectar,"select * from canal_comunicacion where can_clave_int= '".$idcats[$i]."' order by can_nombre");
							if(mysqli_num_rows($rows)){
							$row=mysqli_fetch_array($rows);
							$act = $row['can_sw_activo'];
			        ?>
				<tr>
					<input  type="hidden" name="idcat[]" id="idcat" value="<?php echo $row['can_clave_int'];?>" />
					<td class="auto-style3" style="width: 221px"><?php echo $row['can_nombre']; ?></td>
					<td class="auto-style3" style="width: 221px"><?php echo $row['can_usu_actualiz']; ?></td>
					<td class="auto-style3" style="width: 221px"><?php echo $row['can_fec_actualiz']; ?></td>
					<td class="auto-style1" style="width: 3px">
					<input name="activarinactivar" id="activarinactivar" type="checkbox" <?php if($act == 1){ echo 'checked="checked"'; } ?> disabled="disabled" ></td>
					<td class="auto-style1">&nbsp;
					</td>
				</tr>
				<?php
							}
						}
		        	}
		        ?>
				<tr>
					<td class="auto-style3" style="width: 221px">&nbsp;</td>
					<td class="auto-style3" style="width: 221px">&nbsp;</td>
					<td class="auto-style3" style="width: 221px">&nbsp;</td>
					<td class="auto-style3" style="width: 3px">&nbsp;</td>
					<td class="auto-style3">&nbsp;</td>
				</tr>
			</table>
			<?php
			}
			?>
			</td>
			<td style="width: 55px">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 60px">&nbsp;</td>
			<td style="width: 117px">&nbsp;</td>
			<td style="width: 65px">&nbsp;</td>
			<td style="width: 70px">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td style="width: 55px">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 60px">&nbsp;</td>
			<td style="width: 117px">&nbsp;</td>
			<td style="width: 65px">&nbsp;</td>
			<td style="width: 70px">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td style="width: 55px">&nbsp;</td>
		</tr>
	</table>
</div>
</form>
</body>
</html>