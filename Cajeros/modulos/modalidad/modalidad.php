<?php
	error_reporting(0);
	include('../../data/Conexion.php');
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_COOKIE['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];
	
	//validacion inactividad en el aplicativo
	if(isset($_SESSION['nw']))
    {
        if($_SESSION['nw']<time())
        {
            unset($_SESSION['nw']);
            //echo "<script>alert('Tiempo agotado - Loguearse nuevamente');</script>";
            echo "<script>alert('Tu sesión se cerrara por inactividad en el sistema - Loguearse nuevamente');</script>";
			echo "<script>parent.location.href=parent.location.href</script>";
 //header("LOCATION:../../data/logout.php");
        }
        else
        {
            $_SESSION['nw'] = time() + (60 * 60);
        }
    }
    else
    {
       //echo "<script>alert('Tiempo agotado - Loguearse nuevamente');</script>";
       	echo "<script>alert('Tu sesión se cerrara por inactividad en el sistema - Loguearse nuevamente');</script>";
		echo "<script>parent.location.href=parent.location.href</script>";
 		//header("LOCATION:../../data/logout.php");
    }
    
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");
	
	$con = mysqli_query($conectar,"select u.usu_clave_int,p.prf_clave_int,p.prf_descripcion from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
	$dato = mysqli_fetch_array($con);
	$claprf = $dato['prf_clave_int'];
	
	$con = mysqli_query($conectar,"select per_metodo from permiso where prf_clave_int = '".$claprf."' and ven_clave_int = 15");
	$dato = mysqli_fetch_array($con);
	$metodo = $dato['per_metodo'];
	
	if($_GET['editarmod'] == 'si')
	{
		$modedi = $_GET['modedi'];
		$con = mysqli_query($conectar,"select * from modalidad where mod_clave_int = '".$modedi."'"); 
		$dato = mysqli_fetch_array($con); 
		$mod = $dato['mod_nombre'];
		$act = $dato['mod_sw_activo'];
		$inm = $dato['mod_sw_inmobiliaria'];
		$dinm = $dato['mod_dias_inmobiliaria'];
		$vis = $dato['mod_sw_visita'];
		$dvis = $dato['mod_dias_visita'];
		$comite = $dato['mod_sw_aprocomite'];
		$dcom = $dato['mod_dias_aprocomite'];
		$contrato = $dato['mod_sw_contrato'];
		$dcon = $dato['mod_dias_contrato'];
		$dis = $dato['mod_sw_diseno'];
		$ddis = $dato['mod_dias_diseno'];
		
		$prefact = $dato['mod_sw_prefactibilidad'];
		$dprefact = $dato['mod_dias_prefactibilidad'];
		$ped = $dato['mod_sw_pedido_maquina'];
		$dped = $dato['mod_dias_pedido_maquina'];
		
		$lic = $dato['mod_sw_licencia'];
		$dlic = $dato['mod_dias_licencia'];
		
		$can = $dato['mod_sw_canal'];
		$dcan = $dato['mod_dias_canal'];
		$pro = $dato['mod_sw_proyeccion'];
		$dpro = $dato['mod_dias_proyeccion'];
        $inf = $dato['mod_sw_infraestructura'];
        $dinf = $dato['mod_dias_infraestructura'];
		
		$preliminar = $dato['mod_sw_preliminar'];
		$dpre = $dato['mod_dias_preliminar'];
		$int = $dato['mod_sw_interventoria'];
		$cons = $dato['mod_sw_constructor'];
		$dcons = $dato['mod_dias_constructor'];
		$seg = $dato['mod_sw_seguridad'];
		
		$tipmod = $dato['mod_sw_modalidad1'];
?>
		<table style="width: 38%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Modalidad:</td>
			<td class="auto-style3">
			<input name="modalidad1" id="modalidad1" value="<?php echo $mod; ?>" class="inputs" type="text" style="width: 200px" />&nbsp;
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3" colspan="2">
			<table style="width: 100%">
				<tr>
					<td colspan="6">
					<table style="width: 100%">
						<tr>
							<td style="width: 10px"><input name="inmobiliaria1" id="inmobiliaria1" onclick="ACTIVARDIAS1()" <?php if($inm == 1){ echo 'checked="checked"'; } ?> type="checkbox" /></td>
							<td style="width: 70px"><label for="inmobiliaria1" style="cursor:pointer">Inmobiliaria</label></td>
							<td style="width: 30px"><input name="diasinmobiliaria1" id="diasinmobiliaria1" value="<?php echo $dinm; ?>" <?php if($inm == 0){ echo 'disabled="disabled"'; } ?> type="text" style="width: 36px" class="inputs" /></td>
							<td style="width: 10px"><input name="visita1" id="visita1" onclick="ACTIVARDIAS1()" <?php if($vis == 1){ echo 'checked="checked"'; } ?> type="checkbox" /></td>
							<td style="width: 60px" class="auto-style11"><label for="visita1" style="cursor:pointer">Visita</label></td>
							<td><input name="diasvisita1" id="diasvisita1" value="<?php echo $dvis; ?>" <?php if($vis == 0){ echo 'disabled="disabled"'; } ?> type="text" style="width: 36px" class="inputs" /></td>
						</tr>
						<tr>
							<td style="width: 10px"><input name="comite1" id="comite1" onclick="ACTIVARDIAS1()" <?php if($comite == 1){ echo 'checked="checked"'; } ?> type="checkbox" /></td>
							<td style="width: 70px"><label for="comite1" style="cursor:pointer">aprobación comité</label></td>
							<td style="width: 30px"><input name="diascomite1" id="diascomite1" value="<?php echo $dcom; ?>" <?php if($comite == 0){ echo 'disabled="disabled"'; } ?> type="text" style="width: 36px" class="inputs" /></td>
							<td style="width: 10px"><input name="contrato1" id="contrato1" onclick="ACTIVARDIAS1()" <?php if($contrato == 1){ echo 'checked="checked"'; } ?> type="checkbox" /></td>
							<td style="width: 60px" class="auto-style11"><label for="contrato1" style="cursor:pointer">Contrato</label></td>
							<td><input name="diascontrato1" id="diascontrato1" value="<?php echo $dcon; ?>" <?php if($contrato == 0){ echo 'disabled="disabled"'; } ?> type="text" style="width: 36px" class="inputs" /></td>
						</tr>
						<tr>
							<td style="width: 10px"><input name="diseno1" id="diseno1" onclick="ACTIVARDIAS1()" <?php if($dis == 1){ echo 'checked="checked"'; } ?> type="checkbox" /></td>
							<td style="width: 70px"><label for="diseno1" style="cursor:pointer">Diseñador</label></td>
							<td style="width: 30px"><input name="diasdiseno1" id="diasdiseno1" value="<?php echo $ddis; ?>" <?php if($dis == 0){ echo 'disabled="disabled"'; } ?> type="text" style="width: 36px" class="inputs" /></td>
							<td style="width: 10px"><input name="prefactibilidad1" id="prefactibilidad1" onclick="ACTIVARDIAS1()" <?php if($prefact == 1){ echo 'checked="checked"'; } ?> type="checkbox" /></td>
							<td style="width: 60px" class="auto-style11"><label for="prefactibilidad1" style="cursor:pointer">Prefactibilidad</label></td>
							<td><input name="diasprefactibilidad1" id="diasprefactibilidad1" value="<?php echo $dprefact; ?>" <?php if($prefact == 0){ echo 'disabled="disabled"'; } ?> type="text" style="width: 36px" class="inputs" /></td>
						</tr>
						<tr>
							<td style="width: 10px"><input name="pedidomaquina1" id="pedidomaquina1" onclick="ACTIVARDIAS1()" <?php if($ped == 1){ echo 'checked="checked"'; } ?> type="checkbox" /></td>
							<td style="width: 70px" class="auto-style11"><label for="pedidomaquina1" style="cursor:pointer">Pedido maquina</label></td>
							<td style="width: 30px"><input name="diaspedidomaquina1" id="diaspedidomaquina1" value="<?php echo $dped; ?>" <?php if($ped == 0){ echo 'disabled="disabled"'; } ?> type="text" style="width: 36px" class="inputs" /></td>
							<td style="width: 10px">
							<input name="licencia1" id="licencia1" onclick="ACTIVARDIAS1()" <?php if($lic == 1){ echo 'checked="checked"'; } ?> type="checkbox" />
							</td>
							<td style="width: 60px"><label for="licencia1" style="cursor:pointer">Licencia</label></td>
							<td><input name="diaslicencia1" id="diaslicencia1" value="<?php echo $dlic; ?>" <?php if($lic == 0){ echo 'disabled="disabled"'; } ?> type="text" style="width: 36px" class="inputs" /></td>
						</tr>
						<tr>
							<td style="width: 10px">
							<input name="canal1" id="canal1" onclick="ACTIVARDIAS1()" <?php if($can == 1){ echo 'checked="checked"'; } ?> type="checkbox" />
							</td>
							<td style="width: 70px" class="auto-style11">
							<label for="canal1" style="cursor:pointer">Canal</label>
							</td>
							<td style="width: 30px">
							<input name="diascanal1" id="diascanal1" value="<?php echo $dcan; ?>" <?php if($can == 0){ echo 'disabled="disabled"'; } ?> type="text" style="width: 36px" class="inputs" />
							</td>
							<td style="width: 10px">
							<input name="proyeccion1" id="proyeccion1" onclick="ACTIVARDIAS1()" <?php if($pro == 1){ echo 'checked="checked"'; } ?> type="checkbox" />
							</td>
							<td style="width: 60px" class="auto-style11">
							<label for="proyeccion1" style="cursor:pointer">Proyección</label>
							</td>
							<td>
							<input name="diasproyeccion1" id="diasproyeccion1" value="<?php echo $dpro; ?>" <?php if($pro == 0){ echo 'disabled="disabled"'; } ?> type="text" style="width: 36px" class="inputs" />
							</td>
						</tr>
						<tr>
							<td style="width: 10px">
							<input name="preliminar1" id="preliminar1" onclick="ACTIVARDIAS1()" <?php if($preliminar == 1){ echo 'checked="checked"'; } ?> type="checkbox" /></td>
							<td style="width: 70px" class="auto-style11">
							<label for="preliminar1" style="cursor:pointer">Preliminar</label></td>
							<td style="width: 30px">
							<input name="diaspreliminar1" id="diaspreliminar1" value="<?php echo $dpre; ?>" <?php if($preliminar == 0){ echo 'disabled="disabled"'; } ?> type="text" style="width: 36px" class="inputs" /></td>
							<td style="width: 10px">
							<input name="constructor1" id="constructor1" onclick="ACTIVARDIAS1()" <?php if($cons == 1){ echo 'checked="checked"'; } ?> type="checkbox" /></td>
							<td style="width: 60px" class="auto-style11">
							<label for="constructor1" style="cursor:pointer">Constructor</label></td>
							<td>
							<input name="diasconstructor1" id="diasconstructor1" value="<?php echo $dcons; ?>" <?php if($cons == 0){ echo 'disabled="disabled"'; } ?> type="text" style="width: 36px" class="inputs" /></td>
						</tr>
                        <tr>
                            <td style="width: 10px">
                                <input name="infraestructura1" id="infraestructura1" onclick="ACTIVARDIAS1()" <?php if($inf == 1){ echo 'checked="checked"'; } ?> type="checkbox" />
                            </td>
                            <td style="width: 60px" class="auto-style11">
                            <label for="infraestructura1" style="cursor:pointer">Infraestructura</label></td>
                            <td>
                                <input name="diasinfraestructura1" id="diasinfraestructura1" value="<?php echo $dcons; ?>" <?php if($inf == 0){ echo 'disabled="disabled"'; } ?> type="text" style="width: 36px" class="inputs" /></td>
                        </tr>
					</table>
					</td>
				</tr>
				<tr>
					<td style="width: 10px">
					<input name="interventor1" id="interventor1" <?php if($int == 1){ echo 'checked="checked"'; } ?> type="checkbox" /></td>
					<td style="width: 70px"><label for="interventor1" style="cursor:pointer">Interventor</label></td>
					<td style="width: 10px">
					<input name="seguridad1" id="seguridad1" <?php if($seg == 1){ echo 'checked="checked"'; } ?> type="checkbox" /></td>
					<td><label for="seguridad1" style="cursor:pointer">Seguridad</label></td>
					<td>
					&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3" colspan="2">
			<table style="width: 37%">
				<tr>
					<td style="width: 80px">Activa:</td>
					<td style="width: 80px"><input class="inputs" <?php if($act == 1){ echo 'checked="checked"'; } ?> name="activo1" type="checkbox" /></td>
					<td style="width: 80px"><label for="tipomodadlidad3" style="cursor:pointer">Modadlidad1:</label></td>
					<td style="width: 2px">
					<input name="tipomodadlidad1" id="tipomodadlidad3" <?php if($tipmod == 1){ echo 'checked="checked"'; } ?> type="radio" value="1"></td>
					<td style="width: 3px"><label for="tipomodadlidad4" style="cursor:pointer">Modalidad2:</label></td>
					<td><input name="tipomodadlidad1" id="tipomodadlidad4" <?php if($tipmod == 0){ echo 'checked="checked"'; } ?> type="radio" value="0"></td>
				</tr>
			</table>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4">
			<input name="submit" type="button" value="Guardar" onclick="GUARDAR('MODALIDAD','<?php echo $modedi; ?>')"  style="width: 348px; height: 25px; cursor:pointer" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">
			<div id="datos">
			</div>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
<?php
		exit();
	}
	
	if($_GET['guardarmod'] == 'si')
	{
		sleep(1);
		$mod = $_GET['mod'];
		$act = $_GET['act'];
		$lm = $_GET['lm'];
		$m = $_GET['m'];
		
		$inm = $_GET['inm'];
		$dinm = $_GET['dinm'];
		$vis = $_GET['vis'];
		$dvis = $_GET['dvis'];
		$comite = $_GET['comite'];
		$dcom = $_GET['dcom'];
		$contrato = $_GET['contrato'];
		$dcon = $_GET['dcon'];
		$dis = $_GET['dis'];
		$ddis = $_GET['ddis'];
		
		$prefact = $_GET['prefact'];
		$dprefact = $_GET['dprefact'];
		$ped = $_GET['ped'];
		$dped = $_GET['dped'];
		
		$lic = $_GET['lic'];
		$dlic = $_GET['dlic'];
		
		$can = $_GET['can'];
		$dcan = $_GET['dcan'];
		$pro = $_GET['pro'];
		$dpro = $_GET['dpro'];

		$inf = $_GET['inf'];
		$dinf = $_GET['dinf'];
		
		$preliminar = $_GET['preliminar'];
		$dpre = $_GET['dpre'];
		$inter = $_GET['inter'];
		$cons = $_GET['cons'];
		$dcons = $_GET['dcons'];
		$seg = $_GET['seg'];
		
		$tipmod = $_GET['tipmod'];
		
		$sql = mysqli_query($conectar,"select * from modalidad where (UPPER(mod_nombre) = UPPER('".$mod."')) AND mod_clave_int <> '".$m."'");
		$dato = mysqli_fetch_array($sql);
		$conmod = $dato['mod_nombre'];
		
		if($mod == '')
		{
			echo "<div class='validaciones'>Debe ingresar el campo modalidad</div>";
		}
		else
		if(STRTOUPPER($conmod) == STRTOUPPER($mod))
		{
			echo "<div class='validaciones'>El campo modalidad ingresado ya existe</div>";
		}
		else
		if($lm < 3)
		{
			echo "<div class='validaciones'>El campo modalidad debe ser mí­nimo de 3 dijitos</div>";
		}
		else
		if($tipmod == '')
		{
			echo "<div class='validaciones'>Debe elegir el tipo de modalidad</div>";
		}
		else
		{			
			if($act == 'false'){ $swact = 0; }elseif($act == 'true'){ $swact = 1; }
			if($inm == 'false'){ $inm = 0; }elseif($inm == 'true'){ $inm = 1; }
			if($vis == 'false'){ $vis = 0; }elseif($vis == 'true'){ $vis = 1; }
			if($comite == 'false'){ $comite = 0; }elseif($comite == 'true'){ $comite = 1; }
			if($contrato == 'false'){ $contrato = 0; }elseif($contrato == 'true'){ $contrato = 1; }
			if($dis == 'false'){ $dis = 0; }elseif($dis == 'true'){ $dis = 1; }
			
			if($prefact == 'false'){ $prefact = 0; }elseif($prefact == 'true'){ $prefact = 1; }
			if($ped == 'false'){ $ped = 0; }elseif($ped == 'true'){ $ped = 1; }
			
			if($lic == 'false'){ $lic = 0; }elseif($lic == 'true'){ $lic = 1; }
			
			if($can == 'false'){ $can = 0; }elseif($can == 'true'){ $can = 1; }
			if($pro == 'false'){ $pro = 0; }elseif($pro == 'true'){ $pro = 1; }
            if($inf == 'false'){ $inf = 0; }elseif($inf == 'true'){ $inf = 1; }
			
			if($preliminar == 'false'){ $preliminar = 0; }elseif($preliminar == 'true'){ $preliminar = 1; }
			if($inter == 'false'){ $inter = 0; }elseif($inter == 'true'){ $inter = 1; }
			if($cons == 'false'){ $cons = 0; }elseif($cons == 'true'){ $cons = 1; }
			if($seg == 'false'){ $seg = 0; }elseif($seg == 'true'){ $seg = 1; }
			
			$con = mysqli_query($conectar,"update modalidad set mod_sw_prefactibilidad = '".$prefact."',mod_dias_prefactibilidad = '".$dprefact."',mod_sw_pedido_maquina = '".$ped."',mod_dias_pedido_maquina = '".$dped."',mod_sw_canal = '".$can."',mod_dias_canal = '".$dcan."',mod_sw_proyeccion = '".$pro."',mod_dias_proyeccion = '".$dpro."',mod_nombre = '".$mod."', mod_sw_activo = '".$swact."', mod_sw_inmobiliaria = '".$inm."', mod_dias_inmobiliaria = '".$dinm."', mod_sw_visita = '".$vis."', mod_dias_visita = '".$dvis."', mod_sw_aprocomite = '".$comite."', mod_dias_aprocomite = '".$dcom."', mod_sw_contrato = '".$contrato."', mod_dias_contrato = '".$dcon."', mod_sw_diseno = '".$dis."', mod_dias_diseno = '".$ddis."', mod_sw_licencia = '".$lic."', mod_dias_licencia = '".$dlic."', mod_sw_interventoria = '".$inter."', mod_sw_constructor = '".$cons."', mod_dias_constructor = '".$dcons."', mod_sw_seguridad = '".$seg."', mod_sw_modalidad1 = '".$tipmod."', mod_usu_actualiz = '".$usuario."', mod_fec_actualiz = '".$fecha."',mod_sw_infraestructura = '".$inf."', mod_dias_infraestructura = '".$dinf."' where mod_clave_int = '".$m."'");
			
			if($con >= 1)
			{
				echo "<div class='ok'>Datos grabados correctamente</div>";
				mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'15',16,'".$m."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 16=Actualización atiende
			}
			else
			{
				//echo "update modalidad set mod_sw_prefactibilidad = '".$prefact."',mod_dias_prefactibilidad = '".$dprefact."',mod_sw_pedido_maquina = '".$ped."',mod_dias_pedido_maquina = '".$dped."',mod_sw_canal = '".$can."',mod_dias_canal = '".$dcan."',mod_sw_proyeccion = '".$pro."',mod_dias_proyeccion = '".$dpro."',mod_nombre = '".$mod."', mod_sw_activo = '".$swact."', mod_sw_inmobiliaria = '".$inm."', mod_dias_inmobiliaria = '".$dinm."', mod_sw_visita = '".$vis."', mod_dias_visita = '".$dvis."', mod_sw_aprocomite = '".$comite."', mod_dias_aprocomite = '".$dcom."', mod_sw_contrato = '".$contrato."', mod_dias_contrato = '".$dcon."', mod_sw_diseno = '".$dis."', mod_dias_diseno = '".$ddis."', mod_sw_licencia = '".$lic."', mod_dias_licencia = '".$dlic."', mod_sw_interventoria = '".$inter."', mod_sw_constructor = '".$cons."', mod_dias_constructor = '".$dcons."', mod_sw_seguridad = '".$seg."', mod_sw_modalidad1 = '".$tipmod."', mod_usu_actualiz = '".$usuario."', mod_fec_actualiz = '".$fecha."',mod_sw_infraestructura = '".$inf."', mod_dias_infraestructura = '".$dinf."' where mod_clave_int = '".$m."'";
				echo "<div class='validaciones'>No se han podido guardar los datos</div>";
			}	
		}
		exit();
	}
	
	if($_GET['nuevamodalidad'] == 'si')
	{
		sleep(1);
		$fecha=date("Y/m/d H:i:s");
		$mod = $_GET['mod'];
		$act = $_GET['act'];
		$lm = $_GET['lm'];
		$inm = $_GET['inm'];
		$dinm = $_GET['dinm'];
		$vis = $_GET['vis'];
		$dvis = $_GET['dvis'];
		$comite = $_GET['comite'];
		$dcom = $_GET['dcom'];
		$contrato = $_GET['contrato'];
		$dcon = $_GET['dcon'];
		$dis = $_GET['dis'];
		$ddis = $_GET['ddis'];
		
		$prefact = $_GET['prefact'];
		$dprefact = $_GET['dprefact'];
		$ped = $_GET['ped'];
		$dped = $_GET['dped'];
		
		$lic = $_GET['lic'];
		$dlic = $_GET['dlic'];
		
		$can = $_GET['can'];
		$dcan = $_GET['dcan'];
		$pro = $_GET['pro'];
		$dpro = $_GET['dpro'];

        $inf = $_GET['inf'];
        $dinf = $_GET['dinf'];
		
		$preliminar = $_GET['preliminar'];
		$dpre = $_GET['dpre'];
		$inter = $_GET['inter'];
		$cons = $_GET['cons'];
		$dcons = $_GET['dcons'];
		$seg = $_GET['seg'];
		
		$tipmod = $_GET['tipmod'];
		
		$sql = mysqli_query($conectar,"select * from modalidad where (UPPER(mod_nombre) = UPPER('".$mod."'))");
		$dato = mysqli_fetch_array($sql);
		$conmod = $dato['mod_nombre'];
		
		if($mod == '')
		{
			echo "<div class='validaciones'>Debe ingresar el campo modalidad</div>";
		}
		else
		if(STRTOUPPER($conmod) == STRTOUPPER($mod))
		{
			echo "<div class='validaciones'>El campo modalidad ingresado ya existe</div>";
		}
		else
		if($lm < 3)
		{
			echo "<div class='validaciones'>El campo modalidad debe ser mí­nimo de 3 dijitos</div>";
		}
		else
		if($tipmod == '')
		{
			echo "<div class='validaciones'>Debe elegir el tipo de modalidad</div>";
		}
		else
		{
			if($act == 'false'){ $swact = 0; }elseif($act == 'true'){ $swact = 1; }
			if($inm == 'false'){ $inm = 0; }elseif($inm == 'true'){ $inm = 1; }
			if($vis == 'false'){ $vis = 0; }elseif($vis == 'true'){ $vis = 1; }
			if($comite == 'false'){ $comite = 0; }elseif($comite == 'true'){ $comite = 1; }
			if($contrato == 'false'){ $contrato = 0; }elseif($contrato == 'true'){ $contrato = 1; }
			if($dis == 'false'){ $dis = 0; }elseif($dis == 'true'){ $dis = 1; }
			
			if($prefact == 'false'){ $prefact = 0; }elseif($prefact == 'true'){ $prefact = 1; }
			if($ped == 'false'){ $ped = 0; }elseif($ped == 'true'){ $ped = 1; }
			
			if($lic == 'false'){ $lic = 0; }elseif($lic == 'true'){ $lic = 1; }
			
			if($can == 'false'){ $can = 0; }elseif($can == 'true'){ $can = 1; }
			if($pro == 'false'){ $pro = 0; }elseif($pro == 'true'){ $pro = 1; }
            if($inf == 'false'){ $inf = 0; }elseif($inf == 'true'){ $inf = 1; }
			
			if($preliminar == 'false'){ $preliminar = 0; }elseif($preliminar == 'true'){ $preliminar = 1; }
			if($inter == 'false'){ $inter = 0; }elseif($inter == 'true'){ $inter = 1; }
			if($cons == 'false'){ $cons = 0; }elseif($cons == 'true'){ $cons = 1; }
			if($seg == 'false'){ $seg = 0; }elseif($seg == 'true'){ $seg = 1; }
			$con = mysqli_query($conectar,"insert into modalidad(mod_nombre,mod_sw_activo,mod_sw_inmobiliaria,mod_dias_inmobiliaria,mod_sw_visita,mod_dias_visita,mod_sw_aprocomite,mod_dias_aprocomite,mod_sw_contrato,mod_dias_contrato,mod_sw_diseno,mod_dias_diseno,mod_sw_prefactibilidad,mod_dias_prefactibilidad,mod_sw_pedido_maquina,mod_dias_pedido_maquina,mod_sw_licencia,mod_dias_licencia,mod_sw_canal,mod_dias_canal,mod_sw_proyeccion,mod_dias_proyeccion,mod_sw_preliminar,mod_dias_preliminar,mod_sw_interventoria,mod_sw_constructor,mod_dias_constructor,mod_sw_seguridad,mod_sw_modalidad1,mod_usu_actualiz,mod_fec_actualiz,mod_sw_infraestructura,mod_dias_infraestructura) values('".$mod."','".$swact."','".$inm."','".$dinm."','".$vis."','".$dvis."','".$comite."','".$dcom."','".$contrato."','".$dcon."','".$dis."','".$ddis."','".$prefact."','".$dprefact."','".$ped."','".$dped."','".$lic."','".$dlic."','".$can."','".$dcan."','".$pro."','".$dpro."','".$preliminar."','".$dpre."','".$inter."','".$cons."','".$dcons."','".$seg."','".$tipmod."','".$usuario."','".$fecha."','".$inf."','".$dinf."')");
			
			if($con >= 1)
			{
				echo "<div class='ok'>Datos grabados correctamente</div>";
				$con = mysqli_query($conectar,"select max(mod_clave_int) max from modalidad");
				$dato = mysqli_fetch_array($con);
				$max = $dato['max'];
				mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'15',15,'".$max."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 15=Creación atiende
			}
			else
			{
				echo "<div class='validaciones'>No se han podido guardar los datos</div>";
			}
		}
		exit();
	}
	if($_GET['todos'] == 'si')
	{
		sleep(1);
		$rows=mysqli_query($conectar,"select * from modalidad");
		$total=mysqli_num_rows($rows);
?>
		<table style="width: 100%">
		<tr>
			<td class="auto-style3" style="width: 27px">
				<input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<?php echo $total;?>,this);" class="auto-style6" /><span class="auto-style6">
				</span>
			</td>
			<td class="auto-style3" colspan="18">
			<?php
			if($metodo == 1)
			{
			?>
			<table style="width: 30%">
				<tr>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/activo.png" alt="" class="auto-style6" /><input type="submit" value="Activar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/inactivo.png" alt="" class="auto-style6" /><input type="submit" value="Inactivar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/eliminar.png" alt="" class="auto-style6" /><input type="submit" value="Eliminar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
				</tr>
			</table>
			<?php
			}
			?>
			</td>
		</tr>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px"><strong>Modalidad</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Inmob.</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Visita</strong></td>
			<td class="auto-style9" style="width: 200px"><strong>Aprob. Comité</strong></td>
			<td class="auto-style9" style="width: 200px"><strong>Contrato</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Diseño</strong></td>
			<td class="auto-style9" style="width: 200px"><strong>Prefact.</strong></td>
			<td class="auto-style9" style="width: 200px"><strong>Pedido Maquina</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Licencia</strong></td>
			<td class="auto-style9" style="width: 200px"><strong>Canal</strong></td>
			<td class="auto-style9" style="width: 200px"><strong>Proyección</strong></td>
            <td class="auto-style9" style="width: 200px"><strong>Infraestructura</strong></td>
			<td class="auto-style9" style="width: 200px"><strong>Preliminar</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Interv.</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Cons.</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Segur.</strong></td>
			<td class="auto-style5" style="width: 29px"><strong>Tipo</strong></td>
			<td class="auto-style5" style="width: 29px"><strong>
			Activo</strong></td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
		<?php
			$contador=0;
			$con = mysqli_query($conectar,"select * from modalidad order by mod_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$mod = $dato['mod_nombre'];
				$act = $dato['mod_sw_activo'];
				
				$inm = $dato['mod_sw_inmobiliaria'];
				$dinm = $dato['mod_dias_inmobiliaria'];
				$vis = $dato['mod_sw_visita'];
				$dvis = $dato['mod_dias_visita'];
				$comite = $dato['mod_sw_aprocomite'];
				$dcom = $dato['mod_dias_aprocomite'];
				$contrato = $dato['mod_sw_contrato'];
				$dcon = $dato['mod_dias_contrato'];
				$dis = $dato['mod_sw_diseno'];
				$ddis = $dato['mod_dias_diseno'];
				
				$prefact = $dato['mod_sw_prefactibilidad'];
				$dprefact = $dato['mod_dias_prefactibilidad'];
				$ped = $dato['mod_sw_pedido_maquina'];
				$dped = $dato['mod_dias_pedido_maquina'];
				
				$lic = $dato['mod_sw_licencia'];
				$dlic = $dato['mod_dias_licencia'];
				
				$can = $dato['mod_sw_canal'];
				$dcan = $dato['mod_dias_canal'];
				$pro = $dato['mod_sw_proyeccion'];
				$dpro = $dato['mod_dias_proyeccion'];

                $inf = $dato['mod_sw_infraestructura'];
                $dinf = $dato['mod_dias_infraestructura'];
				
				$preliminar = $dato['mod_sw_preliminar'];
				$dpre = $dato['mod_dias_preliminar'];
				$int = $dato['mod_sw_interventoria'];
				$cons = $dato['mod_sw_constructor'];
				$dcons = $dato['mod_dias_constructor'];
				$seg = $dato['mod_sw_seguridad'];
				$tipmod = $dato['mod_sw_modalidad1'];
				$usuact = $dato['mod_usu_actualiz'];
				$fecact = $dato['mod_fec_actualiz'];
				$contador=$contador+1;
		?>
		<tr>
			<td class="auto-style3" style="width: 27px">
				<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<?php echo $contador;?>" value="<?php echo $dato['mod_clave_int'];?>" class="auto-style6" /></td>
				<td class="auto-style5" style="width: 200px"><?php echo $mod; ?></td>
				<td class="auto-style1" style="width: 200px" ><?php if($inm == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dinm; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" ><?php if($vis == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dvis; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" ><?php if($comite == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dcom; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" /></td>
				<td class="auto-style1" style="width: 200px" ><?php if($contrato == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dcon; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" /></td>
				<td class="auto-style1" style="width: 200px" ><?php if($dis == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $ddis; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" >
				<?php if($prefact == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dprefact; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" >
				<?php if($ped == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dped; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" ><?php if($lic == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dlic; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" >
				<?php if($can == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dcan; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" >
				<?php if($pro == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dpro; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
            <td class="auto-style1" style="width: 200px" >
                <?php if($inf == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
                <input name="Text1" value="<?php echo $dinf; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
            </td>
				<td class="auto-style1" style="width: 200px" ><?php if($preliminar == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dpre; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" /></td>
				<td class="auto-style1" style="width: 200px" ><?php if($int == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?></td>
				<td class="auto-style1" style="width: 200px" ><?php if($cons == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dcons; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" ><?php if($seg == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?></td>
			<td class="auto-style1" style="width: 30px">
			<?php if($tipmod == 1){ echo "M1"; }else{ echo "M2"; } ?></td>
			<td class="auto-style1" style="width: 30px">
			<input name="activarinactivar" id="activarinactivar" type="checkbox" <?php if($act == 1){ echo 'checked="checked"'; } ?> disabled="disabled" class="auto-style6" ></td>
			<td class="auto-style5">
			<?php
			if($metodo == 1)
			{
			?>
			<a data-reveal-id="editarmodalidad" data-animation="fade" style="cursor:pointer" onclick="EDITAR('<?php echo $dato['mod_clave_int']; ?>','MODALIDAD')"><img src="../../img/editar.png" alt="" height="22" width="21" /></a>
			<?php
			}
			?>
			</td>
		</tr>
		<?php
			}
		?>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
            <td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 29px">&nbsp;</td>
			<td class="auto-style5" style="width: 29px">&nbsp;</td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
	</table>
<?php
		exit();
	}
?>
<?php
	if($_GET['buscarmod'] == 'si')
	{
		$mod = $_GET['mod'];
		$act = $_GET['act'];

		$rows=mysqli_query($conectar,"select * from modalidad where (mod_nombre LIKE REPLACE('%".$mod."%',' ','%') OR '".$mod."' IS NULL OR '".$mod."' = '') and (mod_sw_activo = '".$act."' OR '".$act."' IS NULL OR '".$act."' = '')");
		$total=mysqli_num_rows($rows);
?>
		<table style="width: 100%">
		<tr>
			<td class="auto-style3" style="width: 27px">
				<input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<?php echo $total;?>,this);" class="auto-style6" /><span class="auto-style6">
				</span>
			</td>
			<td class="auto-style3" colspan="18">
			<?php
			if($metodo == 1)
			{
			?>
			<table style="width: 30%">
				<tr>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/activo.png" alt="" class="auto-style6" /><input type="submit" value="Activar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/inactivo.png" alt="" class="auto-style6" /><input type="submit" value="Inactivar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/eliminar.png" alt="" class="auto-style6" /><input type="submit" value="Eliminar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
				</tr>
			</table>
			<?php
			}
			?>
			</td>
		</tr>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px"><strong>Modalidad</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Inmob.</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Visita</strong></td>
			<td class="auto-style9" style="width: 200px"><strong>Aprob. Comité</strong></td>
			<td class="auto-style9" style="width: 200px"><strong>Contrato</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Diseño</strong></td>
			<td class="auto-style9" style="width: 200px"><strong>Prefact.</strong></td>
			<td class="auto-style9" style="width: 200px"><strong>Pedido Maquina</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Licencia</strong></td>
			<td class="auto-style9" style="width: 200px"><strong>Canal</strong></td>

			<td class="auto-style9" style="width: 200px"><strong>Proyección</strong></td>
            <td class="auto-style9" style="width: 200px"><strong>Infraestructura</strong></td>
			<td class="auto-style9" style="width: 200px"><strong>Preliminar</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Interv.</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Cons.</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Segur.</strong></td>
			<td class="auto-style5" style="width: 29px"><strong>Tipo</strong></td>
			<td class="auto-style5" style="width: 29px"><strong>
			Activo</strong></td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
		<?php
			$contador=0;
			$con = mysqli_query($conectar,"select * from modalidad where (mod_nombre LIKE REPLACE('%".$mod."%',' ','%') OR '".$mod."' IS NULL OR '".$mod."' = '') and (mod_sw_activo = '".$act."' OR '".$act."' IS NULL OR '".$act."' = '') order by mod_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$mod = $dato['mod_nombre'];
				$act = $dato['mod_sw_activo'];
				
				$inm = $dato['mod_sw_inmobiliaria'];
				$dinm = $dato['mod_dias_inmobiliaria'];
				$vis = $dato['mod_sw_visita'];
				$dvis = $dato['mod_dias_visita'];
				$comite = $dato['mod_sw_aprocomite'];
				$dcom = $dato['mod_dias_aprocomite'];
				$contrato = $dato['mod_sw_contrato'];
				$dcon = $dato['mod_dias_contrato'];
				$dis = $dato['mod_sw_diseno'];
				$ddis = $dato['mod_dias_diseno'];
				
				$prefact = $dato['mod_sw_prefactibilidad'];
				$dprefact = $dato['mod_dias_prefactibilidad'];
				$ped = $dato['mod_sw_pedido_maquina'];
				$dped = $dato['mod_dias_pedido_maquina'];
				
				$lic = $dato['mod_sw_licencia'];
				$dlic = $dato['mod_dias_licencia'];
				
				$can = $dato['mod_sw_canal'];
				$dcan = $dato['mod_dias_canal'];
				$pro = $dato['mod_sw_proyeccion'];
				$dpro = $dato['mod_dias_proyeccion'];
                $inf = $dato['mod_sw_infraestructura'];
                $dinf = $dato['mod_dias_infraestructura'];
				
				$preliminar = $dato['mod_sw_preliminar'];
				$dpre = $dato['mod_dias_preliminar'];
				$int = $dato['mod_sw_interventoria'];
				$cons = $dato['mod_sw_constructor'];
				$dcons = $dato['mod_dias_constructor'];
				$seg = $dato['mod_sw_seguridad'];
				$tipmod = $dato['mod_sw_modalidad1'];
				$usuact = $dato['mod_usu_actualiz'];
				$fecact = $dato['mod_fec_actualiz'];
				$contador=$contador+1;
		?>
		<tr>
			<td class="auto-style3" style="width: 27px">
				<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<?php echo $contador;?>" value="<?php echo $dato['mod_clave_int'];?>" class="auto-style6" /></td>
			<td class="auto-style5" style="width: 200px"><?php echo $mod; ?></td>
				<td class="auto-style1" style="width: 200px" ><?php if($inm == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dinm; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" ><?php if($vis == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dvis; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" ><?php if($comite == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dcom; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" /></td>
				<td class="auto-style1" style="width: 200px" ><?php if($contrato == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dcon; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" /></td>
				<td class="auto-style1" style="width: 200px" ><?php if($dis == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $ddis; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" >
				<?php if($prefact == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dprefact; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" >
				<?php if($ped == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dped; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" ><?php if($lic == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dlic; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" >
				<?php if($can == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dcan; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" >
				<?php if($pro == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dpro; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
            <td class="auto-style1" style="width: 200px" >
                <?php if($inf == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
                <input name="Text1" value="<?php echo $dinf; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
            </td>
				<td class="auto-style1" style="width: 200px" ><?php if($preliminar == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dpre; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" /></td>
				<td class="auto-style1" style="width: 200px" ><?php if($int == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?></td>
				<td class="auto-style1" style="width: 200px" ><?php if($cons == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dcons; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" ><?php if($seg == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?></td>
			<td class="auto-style1" style="width: 30px">
			<?php if($tipmod == 1){ echo "M1"; }else{ echo "M2"; } ?></td>
			<td class="auto-style1" style="width: 30px">
			<input name="activarinactivar" id="activarinactivar" type="checkbox" <?php if($act == 1){ echo 'checked="checked"'; } ?> disabled="disabled" class="auto-style6" ></td>
			<td class="auto-style5">
			<?php
			if($metodo == 1)
			{
			?>
			<a data-reveal-id="editarmodalidad" data-animation="fade" style="cursor:pointer" onclick="EDITAR('<?php echo $dato['mod_clave_int']; ?>','MODALIDAD')"><img src="../../img/editar.png" alt="" height="22" width="21" /></a>
			<?php
			}
			?>
			</td>
		</tr>
		<?php
			}
		?>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
            <td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 29px">&nbsp;</td>
			<td class="auto-style5" style="width: 29px">&nbsp;</td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
	</table>
<?php
		exit();
	}
?>
<!DOCTYPE HTML>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

	
<title>CONTROL DE OBRAS</title>
<meta name="description" content="Service Desk">
<meta name="author" content="InvGate S.R.L.">

<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
<!--[if lte IE 7]>
<link rel="stylesheet" href="css/ie.c32bc18afe0e2883ee4912a51f86c119.css" type="text/css" />
<![endif]-->

<style type="text/css">
.auto-style1 {
	text-align: center;
}
.auto-style2 {
	font-size: 12px;
	font-family: Arial, helvetica;
	outline: none;
/*transition: all 0.75s ease-in-out;*/ /*-webkit-transition: all 0.75s ease-in-out;*/ /*-moz-transition: all 0.75s ease-in-out;*/border-radius: 3px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 3px;
	border: 1px solid rgba(0,0,0, 0.2);
	background-color: #eee;
	padding: 3px;
	box-shadow: 0 0 10px #aaa;
	-webkit-box-shadow: 0 0 10px #aaa;
	-moz-box-shadow: 0 0 10px #aaa;
	border: 1px solid #999;
	background-color: white;
	text-align: center;
}
.auto-style3 {
	text-align: left;
}
.inputs{
	font-size:14px;
    font-family: Arial, helvetica;
    outline:none;
    border-radius:3px;
    -webkit-border-radius:6px;
    -moz-border-radius:3px;
    border:1px solid rgba(0,0,0, 0.5);
    padding: 3px;
}
.validaciones{
	font-size:14px;
    font-family: Arial, helvetica;
    outline:none;
    border-radius:3px;
    -webkit-border-radius:6px;
    -moz-border-radius:3px;
    border:1px solid rgba(0,0,0, 0.2);
    color:maroon;
    background-color:#eee;
    padding: 3px;
    text-align:center;
}
.ok{
	font-size:14px;
    font-family: Arial, helvetica;
    outline:none;
    border-radius:3px;
    -webkit-border-radius:6px;
    -moz-border-radius:3px;
    border:1px solid rgba(0,0,0, 0.2);
    color:green;
    background-color:#eee;
    padding: 3px;
    text-align:center;
}
.auto-style5 {
	text-align: left;
	font-family: Arial, Helvetica, sans-serif;
}
.auto-style6 {
	font-family: Arial, Helvetica, sans-serif;
}
.auto-style7 {
	text-align: center;
	font-size: large;
}
.auto-style8 {
	text-align: center;
	font-family: Arial, Helvetica, sans-serif;
}
.auto-style9 {
	text-align: center;
	font-family: Arial, Helvetica, sans-serif;
	color: #0000FF;
}
.auto-style11 {
	text-align: left;
	color: #0000FF;
}
</style>

<script type="text/javascript" language="javascript">
	selecteds=0;
	
	function CheckUncheck(total,check){
		checkbox=null;
		for(i=1;i<=total;i++){
			checkbox=document.getElementById("idcat"+i);
			//alert(checkbox.value);
			checkbox.checked=check.checked;
		}
		
		if(check.checked){
			selecteds=total;
		}else{
			selecteds=0;
		}
		
	}
	
	function contadorVals(check){
		if(check.checked){
			selecteds=selecteds+1;
		}else{
			selecteds=selecteds-1;
		}
	}
	
	function selectedVals(){
		if(selecteds==0){
			alert("Seleccione al menos un registro.");
			return false;
		}else{
			return true;
		}
	}
	function ACTIVARDIAS1()
	{
		var inm = form1.inmobiliaria1.checked;
		var vis = form1.visita1.checked;
		var dis = form1.diseno1.checked;
		var lic = form1.licencia1.checked;
		var comite = form1.comite1.checked;
		var contrato = form1.contrato1.checked;
		var preliminar = form1.preliminar1.checked;
		var constructor = form1.constructor.checked;
		
		var prefact = form1.prefactibilidad1.checked;
		var ped = form1.pedidomaquina1.checked;
		var can = form1.canal1.checked;
		var pro = form1.proyeccion1.checked;
        var inf = form1.infraestructura1.checked;
		
		if(inm == true){ form1.diasinmobiliaria1.disabled = false; }else{ form1.diasinmobiliaria1.disabled = true; form1.diasinmobiliaria1.value = 0; }
		if(vis == true){ form1.diasvisita1.disabled = false; }else{ form1.diasvisita1.disabled = true; form1.diasvisita1.value = 0; }
		if(dis == true){ form1.diasdiseno1.disabled = false; }else{ form1.diasdiseno1.disabled = true; form1.diasdiseno1.value = 0; }
		if(lic == true){ form1.diaslicencia1.disabled = false; }else{ form1.diaslicencia1.disabled = true; form1.diaslicencia1.value = 0; }
		if(comite == true){ form1.diascomite1.disabled = false; }else{ form1.diascomite1.disabled = true; form1.diascomite1.value = 0; }
		if(contrato == true){ form1.diascontrato1.disabled = false; }else{ form1.diascontrato1.disabled = true; form1.diascontrato1.value = 0; }
		if(preliminar == true){ form1.diaspreliminar1.disabled = false; }else{ form1.diaspreliminar1.disabled = true; form1.diaspreliminar1.value = 0; }
		if(constructor == true){ form1.diasconstructor1.disabled = false; }else{ form1.diasconstructor1.disabled = true; form1.diasconstructor1.value = 0; }
	
		if(prefact == true){ form1.diasprefactibilidad1.disabled = false; }else{ form1.diasprefactibilidad1.disabled = true; form1.diasprefactibilidad1.value = 0; }
		if(ped == true){ form1.diaspedidomaquina1.disabled = false; }else{ form1.diaspedidomaquina1.disabled = true; form1.diaspedidomaquina1.value = 0; }
		if(can == true){ form1.diascanal1.disabled = false; }else{ form1.diascanal1.disabled = true; form1.diascanal1.value = 0; }
		if(pro == true){ form1.diasproyeccion1.disabled = false; }else{ form1.diasproyeccion1.disabled = true; form1.diasproyeccion1.value = 0; }

        if(inf == true){ form1.diasinfraestructura1.disabled = false; }else{ form1.diasinfraestructura1.disabled = true; form1.diasinfraestructura1.value = 0; }
	}
	function ACTIVARDIAS()
	{
		var inm = form1.inmobiliaria.checked;
		var vis = form1.visita.checked;
		var dis = form1.diseno.checked;
		var lic = form1.licencia.checked;
		var comite = form1.comite.checked;
		var contrato = form1.contrato.checked;
		var preliminar = form1.preliminar.checked;
		var constructor = form1.constructor.checked;
		var inf = form1.infraestructura.checked;
		
		var prefact = form1.prefactibilidad.checked;
		var ped = form1.pedidomaquina.checked;
		var can = form1.canal.checked;
		var pro = form1.proyeccion.checked;
		
		if(inm == true){ form1.diasinmobiliaria.disabled = false; }else{ form1.diasinmobiliaria.disabled = true; form1.diasinmobiliaria.value = 0; }
		if(vis == true){ form1.diasvisita.disabled = false; }else{ form1.diasvisita.disabled = true; form1.diasvisita.value = 0; }
		if(dis == true){ form1.diasdiseno.disabled = false; }else{ form1.diasdiseno.disabled = true; form1.diasdiseno.value = 0; }
		if(lic == true){ form1.diaslicencia.disabled = false; }else{ form1.diaslicencia.disabled = true; form1.diaslicencia.value = 0; }
		if(comite == true){ form1.diascomite.disabled = false; }else{ form1.diascomite.disabled = true; form1.diascomite.value = 0; }
		if(contrato == true){ form1.diascontrato.disabled = false; }else{ form1.diascontrato.disabled = true; form1.diascontrato.value = 0; }
		if(preliminar == true){ form1.diaspreliminar.disabled = false; }else{ form1.diaspreliminar.disabled = true; form1.diaspreliminar.value = 0; }
		if(constructor == true){ form1.diasconstructor.disabled = false; }else{ form1.diasconstructor.disabled = true; form1.diasconstructor.value = 0; }
		
		if(prefact == true){ form1.diasprefactibilidad.disabled = false; }else{ form1.diasprefactibilidad.disabled = true; form1.diasprefactibilidad.value = 0; }
		if(ped == true){ form1.diaspedidomaquina.disabled = false; }else{ form1.diaspedidomaquina.disabled = true; form1.diaspedidomaquina.value = 0; }
		if(can == true){ form1.diascanal.disabled = false; }else{ form1.diascanal.disabled = true; form1.diascanal.value = 0; }
		if(pro == true){ form1.diasproyeccion.disabled = false; }else{ form1.diasproyeccion.disabled = true; form1.diasproyeccion.value = 0; }
        if(inf == true){ form1.diasinfraestructura.disabled = false; }else{ form1.diasinfraestructura.disabled = true; form1.diasinfraestructura.value = 0; }
	}
</script>
<?php //VALIDACIONES ?>
<script type="text/javascript" src="llamadas.js?<?php echo time();?>"></script>
<script type="text/javascript" src="../../js/jquery-1.6.min.js"></script>

<?php //VENTANA EMERGENTE ?>
<link rel="stylesheet" href="../../css/reveal.css" />
<script type="text/javascript" src="../../js/jquery-1.6.min.js"></script>
<script type="text/javascript" src="../../js/jquery.reveal.js"></script>

</head>


<body>
<?php
$rows=mysqli_query($conectar,"select * from modalidad");
$total=mysqli_num_rows($rows);
?>
<form name="form1" id="form1" action="confirmar.php" method="post" onsubmit="return selectedVals();">
<!--[if lte IE 7]>
<div class="ieWarning">Este navegador no es compatible con el sistema. Por favor, use Chrome, Safari, Firefox o Internet Explorer 8 o superior.</div>
<![endif]-->
<table style="width: 100%">
	<tr>
		<td class="auto-style2" onclick="CONSULTAMODULO('TODOS')" onmouseover="this.style.backgroundColor='#445B74';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="width: 60px; cursor:pointer">
		Todos
		<?php
			$con = mysqli_query($conectar,"select COUNT(*) cant from modalidad");
			$dato = mysqli_fetch_array($con);
			echo $dato['cant'];
		?>
		</td>
		<td class="auto-style7" style="cursor:pointer" colspan="4">
		MAESTRA MODALIDAD</td>
		<td class="auto-style2" onmouseover="this.style.backgroundColor='#CCCCCC';this.style.color='#0F213C';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="width: 55px; cursor:pointer">
		<?php
		if($metodo == 1)
		{
		?>
		<a data-reveal-id="nuevamodalidad" data-animation="fade" style="cursor:pointer"><table style="width: 100%">
			<tr>
				<td style="width: 14px"><a data-reveal-id="nuevamodalidad" data-animation="fade" style="cursor:pointer"><img alt="" src="../../img/add2.png"></a></td>
				<td>Añadir</td>
			</tr>
		</table></a>
		<?php
		}
		?>
		</td>
	</tr>
	<tr>
		<td class="auto-style2" colspan="6">
		<div id="filtro">
			<table style="width: 33%">
				<tr>
					<td class="auto-style1"><strong>Filtro:<img src="../../img/buscar.png" alt="" height="18" width="15" /></strong></td>
					<td class="auto-style1">
			<input class="inputs" onkeyup="BUSCAR('MODALIDAD')" name="modalidad2" maxlength="70" type="text" placeholder="Modalidad" style="width: 150px" /></td>
					<td class="auto-style1">
					<strong>Activo:</strong></td>
					<td class="auto-style1">
					<select name="buscaractivos" class="inputs" onchange="BUSCAR('MODALIDAD')">
					<option value="">Todos</option>
					<option value="1">Activos</option>
					<option value="0">Inactivo</option>
					</select></td>
				</tr>
			</table>
		</div>
		</td>
	</tr>
	<tr>
		<td colspan="6" class="auto-style2">
		<div id="editarmodalidad" class="reveal-modal" style="left: 57%; top: 50px; height: 300px; width: 350px;">
			<table style="width: 38%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Nombre:</td>
			<td class="auto-style3"><input class="inputs" name="nombre1" maxlength="50" value="<?php echo $nom; ?>" type="text" style="width: 200px" />
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Activo:</td>
			<td class="auto-style3"><input class="inputs" <?php if($act == 1){ echo 'checked="checked"'; } ?> name="activo1" type="checkbox" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4">
			<input name="submit" type="button" value="Guardar" onclick="GUARDAR('MODALIDAD','<?php echo $ubiedi; ?>')"  style="width: 348px; height: 25px; cursor:pointer" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">
			<div id="datos">
			</div>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
			<a class="close-reveal-modal">&#215;</a>
		</div>
		<div id="modalidad">
		<table style="width: 100%">
			<tr>
				<td class="auto-style3" style="width: 27px">
					<input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<?php echo $total;?>,this);" />
				</td>
				<td class="auto-style3" colspan="19">
				<?php
				if($metodo == 1)
				{
				?>
				<table style="width: 30%">
					<tr>
						<td class="auto-style1"><p style="cursor:pointer"><img src="../../img/activo.png" alt="" /><input type="submit" value="Activar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" /></p></td>
						<td class="auto-style1"><p style="cursor:pointer"><img src="../../img/inactivo.png" alt="" /><input type="submit" value="Inactivar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" /></p></td>
						<td class="auto-style1"><p style="cursor:pointer"><img src="../../img/eliminar.png" alt="" /><input type="submit" value="Eliminar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" /></p></td>
					</tr>
				</table>
				<?php
				}
				?>
				</td>
			</tr>
			<tr>
				<td class="auto-style3" style="width: 27px">&nbsp;</td>
				<td class="auto-style3" style="width: 200px" ><strong>Modalidad</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Inmob.</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Visita</strong></td>
			<td class="auto-style9" style="width: 200px"><strong>Aprob. Comité</strong></td>
			<td class="auto-style9" style="width: 200px"><strong>Contrato</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Diseño</strong></td>
			<td class="auto-style9" style="width: 200px"><strong>Prefact.</strong></td>
			<td class="auto-style9" style="width: 200px"><strong>Pedido Maquina</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Licencia</strong></td>
			<td class="auto-style9" style="width: 200px"><strong>Canal</strong></td>
			<td class="auto-style9" style="width: 200px"><strong>Proyección</strong></td>
                <td class="auto-style9" style="width: 200px"><strong>Infraestructura</strong></td>
			<td class="auto-style9" style="width: 200px"><strong>Preliminar</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Interv.</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Cons.</strong></td>
			<td class="auto-style8" style="width: 200px"><strong>Segur.</strong></td>
				<td class="auto-style3" style="width: 29px"><strong>Tipo</strong></td>
				<td class="auto-style3" style="width: 29px"><strong>
				Act.</strong></td>
				<td class="auto-style3">&nbsp;</td>
			</tr>
			<?php
				$contador=0;
				$con = mysqli_query($conectar,"select * from modalidad order by mod_nombre");
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$clamod = $dato['mod_clave_int'];
					$mod = $dato['mod_nombre'];
					$act = $dato['mod_sw_activo'];
					
					$inm = $dato['mod_sw_inmobiliaria'];
					$dinm = $dato['mod_dias_inmobiliaria'];
					$vis = $dato['mod_sw_visita'];
					$dvis = $dato['mod_dias_visita'];
					$comite = $dato['mod_sw_aprocomite'];
					$dcom = $dato['mod_dias_aprocomite'];
					$contrato = $dato['mod_sw_contrato'];
					$dcon = $dato['mod_dias_contrato'];
					$dis = $dato['mod_sw_diseno'];
					$ddis = $dato['mod_dias_diseno'];
					
					$prefact = $dato['mod_sw_prefactibilidad'];
					$dprefact = $dato['mod_dias_prefactibilidad'];
					$ped = $dato['mod_sw_pedido_maquina'];
					$dped = $dato['mod_dias_pedido_maquina'];
					
					$lic = $dato['mod_sw_licencia'];
					$dlic = $dato['mod_dias_licencia'];
					
					$can = $dato['mod_sw_canal'];
					$dcan = $dato['mod_dias_canal'];
					$pro = $dato['mod_sw_proyeccion'];
					$dpro = $dato['mod_dias_proyeccion'];
                    $inf = $dato['mod_sw_infraestructura'];
                    $dinf = $dato['mod_dias_infraestructura'];
					
					$preliminar = $dato['mod_sw_preliminar'];
					$dpre = $dato['mod_dias_preliminar'];
					$int = $dato['mod_sw_interventoria'];
					$cons = $dato['mod_sw_constructor'];
					$dcons = $dato['mod_dias_constructor'];
					$seg = $dato['mod_sw_seguridad'];
					$tipmod = $dato['mod_sw_modalidad1'];
					$usuact = $dato['mod_usu_actualiz'];
					$fecact = $dato['mod_fec_actualiz'];
					$contador=$contador+1;
			?>
			<tr>
				<td class="auto-style3" style="width: 27px">
					<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<?php echo $contador;?>" value="<?php echo $dato['mod_clave_int'];?>" /></td>
				<td class="auto-style3" style="width: 200px" ><?php echo $mod; ?></td>
				<td class="auto-style1" style="width: 200px" ><?php if($inm == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dinm; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" /></td>
				<td class="auto-style1" style="width: 200px" ><?php if($vis == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dvis; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" ><?php if($comite == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dcom; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" ><?php if($contrato == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dcon; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" /></td>
				<td class="auto-style1" style="width: 200px" >
				<?php if($dis == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $ddis; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" >
				<?php if($prefact == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dprefact; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" >
				<?php if($ped == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dped; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" ><?php if($lic == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dlic; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" >
				<?php if($can == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dcan; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" >
				<?php if($pro == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dpro; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
                <td class="auto-style1" style="width: 200px" >
                    <?php if($inf == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
                    <input name="Text1" value="<?php echo $dinf; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
                </td>
				<td class="auto-style1" style="width: 200px" ><?php if($preliminar == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dpre; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" /></td>
				<td class="auto-style1" style="width: 200px" ><?php if($int == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?></td>
				<td class="auto-style1" style="width: 200px" ><?php if($cons == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?>
				<input name="Text1" value="<?php echo $dcons; ?>" disabled="disabled" type="text" style="width: 25px" class="inputs" />
				</td>
				<td class="auto-style1" style="width: 200px" ><?php if($seg == 1){ echo '<input name="Checkbox1" disabled="disabled" checked="checked" type="checkbox" />'; }else{ echo '<input name="Checkbox1" disabled="disabled" type="checkbox" />'; } ?></td>
				<td class="auto-style1" style="width: 30px">
				<?php if($tipmod == 1){ echo "M1"; }else{ echo "M2"; } ?>
				</td>
				<td class="auto-style1" style="width: 30px">
				<input name="activarinactivar" id="activarinactivar" type="checkbox" <?php if($act == 1){ echo 'checked="checked"'; } ?> disabled="disabled" ></td>
				<td class="auto-style3">
				<?php
				if($metodo == 1)
				{
				?>
				<a data-reveal-id="editarmodalidad" data-animation="fade" style="cursor:pointer" onclick="EDITAR('<?php echo $dato['mod_clave_int']; ?>','MODALIDAD')"><img src="../../img/editar.png" alt="" height="22" width="21" /></a>
				<?php
				}
				?>
				</td>
				<?php
					}
				?>
			</tr>
			<tr>
				<td class="auto-style3" style="width: 27px">&nbsp;</td>
				<td class="auto-style3" style="width: 200px" >&nbsp;</td>
				<td class="auto-style3" style="width: 200px" >&nbsp;</td>
				<td class="auto-style3" style="width: 200px" >&nbsp;</td>
				<td class="auto-style3" style="width: 200px" >&nbsp;</td>
				<td class="auto-style3" style="width: 200px" >&nbsp;</td>
				<td class="auto-style3" style="width: 200px" >&nbsp;</td>
				<td class="auto-style3" style="width: 200px" >&nbsp;</td>
				<td class="auto-style3" style="width: 200px" >&nbsp;</td>
                <td class="auto-style3" style="width: 200px" >&nbsp;</td>
				<td class="auto-style3" style="width: 200px" >&nbsp;</td>
				<td class="auto-style3" style="width: 200px" >&nbsp;</td>
				<td class="auto-style3" style="width: 200px" >&nbsp;</td>
				<td class="auto-style3" style="width: 200px" >&nbsp;</td>
				<td class="auto-style3" style="width: 200px" >&nbsp;</td>
				<td class="auto-style3" style="width: 200px" >&nbsp;</td>
				<td class="auto-style3" style="width: 200px" >&nbsp;</td>
				<td class="auto-style3" style="width: 29px">&nbsp;</td>
				<td class="auto-style3" style="width: 29px">&nbsp;</td>
				<td class="auto-style3">&nbsp;</td>
			</tr>
		</table>
		</div>
<div id="nuevamodalidad" class="reveal-modal" style="left: 57%; top: 50px; height: 300px; width: 350px;">
	<table style="width: 38%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td>Modalidad:</td>
			<td>
			<input name="modalidad" id="modalidad"  type="text" style="width: 200px" class="inputs" />
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">
			<table style="width: 100%">
				<tr>
					<td colspan="6">
					<table style="width: 100%">
						<tr>
							<td style="width: 10px"><input name="inmobiliaria" id="inmobiliaria" onclick="ACTIVARDIAS()" type="checkbox" /></td>
							<td style="width: 70px" class="auto-style3"><label for="inmobiliaria" style="cursor:pointer">Inmobiliaria</label></td>
							<td style="width: 30px"><input name="diasinmobiliaria" id="diasinmobiliaria" value="0" disabled="disabled" type="text" style="width: 36px" class="inputs" /></td>
							<td style="width: 10px"><input name="visita" id="visita" onclick="ACTIVARDIAS()" type="checkbox" /></td>
							<td style="width: 60px" class="auto-style11"><label for="visita" style="cursor:pointer">Visita</label></td>
							<td><input name="diasvisita" id="diasvisita" value="0" disabled="disabled" type="text" style="width: 36px" class="inputs" /></td>
						</tr>
						<tr>
							<td style="width: 10px"><input name="comite" id="comite" onclick="ACTIVARDIAS()" type="checkbox" /></td>
							<td style="width: 70px" class="auto-style3"><label for="comite" style="cursor:pointer">aprobación comité</label></td>
							<td style="width: 30px"><input name="diascomite" id="diascomite" value="0" disabled="disabled" type="text" style="width: 36px" class="inputs" /></td>
							<td style="width: 10px"><input name="contrato" id="contrato" onclick="ACTIVARDIAS()" type="checkbox" /></td>
							<td style="width: 60px" class="auto-style11"><label for="contrato" style="cursor:pointer">Contrato</label></td>
							<td><input name="diascontrato" id="diascontrato" value="0" disabled="disabled" type="text" style="width: 36px" class="inputs" /></td>
						</tr>
						<tr>
							<td style="width: 10px"><input name="diseno" id="diseno" onclick="ACTIVARDIAS()" type="checkbox" /></td>
							<td style="width: 70px" class="auto-style3"><label for="diseno" style="cursor:pointer">Diseñador</label></td>
							<td style="width: 30px"><input name="diasdiseno" id="diasdiseno" value="0" disabled="disabled" type="text" style="width: 36px" class="inputs" /></td>
							<td style="width: 10px"><input name="prefactibilidad" id="prefactibilidad" onclick="ACTIVARDIAS()" type="checkbox" /></td>
							<td style="width: 60px" class="auto-style11"><label for="prefactibilidad" style="cursor:pointer">Prefactibilidad</label></td>
							<td><input name="diasprefactibilidad" id="diasprefactibilidad" value="0" disabled="disabled" type="text" style="width: 36px" class="inputs" /></td>
						</tr>
						<tr>
							<td style="width: 10px"><input name="pedidomaquina" id="pedidomaquina" onclick="ACTIVARDIAS()" type="checkbox" /></td>
							<td style="width: 70px" class="auto-style11"><label for="pedidomaquina" style="cursor:pointer">Pedido maquina</label></td>
							<td style="width: 30px"><input name="diaspedidomaquina" id="diaspedidomaquina" value="0" disabled="disabled" type="text" style="width: 36px" class="inputs" /></td>
							<td style="width: 10px"><input name="licencia" id="licencia" onclick="ACTIVARDIAS()" type="checkbox" /></td>
							<td style="width: 60px" class="auto-style3"><label for="licencia" style="cursor:pointer">Licencia</label></td>
							<td><input name="diaslicencia" id="diaslicencia" value="0" disabled="disabled" type="text" style="width: 36px" class="inputs" /></td>
						</tr>
						<tr>
							<td style="width: 10px"><input name="canal" id="canal" onclick="ACTIVARDIAS()" type="checkbox" /></td>
							<td style="width: 70px" class="auto-style11"><label for="canal" style="cursor:pointer">Canal</label></td>
							<td style="width: 30px"><input name="diascanal" id="diascanal" value="0" disabled="disabled" type="text" style="width: 36px" class="inputs" /></td>
							<td style="width: 10px"><input name="proyeccion" id="proyeccion" onclick="ACTIVARDIAS()" type="checkbox" /></td>
							<td style="width: 60px" class="auto-style11"><label for="proyeccion" style="cursor:pointer">Proyección</label></td>
							<td><input name="diasproyeccion" id="diasproyeccion" value="0" disabled="disabled" type="text" style="width: 36px" class="inputs" /></td>
						</tr>
						<tr>
							<td style="width: 10px"><input name="preliminar" id="preliminar" onclick="ACTIVARDIAS()" type="checkbox" /></td>
							<td style="width: 70px" class="auto-style11"><label for="preliminar" style="cursor:pointer">Preliminar</label></td>
							<td style="width: 30px"><input name="diaspreliminar" id="diaspreliminar" value="0" disabled="disabled" type="text" style="width: 36px" class="inputs" /></td>
							<td style="width: 10px"><input name="constructor" id="constructor" onclick="ACTIVARDIAS()" type="checkbox" /></td>
							<td style="width: 60px" class="auto-style11"><label for="constructor" style="cursor:pointer">Constructor</label></td>
							<td><input name="diasconstructor" id="diasconstructor" value="0" disabled="disabled" type="text" style="width: 36px" class="inputs" /></td>
						</tr>
                        <tr>
                            <td style="width: 10px"><input name="infraestructura" id="infraestructura" onclick="ACTIVARDIAS()" type="checkbox" /></td>
                            <td style="width: 60px" class="auto-style11"><label for="infraestructura" style="cursor:pointer">Infraestructura</label></td>
                            <td><input name="diasinfraestructura" id="diasinfraestructura" value="0" disabled="disabled" type="text" style="width: 36px" class="inputs" /></td></tr>
					</table>
					</td>
				</tr>
				<tr>
					<td style="width: 10px">
					<input name="interventor" id="interventor" type="checkbox" /></td>
					<td class="auto-style3" style="width: 70px"><label for="interventor" style="cursor:pointer">Interventor</label></td>
					<td style="width: 10px">
					<input name="seguridad" id="seguridad" type="checkbox" /></td>
					<td class="auto-style3"><label for="seguridad" style="cursor:pointer">Seguridad</label></td>
					<td>
					&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Activo: <input class="inputs" name="activo" checked="checked" type="checkbox" /></td>
			<td>
			<table style="width: 37%">
				<tr>
					<td style="width: 80px"><label for="tipomodadlidad1" style="cursor:pointer">Modadlidad1:</label></td>
					<td style="width: 2px">
					<input name="tipomodadlidad" id="tipomodadlidad1" type="radio" value="1"></td>
					<td style="width: 3px"><label for="tipomodadlidad2" style="cursor:pointer">Modalidad2:</label></td>
					<td><input name="tipomodadlidad" id="tipomodadlidad2" type="radio" value="0"></td>
				</tr>
			</table>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4">
			<input name="nuevo1" type="button" value="Guardar" onclick="NUEVO('MODALIDAD')"  style="width: 348px; height: 25px; cursor:pointer" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">
			<div id="datos1">
			</div>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
</div>
		</td>
	</tr>
	<tr>
		<td style="width: 60px">&nbsp;</td>
		<td style="width: 117px">&nbsp;</td>
		<td style="width: 65px">&nbsp;</td>
		<td style="width: 70px">&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td style="width: 60px">&nbsp;</td>
		<td style="width: 117px">&nbsp;</td>
		<td style="width: 65px">&nbsp;</td>
		<td style="width: 70px">&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>
</form>
</body>
</html>