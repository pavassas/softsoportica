<?php
	error_reporting(0);
	include('../../data/Conexion.php');
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_COOKIE['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];
		
	//validacion inactividad en el aplicativo
	if(isset($_SESSION['nw']))
    {
        if($_SESSION['nw']<time())
        {
            unset($_SESSION['nw']);
            //echo "<script>alert('Tiempo agotado - Loguearse nuevamente');</script>";
            echo "<script>alert('Tu sesión se cerrara por inactividad en el sistema - Loguearse nuevamente');</script>";
			echo "<script>parent.location.href=parent.location.href</script>";
 			//header("LOCATION:../../data/logout.php");
        }
        else
        {
            $_SESSION['nw'] = time() + (60 * 60);
        }
    }
    else
    {
       	//echo "<script>alert('Tiempo agotado - Loguearse nuevamente');</script>";
       	echo "<script>alert('Tu sesión se cerrara por inactividad en el sistema - Loguearse nuevamente');</script>";
		echo "<script>parent.location.href=parent.location.href</script>";
 		//header("LOCATION:../../data/logout.php");
    }
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");
	$anocont = date("Y");
	require_once("lib/Zebra_Pagination.php");
	
	$con = mysqli_query($conectar,"select u.usu_clave_int,p.prf_clave_int,p.prf_descripcion,u.usu_sw_inmobiliaria,u.usu_sw_visita,u.usu_sw_diseno,u.usu_sw_licencia,u.usu_sw_interventoria,u.usu_sw_constructor,u.usu_sw_seguridad,u.usu_sw_otro from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
	$dato = mysqli_fetch_array($con);
	$clausu = $dato['usu_clave_int'];
	$claprf = $dato['prf_clave_int'];
	$perfil = $dato['prf_descripcion'];
	
	$swinm = $dato['usu_sw_inmobiliaria'];
	$swvis = $dato['usu_sw_visita'];
	$swdis = $dato['usu_sw_diseno'];
	$swlic = $dato['usu_sw_licencia'];
	$swint = $dato['usu_sw_interventoria'];
	$swcon = $dato['usu_sw_constructor'];
	$swseg = $dato['usu_sw_seguridad'];
	$swotr = $dato['usu_sw_otro'];
	
	$mostrarcajeros = 0;
	if($swinm == 0 and $swvis == 0 and $swdis == 0 and $swlic == 0 and $swint == 0 and $swcon == 0 and $swseg == 0 and $swotr == 1){ $mostrarcajeros = 1; }
	
	$con = mysqli_query($conectar,"select per_metodo from permiso where prf_clave_int = '".$claprf."' and ven_clave_int = 20");
	$dato = mysqli_fetch_array($con);
	$metodo = $dato['per_metodo'];

    function CORREGIRTEXTO($string)
    {
        $string = str_replace("REEMPLAZARNUMERAL", "#", $string);
        $string = str_replace("REEMPLAZARMAS", "+",$string);
        $string = str_replace("REEMPLAZARMENOS", "-", $string);
        return $string;
    }
	
	if($_GET['guardardatos'] == 'si')
	{
		$fecha=date("Y/m/d H:i:s");
		$caj = $_GET['caj'];
		$numotinm = $_GET['numotinm'];
		$vrinm = str_replace(".","",$_GET['vrinm']);
		$swcotizliquiinm = $_GET['swcotizliquiinm'];
		$numotvis = $_GET['numotvis'];
		$vrvis = str_replace(".","",$_GET['vrvis']);
		$swcotizliquivis = $_GET['swcotizliquivis'];
		$numotdis = $_GET['numotdis'];
		$vrdis = str_replace(".","",$_GET['vrdis']);
		$swcotizliquidis = $_GET['swcotizliquidis'];
		$numotlic = $_GET['numotlic'];
		$vrlic = str_replace(".","",$_GET['vrlic']);
		$swcotizliquilic = $_GET['swcotizliquilic'];
		$numotint = $_GET['numotint'];
		$vrint = str_replace(".","",$_GET['vrint']);
		$swcotizliquiint = $_GET['swcotizliquiint'];
		$numotcons = $_GET['numotcons'];
		$vrcons = str_replace(".","",$_GET['vrcons']);
		$swcotizliquicons = $_GET['swcotizliquicons'];
		$numotseg = $_GET['numotseg'];
		$vrseg = str_replace(".","",$_GET['vrseg']);
		$swcotizliquiseg = $_GET['swcotizliquiseg'];
		
		$conpad = mysqli_query($conectar,"select caj_padre,(select c.caj_nombre from cajero c where c.caj_clave_int = cajero.caj_padre) nomcaj from cajero where caj_clave_int = ".$caj."");
		$datopad = mysqli_fetch_array($conpad);
		$mipad = $datopad['caj_padre'];
		$nomcaj = CORREGIRTEXTO($datopad['nomcaj']);
		
		/*if($mipad != 0 and $mipad != '')
		{
			echo "<div class='validaciones'>No puede editar porque Este cajero es hijo del cajero: $mipad - $nomcaj</div>";
		}
		else
		{*/
			$con = mysqli_query($conectar,"update cajero_facturacion set caf_num_ot_inmobiliaria = '".$numotinm."', caf_valor_inmobiliaria = '".$vrinm."', caf_sw_cotiz_liqui_inmobiliaria = '".$swcotizliquiinm."', caf_num_ot_visita = '".$numotvis."', caf_valor_visita = '".$vrvis."', caf_sw_cotiz_liqui_visita = '".$swcotizliquivis."', caf_num_ot_diseno = '".$numotdis."', caf_valor_diseno = '".$vrdis."', caf_sw_cotiz_liqui_diseno = '".$swcotizliquidis."', caf_num_ot_licencia = '".$numotlic."', caf_valor_licencia = '".$vrlic."', caf_sw_cotiz_liqui_licencia = '".$swcotizliquilic."', caf_num_ot_interventoria = '".$numotint."', caf_valor_interventoria = '".$vrint."', caf_sw_cotiz_liqui_interventoria = '".$swcotizliquiint."', caf_num_ot_constructor = '".$numotcons."', caf_valor_constructor = '".$vrcons."', caf_sw_cotiz_liqui_constructor = '".$swcotizliquicons."', caf_num_ot_seguridad = '".$numotseg."', caf_valor_seguridad = '".$vrseg."', caf_sw_cotiz_liqui_seguridad = '".$swcotizliquiseg."' where caj_clave_int = '".$caj."'");
			
			if($con > 0)
			{
				echo "<div class='ok' style='width: 100%' align='center'>Datos grabados correctamente</div>";
				$con = mysqli_query($conectar,"select caj_clave_int from cajero where caj_padre = ".$caj."");
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$clahij = $dato['caj_clave_int'];
					$con1 = mysqli_query($conectar,"update cajero_facturacion set caf_num_ot_inmobiliaria = '".$numotinm."', caf_valor_inmobiliaria = '".$vrinm."', caf_sw_cotiz_liqui_inmobiliaria = '".$swcotizliquiinm."', caf_num_ot_visita = '".$numotvis."', caf_valor_visita = '".$vrvis."', caf_sw_cotiz_liqui_visita = '".$swcotizliquivis."', caf_num_ot_diseno = '".$numotdis."', caf_valor_diseno = '".$vrdis."', caf_sw_cotiz_liqui_diseno = '".$swcotizliquidis."', caf_num_ot_licencia = '".$numotlic."', caf_valor_licencia = '".$vrlic."', caf_sw_cotiz_liqui_licencia = '".$swcotizliquilic."', caf_num_ot_interventoria = '".$numotint."', caf_valor_interventoria = '".$vrint."', caf_sw_cotiz_liqui_interventoria = '".$swcotizliquiint."', caf_num_ot_constructor = '".$numotcons."', caf_valor_constructor = '".$vrcons."', caf_sw_cotiz_liqui_constructor = '".$swcotizliquicons."', caf_num_ot_seguridad = '".$numotseg."', caf_valor_seguridad = '".$vrseg."', caf_sw_cotiz_liqui_seguridad = '".$swcotizliquiseg."' where caj_clave_int = '".$clahij."'");
				}
				
				mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'20',3,'".$caj."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 3=Actualización cajero
			}
			else
			{
				echo "<div class='validaciones' style='width: 100%' align='center'>No se han podido guardar los datos, porfavor intentelo nuevamente</div>";
			}
		//}
        $deleteconsolidar = mysqli_query($conectar,"DELETE FROM cajero_consolidado WHERE caj_clave_int = '".$caj."'");
        $consolidarcajero = mysqli_query($conectar,"INSERT INTO cajero_consolidado (caj_clave_int ,  `caj_nombre` ,  `caj_direccion` ,`caj_ano_contable` ,`caj_region` ,`tpr_clave_int`, `caj_departamento` ,  `caj_municipio` , `tip_clave_int` ,`tii_clave_int` ,`mod_clave_int`,  `caj_codigo_cajero` ,  `cco_clave_int`,  `rem_clave_int` ,
  `ubi_clave_int` ,  `caj_codigo_suc` ,  `caj_ubicacion_atm` ,  `ati_clave_int`,  `caj_riesgo` ,  `are_clave_int` ,  `caj_fecha_inicio` ,  `caj_fecha_creacion` ,  `caj_estado_proyecto`,  `caj_sw_eliminado` ,  `caj_sw_padre`,  `caj_padre` ,  `caj_fec_apagado_atm` ,  `caj_sw_bancolombia`,  `caj_modalidad` ,  `caj_convenio`,  `cac_constructor`,  `cac_fecha_entrega_atm`,  `cac_porcentaje_avance`,  `cac_adjunto_avance`,  `cac_fecha_entrega_cotizacion`, `cac_fecha_entrega_liquidacion`,  `cac_fecha_inicio_constructor`,  `cad_req_diseno` ,  `cad_disenador` ,  `cad_fecha_inicio_diseno` ,  `esd_clave_int` ,  `cad_sw_envioestado` ,
  `cad_sw_envio_entregado`,  `cad_sw_aprob_esquema`,  `cad_sw_envio_esquema`,  `cad_sw_aprob_ppt`,  `cad_sw_envio_ppt` ,  `cad_sw_aprob_entregado` ,  `cad_sw_envio_aprobentregado` ,  `cad_fecha_entrega_info_diseno` , `cai_req_inmobiliaria`,  `cai_inmobiliaria`,  `cai_fecha_ini_inmobiliaria` ,  `esi_clave_int` ,  `cai_fecha_entrega_info_inmobiliaria` ,   `cin_interventor` ,  `cin_fecha_teorica_entrega` ,  `cin_fecha_inicio_obra`,  `cin_fecha_pedido_suministro`,
  `cin_sw_aprov_cotizacion` ,  `can_clave_int` ,  `lit_clave_int` ,  `cin_fecha_entrega_canal` , `cin_fecha_aprov_comite` ,  `cin_sw_aprov_liquidacion` ,  `cin_sw_img_nexos` ,  `cin_sw_fotos` ,  `cin_sw_actas`,  `cin_fecha_inicio_interventoria` ,  `cal_req_licencia` , `cal_gestionador` , `cal_fecha_inicio_licencia` ,  `esl_clave_int` ,  `cal_fecha_entrega_info_licencia`, `cal_curaduria` ,  `cal_radicado` ,  `cas_instalador`,  `cas_fecha_ingreso_obra_inst` ,  `cas_codigo_monitoreo` ,  `cas_fecha_inicio_seguridad` ,  `cav_req_visita_local` ,  `cav_visitante` ,  `cav_fecha_visita`,  `cav_fecha_entrega_informe` ,
  `cav_estado` ,  `caf_num_ot_inmobiliaria`,  `caf_valor_inmobiliaria` ,  `caf_sw_cotiz_liqui_inmobiliaria`,  `caf_num_ot_visita` ,  `caf_valor_visita`,  `caf_sw_cotiz_liqui_visita` ,  `caf_num_ot_diseno`,  `caf_valor_diseno` ,  `caf_sw_cotiz_liqui_diseno` ,  `caf_num_ot_licencia` ,
  `caf_valor_licencia`,  `caf_sw_cotiz_liqui_licencia` ,  `caf_num_ot_interventoria` ,  `caf_valor_interventoria`,  `caf_sw_cotiz_liqui_interventoria` , `caf_num_ot_constructor` ,  `caf_valor_constructor` ,  `caf_sw_cotiz_liqui_constructor` ,  `caf_num_ot_seguridad`,  `caf_valor_seguridad`, `caf_sw_cotiz_liqui_seguridad` ,  `bac_sw_aprobado` ,  `bac_fecha_inicio`,  `bac_fec_ini_teorica` ,  `bac_fec_ent_teorica` ,  `bac_fec_comite`, `bac_sw_contrato`,  `bac_sw_canal` ,  `bac_sw_diseno`,  `bac_sw_envio_contrato`,  `bac_sw_envio_canal` ,  `bac_usu_actualiz` ,  `bac_fec_actualiz` ,  `bco_sw_aprobado` ,  `bes_clave_int` ,  `bco_fec_ini_teorica` ,  `bco_fec_ent_teorica`,  `bco_fec_entrega`,  `bco_usu_actualiz`,  `bco_fec_actualiz`,  `bca_fec_ini_teorica`,  `bca_fec_ent_teorica`,  `bca_fec_inicio` ,  `bca_fec_entrega` ,  `bca_usu_actualiz`,  `bca_fec_actualiz`,   `bma_clave_int`,  `bpm_sw_aprobado` ,  `bpm_usu_actualiz` ,  `bpm_fec_actualiz` ,  `bpr_sw_aprobado`,  `bpr_fec_ini_teorica` ,  `bpr_fec_ent_teorica` ,  `bpr_fecha_entrega` ,
  `bpr_sw_envio_fecha` ,  `bpr_usu_actualiz`,  `bpr_fec_actualiz`,  `bpi_fec_ini_teorica`,  `bpi_fec_ent_teorica`,  `bpi_usu_actualiz`,  `bpi_fec_actualiz`,caj_ot,tit_clave_int) 
SELECT c.caj_clave_int ,  `caj_nombre` ,  `caj_direccion` ,`caj_ano_contable` ,`caj_region` ,`tpr_clave_int`, `caj_departamento` ,  `caj_municipio` , `tip_clave_int` ,`tii_clave_int` ,`mod_clave_int`,  `caj_codigo_cajero` ,  `cco_clave_int`,  `rem_clave_int` ,
  `ubi_clave_int` ,  `caj_codigo_suc` ,  `caj_ubicacion_atm` ,  `ati_clave_int`,  `caj_riesgo` ,  `are_clave_int` ,  `caj_fecha_inicio` ,  `caj_fecha_creacion` ,  `caj_estado_proyecto`,  `caj_sw_eliminado` ,  `caj_sw_padre`,  `caj_padre` ,  `caj_fec_apagado_atm` ,  `caj_sw_bancolombia`,  `caj_modalidad` ,  `caj_convenio`,  `cac_constructor`,  `cac_fecha_entrega_atm`,  `cac_porcentaje_avance`,  `cac_adjunto_avance`,  `cac_fecha_entrega_cotizacion`, `cac_fecha_entrega_liquidacion`,  `cac_fecha_inicio_constructor`,  `cad_req_diseno` ,  `cad_disenador` ,  `cad_fecha_inicio_diseno` ,  `esd_clave_int` ,  `cad_sw_envioestado` ,
  `cad_sw_envio_entregado`,  `cad_sw_aprob_esquema`,  `cad_sw_envio_esquema`,  `cad_sw_aprob_ppt`,  `cad_sw_envio_ppt` ,  `cad_sw_aprob_entregado` ,  `cad_sw_envio_aprobentregado` ,  `cad_fecha_entrega_info_diseno` , `cai_req_inmobiliaria`,  `cai_inmobiliaria`,  `cai_fecha_ini_inmobiliaria` ,  `esi_clave_int` ,  `cai_fecha_entrega_info_inmobiliaria` ,   `cin_interventor` ,  `cin_fecha_teorica_entrega` ,  `cin_fecha_inicio_obra`,  `cin_fecha_pedido_suministro`,
  `cin_sw_aprov_cotizacion` ,  `can_clave_int` ,  `lit_clave_int` ,  `cin_fecha_entrega_canal` , `cin_fecha_aprov_comite` ,  `cin_sw_aprov_liquidacion` ,  `cin_sw_img_nexos` ,  `cin_sw_fotos` ,  `cin_sw_actas`,  `cin_fecha_inicio_interventoria` ,  `cal_req_licencia` , `cal_gestionador` , `cal_fecha_inicio_licencia` ,  `esl_clave_int` ,  `cal_fecha_entrega_info_licencia`, `cal_curaduria` ,  `cal_radicado` ,  `cas_instalador`,  `cas_fecha_ingreso_obra_inst` ,  `cas_codigo_monitoreo` ,  `cas_fecha_inicio_seguridad` ,  `cav_req_visita_local` ,  `cav_visitante` ,  `cav_fecha_visita`,  `cav_fecha_entrega_informe` ,
  `cav_estado` ,  `caf_num_ot_inmobiliaria`,  `caf_valor_inmobiliaria` ,  `caf_sw_cotiz_liqui_inmobiliaria`,  `caf_num_ot_visita` ,  `caf_valor_visita`,  `caf_sw_cotiz_liqui_visita` ,  `caf_num_ot_diseno`,  `caf_valor_diseno` ,  `caf_sw_cotiz_liqui_diseno` ,  `caf_num_ot_licencia` ,
  `caf_valor_licencia`,  `caf_sw_cotiz_liqui_licencia` ,  `caf_num_ot_interventoria` ,  `caf_valor_interventoria`,  `caf_sw_cotiz_liqui_interventoria` , `caf_num_ot_constructor` ,  `caf_valor_constructor` ,  `caf_sw_cotiz_liqui_constructor` ,  `caf_num_ot_seguridad`,  `caf_valor_seguridad`, `caf_sw_cotiz_liqui_seguridad` ,  `bac_sw_aprobado` ,  `bac_fecha_inicio`,  `bac_fec_ini_teorica` ,  `bac_fec_ent_teorica` ,  `bac_fec_comite`, `bac_sw_contrato`,  `bac_sw_canal` ,  `bac_sw_diseno`,  `bac_sw_envio_contrato`,  `bac_sw_envio_canal` ,  `bac_usu_actualiz` ,  `bac_fec_actualiz` ,  `bco_sw_aprobado` ,  `bes_clave_int` ,  `bco_fec_ini_teorica` ,  `bco_fec_ent_teorica`,  `bco_fec_entrega`,  `bco_usu_actualiz`,  `bco_fec_actualiz`,  `bca_fec_ini_teorica`,  `bca_fec_ent_teorica`,  `bca_fec_inicio` ,  `bca_fec_entrega` ,  `bca_usu_actualiz`,  `bca_fec_actualiz`,   `bma_clave_int`,  `bpm_sw_aprobado` ,  `bpm_usu_actualiz` ,  `bpm_fec_actualiz` ,  `bpr_sw_aprobado`,  `bpr_fec_ini_teorica` ,  `bpr_fec_ent_teorica` ,  `bpr_fecha_entrega` ,
  `bpr_sw_envio_fecha` ,  `bpr_usu_actualiz`,  `bpr_fec_actualiz`,  `bpi_fec_ini_teorica`,  `bpi_fec_ent_teorica`,  `bpi_usu_actualiz`,  `bpi_fec_actualiz`,caj_ot,tit_clave_int
FROM cajero c LEFT OUTER JOIN cajero_inmobiliaria cajinm ON (	cajinm.caj_clave_int = c.caj_clave_int)
LEFT OUTER JOIN cajero_visita cajvis ON (cajvis.caj_clave_int = c.caj_clave_int)
LEFT OUTER JOIN cajero_diseno cajdis ON (cajdis.caj_clave_int = c.caj_clave_int)
LEFT OUTER JOIN cajero_licencia cajlic ON (cajlic.caj_clave_int = c.caj_clave_int)
LEFT OUTER JOIN cajero_interventoria cajint ON (cajint.caj_clave_int = c.caj_clave_int)
LEFT OUTER JOIN cajero_constructor cajcons ON (cajcons.caj_clave_int = c.caj_clave_int)
LEFT OUTER JOIN cajero_seguridad cajseg ON (cajseg.caj_clave_int = c.caj_clave_int)
LEFT OUTER JOIN cajero_facturacion cajfac ON (cajfac.caj_clave_int = c.caj_clave_int)
LEFT OUTER JOIN bancolombia_canal bcan ON (bcan.caj_clave_int = c.caj_clave_int)
LEFT OUTER JOIN bancolombia_comite bcom ON (bcom.caj_clave_int = c.caj_clave_int)
LEFT OUTER JOIN bancolombia_contrato bcon ON (bcon.caj_clave_int = c.caj_clave_int)
LEFT OUTER JOIN bancolombia_pedidomaquina bped ON (bped.caj_clave_int = c.caj_clave_int)
LEFT OUTER JOIN bancolombia_prefactibilidad bpre ON (bpre.caj_clave_int = c.caj_clave_int)
LEFT OUTER JOIN bancolombia_proyeccioninstalacion bpro ON (bpro.caj_clave_int = c.caj_clave_int)
WHERE c.caj_clave_int  ='".$caj."' GROUP BY c.caj_clave_int ORDER BY c.caj_nombre ASC");
        if($consolidarcajero>0){

        }else{
            echo "<div class='validaciones'>Se ha producido un error al actualizar el consolidado del cajeros.</div>";
        }
		exit();
	}
	if($_GET['todos'] == 'si')
	{
		$est = $_GET['est'];
?>
	<table>
	<tr>
		<td align="left" class="auto-style2">
		<table style="width: 50%">
				<tr>
					<td style="width: 60px" rowspan="2"><strong>
					<span class="auto-style13">Filtro:</span><img src="../../img/buscar.png" alt="" height="18" width="15" /></strong></td>
					<td style="width: 150px; height: 31px;">
					<input class="inputs" onkeyup="BUSCAR('CAJERO','')" name="busconsecutivo" id="busconsecutivo" maxlength="70" type="text" placeholder="Consecutivo" style="width: 150px" />
					</td>
					<td style="width: 150px; height: 31px;">
					<input class="inputs" onkeyup="BUSCAR('CAJERO','')" name="busnombre" id="busnombre" maxlength="70" type="text" placeholder="Nombre Cajero" style="width: 150px" />
					</td>
					<td style="width: 150px; height: 31px;">
					<input class="inputs" onkeyup="BUSCAR('CAJERO','')" name="busanocontable" id="busanocontable" maxlength="70" type="text" placeholder="Año Contable" style="width: 150px" />
					</td>
					<td style="width: 150px; height: 31px;" class="alinearizq">
					<select name="buscentrocostos" id="buscentrocostos" onchange="BUSCAR('CAJERO','')" tabindex="4" class="inputs" style="width: 158px">
					<option value="">-Centro Costos-</option>
					<?php
						$con = mysqli_query($conectar,"select * from centrocostos where cco_sw_activo = 1 order by cco_nombre");
						$num = mysqli_num_rows($con);
						for($i = 0; $i < $num; $i++)
						{
							$dato = mysqli_fetch_array($con);
							$clave = $dato['cco_clave_int'];
							$nombre = $dato['cco_nombre'];
					?>
						<option value="<?php echo $clave; ?>" <?php if($clave == $cencos){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
					<?php
						}
					?>
					</select>
					</td>
					<td align="left" style="height: 31px">
					<input class="auto-style5" onkeyup="BUSCAR('CAJERO','')" name="buscodigo" id="buscodigo" maxlength="70" type="text" placeholder="Código Cajero" style="width: 150px" />
					</td>
					<td align="left" style="height: 31px">
					&nbsp;</td>
				</tr>
				<tr>
					<td style="width: 150px; height: 31px;">
					<select name="busregion" id="busregion" onchange="BUSCAR('CAJERO','')" tabindex="6" class="inputs" style="width: 148px">
						<option value="">-Región-</option>
						<?php
							$con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo IS NULL and pog_nieto IS NULL order by pog_nombre");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$clave = $dato['pog_clave_int'];
								$nombre = $dato['pog_nombre'];
						?>
							<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
						<?php
							}
						?>
						</select></td>
					<td style="width: 150px; height: 31px;">
						<select name="busmunicipio" id="busmunicipio" onchange="BUSCAR('CAJERO','')" tabindex="8" class="inputs" style="width: 148px">
						<option value="">-Municipio-</option>
						<?php
							$con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo IS NULL and pog_nieto IS NOT NULL order by pog_nombre");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$clave = $dato['pog_clave_int'];
								$nombre = $dato['pog_nombre'];
						?>
							<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
						<?php
							}
						?>
						</select></td>
					<td style="width: 150px; height: 31px;">
						<select name="bustipologia" id="bustipologia" onchange="BUSCAR('CAJERO','')" tabindex="9" class="inputs" style="width: 148px">
						<option value="">-Tipología-</option>
						<?php
							$con = mysqli_query($conectar,"select * from tipologia where tip_sw_activo = 1 order by tip_nombre");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$clave = $dato['tip_clave_int'];
								$nombre = $dato['tip_nombre'];
						?>
							<option value="<?php echo $clave; ?>" <?php if($clave == $tip){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
						<?php
							}
						?>
						</select></td>
					<td style="width: 150px; height: 31px;" class="alinearizq">
					<select name="bustipointervencion" id="bustipointervencion" onchange="BUSCAR('CAJERO','');VERMODALIDADES1(this.value)" tabindex="10" class="inputs" style="width: 158px">
					<option value="">Tipo Intervención</option>
					<?php
						$con = mysqli_query($conectar,"select * from tipointervencion order by tii_nombre");
						$num = mysqli_num_rows($con);
						for($i = 0; $i < $num; $i++)
						{
							$dato = mysqli_fetch_array($con);
							$clave = $dato['tii_clave_int'];
							$nombre = $dato['tii_nombre'];
					?>
						<option value="<?php echo $clave; ?>" <?php if($clave == $tipint){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
					<?php
						}
					?>
					</select></td>
					<td align="left" style="height: 31px">
					<div id="modalidades1" style="float:left">
					<select name="busmodalidad" id="busmodalidad" onchange="BUSCAR('CAJERO','')" tabindex="8" class="inputs" data-placeholder="-Seleccione-" style="width: 160px">
					<option value="">-Modalidad-</option>
					<?php
						$con = mysqli_query($conectar,"select * from modalidad order by mod_nombre");
						$num = mysqli_num_rows($con);
						for($i = 0; $i < $num; $i++)
						{
							$dato = mysqli_fetch_array($con);
							$clave = $dato['mod_clave_int'];
							$nombre = $dato['mod_nombre'];
					?>
						<option value="<?php echo $clave; ?>" <?php if($moda == $clave){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
					<?php
						}
					?>
					</select>
					</div>
					</td>
					<td align="left" style="height: 31px">
					<select name="busactor" id="busactor" onchange="BUSCAR('CAJERO','')" tabindex="20" class="inputs" style="width: 150px">
					<option value="">-Actor-</option>
					<?php
						$con = mysqli_query($conectar,"select * from usuario where usu_sw_inmobiliaria = 1 or usu_sw_visita = 1 or usu_sw_diseno = 1 or usu_sw_licencia = 1 or usu_sw_interventoria = 1 or usu_sw_constructor = 1 or usu_sw_seguridad = 1 order by usu_nombre");
						$num = mysqli_num_rows($con);
						for($i = 0; $i < $num; $i++)
						{
							$dato = mysqli_fetch_array($con);
							$clave = $dato['usu_clave_int'];
							$nombre = $dato['usu_nombre'];
					?>
						<option value="<?php echo $clave; ?>" <?php if($clave == $inmob){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
					<?php
						}
					?>
					</select>
					</td>
				</tr>
				</table>
		</td>
	</tr>
	<tr>
		<td class="auto-style2">
		<div id="busqueda" style="overflow:auto;width: 1226px">
		<table style="width: 100%">
		<tr>
			<td class="alinearizq">&nbsp;</td>
			<td class="alinearizq"><strong>Consecutivo</strong></td>
			<td class="alinearizq"><strong>Código</strong></td>
			<td class="alinearizq"><strong>Nombre</strong></td>
			<td class="alinearizq"><strong>Padre/Hijo</strong></td>
			<td class="alinearizq"><strong>Región</strong></td>
			<td class="alinearizq"><strong>Tipología</strong></td>
			<td class="alinearizq"><strong>Tip. Intervención</strong></td>
			<td class="alinearizq"><strong>Estado</strong></td>
		</tr>
		<?php
		$query = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int)  inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where c.caj_estado_proyecto = '".$est."' and c.caj_sw_eliminado = 0");
		//$res = $con->query($query);
		$num_registros = mysqli_num_rows($query);

		$resul_x_pagina = 50;
		//Paginar:
		$paginacion = new Zebra_Pagination();
		$paginacion->records($num_registros);
		$paginacion->records_per_page($resul_x_pagina);
			
		$con = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int)  inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where c.caj_estado_proyecto = '".$est."' and c.caj_sw_eliminado = 0 LIMIT ".(($paginacion->get_page() - 1) * $resul_x_pagina). ',' .$resul_x_pagina);
		$num = mysqli_num_rows($con);
		for($i = 0; $i < $num; $i++)
		{
			$dato = mysqli_fetch_array($con);
			//Info Base
			$clacaj = $dato['clacaj'];
			$nomcaj = CORREGIRTEXTO($dato['caj_nombre']);
			$dir = $dato['caj_direccion'];
			$anocon = $dato['caj_ano_contable'];
			$reg = $dato['caj_region'];
			$dep = $dato['caj_departamento'];
			$mun = $dato['caj_municipio'];
			$tip = $dato['tip_clave_int'];
			$tipint = $dato['tii_clave_int'];
			//Info Secun
			$codcaj = $dato['caj_codigo_cajero'];
			$cencos = $dato['cco_clave_int'];
			$refmaq = $dato['rem_clave_int'];
			$ubi = $dato['ubi_clave_int'];
			$codsuc = $dato['caj_codigo_suc'];
			$ubiamt = $dato['caj_ubicacion_atm'];
			$ati = $dato['ati_clave_int'];
			$rie = $dato['caj_riesgo'];
			$are = $dato['are_clave_int'];
			$codrec = $dato['caj_cod_recibido_monto'];
			$codrec = $dato['caj_fecha_creacion'];
			//Info Inmobiliaria
			$reqinm = $dato['cai_req_inmobiliaria'];
			$inmob = $dato['cai_inmobiliaria'];
			$feciniinmob = $dato['cai_fecha_ini_inmobiliaria'];
			$estinmob = $dato['esi_clave_int'];
			$fecentinmob = $dato['cai_fecha_entrega_info_inmobiliaria'];
			//Info Visita Local
			$reqvis = $dato['cav_req_visita_local'];
			$vis = $dato['cav_visitante'];
			$fecvis = $dato['cav_fecha_visita'];
			//Info Diseño
			$reqdis = $dato['cad_req_diseno'];
			$dis = $dato['cad_disenador'];
			$fecinidis = $dato['cad_fecha_inicio_diseno'];
			$estdis = $dato['esd_clave_int'];
			$fecentdis = $dato['cad_fecha_entrega_info_diseno'];
			//Info Licencia
			$reqlic = $dato['cal_req_licencia'];
			$lic = $dato['cal_gestionador'];
			$fecinilic = $dato['cal_fecha_inicio_licencia'];
			$estlic = $dato['esl_clave_int'];
			$fecentlic = $dato['cal_fecha_entrega_info_licencia'];
			//Info Interventoria
			$int = $dato['cin_interventor'];
			$fecteoent = $dato['cin_fecha_teorica_entrega'];
			$feciniobra = $dato['cin_fecha_inicio_obra'];
			$fecpedsum = $dato['cin_fecha_pedido_suministro'];
			$aprovcotiz = $dato['cin_sw_aprov_cotizacion'];
			$cancom = $dato['can_clave_int'];
			$aprovliqui = $dato['cin_sw_aprov_liquidacion'];
			$swimgnex = $dato['cin_sw_img_nexos'];
			$swfot = $dato['cin_sw_fotos'];
			$swact = $dato['cin_sw_actas'];
			$estpro = $dato['caj_estado_proyecto'];
			//Info Constructor
			$cons = $dato['cac_constructor'];
			$cons = $dato['cac_fecha_entrega_atm'];
			$cons = $dato['cac_porcentaje_avance'];
			$cons = $dato['cac_fecha_entrega_cotizacion'];
			$cons = $dato['cac_fecha_entrega_liquidacion'];
			//Info Instalador Seguridad
			$seg = $dato['cas_instalador'];
			$fecingseg = $dato['cas_fecha_ingreso_obra_inst'];
			$codmon = $dato['cas_codigo_monitoreo'];
			
			$swpad = $dato['caj_sw_padre'];
			$hijo = $dato['caj_padre'];
		?>
		<tr <?php if($i % 2 == 0){ echo 'style="background-color:#eeeeee;"'; } ?>>
			<td class="alinearizq"><img src="../../img/editar.png" onclick="VERREGISTRO('<?php echo $clacaj; ?>')" alt="" height="25" width="25" style="cursor:pointer" /></td>
			<td class="alinearizq"><?php echo $clacaj; ?></td>
			<td class="alinearizq"><?php echo $codcaj; ?></td>
			<td class="alinearizq"><?php echo $nomcaj; ?></td>
			<td class="alinearizq">
                <?php
                if($swpad == 1 and ($hijo == 0 or $hijo == ''))
                {
                    $conhij = mysqli_query($conectar,"select caj_clave_int,caj_nombre from cajero where caj_padre = ".$clacaj."");
                    $numhij = mysqli_num_rows($conhij);
                    ?>
                    <div style="cursor:pointer" onmouseover="javascript: VentanaFlotante('<?php echo "<strong>HIJOS:</strong><br>"; for($j = 0; $j < $numhij; $j++){ $datohij = mysqli_fetch_array($conhij); echo $datohij['caj_clave_int']." - ".CORREGIRTEXTO($datohij['caj_nombre'])."<br>"; } ?>', 300, <?php echo 40+40*$numhij; ?>)" onmouseout="javascript: quitarDiv();">Padre</div>
                    <?php
                }
                else
                    if($hijo > 0)
                    {
                        $conpad = mysqli_query($conectar,"select caj_clave_int,caj_nombre from cajero where caj_clave_int = ".$hijo."");
                        $datopad = mysqli_fetch_array($conpad);

                        $conher = mysqli_query($conectar,"select caj_clave_int,caj_nombre from cajero where caj_padre = ".$hijo." and caj_clave_int!='".$clacaj."'");
                        $numher = mysqli_num_rows($conher);
                        ?>
                        <div style="cursor:pointer" onmouseover="javascript: VentanaFlotante('<?php echo "<strong>PADRE:</strong><br>".$datopad['caj_clave_int']." - ".$datopad['caj_nombre']."<br>"; if($numher>0){ echo "<strong>HERMANOS:</strong><br>"; for($h = 0; $h < $numher; $h++){ $datoher = mysqli_fetch_array($conher); echo $datoher['caj_clave_int']." - ".CORREGIRTEXTO($datoher['caj_nombre'])."<br>"; } } ?>', 300,  <?php echo 40+40 +(40*$numher); ?>)" onmouseout="javascript: quitarDiv();">Hijo</div>
                        <?php
                    }
                    else
                    {
                        echo "<div>Ninguno</div>";
                    }
                ?>
			</td>
			<td class="alinearizq">
			<?php 
				$sql = mysqli_query($conectar,"select pog_nombre from posicion_geografica where pog_clave_int = '".$reg."'");
				$datosql = mysqli_fetch_array($sql);
				$nomreg = $datosql['pog_nombre'];
				echo $nomreg; 
			?>
			</td>

			<td class="alinearizq">
			<?php 
				$sql = mysqli_query($conectar,"select tip_nombre from tipologia where tip_clave_int = '".$tip."'");
				$datosql = mysqli_fetch_array($sql);
				$nomtip = $datosql['tip_nombre'];
				echo $nomtip;
			?>
			</td>
			<td class="alinearizq">
			<?php 
				$sql = mysqli_query($conectar,"select tii_nombre from tipointervencion where tii_clave_int = '".$tipint."'");
				$datosql = mysqli_fetch_array($sql);
				$nomtii = $datosql['tii_nombre'];
				echo $nomtii;
			?>
			</td>
			<td class="alinearizq"><?php if($estpro == 1){ echo "ACTIVO"; }elseif($estpro == 2){ echo "SUSPENDIDO"; }elseif($estpro == 3){ echo "ENTREGADO"; }elseif($estpro == 4){ echo "CANCELADO"; }elseif($estpro == 5){ echo "PROGRAMADO"; } ?></td>
		</tr>
		<?php
		}
		?>
		</table>
		<?php $paginacion->render(); ?>
		</div>
	</td>
	</tr>
	</table>
<?php
		exit();
	}
	if($_GET['buscar'] == 'si')
	{
		$consec = $_GET['consec'];
		$nom = $_GET['nom'];
		$anocon = $_GET['anocon'];
		$cencos = $_GET['cencos'];
		$cod = $_GET['cod'];
		$reg = $_GET['reg'];
		$mun = $_GET['mun'];
		$tip = $_GET['tip'];
		$tipint = $_GET['tipint'];
		$moda = $_GET['moda'];
		$actor = $_GET['actor'];
		$est = $_GET['est'];
?>
		<table style="width: 100%">
		<tr>
			<td class="alinearizq">&nbsp;</td>
			<td class="alinearizq"><strong>Consecutivo</strong></td>
			<td class="alinearizq"><strong>Código</strong></td>
			<td class="alinearizq"><strong>Nombre</strong></td>
			<td class="alinearizq"><strong>Padre/Hijo</strong></td>
			<td class="alinearizq"><strong>Región</strong></td>
			<td class="alinearizq"><strong>Tipología</strong></td>
			<td class="alinearizq"><strong>Tip. Intervención</strong></td>
			<td class="alinearizq"><strong>Estado</strong></td>
		</tr>
		<?php
		$query = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int) inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where (c.caj_clave_int = '".$consec."' OR '".$consec."' IS NULL OR '".$consec."' = '') and (caj_nombre LIKE REPLACE('%".$nom."%',' ','%') OR '".$nom."' IS NULL OR '".$nom."' = '') and (caj_ano_contable = '".$anocon."' OR '".$anocon."' IS NULL OR '".$anocon."' = '') and (cco_clave_int = '".$cencos."' OR '".$cencos."' IS NULL OR '".$cencos."' = '') and (caj_codigo_cajero LIKE REPLACE('%".$cod."%',' ','%') OR '".$cod."' IS NULL OR '".$cod."' = '') and (caj_direccion LIKE REPLACE('%".$dir."%',' ','%') OR '".$dir."' IS NULL OR '".$dir."' = '') and (caj_region = '".$reg."' OR '".$reg."' IS NULL OR '".$reg."' = '') and (caj_departamento = '".$dep."' OR '".$dep."' IS NULL OR '".$dep."' = '') and (caj_municipio = '".$mun."' OR '".$mun."' IS NULL OR '".$mun."' = '') and (tip_clave_int = '".$tip."' OR '".$tip."' IS NULL OR '".$tip."' = '') and (tii_clave_int = '".$tipint."' OR '".$tipint."' IS NULL OR '".$tipint."' = '') and (mod_clave_int = '".$moda."' OR '".$moda."' IS NULL OR '".$moda."' = '') and (c.caj_estado_proyecto = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') and (cai_inmobiliaria = '".$actor."' or cav_visitante = '".$actor."' or cad_disenador = '".$actor."' or cal_gestionador = '".$actor."' or cin_interventor = '".$actor."' or cac_constructor = '".$actor."' or cas_instalador = '".$actor."' or '".$actor."' IS NULL or '".$actor."' = '') and c.caj_sw_eliminado = 0");
		//$res = $con->query($query);
		$num_registros = mysqli_num_rows($query);

		$resul_x_pagina = 50;
		//Paginar:
		$paginacion = new Zebra_Pagination();
		$paginacion->records($num_registros);
		$paginacion->records_per_page($resul_x_pagina);
			
		$con = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int) inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where (c.caj_clave_int = '".$consec."' OR '".$consec."' IS NULL OR '".$consec."' = '') and (caj_nombre LIKE REPLACE('%".$nom."%',' ','%') OR '".$nom."' IS NULL OR '".$nom."' = '') and (caj_ano_contable = '".$anocon."' OR '".$anocon."' IS NULL OR '".$anocon."' = '') and (cco_clave_int = '".$cencos."' OR '".$cencos."' IS NULL OR '".$cencos."' = '') and (caj_codigo_cajero LIKE REPLACE('%".$cod."%',' ','%') OR '".$cod."' IS NULL OR '".$cod."' = '') and (caj_direccion LIKE REPLACE('%".$dir."%',' ','%') OR '".$dir."' IS NULL OR '".$dir."' = '') and (caj_region = '".$reg."' OR '".$reg."' IS NULL OR '".$reg."' = '') and (caj_departamento = '".$dep."' OR '".$dep."' IS NULL OR '".$dep."' = '') and (caj_municipio = '".$mun."' OR '".$mun."' IS NULL OR '".$mun."' = '') and (tip_clave_int = '".$tip."' OR '".$tip."' IS NULL OR '".$tip."' = '') and (tii_clave_int = '".$tipint."' OR '".$tipint."' IS NULL OR '".$tipint."' = '') and (mod_clave_int = '".$moda."' OR '".$moda."' IS NULL OR '".$moda."' = '') and (c.caj_estado_proyecto = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') and (cai_inmobiliaria = '".$actor."' or cav_visitante = '".$actor."' or cad_disenador = '".$actor."' or cal_gestionador = '".$actor."' or cin_interventor = '".$actor."' or cac_constructor = '".$actor."' or cas_instalador = '".$actor."' or '".$actor."' IS NULL or '".$actor."' = '') and c.caj_sw_eliminado = 0 LIMIT ".(($paginacion->get_page() - 1) * $resul_x_pagina). ',' .$resul_x_pagina);
		$num = mysqli_num_rows($con);
		for($i = 0; $i < $num; $i++)
		{
			$dato = mysqli_fetch_array($con);
			//Info Base
			$clacaj = $dato['clacaj'];
			$nomcaj = CORREGIRTEXTO($dato['caj_nombre']);
			$dir = CORREGIRTEXTO($dato['caj_direccion']);
			$anocon = $dato['caj_ano_contable'];
			$reg = $dato['caj_region'];
			$dep = $dato['caj_departamento'];
			$mun = $dato['caj_municipio'];
			$tip = $dato['tip_clave_int'];
			$tipint = $dato['tii_clave_int'];
			//Info Secun
			$codcaj = $dato['caj_codigo_cajero'];
			$cencos = $dato['cco_clave_int'];
			$refmaq = $dato['rem_clave_int'];
			$ubi = $dato['ubi_clave_int'];
			$codsuc = $dato['caj_codigo_suc'];
			$ubiamt = $dato['caj_ubicacion_atm'];
			$ati = $dato['ati_clave_int'];
			$rie = $dato['caj_riesgo'];
			$are = $dato['are_clave_int'];
			$codrec = $dato['caj_cod_recibido_monto'];
			$codrec = $dato['caj_fecha_creacion'];
			//Info Inmobiliaria
			$reqinm = $dato['cai_req_inmobiliaria'];
			$inmob = $dato['cai_inmobiliaria'];
			$feciniinmob = $dato['cai_fecha_ini_inmobiliaria'];
			$estinmob = $dato['esi_clave_int'];
			$fecentinmob = $dato['cai_fecha_entrega_info_inmobiliaria'];
			//Info Visita Local
			$reqvis = $dato['cav_req_visita_local'];
			$vis = $dato['cav_visitante'];
			$fecvis = $dato['cav_fecha_visita'];
			//Info Diseño
			$reqdis = $dato['cad_req_diseno'];
			$dis = $dato['cad_disenador'];
			$fecinidis = $dato['cad_fecha_inicio_diseno'];
			$estdis = $dato['esd_clave_int'];
			$fecentdis = $dato['cad_fecha_entrega_info_diseno'];
			//Info Licencia
			$reqlic = $dato['cal_req_licencia'];
			$lic = $dato['cal_gestionador'];
			$fecinilic = $dato['cal_fecha_inicio_licencia'];
			$estlic = $dato['esl_clave_int'];
			$fecentlic = $dato['cal_fecha_entrega_info_licencia'];
			//Info Interventoria
			$int = $dato['cin_interventor'];
			$fecteoent = $dato['cin_fecha_teorica_entrega'];
			$feciniobra = $dato['cin_fecha_inicio_obra'];
			$fecpedsum = $dato['cin_fecha_pedido_suministro'];
			$aprovcotiz = $dato['cin_sw_aprov_cotizacion'];
			$cancom = $dato['can_clave_int'];
			$aprovliqui = $dato['cin_sw_aprov_liquidacion'];
			$swimgnex = $dato['cin_sw_img_nexos'];
			$swfot = $dato['cin_sw_fotos'];
			$swact = $dato['cin_sw_actas'];
			$estpro = $dato['caj_estado_proyecto'];
			//Info Constructor
			$cons = $dato['cac_constructor'];
			$cons = $dato['cac_fecha_entrega_atm'];
			$cons = $dato['cac_porcentaje_avance'];
			$cons = $dato['cac_fecha_entrega_cotizacion'];
			$cons = $dato['cac_fecha_entrega_liquidacion'];
			//Info Instalador Seguridad
			$seg = $dato['cas_instalador'];
			$fecingseg = $dato['cas_fecha_ingreso_obra_inst'];
			$codmon = $dato['cas_codigo_monitoreo'];
			
			$swpad = $dato['caj_sw_padre'];
			$hijo = $dato['caj_padre'];
		?>
		<tr <?php if($i % 2 == 0){ echo 'style="background-color:#eeeeee;"'; } ?>>
			<td class="alinearizq"><img src="../../img/editar.png" onclick="VERREGISTRO('<?php echo $clacaj; ?>')" alt="" height="25" width="25" style="cursor:pointer" /></td>
			<td class="alinearizq"><?php echo $clacaj; ?></td>
			<td class="alinearizq"><?php echo $codcaj; ?></td>
			<td class="alinearizq"><?php echo $nomcaj; ?></td>
			<td class="alinearizq">
                <?php
                if($swpad == 1 and ($hijo == 0 or $hijo == ''))
                {
                    $conhij = mysqli_query($conectar,"select caj_clave_int,caj_nombre from cajero where caj_padre = ".$clacaj."");
                    $numhij = mysqli_num_rows($conhij);
                    ?>
                    <div style="cursor:pointer" onmouseover="javascript: VentanaFlotante('<?php echo "<strong>HIJOS:</strong><br>"; for($j = 0; $j < $numhij; $j++){ $datohij = mysqli_fetch_array($conhij); echo $datohij['caj_clave_int']." - ".CORREGIRTEXTO($datohij['caj_nombre'])."<br>"; } ?>', 300, <?php echo 40+40*$numhij; ?>)" onmouseout="javascript: quitarDiv();">Padre</div>
                    <?php
                }
                else
                    if($hijo > 0)
                    {
                        $conpad = mysqli_query($conectar,"select caj_clave_int,caj_nombre from cajero where caj_clave_int = ".$hijo."");
                        $datopad = mysqli_fetch_array($conpad);

                        $conher = mysqli_query($conectar,"select caj_clave_int,caj_nombre from cajero where caj_padre = ".$hijo." and caj_clave_int!='".$clacaj."'");
                        $numher = mysqli_num_rows($conher);
                        ?>
                        <div style="cursor:pointer" onmouseover="javascript: VentanaFlotante('<?php echo "<strong>PADRE:</strong><br>".$datopad['caj_clave_int']." - ".$datopad['caj_nombre']."<br>"; if($numher>0){ echo "<strong>HERMANOS:</strong><br>"; for($h = 0; $h < $numher; $h++){ $datoher = mysqli_fetch_array($conher); echo $datoher['caj_clave_int']." - ".CORREGIRTEXTO($datoher['caj_nombre'])."<br>"; } } ?>', 300,  <?php echo 40+40 +(40*$numher); ?>)" onmouseout="javascript: quitarDiv();">Hijo</div>
                        <?php
                    }
                    else
                    {
                        echo "<div>Ninguno</div>";
                    }
                ?>
			</td>
			<td class="alinearizq">
			<?php 
				$sql = mysqli_query($conectar,"select pog_nombre from posicion_geografica where pog_clave_int = '".$reg."'");
				$datosql = mysqli_fetch_array($sql);
				$nomreg = $datosql['pog_nombre'];
				echo $nomreg; 
			?>
			</td>

			<td class="alinearizq">
			<?php 
				$sql = mysqli_query($conectar,"select tip_nombre from tipologia where tip_clave_int = '".$tip."'");
				$datosql = mysqli_fetch_array($sql);
				$nomtip = $datosql['tip_nombre'];
				echo $nomtip;
			?>
			</td>
			<td class="alinearizq">
			<?php 
				$sql = mysqli_query($conectar,"select tii_nombre from tipointervencion where tii_clave_int = '".$tipint."'");
				$datosql = mysqli_fetch_array($sql);
				$nomtii = $datosql['tii_nombre'];
				echo $nomtii;
			?>
			</td>
			<td class="alinearizq">
			<?php if($estpro == 1){ echo "ACTIVO"; }elseif($estpro == 2){ echo "SUSPENDIDO"; }elseif($estpro == 3){ echo "ENTREGADO"; }elseif($estpro == 4){ echo "CANCELADO"; }elseif($estpro == 5){ echo "PROGRAMADO"; } ?>
			</td>
		</tr>
		<?php
		}
		?>
		</table>
<?php
		$paginacion->render();
		exit();
	}
	if($_GET['verdepartamentos'] == 'si')
	{
		$reg = $_GET['reg'];
?>
		<select name="departamento" id="departamento" onchange="VERCIUDADES(this.value)" tabindex="7" class="chosen-select" data-placeholder="-Seleccione-" style="width:148px">
		<option value="">-Seleccione-</option>
		<?php
			$con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo IS NOT NULL and pog_nieto IS NULL and pog_hijo = '".$reg."' order by pog_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clave = $dato['pog_clave_int'];
				$nombre = $dato['pog_nombre'];
		?>
			<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
		<?php
			}
		?>
		</select>
<?php
		exit();
	}
	if($_GET['verciudades'] == 'si')
	{
		$dep = $_GET['dep'];
?>
		<select name="ciudad" id="ciudad" tabindex="8" class="chosen-select" data-placeholder="-Seleccione-" style="width:148px">
		<option value="">-Seleccione-</option>
		<?php
			$con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo IS NULL and pog_nieto IS NOT NULL and pog_nieto = '".$dep."' order by pog_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clave = $dato['pog_clave_int'];
				$nombre = $dato['pog_nombre'];
		?>
			<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
		<?php
			}
		?>
		</select>
<?php
		exit();
	}
	if($_GET['verregistro'] == 'si')
	{
		$clacaj = $_GET['clacaj'];
		$con = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where c.caj_clave_int = '".$clacaj."'");
		$dato = mysqli_fetch_array($con);
		//Info Base
		$clacaj = $dato['clacaj'];
		$nomcaj = CORREGIRTEXTO($dato['caj_nombre']);
		$dir = CORREGIRTEXTO($dato['caj_direccion']);
		$anocon = $dato['caj_ano_contable'];
		$reg = $dato['caj_region'];
		$dep = $dato['caj_departamento'];
		$mun = $dato['caj_municipio'];
		$tip = $dato['tip_clave_int'];
		$tipint = $dato['tii_clave_int'];
		//Info Secun
		$codcaj = $dato['caj_codigo_cajero'];
		//Info Facturación
		$numotinm = $dato['caf_num_ot_inmobiliaria'];
		$vrinm = $dato['caf_valor_inmobiliaria'];
		$swcotizliquiinm = $dato['caf_sw_cotiz_liqui_inmobiliaria'];
		$numotvis = $dato['caf_num_ot_visita'];
		$vrvis = $dato['caf_valor_visita'];
		$swcotizliquivis = $dato['caf_sw_cotiz_liqui_visita'];
		$numotdis = $dato['caf_num_ot_diseno'];
		$vrdis = $dato['caf_valor_diseno'];
		$swcotizliquidis = $dato['caf_sw_cotiz_liqui_diseno'];
		$numotlic = $dato['caf_num_ot_licencia'];
		$vrlic = $dato['caf_valor_licencia'];
		$swcotizliquilic = $dato['caf_sw_cotiz_liqui_licencia'];
		$numotint = $dato['caf_num_ot_interventoria'];
		$vrint = $dato['caf_valor_interventoria'];
		$swcotizliquiint = $dato['caf_sw_cotiz_liqui_interventoria'];
		$numotcons = $dato['caf_num_ot_constructor'];
		$vrcons = $dato['caf_valor_constructor'];
		$swcotizliquicons = $dato['caf_sw_cotiz_liqui_constructor'];
		$numotseg = $dato['caf_num_ot_seguridad'];
		$vrseg = $dato['caf_valor_seguridad'];
		$swcotizliquiseg = $dato['caf_sw_cotiz_liqui_seguridad'];
		
		$conpad = mysqli_query($conectar,"select caj_padre,(select c.caj_nombre from cajero c where c.caj_clave_int = cajero.caj_padre) nomcaj from cajero where caj_clave_int = ".$clacaj."");
		$datopad = mysqli_fetch_array($conpad);
		$mipad = $datopad['caj_padre'];
		$nomcajpad = CORREGIRTEXTO($datopad['nomcaj']);
?>
		<fieldset name="Group1" class="auto-style3">
		<legend class="auto-style3"><strong>INFORMACIÓN FACTURACIÓN</strong></legend>
		<input name="clavecajero" id="clavecajero" value="<?php echo $clacaj; ?>" type="hidden" />
		<table style="width: 105%" align="center">
			<tr>
				<td class="alinearizq" style="width: 155px" rowspan="9">
				<fieldset name="Group1">
		<legend><strong>INFORMACIÓN BÁSICA CAJERO</strong></legend>
		
	
					<table style="width: 80%; height: 250px;" align="center">
			<tr>
				<td class="alinearizq" style="width: 141px">&nbsp;</td>
				<td class="alinearizq" style="width: 155px">
				&nbsp;</td>
			</tr>
			<tr>
				<td class="alinearizq" style="width: 141px">Nombre:</td>
				<td class="alinearizq" style="width: 155px">
				<input name="nombrecajero" id="nombrecajero" value="<?php echo $nomcaj; ?>" disabled="disabled" tabindex="1" class="inputs" type="text" style="width: 140px"></td>
			</tr>
			<tr>
				<td class="alinearizq" style="width: 141px">Año Contable:</td>
				<td class="alinearizq" style="width: 155px">
				<input name="codigocajero" id="codigocajero" value="<?php echo $anocon; ?>" disabled="disabled" tabindex="2" class="inputs" type="text" style="width: 140px"></td>
			</tr>
			<tr>
				<td class="alinearizq" style="width: 141px">Región:</td>
				<td class="alinearizq" style="width: 155px">
				<select name="region1" id="region" disabled="disabled" tabindex="6" data-placeholder="-Seleccione-" class="inputs" style="width: 140px">
				<option value="">-Seleccione-</option>
				<?php
					$con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo IS NULL and pog_nieto IS NULL order by pog_nombre");
					$num = mysqli_num_rows($con);
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$clave = $dato['pog_clave_int'];
						$nombre = $dato['pog_nombre'];
				?>
					<option value="<?php echo $clave; ?>" <?php if($clave == $reg){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
				<?php
					}
				?>
				</select></td>
			</tr>
			<tr>
						<td>
				Departamento:</td>
						<td>
						<select name="departamento0" id="departamento0" disabled="disabled" tabindex="7" class="inputs" style="width: 140px">
						<option value="">-Seleccione-</option>
						<?php
							$con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo IS NOT NULL and pog_nieto IS NULL order by pog_nombre");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$clave = $dato['pog_clave_int'];
								$nombre = $dato['pog_nombre'];
						?>
							<option value="<?php echo $clave; ?>" <?php if($clave == $dep){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
						<?php
							}
						?>
						</select></td>
			</tr>
			<tr>
				<td class="alinearizq" style="width: 141px">Municipio:</td>
				<td class="alinearizq" style="width: 155px">
				<select name="ciudad0" id="ciudad0" disabled="disabled" tabindex="8" class="inputs" data-placeholder="-Seleccione-" style="width: 140px">
				<option value="">-Seleccione-</option>
				<?php
					$con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo IS NULL and pog_nieto IS NOT NULL order by pog_nombre");
					$num = mysqli_num_rows($con);
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$clave = $dato['pog_clave_int'];
						$nombre = $dato['pog_nombre'];
				?>
					<option value="<?php echo $clave; ?>" <?php if($clave == $mun){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
				<?php
					}
				?>
				</select></td>
			</tr>
			<tr>
				<td class="alinearizq" style="width: 141px">Tipología:</td>
				<td class="alinearizq" style="width: 155px">
				
				<select name="tipologia" id="tipologia" disabled="disabled" tabindex="9" data-placeholder="-Seleccione-" class="inputs" style="width: 140px">
				<option value="">-Seleccione-</option>
				<?php
					$con = mysqli_query($conectar,"select * from tipologia where tip_sw_activo = 1 order by tip_nombre");
					$num = mysqli_num_rows($con);
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$clave = $dato['tip_clave_int'];
						$nombre = $dato['tip_nombre'];
				?>
					<option value="<?php echo $clave; ?>" <?php if($clave == $tip){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
				<?php
					}
				?>
				</select></td>
			</tr>
			<tr>
				<td class="alinearizq" style="width: 141px">Tipo 
				Intervención:</td>
				<td class="alinearizq" style="width: 155px">
				<select name="tipointervencion1" id="tipointervencion0" disabled="disabled" tabindex="17" data-placeholder="-Seleccione-" class="inputs" style="width: 140px">
				<option value="">-Seleccione-</option>
				<?php
					$con = mysqli_query($conectar,"select * from tipointervencion order by tii_nombre");
					$num = mysqli_num_rows($con);
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$clave = $dato['tii_clave_int'];
						$nombre = $dato['tii_nombre'];
				?>
					<option value="<?php echo $clave; ?>" <?php if($clave == $tipint){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
				<?php
					}
				?>
				</select></td>
			</tr>
			<tr>
				<td class="alinearizq" style="width: 141px">Código:</td>
				<td class="alinearizq" style="width: 155px">
				<input name="codigocajero" id="codigocajero" disabled="disabled" value="<?php echo $codcaj; ?>" tabindex="2" class="inputs" type="text" style="width: 140px"></td>
			</tr>
			</table>
			</fieldset>
				</td>
				<td class="alinearizq" style="width: 19px">
				&nbsp;</td>
				<td class="auto-style3" style="width: 97px">
				<strong>NÚMERO OT</strong></td>
				<td class="auto-style3" style="width: 79px">
				<strong>VALOR</strong></td>
				<td class="auto-style3" style="width: 50px">
				<strong>COTIZ.</strong></td>
				<td class="auto-style3" style="width: 50px">
				<strong>LIQUI.</strong></td>
				<td class="alinearizq" style="width: 81px">
				&nbsp;</td>
				<td class="alinearizq" style="width: 155px">
				&nbsp;</td>
				<td class="alinearizq" style="width: 155px">
				&nbsp;</td>
			</tr>
			<tr>
				<td class="alinearizq" style="width: 19px">
				$Inmobiliaria:</td>
				<td class="auto-style3" style="width: 97px">
				<input name="numotinm" id="numotinm" value="<?php echo $numotinm; ?>" class="inputs" type="text" style="width: 80px"></td>
				<td class="auto-style3" style="width: 79px">
				<input name="vrinm" id="vrinm" onkeyup="format(this);SUMATOTAL()" onchange="format(this)" value="<?php echo number_format($vrinm,0 , "," ,"."); ?>" class="inputs" type="text" style="width: 100px"></td>
				<td class="auto-style3" style="width: 50px">
				<input name="swcotizliquiinm" id="swcotizliquiinm" <?php if($swcotizliquiinm == 1){ echo 'checked="checked"'; } ?> value="1" type="radio"></td>
				<td class="auto-style3" style="width: 50px">
				<input name="swcotizliquiinm" id="swcotizliquiinm" <?php if($swcotizliquiinm == 2){ echo 'checked="checked"'; } ?> value="2" type="radio"></td>
				<td class="alinearizq" style="width: 81px">
				<?php
					$con = mysqli_query($conectar,"select * from adjunto_actor where caj_clave_int = '".$clacaj."' and tad_clave_int = 6 and ada_sw_eliminado = 0");
					$numadj = mysqli_fetch_array($con);
					
					if($numadj != '')
					{
				?>
				<div class='ok' style='width: 105px' align='center'>
				<img src='../../images/veradjuntos.png' onclick="VERADJUNTOS('top');MOSTRARADJUNTOS('<?php echo $clacaj; ?>','6')" style='width:100px; height:30px; cursor: pointer' />
				</div>
				<?php
					}
					else
					{
				?>
						<div class='validaciones' style='width: 50%' align='center'>Sin Adjunto</div>
				<?php
					}
				?>
				</td>
				<td class="alinearizq" style="width: 155px">
				<?php
				if($metodo == 1 and ($mipad == 0 or $mipad == ''))
				{
				?>
                    <progress id="barra_de_progresoinm" value="0" max="100"></progress><br>
				<div class="file-wrapper">
				<input name="adjuntoinm" id="adjuntoinm" type="file" onchange="MOSTRARRUTA('INMOBILIARIA')" class="file" style="cursor:pointer; right: 0; top: 0;" />
				<span class="button">Adjuntar Ot.</span>
				</div>
				<input type="submit" onclick="ADJUNTAR('INMOBILIARIA','barra_de_progresoinm','adjuntoinm')" value="Subir" class="btnSubmit" />
				<?php
				}
				?>
				</td>
				<td class="alinearizq" style="width: 155px">
				<div id="resultadoadjuntoinm" style="width:35%">
				<?php
					$con = mysqli_query($conectar,"select * from adjunto_actor where caj_clave_int = '".$clacaj."' and tad_clave_int = 21 and ada_sw_eliminado = 0");
					$numadj = mysqli_fetch_array($con);
					
					if($numadj != '')
					{
				?>
				<div class='ok' style='width: 105px' align='center'>
				<img src='../../images/veradjuntos.png' onclick="VERADJUNTOS('top');MOSTRARADJUNTOS('<?php echo $clacaj; ?>','21')" style='width:100px; height:30px; cursor: pointer' />
				</div>
				<?php
					}
					else
					{
				?>
						<div class='validaciones' style='width: 50%' align='center'>Sin Adjunto</div>
				<?php
					}
				?>
				</div>
				</td>
			</tr>
			<tr>
				<td class="alinearizq" style="width: 19px">
				$Visita Local:</td>
				<td class="auto-style3" style="width: 97px">
				<input name="numotvis" id="numotvis" value="<?php echo $numotvis; ?>" class="inputs" type="text" style="width: 80px"></td>
				<td class="auto-style3" style="width: 79px">
				<input name="vrvis" id="vrvis" onkeyup="format(this);SUMATOTAL()" onchange="format(this)" value="<?php echo number_format($vrvis,0 , "," ,"."); ?>" class="inputs" type="text" style="width: 100px"></td>
				<td class="auto-style3" style="width: 50px">
				<input name="swcotizliquivis" id="swcotizliquivis" <?php if($swcotizliquivis == 1){ echo 'checked="checked"'; } ?> value="1" type="radio"></td>
				<td class="auto-style3" style="width: 50px">
				<input name="swcotizliquivis" id="swcotizliquiinm" <?php if($swcotizliquivis == 2){ echo 'checked="checked"'; } ?> value="2" type="radio"></td>
				<td class="alinearizq" style="width: 81px">
				<?php
					$con = mysqli_query($conectar,"select * from adjunto_actor where caj_clave_int = '".$clacaj."' and tad_clave_int = 8 and ada_sw_eliminado = 0");
					$numadj = mysqli_fetch_array($con);
					
					if($numadj != '')
					{
				?>
				<div class='ok' style='width: 105px' align='center'>
				<img src='../../images/veradjuntos.png' onclick="VERADJUNTOS('top');MOSTRARADJUNTOS('<?php echo $clacaj; ?>','8')" style='width:100px; height:30px; cursor: pointer' />
				</div>
				<?php
					}
					else
					{
				?>
						<div class='validaciones' style='width: 50%' align='center'>Sin Adjunto</div>
				<?php
					}
				?>
				</td>
				<td class="alinearizq" style="width: 155px">
				<?php
				if($metodo == 1 and ($mipad == 0 or $mipad == ''))
				{
				?>
                    <progress id="barra_de_progresovis" value="0" max="100"></progress><br>
				<div class="file-wrapper">
				<input name="adjuntovis" id="adjuntovis" type="file" onchange="MOSTRARRUTA('VISITA')" class="file" style="cursor:pointer" />
				<span class="button">Adjuntar Ot.</span>
				</div>
				<input type="submit" onclick="ADJUNTAR('VISITA','barra_de_progresovis','adjuntovis')" value="Subir" class="btnSubmit" />
				<?php
				}
				?>
				</td>
				<td class="alinearizq" style="width: 155px">
				<div id="resultadoadjuntovis" style="width:35%">
				<?php
					$con = mysqli_query($conectar,"select * from adjunto_actor where caj_clave_int = '".$clacaj."' and tad_clave_int = 22 and ada_sw_eliminado = 0");
					$numadj = mysqli_fetch_array($con);
					
					if($numadj != '')
					{
				?>
				<div class='ok' style='width: 105px' align='center'>
				<img src='../../images/veradjuntos.png' onclick="VERADJUNTOS('top');MOSTRARADJUNTOS('<?php echo $clacaj; ?>','22')" style='width:100px; height:30px; cursor: pointer' />
				</div>
				<?php
					}
					else
					{
				?>
						<div class='validaciones' style='width: 50%' align='center'>Sin Adjunto</div>
				<?php
					}
				?>
				</div>
				</td>
			</tr>
			<tr>
				<td class="alinearizq" style="width: 19px">
				$Diseño:</td>
				<td class="auto-style3" style="width: 97px">
				<input name="numotdis" id="numotdis" value="<?php echo $numotdis; ?>" class="inputs" type="text" style="width: 80px"></td>
				<td class="auto-style3" style="width: 79px">
				<input name="vrdis" id="vrdis" onkeyup="format(this);SUMATOTAL()" onchange="format(this)" value="<?php echo number_format($vrdis,0 , "," ,"."); ?>" class="inputs" type="text" style="width: 100px"></td>
				<td class="auto-style3" style="width: 50px">
				<input name="swcotizliquidis" id="swcotizliquidis" <?php if($swcotizliquidis == 1){ echo 'checked="checked"'; } ?> value="1" type="radio"></td>
				<td class="auto-style3" style="width: 50px">
				<input name="swcotizliquidis" id="swcotizliquidis" <?php if($swcotizliquidis == 2){ echo 'checked="checked"'; } ?> value="2" type="radio"></td>
				<td class="alinearizq" style="width: 81px">
				<?php
					$con = mysqli_query($conectar,"select * from adjunto_actor where caj_clave_int = '".$clacaj."' and tad_clave_int = 10 and ada_sw_eliminado = 0");
					$numadj = mysqli_fetch_array($con);
					
					if($numadj != '')
					{
				?>
				<div class='ok' style='width: 105px' align='center'>
				<img src='../../images/veradjuntos.png' onclick="VERADJUNTOS('top');MOSTRARADJUNTOS('<?php echo $clacaj; ?>','10')" style='width:100px; height:30px; cursor: pointer' />
				</div>
				<?php
					}
					else
					{
				?>
						<div class='validaciones' style='width: 50%' align='center'>Sin Adjunto</div>
				<?php
					}
				?>
				</td>
				<td class="alinearizq" style="width: 155px">
				<?php
				if($metodo == 1 and ($mipad == 0 or $mipad == ''))
				{
				?>
                    <progress id="barra_de_progresodis" value="0" max="100"></progress><br>
				<div class="file-wrapper">
				<input name="adjuntodis" id="adjuntodis" type="file" onchange="MOSTRARRUTA('DISENO')" class="file" style="cursor:pointer" />
				<span class="button">Adjuntar Ot.</span>
				</div>
				<input type="submit" onclick="ADJUNTAR('DISENO','barra_de_progresodis','adjuntodis')" value="Subir" class="btnSubmit" />
				<?php
				}
				?>
				</td>
				<td class="alinearizq" style="width: 155px">
				<div id="resultadoadjuntodis" style="width:35%">
				<?php
					$con = mysqli_query($conectar,"select * from adjunto_actor where caj_clave_int = '".$clacaj."' and tad_clave_int = 23 and ada_sw_eliminado = 0");
					$numadj = mysqli_fetch_array($con);
					
					if($numadj != '')
					{
				?>
				<div class='ok' style='width: 105px' align='center'>
				<img src='../../images/veradjuntos.png' onclick="VERADJUNTOS('top');MOSTRARADJUNTOS('<?php echo $clacaj; ?>','23')" style='width:100px; height:30px; cursor: pointer' />
				</div>
				<?php
					}
					else
					{
				?>
						<div class='validaciones' style='width: 50%' align='center'>Sin Adjunto</div>
				<?php
					}
				?>
				</div>
				</td>
			</tr>
			<tr>
				<td class="alinearizq" style="width: 19px">
				$Licencia:</td>
				<td class="auto-style3" style="width: 97px">
				<input name="numitlic" id="numotlic" value="<?php echo $numotlic; ?>" class="inputs" type="text" style="width: 80px"></td>
				<td class="auto-style3" style="width: 79px">
				<input name="vrlic" id="vrlic" onkeyup="format(this);SUMATOTAL()" onchange="format(this)" value="<?php echo number_format($vrlic,0 , "," ,"."); ?>" class="inputs" type="text" style="width: 100px"></td>
				<td class="auto-style3" style="width: 50px">
				<input name="swcotizliquilic" id="swcotizliquilic" <?php if($swcotizliquilic == 1){ echo 'checked="checked"'; } ?> value="1" type="radio"></td>
				<td class="auto-style3" style="width: 50px">
				<input name="swcotizliquilic" id="swcotizliquilic" <?php if($swcotizliquilic == 2){ echo 'checked="checked"'; } ?> value="2" type="radio"></td>
				<td class="alinearizq" style="width: 81px">
				<?php
					$con = mysqli_query($conectar,"select * from adjunto_actor where caj_clave_int = '".$clacaj."' and tad_clave_int = 12 and ada_sw_eliminado = 0");
					$numadj = mysqli_fetch_array($con);
					
					if($numadj != '')
					{
				?>
				<div class='ok' style='width: 105px' align='center'>
				<img src='../../images/veradjuntos.png' onclick="VERADJUNTOS('top');MOSTRARADJUNTOS('<?php echo $clacaj; ?>','12')" style='width:100px; height:30px; cursor: pointer' />
				</div>
				<?php
					}
					else
					{
				?>
						<div class='validaciones' style='width: 50%' align='center'>Sin Adjunto</div>
				<?php
					}
				?>
				</td>
				<td class="alinearizq" style="width: 155px">
				<?php
				if($metodo == 1 and ($mipad == 0 or $mipad == ''))
				{
				?><progress id="barra_de_progresolic" value="0" max="100"></progress><br>
				<div class="file-wrapper">
				<input name="adjuntolic" id="adjuntolic" type="file" onchange="MOSTRARRUTA('LICENCIA')" class="file" style="cursor:pointer; right: 0; top: 0;" />
				<span class="button">Adjuntar Ot.</span>
				</div>
				<input type="submit" onclick="ADJUNTAR('LICENCIA','barra_de_progresolic','adjuntolic')" value="Subir" class="btnSubmit" />
				<?php
				}
				?>
				</td>
				<td class="alinearizq" style="width: 155px">
				<div id="resultadoadjuntolic" style="width:35%">
				<?php
					$con = mysqli_query($conectar,"select * from adjunto_actor where caj_clave_int = '".$clacaj."' and tad_clave_int = 24 and ada_sw_eliminado = 0");
					$numadj = mysqli_fetch_array($con);
					
					if($numadj != '')
					{
				?>
				<div class='ok' style='width: 105px' align='center'>
				<img src='../../images/veradjuntos.png' onclick="VERADJUNTOS('top');MOSTRARADJUNTOS('<?php echo $clacaj; ?>','24')" style='width:100px; height:30px; cursor: pointer' />
				</div>
				<?php
					}
					else
					{
				?>
						<div class='validaciones' style='width: 50%' align='center'>Sin Adjunto</div>
				<?php
					}
				?>
				</div>
				</td>
			</tr>
			<tr>
				<td class="alinearizq" style="width: 19px">
				$Interventoria:</td>
				<td class="auto-style3" style="width: 97px">
				<input name="numotint" id="numotint" value="<?php echo $numotint; ?>" class="inputs" type="text" style="width: 80px"></td>
				<td class="auto-style3" style="width: 79px">
				<input name="vrint" id="vrint" onkeyup="format(this);SUMATOTAL()" onchange="format(this)" value="<?php echo number_format($vrint,0 , "," ,"."); ?>" class="inputs" type="text" style="width: 100px"></td>
				<td class="auto-style3" style="width: 50px">
				<input name="swcotizliquiint" id="swcotizliquiint" <?php if($swcotizliquiint == 1){ echo 'checked="checked"'; } ?> value="1" type="radio"></td>
				<td class="auto-style3" style="width: 50px">
				<input name="swcotizliquiint" id="swcotizliquiint" <?php if($swcotizliquiint == 2){ echo 'checked="checked"'; } ?> value="2" type="radio"></td>
				<td class="alinearizq" style="width: 81px">
				<?php
					$con = mysqli_query($conectar,"select * from adjunto_actor where caj_clave_int = '".$clacaj."' and tad_clave_int = 14 and ada_sw_eliminado = 0");
					$numadj = mysqli_fetch_array($con);
					
					if($numadj != '')
					{
				?>
				<div class='ok' style='width: 105px' align='center'>
				<img src='../../images/veradjuntos.png' onclick="VERADJUNTOS('top');MOSTRARADJUNTOS('<?php echo $clacaj; ?>','14')" style='width:100px; height:30px; cursor: pointer' />
				</div>
				<?php
					}
					else
					{
				?>
						<div class='validaciones' style='width: 50%' align='center'>Sin Adjunto</div>
				<?php
					}
				?>
				</td>
				<td class="alinearizq" style="width: 155px">
				<?php
				if($metodo == 1 and ($mipad == 0 or $mipad == ''))
				{
				?><progress id="barra_de_progresoint" value="0" max="100"></progress><br>
				<div class="file-wrapper">
				<input name="adjuntoint" id="adjuntoint" type="file" onchange="MOSTRARRUTA('INTERVENTORIA')" class="file" style="cursor:pointer" />
				<span class="button">Adjuntar Ot.</span>
				</div>
				<input type="submit" onclick="ADJUNTAR('INTERVENTORIA','barra_de_progresoint','adjuntoint')" value="Subir" class="btnSubmit" />
				<?php
				}
				?>
				</td>
				<td class="alinearizq" style="width: 155px">
				<div id="resultadoadjuntoint" style="width:35%">
				<?php
					$con = mysqli_query($conectar,"select * from adjunto_actor where caj_clave_int = '".$clacaj."' and tad_clave_int = 25 and ada_sw_eliminado = 0");
					$numadj = mysqli_fetch_array($con);
					
					if($numadj != '')
					{
				?>
				<div class='ok' style='width: 105px' align='center'>
				<img src='../../images/veradjuntos.png' onclick="VERADJUNTOS('top');MOSTRARADJUNTOS('<?php echo $clacaj; ?>','25')" style='width:100px; height:30px; cursor: pointer' />
				</div>
				<?php
					}
					else
					{
				?>
						<div class='validaciones' style='width: 50%' align='center'>Sin Adjunto</div>
				<?php
					}
				?>
				</div>
				</td>
			</tr>
			<tr>
				<td class="alinearizq" style="width: 19px">
				$Construcción:</td>
				<td class="auto-style3" style="width: 97px">
				<input name="numotcons" id="numotcons" value="<?php echo $numotcons; ?>" class="inputs" type="text" style="width: 80px"></td>
				<td class="auto-style3" style="width: 79px">
				<input name="vrcons" id="vrcons" onkeyup="format(this);SUMATOTAL()" onchange="format(this)" value="<?php echo number_format($vrcons,0 , "," ,"."); ?>" class="inputs" type="text" style="width: 100px"></td>
				<td class="auto-style3" style="width: 50px">
				<input name="swcotizliquicons" id="swcotizliquicons" <?php if($swcotizliquicons == 1){ echo 'checked="checked"'; } ?> value="1" type="radio"></td>
				<td class="auto-style3" style="width: 50px">
				<input name="swcotizliquicons" id="swcotizliquicons" <?php if($swcotizliquicons == 2){ echo 'checked="checked"'; } ?> value="2" type="radio"></td>
				<td class="alinearizq" style="width: 81px">
				<?php
					$con = mysqli_query($conectar,"select * from adjunto_actor where caj_clave_int = '".$clacaj."' and tad_clave_int = 17 and ada_sw_eliminado = 0");
					$numadj = mysqli_fetch_array($con);
					
					if($numadj != '')
					{
				?>
				<div class='ok' style='width: 105px' align='center'>
				<img src='../../images/veradjuntos.png' onclick="VERADJUNTOS('top');MOSTRARADJUNTOS('<?php echo $clacaj; ?>','17')" style='width:100px; height:30px; cursor: pointer' />
				</div>
				<?php
					}
					else
					{
				?>
						<div class='validaciones' style='width: 50%' align='center'>Sin Adjunto</div>
				<?php
					}
				?>
				</td>
				<td class="alinearizq" style="width: 155px">
				<?php
				if($metodo == 1 and ($mipad == 0 or $mipad == ''))
				{
				?>
                    <progress id="barra_de_progresocons" value="0" max="100"></progress><br>
				<div class="file-wrapper">
				<input name="adjuntocon" id="adjuntocon" type="file" onchange="MOSTRARRUTA('CONSTRUCTOR')" class="file" style="cursor:pointer" />
				<span class="button">Adjuntar Ot.</span>
				</div>
				<input type="submit" onclick="ADJUNTAR('CONSTRUCTOR','barra_de_progresocons','adjuntocon')" value="Subir" class="btnSubmit" />
				<?php
				}
				?>
				</td>
				<td class="alinearizq" style="width: 155px">
				<div id="resultadoadjuntocon" style="width:35%">
				<?php
					$con = mysqli_query($conectar,"select * from adjunto_actor where caj_clave_int = '".$clacaj."' and tad_clave_int = 26 and ada_sw_eliminado = 0");
					$numadj = mysqli_fetch_array($con);
					
					if($numadj != '')
					{
				?>
				<div class='ok' style='width: 105px' align='center'>
				<img src='../../images/veradjuntos.png' onclick="VERADJUNTOS('top');MOSTRARADJUNTOS('<?php echo $clacaj; ?>','26')" style='width:100px; height:30px; cursor: pointer' />
				</div>
				<?php
					}
					else
					{
				?>
						<div class='validaciones' style='width: 50%' align='center'>Sin Adjunto</div>
				<?php
					}
				?>
				</div>
				</td>
			</tr>
			<tr>
				<td class="alinearizq" style="width: 19px">
				$Instalador Seguridad:</td>
				<td class="auto-style3" style="width: 97px">
				<input name="numotseg" id="numotseg" value="<?php echo $numotseg; ?>" class="inputs" type="text" style="width: 80px"></td>
				<td class="auto-style3" style="width: 79px">
				<input name="vrseg" id="vrseg" onkeyup="format(this);SUMATOTAL()" onchange="format(this)" value="<?php echo number_format($vrseg,0 , "," ,"."); ?>" class="inputs" type="text" style="width: 100px"></td>
				<td class="auto-style3" style="width: 50px">
				<input name="swcotizliquiseg" id="swcotizliquiseg" <?php if($swcotizliquiseg == 1){ echo 'checked="checked"'; } ?> value="1" type="radio"></td>
				<td class="auto-style3" style="width: 50px">
				<input name="swcotizliquiseg" id="swcotizliquiseg" <?php if($swcotizliquiseg == 2){ echo 'checked="checked"'; } ?> value="2" type="radio"></td>
				<td class="alinearizq" style="width: 81px">
				<?php
					$con = mysqli_query($conectar,"select * from adjunto_actor where caj_clave_int = '".$clacaj."' and tad_clave_int = 20 and ada_sw_eliminado = 0");
					$numadj = mysqli_fetch_array($con);
					
					if($numadj != '')
					{
				?>
				<div class='ok' style='width: 105px' align='center'>
				<img src='../../images/veradjuntos.png' onclick="VERADJUNTOS('top');MOSTRARADJUNTOS('<?php echo $clacaj; ?>','20')" style='width:100px; height:30px; cursor: pointer' />
				</div>
				<?php
					}
					else
					{
				?>
						<div class='validaciones' style='width: 50%' align='center'>Sin Adjunto</div>
				<?php
					}
				?>
				</td>
				<td class="alinearizq" style="width: 155px">
				<?php
				if($metodo == 1 and ($mipad == 0 or $mipad == ''))
				{
				?>
                    <progress id="barra_de_progresoseg" value="0" max="100"></progress>
				<div class="file-wrapper">
				<input name="adjuntoseg" id="adjuntoseg" type="file" onchange="MOSTRARRUTA('SEGURIDAD')" class="file" style="cursor:pointer; right: 0; top: 0;" />
				<span class="button">Adjuntar Ot.</span>
				</div>
				<input type="submit" onclick="ADJUNTAR('SEGURIDAD','barra_de_progresoseg','adjuntoseg')" value="Subir" class="btnSubmit" />
				<?php
				}
				?>
				</td>
				<td class="alinearizq" style="width: 155px">
				<div id="resultadoadjuntoseg" style="width:35%">
				<?php
					$con = mysqli_query($conectar,"select * from adjunto_actor where caj_clave_int = '".$clacaj."' and tad_clave_int = 27 and ada_sw_eliminado = 0");
					$numadj = mysqli_fetch_array($con);
					
					if($numadj != '')
					{
				?>
				<div class='ok' style='width: 105px' align='center'>
				<img src='../../images/veradjuntos.png' onclick="VERADJUNTOS('top');MOSTRARADJUNTOS('<?php echo $clacaj; ?>','27')" style='width:100px; height:30px; cursor: pointer' />
				</div>
				<?php
					}
					else
					{
				?>
						<div class='validaciones' style='width: 50%' align='center'>Sin Adjunto</div>
				<?php
					}
				?>
				</div>
				</td>
			</tr>
			<tr>
				<td class="alinearizq" style="width: 19px">
				$Total:</td>
				<td class="auto-style3" colspan="2">
				<div id="sumatotal">
				<input name="sumatotal" id="sumatotal" disabled="disabled" value="<?php echo number_format($vrinm+$vrvis+$vrdis+$vrlic+$vrint+$vrcons+$vrseg,0 , "," ,"."); ?>" class="inputs" type="text" style="width: 210px"></td>
				</div>
				<td class="auto-style3" style="width: 50px">
				&nbsp;</td>
				<td class="auto-style3" style="width: 50px">
				&nbsp;</td>
				<td class="alinearizq" style="width: 81px">
				&nbsp;</td>
				<td class="alinearizq" style="width: 155px">
				&nbsp;</td>
				<td class="alinearizq" style="width: 155px">
				&nbsp;</td>
			</tr>
			<tr>
				<td colspan="8" class="alinearizq">
				<a onclick="VERNOTAS();MOSTRARBITACORA('<?php echo $clacaj; ?>')" style="cursor:pointer" class="boton"><span class="colorboton">BITACORA DE NOTAS</span></a>
				&nbsp;&nbsp;<a onclick="VERADJUNTOSBITACORA();MOSTRARBITACORAADJUNTOS('<?php echo $clacaj; ?>')" style="cursor:pointer" class="boton"><span class="colorboton">BITACORA DE ADJUNTOS</span></a>				</td>
				<td align="center">
				&nbsp;</td>
			</tr>
			<tr>
				<td colspan="8" align="center">
				<?php
				if($metodo == 1)
				{
				?>
				<br>
				<a onclick="GUARDAR('<?php echo $clacaj; ?>')" style="cursor:pointer" class="boton"><span class="colorboton">GUARDAR INFORMACIÓN</span></a>
				<?php
				}
				?>
				</td>
				<td align="center">
				&nbsp;</td>
			</tr>
		</table>
		<div id="estadoregistro"></div>
	</fieldset>
<?php
		exit();
	}
	if($_GET['estadoadjunto'] == 'si')
	{
		$actor = $_GET['actor'];
		$clacaj = $_GET['clacaj'];
		
		if($actor == 'INMOBILIARIA')
		{
			$con = mysqli_query($conectar,"select * from adjunto_actor where caj_clave_int = '".$clacaj."' and tad_clave_int = 21 and ada_sw_eliminado = 0");
			$numadj = mysqli_fetch_array($con);
			
			if($numadj != '')
			{
		?>
				<div class='ok' style='width: 105px' align='center'>
				<img src='../../images/veradjuntos.png' onclick="VERADJUNTOS('top');MOSTRARADJUNTOS('<?php echo $clacaj; ?>','21')" style='width:100px; height:30px; cursor: pointer' />
				</div>
		<?php
			}
		}
		else
		if($actor == 'VISITA')
		{
			$con = mysqli_query($conectar,"select * from adjunto_actor where caj_clave_int = '".$clacaj."' and tad_clave_int = 22 and ada_sw_eliminado = 0");
			$numadj = mysqli_fetch_array($con);
			
			if($numadj != '')
			{
		?>
				<div class='ok' style='width: 105px' align='center'>
				<img src='../../images/veradjuntos.png' onclick="VERADJUNTOS('top');MOSTRARADJUNTOS('<?php echo $clacaj; ?>','22')" style='width:100px; height:30px; cursor: pointer' />
				</div>
		<?php
			}
		}
		else
		if($actor == 'DISENO')
		{
			$con = mysqli_query($conectar,"select * from adjunto_actor where caj_clave_int = '".$clacaj."' and tad_clave_int = 23 and ada_sw_eliminado = 0");
			$numadj = mysqli_fetch_array($con);
			
			if($numadj != '')
			{
		?>
				<div class='ok' style='width: 105px' align='center'>
				<img src='../../images/veradjuntos.png' onclick="VERADJUNTOS('top');MOSTRARADJUNTOS('<?php echo $clacaj; ?>','23')" style='width:100px; height:30px; cursor: pointer' />
				</div>
		<?php
			}
		}
		else
		if($actor == 'LICENCIA')
		{
			$con = mysqli_query($conectar,"select * from adjunto_actor where caj_clave_int = '".$clacaj."' and tad_clave_int = 24 and ada_sw_eliminado = 0");
			$numadj = mysqli_fetch_array($con);
			
			if($numadj != '')
			{
		?>
				<div class='ok' style='width: 105px' align='center'>
				<img src='../../images/veradjuntos.png' onclick="VERADJUNTOS('top');MOSTRARADJUNTOS('<?php echo $clacaj; ?>','24')" style='width:100px; height:30px; cursor: pointer' />
				</div>
		<?php
			}
		}
		else
		if($actor == 'INTERVENTORIA')
		{
			$con = mysqli_query($conectar,"select * from adjunto_actor where caj_clave_int = '".$clacaj."' and tad_clave_int = 25 and ada_sw_eliminado = 0");
			$numadj = mysqli_fetch_array($con);
			
			if($numadj != '')
			{
		?>
				<div class='ok' style='width: 105px' align='center'>
				<img src='../../images/veradjuntos.png' onclick="VERADJUNTOS('top');MOSTRARADJUNTOS('<?php echo $clacaj; ?>','25')" style='width:100px; height:30px; cursor: pointer' />
				</div>
		<?php
			}
		}
		else
		if($actor == 'CONSTRUCTOR')
		{
			$con = mysqli_query($conectar,"select * from adjunto_actor where caj_clave_int = '".$clacaj."' and tad_clave_int = 26 and ada_sw_eliminado = 0");
			$numadj = mysqli_fetch_array($con);
			
			if($numadj != '')
			{
		?>
				<div class='ok' style='width: 105px' align='center'>
				<img src='../../images/veradjuntos.png' onclick="VERADJUNTOS('top');MOSTRARADJUNTOS('<?php echo $clacaj; ?>','26')" style='width:100px; height:30px; cursor: pointer' />
				</div>
		<?php
			}
		}
		else
		if($actor == 'SEGURIDAD')
		{
			$con = mysqli_query($conectar,"select * from adjunto_actor where caj_clave_int = '".$clacaj."' and tad_clave_int = 27 and ada_sw_eliminado = 0");
			$numadj = mysqli_fetch_array($con);
			
			if($numadj != '')
			{
		?>
				<div class='ok' style='width: 105px' align='center'>
				<img src='../../images/veradjuntos.png' onclick="VERADJUNTOS('top');MOSTRARADJUNTOS('<?php echo $clacaj; ?>','27')" style='width:100px; height:30px; cursor: pointer' />
				</div>
		<?php
			}
		}
			
		exit();
	}
	if($_GET['mostrarruta'] == 'si')
	{
		$nomadj = $_GET['nomadj'];
		echo "<div class='ok1' style='width: 100%' align='center'>$nomadj</div>";
		exit();
	}
	if($_GET['sumatotal'] == 'si')
	{
		$sumatotal = str_replace(".","",$_GET['vrinm']) + str_replace(".","",$_GET['vrvis']) + str_replace(".","",$_GET['vrdis']) + str_replace(".","",$_GET['vrlic']) + str_replace(".","",$_GET['vrint']) + str_replace(".","",$_GET['vrcons']) + str_replace(".","",$_GET['vrseg']);
	?>
		<input name="sumatotal" id="sumatotal" disabled="disabled" value="<?php echo number_format($sumatotal,0 , "," ,"."); ?>" class="inputs" type="text" style="width: 210px">
	<?php	
		exit();
	}
	if($_GET['mostrarbitacora'] == 'si')
	{
		$clacaj = $_GET['clacaj'];
		
		$con = mysqli_query($conectar,"select * from permiso_actor where pea_actor = 'Facturacion'");
		$dato = mysqli_fetch_array($con);
		$notcaj = $dato['pea_sw_nota_caj'];
		$notinm = $dato['pea_sw_nota_inm'];
		$notvis = $dato['pea_sw_nota_vis'];
		$notdis = $dato['pea_sw_nota_dis'];
		$notlic = $dato['pea_sw_nota_lic'];
		$notint = $dato['pea_sw_nota_int'];
		$notcon = $dato['pea_sw_nota_con'];
		$notseg = $dato['pea_sw_nota_seg'];
		$notfac = $dato['pea_sw_nota_fac'];
		
		if($notcaj == 1){ $ven1 = 4; }else{ $ven1 = 0; }
		if($notinm == 1){ $ven2 = 7; }else{ $ven2 = 0; }
		if($notvis == 1){ $ven3 = 18; }else{ $ven3 = 0; }
		if($notdis == 1){ $ven4 = 8; }else{ $ven4 = 0; }
		if($notlic == 1){ $ven5 = 19; }else{ $ven5 = 0; }
		if($notint == 1){ $ven6 = 9; }else{ $ven6 = 0; }
		if($notcon == 1){ $ven7 = 6; }else{ $ven7 = 0; }
		if($notseg == 1){ $ven8 = 10; }else{ $ven8 = 0; }
		if($notfac == 1){ $ven9 = 20; }else{ $ven9 = 0; }
		
		$con = mysqli_query($conectar,"select * from notausuario where ven_clave_int IN (".$ven1.",".$ven2.",".$ven3.",".$ven4.",".$ven5.",".$ven6.",".$ven7.",".$ven8.",".$ven9.") and (caj_clave_int in(SELECT caj_padre FROM cajero WHERE caj_clave_int = '".$clacaj."' and caj_padre>0 ) or caj_clave_int = '".$clacaj."' or caj_clave_int in(select caj_clave_int from cajero where caj_padre = '".$clacaj."')) and caj_clave_int>0 ORDER BY nou_clave_int");//caj_clave_int = '".$clacaj."' and
    $num = mysqli_num_rows($con);
		for($i = 0; $i < $num; $i++)
		{
			$dato = mysqli_fetch_array($con);

			$clac = $dato['caj_clave_int'];
            $concaj = mysqli_query($conectar, "select caj_nombre from cajero WHERE caj_clave_int ='".$clac."'");
            $datcaj = mysqli_fetch_array($concaj);
            $nomcaj = $datcaj['caj_nombre'];

            
			$clanot = $dato['nou_clave_int'];
			$ventana = $dato['ven_clave_int'];
			$claveusu = $dato['usu_clave_int'];
			
			$conusu = mysqli_query($conectar,"select usu_nombre from usuario where usu_clave_int = '".$claveusu."'");
			$datousu = mysqli_fetch_array($conusu);
			$nomusu = $datousu['usu_nombre'];
			
			if($ventana == 4){ $actor = 'Cajeros'; }
			if($ventana == 7){ $actor = 'Inmobiliaria'; }
			if($ventana == 18){ $actor = 'Visita'; }
			if($ventana == 8){ $actor = 'Diseño'; }
			if($ventana == 19){ $actor = 'Licencia'; }
			if($ventana == 9){ $actor = 'Interventoria'; }
			if($ventana == 6){ $actor = 'Constructor'; }
			if($ventana == 10){ $actor = 'Seguridad'; }
			if($ventana == 20){ $actor = 'Facturación'; }
			
			if($i % 2 == 0)
			{
				echo '<div style="width:100%; border:2px; font-size: 14px; outline:none; border-radius:3px; -webkit-border-radius:6px; -moz-border-radius:3px; border:1px solid rgba(0,0,0, 0.5); padding: 3px; background-color: #CCCCCC"><strong>Cajero:</strong>'.$nomcaj.'<br><strong>Nota:</strong> '.$dato['nou_nota']." <strong>Fecha:</strong> ".$dato['nou_fecha_creacion']." <strong>Actor:</strong> ".$actor." <strong>Usuario:</strong> ".$nomusu."<br></div>";
			}
			else
			{
				echo '<div style="width:100%; border:2px; font-size: 14px; outline:none; border-radius:3px; -webkit-border-radius:6px; -moz-border-radius:3px; border:1px solid rgba(0,0,0, 0.5); padding: 3px; background-color: #F0F0F0"><strong>Cajero:</strong>'.$nomcaj.'<br><strong>Nota:</strong> '.$dato['nou_nota']." <strong>Fecha:</strong> ".$dato['nou_fecha_creacion']." <strong>Actor:</strong> ".$actor." <strong>Usuario:</strong> ".$nomusu."<br></div>";
			}
		}
		echo '<div id="editarnota"></div>';
		exit();
	}
	if($_GET['mostrarbitacoraadjuntos'] == 'si')
	{
		$clacaj = $_GET['clacaj'];
		
		$con = mysqli_query($conectar,"select * from permiso_actor where pea_actor = 'Facturacion'");
		$dato = mysqli_fetch_array($con);
		$adjcaj = $dato['pea_sw_adjunto_caj'];
		$adjinm = $dato['pea_sw_adjunto_inm'];
		$adjvis = $dato['pea_sw_adjunto_vis'];
		$adjdis = $dato['pea_sw_adjunto_dis'];
		$adjlic = $dato['pea_sw_adjunto_lic'];
		$adjint = $dato['pea_sw_adjunto_int'];
		$adjcon = $dato['pea_sw_adjunto_con'];
		$adjseg = $dato['pea_sw_adjunto_seg'];
		$adjfac = $dato['pea_sw_adjunto_fac'];
		
		if($adjcaj == 1){ $act1 = 1; }else{ $act1 = ''; }
		if($adjinm == 1){ $act2 = 2; }else{ $act2 = ''; }
		if($adjvis == 1){ $act3 = 3; }else{ $act3 = ''; }
		if($adjdis == 1){ $act4 = 4; }else{ $act4 = ''; }
		if($adjlic == 1){ $act5 = 5; }else{ $act5 = ''; }
		if($adjint == 1){ $act6 = 6; }else{ $act6 = ''; }
		if($adjcon == 1){ $act7 = 7; }else{ $act7 = ''; }
		if($adjseg == 1){ $act8 = 8; }else{ $act8 = ''; }
		if($adjfac == 1){ $act9 = 9; }else{ $act9 = ''; }
		
		$con = mysqli_query($conectar,"select * from adjunto_actor aa inner join tipo_adjunto ta on (aa.tad_clave_int = ta.tad_clave_int) inner join actor a on (a.act_clave_int = ta.act_clave_int) where aa.ada_sw_eliminado = 0 and a.act_clave_int IN ('".$act1."','".$act2."','".$act3."','".$act4."','".$act5."','".$act6."','".$act7."','".$act8."','".$act9."')  and (aa.caj_clave_int in(SELECT caj_padre FROM cajero WHERE caj_clave_int = '".$clacaj."' and caj_padre>0) or aa.caj_clave_int = '".$clacaj."' ) ORDER BY a.act_clave_int,aa.ada_fecha_creacion ASC");//aa.caj_clave_int = '".$clacaj."' and
    $num = mysqli_num_rows($con);
		for($i = 0; $i < $num; $i++)
		{
			$dato = mysqli_fetch_array($con);
			$claada = $dato['ada_clave_int'];
			$nomadj = $dato['ada_nombre_adjunto'];
			$usuact = $dato['ada_usu_actualiz'];
			$adju = $dato['ada_adjunto'];
			$conusu = mysqli_query($conectar,"select usu_nombre from usuario where usu_usuario = '".$usuact."'");
			$datousu = mysqli_fetch_array($conusu);
			$nomusu = $datousu['usu_nombre'];
			
			if($i % 2 == 0)
			{
			?>
				<div style="width:100%; border:2px; float:left; outline:none; border-radius:3px; -webkit-border-radius:6px; -moz-border-radius:3px; border:1px solid rgba(0,0,0, 0.5); padding: 3px; background-color: #CCCCCC;">
				
					<table style="width: 100%; height: 19px; font-size:14px">
						<tr>
							<td><strong>Adjunto: </strong><?php echo $nomadj; ?> <strong>Fec. Creación: </strong><?php echo $dato['ada_fecha_creacion']; ?><strong> Fec. Actualización: </strong><?php echo $dato['ada_fec_actualiz']; ?> <strong> Actor: </strong><?php echo $dato['act_nombre']; ?><strong> Usuario: </strong><?php echo $nomusu; ?></td>
							<td>
							<div style="float:left">
							<div class='ok' style='width: 50px; height:30px; float:left' align='center'>
							<a href='descargar.php?claada=<?php echo $claada; ?>&clacaj=<?php echo $clacaj; ?>' target='_blank'>
							<img src='../../images/descargar.png' style='width:25px; height:25px; cursor: pointer' /></a>
							</div>
							</div>
                                <div class='ok' style='width: 50px; height:30px; float:left' align='center'>
                                    <a onclick="VISOR('<?PHP echo $adju;?>','todoslosadjuntosbitacora')" target='_blank'>
                                        <img src='../../images/ver.png' style='width:25px; height:25px; cursor: pointer' /></a>
                                </div>

							</td>
							<?php
							if($actor < 5)
							{
							?>
							<?php 
							}
							?>
						</tr>
					</table>
				</div>
			<?php
			}
			else
			{
			?>
				<div style="width:100%; border:2px; outline:none; border-radius:3px; -webkit-border-radius:6px; -moz-border-radius:3px; border:1px solid rgba(0,0,0, 0.5); padding: 3px; background-color: #F0F0F0;">
				<table style="width: 100%; height: 19px; font-size:14px">
					<tr>
						<td><strong>Adjunto: </strong><?php echo $nomadj; ?> <strong>Fec. Creación: </strong><?php echo $dato['ada_fecha_creacion']; ?><strong> Fec. Actualización: </strong><?php echo $dato['ada_fec_actualiz']; ?><strong> Actor: </strong><?php echo $dato['act_nombre']; ?><strong> Usuario: </strong><?php echo $nomusu; ?></td>
						<td>
						<div style="float:left">
						<div class='ok' style='width: 50px; height:30px; float:left' align='center'>
						<a href='descargar.php?claada=<?php echo $claada; ?>&clacaj=<?php echo $clacaj; ?>' target='_blank'>
						<img src='../../images/descargar.png' style='width:25px; height:25px; cursor: pointer' /></a>
						</div>
						</div>
                            <div class='ok' style='width: 50px; height:30px; float:left' align='center'>
                                <a onclick="VISOR('<?PHP echo $adju;?>','todoslosadjuntosbitacora')" target='_blank'>
                                    <img src='../../images/ver.png' style='width:25px; height:25px; cursor: pointer' /></a>
                            </div>
						</td>
						<?php
						if($actor < 5)
						{
						?>
						<?php 
						}
						?>
					</tr>
				</table>
			</div>
			<?php
			}
		}
		
		exit();
	}
	if($_GET['mostraradjuntos'] == 'si')
	{
		$clacaj = $_GET['clacaj'];
		$actor = $_GET['actor'];
		$con = mysqli_query($conectar,"select * from adjunto_actor where caj_clave_int = '".$clacaj."' and tad_clave_int = '".$actor."' and ada_sw_eliminado = 0 ORDER BY ada_fecha_creacion ASC");
		$num = mysqli_num_rows($con);
		if($actor == 21){ $tit = 'Adjunto OT. Inmobiliaria'; }elseif($actor == 22){ $tit = 'Adjunto OT. Visita'; }elseif($actor == 23){ $tit = 'Adjunto OT. Diseño'; }elseif($actor == 24){ $tit = 'Adjunto OT. Licencia'; }elseif($actor == 25){ $tit = 'Adjunto OT. Interventoria'; }elseif($actor == 26){ $tit = 'Adjunto OT. Constructor'; }elseif($actor == 27){ $tit = 'Adjunto OT. Instalador seguridad'; }elseif($actor == 6){ $tit = 'Adjunto facturación inmobiliaria'; }elseif($actor == 8){ $tit = 'Adjunto facturación visita'; }elseif($actor == 10){ $tit = 'Adjunto facturación diseño'; }elseif($actor == 12){ $tit = 'Adjunto facturación licencia'; }elseif($actor == 14){ $tit = 'Adjunto facturación interventoria'; }elseif($actor == 17){ $tit = 'Adjunto facturación constructor'; }elseif($actor == 20){ $tit = 'Adjunto facturación seguridad'; }
		echo "<h3>".$tit."</h3><hr>";
		for($i = 0; $i < $num; $i++)
		{
			$dato = mysqli_fetch_array($con);
			$claada = $dato['ada_clave_int'];
			$nomadj = $dato['ada_nombre_adjunto'];
			$adju = $dato['ada_adjunto'];
			if($i % 2 == 0)
			{
			?>
				<div id="service<?php echo $claada; ?>" style="width:100%; border:2px; float:left; outline:none; border-radius:3px; -webkit-border-radius:6px; -moz-border-radius:3px; border:1px solid rgba(0,0,0, 0.5); padding: 3px; background-color: #CCCCCC;">
				
					<table style="width: 100%; height: 19px; font-size:14px">
						<tr>
							<td><strong>Adjunto: </strong><?php echo $nomadj; ?> <strong>Fec. Creación: </strong><?php echo $dato['ada_fecha_creacion']; ?><strong> Fec. Actualización: </strong><?php echo $dato['ada_fec_actualiz']; ?></td>
							<td>
							<div style="float:left">
							<div class='ok' style='width: 50px; height:30px; float:left' align='center'>
							<a href='descargar.php?claada=<?php echo $claada; ?>&clacaj=<?php echo $clacaj; ?>' target='_blank'>
							<img src='../../images/descargar.png' style='width:25px; height:25px; cursor: pointer' /></a>
							</div>
							</div>
                                <div class='ok' style='width: 50px; height:30px; float:left' align='center'>
                                    <a onclick="VISOR('<?PHP echo $adju;?>','adjuntoarchivos')" target='_blank'>
                                        <img src='../../images/ver.png' style='width:25px; height:25px; cursor: pointer' /></a>
                                </div>
							</td>
							<?php 
							if($actor <> 6 and $actor <> 8 and $actor <> 10 and $actor <> 12 and $actor <> 14 and $actor <> 17 and $actor <> 20)
							{
							?>
							<td>
							<img src="../../img/editar.png" onclick="EDITARADJUNTO('<?php echo $claada; ?>','<?php echo $actor; ?>')" alt="" height="25" width="25" style="cursor:pointer" />
							</td>
							<?php 
							}
							?>
							<?php 
							if($actor <> 6 and $actor <> 8 and $actor <> 10 and $actor <> 12 and $actor <> 14 and $actor <> 17 and $actor <> 20)
							{
							?>
							<td>
							<button name="accion" onclick="ELIMINARADJUNTO('<?php echo $claada; ?>')" value="imprimir" type="button" style="cursor:pointer;">
							<img src="../../images/delete.png" height="15" width="15" class="auto-style13"></button>
							</td>
							<?php 
							}
							?>
						</tr>
					</table>
				</div>
			<?php
			}
			else
			{
			?>
				<div id="service<?php echo $claada; ?>" style="width:100%; border:2px; outline:none; border-radius:3px; -webkit-border-radius:6px; -moz-border-radius:3px; border:1px solid rgba(0,0,0, 0.5); padding: 3px; background-color: #F0F0F0;">
				<table style="width: 100%; height: 19px; font-size:14px">
					<tr>
						<td><strong>Adjunto: </strong><?php echo $nomadj; ?> <strong>Fec. Creación: </strong><?php echo $dato['ada_fecha_creacion']; ?><strong> Fec. Actualización: </strong><?php echo $dato['ada_fec_actualiz']; ?></td>
						<td>
						<div style="float:left">
						<div class='ok' style='width: 50px; height:30px; float:left' align='center'>
						<a href='descargar.php?claada=<?php echo $claada; ?>&clacaj=<?php echo $clacaj; ?>' target='_blank'>
						<img src='../../images/descargar.png' style='width:25px; height:25px; cursor: pointer' /></a>
						</div>
						</div>
                            <div class='ok' style='width: 50px; height:30px; float:left' align='center'>
                                <a onclick="VISOR('<?PHP echo $adju;?>','adjuntoarchivos')" target='_blank'>
                                    <img src='../../images/ver.png' style='width:25px; height:25px; cursor: pointer' /></a>
                            </div>
						</td>
						<?php 
						if($actor <> 6 and $actor <> 8 and $actor <> 10 and $actor <> 12 and $actor <> 14 and $actor <> 17 and $actor <> 20)
						{
						?>
						<td>
						<img src="../../img/editar.png" onclick="EDITARADJUNTO('<?php echo $claada; ?>','<?php echo $actor; ?>')" alt="" height="25" width="25" style="cursor:pointer" />
						</td>
						<?php 
						}
						?>
						<?php 
						if($actor <> 6 and $actor <> 8 and $actor <> 10 and $actor <> 12 and $actor <> 14 and $actor <> 17 and $actor <> 20)
						{
						?>
						<td>
						<button name="accion" onclick="ELIMINARADJUNTO('<?php echo $claada; ?>')" value="imprimir" type="button" style="cursor:pointer;">
						<img src="../../images/delete.png" height="15" width="15" class="auto-style13"></button>
						</td>
						<?php 
						}
						?>
					</tr>
				</table>
			</div>
			<?php
			}
		}
		?>
		<div id="editaradjunto"></div>
		<?php
		exit();
	}
	if($_GET['editaradjunto'] == 'si')
	{
		$claada = $_GET['claada'];
		$actor = $_GET['act'];
	?>
		<br>
		<div class="file-wrapper" style="float:left">
		<input name="archivoadjunto" id="archivoadjunto" type="file" onchange="MOSTRARRUTA('ADJUNTO')" class="file" style="cursor:pointer; right: 0; top: 0;" />
		<span class="button">Adjuntar Archivo</span>
		</div>
		<div style="float:left">
		<input name="subir" type="button" id="botonSubidor" onclick="ADJUNTARINFORMACION('ACTUALIZAARCHIVO','<?php echo $claada; ?>','<?php echo $actor; ?>');" value="Subir" class="btnSubmit" />
		</div>
		<div id="resultadoadjunto" style="float:left"></div>
	<?php
		exit();
	}
	if($_GET['estadoadjuntoinformacion'] == 'si')
	{
		$claada = $_GET['claada'];
		?>
			<div class='ok' style='width: 105px' align='center'>
			<?php
				$con = mysqli_query($conectar,"select ada_adjunto from adjunto_actor where ada_clave_int = '".$claada."'");
				$dato = mysqli_fetch_array($con);
				$adj = $dato['ada_adjunto'];
				
				if($adj != '')
				{
			?>
			<div class='ok' style='width: 25%' align='center'>
			<a href='descargar.php?claada=<?php echo $claada; ?>' target='_blank'>
			<img src='../../images/descargar.png' style='width:25px; height:25px; cursor: pointer' /></a>
			</div>
                    <div class='ok' style='width: 50px; height:30px; float:left' align='center'>
                        <a onclick="VISOR('<?PHP echo $adj;?>','adjuntoarchivos')" target='_blank'>
                            <img src='../../images/ver.png' style='width:25px; height:25px; cursor: pointer' /></a>
                    </div>
			<?php
				}
			?>
			</div>
		<?php
		exit();
	}
	if($_GET['vermodalidades1'] == 'si')
	{
		$tii = $_GET['tii'];
?>
		<select name="busmodalidad" id="busmodalidad" onchange="BUSCAR('CAJERO','')" tabindex="8" class="inputs" data-placeholder="-Seleccione-" style="width: 140px">
		<option value="">-Seleccione-</option>
		<?php
			$con = mysqli_query($conectar,"select * from modalidad where mod_clave_int in (select mod_clave_int from intervencion_modalidad where tii_clave_int = ".$tii.") order by mod_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clave = $dato['mod_clave_int'];
				$nombre = $dato['mod_nombre'];
		?>
			<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
		<?php
			}
		?>
		</select>

<?php
		exit();
	}
?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>SEGUIMIENTO CAJEROS</title>
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
    <link rel="stylesheet" href="../../css/reveal.css?<?php echo time();?>" />

<?php //VALIDACIONES ?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="llamadas.js?<?php echo time();?>"></script>

<script type="text/javascript" src="../../js/jquery.searchabledropdown-1.0.8.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("select").searchable();
});
</script>
<?php //CALENDARIO ?>
<link type="text/css" rel="stylesheet" href="../../../css/dhtmlgoodies_calendar.css?random=20051112" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../../../js/dhtmlgoodies_calendar.js?random=20060118"></script>

<link rel="stylesheet" href="../../../css/jquery-ui.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

<link rel="stylesheet" href="../../css/jquery-ui.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

<?php //ESTO ME PERMITE SUBIR ARCHIVOS DE TODO TIPO ?>	
<script language="javascript">
    function ADJUNTAR(v,p,a)
    {
        var clacaj = form1.clavecajero.value;
        var num = 0;
        $("#form1").on('submit',(function(e) {
            e.preventDefault();

            if(clacaj == '')
            {
                alert("Debe guardar los datos del cajero para poder adjuntar este archivo");
            }
            else
            {
                var fileSize = $('#' + a)[0].files[0].size/1024/1024;

                if (fileSize > 8) { // 8M
                    alert("El archivo que ha intentado adjuntar es mayor de 8 Mb, si desea cambie el tamaño del archivo y vuelva a intentarlo. Tamaño de archivo:"+fileSize + "M");
                    $('#' + p).hide();
                    RESULTADOADJUNTO(v, clacaj);
                }
                else
                {
                    $('#' + p).show();
                    $("#" + a).upload('upload.php',
                        {
                            clacaj: clacaj,
                            claada: '', actor: v, archivo: ''
                        },
                        function (respuesta) {
                            //Subida finalizada.
                            $("#" + p).val(0);
                            var data = $.trim(respuesta);
                            if (data == "error1") {
                                alert("No se permite que se suban archivos con extensiones DWG, RAR O ZIP por favor convertirlos a un PDF");
                                $('#' + p).hide();
                                RESULTADOADJUNTO(v, clacaj);
                            }
                            else if (data == "error2") {
                                alert("El archivo no se adjunto");
                                $('#' + p).hide();
                                RESULTADOADJUNTO(v, clacaj);

                            }
                            else if (data == "error3") {
                                alert("El archivo que ha intentado adjuntar es mayor de 8 Mb, si desea cambie el tamaño del archivo y vuelva a intentarlo.");
                                $('#' + p).hide();
                                RESULTADOADJUNTO(v, clacaj);
                            }
                            else if (data == "error4") {
                                alert("Ya hay un archivo con el mismo nombre y formato adjunto a este cajero.");
                                $('#' + p).hide();
                                RESULTADOADJUNTO(v, clacaj);
                            }

                            else {
                                //$("#targetLayer").html(data);
                                $('#' + p).hide(8000);
                                RESULTADOADJUNTO(v, clacaj);
                            }


                        }, function (progreso, valor) {
                            //Barra de progreso.
                            if (valor <= 25) {
                                $('#' + p).css("background-color", "red");
                            } else if (valor > 25 && valor <= 50) {
                                $('#' + p).css("background-color", "blue");
                            }
                            else if (valor > 50 && valor <= 99) {
                                $('#' + p).css("background-color", "orange");
                            } else if (valor == 100) {
                                $('#' + p).css("background-color", "green");
                            }
                            console.log(valor);
                            $("#" + p).val(valor);
                        });
                }
            }
        }));
    }
    /*function ADJUNTAR(v)
{
	var clacaj = form1.clavecajero.value;
	var num = 0;
	$("#form1").on('submit',(function(e) {
		e.preventDefault();

		if(clacaj == '')
		{
			alert("Debe guardar los datos del cajero para poder adjuntar este archivo");
		}
		else
		{
			if(num == 0)
			{
				$.ajax({
		        	url: "upload.php?clacaj="+clacaj+"&actor="+v,
					type: "POST",
					data:  new FormData(this),
					contentType: false,
		    	    cache: false,
					processData:false,
					success: function(data)
				    {
					//$("#targetLayer").html(data);
                        data = $.trim(data);
                        if(data=="error1")
                        {
                            alert("No se permite que se suban archivos con extensiones DWG, RAR O ZIP por favor convertirlos a un PDF");
                        }
                        else if(data=="error2")
                        {
                            alert("El archivo no se adjunto");
                        }
                        else if (data == "error3") {
                            alert("El archivo que ha intentado adjuntar es mayor de 8 Mb, si desea cambie el tamaño del archivo y vuelva a intentarlo.");
                        }
                        else if (data == "error4") {
                            alert("Ya hay un archivo con el mismo nombre y formato adjunto a este cajero.");

                        }
                        else {
                            RESULTADOADJUNTO(v, clacaj);
                        }
				    },
				  	error: function() 
			    	{
			    	} 	        
			   });
			   num = 1;
			}
		}
	}));
}*/
function ADJUNTARINFORMACION(v,ca,a,p)
{
    var clacaj = form1.clavecajero.value;
    var inputFileImage = document.getElementById("archivoadjunto");
    var file = inputFileImage.files[0];
    var data = new FormData();
    data.append('archivoadjunto',file);

    if(clacaj == '')
    {
        alert("Debe guardar los datos del cajero para poder adjuntar este archivo");
    }
    else
    {
        var fileSize = $('#archivoadjunto')[0].files[0].size/1024/1024;

        if (fileSize > 8) { // 8M
            alert("El archivo que ha intentado adjuntar es mayor de 8 Mb, si desea cambie el tamaño del archivo y vuelva a intentarlo. Tamaño de archivo:"+fileSize + "M");
            $('#' + p).hide();
            RESULTADOADJUNTOINFORMACION(ca);
        }
        else
        {
            $('#' + p).show();
            $("#archivoadjunto" ).upload('upload.php',
                {
                    clacaj: clacaj,
                    claada: ca, actor: v, archivo: a
                },
                function (respuesta) {
                    //Subida finalizada.
                    $("#" + p).val(0);
                    var data = $.trim(respuesta);
                    if (data == "error1") {
                        alert("No se permite que se suban archivos con extensiones DWG, RAR O ZIP por favor convertirlos a un PDF");
                        $('#' + p).hide();
                        RESULTADOADJUNTOINFORMACION(ca);
                    }
                    else if (data == "error2") {
                        alert("El archivo no se adjunto");
                        $('#' + p).hide();
                        RESULTADOADJUNTOINFORMACION(ca);

                    }
                    else if (data == "error3") {
                        alert("El archivo que ha intentado adjuntar es mayor de 8 Mb, si desea cambie el tamaño del archivo y vuelva a intentarlo.");
                        $('#' + p).hide();
                        RESULTADOADJUNTOINFORMACION(ca);
                    }
                    else if (data == "error4") {
                        alert("Ya hay un archivo con el mismo nombre y formato adjunto a este cajero.");
                        $('#' + p).hide();
                        RESULTADOADJUNTOINFORMACION(ca);
                    }

                    else {
                        //$("#targetLayer").html(data);
                        $('#' + p).hide(8000);
                        RESULTADOADJUNTOINFORMACION(ca);
                    }


                }, function (progreso, valor) {
                    //Barra de progreso.
                    if (valor <= 25) {
                        $('#' + p).css("background-color", "red");
                    } else if (valor > 25 && valor <= 50) {
                        $('#' + p).css("background-color", "blue");
                    }
                    else if (valor > 50 && valor <= 99) {
                        $('#' + p).css("background-color", "orange");
                    } else if (valor == 100) {
                        $('#' + p).css("background-color", "green");
                    }
                    console.log(valor);
                    $("#" + p).val(valor);
                });
        }
    }
}
/*
function ADJUNTARINFORMACION(v,ca,a)
{
	var clacaj = form1.clavecajero.value;
	
	var inputFileImage = document.getElementById("archivoadjunto");
	var file = inputFileImage.files[0];
	var data = new FormData();
	data.append('archivoadjunto',file);
	
	if(clacaj == '')
	{
		alert("Debe guardar los datos del cajero para poder adjuntar este archivo");
	}
	else
	{
		$.ajax({
        	url: "upload.php?clacaj="+clacaj+"&actor="+v+"&claada="+ca+"&archivo="+a,
			type: "POST",
			data: data,
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
			//$("#targetLayer").html(data);
                data = $.trim(data);
                if(data=="error1")
                {
                    alert("No se permite que se suban archivos con extensiones DWG, RAR O ZIP por favor convertirlos a un PDF");
                }
                else if(data=="error2")
                {
                    alert("El archivo no se adjunto");
                }
                else if (data == "error3") {
                    alert("El archivo que ha intentado adjuntar es mayor de 8 Mb, si desea cambie el tamaño del archivo y vuelva a intentarlo.");
                }
                else if (data == "error4") {
                    alert("Ya hay un archivo con el mismo nombre y formato adjunto a este cajero.");

                }
                else {
                    RESULTADOADJUNTOINFORMACION(ca);
                }
		    },
		  	error: function() 
	    	{
	    	} 	        
	   });
	}
}*/
</script>
<script language="javascript1.2" type="text/javascript">
var IE = document.all ? true : false;
if (!IE) {
    document.captureEvents(Event.MOUSEMOVE);
}
document.onmousemove = getMouseXY;
var tempX = 0;
var tempY = 0;
//esta funcion no necesitas entender solo asigna la posicion del raton a tempX y tempY
function getMouseXY(e){
    if (IE) { //para IE
        tempX = event.clientX + document.body.scrollLeft;
        tempY = event.clientY + document.body.scrollTop;
    }
    else { //para netscape
        tempX = e.pageX;
        tempY = e.pageY;
    }
    if (tempX < 0) {
        tempX = 0;
    }
    if (tempY < 0) {
        tempY = 0;
    }
    return true;
}
function VentanaFlotante(mensaje, x, y){
        //creo el objeto div
        var div_fl = document.createElement('DIV');
        //le asigno que su posicion sera abosoluta
        div_fl.style.position = 'absolute';
        //le asigno el ide a la ventana
        div_fl.id = 'Miventana';
        //digo en que posicion left y top se creara a partir de la posicion del raton tempx tempy
        div_fl.style.left = tempX + 'px';
        div_fl.style.top = tempY + 'px';
        //asigno el ancho del div pasado por parametro
        div_fl.style.width = x + 'px';
        //asigno el alto del div pasado por parametro
        div_fl.style.height = y + 'px';
        //digo con css que el borde sera de grosor 1px solido y de color negro
        div_fl.style.border = "1px solid #000000";
        //asigno el color de fondo
        div_fl.style.backgroundColor = "#cccccc";
        //el objeto añado a la estrutura principal el document.body
        document.body.appendChild(div_fl);
        //el mensaje pasado por parametro muestro dentro del div
        div_fl.innerHTML = mensaje;
}
function quitarDiv()
{
//creo el objeto del div
var mv = document.getElementById('Miventana');
//elimino el objeto
document.body.removeChild(mv);
}
</script>

<script>
function OCULTARSCROLL()
{
	parent.autoResize('iframe20');
	setTimeout("parent.autoResize('iframe20')",500);
	setTimeout("parent.autoResize('iframe20')",1000);
	setTimeout("parent.autoResize('iframe20')",1500);
	setTimeout("parent.autoResize('iframe20')",2000);
	setTimeout("parent.autoResize('iframe20')",2500);
	setTimeout("parent.autoResize('iframe20')",3000);
	setTimeout("parent.autoResize('iframe20')",3500);
	setTimeout("parent.autoResize('iframe20')",4000);
	setTimeout("parent.autoResize('iframe20')",4500);
	setTimeout("parent.autoResize('iframe20')",5000);
	setTimeout("parent.autoResize('iframe20')",5500);
	setTimeout("parent.autoResize('iframe20')",6000);
	setTimeout("parent.autoResize('iframe20')",6500);
	setTimeout("parent.autoResize('iframe20')",7000);
	setTimeout("parent.autoResize('iframe20')",7500);
	setTimeout("parent.autoResize('iframe20')",8000);
	setTimeout("parent.autoResize('iframe20')",8500);
	setTimeout("parent.autoResize('iframe20')",9000);
	setTimeout("parent.autoResize('iframe20')",9500);
	setTimeout("parent.autoResize('iframe20')",10000);
}
parent.autoResize('iframe20');
setTimeout("parent.autoResize('iframe20')",500);
setTimeout("parent.autoResize('iframe20')",1000);
setTimeout("parent.autoResize('iframe20')",1500);
setTimeout("parent.autoResize('iframe20')",2000);
setTimeout("parent.autoResize('iframe20')",2500);
setTimeout("parent.autoResize('iframe20')",3000);
setTimeout("parent.autoResize('iframe20')",3500);
setTimeout("parent.autoResize('iframe20')",4000);
setTimeout("parent.autoResize('iframe20')",4500);
setTimeout("parent.autoResize('iframe20')",5000);
setTimeout("parent.autoResize('iframe20')",5500);
setTimeout("parent.autoResize('iframe20')",6000);
setTimeout("parent.autoResize('iframe20')",6500);
setTimeout("parent.autoResize('iframe20')",7000);
setTimeout("parent.autoResize('iframe20')",7500);
setTimeout("parent.autoResize('iframe20')",8000);
setTimeout("parent.autoResize('iframe20')",8500);
setTimeout("parent.autoResize('iframe20')",9000);
setTimeout("parent.autoResize('iframe20')",9500);
setTimeout("parent.autoResize('iframe20')",10000);
function CAMBIARESTADO(t,p,v)
{
    $('table#' + t).find('tr>td.pestana').removeClass('activo');
    $('#' + p).addClass('activo');
    form1.ocultoestado.value = v;
}
</script>
    <style>
        progress{
            display:none;
        }
        .activo
        {
            background-color: #5a825a;
            color: #ffffff;
        }
    </style>
<script language="JavaScript">
function format(input)
{
var num = input.value.replace(/\./g,'');
if(!isNaN(num)){
num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
num = num.split('').reverse().join('').replace(/^[\.]/,'');
input.value = num;
}
else{ alert('Solo se permiten numeros');
input.value = input.value.replace(/[^\d\.]*/g,'');
}
}
</script>
    <link rel="stylesheet" href="../../js/datatables/jquery.dataTables.css?<?php echo time();?>"/>
    <link rel="stylesheet" href="../../js/datatables/dataTables.bootstrap.css?<?php echo time()?>"/>
    <link rel="stylesheet" href="../../js/datatables/extensions/Responsive/css/dataTables.responsive.css?<?php echo time();?>"/>
</head>
<body>
<form name="form1" id="form1" method="post" action="javascript:void(0);">
<!--[if lte IE 7]>
<div class="ieWarning">Este navegador no es compatible con el sistema. Por favor, use Chrome, Safari, Firefox o Internet Explorer 8 o superior.</div>
<![endif]-->
	<input name="ocultoestado" id="ocultoestado" value="1" type="hidden" />
	<table style="width: 100%" id="tbPestanas">
		<tr>
			<td class="auto-style1 pestana activo" id="pactivos" style="cursor:pointer" colspan="2" onclick="MODULO('TODOS','1'),CAMBIARESTADO('tbPestanas',this.id,'1')" onmouseover="this.style.backgroundColor='#5a825a';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';">
			Ver Activos
			<?php
			$con = mysqli_query($conectar,"select * from cajero where caj_estado_proyecto = 1 and caj_sw_eliminado = 0");
			
			$num = mysqli_num_rows($con);
			echo $num;
			?>
			</td>
			<td class="auto-style2 pestana" id="pprogramados" style="width: 100px; cursor:pointer" onclick="MODULO('TODOS','5'),CAMBIARESTADO('tbPestanas',this.id,'5')">
			<div id="programados">
			Ver Programados
			<?php
			if($claprf == 1 || $mostrarcajeros == 1)
			{
				$con = mysqli_query($conectar,"select * from cajero where caj_estado_proyecto = 5 and caj_sw_eliminado = 0");
			}
			else
			{
				$con = mysqli_query($conectar,"select * from cajero c inner join cajero_interventoria ci on (ci.caj_clave_int = c.caj_clave_int) where (ci.cin_interventor = '".$clausu."' OR ci.cin_interventor IN (select usa_actor from usuario_actor where usu_clave_int = '".$clausu."')) and caj_estado_proyecto = 5 and c.caj_sw_eliminado = 0");
			}
			$num = mysqli_num_rows($con);
			echo $num;
			?>
			</div>
			</td>
			<td class="auto-style2 pestana" id="psuspendidos" style="width: 100px; cursor:pointer" onclick="MODULO('TODOS','2'),CAMBIARESTADO('tbPestanas',this.id,'2')" >
			Ver Suspendidos
			<?php
			$con = mysqli_query($conectar,"select * from cajero where caj_estado_proyecto = 2 and caj_sw_eliminado = 0");
			
			$num = mysqli_num_rows($con);
			echo $num;
			?>
			</td>
			<td class="auto-style2 pestana" id="pentregados" style="width: 100px; cursor:pointer" onclick="MODULO('TODOS','3'),CAMBIARESTADO('tbPestanas',this.id,'3')">
			Entregados
			<?php
			$con = mysqli_query($conectar,"select * from cajero where caj_estado_proyecto = 3 and caj_sw_eliminado = 0");
			
			$num = mysqli_num_rows($con);
			echo $num;
			?>
			</td>
			<td class="auto-style2 pestana" id="pcancelados" style="width: 100px; cursor:pointer" onclick="MODULO('TODOS','4'),CAMBIARESTADO('tbPestanas',this.id,'4')">
			Cancelados
			<?php
			$con = mysqli_query($conectar,"select * from cajero where caj_estado_proyecto = 4 and caj_sw_eliminado = 0");
						
			$num = mysqli_num_rows($con);
			echo $num;
			?>
			</td>
		</tr>
		<tr>
			<td class="auto-style2" colspan="7" align="center">
			<div id="cajeros">
			<?php
			$est = 1;
			?>

			</div>
			</td>
		</tr>
		<tr>
			<td style="width: 100px">&nbsp;</td>
			<td style="width: 10px">&nbsp;</td>
			<td style="width: 100px">&nbsp;</td>
			<td style="width: 43px">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
	<script>
	function VERADJUNTOS(p)
	{
		$("#adjuntoarchivos").dialog({ <!--  ------> muestra la ventana  -->
			width: 650,  <!-- -------------> ancho de la ventana -->
			height: 350,<!--  -------------> altura de la ventana -->
			show: "scale", <!-- -----------> animación de la ventana al aparecer -->
			hide: "scale", <!-- -----------> animación al cerrar la ventana -->
			resizable: "false", <!-- ------> fija o redimensionable si ponemos este valor a "true" -->
			position: p,<!--  ------> posicion de la ventana en la pantalla (left, top, right...) -->
			modal: "true" <!-- ------------> si esta en true bloquea el contenido de la web mientras la ventana esta activa (muy elegante) -->
		});
	}
	</script>
	<div name="adjuntoarchivos" id="adjuntoarchivos" class="ventana" title="Adjuntos agregados">
	<br>
		<div class="file-wrapper" style="float:left">
		<input name="archivoadjunto" id="archivoadjunto" type="file" onchange="MOSTRARRUTA('ADJUNTO')" class="file" style="cursor:pointer; right: 0; top: 0;" />
		<span class="button">Adjuntar Archivo</span>
		</div>
		<div style="float:left">
		<input name="subir" type="button" id="botonSubidor" onclick="ADJUNTARINFORMACION('ACTUALIZAARCHIVO','<?php echo $claada; ?>','<?php echo $actor; ?>');" value="Subir" class="btnSubmit" />
		</div>
		<div id="resultadoadjunto" style="float:left"></div>
	</div>
	<script type="text/javascript" language="javascript">
        $(document).ready(function () {
            MODULO('TODOS','1');
        })
	function VERNOTAS()
	{
		$("#todaslasnotas").dialog({ <!--  ------> muestra la ventana  -->
			width: 590,  <!-- -------------> ancho de la ventana -->
			height: 350,<!--  -------------> altura de la ventana -->
			show: "scale", <!-- -----------> animación de la ventana al aparecer -->
			hide: "scale", <!-- -----------> animación al cerrar la ventana -->
			resizable: "false", <!-- ------> fija o redimensionable si ponemos este valor a "true" -->
			position: "top",<!--  ------> posicion de la ventana en la pantalla (left, top, right...) -->
			modal: "true" <!-- ------------> si esta en true bloquea el contenido de la web mientras la ventana esta activa (muy elegante) -->
		});
	}
    </script>
    <div name="todaslasnotas" id="todaslasnotas" class="ventana" title="Notas Agregadas">	
	</div>
	<script type="text/javascript" language="javascript">
	function VERADJUNTOSBITACORA()
	{
		$("#todoslosadjuntosbitacora").dialog({ <!--  ------> muestra la ventana  -->
			width: 590,  <!-- -------------> ancho de la ventana -->
			height: 350,<!--  -------------> altura de la ventana -->
			show: "scale", <!-- -----------> animación de la ventana al aparecer -->
			hide: "scale", <!-- -----------> animación al cerrar la ventana -->
			resizable: "false", <!-- ------> fija o redimensionable si ponemos este valor a "true" -->
			position: "top",<!--  ------> posicion de la ventana en la pantalla (left, top, right...) -->
			modal: "true" <!-- ------------> si esta en true bloquea el contenido de la web mientras la ventana esta activa (muy elegante) -->
		});
	}
    </script>
    <div name="todoslosadjuntosbitacora" id="todoslosadjuntosbitacora" class="ventana" title="Bitácora de adjuntos">	
	</div>
</form>
<script src="../../js/datatables/jquery.dataTables.js?<?php echo time();?>"></script>
<script src="../../js/datatables/extensions/Responsive/js/dataTables.responsive.js?<?php echo time()?>"></script>
</body>
</html>