<?php
include('../../data/Conexion.php');
require_once('../../Classes/PHPMailer-master/class.phpmailer.php');
header("Cache-Control: no-store, no-cache, must-revalidate");
session_start();
date_default_timezone_set('America/Bogota');
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
ini_set('memory_limit','500M');
ini_set('max_execution_time', 600);
ini_set('upload_max_filesize', '100M');
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];

$fecha=date("Ymd");
$fechaact=date("Y/m/d H:i:s");
$clacaj = $_POST['clacaj'];
$actor = $_POST['actor'];
$arc = $_POST['archivo'];
$claada = $_POST['claada'];
$max=8000000;//8MB

if($clacaj != '' and $clacaj != 0 and $actor != '')
{
	if(is_array($_FILES)) 
	{
		mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'4',5,'".$clacaj."','".$usuario."','".$fechaact."')");//Tercer campo tia_clave_int. 5=Adjuntar archivo
		
		$con = mysqli_query($conectar,"select MAX(ada_clave_int) max from adjunto_actor where caj_clave_int = '".$clacaj."'");
		$dato = mysqli_fetch_array($con);
		$num = $dato['max'];
		
		if($num == 0 || $num == '')
		{
			$num = 1;
		}
		else
		{
			$num = $num+1;
		}
		
		if($actor == 'INMOBILIARIA')
		{
			if(is_uploaded_file($_FILES['adjuntoinm']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntoinm']['tmp_name'];
				$archivo = basename($_FILES['adjuntoinm']['name']);
						
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
				$prefijo = "FACTURACION - INMOBILIARIA";
				$destino =  "../../adjuntos/facturacion/".$clacaj."-".$prefijo."".$num.".".$extension;
                $filesize = $_FILES['adjuntoinm']['size'];
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and caj_clave_int = '".$clacaj."' and tad_clave_int = '21' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "error4";
                }
                else
                    if($filesize>$max)
                    {
                        echo "error3";
                    }
                    else
                if(strtoupper($extension)=="DWG" || strtoupper($extension)=="RAR" || strtoupper($extension)=="ZIP")
                {
                    echo "error1";
                }
                else
                {
                    if(move_uploaded_file($sourcePath,$destino))
                    {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,caj_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'".$clacaj."','".$fechaact."','".$destino."','".$archivo."',21,'".$usuario."','".$fechaact."')");
                    }
                    else
                    {
                        echo "error2";
                    }
                }

			}
		}
		else
		if($actor == 'VISITA')
		{
			if(is_uploaded_file($_FILES['adjuntovis']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntovis']['tmp_name'];
				$archivo = basename($_FILES['adjuntovis']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
				$prefijo = "FACTURACION - VISITA";
				$destino =  "../../adjuntos/facturacion/".$clacaj."-".$prefijo."".$num.".".$extension;
                $filesize = $_FILES['adjuntovis']['size'];
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and caj_clave_int = '".$clacaj."' and tad_clave_int = '22' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "error4";
                }
                else
                    if($filesize>$max)
                    {
                        echo "error3";
                    }
                    else
                if(strtoupper($extension)=="DWG" || strtoupper($extension)=="RAR" || strtoupper($extension)=="ZIP")
                {
                    echo "error1";
                }
                else
                {
                    if(move_uploaded_file($sourcePath,$destino))
                    {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,caj_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'".$clacaj."','".$fechaact."','".$destino."','".$archivo."',22,'".$usuario."','".$fechaact."')");
                    }
                    else
                    {
                        echo "error2";
                    }
                }
			}
		}
		else
		if($actor == 'DISENO')
		{
			if(is_uploaded_file($_FILES['adjuntodis']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntodis']['tmp_name'];
				$archivo = basename($_FILES['adjuntodis']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
				$prefijo = "FACTURACION - DISENO";
				$destino =  "../../adjuntos/facturacion/".$clacaj."-".$prefijo."".$num.".".$extension;
                $filesize = $_FILES['adjuntodis']['size'];
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and caj_clave_int = '".$clacaj."' and tad_clave_int = '23' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "error4";
                }
                else
                    if($filesize>$max)
                    {
                        echo "error3";
                    }
                    else
                if(strtoupper($extension)=="DWG" || strtoupper($extension)=="RAR" || strtoupper($extension)=="ZIP")
                {
                    echo "error1";
                }
                else
                {
                    if(move_uploaded_file($sourcePath,$destino))
                    {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,caj_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'".$clacaj."','".$fechaact."','".$destino."','".$archivo."',23,'".$usuario."','".$fechaact."')");
                    }
                    else
                    {
                        echo "error2";
                    }
                }
			}
		}
		else
		if($actor == 'LICENCIA')
		{
			if(is_uploaded_file($_FILES['adjuntolic']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntolic']['tmp_name'];
				$archivo = basename($_FILES['adjuntolic']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
				$prefijo = "FACTURACION - LICENCIA";
				$destino =  "../../adjuntos/facturacion/".$clacaj."-".$prefijo."".$num.".".$extension;
                $filesize = $_FILES['adjuntolic']['size'];
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and caj_clave_int = '".$clacaj."' and tad_clave_int = '24' and ada_sw_eliminado = 0 and ada_clave_int!='".$claada."'");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "error4";
                }
                else
                    if($filesize>$max)
                    {
                        echo "error3";
                    }
                    else
                if(strtoupper($extension)=="DWG" || strtoupper($extension)=="RAR" || strtoupper($extension)=="ZIP")
                {
                    echo "error1";
                }
                else
                {
                    if(move_uploaded_file($sourcePath,$destino))
                    {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,caj_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'".$clacaj."','".$fechaact."','".$destino."','".$archivo."',24,'".$usuario."','".$fechaact."')");
                    }
                    else
                    {
                        echo "error2";
                    }
                }
			}
		}
		else
		if($actor == 'INTERVENTORIA')
		{
			if(is_uploaded_file($_FILES['adjuntoint']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntoint']['tmp_name'];
				$archivo = basename($_FILES['adjuntoint']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
				$prefijo = "FACTURACION - INTERVENTORIA";
				$destino =  "../../adjuntos/facturacion/".$clacaj."-".$prefijo."".$num.".".$extension;
                $filesize = $_FILES['adjuntoint']['size'];
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and caj_clave_int = '".$clacaj."' and tad_clave_int = '25' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "error4";
                }
                else
                    if($filesize>$max)
                    {
                        echo "error3";
                    }
                    else
                if(strtoupper($extension)=="DWG" || strtoupper($extension)=="RAR" || strtoupper($extension)=="ZIP")
                {
                    echo "error1";
                }
                else
                {
                    if(move_uploaded_file($sourcePath,$destino))
                    {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,caj_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'".$clacaj."','".$fechaact."','".$destino."','".$archivo."',25,'".$usuario."','".$fechaact."')");
                    }
                    else
                    {
                        echo "error2";
                    }
                }
			}
		}
		else
		if($actor == 'CONSTRUCTOR')
		{
			if(is_uploaded_file($_FILES['adjuntocon']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntocon']['tmp_name'];
				$archivo = basename($_FILES['adjuntocon']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
				$prefijo = "FACTURACION - CONSTRUCTOR";
				$destino =  "../../adjuntos/facturacion/".$clacaj."-".$prefijo."".$num.".".$extension;
                $filesize = $_FILES['adjuntocon']['size'];
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and caj_clave_int = '".$clacaj."' and tad_clave_int = '26' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "error4";
                }
                else
                    if($filesize>$max)
                    {
                        echo "error3";
                    }
                    else
                if(strtoupper($extension)=="DWG" || strtoupper($extension)=="RAR" || strtoupper($extension)=="ZIP")
                {
                    echo "error1";
                }
                else
                {
                    if(move_uploaded_file($sourcePath,$destino))
                {
                    $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,caj_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'".$clacaj."','".$fechaact."','".$destino."','".$archivo."',26,'".$usuario."','".$fechaact."')");
                }
                else
                {
                    echo "error2";
                }
                }
			}
		}
		else
		if($actor == 'SEGURIDAD')
		{
			if(is_uploaded_file($_FILES['adjuntoseg']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntoseg']['tmp_name'];
				$archivo = basename($_FILES['adjuntoseg']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
				$prefijo = "FACTURACION - SEGURIDAD";
				$destino =  "../../adjuntos/facturacion/".$clacaj."-".$prefijo."".$num.".".$extension;
                $filesize = $_FILES['adjuntoseg']['size'];
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and caj_clave_int = '".$clacaj."' and tad_clave_int = '27' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "error4";
                }
                else
                    if($filesize>$max)
                    {
                        echo "error3";
                    }
                    else
                if(strtoupper($extension)=="DWG" || strtoupper($extension)=="RAR" || strtoupper($extension)=="ZIP")
                {
                    echo "error1";
                }
                else
                {
                    if(move_uploaded_file($sourcePath,$destino))
                    {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,caj_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'".$clacaj."','".$fechaact."','".$destino."','".$archivo."',27,'".$usuario."','".$fechaact."')");
                    }
                    else
                    {
                        echo "error2";
                    }
                }
			}
		}
		else
		if($actor == 'ACTUALIZAARCHIVO')
		{
			if(is_uploaded_file($_FILES['archivoadjunto']['tmp_name'])) 
			{
				$sourcePath = $_FILES['archivoadjunto']['tmp_name'];
				$archivo = basename($_FILES['archivoadjunto']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				$con = mysqli_query($conectar,"select * from adjunto_actor where ada_clave_int = '".$claada."'");
				$dato = mysqli_fetch_array($con);
				$adj = $dato['ada_adjunto'];
				
				if($arc == 21)
				{
					$prefijo = "FACTURACION - INMOBILIARIA";
					$destino =  "../../adjuntos/visita/".$clacaj."-".$prefijo."".$claada.".".$extension;
				}
				else
				if($arc == 22)
				{
					$prefijo = "FACTURACION - VISITA";
					$destino =  "../../adjuntos/visita/".$clacaj."-".$prefijo."".$claada.".".$extension;
				}
				else
				if($arc == 23)
				{
					$prefijo = "FACTURACION - DISENO";
					$destino =  "../../adjuntos/visita/".$clacaj."-".$prefijo."".$claada.".".$extension;
				}
				else
				if($arc == 24)
				{
					$prefijo = "FACTURACION - LICENCIA";
					$destino =  "../../adjuntos/visita/".$clacaj."-".$prefijo."".$claada.".".$extension;
				}
				else
				if($arc == 25)
				{
					$prefijo = "FACTURACION - INTERVENTORIA";
					$destino =  "../../adjuntos/visita/".$clacaj."-".$prefijo."".$claada.".".$extension;
				}
				else
				if($arc == 26)
				{
					$prefijo = "FACTURACION - CONSTRUCTOR";
					$destino =  "../../adjuntos/visita/".$clacaj."-".$prefijo."".$claada.".".$extension;
				}
				else
				if($arc == 27)
				{
					$prefijo = "FACTURACION - SEGURIDAD";
					$destino =  "../../adjuntos/visita/".$clacaj."-".$prefijo."".$claada.".".$extension;
				}
                $filesize = $_FILES['archivoadjunto']['size'];
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and caj_clave_int = '".$clacaj."' and tad_clave_int = '".$arc."' and ada_sw_eliminado = 0 and ada_clave_int!='".$claada."'");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "error4";
                }
                else
                if($filesize>$max)
                {
                    echo "error3";
                }
                else
                if(strtoupper($extension)=="DWG" || strtoupper($extension)=="RAR" || strtoupper($extension)=="ZIP")
                {
                    echo "error1";
                }
                else
                {
                    if(move_uploaded_file($sourcePath,$destino))
                    {
                        $sql = mysqli_query($conectar,"update adjunto_actor set ada_usu_actualiz = '".$usuario."',ada_fec_actualiz = '".$fechaact."', ada_adjunto = '".$destino."', ada_nombre_adjunto = '".$archivo."' where ada_clave_int = '".$claada."'");
                    }
                    else
                    {
                        echo "error2";
                    }
                }
			}
		}
	}
	/*
	$conhij = mysqli_query($conectar,"select * from cajero where caj_padre = ".$clacaj."");
	$numhij = mysqli_num_rows($conhij);
	for($i = 0; $i < $numhij; $i++)
	{
		$datohij = mysqli_fetch_array($conhij);
		$clahij = $datohij['caj_clave_int'];
		
		mysqli_query($conectar,"delete from adjunto_actor where caj_clave_int = ".$clahij."");
		mysqli_query($conectar,"insert into adjunto_actor select null,".$clahij.",ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_sw_eliminado,ada_usu_actualiz,ada_fec_actualiz from adjunto_actor where caj_clave_int = ".$clacaj."");
	}*/
}
?>