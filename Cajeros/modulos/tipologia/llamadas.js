function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function EDITAR(v,m)
{	
	if(m == 'TIPOLOGIA')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('editarmitipologia').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#editarmitipologia").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?editartip=si&tipedi="+v,true);
		ajax.send(null);
	}
}
function GUARDAR(v,id)
{
	if(v == 'TIPOLOGIA')
	{
		var tip = form1.tipologia1.value;
		var lt = tip.length;
		var act = form1.activo1.checked;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('datos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#datos").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?guardartip=si&tip="+tip+"&act="+act+"&lt="+lt+"&t="+id,true);
		ajax.send(null);
		if(tii != '' && lt >= 3)
		{
			setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
		}
	}
}
function NUEVO(v)
{
	if(v == 'TIPOLOGIA')
	{
		var tip = form1.txttipologia.value;
		var lt = tip.length;
		var act = form1.activo.checked;
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('datos1').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#datos1").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?nuevotipologia=si&tip="+tip+"&act="+act+"&lt="+lt,true);
		ajax.send(null);
		if(tii != '' && lt >= 3)
		{
			//form1.proceso.value = '';
			setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
		}
	}
}
function CONSULTAMODULO(v)
{
	if(v == 'TODOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('tipologia').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#tipologia").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?todos=si",true);
		ajax.send(null);
	}
}
function BUSCAR(m)
{	
	if(m == 'TIPOLOGIA')
	{
		var tip = form1.tipologia2.value;
		var act = form1.buscaractivos.value;
				
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('tipologia').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#tipologia").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?buscartip=si&tip="+tip+"&act="+act,true);
		ajax.send(null);
	}
}
function AGREGARPERMISOS(v)
{
	var ven = $("#agregarven").val();
	
	if(ven != '')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('agregados').innerHTML=ajax.responseText;
		    }
		}
		
		jQuery("#agregados").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?agregarseleccionados=si&ven="+ven+"&per="+v,true);
		ajax.send(null);
		
		for (x=0;x<ven.length;x++)
		{
			$("#agregarven").find("option[value="+ven[x]+"]").remove();
		}
	}
	else
	{
		alert("Por favor seleccione almenos un registros.");
	}
}
function AGREGARTODOS(v)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('agregados').innerHTML=ajax.responseText;
	    }
	}
	
	jQuery("#agregados").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?agregartodos=si&per="+v,true);
	ajax.send(null);
	$('#agregarven').html('');//Limpia todos los datos del select
}
function ELIMINARPERMISO(v)
{	
	var ven1 = $("#agregarven1").val();
	
	if(ven1 != '')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('agregar1').innerHTML=ajax.responseText;
		    }
		}
		
		//jQuery("#agregados").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?eliminaragregados=si&permiso="+ven1+"&per="+v,true);
		ajax.send(null);
		
		for (x=0;x<ven1.length;x++)
		{
			$("#agregarven1").find("option[value="+ven1[x]+"]").remove();
		}
	}
	else
	{
		alert("Por favor seleccione almenos un registros.");
	}
}
function QUITARTODOS(v)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('agregar1').innerHTML=ajax.responseText;
	    }
	}
	
	jQuery("#agregar1").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?eliminartodos=si&per="+v,true);
	ajax.send(null);
	$('#agregarven1').html('');//Limpia todos los datos del select
}
function MOSTRARMOVIMIENTO(ci,c)
{
	var div = document.getElementById('movimiento'+c);
    div.style.display = 'block';
	    
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
			document.getElementById('movimiento'+c).innerHTML=ajax.responseText;
	    }
	}
	//jQuery("#agregados").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarmovimiento=si&ci="+ci+"&c="+c,true);
	ajax.send(null);
}
function OCULTAR(c)
{
	var div = document.getElementById('movimiento'+c);
    div.style.display = 'none';
}
function CREARNUEVOTIPOINTERVENCION()
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
			document.getElementById('crearnuevotipologia').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#crearnuevotipologia").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?crearnuevotipologia=si",true);
	ajax.send(null);
}