<?php
	include('../../data/Conexion.php');
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_COOKIE['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];
	
	//validacion inactividad en el aplicativo
	if(isset($_SESSION['nw']))
    {
        if($_SESSION['nw']<time())
        {
            unset($_SESSION['nw']);
            //echo "<script>alert('Tiempo agotado - Loguearse nuevamente');</script>";
            echo "<script>alert('Tu sesión se cerrara por inactividad en el sistema - Loguearse nuevamente');</script>";
			echo "<script>parent.location.href=parent.location.href</script>";
 //header("LOCATION:../../data/logout.php");
        }
        else
        {
            $_SESSION['nw'] = time() + (60 * 60);
        }
    }
    else
    {
       //echo "<script>alert('Tiempo agotado - Loguearse nuevamente');</script>";
       	echo "<script>alert('Tu sesión se cerrara por inactividad en el sistema - Loguearse nuevamente');</script>";
		echo "<script>parent.location.href=parent.location.href</script>";
 		//header("LOCATION:../../data/logout.php");
    }
    
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");
	
	$con = mysqli_query($conectar,"select u.usu_clave_int,p.prf_clave_int,p.prf_descripcion from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
	$dato = mysqli_fetch_array($con);
	$claprf = $dato['prf_clave_int'];
	
	$con = mysqli_query($conectar,"select per_metodo from permiso where prf_clave_int = '".$claprf."' and ven_clave_int = 12");
	$dato = mysqli_fetch_array($con);
	$metodo = $dato['per_metodo'];
	
	if($_GET['editartip'] == 'si')
	{
		$tipedi = $_GET['tipedi'];
		$con = mysqli_query($conectar,"select * from tipologia where tip_clave_int = '".$tipedi."'");
		$dato = mysqli_fetch_array($con); 
		$tip = $dato['tip_nombre'];
		$act = $dato['tip_sw_activo'];
?>
		<table style="width: 38%" align="center">
			<tr>
				<td>&nbsp;</td>
				<td class="auto-style1">
				<table style="width: 38%" align="center">
			<tr>
				<td>&nbsp;</td>
				<td class="auto-style3" align="left">Tipología:</td>
				<td class="auto-style3">
				<input name="tipologia1" id="tipologia1" value="<?php echo $tip; ?>" class="inputs" type="text" style="width: 200px" />&nbsp;
				</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td class="auto-style3" colspan="2" align="left">Activa:<input class="inputs" <?php if($act == 1){ echo 'checked="checked"'; } ?> name="activo1" type="checkbox" /></td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="4">
				<input name="submit" type="button" value="Guardar" onclick="GUARDAR('TIPOLOGIA','<?php echo $tipedi; ?>')"  style="width: 348px; height: 25px; cursor:pointer" /></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td colspan="2">
				<div id="datos">
				</div>
				</td>
				<td>&nbsp;</td>
			</tr>
			</table>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
			<table style="width: 38%" align="center">
				<tr>
					<td>&nbsp;</td>
					<td align="center" colspan="3">
					<?php echo "<div class='ok' style='width: 99%' align='center'>Tipologia: $tip</div>"; ?>
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td align="center">
					<div id="agregar1" align="center">
						<fieldset name="Group1" style="height: 320px">
						<legend>Registros Sin Seleccionar</legend>
							<select name="agregarven" id="agregarven" multiple="multiple" ondblclick="AGREGARPERMISOS('<?php echo $tipedi; ?>')" style="width: 300px; height: 300px;" size="8">
							<?php
								$con = mysqli_query($conectar,"select tit_clave_int,tit_nombre,tit_dias from tipotrabajo where tit_clave_int not in (select tit_clave_int from tipologia_trabajo where tip_clave_int = '".$tipedi."') order by tit_nombre");
								$num = mysqli_num_rows($con);
								for($i = 0; $i < $num; $i++)
								{
									$dato = mysqli_fetch_array($con);
									$clave = $dato['tit_clave_int'];
									$nom = $dato['tit_nombre'];
									$dia = $dato['tit_dias'];
							?>
								<option value="<?php echo $clave; ?>"><?php echo $nom." Duración: ".$dia." dias"; ?></option>
							<?php
								}
							?>
							</select>
						</fieldset>
					</div>
					</td>
					<td align="center">
					<div style="width: 140px">
						<input type="button" class="pasar izq" onclick="AGREGARPERMISOS('<?php echo $tipedi; ?>')" value="Pasar &raquo;"><input type="button" onclick="ELIMINARPERMISO('<?php echo $tipedi; ?>')" class="quitar der" value="&laquo; Quitar"><br />
						<input type="button" class="pasartodos izq" onclick="AGREGARTODOS('<?php echo $tipedi; ?>')" value="Todos &raquo;"><input type="button" onclick="QUITARTODOS('<?php echo $tipedi; ?>')" class="quitartodos der" value="&laquo; Todos">
					</div>
					</td>
					<td align="center">
					<div id="agregados">
						<fieldset name="Group1" style="height: 320px">
							<legend>Registros Seleccionados</legend>
							<select name="agregarven1" id="agregarven1" multiple="multiple" ondblclick="ELIMINARPERMISO('<?php echo $tipedi; ?>')" style="width: 300px; height: 300px;" size="8">
							<?php
								$con = mysqli_query($conectar,"select pa.ttr_clave_int cla,ma.tit_nombre nom,ma.tit_dias di from tipologia_trabajo pa inner join tipotrabajo ma on (ma.tit_clave_int = pa.tit_clave_int) where pa.tip_clave_int = '".$tipedi."'");
								$num = mysqli_num_rows($con);
								for($i = 0; $i < $num; $i++)
								{
									$dato = mysqli_fetch_array($con);
									$clave = $dato['cla'];
									$nom = $dato['nom'];
									$dia = $dato['di'];
							?>
								<option value="<?php echo $clave; ?>"><?php echo $nom." Duración: ".$dia." dias"; ?></option>
							<?php
								}
							?>
							</select>
					</fieldset>
					</div>
					</td>
					<td>&nbsp;</td>
				</tr>
				</table>
			</td>
			<td>&nbsp;</td>
		</tr>
	</table>
<?php
		exit();
	}
	
	if($_GET['guardartip'] == 'si')
	{
		sleep(1);
		$tip = $_GET['tip'];
		$act = $_GET['act'];
		$lt = $_GET['lt'];
		$t = $_GET['t'];
		
		$sql = mysqli_query($conectar,"select * from tipologia where (UPPER(tip_nombre) = UPPER('".$tip."')) AND tip_clave_int <> '".$t."'");
		$dato = mysqli_fetch_array($sql);
		$contip = $dato['tip_nombre'];
		
		if($tip == '')
		{
			echo "<div class='validaciones'>Debe ingresar el campo Tipología</div>";
		}
		else
		if(STRTOUPPER($contip) == STRTOUPPER($tip))
		{
			echo "<div class='validaciones'>La tipología ingresada ya existe</div>";
		}
		else
		if($lt < 3)
		{
			echo "<div class='validaciones'>El campo tipología debe ser mí­nimo de 3 dijitos</div>";
		}
		else
		{			
			if($act == 'false')
			{
				$swact = 0;
			}
			else
			if($act == 'true')
			{
				$swact = 1;
			}
			
			$con = mysqli_query($conectar,"update tipologia set tip_nombre = '".$tip."', tip_sw_activo = '".$swact."', tip_usu_actualiz = '".$usuario."', tip_fec_actualiz = '".$fecha."' where tip_clave_int = '".$t."'");
			
			if($con >= 1)
			{
				echo "<div class='ok'>Datos grabados correctamente</div>";
				mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'12',36,'".$t."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 17=Actualización Tipo Intervención
			}
			else
			{
				echo "<div class='validaciones'>No se han podido guardar los datos</div>";
			}	
		}
		exit();
	}
	
	if($_GET['nuevotipologia'] == 'si')
	{
		sleep(1);
		$fecha=date("Y/m/d H:i:s");
		$tip = $_GET['tip'];
		$act = $_GET['act'];
		$lt = $_GET['lt'];
		
		$sql = mysqli_query($conectar,"select * from tipologia where (UPPER(tip_nombre) = UPPER('".$tip."'))");
		$dato = mysqli_fetch_array($sql);
		$contip = $dato['tip_nombre'];
		
		if($tip == '')
		{
			echo "<div class='validaciones'>Debe ingresar el campo Tipología</div>";
		}
		else
		if(STRTOUPPER($contip) == STRTOUPPER($tip))
		{
			echo "<div class='validaciones'>El campo tipología ingresada ya existe</div>";
		}
		else
		if($lt < 3)
		{
			echo "<div class='validaciones'>El campo tipología debe ser mí­nimo de 3 dijitos</div>";
		}
		else
		{
			if($act == 'false')
			{
				$swact = 0;
			}
			else
			if($act == 'true')
			{
				$swact = 1;
			}
			$con = mysqli_query($conectar,"insert into tipologia(tip_nombre,tip_sw_activo,tip_usu_actualiz,tip_fec_actualiz) values('".$tip."','".$swact."','".$usuario."','".$fecha."')");
			
			if($con >= 1)
			{
				echo "<div class='ok'>Datos grabados correctamente</div>";
				$con = mysqli_query($conectar,"select max(tip_clave_int) max from tipologia");
				$dato = mysqli_fetch_array($con);
				$max = $dato['max'];
				mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'12',35,'".$max."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 16=Creación Tipo Intervención
			?>
				<table style="width: 38%" align="center">
				<tr>
					<td>&nbsp;</td>
					<td align="center" colspan="3">
					<?php echo "<div class='ok' style='width: 99%' align='center'>Tipología: $tip</div>"; ?>
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td align="center">
					<div id="agregar1" align="center">
						<fieldset name="Group1" style="height: 320px">
						<legend>Registros Sin Seleccionar</legend>
							<select name="agregarven" id="agregarven" multiple="multiple" ondblclick="AGREGARPERMISOS('<?php echo $max; ?>')" style="width: 300px; height: 300px;" size="8">
							<?php
								$con = mysqli_query($conectar,"select tit_clave_int,tit_nombre,tit_dias from tipotrabajo where tit_clave_int not in (select tit_clave_int from tipologia_trabajo where tip_clave_int = '".$max."') order by tit_nombre");
								$num = mysqli_num_rows($con);
								for($i = 0; $i < $num; $i++)
								{
									$dato = mysqli_fetch_array($con);
									$clave = $dato['tit_clave_int'];
									$nom = $dato['tit_nombre'];
									$dia = $dato['tit_dias'];
							?>
								<option value="<?php echo $clave; ?>"><?php echo $nom." Duración: ".$dia." dias"; ?></option>
							<?php
								}
							?>
							</select>
						</fieldset>
					</div>
					</td>
					<td align="center">
					<div style="width: 140px">
						<input type="button" class="pasar izq" onclick="AGREGARPERMISOS('<?php echo $max; ?>')" value="Pasar &raquo;"><input type="button" onclick="ELIMINARPERMISO('<?php echo $max; ?>')" class="quitar der" value="&laquo; Quitar"><br />
						<input type="button" class="pasartodos izq" onclick="AGREGARTODOS('<?php echo $max; ?>')" value="Todos &raquo;"><input type="button" onclick="QUITARTODOS('<?php echo $max; ?>')" class="quitartodos der" value="&laquo; Todos">
					</div>
					</td>
					<td align="center">
					<div id="agregados">
						<fieldset name="Group1" style="height: 320px">
							<legend>Registros Seleccionados</legend>
							<select name="agregarven1" id="agregarven1" multiple="multiple" ondblclick="ELIMINARPERMISO('<?php echo $max; ?>')" style="width: 300px; height: 300px;" size="8">
							<?php
								$con = mysqli_query($conectar,"select pa.ttr_clave_int cla,ma.tit_nombre nom,ma.tit_dias di from tipologia_trabajo pa inner join tipotrabajo ma on (ma.tit_clave_int = pa.tit_clave_int) where pa.tip_clave_int = '".$max."'");
								$num = mysqli_num_rows($con);
								for($i = 0; $i < $num; $i++)
								{
									$dato = mysqli_fetch_array($con);
									$clave = $dato['cla'];
									$nom = $dato['nom'];
									$di = $dato['di'];
							?>
								<option value="<?php echo $clave; ?>"><?php echo $nom. "Duración: ".$di." dias"; ?></option>
							<?php
								}
							?>
							</select>
						</fieldset>
					</div>
					</td>
					<td>&nbsp;</td>
				</tr>
				</table>
			<?php
			}
			else
			{
				echo "<div class='validaciones'>No se han podido guardar los datos</div>";
			}
		}
		exit();
	}
	if($_GET['todos'] == 'si')
	{
		sleep(1);
		$rows=mysqli_query($conectar,"select * from tipologia");
		$total=mysqli_num_rows($rows);
?>
		<table style="width: 100%">
		<tr>
			<td class="auto-style3" style="width: 27px">
				<input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<?php echo $total;?>,this);" class="auto-style6" /><span class="auto-style6">
				</span>
			</td>
			<td class="auto-style3" colspan="5">
			<?php
			if($metodo == 1)
			{
			?>
			<table style="width: 30%">
				<tr>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/activo.png" alt="" class="auto-style6" /><input type="submit" value="Activar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/inactivo.png" alt="" class="auto-style6" /><input type="submit" value="Inactivar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/eliminar.png" alt="" class="auto-style6" /><input type="submit" value="Eliminar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
				</tr>
			</table>
			<?php
			}
			?>
			</td>
		</tr>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px"><strong>Tipología</strong></td>
			<td class="auto-style5" style="width: 98px"><strong>Usuario</strong></td>
			<td class="auto-style5" style="width: 127px"><strong>
				Actualización</strong></td>
			<td class="auto-style5" style="width: 29px"><strong>
			Activo</strong></td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
		<?php
			$contador=0;
			$con = mysqli_query($conectar,"select * from tipologia order by tip_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clatip = $dato['tip_clave_int'];
				$tip = $dato['tip_nombre'];
				$act = $dato['tip_sw_activo'];
				$usuact = $dato['tip_usu_actualiz'];
				$fecact = $dato['tip_fec_actualiz'];
				$contador=$contador+1;
		?>
		<tr style="cursor:pointer;background-color:#eeeeee" onmouseover="this.style.backgroundColor='#D7D7D7';this.style.color='#000000';" onmouseout="this.style.backgroundColor='#eeeeee';this.style.color='#000000';" onclick="MOSTRARMOVIMIENTO('<?php echo $clatip; ?>','<?php echo $i; ?>');OCULTARSCROLL()">
			<td class="auto-style3" style="width: 27px">
				<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<?php echo $contador;?>" value="<?php echo $dato['tip_clave_int'];?>" class="auto-style6" /></td>
			<td class="auto-style5" style="width: 200px"><?php echo $tip; ?></td>
			<td class="auto-style5" style="width: 98px"><?php echo $usuact; ?></td>
			<td class="auto-style5" style="width: 127px"><?php echo $fecact; ?></td>
			<td class="auto-style1" style="width: 30px">
			<input name="activarinactivar" id="activarinactivar" type="checkbox" <?php if($act == 1){ echo 'checked="checked"'; } ?> disabled="disabled" class="auto-style6" ></td>
			<td class="auto-style5">
			<?php
			if($metodo == 1)
			{
			?>
			<a data-reveal-id="editartipologia" data-animation="fade" style="cursor:pointer" onclick="EDITAR('<?php echo $dato['tip_clave_int']; ?>','TIPOLOGIA')"><img src="../../img/editar.png" alt="" height="22" width="21" /></a>
			<?php
			}
			?>
			</td>
		</tr>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" colspan="5">
			<div id="movimiento<?php echo $i; ?>" align="left">
			</div>
			</td>
		</tr>
		<?php
			}
		?>
	</table>
<?php
		exit();
	}
?>
<?php
	if($_GET['buscartip'] == 'si')
	{
		$tip = $_GET['tip'];
		$act = $_GET['act'];

		$rows=mysqli_query($conectar,"select * from tipologia where (tip_nombre LIKE REPLACE('%".$tip."%',' ','%') OR '".$tip."' IS NULL OR '".$tip."' = '') and (tip_sw_activo = '".$act."' OR '".$act."' IS NULL OR '".$act."' = '')");
		$total=mysqli_num_rows($rows);
?>
		<table style="width: 100%">
		<tr>
			<td class="auto-style3" style="width: 27px">
				<input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<?php echo $total;?>,this);" class="auto-style6" /><span class="auto-style6">
				</span>
			</td>
			<td class="auto-style3" colspan="5">
			<?php
			if($metodo == 1)
			{
			?>
			<table style="width: 30%">
				<tr>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/activo.png" alt="" class="auto-style6" /><input type="submit" value="Activar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/inactivo.png" alt="" class="auto-style6" /><input type="submit" value="Inactivar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/eliminar.png" alt="" class="auto-style6" /><input type="submit" value="Eliminar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
				</tr>
			</table>
			<?php
			}
			?>
			</td>
		</tr>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px"><strong>Tipo Intervención</strong></td>
			<td class="auto-style5" style="width: 98px"><strong>Usuario</strong></td>
			<td class="auto-style5" style="width: 127px"><strong>
				Actualización</strong></td>
			<td class="auto-style5" style="width: 29px"><strong>
			Activo</strong></td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
		<?php
			$contador=0;
			$con = mysqli_query($conectar,"select * from tipologia where (tip_nombre LIKE REPLACE('%".$tip."%',' ','%') OR '".$tip."' IS NULL OR '".$tip."' = '') and (tip_sw_activo = '".$act."' OR '".$act."' IS NULL OR '".$act."' = '') order by tip_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clatip = $dato['tip_clave_int'];
				$tip = $dato['tip_nombre'];
				$act = $dato['tip_sw_activo'];
				$usuact = $dato['tip_usu_actualiz'];
				$fecact = $dato['tip_fec_actualiz'];
				$contador=$contador+1;
		?>
		<tr style="cursor:pointer;background-color:#eeeeee" onmouseover="this.style.backgroundColor='#D7D7D7';this.style.color='#000000';" onmouseout="this.style.backgroundColor='#eeeeee';this.style.color='#000000';" onclick="MOSTRARMOVIMIENTO('<?php echo $clatip; ?>','<?php echo $i; ?>');OCULTARSCROLL()">
			<td class="auto-style3" style="width: 27px">
				<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<?php echo $contador;?>" value="<?php echo $dato['tip_clave_int'];?>" class="auto-style6" /></td>
			<td class="auto-style5" style="width: 200px"><?php echo $tip; ?></td>
			<td class="auto-style5" style="width: 98px"><?php echo $usuact; ?></td>
			<td class="auto-style5" style="width: 127px"><?php echo $fecact; ?></td>
			<td class="auto-style1" style="width: 30px">
			<input name="activarinactivar" id="activarinactivar" type="checkbox" <?php if($act == 1){ echo 'checked="checked"'; } ?> disabled="disabled" class="auto-style6" ></td>
			<td class="auto-style5">
			<?php
			if($metodo == 1)
			{
			?>
			<a data-reveal-id="editartipologia" data-animation="fade" style="cursor:pointer" onclick="EDITAR('<?php echo $dato['tip_clave_int']; ?>','TIPOLOGIA')"><img src="../../img/editar.png" alt="" height="22" width="21" /></a>
			<?php
			}
			?>
			</td>
		</tr>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" colspan="5">
			<div id="movimiento<?php echo $i; ?>" align="left">
			</div>
			</td>
		</tr>
		<?php
			}
		?>
	</table>
<?php
		exit();
	}
	if($_GET['agregarseleccionados'] == 'si')
	{
		$ven = $_GET['ven'];
		$per = $_GET['per'];
		$fecha=date("Y/m/d H:i:s");
		
		$seleccionados = explode(',',$ven);
		
		$num = count($seleccionados);
		
		for($i = 0; $i < $num; $i++)
		{
			$con = mysqli_query($conectar,"insert into tipologia_trabajo(ttr_clave_int,tip_clave_int,tit_clave_int,ttr_usu_actualiz,ttr_fec_actualiz) values(null,'".$per."','".$seleccionados[$i]."','".$usuario."','".$fecha."')");
		}
		
		//mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,1,8,'".$per."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 8=Actualización perfil
?>
		<fieldset name="Group1" style="height: 320px">
			<legend>Registros Seleccionados</legend>
			<select name="agregarven1" id="agregarven1" multiple="multiple" ondblclick="ELIMINARPERMISO('<?php echo $per; ?>')" style="width: 300px; height: 300px;" size="8">
			<?php
				$con = mysqli_query($conectar,"select pa.ttr_clave_int cla,ma.tit_nombre nom,ma.tit_dias di from tipologia_trabajo pa inner join tipotrabajo ma on (ma.tit_clave_int = pa.tit_clave_int) where pa.tip_clave_int = '".$per."'");
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$clave = $dato['cla'];
					$nom = $dato['nom'];
					$di = $dato['di'];
			?>
				<option value="<?php echo $clave; ?>"><?php echo $nom." Duración:" . $di." dias"; ?></option>
			<?php
				}
			?>
			</select>
		</fieldset>
<?php
		exit();
	}
	if($_GET['agregartodos'] == 'si')
	{
		$per = $_GET['per'];
		$fecha=date("Y/m/d H:i:s");
		
		$con = mysqli_query($conectar,"insert into tipologia_trabajo select null,'".$per."',tit_clave_int,'".$usuario."','".$fecha."' from tipotrabajo where tit_clave_int not in (select tit_clave_int from tipologia_trabajo where tip_clave_int = '".$per."')");
		//mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,1,8,'".$per."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 8=Actualización perfil
?>
		<fieldset name="Group1" style="height: 320px">
			<legend>Registros Seleccionados</legend>
			<select name="agregarven1" id="agregarven1" multiple="multiple" ondblclick="ELIMINARPERMISO('<?php echo $max; ?>')" style="width: 300px; height: 300px;" size="8">
			<?php
				$con = mysqli_query($conectar,"select pa.ttr_clave_int cla,ma.tit_nombre nom,ma.tit_dias di from tipologia_trabajo pa inner join tipotrabajo ma on (ma.tit_clave_int = pa.tit_clave_int) where pa.tip_clave_int = '".$per."'");
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$clave = $dato['cla'];
					$nom = $dato['nom'];
					$di = $dato['di'];

			?>
				<option value="<?php echo $clave; ?>"><?php echo $nom. "Duración:".$di." dias"; ?></option>
			<?php
				}
			?>
			</select>
		</fieldset>
<?php
		exit();
	}
	if($_GET['eliminaragregados'] == 'si')
	{
		$perm = $_GET['permiso'];
		$per = $_GET['per'];
		$seleccionados = explode(',',$perm);
		
		$num = count($seleccionados);
		
		for($i = 0; $i < $num; $i++)
		{
			if($seleccionados[$i] != '')
			{
				$con = mysqli_query($conectar,"delete from tipologia_trabajo where ttr_clave_int = '".$seleccionados[$i]."'");
			}
		}
?>
		<fieldset name="Group1" style="height: 320px">
		<legend>Registros Sin Seleccionar</legend>
			<select name="agregarven" id="agregarven" multiple="multiple" ondblclick="AGREGARPERMISOS('<?php echo $per; ?>')" style="width: 300px; height: 300px;" size="8">
			<?php
				$con = mysqli_query($conectar,"select tit_clave_int,tit_nombre,tit_dias from tipotrabajo where tit_clave_int not in (select tit_clave_int from tipologia_trabajo where tip_clave_int = '".$per."') order by tit_nombre");
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$clave = $dato['tit_clave_int'];
					$nom = $dato['tit_nombre'];
					$dia = $dato['tit_dias'];
			?>
				<option value="<?php echo $clave; ?>"><?php echo $nom." Duración: ".$dia." dias"; ?></option>
			<?php
				}
			?>
			</select>
		</fieldset>
<?php
		exit();
	}
	if($_GET['eliminartodos'] == 'si')
	{
		$per = $_GET['per'];
		$con = mysqli_query($conectar,"delete from tipologia_trabajo where tip_clave_int = '".$per."'");
?>
		<fieldset name="Group1" style="height: 320px">
		<legend>Registros Sin Seleccionar</legend>
			<select name="agregarven" id="agregarven" multiple="multiple" ondblclick="AGREGARPERMISOS('<?php echo $per; ?>')" style="width: 300px; height: 300px;" size="8">
			<?php
				$con = mysqli_query($conectar,"select tit_clave_int,tit_nombre,tit_dias from tipotrabajo where tit_clave_int not in (select tit_clave_int from tipologia_trabajo where tip_clave_int = '".$per."') order by tit_nombre");
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$clave = $dato['tit_clave_int'];
					$nom = $dato['tit_nombre'];
					$dia = $dato['tit_dias']
			?>
				<option value="<?php echo $clave; ?>"><?php echo $nom." Duración: ".$dia." dias"; ?></option>
			<?php
				}
			?>
			</select>
		</fieldset>
<?php
		exit();
	}
	if($_GET['mostrarmovimiento'] == 'si')
	{
		$claveint = $_GET['ci'];
		$c = $_GET['c'];
		
		$con = mysqli_query($conectar,"select ma.tit_nombre,ma.tit_sw_activo,ma.tit_dias from tipologia_trabajo pa inner join tipotrabajo ma on (ma.tit_clave_int = pa.tit_clave_int) where pa.tip_clave_int = '".$claveint."'");
		$num = mysqli_num_rows($con);
?>
		<fieldset name="Group1" style="width:763px;resize:both" align="left">
			<legend align="center"><strong>TIPO TRABAJOS</strong><br><img src="../../images/ver.png" alt="" style="cursor:pointer" height="20" width="26" title="Ocultar" onclick="OCULTAR('<?php echo $c; ?>')" /></legend>
<?php
		for($i = 0; $i < $num; $i++)
		{
			$dato = mysqli_fetch_array($con);
			$nom = $dato['tit_nombre'];
			$act = $dato['tit_sw_activo'];
			$dia = $dato['tit_dias']
?>
		<table style="width: 763px" align="left">
			<tr>
				<td style="width: 9px"><strong>Tipo trabajo:</strong></td>
				<td class="auto-style13" style="width: 200px"><?php echo $nom; ?></td>
                <td style="width: 9px"><strong>Duración(dias) :</strong></td>
                <td class="auto-style13" style="width: 200px"><?php echo $dia; ?></td>
				<td style="width: 9px"><strong>Activa:</strong></td>
				<td class="auto-style13" style="width: 231px"><?php if($act == 1){ echo "Si"; }else{ echo "No"; } ?></td>
			</tr>
		</table>
<?php
		}
?>
		</fieldset>
<?php
		exit();
	}
	if($_GET['crearnuevotipologia'] == 'si')
	{
	?>
		<table style="width: 38%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style1">
			<table style="width: 38%" align="center">
				<tr>
					<td>&nbsp;</td>
					<td align="left">Tipología:</td>
					<td align="left">
					<input name="txttipologia" align="left" id="txttipologia"  type="text" style="width: 200px" class="inputs" />
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td align="left">Activo:</td>
					<td align="left"><input class="inputs" align="left" name="activo" checked="checked" type="checkbox" /></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="4">
					<input name="nuevo1" type="button" value="Guardar" onclick="NUEVO('TIPOLOGIA')"  style="width: 348px; height: 25px; cursor:pointer" /></td>
				</tr>
				</table>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
			<div id="datos1">
			<table style="width: 38%" align="center">
			<tr>
				<td>&nbsp;</td>
				<td align="center">
				<div id="agregar1" align="center">
					<fieldset name="Group1" style="height: 320px">
					<legend>Registros Sin Seleccionar</legend>
						<select name="agregarven" id="agregarven" disabled="disabled" multiple="multiple" ondblclick="AGREGARPERMISOS('<?php echo $per; ?>')" style="width: 300px; height: 300px;" size="8">
						<?php
							$con = mysqli_query($conectar,"select tit_clave_int,tit_nombre,tit_dias from tipotrabajo order by tit_nombre");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$clave = $dato['tit_clave_int'];
								$nom = $dato['tit_nombre'];
								$dia = $dato['tit_dias'];
						?>
							<option value="<?php echo $clave; ?>"><?php echo $nom." Duración: ".$dia." dias"; ?></option>
						<?php
							}
						?>
						</select>
					</fieldset>
				</div>
				</td>
				<td align="center">
				<div style="width: 140px">
					<input type="button" class="pasar izq" disabled="disabled" value="Pasar &raquo;"><input type="button" disabled="disabled" class="quitar der" value="&laquo; Quitar"><br />
					<input type="button" class="pasartodos izq" disabled="disabled" value="Todos &raquo;"><input type="button" disabled="disabled" class="quitartodos der" value="&laquo; Todos">
				</div>
				</td>
				<td align="center">
				<div id="agregados">
					<fieldset name="Group1" style="height: 320px">
						<legend>Registros Seleccionados</legend>
						<select name="agregarven1" id="agregarven1" disabled="disabled" multiple="multiple" ondblclick="ELIMINARPERMISO('<?php echo $tipedi; ?>')" style="width: 300px; height: 300px;" size="8">
						</select>							
					</fieldset>
				</div>
				</td>
				<td>&nbsp;</td>
			</tr>
			</table>
			</div>
			</td>
			<td>&nbsp;</td>
		</tr>
	</table>
	<?php
		exit();
	}
?>
<!DOCTYPE HTML>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

	
<title>CONTROL DE OBRAS</title>
<meta name="description" content="Service Desk">
<meta name="author" content="InvGate S.R.L.">

<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
<!--[if lte IE 7]>
<link rel="stylesheet" href="css/ie.c32bc18afe0e2883ee4912a51f86c119.css" type="text/css" />
<![endif]-->

<style type="text/css">
.auto-style1 {
	text-align: center;
}
.auto-style2 {
	font-size: 12px;
	font-family: Arial, helvetica;
	outline: none;
/*transition: all 0.75s ease-in-out;*/ /*-webkit-transition: all 0.75s ease-in-out;*/ /*-moz-transition: all 0.75s ease-in-out;*/border-radius: 3px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 3px;
	border: 1px solid rgba(0,0,0, 0.2);
	background-color: #eee;
	padding: 3px;
	box-shadow: 0 0 10px #aaa;
	-webkit-box-shadow: 0 0 10px #aaa;
	-moz-box-shadow: 0 0 10px #aaa;
	border: 1px solid #999;
	background-color: white;
	text-align: center;
}
.auto-style3 {
	text-align: left;
}
.inputs{
	font-size:14px;
    font-family: Arial, helvetica;
    outline:none;
    border-radius:3px;
    -webkit-border-radius:6px;
    -moz-border-radius:3px;
    border:1px solid rgba(0,0,0, 0.5);
    padding: 3px;
}
.validaciones{
	font-size:14px;
    font-family: Arial, helvetica;
    outline:none;
    border-radius:3px;
    -webkit-border-radius:6px;
    -moz-border-radius:3px;
    border:1px solid rgba(0,0,0, 0.2);
    color:maroon;
    background-color:#eee;
    padding: 3px;
    text-align:center;
}
.ok{
	font-size:14px;
    font-family: Arial, helvetica;
    outline:none;
    border-radius:3px;
    -webkit-border-radius:6px;
    -moz-border-radius:3px;
    border:1px solid rgba(0,0,0, 0.2);
    color:green;
    background-color:#eee;
    padding: 3px;
    text-align:center;
}
.auto-style5 {
	text-align: left;
	font-family: Arial, Helvetica, sans-serif;
}
.auto-style6 {
	font-family: Arial, Helvetica, sans-serif;
}
.auto-style7 {
	text-align: center;
	font-size: large;
}
</style>

<script type="text/javascript" language="javascript">
	selecteds=0;
	
	function CheckUncheck(total,check){
		checkbox=null;
		for(i=1;i<=total;i++){
			checkbox=document.getElementById("idcat"+i);
			//alert(checkbox.value);
			checkbox.checked=check.checked;
		}
		
		if(check.checked){
			selecteds=total;
		}else{
			selecteds=0;
		}
		
	}
	
	function contadorVals(check){
		if(check.checked){
			selecteds=selecteds+1;
		}else{
			selecteds=selecteds-1;
		}
	}
	
	function selectedVals(){
		if(selecteds==0){
			alert("Seleccione al menos un registro.");
			return false;
		}else{
			return true;
		}
	}
	
	function OCULTARSCROLL()
	{
		setTimeout("parent.autoResize('iframe12')",500);
		setTimeout("parent.autoResize('iframe12')",1000);
		setTimeout("parent.autoResize('iframe12')",2000);
		setTimeout("parent.autoResize('iframe12')",3000);
		setTimeout("parent.autoResize('iframe12')",4000);
		setTimeout("parent.autoResize('iframe12')",5000);
		setTimeout("parent.autoResize('iframe12')",6000);
		setTimeout("parent.autoResize('iframe12')",7000);
		setTimeout("parent.autoResize('iframe12')",8000);
		setTimeout("parent.autoResize('iframe12')",9000);
		setTimeout("parent.autoResize('iframe12')",10000);
		setTimeout("parent.autoResize('iframe12')",11000);
		setTimeout("parent.autoResize('iframe12')",12000);
		setTimeout("parent.autoResize('iframe12')",13000);
		setTimeout("parent.autoResize('iframe12')",14000);
		setTimeout("parent.autoResize('iframe12')",15000);
		setTimeout("parent.autoResize('iframe12')",16000);
		setTimeout("parent.autoResize('iframe12')",17000);
		setTimeout("parent.autoResize('iframe12')",18000);
		setTimeout("parent.autoResize('iframe12')",19000);
		setTimeout("parent.autoResize('iframe12')",20000);
	}
	setTimeout("parent.autoResize('iframe12')",500);
	setTimeout("parent.autoResize('iframe12')",1000);
	setTimeout("parent.autoResize('iframe12')",2000);
	setTimeout("parent.autoResize('iframe12')",3000);
	setTimeout("parent.autoResize('iframe12')",4000);
	setTimeout("parent.autoResize('iframe12')",5000);
	setTimeout("parent.autoResize('iframe12')",6000);
	setTimeout("parent.autoResize('iframe12')",7000);
	setTimeout("parent.autoResize('iframe12')",8000);
	setTimeout("parent.autoResize('iframe12')",9000);
	setTimeout("parent.autoResize('iframe12')",10000);
	setTimeout("parent.autoResize('iframe12')",11000);
	setTimeout("parent.autoResize('iframe12')",12000);
	setTimeout("parent.autoResize('iframe12')",13000);
	setTimeout("parent.autoResize('iframe12')",14000);
	setTimeout("parent.autoResize('iframe12')",15000);
	setTimeout("parent.autoResize('iframe12')",16000);
	setTimeout("parent.autoResize('iframe12')",17000);
	setTimeout("parent.autoResize('iframe12')",18000);
	setTimeout("parent.autoResize('iframe12')",19000);
	setTimeout("parent.autoResize('iframe12')",20000);
</script>
<?php //VALIDACIONES ?>
<script type="text/javascript" src="llamadas.js?<?php echo time();?>"></script>
<script type="text/javascript" src="../../js/jquery-1.6.min.js"></script>

<?php //VENTANA EMERGENTE ?>
<link rel="stylesheet" href="../../css/reveal.css" />
<script type="text/javascript" src="../../js/jquery-1.6.min.js"></script>
<script type="text/javascript" src="../../js/jquery.reveal.js"></script>

</head>


<body>
<?php
$rows=mysqli_query($conectar,"select * from tipologia");
$total=mysqli_num_rows($rows);
?>
<form name="form1" id="form1" action="confirmar.php" method="post" onsubmit="return selectedVals();">
<!--[if lte IE 7]>
<div class="ieWarning">Este navegador no es compatible con el sistema. Por favor, use Chrome, Safari, Firefox o Internet Explorer 8 o superior.</div>
<![endif]-->
<table style="width: 100%">
	<tr>
		<td class="auto-style2" onclick="CONSULTAMODULO('TODOS')" onmouseover="this.style.backgroundColor='#445B74';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="width: 60px; cursor:pointer">
		Todos
		<?php
			$con = mysqli_query($conectar,"select COUNT(*) cant from tipologia");
			$dato = mysqli_fetch_array($con);
			echo $dato['cant'];
		?>
		</td>
		<td class="auto-style7" style="cursor:pointer" colspan="4">
		MAESTRA TIPOLOGÍAS</td>
		<td class="auto-style2" onmouseover="this.style.backgroundColor='#CCCCCC';this.style.color='#0F213C';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="width: 55px; cursor:pointer">
		<?php
		if($metodo == 1)
		{
		?>
		<a data-reveal-id="nuevotipologia" data-animation="fade" onclick="CREARNUEVOTIPOLOGIA()" style="cursor:pointer">
		<table style="width: 100%">
			<tr>
				<td style="width: 14px"><a data-reveal-id="nuevotipologia" data-animation="fade" style="cursor:pointer"><img alt="" src="../../img/add2.png"></a></td>
				<td>Añadir</td>
			</tr>
		</table></a>
		<?php
		}
		?>
		</td>
	</tr>
	<tr>
		<td class="auto-style2" colspan="6">
		<div id="filtro">
			<table style="width: 33%">
				<tr>
					<td class="auto-style1"><strong>Filtro:<img src="../../img/buscar.png" alt="" height="18" width="15" /></strong></td>
					<td class="auto-style1">
			<input class="inputs" onkeyup="BUSCAR('TIPOLOGIA')" name="tipologia2" maxlength="70" type="text" placeholder="Tipología" style="width: 150px" /></td>
					<td class="auto-style1">
					<strong>Activo:</strong></td>
					<td class="auto-style1">
					<select name="buscaractivos" class="inputs" onchange="BUSCAR('TIPOLOGIA')">
					<option value="">Todos</option>
					<option value="1">Activos</option>
					<option value="0">Inactivo</option>
					</select></td>
				</tr>
			</table>
		</div>
		</td>
	</tr>
	<tr>
		<td colspan="6" class="auto-style2">
		<div id="editartipologia" class="reveal-modal" style="left: 37%; top: 50px; height: 460px; width: 850px;">
		<div id="editarmitipologia">
		<table style="width: 38%" align="center">
			<tr>
				<td>&nbsp;</td>
				<td class="auto-style3">Nombre:</td>
				<td class="auto-style3"><input class="inputs" name="nombre1" maxlength="50" value="<?php echo $nom; ?>" type="text" style="width: 200px" />
				</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td class="auto-style3">Activo:</td>
				<td class="auto-style3"><input class="inputs" <?php if($act == 1){ echo 'checked="checked"'; } ?> name="activo1" type="checkbox" /></td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="4">
				<input name="submit" type="button" value="Guardar" onclick="GUARDAR('TIPOLOGIA','<?php echo $ubiedi; ?>')"  style="width: 348px; height: 25px; cursor:pointer" /></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td colspan="2">
				<div id="datos">
				</div>
				</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
		</table>
		</div>
		</div>
		<div id="tipologia">
		<table style="width: 100%">
			<tr>
				<td class="auto-style3" style="width: 27px">
					<input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<?php echo $total;?>,this);" />
				</td>
				<td class="auto-style3" colspan="5">
				<?php
				if($metodo == 1)
				{
				?>
				<table style="width: 30%">
					<tr>
						<td class="auto-style1"><p style="cursor:pointer"><img src="../../img/activo.png" alt="" /><input type="submit" value="Activar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" /></p></td>
						<td class="auto-style1"><p style="cursor:pointer"><img src="../../img/inactivo.png" alt="" /><input type="submit" value="Inactivar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" /></p></td>
						<td class="auto-style1"><p style="cursor:pointer"><img src="../../img/eliminar.png" alt="" /><input type="submit" value="Eliminar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" /></p></td>
					</tr>
				</table>
				<?php
				}
				?>
				</td>
			</tr>
			<tr>
				<td class="auto-style3" style="width: 27px">&nbsp;</td>
				<td class="auto-style3" style="width: 200px" ><strong>Tipología</strong></td>
				<td class="auto-style3" style="width: 98px"><strong>Usuario</strong></td>
				<td class="auto-style3" style="width: 127px"><strong>
				Actualización</strong></td>
				<td class="auto-style3" style="width: 29px"><strong>
				Activo</strong></td>
				<td class="auto-style3">&nbsp;</td>
			</tr>
			<?php
				$contador=0;
				$con = mysqli_query($conectar,"select * from tipologia order by tip_nombre");
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$clatip = $dato['tip_clave_int'];
					$tip = $dato['tip_nombre'];
					$act = $dato['tip_sw_activo'];
					$usuact = $dato['tip_usu_actualiz'];
					$fecact = $dato['tip_fec_actualiz'];
					$contador=$contador+1;
			?>
			<tr style="cursor:pointer;background-color:#eeeeee" onmouseover="this.style.backgroundColor='#D7D7D7';this.style.color='#000000';" onmouseout="this.style.backgroundColor='#eeeeee';this.style.color='#000000';" onclick="MOSTRARMOVIMIENTO('<?php echo $clatip; ?>','<?php echo $i; ?>');OCULTARSCROLL()">
				<td class="auto-style3" style="width: 27px">
					<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<?php echo $contador;?>" value="<?php echo $dato['tip_clave_int'];?>" /></td>
				<td class="auto-style3" style="width: 200px" ><?php echo $tip; ?></td>
				<td class="auto-style3" style="width: 98px"><?php echo $usuact; ?></td>
				<td class="auto-style3" style="width: 127px"><?php echo $fecact; ?></td>
				<td class="auto-style1" style="width: 30px">
				<input name="activarinactivar" id="activarinactivar" type="checkbox" <?php if($act == 1){ echo 'checked="checked"'; } ?> disabled="disabled" ></td>
				<td class="auto-style3">
				<?php
				if($metodo == 1)
				{
				?>
				<a data-reveal-id="editartipologia" data-animation="fade" style="cursor:pointer" onclick="EDITAR('<?php echo $dato['tip_clave_int']; ?>','TIPOLOGIA')"><img src="../../img/editar.png" alt="" height="22" width="21" /></a>
				<?php
				}
				?>
				</td>
				
			</tr>
			<tr>
				<td class="auto-style3" style="width: 27px">&nbsp;</td>
				<td class="auto-style3" colspan="5" >
				<div id="movimiento<?php echo $i; ?>" align="left">
				</div>
				</td>
			</tr>
			<?php
			}
			?>
		</table>
		</div>
<div id="nuevotipologia" class="reveal-modal" style="left: 37%; top: 10px; height: 450px; width: 850px;">
	<div id="crearnuevotipologia">
	<table style="width: 38%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style1">
			<table style="width: 38%" align="center">
				<tr>
					<td>&nbsp;</td>
					<td align="left">Tipología:</td>
					<td align="left">
					<input name="txttipologia" align="left" id="txttipologia"  type="text" style="width: 200px" class="inputs" />
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td align="left">Activo:</td>
					<td align="left"><input class="inputs" align="left" name="activo" checked="checked" type="checkbox" /></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="4">
					<input name="nuevo1" type="button" value="Guardar" onclick="NUEVO('TIPOLOGIA')"  style="width: 348px; height: 25px; cursor:pointer" /></td>
				</tr>
				</table>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
			<div id="datos1">
			<table style="width: 38%" align="center">
			<tr>
				<td>&nbsp;</td>
				<td align="center">
				<div id="agregar1" align="center">
					<fieldset name="Group1" style="height: 320px">
					<legend>Registros Sin Seleccionar</legend>
						<select name="agregarven" id="agregarven" disabled="disabled" multiple="multiple" ondblclick="AGREGARPERMISOS('<?php echo $per; ?>')" style="width: 300px; height: 300px;" size="8">
						<?php
							$con = mysqli_query($conectar,"select tit_clave_int,tit_nombre,tit_dias from tipotrabajo order by tit_nombre");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$clave = $dato['tit_clave_int'];
								$nom = $dato['tit_nombre'];
								$di = $dato['tit_dias'];
						?>
							<option value="<?php echo $clave; ?>"><?php echo $nom." Duración: ".$di." dias"; ?></option>
						<?php
							}
						?>
						</select>
					</fieldset>
				</div>
				</td>
				<td align="center">
				<div style="width: 140px">
					<input type="button" class="pasar izq" disabled="disabled" value="Pasar &raquo;"><input type="button" disabled="disabled" class="quitar der" value="&laquo; Quitar"><br />
					<input type="button" class="pasartodos izq" disabled="disabled" value="Todos &raquo;"><input type="button" disabled="disabled" class="quitartodos der" value="&laquo; Todos">
				</div>
				</td>
				<td align="center">
				<div id="agregados">
					<fieldset name="Group1" style="height: 320px">
						<legend>Registros Seleccionados</legend>
						<select name="agregarven1" id="agregarven1" disabled="disabled" multiple="multiple" ondblclick="ELIMINARPERMISO('<?php echo $tipedi; ?>')" style="width: 300px; height: 300px;" size="8">
						</select>							
					</fieldset>
				</div>
				</td>
				<td>&nbsp;</td>
			</tr>
			</table>
			</div>
			</td>
			<td>&nbsp;</td>
		</tr>
	</table>
	</div>
</div>
		</td>
	</tr>
	<tr>
		<td style="width: 60px">&nbsp;</td>
		<td style="width: 117px">&nbsp;</td>
		<td style="width: 65px">&nbsp;</td>
		<td style="width: 70px">&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td style="width: 60px">&nbsp;</td>
		<td style="width: 117px">&nbsp;</td>
		<td style="width: 65px">&nbsp;</td>
		<td style="width: 70px">&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>
</form>
</body>
</html>