function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function MODULO(v,e)
{	
	if(v == 'CAJEROS')
	{
		window.location.href = "cajeros.php";
	}
	else
	if(v == 'TODOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('cajeros').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#cajeros").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?todos=si&est="+e,true);
		ajax.send(null);
		setTimeout("REFRESCARLISTAS()",500);
	}
	OCULTARSCROLL();
}
function BUSCAR(v,p)
{	
	var cajeros = "";
	var objCBarray = document.getElementsByName('multiselect_buscajero');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	cajeros += objCBarray[i].value + ",";
	    }
	}
	
	var anocon = form1.busanocontable.value;
	var cencos = form1.buscentrocostos.value;
	var cod = form1.buscodigo.value;
	var reg = form1.busregion.value;
	var mun = form1.busmunicipio.value;
	var tip = form1.bustipologia.value;
	var tipint = form1.bustipointervencion.value;
	var moda = form1.busmodalidad.value;
	var actor = form1.busactor.value;
	var est = form1.busestado.value;
	var fii = form1.fechainicio.value;
	var ffi = form1.fechafin.value;
	var fie = form1.fechainicioentrega.value;
	var ffe = form1.fechafinentrega.value;
	
	if(v == 'CAJERO')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('busqueda').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#busqueda").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='50' width='50' />"); //loading gif will be overwrited when ajax have success
		if(p > 0)
		{
			ajax.open("GET","?buscar=si&anocon="+anocon+"&caj="+cajeros+"&cencos="+cencos+"&cod="+cod+"&reg="+reg+"&mun="+mun+"&tip="+tip+"&tipint="+tipint+"&moda="+moda+"&est="+est+"&actor="+actor+"&fii="+fii+"&ffi="+ffi+"&fie="+fie+"&ffe="+ffe+"&page="+p,true);
		}
		else
		{
			ajax.open("GET","?buscar=si&anocon="+anocon+"&caj="+cajeros+"&cencos="+cencos+"&cod="+cod+"&reg="+reg+"&mun="+mun+"&tip="+tip+"&tipint="+tipint+"&moda="+moda+"&est="+est+"&fii="+fii+"&ffi="+ffi+"&fie="+fie+"&ffe="+ffe+"&actor="+actor,true);
		}
		ajax.send(null);
	}
	OCULTARSCROLL();
}
function EXPORTAR()
{	
	var cajeros = "";
	var objCBarray = document.getElementsByName('multiselect_buscajero');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	cajeros += objCBarray[i].value + ",";
	    }
	}
	var anocon = form1.busanocontable.value;
	var cencos = form1.buscentrocostos.value;
	var cod = form1.buscodigo.value;
	var reg = form1.busregion.value;
	var mun = form1.busmunicipio.value;
	var tip = form1.bustipologia.value;
	var tipint = form1.bustipointervencion.value;
	var actor = form1.busactor.value;
	var est = form1.busestado.value;
	
	window.location.href = "informes/informeexcel.php?caj="+cajeros+"&anocon="+anocon+"&cencos="+cencos+"&cod="+cod+"&reg="+reg+"&mun="+mun+"&tip="+tip+"&tipint="+tipint+"&actor="+actor+"&est="+est;
}
function VERCIUDADES(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('ciudades').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#ciudades").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?verciudades=si&reg="+v,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTACIUDADES()",800);
}
function VERMODALIDADES1(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('modalidades1').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#modalidades1").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?vermodalidades1=si&tii="+v,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTAMODALIDADES1()",800);
}
function MOSTRARMOVIMIENTO(cc)
{
	var ocu = $('#ocumovimiento'+cc).val();
	if(ocu == 0)
	{
		var div = document.getElementById('movimiento'+cc);
    	div.style.display = 'block';
    	$('#ocumovimiento'+cc).val(1);
	}
	else
	if(ocu == 1)
	{
		var div = document.getElementById('movimiento'+cc);
    	div.style.display = 'none';
    	$('#ocumovimiento'+cc).val(0);
	}
	  
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
			document.getElementById('movimiento'+cc).innerHTML=ajax.responseText;
	    }
	}
	//jQuery("#agregados").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarmovimiento=si&cc="+cc,true);
	ajax.send(null);
	OCULTARSCROLL();
}