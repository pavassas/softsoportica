function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function EDITAR(v,m)
{	
	if(m == 'AREA')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('editararea').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#editararea").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?editarare=si&areedi="+v,true);
		ajax.send(null);
	}
}
function GUARDAR(v,id)
{
	if(v == 'AREA')
	{
		var are = form1.area1.value;
		var la = are.length;
		var act = form1.activo1.checked;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('datos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#datos").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?guardarare=si&are="+are+"&act="+act+"&la="+la+"&a="+id,true);
		ajax.send(null);
		if(are != '' && la >= 3)
		{
			setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
		}
	}
}
function NUEVO(v)
{
	if(v == 'AREA')
	{
		var are = form1.area.value;
		var la = are.length;		
		var act = form1.activo.checked;
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('datos1').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#datos1").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?nuevaarea=si&are="+are+"&act="+act+"&la="+la,true);
		ajax.send(null);
		if(are != '' && la >= 3)
		{
			form1.area.value = '';
			setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
		}
	}
}
function CONSULTAMODULO(v)
{
	if(v == 'TODOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('areas').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#areas").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?todos=si",true);
		ajax.send(null);
	}
}
function BUSCAR(m)
{	
	if(m == 'AREA')
	{
		var are = form1.area2.value;
		var act = form1.buscaractivos.value;
				
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('areas').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#areas").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?buscarare=si&are="+are+"&act="+act,true);
		ajax.send(null);
	}
}