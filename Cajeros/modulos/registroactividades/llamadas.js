function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function CONSULTAMODULO(v)
{
	if(v == 'INFORMEINVENTARIO')
	{
		var div = document.getElementById('filtro');
    	div.style.display = 'none';
		window.location.href = "inventario.php";
	}
	else
	if(v == 'TODOS')
	{
		var div = document.getElementById('filtro');
    	div.style.display = 'block';
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('asignados').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#asignados").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?todos=si",true);
		ajax.send(null);
	}
	OCULTARSCROLL();
}
function BUSCAR(v)
{
	var actividades = "";
	var objCBarray = document.getElementsByName('multiselect_busactividad');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	actividades += objCBarray[i].value + ",";
	    }
	}
	
	var usuarios = "";
	var objCBarray = document.getElementsByName('multiselect_bususuario');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	usuarios += objCBarray[i].value + ",";
	    }
	}
	
	var articulos = "";
	var objCBarray = document.getElementsByName('multiselect_busarticulo');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	articulos += objCBarray[i].value + ",";
	    }
	}
	
	var ventanas = "";
	var objCBarray = document.getElementsByName('multiselect_busventana');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	ventanas += objCBarray[i].value + ",";
	    }
	}
	
	var fi = form1.busfecini.value;
	var ff = form1.busfecfin.value;
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('materialesagg').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#materialesagg").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
	if(v > 0)
	{
		ajax.open("GET","?buscarmat=si&act="+actividades+"&usu="+usuarios+"&art="+articulos+"&ven="+ventanas+"&fi="+fi+"&ff="+ff+"&page="+v,true);
	}
	else
	{
		ajax.open("GET","?buscarmat=si&act="+actividades+"&usu="+usuarios+"&art="+articulos+"&ven="+ventanas+"&fi="+fi+"&ff="+ff,true);
	}
	ajax.send(null);
	OCULTARSCROLL();
}
function EXPORTAR()
{
	var actividades = "";
	var objCBarray = document.getElementsByName('multiselect_busactividad');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	actividades += objCBarray[i].value + ",";
	    }
	}
	
	var usuarios = "";
	var objCBarray = document.getElementsByName('multiselect_bususuario');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	usuarios += objCBarray[i].value + ",";
	    }
	}
	
	var articulos = "";
	var objCBarray = document.getElementsByName('multiselect_busarticulo');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	articulos += objCBarray[i].value + ",";
	    }
	}
	
	var ventanas = "";
	var objCBarray = document.getElementsByName('multiselect_busventana');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	ventanas += objCBarray[i].value + ",";
	    }
	}
	
	var fi = form1.busfecini.value;
	var ff = form1.busfecfin.value;
	
	window.open("informes/informeexcel.php?act="+actividades+"&usu="+usuarios+"&art="+articulos+"&ven="+ventanas+"&fi="+fi+"&ff="+ff, "_blank");
	//window.location.href = "informes/informeexcel.php?act="+actividades+"&usu="+usuarios+"&art="+articulos+"&ven="+ventanas+"&fi="+fi+"&ff="+ff;
}
