<?php
include('../../data/Conexion.php');
require_once('../../Classes/PHPMailer-master/class.phpmailer.php');
header("Cache-Control: no-store, no-cache, must-revalidate");
session_start();
error_reporting(0);
date_default_timezone_set('America/Bogota');
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
ini_set('memory_limit','500M');
ini_set('max_execution_time', 600);
ini_set('upload_max_filesize', '100M');
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];

$fecha=date("Ymd");
$fechaact=date("Y/m/d H:i:s");
$clacaj = $_POST['clacaj'];
$actor = $_POST['actor'];
$arc = $_POST['archivo'];
$claada = $_POST['claada'];
function CORREGIRTEXTO($string)
{
    $string = str_replace("REEMPLAZARNUMERAL", "#", $string);
    $string = str_replace("REEMPLAZARMAS", "+",$string);
    $string = str_replace("REEMPLAZARMENOS", "-", $string);
    return $string;
}
$max=8000000;//8MB

if($clacaj != '' and $clacaj != 0 and $actor != '')
{
	if(is_array($_FILES)) 
	{
		$con = mysqli_query($conectar,"select MAX(ada_clave_int) max from adjunto_actor where caj_clave_int = '".$clacaj."'");
		$dato = mysqli_fetch_array($con);
		$num = $dato['max'];
		
		if($num == 0 || $num == '')
		{
			$num = 1;
		}
		else
		{
			$num = $num+1;
		}
				
		if($actor == 'INFOBASICA')
		{
			if(is_uploaded_file($_FILES['adjuntoinfobase']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntoinfobase']['tmp_name'];
				$archivo = basename($_FILES['adjuntoinfobase']['name']);
						
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
				$prefijo = "INFO - BASE";
				$destino =  "../../adjuntos/infobase/".$clacaj."-".$prefijo."".$num.".".$extension;
                $filesize = $_FILES['adjuntoinfobase']['size'];

                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and caj_clave_int = '".$clacaj."' and tad_clave_int = '34' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "error4";
                }
                else
                if($filesize>$max)
                {
                    echo "error3";
                }
                else
				if(strtoupper($extension)=="DWG" || strtoupper($extension)=="RAR" || strtoupper($extension)=="ZIP")
                {
                    echo "error1";
                }
                else
                {

                    if (move_uploaded_file($sourcePath, $destino)) {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,caj_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clacaj . "','" . $fechaact . "','" . $destino . "','" . $archivo . "',34,'" . $usuario . "','" . $fechaact . "')");
                        echo "ok";
                    }
                    else
                    {
                        echo "error2";
                    }
                }
			}
		}
		else
		if($actor == 'INMOBILIARIA')
		{
			if(is_uploaded_file($_FILES['adjuntoinm']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntoinm']['tmp_name'];
				$archivo = basename($_FILES['adjuntoinm']['name']);
						
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/


				$prefijo = "INMOBILIARIA - REQUERIMIENTO LOCAL";				
				$destino =  "../../adjuntos/inmobiliaria/".$clacaj."-".$prefijo."".$num.".".$extension;
                $filesize = $_FILES['adjuntoinm']['size'];
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and caj_clave_int = '".$clacaj."' and tad_clave_int = '1' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "error4";
                }
                else
                if($filesize>$max)
                {
                    echo "error3";
                }
                else
                if(strtoupper($extension)=="DWG" || strtoupper($extension)=="RAR" || strtok($extension)=="ZIP")
                {
                    echo "error1";
                }
                else
                {
                    if (move_uploaded_file($sourcePath, $destino))
                    {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,caj_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clacaj . "','" . $fechaact . "','" . $destino . "','" . $archivo . "',1,'" . $usuario . "','" . $fechaact . "')");
                        /*
                          //Envió correo a box con el archivo que acabaron de adjuntar
                        $mail = new PHPMailer();
                        $mail->Host = "localhost";
                        $mail->From = "upload.Cajeros.1telzd5g6a@u.box.com";
                        $mail->FromName = "Servidor web";
                        $mail->Subject = "Socio nuevo";
                        $mail->AddAddress("upload.Cajeros.1telzd5g6a@u.box.com", "Altas");
                        $body = "Nuevo sociorn";
                        $body .= "Nombre: Brayan";
                        $body .= "Edad: 22";
                        $mail->Body = $body;
                        //adjuntamos un archivo
                        $mail->AddAttachment($destino, $fecha." ".$prefijo.".".$extension);
                        $mail->Send();
                        //Por si quiero mostrar la imagen subida en un div
                        //<img src="echo $targetPath;" width="100px" height="100px" />*/
                        echo "ok";
                    }
                    else
                    {
                        echo "error2";
                    }
                }
			}
		}
		else
		if($actor == 'VISITA')
		{
			if(is_uploaded_file($_FILES['adjuntovis']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntovis']['tmp_name'];
				$archivo = basename($_FILES['adjuntovis']['name']);
						
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
				$prefijo = "VISITA - REQUERIMIENTO VISITA";
				$destino =  "../../adjuntos/visita/".$clacaj."-".$prefijo."".$num.".".$extension;
                $filesize = $_FILES['adjuntovis']['size'];

                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and caj_clave_int = '".$clacaj."' and tad_clave_int = '2' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "error4";
                }
                else
                if($filesize>$max)
                {
                    echo "error3";
                }
                else
                if(strtoupper($extension)=="DWG" || strtoupper($extension)=="RAR" || strtok($extension)=="ZIP")
                {
                    echo "error1";
                }
                else
                {

                    if (move_uploaded_file($sourcePath, $destino))
                    {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,caj_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clacaj . "','" . $fechaact . "','" . $destino . "','" . $archivo . "',2,'" . $usuario . "','" . $fechaact . "')");
                        echo "ok";
                    }
                    else
                    {
                        echo "error2";
                    }
                }
			}
		}
		else
		if($actor == 'COMITE')
		{
			if(is_uploaded_file($_FILES['adjuntocomite']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntocomite']['tmp_name'];
				$archivo = basename($_FILES['adjuntocomite']['name']);
						
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
				$prefijo = "COMITE - INFORME COMITE";
				$destino =  "../../adjuntos/comite/".$clacaj."-".$prefijo."".$num.".".$extension;
                $filesize = $_FILES['adjuntocomite']['size'];
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and caj_clave_int = '".$clacaj."' and tad_clave_int = '28' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "error4";
                }
                else
                if($filesize>$max)
                {
                    echo "error3";
                }
                else
                if(strtoupper($extension)=="DWG" || strtoupper($extension)=="RAR" || strtok($extension)=="ZIP")
                {
                    echo "error1";
                }
                else
                {
                    if (move_uploaded_file($sourcePath, $destino))
                    {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,caj_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clacaj . "','" . $fechaact . "','" . $destino . "','" . $archivo . "',28,'" . $usuario . "','" . $fechaact . "')");
                        echo "ok";
                    }
                    else
                    {
                        echo "error2";
                    }
                }
			}
		}
		else
		if($actor == 'CONTRATO')
		{
			if(is_uploaded_file($_FILES['adjuntocontrato']['tmp_name']))
			{
                $sourcePath = $_FILES['adjuntocontrato']['tmp_name'];
                $archivo = basename($_FILES['adjuntocontrato']['name']);

                $array_nombre = explode('.', $archivo);
                $cuenta_arr_nombre = count($array_nombre);
                $extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
                /*if($extension == 'jpg')
                {
                    $extension = 'png';
                }*/
                $prefijo = "CONTRATO - INFORME CONTRATO";
                $destino = "../../adjuntos/contrato/" . $clacaj . "-" . $prefijo . "" . $num . "." . $extension;
                $filesize = $_FILES['adjuntocontrato']['size'];
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and caj_clave_int = '".$clacaj."' and tad_clave_int = '29' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "error4";
                }
                else
                if($filesize>$max)
                {
                    echo "error3";
                }
                else
                if (strtoupper($extension) == "DWG" || strtoupper($extension) == "RAR" || strtoupper($extension) == "ZIP")
                {
                    echo "error1";
                }
                else
                {
                    if (move_uploaded_file($sourcePath, $destino))
                    {
                            $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,caj_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clacaj . "','" . $fechaact . "','" . $destino . "','" . $archivo . "',29,'" . $usuario . "','" . $fechaact . "')");
                            //alerta10
                            $conu = mysqli_query($conectar,"select usu_clave_int from asignacion_alerta where tas_clave_int = '8'");
                            $numu = mysqli_num_rows($conu);
                            if ($numu > 0) {
                                $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario where usu_clave_int in (select usu_clave_int from asignacion_alerta where tas_clave_int = '8')");
                            } else {
                                $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario where usu_usuario IN ('superadmin','Ruben.escudero','Carolina.garcia','Camilo.franco','Daniel.guzman','marta.lopez')");
                            }
                        //$conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario where usu_usuario IN ('superadmin','Ruben.escudero','Carolina.garcia','Camilo.franco','Daniel.guzman','marta.lopez')");
                            $num = mysqli_num_rows($conema);
                            for ($i = 0; $i < $num; $i++)
                            {
                                $mail = new PHPMailer();

                                $datoema = mysqli_fetch_array($conema);
                                $mail->Body = '';
                                $ema = '';
                                $nom = $datoema['usu_nombre'];
                                $ema = $datoema['usu_email'];

                                $concaj = mysqli_query($conectar,"select caj_nombre from cajero where caj_clave_int = '" . $clacaj . "'");
                                $datocaj = mysqli_fetch_array($concaj);
                                $nomcaj = CORREGIRTEXTO($datocaj['caj_nombre']);

                                $conact = mysqli_query($conectar,"select usu_nombre from usuario where usu_usuario = '" . $usuario . "'");
                                $datoact = mysqli_fetch_array($conact);
                                $nomact = $datoact['usu_nombre'];

                                $mail->From = "admin@pavastecnologia.com";
                                $mail->FromName = "Soportica";
                                $mail->AddAddress($ema, "Destino");
                                $mail->Subject = "Alerta Adjunto Informe CONTRATO. Cajero Nro. " . $clacaj;

                                // Cuerpo del mensaje
                                $mail->Body .= "Hola " . $nom . "!\n\n";
                                $mail->Body .= "CONTROL CAJEROS registra que el actor contrato ha adjuntado el informe.\n";
                                $mail->Body .= "CAJERO: " . $nomcaj . "\n";
                                $mail->Body .= "USUARIO QUE ADJUNTA: " . $nomact . "\n";
                                $mail->Body .= date("d/m/Y H:m:s") . "\n\n";
                                $mail->Body .= "Este mensaje es generado automáticamente por CONTROL CAJEROS, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.co/Cajeros \n";

                                if (!$mail->Send())
                                {
                                    //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                                    //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                                }
                                else
                                {
                                }
                            }
                            echo "ok";
                        }
                        else
                        {
                            echo "error2";
                        }
                    }
                }
		}
		else
		if($actor == 'PREFACTIBILIDAD')
		{
			if(is_uploaded_file($_FILES['adjuntoprefactibilidad']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntoprefactibilidad']['tmp_name'];
				$archivo = basename($_FILES['adjuntoprefactibilidad']['name']);
						
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
				$prefijo = "PREFACTIBILIDAD - INFORME PREFACTIBILIDAD";
				$destino =  "../../adjuntos/prefactibilidad/".$clacaj."-".$prefijo."".$num.".".$extension;
                $filesize = $_FILES['adjuntoprefactibilidad']['size'];
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and caj_clave_int = '".$clacaj."' and tad_clave_int = '30' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "error4";
                }
                else
                if($filesize>$max)
                {
                    echo "error3";
                }
                else
                if (strtoupper($extension) == "DWG" || strtoupper($extension) == "RAR" || strtoupper($extension) == "ZIP")
                {
                    echo "error1";
                }
                else
                {
                    if (move_uploaded_file($sourcePath, $destino))
                    {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,caj_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clacaj . "','" . $fechaact . "','" . $destino . "','" . $archivo . "',30,'" . $usuario . "','" . $fechaact . "')");
                        echo "ok";
                    }
                    else
                    {
                        echo "error2";
                    }
                }
			}
		}
		else
		if($actor == 'PEDIDOMAQUINA')
		{
			if(is_uploaded_file($_FILES['adjuntopedidomaquina']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntopedidomaquina']['tmp_name'];
				$archivo = basename($_FILES['adjuntopedidomaquina']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
				$prefijo = "PEDIDO MAQUINA - INFORME PEDIDO MAQUINA";
				$destino =  "../../adjuntos/pedidomaquina/".$clacaj."-".$prefijo."".$num.".".$extension;
                $filesize = $_FILES['adjuntopedidomaquina']['size'];
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and caj_clave_int = '".$clacaj."' and tad_clave_int = '31' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "error4";
                }
                else
                if($filesize>$max)
                {
                    echo "error3";
                }
                else
                if (strtoupper($extension) == "DWG" || strtoupper($extension) == "RAR" || strtoupper($extension) == "ZIP")
                {
                    echo "error1";
                }
                else
                {

                    if (move_uploaded_file($sourcePath, $destino))
                    {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,caj_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clacaj . "','" . $fechaact . "','" . $destino . "','" . $archivo . "',31,'" . $usuario . "','" . $fechaact . "')");
                        echo "ok";
                    }
                    else
                    {
                        echo "error2";
                    }
                }
			}
		}
		else
		if($actor == 'PROYECCION')
		{
			if(is_uploaded_file($_FILES['adjuntoproyeccion']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntoproyeccion']['tmp_name'];
				$archivo = basename($_FILES['adjuntoproyeccion']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
				$prefijo = "PROYECCION - INFORME PROYECCION";
				$destino =  "../../adjuntos/proyeccion/".$clacaj."-".$prefijo."".$num.".".$extension;
                $filesize = $_FILES['adjuntoproyeccion']['size'];
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and caj_clave_int = '".$clacaj."' and tad_clave_int = '32' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "error4";
                }
                else
                if($filesize>$max)
                {
                    echo "error3";
                }
                else
                if (strtoupper($extension) == "DWG" || strtoupper($extension) == "RAR" || strtoupper($extension) == "ZIP")
                {
                    echo "error1";
                }

                else {
                    if (move_uploaded_file($sourcePath, $destino)) {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,caj_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clacaj . "','" . $fechaact . "','" . $destino . "','" . $archivo . "',32,'" . $usuario . "','" . $fechaact . "')");
                        echo "ok";
                    }
                    else{
                        echo "error2";
                    }
                }
			}
		}
		else
		if($actor == 'CANAL')
		{
			if(is_uploaded_file($_FILES['adjuntocanal']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntocanal']['tmp_name'];
				$archivo = basename($_FILES['adjuntocanal']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
				$prefijo = "CANAL - INFORME CANAL";
				$destino =  "../../adjuntos/canal/".$clacaj."-".$prefijo."".$num.".".$extension;
                $filesize = $_FILES['adjuntocanal']['size'];
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and caj_clave_int = '".$clacaj."' and tad_clave_int = '33' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "error4";
                }
                else
                if($filesize>$max)
                {
                    echo "error3";
                }
                else
                if (strtoupper($extension) == "DWG" || strtoupper($extension) == "RAR" || strtoupper($extension) == "ZIP")
                {
                    echo "error1";
                }
                else
                {
                    if (move_uploaded_file($sourcePath, $destino))
                    {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,caj_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clacaj . "','" . $fechaact . "','" . $destino . "','" . $archivo . "',33,'" . $usuario . "','" . $fechaact . "')");
                        echo "ok";
                    }
                    else
                    {
                        echo "error2";
                    }
                }
			}
		}
		else
		if($actor == 'DISENO')
		{
			if(is_uploaded_file($_FILES['adjuntodis']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntodis']['tmp_name'];
				$archivo = basename($_FILES['adjuntodis']['name']);
						
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
				$prefijo = "DISENO - REQUERIMIENTO DISENO";
				$destino =  "../../adjuntos/diseno/".$clacaj."-".$prefijo."".$num.".".$extension;
                $filesize = $_FILES['adjuntodis']['size'];
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and caj_clave_int = '".$clacaj."' and tad_clave_int = '3' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "error4";
                }
                else
                if($filesize>$max)
                {
                    echo "error3";
                }
                else
                if (strtoupper($extension) == "DWG" || strtoupper($extension) == "RAR" || strtoupper($extension) == "ZIP")
                {
                    echo "error1";
                }
                else
                {
                    if (move_uploaded_file($sourcePath, $destino))
                    {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,caj_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clacaj . "','" . $fechaact . "','" . $destino . "','" . $archivo . "',3,'" . $usuario . "','" . $fechaact . "')");
                        echo "ok";
                    }
                    else
                    {
                        echo "error2";
                    }
                }
			}
		}
		else
		if($actor == 'LICENCIA')
		{
			if(is_uploaded_file($_FILES['adjuntolic']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntolic']['tmp_name'];
				$archivo = basename($_FILES['adjuntolic']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
				$prefijo = "LICENCIA - REQUERIMIENTO LICENCIA";
				$destino =  "../../adjuntos/licencia/".$clacaj."-".$prefijo."".$num.".".$extension;
                $filesize = $_FILES['adjuntolic']['size'];
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and caj_clave_int = '".$clacaj."' and tad_clave_int = '4' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "error4";
                }
                else
                if($filesize>$max)
                {
                    echo "error3";
                }
                else
                if (strtoupper($extension) == "DWG" || strtoupper($extension) == "RAR" || strtoupper($extension) == "ZIP")
                {
                    echo "error1";
                }
                else
                {

                    if (move_uploaded_file($sourcePath, $destino))
                    {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,caj_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'" . $clacaj . "','" . $fechaact . "','" . $destino . "','" . $archivo . "',4,'" . $usuario . "','" . $fechaact . "')");
                        echo "ok";
                    }
                    else
                    {
                        echo "error2";
                    }
                }
			}
		}
		else
		if($actor == 'ACTUALIZAARCHIVO')
		{
			if(is_uploaded_file($_FILES['archivoadjunto']['tmp_name'])) 
			{
				$sourcePath = $_FILES['archivoadjunto']['tmp_name'];
				$archivo = basename($_FILES['archivoadjunto']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				$con = mysqli_query($conectar,"select * from adjunto_actor where ada_clave_int = '".$claada."'");
				$dato = mysqli_fetch_array($con);
				$adj = $dato['ada_adjunto'];
				
				if($arc == 1)
				{
					$prefijo = "INMOBILIARIA - REQUERIMIENTO LOCAL";
					$destino =  "../../adjuntos/inmobiliaria/".$clacaj."-".$prefijo."".$claada.".".$extension;
				}else
				if($arc == 2)
				{
					$prefijo = "VISITA - REQUERIMIENTO VISITA";
					$destino =  "../../adjuntos/visita/".$clacaj."-".$prefijo."".$claada.".".$extension;
				}else
				if($arc == 3)
				{
					$prefijo = "DISENO - REQUERIMIENTO DISENO";
					$destino =  "../../adjuntos/diseno/".$clacaj."-".$prefijo."".$claada.".".$extension;
				}else
				if($arc == 4)
				{
					$prefijo = "LICENCIA - REQUERIMIENTO LICENCIA";
					$destino =  "../../adjuntos/licencia/".$clacaj."-".$prefijo."".$claada.".".$extension;
				}
                else
                if($arc == 34)
                {
                    $prefijo = "INFO - BASE";
                    $destino =  "../../adjuntos/infobase/".$clacaj."-".$prefijo."".$claada.".".$extension;
                }
                $filesize = $_FILES['archivoadjunto']['size'];
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and caj_clave_int = '".$clacaj."' and tad_clave_int = '".$arc."' and ada_sw_eliminado = 0 and ada_clave_int != '".$claada."'");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "error4";
                }
                else
                if($filesize>$max)
                {
                    echo "error3";
                }
                else
                if (strtoupper($extension) == "DWG" || strtoupper($extension) == "RAR" || strtoupper($extension) == "ZIP")
                {
                    echo "error1";
                }
                else {

                    if (move_uploaded_file($sourcePath, $destino))
                    {
                        $sql = mysqli_query($conectar,"update adjunto_actor set ada_usu_actualiz = '" . $usuario . "',ada_fec_actualiz = '" . $fechaact . "', ada_adjunto = '" . $destino . "', ada_nombre_adjunto = '" . $archivo . "' where ada_clave_int = '" . $claada . "'");
                        echo "ok";
                    }
                    else
                    {
                        echo "error2";
                    }
                }
			}
		}
	}
}
?>