<?php
include('../../data/Conexion.php');
$moda = $_POST['moda'];
$fecini = $_POST['fecini'];
$caj = $_POST['caj'];
$conmod = mysqli_query($conectar,"select * from modalidad where mod_clave_int = '".$moda."'");
$datomod = mysqli_fetch_array($conmod);
//Orden: Inmobiliaria, Visita, Comite, Contrato, Diseño, Prefactibilidad, Pedido maquina, Licencia,
//Canal, Proyección, Preliminar, Constructor, Seguridad
$modainm = $datomod['mod_sw_inmobiliaria'];
$dinm = $datomod['mod_dias_inmobiliaria'];
$modavis = $datomod['mod_sw_visita'];
$dvis = $datomod['mod_dias_visita'];
$modacom = $datomod['mod_sw_aprocomite'];
$dcom = $datomod['mod_dias_aprocomite'];
$modacon = $datomod['mod_sw_contrato'];
$dcon = $datomod['mod_dias_contrato'];
$modadis = $datomod['mod_sw_diseno'];
$ddis = $datomod['mod_dias_diseno'];
$modaprefact = $datomod['mod_sw_prefactibilidad'];

$dprefact = $datomod['mod_dias_prefactibilidad'];
$modaped = $datomod['mod_sw_pedido_maquina'];
$dped = $datomod['mod_dias_pedido_maquina'];
$modalic = $datomod['mod_sw_licencia'];
$dlic = $datomod['mod_dias_licencia'];
$modacan = $datomod['mod_sw_canal'];
$dcan = $datomod['mod_dias_canal'];
$modapro = $datomod['mod_sw_proyeccion'];
$dpro = $datomod['mod_dias_proyeccion'];
$modapre = $datomod['mod_sw_preliminar'];
$dpre = $datomod['mod_dias_preliminar'];
$modaint = $datomod['mod_sw_interventoria'];
$modacons = $datomod['mod_sw_constructor'];
$dcons = $datomod['mod_dias_constructor'];
$modaseg = $datomod['mod_sw_seguridad'];

if($caj>0)
{
    $coninfo = mysqli_query($conectar,"select caj_nombre,caj_direccion,caj_ano_contable,caj_region,caj_departamento,caj_municipio,tip_clave_int,tii_clave_int,mod_clave_int,caj_codigo_cajero,cco_clave_int,caj_estado_proyecto,caj_sw_padre,caj_padre,c.caj_clave_int clacaj, cai_fecha_ini_inmobiliaria,cai_inmobiliaria,cav_visitante,cav_fecha_visita,cad_disenador,cad_fecha_inicio_diseno,cac_constructor,cac_fecha_inicio_constructor,cin_fecha_inicio_interventoria,cas_fecha_inicio_seguridad,cal_fecha_inicio_licencia,bco.bac_sw_contrato,bco.bac_sw_canal,bc.bco_fec_ini_teorica,bca.bca_fec_ini_teorica from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int)  inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) left outer join bancolombia_comite bco ON(bco.caj_clave_int = c.caj_clave_int) LEFT OUTER JOIN bancolombia_contrato bc ON(bc.caj_clave_int = c.caj_clave_int) LEFT OUTER JOIN  bancolombia_canal bca ON(bca.caj_clave_int = c.caj_clave_int) where c.caj_clave_int =  '".$caj."'");
    $datinfo = mysqli_fetch_array($coninfo);
    $fiinm = $datinfo['cai_fecha_ini_inmobiliaria'];
    $fivis = $datinfo['cav_fecha_visita'];
    $fidis = $datinfo['cad_fecha_inicio_diseno'];
    $ficons = $datinfo['cac_fecha_inicio_constructor'];
    $fiint = $datinfo['cai_fecha_inicio_interventoria'];
    $fiseg = $datinfo['cas_fecha_inicio_seguridad'];
    $filic = $datinfo['cal_fecha_inicio_licencia'];
    $ficon = $datinfo['bco_fec_ini_teorica'];
    $fican = $datinfo['bca_fec_ini_teorica'];
    $swcon = $datinfo['bac_sw_contrato'];
    $swcan = $datinfo['bac_sw_canal'];

}
else{
    $fiinm = "";
    $fivis = "";
    $fidis = "";
    $ficons = "";
    $fiint = "";
    $fiseg = "";
    $filic = "";
}

//Inmobiliaria
if($modainm == 1)
{
    if($fiinm=="" || $fiinm==NULL || $fiinm=="0000-00-00") {
        $fiinm = $fecini;
    }

	$ffinm = CALCULARFECHA($fiinm,$dinm,"Inmobiliaria");
	$diasinm = CALCULARDIAS($fiinm,$ffinm,"Inmobiliaria");
}
else
{
	$fiinm = "";
	$ffinm = $fecini;
	$diasinm = 0;
}
//Visita
if($modavis == 1)
{
    if($fivis=="" || $fivis==NULL || $fivis=="0000-00-00") {
	$fivis = $ffinm; }
	$ffvis = CALCULARFECHA($fivis,$dvis,"Visita");
	$diasvis = CALCULARDIAS($fivis,$ffvis,"Visita");
}
else
{
	$fivis = "";
	$ffvis = "";//$ffinm;
	$diasvis = 0;
}

//Comite
if($modacom == 1)
{

	$ficom = $ffvis;
	$ffcom = CALCULARFECHA($ficom,$dcom,"Cajero");
	$diascom = CALCULARDIAS($ficom,$ffcom,"Cajero");
}
else
{
	$ficom = "";
	$ffcom = $ffvis;
	$diascom = 0;
}
//Contrato
if($modacon == 1)
{
    if($swcon==1)
    {
        $ficon = $ficon;
    }
    else{
	$ficon = $fecini;
        }
	$ffcon = CALCULARFECHA($ficon,$dcon,"Cajero");
	$diascon = CALCULARDIAS($ficon,$ffcon, "Cajero");
}
else
{
	$ficon = $ficon;
    $ffcon = CALCULARFECHA($ficon,$dcon,"Cajero");
	//$ffcon = $fecini;
	$diascon = 0;
}
//Diseño
if($modadis == 1)
{
    if($fidis=="" || $fidis==NULL || $fidis=="0000-00-00") {
	$fidis = $fecini; }
	$ffdis = CALCULARFECHA($fidis,$ddis,"Diseno");
	$diasdis = CALCULARDIAS($fidis,$ffdis,"Diseno");
}
else
{
	$fidis = "";
	$ffdis = $fecini;
	$diasdis = 0;
}
//Prefactibilidad canal inicia cuando finaliza licencia: Daniel Guzman 31 08 2016
/*if($modaprefact == 1)
{
	$fiprefact = $fecini;
	$ffprefact = CALCULARFECHA($fiprefact,$dprefact);
	$diasprefact = CALCULARDIAS($fiprefact,$ffprefact);
}
else
{
	$fiprefact = "";
	$ffprefact = $fecini;
	$diasprefact = 0;
}*/
//Pedido maquina
if($modaped == 1)
{
	$fiped = $fecini;
	$ffped = CALCULARFECHA($fiped,$dped,"Cajero");
	$diasped = CALCULARDIAS($fiped,$ffped,"Cajero");
}
else
{
	$fiped = "";
	$ffped = $fecini;
	$diasped = 0;
}
//Licencia
if($modalic == 1)
{
    if($filic=="" || $filic==NULL || $filic=="0000-00-00") {
        $filic = $ffdis;
    }
	$fflic = CALCULARFECHA($filic,$dlic,"Licencia");
	$diaslic = CALCULARDIAS($filic,$fflic,"Licencia");
}
else
{
	$filic = "";
	$fflic = $ffdis;
	$diaslic = 0;
}
//Prefactibilidad canal
if($modaprefact == 1)
{
	$fiprefact = $fflic;
	$ffprefact = CALCULARFECHA($fiprefact,$dprefact,"Cajero");
	$diasprefact = CALCULARDIAS($fiprefact,$ffprefact,"Cajero");
}
else
{
	$fiprefact = "";
	$ffprefact = $fflic;
	$diasprefact = 0;
}
//Canal
if($modacan == 1 || $swcan==1)
{
    if($swcan==1){ $fican = $fican;}else{
	$fican = $fflic;}
	$ffcan = CALCULARFECHA($fican,$dcan,"Cajero");
	$diascan = CALCULARDIAS($fican,$ffcan, "Cajero");
}
else
{
	$fican = $fican;
	//$ffcan = $fflic;
    $ffcan = CALCULARFECHA($fican,$dcan,"Cajero");
    $diascan = CALCULARDIAS($fican,$ffcan, "Cajero");
	//$diascan = 0;
}
//Proyeccion
if($modapro == 1)
{
	$fipro = $fflic;
	$ffpro = CALCULARFECHA($fipro,$dpro,"Cajero");
	$diaspro = CALCULARDIAS($fipro,$ffpro, "Cajero");
}
else
{
	$fipro = "";
	$ffpro = $fflic;
	$diaspro = 0;
}
//Preliminar
if($modapre == 1)
{
	$fipre = $ffpro;
	$ffpre = CALCULARFECHA($fipre,$dpre,"Cajero");
	$diaspre = CALCULARDIAS($fipre,$ffpre,"Cajero");
}
else
{
	$fipre = "";
	$ffpre = $ffpro;
	$diaspre = 0;
}
//Interventoria
if($modaint == 1)
{
    if($fiint=="" || $fiint==NULL || $fiint=="0000-00-00") {
	$fiint = $ffpro;}
	$ffint = $ffpro;
	$diasint = CALCULARDIAS($fiint,$ffint,"Interventoria");
}
else
{
	$fiint = "";
	$ffint = $ffpro;
	$diasint = 0;
}
//Constructor
if($modacons == 1)
{
    if($ficons=="" || $ficons==NULL || $ficons=="0000-00-00") {
	$ficons = $ffint; }
	$ffcons = CALCULARFECHA($ficons,$dcons,"Constructor");
	$diascons = CALCULARDIAS($ficons,$ffcons,"Constructor");
}
else
{
	$ficons = "";
	$ffcons = $ffint;
	$diascons = 0;
}
//Seguridad
if($modaseg == 1)
{
    if($fiseg=="" || $fiseg==NULL || $fiseg=="0000-00-00") {
	$fiseg = $ffcons; }
	$ffseg = $ffcons;
	$diasseg = CALCULARDIAS($fiseg,$ffseg,"Seguridad");
}
else
{
	$fiseg = "";
	$ffseg = $ffcons;
	$diasseg = 0;
}
/*
function CALCULARFECHA($fi,$d)
{
	$confec = mysqli_query($conectar,"select DATE_ADD('".$fi."', INTERVAL $d DAY) fecha from usuario LIMIT 1");
	$datofec = mysqli_fetch_array($confec);
	return $datofec['fecha'];
}*/
/*
function CALCULARDIAS($fi,$ff,$actor)
{
	
	//$condias = mysqli_query($conectar,"select DATEDIFF('".$ff."','".$fi."') dias from usuario LIMIT 1");
	//$datodias = mysqli_fetch_array($condias);
	//return $datodias['dias'];

	$conectar = mysqli_connect("localhost", "usrpavas", "9A12)WHFy$2p4v4s", "cajeros");

	$sqlp = mysqli_query($conectar, "SELECT pea_sw_dias_habiles FROM permiso_actor WHERE pea_actor = '".$actor."'");
	$datp = mysqli_fetch_array($sqlp);
	$per = $datp['pea_sw_dias_habiles'];

	$sql = mysqli_query($conectar,"select diasHabiles('".$fi."','".$ff."',".$per.") cant");
	$dat = mysqli_fetch_array($sql);
	$di = $dat['cant'];
	return $di;
}*/
$myArray[] = array("fiinm"=>$fiinm, "ffinm"=>$ffinm, "dinm"=>$diasinm, "fivis"=>$fivis, "ffvis"=>$ffvis, "dvis"=>$diasvis, "ficom"=>$ficom, "ffcom"=>$ffcom, "dcom"=>$diascom, "ficon"=>$ficon, "ffcon"=>$ffcon, "dcon"=>$diascon, "fidis"=>$fidis, "ffdis"=>$ffdis, "ddis"=>$diasdis, "fiprefact"=>$fiprefact, "ffprefact"=>$ffprefact, "dprefact"=>$diasprefact, "fiped"=>$fiped, "ffped"=>$ffped, "dped"=>$diasped, "filic"=>$filic, "fflic"=>$fflic, "dlic"=>$diaslic, "fican"=>$fican, "ffcan"=>$ffcan, "dcan"=>$diascan, "fipro"=>$fipro, "ffpro"=>$ffpro, "dpro"=>$diaspro, "fipre"=>$fipre, "ffpre"=>$ffpre, "dpre"=>$diaspre, "fiint"=>$fiint, "ffint"=>$ffint, "diasint"=>$diasint, "ficons"=>$ficons, "ffcons"=>$ffcons, "dcons"=>$diascons, "fiseg"=>$fiseg, "ffseg"=>$ffseg, "diasseg"=>$diasseg, "swinm"=>$modainm, "swvis"=>$modavis, "swcom"=>$modacom, "swcon"=>$modacon, "swdis"=>$modadis, "swprefact"=>$modaprefact, "swped"=>$modaped, "swlic"=>$modalic, "swcan"=>$modacan, "swpro"=>$modapro, "swpre"=>$modapre, "swint"=>$modaint, "swcons"=>$modacons, "swseg"=>$modaseg);
echo json_encode($myArray);
?>