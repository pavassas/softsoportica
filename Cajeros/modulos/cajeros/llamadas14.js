function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function MODULO(v,e)
{	
	if(v == 'CAJEROS')
	{
		window.location.href = "cajeros.php";
	}
	else
	if(v == 'TODOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('cajeros').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#cajeros").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?todos=si&est="+e,true);
		ajax.send(null);
		setTimeout("REFRESCARLISTAS()",1000);
	}
	OCULTARSCROLL();
}
function GUARDAR()
{		
	var swbanco = $('input[name=tipocajero]:checked', '#form1').val();
	var nomcaj = form1.nombrecajero.value;
	var dir = form1.direccion.value;
	var anocon = form1.anocontable.value;
	var reg = form1.region.value;
	var tpr = form1.tipoproyecto.value;
	var dep = form1.departamento.value;
	var mun = form1.municipio.value;
	var modini = form1.modalidadini.value;
	var tip = form1.tipologia.value;
	var tipint = form1.tipointervencion.value;
	var moda = form1.modalidad.value;
	var codcaj = form1.codigocajero.value;
	var cencos = form1.centrocostos.value;
	var refmaq = form1.referenciamaquina.value;
	var ubi = form1.ubicacion.value;
	var codsuc = form1.codigosuc.value;
	var ubiamt = form1.ubicacionamt.value;
	var ati = form1.atiende.value;
	var rie = form1.riesgo.value;
	var are = form1.area.value;
	var fecapagadoatm = form1.fechaapagadoatm.value;
	var fecinicaj = form1.fechainiciocajero.value;
	var reqinm = $('input[name=requiereinmobiliaria]:checked', '#form1').val();
	var nominm = form1.nombreinmobiliaria.value;
	var feciniinm = form1.fechainicioinmobiliaria.value;
	var reqvis = $('input[name=requierevisita]:checked', '#form1').val();
	var nomvis = form1.nombrevisita.value;
	var fecinivis = form1.fechainiciovisita.value;
	var reqdis = $('input[name=requierediseno]:checked', '#form1').val();
	var nomdis = form1.nombrediseno.value;
	var fecinidis = form1.fechainiciodiseno.value;
	var reqlic = $('input[name=requierelicencia]:checked', '#form1').val();
	var nomlic = form1.nombregestionador.value;
	var fecinilic = form1.fechainiciolicencia.value;
	var nomint = form1.nombreinterventoria.value;
	var fecteoint = $('#teoricainterventoria').val();//form1.teoricainterventoria.value;
	var fecaprocom = form1.fechaaprovacioncomite.value;
	var nomcons = form1.nombreconstructor.value;
	var nominsseg = form1.nombreinstaladorseguridad.value;
	var estpro = form1.estadoproyecto.value;
	var not = form1.nota.value;
	var cancom = form1.canalcomunicacion.value;
	var lintel = form1.lineatelefonica.value;
	
	var fecteoinm = form1.teoricainmobiliaria.value;
	var fecteovis = form1.teoricavisita.value;
	var fecteodis = form1.teoricadiseno.value;
	var fecteolic = form1.teoricalicencia.value;
	
	var che = form1.padre.checked;
	var hijos = "";
	var objCBarray = document.getElementsByName('selectItemhijos');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	hijos += objCBarray[i].value + ",";
	    }
	}
	
	//DATOS BANCOLOMBIA
	//COMITE
	var fecinicom = form1.fechainiciocomite.value;
	var comab = $('input[name=comiteaprobadobancolombia]:checked', '#form1').val();
	var comfitb = form1.fechainicioteoricacomitebancolombia.value;
	var comfetb = form1.fechaentregateoricacomitebancolombia.value;
	var comfcb = form1.fechacomitebancolombia.value;
	var comnotb = form1.notacomite.value;
	var reqcontrato = $('input[name=requierecontrato]:checked', '#form1').val();
	var reqdiseno = $('input[name=requierediseno1]:checked', '#form1').val();
	var reqcanal = $('input[name=requierecanal]:checked', '#form1').val();
	//CONTRATO
	var conab = $('input[name=contratoaprobadobancolombia]:checked', '#form1').val();
	var coneb = form1.estadocontratobancolombia.value;
	var confitb = form1.fechainicioteoricacontratobancolombia.value;
	var confetb = form1.fechaentregateoricacontratobancolombia.value;
	var confeb = form1.fechaentregacontratobancolombia.value;
	var connotb = form1.notacontrato.value;
	//PREFACTIBILIDAD CANAL
	var prefitb = form1.fechainicioteoricaprefactibilidadbancolombia.value;
	var prefetb = form1.fechaentregateoricaprefactibilidadbancolombia.value;
	var prefeb = form1.fechaentregaprefactibilidadbancolombia.value;
	var preab = $('input[name=prefactibilidadaprobadobancolombia]:checked', '#form1').val();
	var prenotb = form1.notaprefactibilidad.value;
	//PEDIDO MAQUINA
	var pedm = form1.pedidomaquinabancolombia.value;
	var pedab = $('input[name=pedidomaquinaaprobadobancolombia]:checked', '#form1').val();
	var pednotb = form1.notapedidomaquina.value;
	//PROYECCI�N INSTALACI�N
	var profitb = form1.fechainicioteoricaproyeccionbancolombia.value;
	var profetb = form1.fechaentregateoricaproyeccionbancolombia.value;
	var pronotb = form1.notaproyeccion.value;
	var fiproyeccion = form1.fechainicioproyeccion.value;
	//CANAL
	var canfitb = form1.fechainicioteoricacanalbancolombia.value;
	var canfetb = form1.fechaentregateoricacanalbancolombia.value;
	var canfib = form1.fechainiciocanalbancolombia.value;
	var canfeb = form1.fechaentregacanalbancolombia.value;
	var cannotb = form1.notacanal.value;
	
	//NUEVO PEDIDO MAQUINA
	var propm = $('#proyectopm').val();
	var codconpm = $('#codigocontablepm').val();
	var contapm = $('#contactopm').val();
	var contrapm = $('#contratistapm').val();
	var direntpm = $('#direccionentregapm').val();
	var horentpm = $('#horaentregapm').val();
	var fecentpm = $('#fechaentregapm').val();
	var maqpm = $('#maquinapm').val();
	var refmaqpm = $('#referenciamaquinapm').val();
	var retgavpm = $('#retirogavetaspm').val();
	
	if(nomcaj == '')
	{
		alert("Debe ingresar el nombre del cajero");
	}
	else
	if(anocon == '')
	{
		alert("Debe ingresar el ano contable");
	}
	else
	if(reg == '')
	{
		alert("Debe elegir la region");
	}
	else
	if(dep == '')
	{
		alert("Debe elegir el departamento");
	}
	else
	if(mun == '')
	{
		alert("Debe elegir el municipio");
	}
	else
	if(fecinicaj == '')
	{
		alert("Debe seleccionar la fecha de inicio");
	}
	else
	{
		var nuevonombre = CORREGIRTEXTO(nomcaj);
		var nuevadireccion = CORREGIRTEXTO(dir);
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('masnotas').innerHTML=ajax.responseText;
	   		     document.getElementById('masnotas1').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#masnotas").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		jQuery("#masnotas1").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />");
		ajax.open("GET","?guardardatos=si&nomcaj="+nuevonombre+"&swbanco="+swbanco+"&dir="+nuevadireccion+"&anocon="+anocon+"&reg="+reg+"&dep="+dep+"&mun="+mun+"&tip="+tip+"&tipint="+tipint+"&moda="+moda+"&codcaj="+codcaj+"&cencos="+cencos+"&refmaq="+refmaq+"&ubi="+ubi+"&codsuc="+codsuc+"&ubiamt="+ubiamt+"&ati="+ati+"&rie="+rie+"&are="+are+"&fecapagadoatm="+fecapagadoatm+"&fecinicaj="+fecinicaj+"&reqinm="+reqinm+"&nominm="+nominm+"&feciniinm="+feciniinm+"&reqvis="+reqvis+"&nomvis="+nomvis+"&fecinivis="+fecinivis+"&reqdis="+reqdis+"&nomdis="+nomdis+"&fecinidis="+fecinidis+"&reqlic="+reqlic+"&nomlic="+nomlic+"&fecinilic="+fecinilic+"&nomint="+nomint+"&fecteoint="+fecteoint+"&fecaprocom="+fecaprocom+"&nomcons="+nomcons+"&nominsseg="+nominsseg+"&estpro="+estpro+"&not="+not+"&hij="+hijos+"&che="+che+"&fecteoinm="+fecteoinm+"&fecteovis="+fecteovis+"&fecteodis="+fecteodis+"&fecteolic="+fecteolic/*BANCOLOMBIA: */+"&comab="+comab+"&comfitb="+comfitb+"&comfetb="+comfetb+"&comfcb="+comfcb+"&conab="+conab+"&coneb="+coneb+"&confitb="+confitb+"&confetb="+confetb+"&confeb="+confeb+"&prefitb="+prefitb+"&prefetb="+prefetb+"&prefeb="+prefeb+"&preab="+preab+"&pedm="+pedm+"&pedab="+pedab+"&profitb="+profitb+"&profetb="+profetb+"&canfitb="+canfitb+"&canfetb="+canfetb+"&canfib="+canfib+"&canfeb="+canfeb+"&comnotb="+comnotb+"&connotb="+connotb+"&prenotb="+prenotb+"&pednotb="+pednotb+"&pronotb="+pronotb+"&cannotb="+cannotb+"&modini="+modini+"&fecinicom="+fecinicom+"&fiproyeccion="+fiproyeccion+"&propm="+propm+"&codconpm="+codconpm+"&contapm="+contapm+"&contrapm="+contrapm+"&direntpm="+direntpm+"&horentpm="+horentpm+"&fecentpm="+fecentpm+"&maqpm="+maqpm+"&refmaqpm="+refmaqpm+"&retgavpm="+retgavpm+"&reqcontrato="+reqcontrato+"&reqdiseno="+reqdiseno+"&reqcanal="+reqcanal+"&tpr="+tpr+"&cancom="+cancom+"&lintel="+lintel,true);
		ajax.send(null);
		
		REFRESCARACTIVOS();
	}
}
function ACTUALIZARINFORMACION(v)
{		
	var swbanco = $('input[name=tipocajero]:checked', '#form1').val();
	var nomcaj = form1.nombrecajero.value;
	var dir = form1.direccion.value;
	var anocon = form1.anocontable.value;
	var reg = form1.region.value;
	var tpr = form1.tipoproyecto.value;
	var dep = form1.departamento.value;
	var mun = form1.municipio.value;
	var modini = form1.modalidadini.value;
	var tip = form1.tipologia.value;
	var tipint = form1.tipointervencion.value;
	var moda = form1.modalidad.value;
	var codcaj = form1.codigocajero.value;
	var cencos = form1.centrocostos.value;
	var refmaq = form1.referenciamaquina.value;
	var ubi = form1.ubicacion.value;
	var codsuc = form1.codigosuc.value;
	var ubiamt = form1.ubicacionamt.value;
	var ati = form1.atiende.value;
	var rie = form1.riesgo.value;
	var are = form1.area.value;
	var fecapagadoatm = form1.fechaapagadoatm.value;
	var fecinicaj = form1.fechainiciocajero.value;
	var reqinm = $('input[name=requiereinmobiliaria]:checked', '#form1').val();
	var nominm = form1.nombreinmobiliaria.value;
	var feciniinm = form1.fechainicioinmobiliaria.value;
	var reqvis = $('input[name=requierevisita]:checked', '#form1').val();
	var nomvis = form1.nombrevisita.value;
	var fecinivis = form1.fechainiciovisita.value;
	var reqdis = $('input[name=requierediseno]:checked', '#form1').val();
	var nomdis = form1.nombrediseno.value;
	var fecinidis = form1.fechainiciodiseno.value;
	var reqlic = $('input[name=requierelicencia]:checked', '#form1').val();
	var nomlic = form1.nombregestionador.value;
	var fecinilic = form1.fechainiciolicencia.value;
	var nomint = form1.nombreinterventoria.value;
	var fecteoint = $('#teoricainterventoria').val();//form1.teoricainterventoria.value;
	var fecaprocom = form1.fechaaprovacioncomite.value;
	var nomcons = form1.nombreconstructor.value;
	var nominsseg = form1.nombreinstaladorseguridad.value;
	var estpro = form1.estadoproyecto.value;
	var cancom = form1.canalcomunicacion.value;
	var lintel = form1.lineatelefonica.value;
	
	var fecteoinm = form1.teoricainmobiliaria.value;
	var fecteovis = form1.teoricavisita.value;
	var fecteodis = form1.teoricadiseno.value;
	var fecteolic = form1.teoricalicencia.value;
	
	var aprobesquema = $('input[name=aprobaresquema]:checked', '#form1').val();
	var aprobppt = $('input[name=aprobarppt]:checked', '#form1').val();
	
	var che = form1.padre.checked;
	var hijos = "";
	var objCBarray = iframe2.document.getElementsByName('selectItemhijos');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	hijos += objCBarray[i].value + ",";
	    }
	}
	
	//DATOS BANCOLOMBIA
	//COMITE
	var fecinicom = form1.fechainiciocomite.value;
	var comab = $('input[name=comiteaprobadobancolombia]:checked', '#form1').val();
	var comfitb = form1.fechainicioteoricacomitebancolombia.value;
	var comfetb = form1.fechaentregateoricacomitebancolombia.value;
	var comfcb = form1.fechacomitebancolombia.value;
	var comnotb = form1.notacomite.value;
	var reqcontrato = $('input[name=requierecontrato]:checked', '#form1').val();
	var reqcanal = $('input[name=requierecanal]:checked', '#form1').val();
	var reqdiseno = $('input[name=requierediseno1]:checked', '#form1').val();
	//CONTRATO
	var conab = $('input[name=contratoaprobadobancolombia]:checked', '#form1').val();
	var coneb = form1.estadocontratobancolombia.value;
	var confitb = form1.fechainicioteoricacontratobancolombia.value;
	var confetb = form1.fechaentregateoricacontratobancolombia.value;
	var confeb = form1.fechaentregacontratobancolombia.value;
	var connotb = form1.notacontrato.value;
	//PREFACTIBILIDAD CANAL
	var prefitb = form1.fechainicioteoricaprefactibilidadbancolombia.value;
	var prefetb = form1.fechaentregateoricaprefactibilidadbancolombia.value;
	var prefeb = form1.fechaentregaprefactibilidadbancolombia.value;
	var preab = $('input[name=prefactibilidadaprobadobancolombia]:checked', '#form1').val();
	var prenotb = form1.notaprefactibilidad.value;
	//PEDIDO MAQUINA
	var pedm = form1.pedidomaquinabancolombia.value;
	var pedab = $('input[name=pedidomaquinaaprobadobancolombia]:checked', '#form1').val();
	var pednotb = form1.notapedidomaquina.value;
	//PROYECCI�N INSTALACI�N
	var profitb = form1.fechainicioteoricaproyeccionbancolombia.value;
	var profetb = form1.fechaentregateoricaproyeccionbancolombia.value;
	var pronotb = form1.notaproyeccion.value;
	var fiproyeccion = form1.fechainicioproyeccion.value;
	//CANAL
	var canfitb = form1.fechainicioteoricacanalbancolombia.value;
	var canfetb = form1.fechaentregateoricacanalbancolombia.value;
	var canfib = form1.fechainiciocanalbancolombia.value;
	var canfeb = form1.fechaentregacanalbancolombia.value;
	var cannotb = form1.notacanal.value;
	
	//NUEVO PEDIDO MAQUINA
	var propm = $('#proyectopm1').val();
	var codconpm = $('#codigocontablepm1').val();
	var contapm = $('#contactopm1').val();
	var contrapm = $('#contratistapm1').val();
	var direntpm = $('#direccionentregapm1').val();
	var horentpm = $('#horaentregapm1').val();
	var fecentpm = $('#fechaentregapm1').val();
	var maqpm = $('#maquinapm1').val();
	var refmaqpm = $('#referenciamaquinapm1').val();
	var retgavpm = $('#retirogavetaspm1').val();
	
	if(nomcaj == '')
	{
		alert("Debe ingresar el nombre del cajero");
	}
	else
	if(anocon == '')
	{
		alert("Debe ingresar el ano contable");
	}
	else
	if(reg == '')
	{
		alert("Debe elegir la region");
	}
	else
	if(dep == '')
	{
		alert("Debe elegir el departamento");
	}
	else
	if(mun == '')
	{
		alert("Debe elegir el municipio");
	}
	else
	if(fecinicaj == '')
	{
		alert("Debe seleccionar la fecha de inicio");
	}
	else
	{
		var nuevonombre = CORREGIRTEXTO(nomcaj);
		var nuevadireccion = CORREGIRTEXTO(dir);
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('estadoregistro').innerHTML=ajax.responseText;
	   		     document.getElementById('estadoregistro1').innerHTML=ajax.responseText;
		    }
			else
			{
				//EDITAR(v);
			}
		}
		jQuery("#estadoregistro").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		jQuery("#estadoregistro1").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />");
		ajax.open("GET","?actualizardatos=si&nomcaj="+nuevonombre+"&swbanco="+swbanco+"&dir="+nuevadireccion+"&anocon="+anocon+"&reg="+reg+"&dep="+dep+"&mun="+mun+"&tip="+tip+"&tipint="+tipint+"&moda="+moda+"&codcaj="+codcaj+"&cencos="+cencos+"&refmaq="+refmaq+"&ubi="+ubi+"&codsuc="+codsuc+"&ubiamt="+ubiamt+"&ati="+ati+"&rie="+rie+"&are="+are+"&fecapagadoatm="+fecapagadoatm+"&fecinicaj="+fecinicaj+"&reqinm="+reqinm+"&nominm="+nominm+"&feciniinm="+feciniinm+"&reqvis="+reqvis+"&nomvis="+nomvis+"&fecinivis="+fecinivis+"&reqdis="+reqdis+"&nomdis="+nomdis+"&fecinidis="+fecinidis+"&reqlic="+reqlic+"&nomlic="+nomlic+"&fecinilic="+fecinilic+"&nomint="+nomint+"&fecteoint="+fecteoint+"&fecaprocom="+fecaprocom+"&nomcons="+nomcons+"&nominsseg="+nominsseg+"&estpro="+estpro+"&clacaj="+v+"&hij="+hijos+"&che="+che+"&fecteoinm="+fecteoinm+"&fecteovis="+fecteovis+"&fecteodis="+fecteodis+"&fecteolic="+fecteolic/*BANCOLOMBIA: */+"&comab="+comab+"&comfitb="+comfitb+"&comfetb="+comfetb+"&comfcb="+comfcb+"&conab="+conab+"&coneb="+coneb+"&confitb="+confitb+"&confetb="+confetb+"&confeb="+confeb+"&prefitb="+prefitb+"&prefetb="+prefetb+"&prefeb="+prefeb+"&preab="+preab+"&pedm="+pedm+"&pedab="+pedab+"&profitb="+profitb+"&profetb="+profetb+"&canfitb="+canfitb+"&canfetb="+canfetb+"&canfib="+canfib+"&canfeb="+canfeb+"&comnotb="+comnotb+"&connotb="+connotb+"&prenotb="+prenotb+"&pednotb="+pednotb+"&pronotb="+pronotb+"&cannotb="+cannotb+"&modini="+modini+"&fecinicom="+fecinicom+"&fiproyeccion="+fiproyeccion+"&propm="+propm+"&codconpm="+codconpm+"&contapm="+contapm+"&contrapm="+contrapm+"&direntpm="+direntpm+"&horentpm="+horentpm+"&fecentpm="+fecentpm+"&maqpm="+maqpm+"&refmaqpm="+refmaqpm+"&retgavpm="+retgavpm+"&reqcontrato="+reqcontrato+"&reqcanal="+reqcanal+"&reqdiseno="+reqdiseno+"&aprobesquema="+aprobesquema+"&aprobppt="+aprobppt+"&tpr="+tpr+"&cancom="+cancom+"&lintel="+lintel,true);
		ajax.send(null);
		REFRESCARACTIVOS();
	}
}

//DB->Eliminar linea base (Solicitar motivo)
function ELIMINARLINEABASE(v)
{
	var rec = $("#motivo").val();
	if (v == "")
		v = $("#clavecajero").val();

	if (stringTrim(rec) != "")
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
			{
				document.getElementById('recu').innerHTML=ajax.responseText;
			}
			else
			{
				var closeButton = $(".close-reveal-modal");
				if (closeButton.length > 0)
				{
					closeButton[0].click();
				}
			}
		}
		jQuery("#recu").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?eliminarlineabase=si&motivo="+rec+"&clacaj="+v, true);
		ajax.send(null);
	}
	else
	{
		document.getElementById('recu').innerHTML = "<div class='validaciones' style='width: 100%' align='center'>Debe ingresar el motivo</div>";
	}
}

function CORREGIRTEXTO(v)
{
	var res = v.replace('#','REEMPLAZARNUMERAL');
	var res = res.replace('+','REEMPLAZARMAS');
	
	return res;
}
function REFRESCARACTIVOS()
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('activos').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#activos").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?refrescaractivos=si",true);
	ajax.send(null);
}
function CALCULARMODALIDAD1()
{
	var moda = form1.modalidadini.value;
	var fec = form1.fechainiciocajero.value;
	
	if(moda != '')
	{
		$.post('calcularfechas.php', { moda: moda,fecini: fec }, function(data){
			if(data != "")
			{
				for(var j=0;j<data.length;j++)
				{
					var swinm = data[j].swinm;
					var swvis = data[j].swvis;
					var swcom = data[j].swcom;
					
					var fiinm = data[j].fiinm;
					var ffinm = data[j].ffinm;
					var dinm = data[j].dinm;
					
					var fivis = data[j].fivis;
					var ffvis = data[j].ffvis;
					var dvis = data[j].dvis;
					
					var ficom = data[j].ficom;
					var ffcom = data[j].ffcom;
					var dcom = data[j].dcom;					
					
					if(swinm == 0)
					{
						document.getElementById('seccioninmobiliaria').style.display = 'none';
						form1.requiereinmobiliaria[0].checked = false;
						form1.requiereinmobiliaria[1].checked = true;
						form1.nombreinmobiliaria.disabled = true;
						form1.nombreinmobiliaria.value = "";
						form1.fechainicioinmobiliaria.disabled = true;
						form1.fechainicioinmobiliaria.value = "";
					}
					else
					{
						document.getElementById('seccioninmobiliaria').style.display = 'block';
						form1.requiereinmobiliaria[0].checked = true;
						form1.requiereinmobiliaria[1].checked = false;
						form1.nombreinmobiliaria.disabled = false;
						//form1.fechainicioinmobiliaria.disabled = false;
						
						form1.fechainicioinmobiliaria.value = fiinm;
						form1.teoricainmobiliaria.value = ffinm;
						form1.totaldiasinmobiliaria.value = dinm;
					}
					
					if(swvis == 0)
					{
						document.getElementById('seccionvisita').style.display = 'none';
						form1.requierevisita[0].checked = false;
						form1.requierevisita[1].checked = true;
						form1.nombrevisita.disabled = true;
						form1.nombrevisita.value = "";
					}
					else
					{
						document.getElementById('seccionvisita').style.display = 'block';
						form1.requierevisita[0].checked = true; 
						form1.requierevisita[1].checked = false; 
						form1.nombrevisita.disabled = false;
						
						form1.fechainiciovisita.value = fivis;
						form1.teoricavisita.value = ffvis;
						form1.totaldiasvisita.value = dvis;
					}
					if(swcom == 1)
					{
						form1.fechainiciocomitelb.value = ficom;
						form1.fechateoricacomitelb.value = ffcom;
						form1.totaldiascomitelb.value = dcom;
					}
				}
			}
		},"json");
	}
}
function CALCULARMODALIDAD2()
{
	var moda = form1.modalidad.value;
	var fec = form1.fechainiciocomite.value;
	if(moda != '' && fec != '')
	{
		$.post('calcularfechas.php', { moda: moda,fecini: fec }, function(data){
			if(data != "")
			{
				for(var j=0;j<data.length;j++)
				{
					var swcom = data[j].swcom;
					var swcon = data[j].swcon;
					var swdis = data[j].swdis;
					var swprefact = data[j].swprefact;
					var swped = data[j].swped;
					var swlic = data[j].swlic;
					var swcan = data[j].swcan;
					var swpro = data[j].swpro;
					var swpre = data[j].swpre;
					var swint = data[j].swint;
					var swcons = data[j].swcons;
					var swseg = data[j].swseg;
										
					var ficom = data[j].ficom;
					var ffcom = data[j].ffcom;
					var dcom = data[j].dcom;
					
					var ficon = data[j].ficon;
					var ffcon = data[j].ffcon;
					var dcon = data[j].dcon;
					
					var fidis = data[j].fidis;
					var ffdis = data[j].ffdis;
					var ddis = data[j].ddis;
					
					var fiprefact = data[j].fiprefact;
					var ffprefact = data[j].ffprefact;
					var dprefact = data[j].dprefact;
					
					var fiped = data[j].fiped;
					var ffped = data[j].ffped;
					var dped = data[j].dped;
					
					var filic = data[j].filic;
					var fflic = data[j].fflic;
					var dlic = data[j].dlic;
					
					var fican = data[j].fican;
					var ffcan = data[j].ffcan;
					var dcan = data[j].dcan;
					
					var fipro = data[j].fipro;
					var ffpro = data[j].ffpro;
					var dpro = data[j].dpro;
					
					var fipre = data[j].fipre;
					var ffpre = data[j].ffpre;
					var dpre = data[j].dpre;
					
					var fiint = data[j].fiint;
					var ffint = data[j].ffint;
					var dint = data[j].diasint;
					
					var ficons = data[j].ficons;
					var ffcons = data[j].ffcons;
					var dcons = data[j].dcons;
					
					var fiseg = data[j].fiseg;
					var ffseg = data[j].ffseg;
					var dseg = data[j].diasseg;
					
					/*if(swcom == 0)
					{
					}
					else
					{						
						form1.fechainiciocomitelb.value = ficom;
						form1.fechateoricacomitelb.value = ffcom;
						form1.totaldiascomitelb.value = dcom;
					}*/
					
					if(swcon == 0)
					{
						//document.getElementById('seccioncontrato').style.display = 'none';
					}
					else
					{
						//document.getElementById('seccioncontrato').style.display = 'block';
						form1.fechainicioteoricacontratobancolombia.value = ficon;
						form1.fechaentregateoricacontratobancolombia.value = ffcon;
						form1.totaldiascontrato.value = dcon;
					}
					if(swdis == 0)
					{
						//document.getElementById('secciondiseno').style.display = 'none';
					}
					else
					{
						//document.getElementById('secciondiseno').style.display = 'block';
						form1.requierediseno[0].checked = true; 
						form1.requierediseno[1].checked = false; 
						form1.nombrediseno.disabled = false;
						form1.fechainiciodiseno.value = fidis;
						form1.teoricadiseno.value = ffdis;
						form1.totaldiasdiseno.value = ddis;
					}
					if(swprefact == 0)
					{
						//document.getElementById('seccionprefactibilidad').style.display = 'none';
					}
					else
					{
						//document.getElementById('seccionprefactibilidad').style.display = 'block';
						form1.fechainicioteoricaprefactibilidadbancolombia.value = fiprefact;
						form1.fechaentregateoricaprefactibilidadbancolombia.value = ffprefact;
						form1.totaldiasprefactibilidad.value = dprefact;
					}
					if(swped == 0)
					{
						document.getElementById('seccionpedidomaquina').style.display = 'none';
					}
					else
					{
						document.getElementById('seccionpedidomaquina').style.display = 'block';
						form1.fechainiciopedidolb.value = fiped;
						form1.fechateoricapedidolb.value = ffped;
						form1.totaldiaspedidolb.value = dped;
					}
					if(swlic == 0)
					{
						document.getElementById('seccionlicencia').style.display = 'none';
					}
					else
					{
						document.getElementById('seccionlicencia').style.display = 'block';
						form1.requierelicencia[0].checked = true; 
						form1.requierelicencia[1].checked = false; 
						form1.nombregestionador.disabled = false;
						form1.fechainiciolicencia.value = filic;
						form1.teoricalicencia.value = fflic;
						form1.totaldiaslicencia.value = dlic;
					}
					if(swcan == 0)
					{
						//document.getElementById('seccioncanal').style.display = 'none';
					}
					else
					{
						//document.getElementById('seccioncanal').style.display = 'block';
						form1.fechainicioteoricacanalbancolombia.value = fican;
						form1.fechaentregateoricacanalbancolombia.value = ffcan;
						form1.totaldiascanal.value = dcan;
					}
					if(swpro == 0)
					{
						document.getElementById('seccionproyeccion').style.display = 'none';
					}
					else
					{
						document.getElementById('seccionproyeccion').style.display = 'block';
						form1.fechainicioteoricaproyeccionbancolombia.value = fipro;
						form1.fechaentregateoricaproyeccionbancolombia.value = ffpro;
						form1.totaldiasproyeccion.value = dpro;
					}
					if(swint == 0)
					{
						document.getElementById('seccioninterventoria').style.display = 'none';
					}
					else
					{
						document.getElementById('seccioninterventoria').style.display = 'block';
						form1.fechainiciointerventorialb.value = fiint;
						form1.fechateoricainterventorialb.value = ffint;
						form1.totaldiasinterventorialb.value = dint;
					}
					if(swcons == 0)
					{
						document.getElementById('seccionconstructor').style.display = 'none';
					}
					else
					{
						document.getElementById('seccionconstructor').style.display = 'block';
						form1.fechainicioconstructorlb.value = ficons;
						form1.fechateoricaconstructorlb.value = ffcons;
						form1.totaldiasconstructorlb.value = dcons;
					}
					if(swseg == 0)
					{
						document.getElementById('seccioninstalador').style.display = 'none';
					}
					else
					{
						document.getElementById('seccioninstalador').style.display = 'block';
						form1.fechainicioseguridadlb.value = fiseg;
						form1.fechateoricaseguridadlb.value = ffseg;
						form1.totaldiasseguridadlb.value = dseg;
					}
				}
			}
		},"json");
	}
}
function CALCULARFECHAS(o)
{
	var moda = form1.modalidad.value;
	var modinm = form1.modainm.value;
	var modvis = form1.modavis.value;
	var moddis = form1.modadis.value;
	var modlic = form1.modalic.value;
	var modint = form1.modaint.value;
	
	var fecini = $("#fechainiciocajero").val();
	$.post('calcularfechas.php', { moda: moda,fecini: fecini }, function(data){
		if(data != "")
		{
			for(var j=0;j<data.length;j++)
			{
				alert("Inmobiliaria: "+data[j].fiinm+" "+data[j].ffinm+" "+data[j].dinm);
				alert("Visita: "+data[j].fivis+" "+data[j].ffvis+" "+data[j].dvis);
				alert("Comite: "+data[j].ficom+" "+data[j].ffcom+" "+data[j].dcom);
				alert("Contrato: "+data[j].ficon+" "+data[j].ffcon+" "+data[j].dcon);
				alert("Dise�o: "+data[j].fidis+" "+data[j].ffdis+" "+data[j].ddis);
				alert("Prefactibilidad: "+data[j].fiprefact+" "+data[j].ffprefact+" "+data[j].dprefact);
				alert("Pedido maquina: "+data[j].fiped+" "+data[j].ffped+" "+data[j].dped);
				alert("Licencia: "+data[j].filic+" "+data[j].fflic+" "+data[j].dlic);
				alert("Canal: "+data[j].fican+" "+data[j].ffcan+" "+data[j].dcan);
				alert("Proyeccion: "+data[j].fipro+" "+data[j].ffpro+" "+data[j].dpro);
				alert("Preliminar: "+data[j].fipre+" "+data[j].ffpre+" "+data[j].dpre);
				alert("Constructor: "+data[j].ficons+" "+data[j].ffcons+" "+data[j].dcons);
			}
		}
	},"json");

	if(modinm == 1)
	{
		$("#fechainicioinmobiliaria").val(fecini);
	}
	else
	if(modinm == 0 && modvis == 1)
	{
		$("#fechainiciovisita").val(fecini);
	}
	else
	if(modinm == 0 && modvis == 0 && moddis == 1)
	{
		$("#fechainiciodiseno").val(fecini);
	}
	else
	if(modinm == 0 && modvis == 0 && moddis == 0 && modlic == 1)
	{
		$("#fechainiciolicencia").val(fecini);
	}
	
	setTimeout("CALCULARFECHATEORICA('teoricainmobiliaria','INICAJ')",500);
	setTimeout("CALCULARTOTALDIAS('diastotalinmobiliaria','INICAJ')",500);
	setTimeout("CALCULARDIASACTOR('diasinmobiliaria')",500);
	
	setTimeout("CALCULARFECHATEORICA('teoricavisita','INICAJ')",500);
	setTimeout("CALCULARTOTALDIAS('diastotalvisita','INICAJ')",500);
	setTimeout("CALCULARDIASACTOR('diasvisita')",500);
	
	setTimeout("CALCULARFECHATEORICA('teoricadiseno','INICAJ')",500);
	setTimeout("CALCULARTOTALDIAS('diastotaldiseno','INICAJ')",500);
	setTimeout("CALCULARDIASACTOR('diasdiseno')",500);
	
	setTimeout("CALCULARFECHATEORICA('teoricalicencia','INICAJ')",500);
	setTimeout("CALCULARTOTALDIAS('diastotallicencia','INICAJ')",500);
	setTimeout("CALCULARDIASACTOR('diaslicencia')",500);
	
	setTimeout("CALCULARFECHATEORICA('teoricainterventoria','INICAJ')",500);
	
	if(modinm == 0){ form1.fechainicioinmobiliaria.value = ''; form1.teoricainmobiliaria.value = ''; form1.totaldiasinmobiliaria.value = ''; }
	if(modvis == 0){ form1.fechainiciovisita.value = ''; form1.teoricavisita.value = ''; form1.totaldiasvisita.value = ''; }
	if(moddis == 0){ form1.fechainiciodiseno.value = ''; form1.teoricadiseno.value = ''; form1.totaldiasdiseno.value = ''; }
	if(modlic == 0){ form1.fechainiciolicencia.value = ''; form1.teoricalicencia.value = ''; form1.totaldiaslicencia.value = ''; }
	if(modint == 0){ form1.teoricainterventoria.value = ''; }

	if(moda == '' || fecini == '')
	{
		$("#fechainicioinmobiliaria").val("");
		$("#teoricainmobiliaria").val("");
		$("#totaldiasinmobiliaria").val("");
		$("#fechainiciovisita").val("");
		$("#teoricavisita").val("");
		$("#totaldiasvisita").val("");
		$("#fechainiciodiseno").val("");
		$("#teoricadiseno").val("");
		$("#totaldiasdiseno").val("");
		$("#fechainiciolicencia").val("");
		$("#teoricalicencia").val("");
		$("#totaldiaslicencia").val("");
		$("#teoricainterventoria").val("");
	}
}
function CALCULARFECHATEORICA(o,c)
{
	var moda = form1.modalidad.value;
	
	var modinm = form1.modainm.value;
	var modvis = form1.modavis.value;
	var moddis = form1.modadis.value;
	var modlic = form1.modalic.value;
	var modint = form1.modaint.value;
	var modcons = form1.modacons.value;
	var modcom = form1.modacom.value;
	var modcon = form1.modacon.value;
	var modpre = form1.modapre.value;
	
	if(c == 'INICAJ')
	{
		var feciniinm = $("#fechainiciocajero").val();
		
		if(modinm == 1)
		{
			$("#fechainicioinmobiliaria").val(feciniinm);
		}
		else
		if(modinm == 0 && modvis == 1)
		{
			$("#fechainiciovisita").val(feciniinm);
		}
		else
		if(modinm == 0 && modvis == 0 && moddis == 1)
		{
			$("#fechainiciodiseno").val(feciniinm);
		}
		else
		if(modinm == 0 && modvis == 0 && moddis == 0 && modlic == 1)
		{
			$("#fechainiciolicencia").val(feciniinm);
		}
	}
	
	var dinm = form1.diasinm.value;
	var dvis = form1.diasvis.value;
	var ddis = form1.diasdis.value;
	var dlic = form1.diaslic.value;
	var dcons = form1.diascons.value;
	var dcom = form1.diascom.value;
	var dcon = form1.diascon.value;
	var dpre = form1.diaspre.value;
	
	if(o == 'teoricainmobiliaria')
	{
		if(modinm == 1)
		{
			var fecentinmob = $("#fechaentregainfoinmobiliaria").val();
			
			var dias = dinm;
			if(c == 'INICAJ')
			{
				var fecini = $("#fechainiciocajero").val();
			}
			else
			{
				var fecini = $("#fechainicioinmobiliaria").val();
			}
			
			if(c == 'INICAJ')
			{
				if(fecentinmob != '' && fecentinmob != '0000-00-00')//Si ya se entrego la informacion
				{
					var d = restaFechas(fecini,fecentinmob);
					
					var fechafin = sumaFecha(parseInt(d),fecini);
					if(modvis == 1)
					{
						$("#fechainiciovisita").val(fechafin);//Inicio visita: Es la final de inmobiliaria
					}
					else
					{
						$("#fechainiciovisita").val('');
					}
				}
				else
				{
					var fechafin = sumaFecha(parseInt(dias),fecini);
					if(modvis == 1)
					{
						$("#fechainiciovisita").val(fechafin);//Inicio visita: Es la final de inmobiliaria
					}
					else
					{
						$("#fechainiciovisita").val('');
					}
				}
			}
		}
		else
		{
			var dias = 0;
			var fecini = '';
		}
	}
	else
	if(o == 'teoricavisita')
	{
		if(modvis == 1)
		{
			var fecentinmob = $("#fechaentregainfoinmobiliaria").val();
			var fecentvis = $("#fechaentregavisita").val();
			
			if(c == 'INICAJ')
			{
				var fecini = $("#fechainiciocajero").val();
			}
			else
			{
				var fecini = $("#fechainicioinmobiliaria").val();
			}
			
			var dias = dvis;
			var fecini = $("#fechainiciovisita").val();//Fecha inicio de visita
			if(c == 'INICAJ')
			{
				if(fecentvis != '' && fecentvis != '0000-00-00')//Si ya se entrego la informacion
				{
					var d = restaFechas(fecini,fecentvis);
					
					var fechafin = sumaFecha(parseInt(d)+parseInt(dcom),fecini);
					if(moddis == 1)
					{
						$("#fechainiciodiseno").val(fechafin);//Inicio visita: Es la final de inmobiliaria
					}
					else
					{
						$("#fechainiciodiseno").val('');
					}
				}
				else
				{
					var fechafin = sumaFecha(parseInt(dias)+parseInt(dcom),fecini);
					if(moddis == 1)
					{
						$("#fechainiciodiseno").val(fechafin);//Inicio visita: Es la final de inmobiliaria
					}
					else
					{
						$("#fechainiciodiseno").val('');
					}
				}
			}
		}
		else
		{
			var dias = 0;
			var fecini = '';
		}
	}
	else
	if(o == 'teoricadiseno')
	{
		if(moddis == 1)
		{
			var fecentvis = $("#fechaentregavisita").val();
			var fecentdis = $("#fechaentregainfodiseno").val();
			if(c == 'INICAJ')
			{
				var fecini = $("#fechainiciocajero").val();
			}
			else
			{
				var fecini = $("#fechainicioinmobiliaria").val();
			}
			
			var fechafin = sumaFecha(parseInt(dinm),fecini);//final de inmobiliaria
			
			var dias = ddis;
			var fecini = $("#fechainiciodiseno").val();
			if(c == 'INICAJ')
			{
				if(fecentdis != '' && fecentdis != '0000-00-00')//Si ya se entrego la informacion
				{
					if(modlic == 1)
					{
						var d = restaFechas(fecini,fecentdis);
						$("#fechainiciolicencia").val(fecentdis);
					}
					else
					{
						$("#fechainiciolicencia").val('');
					}
				}
				else
				{
					var fechafin = sumaFecha(parseInt(ddis),fecini);
					if(modlic == 1)
					{
						$("#fechainiciolicencia").val(fechafin);//Inicio visita: Es la final de inmobiliaria
					}
					else
					{
						$("#fechainiciolicencia").val('');
					}
				}
			}
		}
		else
		{
			var dias = 0;
			var fecini = '';
		}
	}
	else
	if(o == 'teoricalicencia')
	{
		if(modlic == 1)
		{
			var fecentdis = $("#fechaentregainfodiseno").val();
			var fecentlic = $("#fechaentregainfolicencia").val();
			if(c == 'INICAJ')
			{
				var fecini = $("#fechainiciocajero").val();
			}
			else
			{
				var fecini = $("#fechainicioinmobiliaria").val();
			}
			var fechafin = sumaFecha(parseInt(dinm),fecini);//final de inmobiliaria
			
			var dias = dlic;
			var fecini = $("#fechainiciolicencia").val();
			
			if(fecentlic != '' && fecentlic != '0000-00-00')//Si ya se entrego la informacion
			{
				var d = restaFechas(fecini,fecentlic);
			}
		}
		else
		{
			var dias = 0;
			var fecini = '';
		}
	}
	else
	if(o == 'teoricainterventoria' && modint == 1)
	{
		var fecentcons = $("#fechaentregaatm").val();
		//Cuando no hay licencia
		if(modlic == 0)
		{
			if(c == 'INICAJ')
			{
				var fecini = $("#fechainiciocajero").val();
			}
			else
			{
				var fecini = $("#fechainicioinmobiliaria").val();
			}
			var fechafin = sumaFecha(parseInt(dinm),fecini);//final de inmobiliaria
			if(modvis == 1)
			{
				var fechafin = sumaFecha(parseInt(dvis),fechafin);
			}
			var fechafin = sumaFecha(parseInt(dcom)+parseInt(dcon)+parseInt(dpre)+parseInt(dcons),fechafin);//Final de dise�o + 5 dias del banco + 30 dias banco + 25 dias de ejecucion
			$("#teoricainterventoria").val(fechafin);
		}
		else
		{
			//Cuando hay licencia
			if(c == 'INICAJ')
			{
				var fecini = $("#fechainiciocajero").val();
			}
			else
			{
				var fecini = $("#fechainicioinmobiliaria").val();
			}
			var fechafin = sumaFecha(parseInt(dinm),fecini);//final de inmobiliaria
			if(modvis == 1)
			{
				var fechafin = sumaFecha(parseInt(dvis),fechafin);
			}
			if(moddis == 1)
			{
				var fechafin = sumaFecha(parseInt(ddis)+parseInt(dcom),fechafin);
			}
			var fechafin = sumaFecha(parseInt(dlic)+parseInt(dcom)+parseInt(dcons),fechafin);//Final de licencia + 25 dias de ejecucion
			$("#teoricainterventoria").val(fechafin);
		}
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById(o).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#"+o).html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarfechateorica=si&fec="+fecini+"&dias="+dias+"&opc="+o,true);
	ajax.send(null);
	
	if(modinm == 0)
	{
		form1.requiereinmobiliaria[0].checked = false;
		form1.requiereinmobiliaria[1].checked = true;
		form1.nombreinmobiliaria.disabled = true;
		form1.nombreinmobiliaria.value = "";
		form1.fechainicioinmobiliaria.disabled = true;
		form1.fechainicioinmobiliaria.value = "";
	}
	else
	{ 
		form1.requiereinmobiliaria[0].checked = true;
		form1.requiereinmobiliaria[1].checked = false;
		form1.nombreinmobiliaria.disabled = false;
		form1.fechainicioinmobiliaria.disabled = false;
	}
	
	if(modvis == 0)
	{ 
		form1.requierevisita[0].checked = false;
		form1.requierevisita[1].checked = true;
		form1.nombrevisita.disabled = true;
		form1.nombrevisita.value = "";
	}
	else
	{ 
		form1.requierevisita[0].checked = true; 
		form1.requierevisita[1].checked = false; 
		form1.nombrevisita.disabled = false;
	}
	
	if(moddis == 0)
	{ 
		form1.requierediseno[0].checked = false;
		form1.requierediseno[1].checked = true;
		form1.nombrediseno.disabled = true;
		form1.nombrediseno.value = "";
	}
	else
	{ 
		form1.requierediseno[0].checked = true; 
		form1.requierediseno[1].checked = false; 
		form1.nombrediseno.disabled = false;
	}
	
	if(modlic == 0)
	{ 
		form1.requierelicencia[0].checked = false;
		form1.requierelicencia[1].checked = true;
		form1.nombregestionador.disabled = true;
		form1.nombregestionador.value = "";
	}
	else
	{ 
		form1.requierelicencia[0].checked = true; 
		form1.requierelicencia[1].checked = false; 
		form1.nombregestionador.disabled = false;
	}
	
	if(moda == '')
	{
		$("#fechainicioinmobiliaria").val("");
		$("#teoricainmobiliaria").val("");
		$("#totaldiasinmobiliaria").val("");
		$("#fechainiciovisita").val("");
		$("#teoricavisita").val("");
		$("#totaldiasvisita").val("");
		$("#fechainiciodiseno").val("");
		$("#teoricadiseno").val("");
		$("#totaldiasdiseno").val("");
		$("#fechainiciolicencia").val("");
		$("#teoricalicencia").val("");
		$("#totaldiaslicencia").val("");
		$("#teoricainterventoria").val("");
	}
}
function restaFechas(f1,f2)
{
	var aFecha1 = f1.split('-'); 
	var aFecha2 = f2.split('-'); 
	var fFecha1 = Date.UTC(aFecha1[0],aFecha1[1]-1,aFecha1[2]); 
	var fFecha2 = Date.UTC(aFecha2[0],aFecha2[1]-1,aFecha2[2]); 
	var dif = fFecha2 - fFecha1;
	var dias = Math.floor(dif / (1000 * 60 * 60 * 24)); 
	return dias;
}
function sumaFecha(d, fecha)
{
	fecha=fecha.replace("-", "/").replace("-", "/");	  
	
	fecha= new Date(fecha);
	fecha.setDate(fecha.getDate()+d);
	
	var anio=fecha.getFullYear();
	var mes= fecha.getMonth()+1;
	var dia= fecha.getDate();
	
	if(mes.toString().length<2)
	{
		mes="0".concat(mes);        
	}    
	
	if(dia.toString().length<2)
	{
		dia="0".concat(dia);        
	}
	return anio+"-"+mes+"-"+dia;
}
function CALCULARTOTALDIAS(o,c)
{
	var modinm = form1.modainm.value;
	var modvis = form1.modavis.value;
	var moddis = form1.modadis.value;
	var modlic = form1.modalic.value;
	var modcons = form1.modacons.value;
	var modcom = form1.modacom.value;
	var modcon = form1.modacon.value;
	var modpre = form1.modapre.value;
	
	var dinm = form1.diasinm.value;
	var dvis = form1.diasvis.value;
	var ddis = form1.diasdis.value;
	var dlic = form1.diaslic.value;
	var dcons = form1.diascons.value;
	var dcom = form1.diascom.value;
	var dcon = form1.diascon.value;
	var dpre = form1.diaspre.value;
	
	if(o == 'diastotalinmobiliaria' && modinm == 1)
	{
		var dias = dinm;
		if(c == 'INICAJ')
		{
			var fecini = $("#fechainiciocajero").val();
		}
		else
		{
			var fecini = $("#fechainicioinmobiliaria").val();
		}
		var fecini = $("#fechainiciocajero").val();
		var fecent = $("#fechaentregainfoinmobiliaria").val();
	}
	else
	if(o == 'diastotalvisita' && modvis == 1)
	{
		var dias = dvis;
		var fecini = $("#fechainiciovisita").val();
		var fecent = $("#fechaentregavisita").val();
	}
	else
	if(o == 'diastotaldiseno' && moddis == 1)
	{
		var dias = ddis;
		var fecini = $("#fechainiciodiseno").val();
		var fecent = $("#fechaentregainfodiseno").val();
	}
	else
	if(o == 'diastotallicencia' && modlic == 1)
	{
		var dias = dlic;
		var fecini = $("#fechainiciolicencia").val();
		var fecent = $("#fechaentregainfolicencia").val();
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById(o).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#"+o).html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrartotaldias=si&fec="+fecini+"&dias="+dias+"&fecent="+fecent+"&campo="+o,true);
	ajax.send(null);
}
function CALCULARDIASACTOR(o)
{
	var dinm = form1.diasinm.value;
	var dvis = form1.diasvis.value;
	var ddis = form1.diasdis.value;
	var dlic = form1.diaslic.value;
	var dcons = form1.diascons.value;
	var dcom = form1.diascom.value;
	var dcon = form1.diascon.value;
	var dpre = form1.diaspre.value;
	
	if(o == 'diasinmobiliaria')
	{
		var fec = dinm;
	}
	else
	if(o == 'diasvisita')
	{
		var fec = dvis;
	}
	else
	if(o == 'diasdiseno')
	{
		var fec = ddis;
	}
	else
	if(o == 'diaslicencia')
	{
		var fec = dlic;
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById(o).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#"+o).html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrardiasactores=si&fec="+fec,true);
	ajax.send(null);
}
function EDITAR(v,m)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('cajeros').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#cajeros").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?editardatos=si&caj="+v,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTAS()",500);
	//setTimeout("MODALIDADACTOR("+m+")",500);
	OCULTARSCROLL();
}
function BUSCAR(v,p)
{	
	var consec = form1.busconsecutivo.value;
	var nom = form1.busnombre.value;
	var anocon = form1.busanocontable.value;
	var cencos = form1.buscentrocostos.value;
	var cod = form1.buscodigo.value;
	var reg = form1.busregion.value;
	var mun = form1.busmunicipio.value;
	var tip = form1.bustipologia.value;
	var tipint = form1.bustipointervencion.value;
	var moda = form1.busmodalidad.value;
	var actor = form1.busactor.value;
	var est = form1.ocultoestado.value;
	
	if(v == 'CAJERO')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('busqueda').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#busqueda").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='50' width='50' />"); //loading gif will be overwrited when ajax have success
		if(p > 0)
		{
			ajax.open("GET","?buscar=si&consec="+consec+"&nom="+nom+"&anocon="+anocon+"&cencos="+cencos+"&cod="+cod+"&reg="+reg+"&mun="+mun+"&tip="+tip+"&tipint="+tipint+"&moda="+moda+"&est="+est+"&actor="+actor+"&page="+p,true);
		}
		else
		{
			ajax.open("GET","?buscar=si&consec="+consec+"&nom="+nom+"&anocon="+anocon+"&cencos="+cencos+"&cod="+cod+"&reg="+reg+"&mun="+mun+"&tip="+tip+"&tipint="+tipint+"&moda="+moda+"&est="+est+"&actor="+actor,true);
		}
		ajax.send(null);
	}
	OCULTARSCROLL();
}
function VERDEPARTAMENTOS(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('departamentos').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#departamentos").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?verdepartamentos=si&reg="+v,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTADEPARTAMENTOS()",800);
}
function VERCIUDADES(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('ciudades').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#ciudades").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?verciudades=si&dep="+v,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTACIUDADES()",800);
}
function VERCIUDADES1(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('ciudades1').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#ciudades1").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?verciudades1=si&reg="+v,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTACIUDADES1()",1000);
}
function VERMODALIDADES(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('modalidades').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#modalidades").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?vermodalidades=si&tii="+v,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTAMODALIDADES()",800);
}
function VERMODALIDADES1(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('modalidades1').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#modalidades1").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?vermodalidades1=si&tii="+v,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTAMODALIDADES1()",800);
}
function MODALIDADACTOR(v)
{
	var fecini = $("#fechainicioinmobiliaria").val();	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('modalidadactor').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#modalidadactor").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?modalidadactor=si&mod="+v,true);
	ajax.send(null);
	setTimeout("CALCULARFECHAS('INMOBILIARIA')",800);
	if(v == '' || fecini == '')
	{
		$("#fechainicioinmobiliaria").val("");
		$("#teoricainmobiliaria").val("");
		$("#totaldiasinmobiliaria").val("");
		$("#fechainiciovisita").val("");
		$("#teoricavisita").val("");
		$("#totaldiasvisita").val("");
		$("#fechainiciodiseno").val("");
		$("#teoricadiseno").val("");
		$("#totaldiasdiseno").val("");
		$("#fechainiciolicencia").val("");
		$("#teoricalicencia").val("");
		$("#totaldiaslicencia").val("");
		$("#teoricainterventoria").val("");
	}
}
function LIMPIAR()
{
	form1.nombrecajero.value = '';
	form1.direccion.value = '';
	form1.region.value = '';
	form1.departamento.value = '';
	form1.municipio.value = '';
	form1.tipologia.value = '';
	form1.tipointervencion.value = '';
	form1.codigocajero.value = '';
	form1.centrocostos.value = '';
	form1.referenciamaquina.value = '';
	form1.ubicacion.value = '';
	form1.codigosuc.value = '';
	form1.ubicacionamt.value = '';
	form1.atiende.value = '';
	form1.riesgo.value = '';
	form1.area.value = '';
	form1.codigorecibido.value = '';
	form1.requiereinmobiliaria.value = '';
	form1.nombreinmobiliaria.value = '';
	form1.fechainicioinmobiliaria.value = '';
	form1.requierevisita.value = '';
	form1.nombrevisita.value = '';
	form1.fechainiciovisita.value = '';
	form1.requierediseno.value = '';
	form1.nombrediseno.value = '';
	form1.fechainiciodiseno.value = '';
	form1.requierelicencia.value = '';
	form1.nombregestionador.value = '';
	form1.fechainiciolicencia.value = '';
	form1.nombreinterventoria.value = '';
	form1.teoricainterventoria.value = '';
	form1.nombreinterventoria.value = '';
	form1.nombreinstaladorseguridad.value = '';
	form1.estadoproyecto.value = '1';
}
function RESULTADOADJUNTO(v,cc)
{
	if(v == 'INMOBILIARIA')
	{
		var div = 'resultadoadjuntoinm';
	}
	else
	if(v == 'VISITA')
	{
		var div = 'resultadoadjuntovis';
	}
	else
	if(v == 'DISENO')
	{
		var div = 'resultadoadjuntodis';
	}
	else
	if(v == 'LICENCIA')
	{
		var div = 'resultadoadjuntolic';
	}
	else
	if(v == 'COMITE')
	{
		var div = 'resultadoadjuntocomite';
	}
	else
	if(v == 'CONTRATO')
	{
		var div = 'resultadoadjuntocontrato';
	}
	else
	if(v == 'PREFACTIBILIDAD')
	{
		var div = 'resultadoadjuntoprefactibilidad';
	}
	else
	if(v == 'PEDIDOMAQUINA')
	{
		var div = 'resultadoadjuntopedidomaquina';
	}
	else
	if(v == 'PROYECCION')
	{
		var div = 'resultadoadjuntoproyeccion';
	}
	else
	if(v == 'CANAL')
	{
		var div = 'resultadoadjuntocanal';
	}
	else
	if(v == 'INFOBASICA')
	{
		var div = 'resultadoadjuntoinfobase';
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById(div).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#"+div).html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?estadoadjunto=si&actor="+v+"&clacaj="+cc,true);
	ajax.send(null);
	OCULTARSCROLL();
}
function MOSTRARRUTA(v)
{
	if(v == 'INMOBILIARIA')
	{
		var div = 'resultadoadjuntoinm';
		var nomadj = form1.adjuntoinm.value;
	}
	else
	if(v == 'VISITA')
	{
		var div = 'resultadoadjuntovis';
		var nomadj = form1.adjuntovis.value;
	}
	else
	if(v == 'DISENO')
	{
		var div = 'resultadoadjuntodis';
		var nomadj = form1.adjuntodis.value;
	}
	else
	if(v == 'LICENCIA')
	{
		var div = 'resultadoadjuntolic';
		var nomadj = form1.adjuntolic.value;
	}
	else
	if(v == 'ADJUNTO')
	{
		var div = 'resultadoadjunto';
		var nomadj = $('#archivoadjunto').val();
	}
	else
	if(v == 'COMITE')
	{
		var div = 'resultadoadjuntocomite';
		var nomadj = form1.adjuntocomite.value;
	}
	else
	if(v == 'CONTRATO')
	{
		var div = 'resultadoadjuntocontrato';
		var nomadj = form1.adjuntocontrato.value;
	}
	else
	if(v == 'PREFACTIBILIDAD')
	{
		var div = 'resultadoadjuntoprefactibilidad';
		var nomadj = form1.adjuntoprefactibilidad.value;
	}
	else
	if(v == 'PEDIDOMAQUINA')
	{
		var div = 'resultadoadjuntopedidomaquina';
		var nomadj = form1.adjuntopedidomaquina.value;
	}
	else
	if(v == 'PROYECCION')
	{
		var div = 'resultadoadjuntoproyeccion';
		var nomadj = form1.adjuntoproyeccion.value;
	}
	else
	if(v == 'CANAL')
	{
		var div = 'resultadoadjuntocanal';
		var nomadj = form1.adjuntocanal.value;
	}
	else
	if(v == 'INFOBASICA')
	{
		var div = 'resultadoadjuntoinfobase';
		var nomadj = form1.adjuntoinfobase.value;
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById(div).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#"+div).html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarruta=si&nomadj="+nomadj,true);
	ajax.send(null);
	OCULTARSCROLL();
}
function MOSTRARADJUNTOS(c,a)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('adjuntoarchivos').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#adjuntoarchivos").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostraradjuntos=si&clacaj="+c+"&actor="+a,true);
	ajax.send(null);
}
function VERIFICAROTRO(v)
{
	if(v == 'TIPOLOGIA')
	{
		var tip = form1.tipologia.value;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('cualtip').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#cualtip").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?verificarotro=si&tip="+tip+"&otro="+v,true);
		ajax.send(null);
	}
	else
	if(v == 'TIPOINTERVENCION')
	{
		var tipint = form1.tipointervencion.value;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('cualtipint').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#cualtipint").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?verificarotro=si&tipint="+tipint+"&otro="+v,true);
		ajax.send(null);
	}
	else
	if(v == 'CENTROCOSTOS')
	{
		var cc = form1.centrocostos.value;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('cualcc').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#cualcc").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?verificarotro=si&cc="+cc+"&otro="+v,true);
		ajax.send(null);
	}
	else
	if(v == 'REFERENCIAMAQUINA')
	{
		var ref = form1.referenciamaquina.value;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('cualref').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#cualref").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?verificarotro=si&ref="+ref+"&otro="+v,true);
		ajax.send(null);
	}
	else
	if(v == 'UBICACION')
	{
		var ubi = form1.ubicacion.value;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('cualubi').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#cualubi").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?verificarotro=si&ubi="+ubi+"&otro="+v,true);
		ajax.send(null);
	}
	else
	if(v == 'ATIENDE')
	{
		var ati = form1.atiende.value;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('cualati').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#cualati").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?verificarotro=si&ati="+ati+"&otro="+v,true);
		ajax.send(null);
	}
	else
	if(v == 'AREA')
	{
		var are = form1.area.value;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('cualare').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#cualare").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?verificarotro=si&are="+are+"&otro="+v,true);
		ajax.send(null);
	}
}
function AGREGAR(v)
{
	if(v == 'TIPOLOGIA')
	{
		var tip = form1.cualtip.value;
		
		document.getElementById('cualtip').style.display = 'none';
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('mitipologia').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#mitipologia").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?agregartip=si&tip="+tip,true);
		ajax.send(null);
	}
	else
	if(v == 'TIPOINTERVENCION')
	{
		var tipint = form1.cualtipint.value;
		
		document.getElementById('cualtipint').style.display = 'none';
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('mitipointervencion').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#mitipointervencion").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?agregartipint=si&tipint="+tipint,true);
		ajax.send(null);
	}
	else
	if(v == 'CENTROCOSTOS')
	{
		var cco = form1.cualcc.value;
		
		document.getElementById('cualcc').style.display = 'none';
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('micentrocostos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#micentrocostos").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?agregarcc=si&cco="+cco,true);
		ajax.send(null);
	}
	else
	if(v == 'REFERENCIAMAQUINA')
	{
		var ref = form1.cualref.value;
		
		document.getElementById('cualref').style.display = 'none';
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('mireferenciamaquina').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#mireferenciamaquina").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?agregarref=si&ref="+ref,true);
		ajax.send(null);
	}
	else
	if(v == 'UBICACION')
	{
		var ubi = form1.cualubi.value;
		
		document.getElementById('cualubi').style.display = 'none';
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('miubicacion').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#miubicacion").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?agregarubi=si&ubi="+ubi,true);
		ajax.send(null);
	}
	else
	if(v == 'ATIENDE')
	{
		var ati = form1.cualati.value;
		
		document.getElementById('cualati').style.display = 'none';
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('miatiende').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#miatiende").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?agregarati=si&ati="+ati,true);
		ajax.send(null);
	}
	else
	if(v == 'AREA')
	{
		var are = form1.cualare.value;
		
		document.getElementById('cualare').style.display = 'none';
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('miarea').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#miarea").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?agregarare=si&are="+are,true);
		ajax.send(null);
	}
}
function ELIMINARCAJERO(v)
{	
	if(confirm('Esta seguro/a de Eliminar este cajero?'))
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('estadoregistro').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#estadoregistro").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?eliminarcajero=si&caj="+v,true);
		ajax.send(null);
		setTimeout("window.location.href = 'cajeros.php';",500);
	}
}
function AGREGARNOTA(c)
{
	var not = $('#nota').val();
	
	var nuevanota = CORREGIRTEXTO(not);
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('agregados').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#agregados").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?agregarnota=si&not="+nuevanota+"&caj="+c,true);
	ajax.send(null);
}
function MOSTRARNOTAS(c)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todaslasnotas').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todaslasnotas").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarnotas=si&clacaj="+c,true);
	ajax.send(null);
}
function MOSTRARBITACORA(c)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todaslasnotas').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todaslasnotas").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarbitacora=si&clacaj="+c,true);
	ajax.send(null);
}
function MOSTRARBITACORAADJUNTOS(c)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todoslosadjuntosbitacora').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todoslosadjuntosbitacora").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarbitacoraadjuntos=si&clacaj="+c,true);
	ajax.send(null);
}
function EDITARNOTA(c,cn)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('editarnota').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#editarnota").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?editarnota=si&clanot="+cn+"&caj="+c,true);
	ajax.send(null);
}
function ACTUALIZARNOTA(n,c)
{
	var not = $('#notaedi').val();
	var nuevanota = CORREGIRTEXTO(not);
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todaslasnotas').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todaslasnotas").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?actualizarnota=si&not="+nuevanota+"&clanot="+n+"&caj="+c,true);
	ajax.send(null);
}
function ELIMINARNOTA(c,caj)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todaslasnotas').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todaslasnotas").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?eliminarnota=si&clanot="+c+"&caj="+caj,true);
	ajax.send(null);
}
function EDITARADJUNTO(c,a)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('editaradjunto').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#editaradjunto").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?editaradjunto=si&claada="+c+"&act="+a,true);
	ajax.send(null);
}
function RESULTADOADJUNTOINFORMACION(ca)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('resultadoadjunto').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#resultadoadjunto").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?estadoadjuntoinformacion=si&claada="+ca,true);
	ajax.send(null);
}
function ELIMINARADJUNTO(v)
{
	if(confirm('Esta seguro/a de Eliminar este archivo?'))
	{
		var dataString = 'id='+v;
		
		$.ajax({
	        type: "POST",
	        url: "deletearchivo.php",
	        data: dataString,
	        success: function() {
				//$('#delete-ok').empty();
				//$('#delete-ok').append('<div class="correcto">Se ha eliminado correctamente la entrada con id='+v+'.</div>').fadeIn("slow");
				$('#service'+v).fadeOut("slow");
				//$('#'+v).remove();
	        }
	    });
	}
}
function HIJOSSELECCIONADOS()
{
	var che = form1.padre.checked;
	var hijos = "";
	var objCBarray = document.getElementsByName('selectItemhijos');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	hijos += objCBarray[i].value + ",";
	    }
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('mostrarhijos').innerHTML=ajax.responseText;
	    }
	}
	//jQuery("#resultadoadjunto").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarhijos=si&hij="+hijos+"&che="+che,true);
	ajax.send(null);
}
function HIJOSSELECCIONADOS1()
{
	var che = form1.padre.checked;
	var hijos = "";
	var objCBarray = iframe2.document.getElementsByName('selectItemhijos');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	hijos += objCBarray[i].value + ",";
	    }
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('mostrarhijos').innerHTML=ajax.responseText;
	    }
	}
	//jQuery("#resultadoadjunto").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarhijos=si&hij="+hijos+"&che="+che,true);
	ajax.send(null);
}
function VERPEDIDOMAQUINA(cc)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('pedirmaquina').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#pedirmaquina").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?verpedidomaquina=si&cc="+cc,true);
	ajax.send(null);
}
function stringTrim(x) {
	return x.replace(/^\s+|\s+$/gm,'');
}
function VERINFO(opc)
{
	if(opc == "INFORMACIONBASICA")
	{
		var ocu = $('#OCUINFORMACIONBASICA').val();
		if(ocu == 0)
		{
			document.getElementById('IMGINFORMACIONBASICA').src = "../../images/Minus.png";
			$('#OCUINFORMACIONBASICA').val(1);
			document.getElementById("VERINFORMACIONBASICA").style.display = "block";
		}
		else
		{
			document.getElementById('IMGINFORMACIONBASICA').src = "../../images/Plus.png";
			$('#OCUINFORMACIONBASICA').val(0);
			document.getElementById("VERINFORMACIONBASICA").style.display = "none";
		}
	}
	else
	if(opc == "COMITE")
	{
		var ocu = $('#OCUCOMITE').val();
		if(ocu == 0)
		{
			document.getElementById('IMGCOMITE').src = "../../images/Minus.png";
			$('#OCUCOMITE').val(1);
			document.getElementById("VERCOMITE").style.display = "block";
		}
		else
		{
			document.getElementById('IMGCOMITE').src = "../../images/Plus.png";
			$('#OCUCOMITE').val(0);
			document.getElementById("VERCOMITE").style.display = "none";
		}
	}
	else
	if(opc == "CONTRATO")
	{
		var ocu = $('#OCUCONTRATO').val();
		if(ocu == 0)
		{
			document.getElementById('IMGCONTRATO').src = "../../images/Minus.png";
			$('#OCUCONTRATO').val(1);
			document.getElementById("VERCONTRATO").style.display = "block";
		}
		else
		{
			document.getElementById('IMGCONTRATO').src = "../../images/Plus.png";
			$('#OCUCONTRATO').val(0);
			document.getElementById("VERCONTRATO").style.display = "none";
		}
	}
	else
	if(opc == "INMOBILIARIA")
	{
		var ocu = $('#OCUINMOBILIARIA').val();
		if(ocu == 0)
		{
			document.getElementById('IMGINMOBILIARIA').src = "../../images/Minus.png";
			$('#OCUINMOBILIARIA').val(1);
			document.getElementById("VERINMOBILIARIA").style.display = "block";
		}
		else
		{
			document.getElementById('IMGINMOBILIARIA').src = "../../images/Plus.png";
			$('#OCUINMOBILIARIA').val(0);
			document.getElementById("VERINMOBILIARIA").style.display = "none";
		}
	}
	else
	if(opc == "VISITALOCAL")
	{
		var ocu = $('#OCUVISITALOCAL').val();
		if(ocu == 0)
		{
			document.getElementById('IMGVISITALOCAL').src = "../../images/Minus.png";
			$('#OCUVISITALOCAL').val(1);
			document.getElementById("VERVISITALOCAL").style.display = "block";
		}
		else
		{
			document.getElementById('IMGVISITALOCAL').src = "../../images/Plus.png";
			$('#OCUVISITALOCAL').val(0);
			document.getElementById("VERVISITALOCAL").style.display = "none";
		}
	}
	else
	if(opc == "DISENO")
	{
		var ocu = $('#OCUDISENO').val();
		if(ocu == 0)
		{
			document.getElementById('IMGDISENO').src = "../../images/Minus.png";
			$('#OCUDISENO').val(1);
			document.getElementById("VERDISENO").style.display = "block";
		}
		else
		{
			document.getElementById('IMGDISENO').src = "../../images/Plus.png";
			$('#OCUDISENO').val(0);
			document.getElementById("VERDISENO").style.display = "none";
		}
	}
	else
	if(opc == "LICENCIA")
	{
		var ocu = $('#OCULICENCIA').val();
		if(ocu == 0)
		{
			document.getElementById('IMGLICENCIA').src = "../../images/Minus.png";
			$('#OCULICENCIA').val(1);
			document.getElementById("VERLICENCIA").style.display = "block";
		}
		else
		{
			document.getElementById('IMGLICENCIA').src = "../../images/Plus.png";
			$('#OCULICENCIA').val(0);
			document.getElementById("VERLICENCIA").style.display = "none";
		}
	}
	else
	if(opc == "PREFACTIBILIDAD")
	{
		var ocu = $('#OCUPREFACTIBILIDAD').val();
		if(ocu == 0)
		{
			document.getElementById('IMGPREFACTIBILIDAD').src = "../../images/Minus.png";
			$('#OCUPREFACTIBILIDAD').val(1);
			document.getElementById("VERPREFACTIBILIDAD").style.display = "block";
		}
		else
		{
			document.getElementById('IMGPREFACTIBILIDAD').src = "../../images/Plus.png";
			$('#OCUPREFACTIBILIDAD').val(0);
			document.getElementById("VERPREFACTIBILIDAD").style.display = "none";
		}
	}
	else
	if(opc == "PEDIDOMAQUINA")
	{
		var ocu = $('#OCUPEDIDOMAQUINA').val();
		if(ocu == 0)
		{
			document.getElementById('IMGPEDIDOMAQUINA').src = "../../images/Minus.png";
			$('#OCUPEDIDOMAQUINA').val(1);
			document.getElementById("VERPEDIDOMAQUINA").style.display = "block";
		}
		else
		{
			document.getElementById('IMGPEDIDOMAQUINA').src = "../../images/Plus.png";
			$('#OCUPEDIDOMAQUINA').val(0);
			document.getElementById("VERPEDIDOMAQUINA").style.display = "none";
		}
	}
	else
	if(opc == "PROYECCION")
	{
		var ocu = $('#OCUPROYECCION').val();
		if(ocu == 0)
		{
			document.getElementById('IMGPROYECCION').src = "../../images/Minus.png";
			$('#OCUPROYECCION').val(1);
			document.getElementById("VERPROYECCION").style.display = "block";
		}
		else
		{
			document.getElementById('IMGPROYECCION').src = "../../images/Plus.png";
			$('#OCUPROYECCION').val(0);
			document.getElementById("VERPROYECCION").style.display = "none";
		}
	}
	else
	if(opc == "CANAL")
	{
		var ocu = $('#OCUCANAL').val();
		if(ocu == 0)
		{
			document.getElementById('IMGCANAL').src = "../../images/Minus.png";
			$('#OCUCANAL').val(1);
			document.getElementById("VERCANAL").style.display = "block";
		}
		else
		{
			document.getElementById('IMGCANAL').src = "../../images/Plus.png";
			$('#OCUCANAL').val(0);
			document.getElementById("VERCANAL").style.display = "none";
		}
	}
	else
	if(opc == "INTERVENTORIA")
	{
		var ocu = $('#OCUINTERVENTORIA').val();
		if(ocu == 0)
		{
			document.getElementById('IMGINTERVENTORIA').src = "../../images/Minus.png";
			$('#OCUINTERVENTORIA').val(1);
			document.getElementById("VERINTERVENTORIA").style.display = "block";
		}
		else
		{
			document.getElementById('IMGINTERVENTORIA').src = "../../images/Plus.png";
			$('#OCUINTERVENTORIA').val(0);
			document.getElementById("VERINTERVENTORIA").style.display = "none";
		}
	}
	else
	if(opc == "CONSTRUCCION")
	{
		var ocu = $('#OCUCONSTRUCCION').val();
		if(ocu == 0)
		{
			document.getElementById('IMGCONSTRUCCION').src = "../../images/Minus.png";
			$('#OCUCONSTRUCCION').val(1);
			document.getElementById("VERCONSTRUCCION").style.display = "block";
		}
		else
		{
			document.getElementById('IMGCONSTRUCCION').src = "../../images/Plus.png";
			$('#OCUCONSTRUCCION').val(0);
			document.getElementById("VERCONSTRUCCION").style.display = "none";
		}
	}
	else
	if(opc == "INSTALADORSEGURIDAD")
	{
		var ocu = $('#OCUINSTALADORSEGURIDAD').val();
		if(ocu == 0)
		{
			document.getElementById('IMGINSTALADORSEGURIDAD').src = "../../images/Minus.png";
			$('#OCUINSTALADORSEGURIDAD').val(1);
			document.getElementById("VERINSTALADORSEGURIDAD").style.display = "block";
		}
		else
		{
			document.getElementById('IMGINSTALADORSEGURIDAD').src = "../../images/Plus.png";
			$('#OCUINSTALADORSEGURIDAD').val(0);
			document.getElementById("VERINSTALADORSEGURIDAD").style.display = "none";
		}
	}
	else
	if(opc == "FACTURACION")
	{
		var ocu = $('#OCUFACTURACION').val();
		if(ocu == 0)
		{
			document.getElementById('IMGFACTURACION').src = "../../images/Minus.png";
			$('#OCUFACTURACION').val(1);
			document.getElementById("VERFACTURACION").style.display = "block";
		}
		else
		{
			document.getElementById('IMGFACTURACION').src = "../../images/Plus.png";
			$('#OCUFACTURACION').val(0);
			document.getElementById("VERFACTURACION").style.display = "none";
		}
	}
	else
	if(opc == "NOTAS")
	{
		var ocu = $('#OCUNOTAS').val();
		if(ocu == 0)
		{
			document.getElementById('IMGNOTAS').src = "../../images/Minus.png";
			$('#OCUNOTAS').val(1);
			document.getElementById("VERNOTAS").style.display = "block";
		}
		else
		{
			document.getElementById('IMGNOTAS').src = "../../images/Plus.png";
			$('#OCUNOTAS').val(0);
			document.getElementById("VERNOTAS").style.display = "none";
		}
	}
	else
	if(opc == "PADRE")
	{
		var ocu = $('#OCUPADRE').val();
		if(ocu == 0)
		{
			document.getElementById('IMGPADRE').src = "../../images/Minus.png";
			$('#OCUPADRE').val(1);
			document.getElementById("VERPADRE").style.display = "block";
		}
		else
		{
			document.getElementById('IMGPADRE').src = "../../images/Plus.png";
			$('#OCUPADRE').val(0);
			document.getElementById("VERPADRE").style.display = "none";
		}
	}
	else
	if(opc == "INFORMACIONCAJERO")
	{
		var ocu = $('#OCUINFORMACIONCAJERO').val();
		if(ocu == 0)
		{
			document.getElementById('IMGINFORMACIONCAJERO').src = "../../images/Minus.png";
			$('#OCUINFORMACIONCAJERO').val(1);
			
			document.getElementById("VERINFORMACIONBASICA").style.display = "block";
			document.getElementById("VERCOMITE").style.display = "block";
			document.getElementById("VERCONTRATO").style.display = "block";
			document.getElementById("VERINMOBILIARIA").style.display = "block";
			document.getElementById("VERVISITALOCAL").style.display = "block";
			document.getElementById("VERDISENO").style.display = "block";
			document.getElementById("VERLICENCIA").style.display = "block";
			document.getElementById("VERPREFACTIBILIDAD").style.display = "block";
			document.getElementById("VERPEDIDOMAQUINA").style.display = "block";
			document.getElementById("VERPROYECCION").style.display = "block";
			document.getElementById("VERCANAL").style.display = "block";
			document.getElementById("VERINTERVENTORIA").style.display = "block";
			document.getElementById("VERCONSTRUCCION").style.display = "block";
			document.getElementById("VERINSTALADORSEGURIDAD").style.display = "block";
			document.getElementById("VERFACTURACION").style.display = "block";
			document.getElementById("VERNOTAS").style.display = "block";
			document.getElementById("VERPADRE").style.display = "block";
			
			document.getElementById('IMGINFORMACIONBASICA').src = "../../images/Minus.png";
			document.getElementById('IMGCOMITE').src = "../../images/Minus.png";
			document.getElementById('IMGCONTRATO').src = "../../images/Minus.png";
			document.getElementById('IMGINMOBILIARIA').src = "../../images/Minus.png";
			document.getElementById('IMGVISITALOCAL').src = "../../images/Minus.png";
			document.getElementById('IMGDISENO').src = "../../images/Minus.png";
			document.getElementById('IMGLICENCIA').src = "../../images/Minus.png";
			document.getElementById('IMGPREFACTIBILIDAD').src = "../../images/Minus.png";
			document.getElementById('IMGPEDIDOMAQUINA').src = "../../images/Minus.png";
			document.getElementById('IMGPROYECCION').src = "../../images/Minus.png";
			document.getElementById('IMGCANAL').src = "../../images/Minus.png";
			document.getElementById('IMGINTERVENTORIA').src = "../../images/Minus.png";
			document.getElementById('IMGCONSTRUCCION').src = "../../images/Minus.png";
			document.getElementById('IMGINSTALADORSEGURIDAD').src = "../../images/Minus.png";
			document.getElementById('IMGFACTURACION').src = "../../images/Minus.png";
			document.getElementById('IMGNOTAS').src = "../../images/Minus.png";
			document.getElementById('IMGPADRE').src = "../../images/Minus.png";
			
			$('#OCUINFORMACIONBASICA').val(1);
			$('#OCUCOMITE').val(1);
			$('#OCUCONTRATO').val(1);
			$('#OCUINMOBILIARIA').val(1);
			$('#OCUVISITALOCAL').val(1);
			$('#OCUDISENO').val(1);
			$('#OCULICENCIA').val(1);
			$('#OCUPREFACTIBILIDAD').val(1);
			$('#OCUPEDIDOMAQUINA').val(1);
			$('#OCUPROYECCION').val(1);
			$('#OCUCANAL').val(1);
			$('#OCUINTERVENTORIA').val(1);
			$('#OCUCONSTRUCCION').val(1);
			$('#OCUINSTALADORSEGURIDAD').val(1);
			$('#OCUFACTURACION').val(1);
			$('#OCUNOTAS').val(1);
			$('#OCUPADRE').val(1);
		}
		else
		{
			document.getElementById('IMGINFORMACIONCAJERO').src = "../../images/Plus.png";
			$('#OCUINFORMACIONCAJERO').val(0);
			
			document.getElementById("VERINFORMACIONBASICA").style.display = "none";
			document.getElementById("VERCOMITE").style.display = "none";
			document.getElementById("VERCONTRATO").style.display = "none";
			document.getElementById("VERINMOBILIARIA").style.display = "none";
			document.getElementById("VERVISITALOCAL").style.display = "none";
			document.getElementById("VERDISENO").style.display = "none";
			document.getElementById("VERLICENCIA").style.display = "none";
			document.getElementById("VERPREFACTIBILIDAD").style.display = "none";
			document.getElementById("VERPEDIDOMAQUINA").style.display = "none";
			document.getElementById("VERPROYECCION").style.display = "none";
			document.getElementById("VERCANAL").style.display = "none";
			document.getElementById("VERINTERVENTORIA").style.display = "none";
			document.getElementById("VERCONSTRUCCION").style.display = "none";
			document.getElementById("VERINSTALADORSEGURIDAD").style.display = "none";
			document.getElementById("VERFACTURACION").style.display = "none";
			document.getElementById("VERNOTAS").style.display = "none";
			document.getElementById("VERPADRE").style.display = "none";
			
			document.getElementById('IMGINFORMACIONBASICA').src = "../../images/Plus.png";
			document.getElementById('IMGCOMITE').src = "../../images/Plus.png";
			document.getElementById('IMGCONTRATO').src = "../../images/Plus.png";
			document.getElementById('IMGINMOBILIARIA').src = "../../images/Plus.png";
			document.getElementById('IMGVISITALOCAL').src = "../../images/Plus.png";
			document.getElementById('IMGDISENO').src = "../../images/Plus.png";
			document.getElementById('IMGLICENCIA').src = "../../images/Plus.png";
			document.getElementById('IMGPREFACTIBILIDAD').src = "../../images/Plus.png";
			document.getElementById('IMGPEDIDOMAQUINA').src = "../../images/Plus.png";
			document.getElementById('IMGPROYECCION').src = "../../images/Plus.png";
			document.getElementById('IMGCANAL').src = "../../images/Plus.png";
			document.getElementById('IMGINTERVENTORIA').src = "../../images/Plus.png";
			document.getElementById('IMGCONSTRUCCION').src = "../../images/Plus.png";
			document.getElementById('IMGINSTALADORSEGURIDAD').src = "../../images/Plus.png";
			document.getElementById('IMGFACTURACION').src = "../../images/Plus.png";
			document.getElementById('IMGNOTAS').src = "../../images/Plus.png";
			document.getElementById('IMGPADRE').src = "../../images/Plus.png";
			
			$('#OCUINFORMACIONBASICA').val(0);
			$('#OCUCOMITE').val(0);
			$('#OCUCONTRATO').val(0);
			$('#OCUINMOBILIARIA').val(0);
			$('#OCUVISITALOCAL').val(0);
			$('#OCUDISENO').val(0);
			$('#OCULICENCIA').val(0);
			$('#OCUPREFACTIBILIDAD').val(0);
			$('#OCUPEDIDOMAQUINA').val(0);
			$('#OCUPROYECCION').val(0);
			$('#OCUCANAL').val(0);
			$('#OCUINTERVENTORIA').val(0);
			$('#OCUCONSTRUCCION').val(0);
			$('#OCUINSTALADORSEGURIDAD').val(0);
			$('#OCUFACTURACION').val(0);
			$('#OCUNOTAS').val(0);
			$('#OCUPADRE').val(0);
		}
	}
	OCULTARSCROLL();
}