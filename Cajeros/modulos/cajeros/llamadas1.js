function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function MODULO(v,e)
{	
	if(v == 'CAJEROS')
	{
		window.location.href = "cajeros.php";
	}
	else
	if(v == 'TODOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('cajeros').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#cajeros").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?todos=si&est="+e,true);
		ajax.send(null);
		setTimeout("REFRESCARLISTAS()",1000);
	}
	OCULTARSCROLL();
}
function GUARDAR()
{		
	var nomcaj = form1.nombrecajero.value;
	var dir = form1.direccion.value;
	var anocon = form1.anocontable.value;
	var reg = form1.region.value;
	var dep = form1.departamento.value;
	var mun = form1.municipio.value;
	var tip = form1.tipologia.value;
	var tipint = form1.tipointervencion.value;
	var moda = form1.modalidad.value;
	var codcaj = form1.codigocajero.value;
	var cencos = form1.centrocostos.value;
	var refmaq = form1.referenciamaquina.value;
	var ubi = form1.ubicacion.value;
	var codsuc = form1.codigosuc.value;
	var ubiamt = form1.ubicacionamt.value;
	var ati = form1.atiende.value;
	var rie = form1.riesgo.value;
	var are = form1.area.value;
	var fecapagadoatm = form1.fechaapagadoatm.value;
	var fecinicaj = form1.fechainiciocajero.value;
	var reqinm = $('input[name=requiereinmobiliaria]:checked', '#form1').val();
	var nominm = form1.nombreinmobiliaria.value;
	var feciniinm = form1.fechainicioinmobiliaria.value;
	var reqvis = $('input[name=requierevisita]:checked', '#form1').val();
	var nomvis = form1.nombrevisita.value;
	var fecinivis = form1.fechainiciovisita.value;
	var reqdis = $('input[name=requierediseno]:checked', '#form1').val();
	var nomdis = form1.nombrediseno.value;
	var fecinidis = form1.fechainiciodiseno.value;
	var reqlic = $('input[name=requierelicencia]:checked', '#form1').val();
	var nomlic = form1.nombregestionador.value;
	var fecinilic = form1.fechainiciolicencia.value;
	var nomint = form1.nombreinterventoria.value;
	var fecteoint = $('#teoricainterventoria').val();//form1.teoricainterventoria.value;
	var fecaprocom = form1.fechaaprovacioncomite.value;
	var nomcons = form1.nombreconstructor.value;
	var nominsseg = form1.nombreinstaladorseguridad.value;
	var estpro = form1.estadoproyecto.value;
	var not = form1.nota.value;
	
	var fecteoinm = form1.teoricainmobiliaria.value;
	var fecteovis = form1.teoricavisita.value;
	var fecteodis = form1.teoricadiseno.value;
	var fecteolic = form1.teoricalicencia.value;
	
	var che = form1.padre.checked;
	var hijos = "";
	var objCBarray = document.getElementsByName('selectItemhijos');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	hijos += objCBarray[i].value + ",";
	    }
	}

	if(nomcaj == '')
	{
		alert("Debe ingresar el nombre del cajero");
	}
	else
	if(anocon == '')
	{
		alert("Debe ingresar el ano contable");
	}
	else
	if(dir == '')
	{
		alert("Debe ingresar la direccion");
	}
	else
	if(reg == '')
	{
		alert("Debe elegir la region");
	}
	else
	if(dep == '')
	{
		alert("Debe elegir el departamento");
	}
	else
	if(mun == '')
	{
		alert("Debe elegir el municipio");
	}
	else
	if(tip == '')
	{
		alert("Debe elegir la tipologia");
	}
	else
	if(tipint== '')
	{
		alert("Debe elegir el tipo de intervencion");
	}
	else
	{
		var nuevonombre = CORREGIRTEXTO(nomcaj);
		var nuevadireccion = CORREGIRTEXTO(dir);
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('masnotas').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#masnotas").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?guardardatos=si&nomcaj="+nuevonombre+"&dir="+nuevadireccion+"&anocon="+anocon+"&reg="+reg+"&dep="+dep+"&mun="+mun+"&tip="+tip+"&tipint="+tipint+"&moda="+moda+"&codcaj="+codcaj+"&cencos="+cencos+"&refmaq="+refmaq+"&ubi="+ubi+"&codsuc="+codsuc+"&ubiamt="+ubiamt+"&ati="+ati+"&rie="+rie+"&are="+are+"&fecapagadoatm="+fecapagadoatm+"&fecinicaj="+fecinicaj+"&reqinm="+reqinm+"&nominm="+nominm+"&feciniinm="+feciniinm+"&reqvis="+reqvis+"&nomvis="+nomvis+"&fecinivis="+fecinivis+"&reqdis="+reqdis+"&nomdis="+nomdis+"&fecinidis="+fecinidis+"&reqlic="+reqlic+"&nomlic="+nomlic+"&fecinilic="+fecinilic+"&nomint="+nomint+"&fecteoint="+fecteoint+"&fecaprocom="+fecaprocom+"&nomcons="+nomcons+"&nominsseg="+nominsseg+"&estpro="+estpro+"&not="+not+"&hij="+hijos+"&che="+che+"&fecteoinm="+fecteoinm+"&fecteovis="+fecteovis+"&fecteodis="+fecteodis+"&fecteolic="+fecteolic,true);
		ajax.send(null);
		
		REFRESCARACTIVOS();
	}
}
function ACTUALIZARINFORMACION(v)
{		
	var nomcaj = form1.nombrecajero.value;
	var dir = form1.direccion.value;
	var anocon = form1.anocontable.value;
	var reg = form1.region.value;
	var dep = form1.departamento.value;
	var mun = form1.municipio.value;
	var tip = form1.tipologia.value;
	var tipint = form1.tipointervencion.value;
	var moda = form1.modalidad.value;
	var codcaj = form1.codigocajero.value;
	var cencos = form1.centrocostos.value;
	var refmaq = form1.referenciamaquina.value;
	var ubi = form1.ubicacion.value;
	var codsuc = form1.codigosuc.value;
	var ubiamt = form1.ubicacionamt.value;
	var ati = form1.atiende.value;
	var rie = form1.riesgo.value;
	var are = form1.area.value;
	var fecapagadoatm = form1.fechaapagadoatm.value;
	var fecinicaj = form1.fechainiciocajero.value;
	var reqinm = $('input[name=requiereinmobiliaria]:checked', '#form1').val();
	var nominm = form1.nombreinmobiliaria.value;
	var feciniinm = form1.fechainicioinmobiliaria.value;
	var reqvis = $('input[name=requierevisita]:checked', '#form1').val();
	var nomvis = form1.nombrevisita.value;
	var fecinivis = form1.fechainiciovisita.value;
	var reqdis = $('input[name=requierediseno]:checked', '#form1').val();
	var nomdis = form1.nombrediseno.value;
	var fecinidis = form1.fechainiciodiseno.value;
	var reqlic = $('input[name=requierelicencia]:checked', '#form1').val();
	var nomlic = form1.nombregestionador.value;
	var fecinilic = form1.fechainiciolicencia.value;
	var nomint = form1.nombreinterventoria.value;
	var fecteoint = $('#teoricainterventoria').val();//form1.teoricainterventoria.value;
	var fecaprocom = form1.fechaaprovacioncomite.value;
	var nomcons = form1.nombreconstructor.value;
	var nominsseg = form1.nombreinstaladorseguridad.value;
	var estpro = form1.estadoproyecto.value;
	
	var fecteoinm = form1.teoricainmobiliaria.value;
	var fecteovis = form1.teoricavisita.value;
	var fecteodis = form1.teoricadiseno.value;
	var fecteolic = form1.teoricalicencia.value;
	
	var che = form1.padre.checked;
	var hijos = "";
	var objCBarray = iframe2.document.getElementsByName('selectItemhijos');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	hijos += objCBarray[i].value + ",";
	    }
	}
	
	if(nomcaj == '')
	{
		alert("Debe ingresar el nombre del cajero");
	}
	else
	if(anocon == '')
	{
		alert("Debe ingresar el ano contable");
	}
	else
	if(dir == '')
	{
		alert("Debe ingresar la direccion");
	}
	else
	if(reg == '')
	{
		alert("Debe elegir la region");
	}
	else
	if(dep == '')
	{
		alert("Debe elegir el departamento");
	}
	else
	if(mun == '')
	{
		alert("Debe elegir el municipio");
	}
	else
	if(tip == '')
	{
		alert("Debe elegir la tipologia");
	}
	else
	if(tipint == '')
	{
		alert("Debe elegir el tipo de intervencion");
	}
	else
	{
		var nuevonombre = CORREGIRTEXTO(nomcaj);
		var nuevadireccion = CORREGIRTEXTO(dir);
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('estadoregistro').innerHTML=ajax.responseText;
		    }
			else
			{
				//EDITAR(v);
			}
		}
		jQuery("#estadoregistro").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?actualizardatos=si&nomcaj="+nuevonombre+"&dir="+nuevadireccion+"&anocon="+anocon+"&reg="+reg+"&dep="+dep+"&mun="+mun+"&tip="+tip+"&tipint="+tipint+"&moda="+moda+"&codcaj="+codcaj+"&cencos="+cencos+"&refmaq="+refmaq+"&ubi="+ubi+"&codsuc="+codsuc+"&ubiamt="+ubiamt+"&ati="+ati+"&rie="+rie+"&are="+are+"&fecapagadoatm="+fecapagadoatm+"&fecinicaj="+fecinicaj+"&reqinm="+reqinm+"&nominm="+nominm+"&feciniinm="+feciniinm+"&reqvis="+reqvis+"&nomvis="+nomvis+"&fecinivis="+fecinivis+"&reqdis="+reqdis+"&nomdis="+nomdis+"&fecinidis="+fecinidis+"&reqlic="+reqlic+"&nomlic="+nomlic+"&fecinilic="+fecinilic+"&nomint="+nomint+"&fecteoint="+fecteoint+"&fecaprocom="+fecaprocom+"&nomcons="+nomcons+"&nominsseg="+nominsseg+"&estpro="+estpro+"&clacaj="+v+"&hij="+hijos+"&che="+che+"&fecteoinm="+fecteoinm+"&fecteovis="+fecteovis+"&fecteodis="+fecteodis+"&fecteolic="+fecteolic,true);
		ajax.send(null);
		REFRESCARACTIVOS();
	}
}

//DB->Eliminar linea base (Solicitar motivo)
function ELIMINARLINEABASE(v)
{
	var rec = $("#motivo").val();
	if (v == "")
		v = $("#clavecajero").val();

	if (stringTrim(rec) != "")
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
			{
				document.getElementById('recu').innerHTML=ajax.responseText;
			}
			else
			{
				var closeButton = $(".close-reveal-modal");
				if (closeButton.length > 0)
				{
					closeButton[0].click();
				}
			}
		}
		jQuery("#recu").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?eliminarlineabase=si&motivo="+rec+"&clacaj="+v, true);
		ajax.send(null);
	}
	else
	{
		document.getElementById('recu').innerHTML = "<div class='validaciones' style='width: 100%' align='center'>Debe ingresar el motivo</div>";
	}
}

function CORREGIRTEXTO(v)
{
	var res = v.replace('#','REEMPLAZARNUMERAL');
	var res = res.replace('+','REEMPLAZARMAS');
	
	return res;
}
function REFRESCARACTIVOS()
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('activos').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#activos").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?refrescaractivos=si",true);
	ajax.send(null);
}
function CALCULARFECHAS(o)
{
	var moda = form1.modalidad.value;
	
	var modinm = form1.modainm.value;
	var modvis = form1.modavis.value;
	var moddis = form1.modadis.value;
	var modlic = form1.modalic.value;
	var modint = form1.modaint.value;
	
	var fecini = $("#fechainiciocajero").val();
	
	if(modinm == 1)
	{
		$("#fechainicioinmobiliaria").val(fecini);
	}
	else
	if(modinm == 0 && modvis == 1)
	{
		$("#fechainiciovisita").val(fecini);
	}
	else
	if(modinm == 0 && modvis == 0 && moddis == 1)
	{
		$("#fechainiciodiseno").val(fecini);
	}
	else
	if(modinm == 0 && modvis == 0 && moddis == 0 && modlic == 1)
	{
		$("#fechainiciolicencia").val(fecini);
	}
	
	setTimeout("CALCULARFECHATEORICA('teoricainmobiliaria','INICAJ')",500);
	setTimeout("CALCULARTOTALDIAS('diastotalinmobiliaria','INICAJ')",500);
	setTimeout("CALCULARDIASACTOR('diasinmobiliaria')",500);
	
	setTimeout("CALCULARFECHATEORICA('teoricavisita','INICAJ')",500);
	setTimeout("CALCULARTOTALDIAS('diastotalvisita','INICAJ')",500);
	setTimeout("CALCULARDIASACTOR('diasvisita')",500);
	
	setTimeout("CALCULARFECHATEORICA('teoricadiseno','INICAJ')",500);
	setTimeout("CALCULARTOTALDIAS('diastotaldiseno','INICAJ')",500);
	setTimeout("CALCULARDIASACTOR('diasdiseno')",500);
	
	setTimeout("CALCULARFECHATEORICA('teoricalicencia','INICAJ')",500);
	setTimeout("CALCULARTOTALDIAS('diastotallicencia','INICAJ')",500);
	setTimeout("CALCULARDIASACTOR('diaslicencia')",500);
	
	setTimeout("CALCULARFECHATEORICA('teoricainterventoria','INICAJ')",500);
	
	if(modinm == 0){ form1.fechainicioinmobiliaria.value = ''; form1.teoricainmobiliaria.value = ''; form1.totaldiasinmobiliaria.value = ''; }
	if(modvis == 0){ form1.fechainiciovisita.value = ''; form1.teoricavisita.value = ''; form1.totaldiasvisita.value = ''; }
	if(moddis == 0){ form1.fechainiciodiseno.value = ''; form1.teoricadiseno.value = ''; form1.totaldiasdiseno.value = ''; }
	if(modlic == 0){ form1.fechainiciolicencia.value = ''; form1.teoricalicencia.value = ''; form1.totaldiaslicencia.value = ''; }
	if(modint == 0){ form1.teoricainterventoria.value = ''; }

	if(moda == '' || fecini == '')
	{
		$("#fechainicioinmobiliaria").val("");
		$("#teoricainmobiliaria").val("");
		$("#totaldiasinmobiliaria").val("");
		$("#fechainiciovisita").val("");
		$("#teoricavisita").val("");
		$("#totaldiasvisita").val("");
		$("#fechainiciodiseno").val("");
		$("#teoricadiseno").val("");
		$("#totaldiasdiseno").val("");
		$("#fechainiciolicencia").val("");
		$("#teoricalicencia").val("");
		$("#totaldiaslicencia").val("");
		$("#teoricainterventoria").val("");
	}
}
function CALCULARFECHATEORICA(o,c)
{
	var moda = form1.modalidad.value;
	
	var modinm = form1.modainm.value;
	var modvis = form1.modavis.value;
	var moddis = form1.modadis.value;
	var modlic = form1.modalic.value;
	var modint = form1.modaint.value;
	var modcons = form1.modacons.value;
	var modcom = form1.modacom.value;
	var modcon = form1.modacon.value;
	var modpre = form1.modapre.value;
	
	if(c == 'INICAJ')
	{
		var feciniinm = $("#fechainiciocajero").val();
		
		if(modinm == 1)
		{
			$("#fechainicioinmobiliaria").val(feciniinm);
		}
		else
		if(modinm == 0 && modvis == 1)
		{
			$("#fechainiciovisita").val(feciniinm);
		}
		else
		if(modinm == 0 && modvis == 0 && moddis == 1)
		{
			$("#fechainiciodiseno").val(feciniinm);
		}
		else
		if(modinm == 0 && modvis == 0 && moddis == 0 && modlic == 1)
		{
			$("#fechainiciolicencia").val(feciniinm);
		}
	}
	
	var dinm = form1.diasinm.value;
	var dvis = form1.diasvis.value;
	var ddis = form1.diasdis.value;
	var dlic = form1.diaslic.value;
	var dcons = form1.diascons.value;
	var dcom = form1.diascom.value;
	var dcon = form1.diascon.value;
	var dpre = form1.diaspre.value;
	
	if(o == 'teoricainmobiliaria')
	{
		if(modinm == 1)
		{
			var fecentinmob = $("#fechaentregainfoinmobiliaria").val();
			
			var dias = dinm;
			if(c == 'INICAJ')
			{
				var fecini = $("#fechainiciocajero").val();
			}
			else
			{
				var fecini = $("#fechainicioinmobiliaria").val();
			}
			
			if(c == 'INICAJ')
			{
				if(fecentinmob != '' && fecentinmob != '0000-00-00')//Si ya se entrego la informacion
				{
					var d = restaFechas(fecini,fecentinmob);
					
					var fechafin = sumaFecha(parseInt(d),fecini);
					if(modvis == 1)
					{
						$("#fechainiciovisita").val(fechafin);//Inicio visita: Es la final de inmobiliaria
					}
					else
					{
						$("#fechainiciovisita").val('');
					}
				}
				else
				{
					var fechafin = sumaFecha(parseInt(dias),fecini);
					if(modvis == 1)
					{
						$("#fechainiciovisita").val(fechafin);//Inicio visita: Es la final de inmobiliaria
					}
					else
					{
						$("#fechainiciovisita").val('');
					}
				}
			}
		}
		else
		{
			var dias = 0;
			var fecini = '';
		}
	}
	else
	if(o == 'teoricavisita')
	{
		if(modvis == 1)
		{
			var fecentinmob = $("#fechaentregainfoinmobiliaria").val();
			var fecentvis = $("#fechaentregavisita").val();
			
			if(c == 'INICAJ')
			{
				var fecini = $("#fechainiciocajero").val();
			}
			else
			{
				var fecini = $("#fechainicioinmobiliaria").val();
			}
			
			var dias = dvis;
			var fecini = $("#fechainiciovisita").val();//Fecha inicio de visita
			if(c == 'INICAJ')
			{
				if(fecentvis != '' && fecentvis != '0000-00-00')//Si ya se entrego la informacion
				{
					var d = restaFechas(fecini,fecentvis);
					
					var fechafin = sumaFecha(parseInt(d)+parseInt(dcom),fecini);
					if(moddis == 1)
					{
						$("#fechainiciodiseno").val(fechafin);//Inicio visita: Es la final de inmobiliaria
					}
					else
					{
						$("#fechainiciodiseno").val('');
					}
				}
				else
				{
					var fechafin = sumaFecha(parseInt(dias)+parseInt(dcom),fecini);
					if(moddis == 1)
					{
						$("#fechainiciodiseno").val(fechafin);//Inicio visita: Es la final de inmobiliaria
					}
					else
					{
						$("#fechainiciodiseno").val('');
					}
				}
			}
		}
		else
		{
			var dias = 0;
			var fecini = '';
		}
	}
	else
	if(o == 'teoricadiseno')
	{
		if(moddis == 1)
		{
			var fecentvis = $("#fechaentregavisita").val();
			var fecentdis = $("#fechaentregainfodiseno").val();
			if(c == 'INICAJ')
			{
				var fecini = $("#fechainiciocajero").val();
			}
			else
			{
				var fecini = $("#fechainicioinmobiliaria").val();
			}
			
			var fechafin = sumaFecha(parseInt(dinm),fecini);//final de inmobiliaria
			
			var dias = ddis;
			var fecini = $("#fechainiciodiseno").val();
			if(c == 'INICAJ')
			{
				if(fecentdis != '' && fecentdis != '0000-00-00')//Si ya se entrego la informacion
				{
					if(modlic == 1)
					{
						var d = restaFechas(fecini,fecentdis);
						$("#fechainiciolicencia").val(fecentdis);
					}
					else
					{
						$("#fechainiciolicencia").val('');
					}
				}
				else
				{
					var fechafin = sumaFecha(parseInt(ddis),fecini);
					if(modlic == 1)
					{
						$("#fechainiciolicencia").val(fechafin);//Inicio visita: Es la final de inmobiliaria
					}
					else
					{
						$("#fechainiciolicencia").val('');
					}
				}
			}
		}
		else
		{
			var dias = 0;
			var fecini = '';
		}
	}
	else
	if(o == 'teoricalicencia')
	{
		if(modlic == 1)
		{
			var fecentdis = $("#fechaentregainfodiseno").val();
			var fecentlic = $("#fechaentregainfolicencia").val();
			if(c == 'INICAJ')
			{
				var fecini = $("#fechainiciocajero").val();
			}
			else
			{
				var fecini = $("#fechainicioinmobiliaria").val();
			}
			var fechafin = sumaFecha(parseInt(dinm),fecini);//final de inmobiliaria
			
			var dias = dlic;
			var fecini = $("#fechainiciolicencia").val();
			
			if(fecentlic != '' && fecentlic != '0000-00-00')//Si ya se entrego la informacion
			{
				var d = restaFechas(fecini,fecentlic);
			}
		}
		else
		{
			var dias = 0;
			var fecini = '';
		}
	}
	else
	if(o == 'teoricainterventoria' && modint == 1)
	{
		var fecentcons = $("#fechaentregaatm").val();
		//Cuando no hay licencia
		if(modlic == 0)
		{
			if(c == 'INICAJ')
			{
				var fecini = $("#fechainiciocajero").val();
			}
			else
			{
				var fecini = $("#fechainicioinmobiliaria").val();
			}
			var fechafin = sumaFecha(parseInt(dinm),fecini);//final de inmobiliaria
			if(modvis == 1)
			{
				var fechafin = sumaFecha(parseInt(dvis),fechafin);
			}
			var fechafin = sumaFecha(parseInt(dcom)+parseInt(dcon)+parseInt(dpre)+parseInt(dcons),fechafin);//Final de dise�o + 5 dias del banco + 30 dias banco + 25 dias de ejecucion
			$("#teoricainterventoria").val(fechafin);
		}
		else
		{
			//Cuando hay licencia
			if(c == 'INICAJ')
			{
				var fecini = $("#fechainiciocajero").val();
			}
			else
			{
				var fecini = $("#fechainicioinmobiliaria").val();
			}
			var fechafin = sumaFecha(parseInt(dinm),fecini);//final de inmobiliaria
			if(modvis == 1)
			{
				var fechafin = sumaFecha(parseInt(dvis),fechafin);
			}
			if(moddis == 1)
			{
				var fechafin = sumaFecha(parseInt(ddis)+parseInt(dcom),fechafin);
			}
			var fechafin = sumaFecha(parseInt(dlic)+parseInt(dcom)+parseInt(dcons),fechafin);//Final de licencia + 25 dias de ejecucion
			$("#teoricainterventoria").val(fechafin);
		}
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById(o).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#"+o).html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarfechateorica=si&fec="+fecini+"&dias="+dias+"&opc="+o,true);
	ajax.send(null);
	
	if(modinm == 0)
	{
		form1.requiereinmobiliaria[0].checked = false;
		form1.requiereinmobiliaria[1].checked = true;
		form1.nombreinmobiliaria.disabled = true;
		form1.nombreinmobiliaria.value = "";
		form1.fechainicioinmobiliaria.disabled = true;
		form1.fechainicioinmobiliaria.value = "";
	}
	else
	{ 
		form1.requiereinmobiliaria[0].checked = true;
		form1.requiereinmobiliaria[1].checked = false;
		form1.nombreinmobiliaria.disabled = false;
		form1.fechainicioinmobiliaria.disabled = false;
	}
	
	if(modvis == 0)
	{ 
		form1.requierevisita[0].checked = false;
		form1.requierevisita[1].checked = true;
		form1.nombrevisita.disabled = true;
		form1.nombrevisita.value = "";
	}
	else
	{ 
		form1.requierevisita[0].checked = true; 
		form1.requierevisita[1].checked = false; 
		form1.nombrevisita.disabled = false;
	}
	
	if(moddis == 0)
	{ 
		form1.requierediseno[0].checked = false;
		form1.requierediseno[1].checked = true;
		form1.nombrediseno.disabled = true;
		form1.nombrediseno.value = "";
	}
	else
	{ 
		form1.requierediseno[0].checked = true; 
		form1.requierediseno[1].checked = false; 
		form1.nombrediseno.disabled = false;
	}
	
	if(modlic == 0)
	{ 
		form1.requierelicencia[0].checked = false;
		form1.requierelicencia[1].checked = true;
		form1.nombregestionador.disabled = true;
		form1.nombregestionador.value = "";
	}
	else
	{ 
		form1.requierelicencia[0].checked = true; 
		form1.requierelicencia[1].checked = false; 
		form1.nombregestionador.disabled = false;
	}
	
	if(moda == '')
	{
		$("#fechainicioinmobiliaria").val("");
		$("#teoricainmobiliaria").val("");
		$("#totaldiasinmobiliaria").val("");
		$("#fechainiciovisita").val("");
		$("#teoricavisita").val("");
		$("#totaldiasvisita").val("");
		$("#fechainiciodiseno").val("");
		$("#teoricadiseno").val("");
		$("#totaldiasdiseno").val("");
		$("#fechainiciolicencia").val("");
		$("#teoricalicencia").val("");
		$("#totaldiaslicencia").val("");
		$("#teoricainterventoria").val("");
	}
}
function restaFechas(f1,f2)
{
	var aFecha1 = f1.split('-'); 
	var aFecha2 = f2.split('-'); 
	var fFecha1 = Date.UTC(aFecha1[0],aFecha1[1]-1,aFecha1[2]); 
	var fFecha2 = Date.UTC(aFecha2[0],aFecha2[1]-1,aFecha2[2]); 
	var dif = fFecha2 - fFecha1;
	var dias = Math.floor(dif / (1000 * 60 * 60 * 24)); 
	return dias;
}
function sumaFecha(d, fecha)
{
	fecha=fecha.replace("-", "/").replace("-", "/");	  
	
	fecha= new Date(fecha);
	fecha.setDate(fecha.getDate()+d);
	
	var anio=fecha.getFullYear();
	var mes= fecha.getMonth()+1;
	var dia= fecha.getDate();
	
	if(mes.toString().length<2)
	{
		mes="0".concat(mes);        
	}    
	
	if(dia.toString().length<2)
	{
		dia="0".concat(dia);        
	}
	return anio+"-"+mes+"-"+dia;
}
function CALCULARTOTALDIAS(o,c)
{
	var modinm = form1.modainm.value;
	var modvis = form1.modavis.value;
	var moddis = form1.modadis.value;
	var modlic = form1.modalic.value;
	var modcons = form1.modacons.value;
	var modcom = form1.modacom.value;
	var modcon = form1.modacon.value;
	var modpre = form1.modapre.value;
	
	var dinm = form1.diasinm.value;
	var dvis = form1.diasvis.value;
	var ddis = form1.diasdis.value;
	var dlic = form1.diaslic.value;
	var dcons = form1.diascons.value;
	var dcom = form1.diascom.value;
	var dcon = form1.diascon.value;
	var dpre = form1.diaspre.value;
	
	if(o == 'diastotalinmobiliaria' && modinm == 1)
	{
		var dias = dinm;
		if(c == 'INICAJ')
		{
			var fecini = $("#fechainiciocajero").val();
		}
		else
		{
			var fecini = $("#fechainicioinmobiliaria").val();
		}
		var fecini = $("#fechainiciocajero").val();
		var fecent = $("#fechaentregainfoinmobiliaria").val();
	}
	else
	if(o == 'diastotalvisita' && modvis == 1)
	{
		var dias = dvis;
		var fecini = $("#fechainiciovisita").val();
		var fecent = $("#fechaentregavisita").val();
	}
	else
	if(o == 'diastotaldiseno' && moddis == 1)
	{
		var dias = ddis;
		var fecini = $("#fechainiciodiseno").val();
		var fecent = $("#fechaentregainfodiseno").val();
	}
	else
	if(o == 'diastotallicencia' && modlic == 1)
	{
		var dias = dlic;
		var fecini = $("#fechainiciolicencia").val();
		var fecent = $("#fechaentregainfolicencia").val();
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById(o).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#"+o).html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrartotaldias=si&fec="+fecini+"&dias="+dias+"&fecent="+fecent,true);
	ajax.send(null);
}
function CALCULARDIASACTOR(o)
{
	var dinm = form1.diasinm.value;
	var dvis = form1.diasvis.value;
	var ddis = form1.diasdis.value;
	var dlic = form1.diaslic.value;
	var dcons = form1.diascons.value;
	var dcom = form1.diascom.value;
	var dcon = form1.diascon.value;
	var dpre = form1.diaspre.value;
	
	if(o == 'diasinmobiliaria')
	{
		var fec = dinm;
	}
	else
	if(o == 'diasvisita')
	{
		var fec = dvis;
	}
	else
	if(o == 'diasdiseno')
	{
		var fec = ddis;
	}
	else
	if(o == 'diaslicencia')
	{
		var fec = dlic;
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById(o).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#"+o).html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrardiasactores=si&fec="+fec,true);
	ajax.send(null);
}
function EDITAR(v,m)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('cajeros').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#cajeros").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?editardatos=si&caj="+v,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTAS()",500);
	//setTimeout("MODALIDADACTOR("+m+")",500);
	OCULTARSCROLL();
}
function BUSCAR(v,p)
{	
	var consec = form1.busconsecutivo.value;
	var nom = form1.busnombre.value;
	var anocon = form1.busanocontable.value;
	var cencos = form1.buscentrocostos.value;
	var cod = form1.buscodigo.value;
	var reg = form1.busregion.value;
	var mun = form1.busmunicipio.value;
	var tip = form1.bustipologia.value;
	var tipint = form1.bustipointervencion.value;
	var moda = form1.busmodalidad.value;
	var actor = form1.busactor.value;
	var est = form1.ocultoestado.value;
	
	if(v == 'CAJERO')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('busqueda').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#busqueda").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='50' width='50' />"); //loading gif will be overwrited when ajax have success
		if(p > 0)
		{
			ajax.open("GET","?buscar=si&consec="+consec+"&nom="+nom+"&anocon="+anocon+"&cencos="+cencos+"&cod="+cod+"&reg="+reg+"&mun="+mun+"&tip="+tip+"&tipint="+tipint+"&moda="+moda+"&est="+est+"&actor="+actor+"&page="+p,true);
		}
		else
		{
			ajax.open("GET","?buscar=si&consec="+consec+"&nom="+nom+"&anocon="+anocon+"&cencos="+cencos+"&cod="+cod+"&reg="+reg+"&mun="+mun+"&tip="+tip+"&tipint="+tipint+"&moda="+moda+"&est="+est+"&actor="+actor,true);
		}
		ajax.send(null);
	}
	OCULTARSCROLL();
}
function VERDEPARTAMENTOS(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('departamentos').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#departamentos").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?verdepartamentos=si&reg="+v,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTADEPARTAMENTOS()",800);
}
function VERCIUDADES(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('ciudades').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#ciudades").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?verciudades=si&dep="+v,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTACIUDADES()",800);
}
function VERCIUDADES1(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('ciudades1').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#ciudades1").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?verciudades1=si&reg="+v,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTACIUDADES1()",1000);
}
function VERMODALIDADES(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('modalidades').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#modalidades").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?vermodalidades=si&tii="+v,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTAMODALIDADES()",800);
}
function VERMODALIDADES1(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('modalidades1').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#modalidades1").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?vermodalidades1=si&tii="+v,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTAMODALIDADES1()",800);
}
function MODALIDADACTOR(v)
{
	var fecini = $("#fechainicioinmobiliaria").val();	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('modalidadactor').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#modalidadactor").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?modalidadactor=si&mod="+v,true);
	ajax.send(null);
	setTimeout("CALCULARFECHAS('INMOBILIARIA')",800);
	if(v == '' || fecini == '')
	{
		$("#fechainicioinmobiliaria").val("");
		$("#teoricainmobiliaria").val("");
		$("#totaldiasinmobiliaria").val("");
		$("#fechainiciovisita").val("");
		$("#teoricavisita").val("");
		$("#totaldiasvisita").val("");
		$("#fechainiciodiseno").val("");
		$("#teoricadiseno").val("");
		$("#totaldiasdiseno").val("");
		$("#fechainiciolicencia").val("");
		$("#teoricalicencia").val("");
		$("#totaldiaslicencia").val("");
		$("#teoricainterventoria").val("");
	}
}
function LIMPIAR()
{
	form1.nombrecajero.value = '';
	form1.direccion.value = '';
	form1.region.value = '';
	form1.departamento.value = '';
	form1.municipio.value = '';
	form1.tipologia.value = '';
	form1.tipointervencion.value = '';
	form1.codigocajero.value = '';
	form1.centrocostos.value = '';
	form1.referenciamaquina.value = '';
	form1.ubicacion.value = '';
	form1.codigosuc.value = '';
	form1.ubicacionamt.value = '';
	form1.atiende.value = '';
	form1.riesgo.value = '';
	form1.area.value = '';
	form1.codigorecibido.value = '';
	form1.requiereinmobiliaria.value = '';
	form1.nombreinmobiliaria.value = '';
	form1.fechainicioinmobiliaria.value = '';
	form1.requierevisita.value = '';
	form1.nombrevisita.value = '';
	form1.fechainiciovisita.value = '';
	form1.requierediseno.value = '';
	form1.nombrediseno.value = '';
	form1.fechainiciodiseno.value = '';
	form1.requierelicencia.value = '';
	form1.nombregestionador.value = '';
	form1.fechainiciolicencia.value = '';
	form1.nombreinterventoria.value = '';
	form1.teoricainterventoria.value = '';
	form1.nombreinterventoria.value = '';
	form1.nombreinstaladorseguridad.value = '';
	form1.estadoproyecto.value = '1';
}
function RESULTADOADJUNTO(v,cc)
{
	if(v == 'INMOBILIARIA')
	{
		var div = 'resultadoadjuntoinm';
	}
	else
	if(v == 'VISITA')
	{
		var div = 'resultadoadjuntovis';
	}
	else
	if(v == 'DISENO')
	{
		var div = 'resultadoadjuntodis';
	}
	else
	if(v == 'LICENCIA')
	{
		var div = 'resultadoadjuntolic';
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById(div).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#"+div).html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?estadoadjunto=si&actor="+v+"&clacaj="+cc,true);
	ajax.send(null);
	OCULTARSCROLL();
}
function MOSTRARRUTA(v)
{
	if(v == 'INMOBILIARIA')
	{
		var div = 'resultadoadjuntoinm';
		var nomadj = form1.adjuntoinm.value;
	}
	else
	if(v == 'VISITA')
	{
		var div = 'resultadoadjuntovis';
		var nomadj = form1.adjuntovis.value;
	}
	else
	if(v == 'DISENO')
	{
		var div = 'resultadoadjuntodis';
		var nomadj = form1.adjuntodis.value;
	}
	else
	if(v == 'LICENCIA')
	{
		var div = 'resultadoadjuntolic';
		var nomadj = form1.adjuntolic.value;
	}
	else
	if(v == 'ADJUNTO')
	{
		var div = 'resultadoadjunto';
		var nomadj = $('#archivoadjunto').val();
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById(div).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#"+div).html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarruta=si&nomadj="+nomadj,true);
	ajax.send(null);
	OCULTARSCROLL();
}
function MOSTRARADJUNTOS(c,a)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('adjuntoarchivos').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#adjuntoarchivos").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostraradjuntos=si&clacaj="+c+"&actor="+a,true);
	ajax.send(null);
}
function VERIFICAROTRO(v)
{
	if(v == 'TIPOLOGIA')
	{
		var tip = form1.tipologia.value;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('cualtip').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#cualtip").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?verificarotro=si&tip="+tip+"&otro="+v,true);
		ajax.send(null);
	}
	else
	if(v == 'TIPOINTERVENCION')
	{
		var tipint = form1.tipointervencion.value;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('cualtipint').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#cualtipint").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?verificarotro=si&tipint="+tipint+"&otro="+v,true);
		ajax.send(null);
	}
	else
	if(v == 'CENTROCOSTOS')
	{
		var cc = form1.centrocostos.value;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('cualcc').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#cualcc").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?verificarotro=si&cc="+cc+"&otro="+v,true);
		ajax.send(null);
	}
	else
	if(v == 'REFERENCIAMAQUINA')
	{
		var ref = form1.referenciamaquina.value;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('cualref').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#cualref").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?verificarotro=si&ref="+ref+"&otro="+v,true);
		ajax.send(null);
	}
	else
	if(v == 'UBICACION')
	{
		var ubi = form1.ubicacion.value;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('cualubi').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#cualubi").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?verificarotro=si&ubi="+ubi+"&otro="+v,true);
		ajax.send(null);
	}
	else
	if(v == 'ATIENDE')
	{
		var ati = form1.atiende.value;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('cualati').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#cualati").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?verificarotro=si&ati="+ati+"&otro="+v,true);
		ajax.send(null);
	}
	else
	if(v == 'AREA')
	{
		var are = form1.area.value;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('cualare').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#cualare").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?verificarotro=si&are="+are+"&otro="+v,true);
		ajax.send(null);
	}
}
function AGREGAR(v)
{
	if(v == 'TIPOLOGIA')
	{
		var tip = form1.cualtip.value;
		
		document.getElementById('cualtip').style.display = 'none';
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('mitipologia').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#mitipologia").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?agregartip=si&tip="+tip,true);
		ajax.send(null);
	}
	else
	if(v == 'TIPOINTERVENCION')
	{
		var tipint = form1.cualtipint.value;
		
		document.getElementById('cualtipint').style.display = 'none';
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('mitipointervencion').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#mitipointervencion").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?agregartipint=si&tipint="+tipint,true);
		ajax.send(null);
	}
	else
	if(v == 'CENTROCOSTOS')
	{
		var cco = form1.cualcc.value;
		
		document.getElementById('cualcc').style.display = 'none';
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('micentrocostos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#micentrocostos").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?agregarcc=si&cco="+cco,true);
		ajax.send(null);
	}
	else
	if(v == 'REFERENCIAMAQUINA')
	{
		var ref = form1.cualref.value;
		
		document.getElementById('cualref').style.display = 'none';
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('mireferenciamaquina').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#mireferenciamaquina").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?agregarref=si&ref="+ref,true);
		ajax.send(null);
	}
	else
	if(v == 'UBICACION')
	{
		var ubi = form1.cualubi.value;
		
		document.getElementById('cualubi').style.display = 'none';
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('miubicacion').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#miubicacion").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?agregarubi=si&ubi="+ubi,true);
		ajax.send(null);
	}
	else
	if(v == 'ATIENDE')
	{
		var ati = form1.cualati.value;
		
		document.getElementById('cualati').style.display = 'none';
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('miatiende').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#miatiende").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?agregarati=si&ati="+ati,true);
		ajax.send(null);
	}
	else
	if(v == 'AREA')
	{
		var are = form1.cualare.value;
		
		document.getElementById('cualare').style.display = 'none';
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('miarea').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#miarea").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?agregarare=si&are="+are,true);
		ajax.send(null);
	}
}
function ELIMINARCAJERO(v)
{	
	if(confirm('Esta seguro/a de Eliminar este cajero?'))
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('estadoregistro').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#estadoregistro").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?eliminarcajero=si&caj="+v,true);
		ajax.send(null);
		setTimeout("window.location.href = 'cajeros.php';",500);
	}
}
function AGREGARNOTA(c)
{
	var not = $('#nota').val();
	
	var nuevanota = CORREGIRTEXTO(not);
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('agregados').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#agregados").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?agregarnota=si&not="+nuevanota+"&caj="+c,true);
	ajax.send(null);
}
function MOSTRARNOTAS(c)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todaslasnotas').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todaslasnotas").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarnotas=si&clacaj="+c,true);
	ajax.send(null);
}
function MOSTRARBITACORA(c)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todaslasnotas').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todaslasnotas").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarbitacora=si&clacaj="+c,true);
	ajax.send(null);
}
function MOSTRARBITACORAADJUNTOS(c)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todoslosadjuntosbitacora').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todoslosadjuntosbitacora").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarbitacoraadjuntos=si&clacaj="+c,true);
	ajax.send(null);
}
function EDITARNOTA(c,cn)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('editarnota').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#editarnota").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?editarnota=si&clanot="+cn+"&caj="+c,true);
	ajax.send(null);
}
function ACTUALIZARNOTA(n,c)
{
	var not = $('#notaedi').val();
	var nuevanota = CORREGIRTEXTO(not);
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todaslasnotas').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todaslasnotas").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?actualizarnota=si&not="+nuevanota+"&clanot="+n+"&caj="+c,true);
	ajax.send(null);
}
function ELIMINARNOTA(c,caj)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todaslasnotas').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todaslasnotas").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?eliminarnota=si&clanot="+c+"&caj="+caj,true);
	ajax.send(null);
}
function EDITARADJUNTO(c,a)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('editaradjunto').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#editaradjunto").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?editaradjunto=si&claada="+c+"&act="+a,true);
	ajax.send(null);
}
function RESULTADOADJUNTOINFORMACION(ca)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('resultadoadjunto').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#resultadoadjunto").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?estadoadjuntoinformacion=si&claada="+ca,true);
	ajax.send(null);
}
function ELIMINARADJUNTO(v)
{
	if(confirm('Esta seguro/a de Eliminar este archivo?'))
	{
		var dataString = 'id='+v;
		
		$.ajax({
	        type: "POST",
	        url: "deletearchivo.php",
	        data: dataString,
	        success: function() {
				//$('#delete-ok').empty();
				//$('#delete-ok').append('<div class="correcto">Se ha eliminado correctamente la entrada con id='+v+'.</div>').fadeIn("slow");
				$('#service'+v).fadeOut("slow");
				//$('#'+v).remove();
	        }
	    });
	}
}
function HIJOSSELECCIONADOS()
{
	var che = form1.padre.checked;
	var hijos = "";
	var objCBarray = document.getElementsByName('selectItemhijos');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	hijos += objCBarray[i].value + ",";
	    }
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('mostrarhijos').innerHTML=ajax.responseText;
	    }
	}
	//jQuery("#resultadoadjunto").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarhijos=si&hij="+hijos+"&che="+che,true);
	ajax.send(null);
}
function HIJOSSELECCIONADOS1()
{
	var che = form1.padre.checked;
	var hijos = "";
	var objCBarray = iframe2.document.getElementsByName('selectItemhijos');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	hijos += objCBarray[i].value + ",";
	    }
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('mostrarhijos').innerHTML=ajax.responseText;
	    }
	}
	//jQuery("#resultadoadjunto").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarhijos=si&hij="+hijos+"&che="+che,true);
	ajax.send(null);
}

function stringTrim(x) {
	return x.replace(/^\s+|\s+$/gm,'');
}