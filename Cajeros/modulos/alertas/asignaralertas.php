
<?php
include('../../data/Conexion.php');
error_reporting(0);
header("Cache-Control: no-store, no-cache, must-revalidate");
date_default_timezone_set('America/Bogota');
header('Content-Type: text/html; charset=UTF-8');
$fecha=date("Y/m/d H:i:s");
?>
<div style="display:none">
    <?php
    session_start();
    $usuario= $_COOKIE['usuario'];
    ?>
</div>
<?php
require_once("lib/Zebra_Pagination.php");
$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
$dato = mysqli_fetch_array($con);
$perfil = $dato['prf_descripcion'];
$claveusuario = $dato['usu_clave_int'];

?>
<!DOCTYPE HTML>
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>ASIGNACIÓN ALERTAS</title>
    <meta name="description" content="Service Desk">
    <meta name="author" content="InvGate S.R.L.">

    <link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
    <!--[if lte IE 7]>
    <link rel="stylesheet" href="css/ie.c32bc18afe0e2883ee4912a51f86c119.css" type="text/css" />
    <![endif]-->

    <link rel="stylesheet" href="css/style.css" type="text/css" />

    <script>
        function OCULTARSCROLL()
        {
            parent.autoResize('iframe43');
            setTimeout("parent.autoResize('iframe43')",500);
            setTimeout("parent.autoResize('iframe43')",1000);
            setTimeout("parent.autoResize('iframe43')",1500);
            setTimeout("parent.autoResize('iframe43')",2000);
            setTimeout("parent.autoResize('iframe43')",2500);
            setTimeout("parent.autoResize('iframe43')",3000);
            setTimeout("parent.autoResize('iframe43')",3500);
            setTimeout("parent.autoResize('iframe43')",4000);
            setTimeout("parent.autoResize('iframe43')",4500);
            setTimeout("parent.autoResize('iframe43')",5000);
            setTimeout("parent.autoResize('iframe43')",5500);
            setTimeout("parent.autoResize('iframe43')",6000);
            setTimeout("parent.autoResize('iframe43')",6500);
            setTimeout("parent.autoResize('iframe43')",7000);
            setTimeout("parent.autoResize('iframe43')",7500);
            setTimeout("parent.autoResize('iframe43')",8000);
            setTimeout("parent.autoResize('iframe43')",8500);
            setTimeout("parent.autoResize('iframe43')",9000);
            setTimeout("parent.autoResize('iframe43')",9500);
            setTimeout("parent.autoResize('iframe43')",10000);
        }
        parent.autoResize('iframe43');
        setTimeout("parent.autoResize('iframe43')",500);
        setTimeout("parent.autoResize('iframe43')",1000);
        setTimeout("parent.autoResize('iframe43')",1500);
        setTimeout("parent.autoResize('iframe43')",2000);
        setTimeout("parent.autoResize('iframe43')",2500);
        setTimeout("parent.autoResize('iframe43')",3000);
        setTimeout("parent.autoResize('iframe43')",3500);
        setTimeout("parent.autoResize('iframe43')",4000);
        setTimeout("parent.autoResize('iframe43')",4500);
        setTimeout("parent.autoResize('iframe43')",5000);
        setTimeout("parent.autoResize('iframe43')",5500);
        setTimeout("parent.autoResize('iframe43')",6000);
        setTimeout("parent.autoResize('iframe43')",6500);
        setTimeout("parent.autoResize('iframe43')",7000);
        setTimeout("parent.autoResize('iframe43')",7500);
        setTimeout("parent.autoResize('iframe43')",8000);
        setTimeout("parent.autoResize('iframe43')",8500);
        setTimeout("parent.autoResize('iframe43')",9000);
        setTimeout("parent.autoResize('iframe43')",9500);
        setTimeout("parent.autoResize('iframe43')",10000);
    </script>
    <?php //VALIDACIONES ?>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="llamadas.js?<?php echo time() ?>"></script>

    <?php //CALENDARIO ?>
    <link type="text/css" rel="stylesheet" href="../../css/dhtmlgoodies_calendar.css?random=20051112" media="screen"></LINK>
    <SCRIPT type="text/javascript" src="../../js/dhtmlgoodies_calendar.js?random=20060118"></script>

    <?php //********ESTAS LIBRERIAS JS Y CSS SIRVEN PARA HACER LA BUSQUEDA DINAMICA CON CHECKLIST************//?>
    <link rel="stylesheet" type="text/css" href="../../css/checklist/jquery.multiselect.css" />
    <link rel="stylesheet" type="text/css" href="../../css/checklist/jquery.multiselect.filter.css" />
    <link rel="stylesheet" type="text/css" href="../../css/checklist/styleselect.css" />
    <link rel="stylesheet" type="text/css" href="../../css/checklist/prettify.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../js/checklist/jquery.multiselect.js"></script>
    <script type="text/javascript" src="../../js/checklist/jquery.multiselect.filter.js"></script>
    <script type="text/javascript" src="../../js/checklist/prettify.js"></script>
    <?php //**************************************************************************************************//?>
</head>
<body>
<form name="form1" id="form1" action="" method="post" onsubmit="return selectedVals();">

    <table style="width: 100%">
        <tr>
            <td class="auto-style1" style="cursor:pointer" colspan="8">
                <strong>ASIGNACION ALERTAS</strong></td>
        </tr>
        <tr>
            <td colspan="8" class="auto-style2">
                <div style="float: left; width: 40%">
                <table>
                    <thead>
                    <tr><th class="auto-style15"></th>
                        <th class="auto-style15"></th>
                    <th></th>
                    </tr>
                    <tr><th class="auto-style15">ALERTA</th>
                        <th class="auto-style15">CANT.USUARIOS</th>
                    <th></th>
                    </tr></thead>
                    <tbody>
                    <?php
                    $con = mysqli_query($conectar,"select * from tipo_alerta_sistema order by tas_nombre");
                    $num = mysqli_num_rows($con);
                    for($i = 0; $i < $num; $i++)
                    {
                        $dato = mysqli_fetch_array($con);
                        $clave = $dato['tas_clave_int'];
                        $nom = $dato['tas_nombre'];
                        $conca = mysqli_query($conectar,"select count(asi_clave_int) canta from asignacion_alerta where tas_clave_int = '".$clave."'");
                        $datca = mysqli_fetch_array($conca);
                        $canta = $datca['canta']; if($canta=="" || $canta==NULL){ $canta = 0; }
                        ?>
                        <tr>
                            <td style="text-align: left"><?php echo $nom;?></td>
                            <td style="text-align: center"><span id="canta_<?php echo $clave;?>"><?php echo $canta;?></span> </td>
                            <td><button type="button" style="background-color: transparent; border: thin">
                                <img src="../../images/Plus.png" id="IMGINFORMACIONCAJERO" onclick="CRUDALERTAS('ASIGNARUSUARIOS','<?PHP echo $clave;?>','')"></button></td></tr>
                        <tr><td colspan="3"><hr></td></tr>
                        <?php
                    }
                    ?>
                   </tbody>
                </table>
                </div>
                <div style="float: left; width: 60%" id="divasignar">

                </div>

            </td>
        </tr>
        <tr>
            <td style="width: 100px">&nbsp;</td>
            <td style="width: 210px">&nbsp;</td>
            <td style="width: 90px">&nbsp;</td>
            <td style="width: 100px">&nbsp;</td>
            <td style="width: 80px">&nbsp;</td>
            <td style="width: 100px">&nbsp;</td>
            <td style="width: 50px">&nbsp;</td>
            <td style="width: 49px">&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 100px">&nbsp;</td>
            <td style="width: 210px">&nbsp;</td>
            <td style="width: 90px">&nbsp;</td>
            <td style="width: 100px">&nbsp;</td>
            <td style="width: 80px">&nbsp;</td>
            <td style="width: 100px">&nbsp;</td>
            <td style="width: 50px">&nbsp;</td>
            <td style="width: 49px">&nbsp;</td>
        </tr>
    </table>
    <script type="text/javascript">
        $("#buscajero").multiselect().multiselectfilter();
        $("#busalerta").multiselect().multiselectfilter();
        $("#busactor").multiselect().multiselectfilter();
    </script>
</form>
</body>
</html>