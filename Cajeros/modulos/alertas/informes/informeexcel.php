<?php
include('../../../data/Conexion.php');
require_once('../../../Classes/PHPExcel.php');
date_default_timezone_set('America/Bogota');
session_start();
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];
	
// verifica si no se ha loggeado
if(!isset($_SESSION["persona"]))
{
  session_destroy();
  header("LOCATION:index.php");
}else{
}
function CORREGIRTEXTO($string)
{
    $string = str_replace("REEMPLAZARNUMERAL", "#", $string);
    $string = str_replace("REEMPLAZARMAS", "+",$string);
    $string = str_replace("REEMPLAZARMENOS", "-", $string);
    return $string;
}
$caj = $_GET['caj'];
$ale = $_GET['ale'];
$act = $_GET['act'];
$fid = $_GET['fid'];
$fih = $_GET['fih'];
$ftd = $_GET['ftd'];
$fth = $_GET['fth'];
$fed = $_GET['fed'];
$feh = $_GET['feh'];

$fecha=date("d/m/Y");

//************ESTILOS******************

$styleA1 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => '000000'),
    'size'  => 12,
    'name'  => 'Calibri'
));
$styleA2 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => 'C83000'),
    'size'  => 10,
    'name'  => 'Arial'
));
$styleA3 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => '000000'),
    'size'  => 10,
    'name'  => 'Arial'
));
$styleA4 = array(
'font'  => array(
    'bold'  => false,
    'color' => array('rgb' => '000000'),
    'size'  => 10,
    'name'  => 'Arial'
));
$styleA3p1 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => '000000'),
    'size'  => 9,
    'name'  => 'Arial'
));
$styleA4p1 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => '000000'),
    'size'  => 9,
    'name'  => 'Arial'
));

$borders = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '000000'),
		)
	),
);

//*************************************

$objPHPExcel = new PHPExcel();
$archivo = 'PAVAS - Informe Alertas.xls';

//Propiedades de la hoja de excel
$objPHPExcel->getProperties()
		->setCreator("PAVAS TECNOLOGIA")
		->setLastModifiedBy("PAVAS TECNOLOGIA")
		->setTitle("Informe Alertas")
		->setSubject("Informe Alertas")
		->setDescription("Documento generado con el software Control de Materiales")
		->setKeywords("Control de Materiales")
		->setCategory("Reportes");
		
//Ancho de las Columnas
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);

//************A1**************
$objPHPExcel->getActiveSheet()->getStyle('A1')-> applyFromArray($styleA1);//
$objPHPExcel->getActiveSheet()->getCell('A1')->setValue("LISTA DE ALERTAS");
$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setWrapText(true); //Crea un enter entre palabras
$objPHPExcel->getActiveSheet()->mergeCells('A1:H1');//Conbinar celdas
$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('00D8D8D8');//COLOR DE FONDO
//****************************

/**************** COLUMNAS ****************/

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('A2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('A2')->setValue("Cajero");
$objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('B2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('B2')->setValue("Nombre");
$objPHPExcel->getActiveSheet()->getStyle('B2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('C2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('C2')->setValue("Alerta");
$objPHPExcel->getActiveSheet()->getStyle('C2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('D2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('D2')->setValue("Actor");
$objPHPExcel->getActiveSheet()->getStyle('D2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('E2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('E2')->setValue("Fecha Inicio");
$objPHPExcel->getActiveSheet()->getStyle('E2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('F2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('F2')->setValue("Fecha teorica entrega");
$objPHPExcel->getActiveSheet()->getStyle('F2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('G2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('G2')->setValue("Fecha entrega");
$objPHPExcel->getActiveSheet()->getStyle('G2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('H2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('H2')->setValue("Días retraso");
$objPHPExcel->getActiveSheet()->getStyle('H2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

$seleccionados = explode(',',$caj);
$num = count($seleccionados);
$cajeros = array();
for($i = 0; $i < $num; $i++)
{
	if($seleccionados[$i] != '')
	{
		$cajeros[$i]=$seleccionados[$i];
	}
}
$listacajeros=implode(',',$cajeros);

$seleccionados = explode(',',$ale);
$num = count($seleccionados);
$alertas = array();
for($i = 0; $i < $num; $i++)
{
	if($seleccionados[$i] != '')
	{
		$alertas[$i]=$seleccionados[$i];
	}
}
$listaalertas=implode(',',$alertas);

$seleccionados = explode(',',$act);
$num = count($seleccionados);
$actores = array();
for($i = 0; $i < $num; $i++)
{
	if($seleccionados[$i] != '')
	{
		$actores[$i]=$seleccionados[$i];
	}
}
$listaactores=implode(',',$actores);

if($listacajeros != ''){ if($sql1 == ''){	$sql1 = "a.caj_clave_int in (".$listacajeros.")"; }else{ $sql1 = $sql1." and a.caj_clave_int in (".$listacajeros.")"; } }

if($listaalertas != ''){ if($sql1 == ''){	$sql1 = "a.tal_clave_int in (".$listaalertas.")"; }else{ $sql1 = $sql1." and a.tal_clave_int in (".$listaalertas.")"; } }

if($listaactores != ''){ if($sql1 == ''){	$sql1 = "act.act_clave_int in (".$listaactores.")"; }else{ $sql1 = $sql1." and act.act_clave_int in (".$listaactores.")"; } }

if($sql1 == ''){ $sql1 = "((a.ale_fecha_inicio BETWEEN '".$fid." 00:00:00' AND '".$fih." 23:59:59') or ('".$fid."' Is Null and '".$fih."' Is Null) or ('".$fid."' = '' and '".$fih."' = ''))"; }else{ $sql1 = $sql1." and ((a.ale_fecha_inicio BETWEEN '".$fid." 00:00:00' AND '".$fih." 23:59:59') or ('".$fid."' Is Null and '".$fih."' Is Null) or ('".$fid."' = '' and '".$fih."' = ''))"; }

if($sql1 == ''){ $sql1 = "(CASE a.tal_clave_int 
WHEN 6 THEN (a.ale_fecha_entrega BETWEEN '".$ftd." 00:00:00' AND '".$fth." 23:59:59') or ('".$ftd."' Is Null and '".$fth."' Is Null) or ('".$ftd."' = '' and '".$fth."' = '') 
WHEN 1 THEN (ADDDATE(a.ale_fecha_inicio, INTERVAL 45 DAY) BETWEEN '".$ftd." 00:00:00' AND '".$fth." 23:59:59') or ('".$ftd."' Is Null and '".$fth."' Is Null) or ('".$ftd."' = '' and '".$fth."' = '') 
WHEN 2 THEN (ADDDATE(a.ale_fecha_inicio, INTERVAL 7 DAY) BETWEEN '".$ftd." 00:00:00' AND '".$fth." 23:59:59') or ('".$ftd."' Is Null and '".$fth."' Is Null) or ('".$ftd."' = '' and '".$fth."' = '') 
WHEN 3 THEN (ADDDATE(a.ale_fecha_inicio, INTERVAL 7 DAY) BETWEEN '".$ftd." 00:00:00' AND '".$fth." 23:59:59') or ('".$ftd."' Is Null and '".$fth."' Is Null) or ('".$ftd."' = '' and '".$fth."' = '') 
WHEN 4 THEN (ADDDATE(a.ale_fecha_inicio, INTERVAL 90 DAY) BETWEEN '".$ftd." 00:00:00' AND '".$fth." 23:59:59') or ('".$ftd."' Is Null and '".$fth."' Is Null) or ('".$ftd."' = '' and '".$fth."' = '') 
WHEN 5 THEN (a.ale_fecha_entrega BETWEEN '".$ftd." 00:00:00' AND '".$fth." 23:59:59') or ('".$ftd."' Is Null and '".$fth."' Is Null) or ('".$ftd."' = '' and '".$fth."' = '') 
END)"; }else{ $sql1 = $sql1." and (CASE a.tal_clave_int 
WHEN 6 THEN (a.ale_fecha_entrega BETWEEN '".$ftd." 00:00:00' AND '".$fth." 23:59:59') or ('".$ftd."' Is Null and '".$fth."' Is Null) or ('".$ftd."' = '' and '".$fth."' = '') 
WHEN 1 THEN (ADDDATE(a.ale_fecha_inicio, INTERVAL 45 DAY) BETWEEN '".$ftd." 00:00:00' AND '".$fth." 23:59:59') or ('".$ftd."' Is Null and '".$fth."' Is Null) or ('".$ftd."' = '' and '".$fth."' = '') 
WHEN 2 THEN (ADDDATE(a.ale_fecha_inicio, INTERVAL 7 DAY) BETWEEN '".$ftd." 00:00:00' AND '".$fth." 23:59:59') or ('".$ftd."' Is Null and '".$fth."' Is Null) or ('".$ftd."' = '' and '".$fth."' = '') 
WHEN 3 THEN (ADDDATE(a.ale_fecha_inicio, INTERVAL 7 DAY) BETWEEN '".$ftd." 00:00:00' AND '".$fth." 23:59:59') or ('".$ftd."' Is Null and '".$fth."' Is Null) or ('".$ftd."' = '' and '".$fth."' = '') 
WHEN 4 THEN (ADDDATE(a.ale_fecha_inicio, INTERVAL 90 DAY) BETWEEN '".$ftd." 00:00:00' AND '".$fth." 23:59:59') or ('".$ftd."' Is Null and '".$fth."' Is Null) or ('".$ftd."' = '' and '".$fth."' = '') 
WHEN 5 THEN (a.ale_fecha_entrega BETWEEN '".$ftd." 00:00:00' AND '".$fth." 23:59:59') or ('".$ftd."' Is Null and '".$fth."' Is Null) or ('".$ftd."' = '' and '".$fth."' = '') END)"; }

if($sql1 == ''){ $sql1 = "(CASE a.tal_clave_int 
WHEN 6 THEN (a.ale_fecha_entrega_cajero BETWEEN '".$fed." 00:00:00' AND '".$feh." 23:59:59') or ('".$fed."' Is Null and '".$feh."' Is Null) or ('".$fed."' = '' and '".$feh."' = '') 
ELSE
(a.ale_fecha_entrega BETWEEN '".$fed." 00:00:00' AND '".$feh." 23:59:59') or ('".$fed."' Is Null and '".$feh."' Is Null) or ('".$fed."' = '' and '".$feh."' = '')
END)"; }else{ $sql1 = $sql1." and (CASE a.tal_clave_int 
WHEN 6 THEN (a.ale_fecha_entrega_cajero BETWEEN '".$fed." 00:00:00' AND '".$feh." 23:59:59') or ('".$fed."' Is Null and '".$feh."' Is Null) or ('".$fed."' = '' and '".$feh."' = '') 
ELSE
(a.ale_fecha_entrega BETWEEN '".$fed." 00:00:00' AND '".$feh." 23:59:59') or ('".$fed."' Is Null and '".$feh."' Is Null) or ('".$fed."' = '' and '".$feh."' = '')
END)"; }

$con = mysqli_query($conectar,"select ale_clave_int,c.caj_clave_int,c.caj_nombre,tal_nombre,act_nombre,a.tal_clave_int,ale_fecha_inicio,ale_fecha_entrega,ale_fecha_entrega_cajero from alerta a inner join cajero c on (c.caj_clave_int = a.caj_clave_int) inner join tipo_alerta ta on (ta.tal_clave_int = a.tal_clave_int) inner join actor act on (act.act_clave_int = ta.act_clave_int) where ".$sql1." and a.ale_sw_eliminado = 0");
$num = mysqli_num_rows($con);
			
$j = 3;
for($i = 0; $i < $num; $i++)
{
	$dato = mysqli_fetch_array($con);
	$cla = $dato['ale_clave_int'];
	$caj = $dato['caj_clave_int'];
	$nomcaj = CORREGIRTEXTO($dato['caj_nombre']);
	$ale = $dato['tal_nombre'];
	$act = $dato['act_nombre'];
	$tip = $dato['tal_clave_int'];
	$fi = $dato['ale_fecha_inicio'];
	$fe = $dato['ale_fecha_entrega'];
	$fecaj = $dato['ale_fecha_entrega_cajero'];
	
	if($tip == 1)
	{
		$con1 = mysqli_query($conectar,"select ADDDATE('".$fi."', INTERVAL 45 DAY) fecteo from usuario LIMIT 1");
		$dato1 = mysqli_fetch_array($con1);
		$fecteo = $dato1['fecteo'];
	}
	else
	if($tip == 2 or $tip == 3)
	{
		$con1 = mysqli_query($conectar,"select ADDDATE('".$fi."', INTERVAL 7 DAY) fecteo from usuario LIMIT 1");
		$dato1 = mysqli_fetch_array($con1);
		$fecteo = $dato1['fecteo'];
	}
	else
	if($tip == 4)
	{
		$con1 = mysqli_query($conectar,"select ADDDATE('".$fi."', INTERVAL 90 DAY) fecteo from usuario LIMIT 1");
		$dato1 = mysqli_fetch_array($con1);
		$fecteo = $dato1['fecteo'];
	}
	else
	if($tip == 5)
	{
		$fecteo = $fe;
	}
	
	if($tip == 6)
	{
		if($fecaj == '0000-00-00')
		{
			$condias = mysqli_query($conectar,"select DATEDIFF(CURDATE(),'".$fe."') dias from usuario LIMIT 1");
			$datodias = mysqli_fetch_array($condias);
			$dias = $datodias['dias'];
		}
		else
		{
			$condias = mysqli_query($conectar,"select DATEDIFF('".$fecaj."','".$fe."') dias from usuario LIMIT 1");
			$datodias = mysqli_fetch_array($condias);
			$dias = $datodias['dias'];
		}
	}
	else
	if($fe == '0000-00-00' or $tip == 5)
	{
		$condias = mysqli_query($conectar,"select DATEDIFF(CURDATE(),'".$fecteo."') dias from usuario LIMIT 1");
		$datodias = mysqli_fetch_array($condias);
		$dias = $datodias['dias'];
	}
	else
	{
		$condias = mysqli_query($conectar,"select DATEDIFF('".$fe."','".$fecteo."') dias from usuario LIMIT 1");
		$datodias = mysqli_fetch_array($condias);
		$dias = $datodias['dias'];
	}
	
	if($tip == 6){ $fte = $fe; }else { $fte = $fecteo; }
	if($tip == 6){ $entrega = $fecaj; }else { $entrega = $fe; }
	
	/**************** DATOS DE LAS COLUMNAS ****************/ 

	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('A'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("A".$j, $caj);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('B'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("B".$j, $nomcaj);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('C'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("C".$j, $ale);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('D'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("D".$j, $act);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('E'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("E".$j, $fi);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('F'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("F".$j, $fte);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('G'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("G".$j, $entrega);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('H'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("H".$j, $dias);
	//****************************
	
	$j++;
}
//DATOS DE SALIDA DEL EXCEL
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$archivo.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
?>