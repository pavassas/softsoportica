// JavaScript Document/* Formatting function for row details - modify as you need */

$(document).ready(function(e) {

		var selected = [];

        var consec = form1.busconsecutivo.value;
        var nom = form1.busnombre.value;
        var anocon = form1.busanocontable.value;
        var cencos = form1.buscentrocostos.value;
        var cod = form1.buscodigo.value;
        var reg = form1.busregion.value;
        var mun = form1.busmunicipio.value;
        var tip = form1.bustipologia.value;
        var tipint = form1.bustipointervencion.value;
        var moda = form1.busmodalidad.value;
        var actor = form1.busactor.value;
        var est = form1.ocultoestado.value;
		
		var table2 = $('#tbCajeros').DataTable( {

            "dom": '<"top"if>rt<"bottom"lp><"clear">',
            "columnDefs": [],
            "ordering": true,
		    "info": true,
		    "autoWidth": true,
		    "pagingType": "simple_numbers",
		    "lengthMenu": [[20,30,50,-1], [20,30,50,"Todos"]],
		    "language": {
                "sProcessing": "Buscando datos..",
		    "lengthMenu": "Ver _MENU_ registros",
		    "zeroRecords": "No se encontraron datos",
		    "info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		    "infoEmpty": "No se encontraron datos",
		    "infoFiltered": "",
		    "paginate": {"previous": "Anterior","next":"Siguiente"}

		    },
            "searching":false,
		    "processing": true,
            "serverSide": true,
            "ajax": {
                    "url": "busquedacajeros.php",
                    "data": {
                        consec:consec,nom:nom,anocon:anocon,cencos:cencos,cod:cod,reg:reg,mun:mun,tip:tip,tipint:tipint,moda:moda,est:est,actor:actor
                    },
                "type": "POST"
				},
			"columns": [
                { "data" : "Editar", "orderable": false },
			    { "data" : "Cajero", "orderable": true },
                { "data" : "Codigo" , "className" : "dt-left" },
                { "data" : "Nombre", "className" : "dt-left"},
                { "data" : "Hijo", "className" : "dt-left"},
                { "data" : "Region", "className" : "dt-left" },
                { "data" : "Tipologia" , "className" : "dt-left"},
                { "data" : "Intervencion" , "className" : "dt-left" },
                { "data" : "Estado" , "className" : "dt-left" },
                /*
                { "data" : "TipoProyecto" , "className" : "dt-left"},

			    { "data" : "Direccion", "className" : "dt-left"},
			    { "data" : "Contable", "className" : "dt-left" },

			    { "data" : "Departamento", "className" : "dt-left" },
			    { "data" : "Municipio", "className" : "dt-left" },


                { "data" : "Modalidad" , "className" : "dt-left" },
                { "data" : "FecInicio" , "className" : "dt-left" },


                { "data" : "Cencos" , "className" : "dt-left" },
                { "data" : "RefMaquina" , "className" : "dt-left" },
                { "data" : "Ubicacion" , "className" : "dt-left" },
                { "data" : "CodSuc" , "className" : "dt-left" },
                { "data" : "UbicacionATM" , "className" : "dt-left" },
                { "data" : "Atiende" , "className" : "dt-left" },
                { "data" : "Riesgo" , "className" : "dt-left" },
                { "data" : "Area" , "className" : "dt-left" },
                { "data" : "ApagadoATM" , "className" : "dt-left" },

                { "data" : "Inmobiliaria" , "className" : "dt-left" },
                { "data" : "EstadoInmobiliaria" , "className" : "dt-left" },
                { "data" : "FIInmobiliaria" , "className" : "dt-left" },
                { "data" : "FEInmobiliaria" , "className" : "dt-left" },
                { "data" : "TDInmobiliaria" , "className" : "dt-left" },
                { "data" : "NInmobiliaria" , "className" : "dt-left" },

                { "data" : "Visitante" , "className" : "dt-left" },
                { "data" : "EstadoVisitante" , "className" : "dt-left" },
                { "data" : "FIVisitante" , "className" : "dt-left" },
                { "data" : "FEVisitante" , "className" : "dt-left" },
                { "data" : "TDVisitante" , "className" : "dt-left" },
                { "data" : "NVisitante" , "className" : "dt-left" },

                { "data" : "Disenador" , "className" : "dt-left" },
                { "data" : "EstadoDisenador" , "className" : "dt-left" },
                { "data" : "FIDisenador" , "className" : "dt-left" },
                { "data" : "FEDisenador" , "className" : "dt-left" },
                { "data" : "TDDisenador" , "className" : "dt-left" },
                { "data" : "NDisenador" , "className" : "dt-left" },

                { "data" : "Gestionador" , "className" : "dt-left" },
                { "data" : "EstadoLicencia" , "className" : "dt-left" },
                { "data" : "FILicencia" , "className" : "dt-left" },
                { "data" : "FELicencia" , "className" : "dt-left" },
                { "data" : "TDLicencia" , "className" : "dt-left" },
                { "data" : "NLicencia" , "className" : "dt-left" },

                { "data" : "Interventor" , "className" : "dt-left" },
                { "data" : "OpCanal" , "className" : "dt-left" },
                { "data" : "FIObra" , "className" : "dt-left" },
                { "data" : "FEObra" , "className" : "dt-left" },
                { "data" : "FPSuministro" , "className" : "dt-left" },
                { "data" : "NInterventor" , "className" : "dt-left" },

                { "data" : "Constructor" , "className" : "dt-left" },
                { "data" : "Avance" , "className" : "dt-left" },
                { "data" : "FIConstructor" , "className" : "dt-left" },
                { "data" : "FEConstructorATM" , "className" : "dt-left" },
                { "data" : "TDConstructor" , "className" : "dt-left" },
                { "data" : "NConstructor" , "className" : "dt-left" },
                { "data" : "IFConstructor" , "className" : "dt-left" },

                { "data" : "Seguridad" , "className" : "dt-left" },
                { "data" : "CodMonitoreo" , "className" : "dt-left" },
                { "data" : "FINObra" , "className" : "dt-left" },
                { "data" : "NSeguridad" , "className" : "dt-left" },

                { "data" : "ComiteAprobado" , "className" : "dt-left" },
                { "data" : "FITComite" , "className" : "dt-left" },
                { "data" : "FETComite" , "className" : "dt-left" },
                { "data" : "FechaComite" , "className" : "dt-left" },

                { "data" : "ContratoAprobado" , "className" : "dt-left" },
                { "data" : "EstadoContrato" , "className" : "dt-left" },
                { "data" : "FITContrato" , "className" : "dt-left" },
                { "data" : "FETContrato" , "className" : "dt-left" },
                { "data" : "FechaContrato" , "className" : "dt-left" },

                { "data" : "Máquina" , "className" : "dt-left" },

                { "data" : "PreAprobada" , "className" : "dt-left" },
                { "data" : "FITPrefactibilidad" , "className" : "dt-left" },
                { "data" : "FETPrefactibilidad" , "className" : "dt-left" },
                { "data" : "FechaPrefactibilidad" , "className" : "dt-left" },

                { "data" : "FITProyeccion" , "className" : "dt-left" },
                { "data" : "FETProyeccion" , "className" : "dt-left" },

                { "data" : "FITCanal" , "className" : "dt-left" },
                { "data" : "FETCanal" , "className" : "dt-left" },
                { "data" : "FICanal" , "className" : "dt-left" },
                { "data" : "FECanal" , "className" : "dt-left" },

                { "data" : "NotGenerales" , "className" : "dt-left" }*/
            ],

		    "order": [[1, 'asc']]
        } );
     
	 $('#tbCajeros tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
         OCULTARSCROLL();
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );
	
	   var table2 = $('#tbCajeros').DataTable();
 
    // Apply the search
    table2.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
    
	
	//detalle
	// Array to track the ids of the details displayed rows
    var detailRows = [];
 
    $('#tbCajeros tbody').on( 'click', 'tr td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table2.row( tr );
        var idx = $.inArray( tr.attr('id'), detailRows );
 
        if ( row.child.isShown() ) {
            tr.removeClass( 'shown' );
            row.child.hide();
 
            // Remove from the 'open' array
            detailRows.splice( idx, 1 );
        }
        else {
            tr.addClass( 'shown' );
            row.child( format( row.data() ) ).show();
 
            // Add to the 'open' array
            if ( idx === -1 ) {
                detailRows.push( tr.attr('id') );
            }
        }
    } );
 
    // On each draw, loop over the `detailRows` array and show any child rows
    table2.on( 'draw', function () {
        $.each( detailRows, function ( i, id ) {
            $('#'+id+' td.details-control').trigger( 'click' );
        } );
    } );
});

function format ( d ) {
 
	return '<div class="row"><div class="col-md-3"><strong>Usuario: </strong>'+d.Cliente+'<br/>'+
	'<strong>Direccion: </strong>'+d.Direccion+'&nbsp;&nbsp;'+
	'<strong>Telefono: </strong>'+d.Telefono+'<br/>'+
	'<strong>Descripción: </strong><br/>'+d.Descripcion+'<br/>'+
	'<strong>Creado Por: </strong>'+d.Usuario+'&nbsp;&nbsp;'+
	'<strong>Actualización: </strong>'+d.Fecha+d.Correo+d.Cotizacion+
	'</div>'+
	d.Soluciones+
	'</div>';
        
}


