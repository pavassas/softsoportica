<?php
session_start();
include('../../data/Conexion.php');
require_once('../../Classes/PHPMailer-master/class.phpmailer.php');
//header("Cache-Control: no-store, no-cache, must-revalidate");

date_default_timezone_set('America/Bogota');
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
ini_set('memory_limit','500M');
ini_set('max_execution_time', 600);
ini_set('upload_max_filesize', '250M');
error_reporting(0);
//set_time_limit(600);
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada

$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];
$usuario = $_COOKIE['usuario'];
$fecha = date("Ymd");
$fechaactual = date('Y/m/d H:i:s');
$clacaj = $_POST['clacaj'];
$actor = $_POST['actor'];
$arc = $_POST['archivo'];
$claada = $_POST['claada'];
$max=8000000;//8MB



function CORREGIRTEXTO($string)
{
    $string = str_replace("REEMPLAZARNUMERAL", "#", $string);
    $string = str_replace("REEMPLAZARMAS", "+",$string);
    $string = str_replace("REEMPLAZARMENOS", "-", $string);
    return $string;
}



if($clacaj != '' and $clacaj != 0 and $actor != '')
{
	if(is_array($_FILES)) 
	{
		mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'6',5,'".$clacaj."','".$usuario."','".$fechaact."')");//Tercer campo tia_clave_int. 5=Adjuntar archivo

		$con = mysqli_query($conectar,"select MAX(ada_clave_int) max from adjunto_actor where caj_clave_int = '".$clacaj."'");
		$dato = mysqli_fetch_array($con);
		$num = $dato['max'];
		
		if($num == 0 || $num == '')
		{
			$num = 1;
		}
		else
		{
			$num = $num+1;
		}
				
		/*if($actor == 'COTIZACION')
		{
			if(is_uploaded_file($_FILES['adjuntocot']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntocot']['tmp_name'];
				$archivo = basename($_FILES['adjuntocot']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				
				$prefijo = "CONSTRUCTOR - COTIZACION";
				$destino =  "../../adjuntos/constructor/".$clacaj."-".$prefijo."".$num.".".$extension;
                $filesize = $_FILES['adjuntocot']['size'];
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and caj_clave_int = '".$clacaj."' and tad_clave_int = '15' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "error4";
                }
                else
                    if($filesize>$max)
                    {
                        echo "error3";
                    }
                    else
                if(strtoupper($extension)=="DWG" || strtoupper($extension)=="RAR" || strtoupper($extension)=="ZIP")
                {
                    echo "error1";
                }
                else
                {
				
                    if(move_uploaded_file($sourcePath,$destino))
                    {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,caj_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'".$clacaj."','".$fechaactual."','".$destino."','".$archivo."',15,'".$usuario."','".$fechaactual."')");
                    }
                    else
                    {
                        echo "error2";
                    }
                }
			}
		}
		else*/
		if($actor == 'INFORMEFINAL' || $actor=="LIQUIDACION" || $actor=="ACTAS" || $actor=="ESCOMBROS" || $actor=="COTIZACION" || $actor=="FACTURACION" || $actor=="AVANCE")
		{
			if(is_uploaded_file($_FILES['adjuntoinf']['tmp_name']) || is_uploaded_file($_FILES['adjuntoliq']['tmp_name']) || is_uploaded_file($_FILES['adjuntoact']['tmp_name']) || is_uploaded_file($_FILES['adjuntoesc']['tmp_name']) || is_uploaded_file($_FILES['adjuntocot']['tmp_name']) || is_uploaded_file($_FILES['adjuntofac']['tmp_name']) || is_uploaded_file($_FILES['adjuntoava']['tmp_name']))
			{
				if($actor=="INFORMEFINAL")
				{
					$sourcePath = $_FILES['adjuntoinf']['tmp_name'];
					$archivo = basename($_FILES['adjuntoinf']['name']);
					$prefijo = "CONSTRUCTOR - INFORME FINAL";
					$tad = 16;
					$tas = 9;
					$subject = "Alerta Adjunto Informe CONSTRUCTOR. Cajero Nro. ".$clacaj;
					$descripcion = "CONTROL CAJEROS registra que el constructor adjunto el informe final.\n";
					$filesize = $_FILES['adjuntoinf']['size'];
				}
				else if($actor=="LIQUIDACION")
				{
					$sourcePath = $_FILES['adjuntoliq']['tmp_name'];
					$archivo = basename($_FILES['adjuntoliq']['name']);
					$prefijo = "CONSTRUCTOR - INFORME LIQUIDACION";
					$tad = 35;
					$tas = 24;
					$subject = "Alerta Adjunto Informe Liquidación . Cajero Nro. ".$clacaj;
					$descripcion = "CONTROL CAJEROS registra que el constructor adjunto informe liquidación.\n";
					$filesize = $_FILES['adjuntoliq']['size'];

				}
				else if($actor=="ACTAS")
				{
					$sourcePath = $_FILES['adjuntoact']['tmp_name'];
					$archivo = basename($_FILES['adjuntoact']['name']);
					$prefijo = "CONSTRUCTOR - INFORME ACTAS";
					$tad = 36;
					$tas = 25;
					$subject = "Alerta Adjunto Informe Actas . Cajero Nro. ".$clacaj;
					$descripcion = "CONTROL CAJEROS registra que el constructor adjunto informe actas.\n";
					$filesize = $_FILES['adjuntoact']['size'];

				}

				else if($actor=="ESCOMBROS")
				{
					$sourcePath = $_FILES['adjuntoesc']['tmp_name'];
					$archivo = basename($_FILES['adjuntoesc']['name']);
					$prefijo = "CONSTRUCTOR - INFORME CERTIFICADO ESCOMBROS";
					$tad = 37;
					$tas = 26;
					$subject = "Alerta Adjunto Informe Certificado Escombros . Cajero Nro. ".$clacaj;
					$descripcion = "CONTROL CAJEROS registra que el constructor adjunto informe Certificado Escombros.\n";
					$filesize = $_FILES['adjuntoesc']['size'];
				}
				else if($actor=="COTIZACION")
				{
					$sourcePath = $_FILES['adjuntocot']['tmp_name'];
					$archivo = basename($_FILES['adjuntocot']['name']);
					$prefijo = "CONSTRUCTOR - INFORME COTIZACION";
					$tad = 15;
					$tas = 27;
					$subject = "Alerta Adjunto Informe Cotización . Cajero Nro. ".$clacaj;
					$descripcion = "CONTROL CAJEROS registra que el constructor adjunto informe Cotización .\n";
					$filesize = $_FILES['adjuntocot']['size'];
				}
				else if($actor=="FACTURACION")
				{
					$sourcePath = $_FILES['adjuntofac']['tmp_name'];
					$archivo = basename($_FILES['adjuntofac']['name']);
					$prefijo = "CONSTRUCTOR - INFORME FACTURACION";
					$tad = 17;
					$tas = 28;
					$subject = "Alerta Adjunto Informe Facturación . Cajero Nro. ".$clacaj;
					$descripcion = "CONTROL CAJEROS registra que el constructor adjunto informe Facturación .\n";
					$filesize = $_FILES['adjuntofac']['size'];
				}
				else if($actor=="AVANCE")
				{
					$sourcePath = $_FILES['adjuntoava']['tmp_name'];
					$archivo = basename($_FILES['adjuntoava']['name']);
					$prefijo = "CONSTRUCTOR - INFORME AVANCE";
					$tad = 18;
					$tas = 29;
					$subject = "Alerta Adjunto Informe Avance . Cajero Nro. ".$clacaj;
					$descripcion = "CONTROL CAJEROS registra que el constructor adjunto informe Avance .\n";
					$filesize = $_FILES['adjuntoava']['size'];
				}
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
				//$prefijo = "CONSTRUCTOR - INFORME FINAL";


				$destino =  "../../adjuntos/constructor/".$clacaj."-".$prefijo."".$num.".".$extension;
                
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and caj_clave_int = '".$clacaj."' and tad_clave_int = '".$tad."' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "error4";
                }
                else
                if($filesize>$max)
                {
                    echo "error3";
                }
                else
                if(strtoupper($extension)=="DWG" || strtoupper($extension)=="RAR" || strtoupper($extension)=="ZIP")
                {
                    echo "error1";
                }
                else
                {
				    if(move_uploaded_file($sourcePath,$destino))
					{
						$sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,caj_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'".$clacaj."','".$fechaactual."','".$destino."','".$archivo."','".$tad."','".$usuario."','".$fechaactual."')");
						
					//Envio correo de alerta a Yurany incapie cuando el actor constructor adjunte informe final
                    //alerta11
	                    $conu = mysqli_query($conectar,"select usu_clave_int from asignacion_alerta where tas_clave_int = '".$tas."'");
	                    $numu = mysqli_num_rows($conu);
	                    if($numu>0)
	                    {
	                        $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario where usu_clave_int in (select usu_clave_int from asignacion_alerta where tas_clave_int = '".$tas."')");
	                        $num = mysqli_num_rows($conema);
	                    }
	                    else
	                    {
	                    	$num =  0;
	                        //$conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario where usu_usuario in('juanpablo.mejia')");
	                    }
                    //$conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario where usu_usuario in('yurany.hincapie','juanpablo.mejia')");
						
						$concaj = mysqli_query($conectar,"select caj_nombre,cac_fecha_entrega_atm  from cajero c left outer join cajero_constructor cajcon on (cajcon.caj_clave_int = c.caj_clave_int)  where c.caj_clave_int = '".$clacaj."'");
						$datocaj = mysqli_fetch_array($concaj);
						$nomcaj = CORREGIRTEXTO($datocaj['caj_nombre']);
						$fecentatm = $datocaj['cac_fecha_entrega_atm'];


						$concons = mysqli_query($conectar,"select usu_nombre from usuario where usu_usuario = '".$usuario."'");
						$datocons = mysqli_fetch_array($concons);
						$nomcons = $datocons['usu_nombre'];
						
						if($num>0)
						{


							for($i = 0; $i < $num; $i++)
							{
								$datoema = mysqli_fetch_array($conema);
								$nom = $datoema['usu_nombre'];
							    $ema = $datoema['usu_email'];
						
						
								$mail = new PHPMailer();
								
								$mail->From = "adminpavas@pavas.com.co";
								$mail->FromName = "Soportica";
								$mail->AddAddress($ema, "Destino");
								$mail->Subject = $subject;
								
								// Cuerpo del mensaje
								$mail->Body .= "Hola ".$nom."!\n\n";	
								$mail->Body .= $descripcion;
								$mail->Body .= "CAJERO: ".$nomcaj."\n";
								$mail->Body .= "CONSTRUCTOR: ".$nomcons."\n";
								$mail->Body .= "FECHA ENTREGA ATM: ".$fecentatm."\n";
								$mail->Body .= date("d/m/Y H:m:s")."\n\n";
								$mail->Body .= "Este mensaje es generado automáticamente por CONTROL CAJEROS, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Cajeros \n";
						
								if(!$mail->Send())
								{
									//echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
									//echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
								}
								else
								{
									//mysqli_query($conectar,"update alerta set ale_sw_enviado = 1 where ale_clave_int = '".$a."'");
								}
							}
						}
					
						//Busco el o los interventores de este cajero para alertar que se ha adjunto el informe final
						
						$ema = '';
						$con = mysqli_query($conectar,"select * from cajero_interventoria where caj_clave_int = '".$clacaj."'");
						$dato = mysqli_fetch_array($con);
						$inter = $dato['cin_interventor'];
						
						$con1 = mysqli_query($conectar,"select * from usuario where usu_clave_int = '".$inter."'");
						$dato1 = mysqli_fetch_array($con1);
						$nom = $dato1['usu_nombre'];
						$ema = $dato1['usu_email'];
						$cat = $dato1['usu_categoria'];
					
						if($cat == 1)
						{
							$mail2 = new PHPMailer();
							
							$mail2->From = "adminpavas@pavas.com.co";
							$mail2->FromName = "Soportica";
							$mail2->AddAddress($ema, "Destino");
							$mail2->Subject = $subject;
							$mail2->Body = '';
							// Cuerpo del mensaje
							$mail2->Body .= "Hola ".$nom."!\n\n";	
							$mail2->Body .= $descripcion;
							$mail2->Body .= "CAJERO: ".$nomcaj."\n";
							$mail2->Body .= "CONSTRUCTOR: ".$nomcons."\n";
							$mail2->Body .= "FECHA ENTREGA ATM: ".$fecentatm."\n";
							$mail2->Body .= date("d/m/Y H:m:s")."\n\n";
							$mail2->Body .= "Este mensaje es generado automáticamente por CONTROL CAJEROS, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Cajeros \n";
						
							if(!$mail2->Send())
							{
							//echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
							//echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
							}
							else
							{
							}
						}
						else
						if($cat == 2)
						{
							$con = mysqli_query($conectar,"select * from usuario_actor ua inner join usuario u on (u.usu_clave_int = ua.usu_clave_int) where ua.usa_actor = '".$inter."'");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
							$mail2 = new PHPMailer();
							
							$dato1 = mysqli_fetch_array($con);
							$mail2->Body = '';
							$ema = '';
							$nom = $dato1['usu_nombre'];
							$ema = $dato1['usu_email'];
							
							$mail2->From = "adminpavas@pavas.com.co";
							$mail2->FromName = "Soportica";
							$mail2->AddAddress($ema, "Destino");
							$mail2->Subject = $subject;
							
							// Cuerpo del mensaje
							$mail2->Body .= "Hola ".$nom."!\n\n";	
							$mail2->Body .= $descripcion;
							$mail2->Body .= "CAJERO: ".$nomcaj."\n";
							$mail2->Body .= "CONSTRUCTOR: ".$nomcons."\n";
							$mail2->Body .= "FECHA ENTREGA ATM: ".$fecentatm."\n";
							$mail2->Body .= date("d/m/Y H:m:s")."\n\n";
							$mail2->Body .= "Este mensaje es generado automáticamente por CONTROL CAJEROS, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Cajeros \n";
							
								if(!$mail2->Send())
								{
								//echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
								//echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
								}
								else
								{
								}
							}
						}
						echo "ok";
					}
                    else
                	{
                    	echo "error2";
                	}
                }
			}
			else
			{
				echo "error5";	
			}
		}
		/*
		else
		if($actor == 'FACTURACION')
		{
			if(is_uploaded_file($_FILES['adjuntofac']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntofac']['tmp_name'];
				$archivo = basename($_FILES['adjuntofac']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				
				$prefijo = "CONSTRUCTOR - FACTURACION";
				$destino =  "../../adjuntos/constructor/".$clacaj."-".$prefijo."".$num.".".$extension;
                $filesize = $_FILES['adjuntofac']['size'];
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and caj_clave_int = '".$clacaj."' and tad_clave_int = '17' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "error4";
                }
                else
                if($filesize>$max)
                {
                    echo "error3";
                }
                else
                if(strtoupper($extension)=="DWG" || strtoupper($extension)=="RAR" || strtoupper($extension)=="ZIP")
                {
                    echo "error1";
                }
                else
                {
				    if(move_uploaded_file($sourcePath,$destino))
				{
					$sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,caj_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'".$clacaj."','".$fechaactual."','".$destino."','".$archivo."',17,'".$usuario."','".$fechaactual."')");
				}
                    else
                {
                    echo "error2";
                }
                }
			}
		}
		else
		if($actor == 'AVANCE')
		{
			if(is_uploaded_file($_FILES['adjuntoava']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntoava']['tmp_name'];
				$archivo = basename($_FILES['adjuntoava']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
			
				$prefijo = "CONSTRUCTOR - AVANCE";
				$destino =  "../../adjuntos/constructor/".$clacaj."-".$prefijo."".$num.".".$extension;
                $filesize = $_FILES['archivoadjunto']['size'];
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and caj_clave_int = '".$clacaj."' and tad_clave_int = '".$arc."' and ada_sw_eliminado = 0 and ada_clave_int!='".$claada."'");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "error4";
                }
                else
                if($filesize>$max)
                {
                    echo "error3";
                }
                else
                if(strtoupper($extension)=="DWG" || strtoupper($extension)=="RAR" || strtoupper($extension)=="ZIP")
                {
                    echo "error1";
                }
                else
                {
				    if(move_uploaded_file($sourcePath,$destino))
				{
					$destinofinal = $num."".$prefijo."".$num;
					$sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,caj_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'".$clacaj."','".$fechaactual."','".$destino."','".$archivo."',18,'".$usuario."','".$fechaactual."')");
				}
                    else
                {
                    echo "error2";
                }
                }
			}
		}*/
		else
		if($actor == 'ACTUALIZAARCHIVO')
		{
			if(is_uploaded_file($_FILES['adjuntoava1']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntoava1']['tmp_name'];
				$archivo = basename($_FILES['adjuntoava1']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				$con = mysqli_query($conectar,"select * from adjunto_actor where ada_clave_int = '".$claada."'");
				$dato = mysqli_fetch_array($con);
				$adj = $dato['ada_adjunto'];
				
				
				if($arc == 15)
				{
					$prefijo = "CONSTRUCTOR - INFORME COTIZACION";
					$destino =  "../../adjuntos/constructor/".$clacaj."-".$prefijo."".$claada.".".$extension;
				}
				else
				if($arc == 16)
				{
					$prefijo = "CONSTRUCTOR - INFORME FINAL";
					$destino =  "../../adjuntos/constructor/".$clacaj."-".$prefijo."".$claada.".".$extension;
				}
				else
				if($arc == 17)
				{
					$prefijo = "CONSTRUCTOR - INFORME FACTURACION";
					$destino =  "../../adjuntos/constructor/".$clacaj."-".$prefijo."".$claada.".".$extension;
				}
				else
				if($arc == 18)
				{
					$prefijo = "CONSTRUCTOR - INFORME AVANCE";
					$destino =  "../../adjuntos/constructor/".$clacaj."-".$prefijo."".$claada.".".$extension;
				}
				else if($arc==35)
				{
					$prefijo = "CONSTRUCTOR - INFORME LIQUIDACION";
					$destino =  "../../adjuntos/constructor/".$clacaj."-".$prefijo."".$claada.".".$extension;
				}
				else if($arc==36)
				{
					$prefijo = "CONSTRUCTOR - INFORME ACTAS";
					$destino =  "../../adjuntos/constructor/".$clacaj."-".$prefijo."".$claada.".".$extension;
				}
				else if($arc==37)
				{
					$prefijo = "CONSTRUCTOR - INFORME CERTIFICADO ESCOMBROS";
					$destino =  "../../adjuntos/constructor/".$clacaj."-".$prefijo."".$claada.".".$extension;
				}
                $filesize = $_FILES['adjuntoava1']['size'];
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and caj_clave_int = '".$clacaj."' and tad_clave_int = '".$arc."' and ada_sw_eliminado = 0 and ada_clave_int!='".$claada."'");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "error4";
                }
                else
                if($filesize>$max)
                {
                    echo "error3";
                }
                else
                if(strtoupper($extension)=="DWG" || strtoupper($extension)=="RAR" || strtoupper($extension)=="ZIP")
                {
                    echo "error1";
                }
                else
                {
                    if(move_uploaded_file($sourcePath,$destino))
                    {
                        $sql = mysqli_query($conectar,"update adjunto_actor set ada_usu_actualiz = '".$usuario."',ada_fec_actualiz = '".$fechaactual."', ada_adjunto = '".$destino."', ada_nombre_adjunto = '".$archivo."' where ada_clave_int = '".$claada."'");
                    }
                    else
                    {
                        echo "error2";
                    }
                }
			}
		}
	}
	/*
	$conhij = mysqli_query($conectar,"select * from cajero where caj_padre = ".$clacaj."");
	$numhij = mysqli_num_rows($conhij);
	for($i = 0; $i < $numhij; $i++)
	{
		$datohij = mysqli_fetch_array($conhij);
		$clahij = $datohij['caj_clave_int'];
		
		mysqli_query($conectar,"delete from adjunto_actor where caj_clave_int = ".$clahij."");
		mysqli_query($conectar,"insert into adjunto_actor select null,".$clahij.",ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_sw_eliminado,ada_usu_actualiz,ada_fec_actualiz from adjunto_actor where caj_clave_int = ".$clacaj."");
	}*/
}


?>