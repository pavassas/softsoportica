function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function MODULO(v,e)
{	
	if(v == 'INTERVENTORIA')
	{
		window.location.href = "interventoria.php";
	}
	else
	if(v == 'TODOS')
	{
        $("#cajeros").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />");
        $.post('fnCajeros.php',{opcion:v,est:e},function (data) {
            $('#cajeros').html(data);
        })
		/*
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('cajeros').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#cajeros").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?todos=si&est="+e,true);
		ajax.send(null);
		setTimeout("REFRESCARLISTA()",500);*/
	}
	OCULTARSCROLL();
}
function BUSCAR(v,p)
{	
	var consec = form1.busconsecutivo.value;
	var nom = form1.busnombre.value;
	var anocon = form1.busanocontable.value;
	var cencos = form1.buscentrocostos.value;
	var cod = form1.buscodigo.value;
	var reg = form1.busregion.value;
	var mun = form1.busmunicipio.value;
	var tip = form1.bustipologia.value;
	var tipint = form1.bustipointervencion.value;
	var moda = form1.busmodalidad.value;
	var actor = form1.busactor.value;
	var est = form1.ocultoestado.value;
	
	if(v == 'CAJERO')
	{
        $("#busqueda").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='50' width='50' />");
        $.post('fnCajeros.php',{opcion:"BUSCAR"},function (data){
            $("#busqueda").html(data);
        })
		/*
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('busqueda').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#busqueda").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		if(p > 0)
		{
			ajax.open("GET","?buscar=si&consec="+consec+"&nom="+nom+"&anocon="+anocon+"&cencos="+cencos+"&cod="+cod+"&reg="+reg+"&mun="+mun+"&tip="+tip+"&tipint="+tipint+"&moda="+moda+"&est="+est+"&actor="+actor+"&page="+p,true);
		}
		else
		{
			ajax.open("GET","?buscar=si&consec="+consec+"&nom="+nom+"&anocon="+anocon+"&cencos="+cencos+"&cod="+cod+"&reg="+reg+"&mun="+mun+"&tip="+tip+"&tipint="+tipint+"&moda="+moda+"&est="+est+"&actor="+actor,true);
		}
		ajax.send(null);*/
	}
	OCULTARSCROLL();
}
function VERREGISTRO(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('cajeros').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#cajeros").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?verregistro=si&clacaj="+v,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTA()",500);
	OCULTARSCROLL();
}
function AGREGARNOTA(c)
{
	/*var not = $('#nota').val();
	var nuevanota = not;// CORREGIRTEXTO(not);
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('agregados').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#agregados").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?agregarnota=si&not="+nuevanota+"&caj="+c,true);
	ajax.send(null);*/
	var not = $('#nota').val();	
	var nuevanota = not;// CORREGIRTEXTO(not);
	
	if(not=="" || not==null)
	{
		alert("Diligenciar la descripción de la nota");
	}
	else
	{
		$.post('fnCajeros.php', {opcion: 'AGREGARNOTA',not:nuevanota,caj:c}, 
			function(data, textStatus, xhr) {
				var res = data[0].res;
				var ultnota = data[0].nota;
				var msn = data[0].msn;
				if(res=="ok")
				{

				}
				else
				{
					alert(msn);
				}
				$('#agregados').html(ultnota);
			/*optional stuff to do after success */
		},"json");
	}
}
function CORREGIRTEXTO(v)
{
	var res = v.replace('#','REEMPLAZARNUMERAL');
	var res = res.replace('+','REEMPLAZARMAS');
	
	return res;
}
function EDITARNOTA(c,cn)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('editarnota').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#editarnota").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?editarnota=si&clanot="+cn+"&caj="+c,true);
	ajax.send(null);
}
function ACTUALIZARNOTA(n,c)
{
	var not = $('#notaedi').val();
	var nuevanota = not;// CORREGIRTEXTO(not);
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todaslasnotas').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todaslasnotas").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?actualizarnota=si&not="+nuevanota+"&clanot="+n+"&caj="+c,true);
	ajax.send(null);
}
function ELIMINARNOTA(c,caj)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todaslasnotas').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todaslasnotas").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?eliminarnota=si&clanot="+c+"&caj="+caj,true);
	ajax.send(null);
}
function MOSTRARNOTAS(c)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todaslasnotas').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todaslasnotas").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarnotas=si&clacaj="+c,true);
	ajax.send(null);
}
function AVISO()
{
	alert("Porfavor seleccione un cajero para agregar notas");
}
function GUARDAR(v)
{
	var fecteo = $('#fechateoricaentrega').val();
	var fecini = $('#fechainicioobra').val();
	var fecped = $('#fechapedidosuministros').val();
	var aprovcotiz = $('input[name=aprovarcotizacion]:checked', '#form1').val();
	var cancom = $('#canalcomunicacion').val();
	var lintel = $('#lineatelefonica').val();
	var aprovliqui = $('input[name=aprovarliquidacion]:checked', '#form1').val();
	var fecentcan = $('#fechaentregacanal').val();
	var swimg = $('input[name=imagenexos]:checked', '#form1').val();
	var swfot = $('input[name=fotos]:checked', '#form1').val();
	var swact = $('input[name=actas]:checked', '#form1').val();
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('estadoregistro').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#estadoregistro").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?guardardatos=si&fecini="+fecini+"&fecteo="+fecteo+"&fecped="+fecped+"&aprovcotiz="+aprovcotiz+"&cancom="+cancom+"&lintel="+lintel+"&aprovliqui="+aprovliqui+"&fecentcan="+fecentcan+"&swimg="+swimg+"&swfot="+swfot+"&swact="+swact+"&caj="+v,true);
	ajax.send(null);
}
function RESULTADOADJUNTO(v,cc)
{
	if(v == 'INFORME')
	{
		var div = 'resultadoadjuntoinf';
	}
	else
	if(v == 'FACTURACION')
	{
		var div = 'resultadoadjuntofac';
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById(div).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#"+div).html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?estadoadjunto=si&actor="+v+"&clacaj="+cc,true);
	ajax.send(null);
	OCULTARSCROLL();
}
function MOSTRARRUTA(v)
{
	if(v == 'INFORME')
	{
		var div = 'resultadoadjuntoinf';
		var nomadj = form1.adjuntoinf.value;
	}
	else
	if(v == 'FACTURACION')
	{
		var div = 'resultadoadjuntofac';
		var nomadj = form1.adjuntofac.value;
	}
	else
	if(v == 'ADJUNTO')
	{
		var div = 'resultadoadjunto';
		var nomadj = $('#archivoadjunto').val();
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById(div).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#"+div).html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarruta=si&nomadj="+nomadj,true);
	ajax.send(null);
	OCULTARSCROLL();
}
function MOSTRARBITACORA(c)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todaslasnotas').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todaslasnotas").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarbitacora=si&clacaj="+c,true);
	ajax.send(null);
}
function MOSTRARBITACORAADJUNTOS(c)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todoslosadjuntosbitacora').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todoslosadjuntosbitacora").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarbitacoraadjuntos=si&clacaj="+c,true);
	ajax.send(null);
}
function VISOR(c,v)
{
    var url = c.replace('../../','www.pavas.com.co/Cajeros/');
    var mdl =  "<div class='embed-responsive embed-responsive-16by9'><iframe class='embed-responsive-item' src='https://docs.google.com/viewer?url="+url+"&embedded=true' frameborder='0' allowfullscreen></iframe></div>";
    $('#' + v).html(mdl);

}
function MOSTRARADJUNTOS(c,a)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('adjuntoarchivos').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#adjuntoarchivos").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostraradjuntos=si&clacaj="+c+"&actor="+a,true);
	ajax.send(null);
}
function EDITARADJUNTO(c,a)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('editaradjunto').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#editaradjunto").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?editaradjunto=si&claada="+c+"&act="+a,true);
	ajax.send(null);
}
function RESULTADOADJUNTOINFORMACION(ca)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('resultadoadjunto').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#resultadoadjunto").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?estadoadjuntoinformacion=si&claada="+ca,true);
	ajax.send(null);
}
function ELIMINARADJUNTO(v)
{
	if(confirm('Esta seguro/a de Eliminar este archivo?'))
	{
		var dataString = 'id='+v;
		
		$.ajax({
	        type: "POST",
	        url: "deletearchivo.php",
	        data: dataString,
	        success: function() {
				//$('#delete-ok').empty();
				//$('#delete-ok').append('<div class="correcto">Se ha eliminado correctamente la entrada con id='+v+'.</div>').fadeIn("slow");
				$('#service'+v).fadeOut("slow");
				//$('#'+v).remove();
	        }
	    });
	}
}
function VERMODALIDADES1(v)
{
    var mod = $('#busmodalidad');
    var tii = $('#bustipointervencion').val();
    mod.find('option').remove().end().append('<option value="">Cargando...</option>').val('');
    $.post('fnCajeros.php',{opcion:'VERMODALIDADES',tii:tii},
        function(data)
        {
            mod.empty();
            mod.append('<option value="">-Modalidad-</option>');

            for (var i=0; i<data.length; i++)
            {
                mod.append('<option value="' + data[i].id + '">' + data[i].literal + '</option>');
            }
           // setTimeout("REFRESCARLISTAMODALIDADES1()",800);
            setTimeout(function() { BUSCAR('CAJERO','') },1000);

        },"json");
    /*
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('modalidades1').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#modalidades1").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?vermodalidades1=si&tii="+v,true);
	ajax.send(null);
	//setTimeout("REFRESCARLISTAMODALIDADES1()",800);*/
}
function INICIALIZARLISTAS(){
    $(".buslista").searchable();
}