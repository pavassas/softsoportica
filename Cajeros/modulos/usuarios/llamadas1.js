


    function validarpass(id) {

        $('#'+id).keyup(function() {
            // set password variable
            var pswd = $(this).val();

            //validate the length
            if ( pswd.length < 8 ) {
                $('#length').html('Debería tener <strong>8 carácteres</strong> como mínimo');
                $('#length').css('color',"red");
            } else {
                $('#length').html('');
            }

            //validate letter
            if ( pswd.match(/[A-z]/) ) {
                $('#letter').html('');
            } else {
                $('#letter').html('Al menos debería tener <strong>una letra</strong>');
                $('#letter').css('color',"red");

            }

            //validate capital letter
            if ( pswd.match(/[A-Z]/) ) {
                $('#capital').html('');
            } else {
                $('#capital').html('Al menos debería tener <strong>una letra en mayúsculas</strong>');
                $('#capital').css('color',"red");

            }

            //validate number
            if ( pswd.match(/\d/) ) {
                $('#number').html('');
            } else {
                $('#number').html('Al menos debería tener <strong>un número</strong>');
                $('#number').css('color',"red");

            }

            if(/\s/.test(pswd)){
                $('#space').html('No puede contener  <strong>espacios vacios</strong>');
                $('#space').css('color',"red");
            }
            else
            {
                $('#space').html('');
            }

        }).focus(function() {
            $('#pswd_info').show();
        }).blur(function() {
            $('#pswd_info').hide();
        });
    }
    function validar_texto(e)
    {
        tecla = (document.all) ? e.keyCode : e.which;
        //Tecla de retroceso para borrar, siempre la permite
        if (tecla==8)
        {
            return true;
        }
        // Patron de entrada, en este caso solo acepta numeros
        patron =/[0-9\.]/g, "0";
        tecla_final = String.fromCharCode(tecla);
        return patron.test(tecla_final);
    }
    function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }

    function ENCRIPTAR() {
        var ajax;
        ajax=new ajaxFunction();
        ajax.onreadystatechange=function()
        {
            if(ajax.readyState==4)
            {
                document.getElementById('modulos').innerHTML=ajax.responseText;
            }
        }
        jQuery("#divencryptar").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
        ajax.open("GET","?encryptar=si",true);
        ajax.send(null);
    }
function MODULO(v)
{	
	if(v == 'USUARIOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulousuarios=si",true);
		ajax.send(null);
	}
}
function EDITAR(v,m)
{	
	if(m == 'USUARIO')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('editarusuario').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#editarusuario").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?editarusu=si&usuedi="+v,true);
		ajax.send(null);
		window.location.href = "#editarusuario";
	}
	else
	if(m == 'PERFIL')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('nuevoperfil').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#nuevoperfil").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?editarper=si&peredi="+v,true);
		ajax.send(null);
	}
}
function VALIDAR(v)
{	
	if(v == 'CONTRASENA')
	{
		var con1 = form1.Password1.value;
		var con2 = form1.Password2.value;
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('datos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#datos").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?contrasena=si&con1="+con1+"&con2="+con2,true);
		ajax.send(null);
	}
}
function GUARDAR(v,id)
{	
	if(v == 'USUARIO')
	{
		var cat = form1.categoria1.value;
		var nom = form1.nombre1.value;
		var usu = form1.usuario1.value;
		var lu = usu.length;
		var con1 = form1.Password3.value;
		var lc = con1.length
		var con2 = form1.Password4.value;
		var per = form1.perfil1.value;
		var ema = form1.email1.value;
		var act = form1.activo1.checked;
		var inm = form1.inmobiliaria1.checked;
		var vis = form1.visita1.checked;
		var dis = form1.diseno1.checked;
		var lic = form1.licencia1.checked;
		var inte = form1.interventor1.checked;
		var cons = form1.constructor1.checked;
		var seg = form1.seguridad1.checked;
		var otr = form1.otro1.checked;	
		var her = form1.heredar1.checked;
		var tel = form1.telefono1.value;
        var code = form1.code1.value;

        var longitud = false,
            minuscula = false,
            numero = false,
            mayuscula = false, space = false;


            if (con1.length < 8) {
                //$('#length').removeClass('valid').addClass('invalid');
                longitud = false;
            } else {
                //$('#length').removeClass('invalid').addClass('valid');
                longitud = true;
            }

            //validate letter
            if (con1.match(/[A-z]/)) {
                //$('#letter').removeClass('invalid').addClass('valid');
                minuscula = true;
            } else {
               // $('#letter').removeClass('valid').addClass('invalid');
                minuscula = false;
            }

            //validate capital letter
            if (con1.match(/[A-Z]/)) {
               // $('#capital').removeClass('invalid').addClass('valid');
                mayuscula = true;
            } else {
				//$('#capital').removeClass('valid').addClass('invalid');
                mayuscula = false;
            }

            //validate number
            if (con1.match(/\d/)) {
               // $('#number').removeClass('invalid').addClass('valid');
                numero = true;
            } else {
                //$('#number').removeClass('valid').addClass('invalid');
                numero = false;
            }

			if(/\s/.test(con1)){
				space = false;
			}
			else
			{
				space = true
			}
        if((longitud==false || minuscula==false || numero==false || mayuscula==false || space==false) && cat!=2 )
        {
            alert("No cumple con los parametros de seguridad de una contraseña");
        }
        else {
            var ajax;
            ajax = new ajaxFunction();
            ajax.onreadystatechange = function () {
                if (ajax.readyState == 4) {
                    document.getElementById('datos').innerHTML = ajax.responseText;
                }
            }
            jQuery("#datos").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
            ajax.open("GET", "?guardarusu=si&nom=" + nom + "&cat=" + cat + "&usu=" + usu + "&con1=" + con1 + "&con2=" + con2 + "&per=" + per + "&ema=" + ema + "&act=" + act + "&lu=" + lu + "&lc=" + lc + "&u=" + id + "&inm=" + inm + "&vis=" + vis + "&dis=" + dis + "&lic=" + lic + "&int=" + inte + "&cons=" + cons + "&seg=" + seg + "&otr=" + otr + "&her=" + her + "&tel=" + tel + "&code=" + code, true);
            ajax.send(null);
            if (nom != '' && usu != '' && lu >= 6 && con1 == con2 && cat == 1) {
                setTimeout("CONSULTAMODULO('TODOS');", 1000);//setInterval("window.location.href='usuarios.php';",3000);
            }
            else if (nom != '' && cat == 2) {
                setTimeout("CONSULTAMODULO('TODOS');", 1000);//setInterval("window.location.href='usuarios.php';",3000);
            }
        }
	}
	else
	if(v == 'PERFIL')
	{
		var cod = form1.codigoper1.value;
		var des = form1.descripcionper1.value;
		var act = form1.actualizainfo.checked;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('usuarios').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#usuarios").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?guardarper=si&cod="+cod+"&des="+des+"&act="+act+"&p="+id,true);
		ajax.send(null);
	}
	OCULTARSCROLL();
}
function NUEVO(v)
{
	if(v == 'USUARIO')
	{
		var cat = form1.categoria.value;
		var nom = form1.nombre.value;
		var usu = form1.usuario.value;
		var lu = usu.length;
		var con1 = form1.Password1.value;
		var lc = con1.length
		var con2 = form1.Password2.value;
		var per = form1.perfil.value;
		var ema = form1.email.value;
		var act = form1.activo.checked;
		var inm = form1.inmobiliaria.checked;
		var vis = form1.visita.checked;
		var dis = form1.diseno.checked;
		var lic = form1.licencia.checked;
		var inte = form1.interventor.checked;
		var cons = form1.constructor.checked;
		var seg = form1.seguridad.checked;
		var otr = form1.otro.checked;
		var her = form1.heredar.checked;
		var tel = form1.telefono.value;
		var code = form1.code.value;
        var longitud = false,
            minuscula = false,
            numero = false,
            mayuscula = false, space = false;


        if (con1.length < 8) {
            //$('#length').removeClass('valid').addClass('invalid');
            longitud = false;
        } else {
            //$('#length').removeClass('invalid').addClass('valid');
            longitud = true;
        }

        //validate letter
        if (con1.match(/[A-z]/)) {
            //$('#letter').removeClass('invalid').addClass('valid');
            minuscula = true;
        } else {
            // $('#letter').removeClass('valid').addClass('invalid');
            minuscula = false;
        }

        //validate capital letter
        if (con1.match(/[A-Z]/)) {
            // $('#capital').removeClass('invalid').addClass('valid');
            mayuscula = true;
        } else {
            //$('#capital').removeClass('valid').addClass('invalid');
            mayuscula = false;
        }

        //validate number
        if (con1.match(/\d/)) {
            // $('#number').removeClass('invalid').addClass('valid');
            numero = true;
        } else {
            //$('#number').removeClass('valid').addClass('invalid');
            numero = false;
        }

        if(/\s/.test(con1)){
            space = false;
        }
        else
        {
            space = true
        }
        if((longitud==false || minuscula==false || numero==false || mayuscula==false || space==false) && cat!=2 )
        {
            alert("No cumple con los parametros de seguridad de una contraseña");
        }
        else {

            var ajax;
            ajax = new ajaxFunction();
            ajax.onreadystatechange = function () {
                if (ajax.readyState == 4) {
                    document.getElementById('datos1').innerHTML = ajax.responseText;
                }
            }
            jQuery("#datos1").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
            ajax.open("GET", "?nuevousuario=si&nom=" + nom + "&cat=" + cat + "&usu=" + usu + "&con1=" + con1 + "&con2=" + con2 + "&per=" + per + "&ema=" + ema + "&act=" + act + "&lu=" + lu + "&lc=" + lc + "&inm=" + inm + "&vis=" + vis + "&dis=" + dis + "&lic=" + lic + "&int=" + inte + "&cons=" + cons + "&seg=" + seg + "&otr=" + otr + "&her=" + her + "&tel=" + tel+ "&code="+ code, true);
            ajax.send(null);
            if (nom != '' && usu != '' && lu >= 6 && con1 == con2 && cat == 1) {
                form1.categoria.value = 1;
                form1.nombre.value = '';
                form1.usuario.value = '';
                form1.Password1.value = '';
                form1.Password2.value = '';
                //form1.perfil.value = '';
                form1.email.value = '';
                form1.telefono.value = '';
                form1.code.value = '';
                $("#inmobiliaria").attr('checked', false);
                $("#visita").attr('checked', false);
                $("#diseno").attr('checked', false);
                $("#licencia").attr('checked', false);
                $("#interventor").attr('checked', false);
                $("#constructor").attr('checked', false);
                $("#seguridad").attr('checked', false);
                $("#otro").attr('checked', false);
                $("#heredar").attr('checked', false);
                setTimeout("CONSULTAMODULO('TODOS');", 1000);
            }
            else if (nom != '' && cat == 2) {
                form1.categoria.value = 1;
                form1.nombre.value = '';
                form1.usuario.value = '';
                form1.Password1.value = '';
                form1.Password2.value = '';
                //form1.perfil.value = '';
                form1.email.value = '';
                form1.telefono.value = '';
                form1.code.
                $("#inmobiliaria").attr('checked', false);
                $("#visita").attr('checked', false);
                $("#diseno").attr('checked', false);
                $("#licencia").attr('checked', false);
                $("#interventor").attr('checked', false);
                $("#constructor").attr('checked', false);
                $("#seguridad").attr('checked', false);
                $("#otro").attr('checked', false);
                setTimeout("CONSULTAMODULO('TODOS');", 1000);//setInterval("window.location.href='usuarios.php';",3000);
            }
        }
	}
	else
	if(v == 'PERFIL')
	{
		var cod = form1.codigoper.value;
		var des = form1.descripcionper.value;
		var act = form1.actualizainfo.checked;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('usuarios').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#usuarios").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?perfil=si&cod="+cod+"&des="+des+"&act="+act+"&nuevo=si",true);
		ajax.send(null);
	}
	else
	if(v == 'PERFIL1')
	{
		var cod = form1.codigoper1.value;
		var des = form1.descripcionper1.value;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('usuarios').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#usuarios").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?perfil=si&cod="+cod+"&des="+des+"&nuevo=si",true);
		ajax.send(null);
	}
	OCULTARSCROLL();
}
function CONSULTAMODULO(v)
{
	if(v == 'TODOS')
	{
		var div = document.getElementById('nuevoperfil');
    	div.style.display = 'none';
    	
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('usuarios').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#usuarios").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?todos=si",true);
		ajax.send(null);
		CAMBIOFILTRO('USUARIO');
	}
	if(v == 'PERFIL')
	{
		var div = document.getElementById('nuevoperfil');
    	div.style.display = 'block';
    	
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('usuarios').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#usuarios").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?perfil=si&nuevo=no",true);
		ajax.send(null);
		CAMBIOFILTRO('PERFIL');
	}
}
function CAMBIOFILTRO(v)
{
	if(v == 'PERFIL')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('filtro').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#filtro").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?filtroperfil=si",true);
		ajax.send(null);
	}
	else
	if(v == 'USUARIO')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('filtro').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#filtro").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?filtrousuario=si",true);
		ajax.send(null);
	}
}
function BUSCAR(m)
{	
	if(m == 'USUARIO')
	{
		var gru = form1.grupo2.value;
		var nom = form1.nombre2.value;
		var ema = form1.email2.value;
		var usu = form1.usuario2.value;
		var cel = form1.nucelular.value;

				
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('usuarios').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#usuarios").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?buscarusu=si&nom="+nom+"&ema="+ema+"&usu="+usu+"&gru="+gru+"&cel="+cel,true);
		ajax.send(null);
	}
	else
	if(m == 'PERFIL')
	{
		var cod = form1.buscodigoper.value;
		var des = form1.busdescripcionper.value;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('usuarios').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#usuarios").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?buscarper=si&cod="+cod+"&des="+des,true);
		ajax.send(null);
	}
}
function BUSCAREQU(m,u)
{	
	if(m == 'EQUIPO')
	{
		var equ = form1.equipo2.value;
		var cod = form1.codigo2.value;
		var codalt = form1.codigoalterno2.value;
		var tip = form1.tipoequipo.value;
				
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('usuarios').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#usuarios").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?buscarequ=si&equ="+equ+"&cod="+cod+"&codalt="+codalt+"&tip="+tip+"&usu="+u,true);
		ajax.send(null);
	}
}
function PERMISOS(v)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('permisos').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#permisos").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?agregar=si&per="+v,true);
	ajax.send(null);
}
function AGREGARPERMISOS(v)
{
	var ven = $("#agregarven").val();
	
	if(ven != '')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('agregados').innerHTML=ajax.responseText;
		    }
		}
		
		jQuery("#agregados").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?agregarseleccionados=si&ven="+ven+"&per="+v,true);
		ajax.send(null);

		for (x=0;x<ven.length;x++)
		{
			$("#agregarven").find("option[value="+ven[x]+"]").remove();
		}
	}
	else
	{
		alert("Por favor seleccione almenos un registros.");
	}
}
function ELIMINARPERMISO(v)
{	
	var strChoices = "";
	var strChoices1 = "";
	var objCBarray = document.getElementsByName('metodoseleccionado');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	strChoices += objCBarray[i].value + ",";
	    	strChoices1 = objCBarray[i].value;
	        $('#service'+strChoices1).fadeOut("slow");//Esta linea me permite Eliminar las filas seleccionadas
	    }
	}
	
	if (strChoices.length <= 0) 
	{
		alert("Por favor seleccione almenos un registros.");
	}
	else
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('agregar').innerHTML=ajax.responseText;
		    }
		}

		//jQuery("#agregados").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?eliminaragregados=si&permiso="+strChoices+"&per="+v,true);
		ajax.send(null);
	}
}
function AGREGARTODOS(v)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('agregados').innerHTML=ajax.responseText;
	    }
	}
	
	jQuery("#agregados").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?agregartodos=si&per="+v,true);
	ajax.send(null);
	$('#agregarven').html('');//Limpia todos los datos del select
}
function QUITARTODOS(v)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('agregar').innerHTML=ajax.responseText;
	    }
	}
	
	jQuery("#agregar").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?eliminartodos=si&per="+v,true);
	ajax.send(null);
	$('.service_list').fadeOut("slow");//Esta linea me permite Eliminar las filas seleccionadas
}
function METODO(v)
{
	var met = $('#metodo'+v).val();
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     //document.getElementById('agregar').innerHTML=ajax.responseText;
	    }
	}
	//jQuery("#agregar").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?cambiarmetodo=si&per="+v+"&met="+met,true);
	ajax.send(null);
}
function AGREGAR(u)
{
	var usu = $("#agregarusu").val();

	if(usu != '')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('agregados1').innerHTML=ajax.responseText;
		    }
		}
		
		jQuery("#agregados1").html("<img alt='cargando' src='../../img/cargando.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?agregarusuario=si&usu="+usu+"&act="+u,true);
		ajax.send(null);
		
		for (x=0;x<usu.length;x++)
		{
			$("#agregarusu").find("option[value="+usu[x]+"]").remove();
		}
	}
	else
	{
		alert("Por favor seleccione almenos un registros.");
	}
}
function QUITAR(u)
{
	var usu = $("#quitarusu").val();
	
	if(usu != '')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('agregar1').innerHTML=ajax.responseText;
		    }
		}
		
		jQuery("#agregar1").html("<img alt='cargando' src='../../img/cargando.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?quitarusuario=si&usu="+usu+"&act="+u,true);
		ajax.send(null);
		
		for (x=0;x<usu.length;x++)
		{
			$("#quitarusu").find("option[value="+usu[x]+"]").remove();
		}
	}
	else
	{
		alert("Por favor seleccione almenos un registros.");
	}
}
function PONERTODOS(u)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('agregados1').innerHTML=ajax.responseText;
	    }
	}
	
	jQuery("#agregados1").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?ponertodos=si&act="+u,true);
	ajax.send(null);
	$('#agregarusu').html('');//Limpia todos los datos del select
}
function REMOVERTODOS(u)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('agregar1').innerHTML=ajax.responseText;
	    }
	}
	
	jQuery("#agregar1").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?quitartodos=si&act="+u,true);
	ajax.send(null);
	$('#quitarusu').html('');//Limpia todos los datos del select
}
function MOSTRARACTORES(u)
{
	var inm = form1.inmobiliaria.checked;
	var vis = form1.visita.checked;
	var dis = form1.diseno.checked;
	var lic = form1.licencia.checked;
	var inte = form1.interventor.checked;
	var cons = form1.constructor.checked;
	var seg = form1.seguridad.checked;
	var otr = form1.otro.checked;
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('agregar').innerHTML=ajax.responseText;
	    }
	}
	
	jQuery("#agregar").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostraractores=si&act="+u+"&inm="+inm+"&vis="+vis+"&dis="+dis+"&lic="+lic+"&inte="+inte+"&cons="+cons+"&seg="+seg+"&otr="+otr,true);
	ajax.send(null);
	setTimeout("MOSTRARACTORESAGREGADOS("+u+");",500);
}
function MOSTRARACTORES1(u)
{
	var inm = form1.inmobiliaria1.checked;
	var vis = form1.visita1.checked;
	var dis = form1.diseno1.checked;
	var lic = form1.licencia1.checked;
	var inte = form1.interventor1.checked;
	var cons = form1.constructor1.checked;
	var seg = form1.seguridad1.checked;
	var otr = form1.otro1.checked;
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('agregar').innerHTML=ajax.responseText;
	    }
	}
	
	jQuery("#agregar").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostraractores=si&act="+u+"&inm="+inm+"&vis="+vis+"&dis="+dis+"&lic="+lic+"&inte="+inte+"&cons="+cons+"&seg="+seg+"&otr="+otr,true);
	ajax.send(null);
	setTimeout("MOSTRARACTORESAGREGADOS("+u+");",500);
}
function MOSTRARACTORESAGREGADOS(u)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('agregados').innerHTML=ajax.responseText;
	    }
	}
	
	jQuery("#agregados").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostraractoresagregados=si&act="+u,true);
	ajax.send(null);
}
function USUARIOS(u,nom,ema,usu)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('usuario'+u).innerHTML=ajax.responseText;
	    }
	}
	
	jQuery("#usuario"+u).html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?usuarios=si&usu="+u+"&busnom="+nom+"&busema="+ema+"&bususu="+usu,true);
	ajax.send(null);
	OCULTARSCROLL();
}