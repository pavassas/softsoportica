function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function MODULO(v,e)
{	
	if(v == 'CAJEROS')
	{
		window.location.href = "cajeros.php";
	}
	else
	if(v == 'TODOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('cajeros').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#cajeros").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?todos=si&est="+e,true);
		ajax.send(null);
		setTimeout("REFRESCARLISTAS()",500);
	}
	OCULTARSCROLL();
}
function BUSCAR(v,p)
{
	//ENCABEZADO
    var cajeros = OBTENERSELECCIONMULTIPLE("multiselect_buscajero");//form1.buscajero.value;
	var fii = form1.fechainicio.value;
	var ffi = form1.fechafin.value;
	var fie = form1.fechainicioentrega.value;
	var ffe = form1.fechafinentrega.value;

	//INFO BASE
	var nomcaj = form1.busnombre.value;
	var dircaj = form1.busdireccion.value;
	var est = OBTENERSELECCIONMULTIPLE("multiselect_busestado");//form1.busestado.value;
	var anocon = form1.busanocontable.value;
	var reg = OBTENERSELECCIONMULTIPLE("multiselect_busregion");//form1.busregion.value;
	var dep = OBTENERSELECCIONMULTIPLE("multiselect_busdepartamento");//form1.busdepartamento.value;
	var mun = OBTENERSELECCIONMULTIPLE("multiselect_busmunicipio");//form1.busmunicipio.value;
	var tipro = OBTENERSELECCIONMULTIPLE("multiselect_bustipoproyecto");//form1.bustipoproyecto.value;

	//INMOBILIARIA
	var nominm = OBTENERSELECCIONMULTIPLE("multiselect_busnombreinmobiliaria");//form1.busnombreinmobiliaria.value;
	var estinm = OBTENERSELECCIONMULTIPLE("multiselect_busestadoinmobiliaria");//form1.busestadoinmobiliaria.value;
	var feciniinmdesde = form1.busfechainicioinmobiliaria.value;
	var feciniinmhasta = form1.busfechafininmobiliaria.value;
	var fecentinmdesde = form1.busfechaentregainiinfoinmobiliaria.value;
	var fecentinmhasta = form1.busfechaentregafininfoinmobiliaria.value;
	var diasinm = form1.bustotaldiasinmobiliaria.value;
	var notinm = form1.busnotasinmobiliaria.value;
	
	//VISITA LOCAL
	var nomvis = OBTENERSELECCIONMULTIPLE("multiselect_busnombrevisita");//form1.busnombrevisita.value;
	var estvis = OBTENERSELECCIONMULTIPLE("multiselect_busestadovisita");//form1.busestadovisita.value;
	var fecinivisdesde = form1.busfechainiciovisita.value;
	var fecinivishasta = form1.busfechafinvisita.value;
	var fecentvisdesde = form1.busfechaentregainiinfovisita.value;
	var fecentvishasta = form1.busfechaentregafininfovisita.value;
	var diasvis = form1.bustotaldiasvisita.value;
	var notvis = form1.busnotasvisita.value;
		
	//COMITE
	var cod = form1.buscodigo.value;
	var cencos =  OBTENERSELECCIONMULTIPLE("multiselect_buscentrocostos");// form1.buscentrocostos.value;
	var notcom = form1.busnotascomite.value;
	var ubi = OBTENERSELECCIONMULTIPLE("multiselect_busubicacion");//form1.busubicacion.value;
	var codsuc = form1.buscodigosuc.value;
	var ubiatm = OBTENERSELECCIONMULTIPLE("multiselect_busubicacionamt");//form1.busubicacionamt.value;
	var ati = OBTENERSELECCIONMULTIPLE("multiselect_busatiende");//form1.busatiende.value;
	var rie = OBTENERSELECCIONMULTIPLE("multiselect_busriesgo");//form1.busriesgo.value;
	var feciniapaatm = form1.busfechainiapagadoatm.value;
	var fecfinapaatm = form1.busfechafinapagadoatm.value;
	var comaprob = $('input[name=comiteaprobadobancolombia]:checked', '#form1').val();
	var are = OBTENERSELECCIONMULTIPLE("multiselect_busarea");//form1.busarea.value;
	var tipol = OBTENERSELECCIONMULTIPLE("multiselect_bustipologia");//form1.busarea.value;
	var tipinter = OBTENERSELECCIONMULTIPLE("multiselect_bustipointervencion");//form1.busarea.value;
	var modal = OBTENERSELECCIONMULTIPLE("multiselect_busmodalidad");//form1.busarea.value;
	var feciniteocomdesde = form1.fechainicioteoricacomitebancolombia.value;
	var fecfinteocomhasta = form1.fechafinalteoricacomitebancolombia.value;
	var fecentiniteocomdesde = form1.fechaentregainiteoricacomitebancolombia.value;
	var fecentfinteocomhasta = form1.fechaentregafinteoricacomitebancolombia.value;
	var fecinicomdesde = form1.fechainicomitebancolombia.value;
	var fecfincomhasta = form1.fechafincomitebancolombia.value;

	//CONTRATO
	var conaprob = $('input[name=contratoaprobadobancolombia]:checked', '#form1').val();
	var estcon = OBTENERSELECCIONMULTIPLE("multiselect_busestadocontratobancolombia");
	var feciniteocondesde = form1.fechainicioteoricacontratobancolombia.value;
	var fecfinteoconhasta = form1.fechainiciofinteoricacontratobancolombia.value;
	var fecentiniteocondesde = form1.fechaentregainiteoricacontratobancolombia.value;
	var fecentfinteoconhasta = form1.fechaentregafinteoricacontratobancolombia.value;
	var fecinicondesde = form1.fechaentregainicontratobancolombia.value;
	var fecfinconhasta = form1.fechaentregafincontratobancolombia.value;
	var notcon = form1.notacontrato.value;
	
	//DISENO
	var nomdis = OBTENERSELECCIONMULTIPLE("multiselect_busnombrediseno");//form1.busnombrediseno.value;
	var estdis = OBTENERSELECCIONMULTIPLE("multiselect_busestadodiseno");//form1.busestadodiseno.value;
	var fecinidisdesde = form1.busfechainiciodiseno.value;
	var fecinidishasta = form1.busfechafindiseno.value;
	var fecentdisdesde = form1.busfechaentregainiinfodiseno.value;
	var fecentdishasta = form1.busfechaentregafininfodiseno.value;
	var diasdis = form1.bustotaldiasdiseno.value;
	var notdis = form1.busnotasdiseno.value;
	
	//LICENCIA
	var nomges = OBTENERSELECCIONMULTIPLE("multiselect_busnombregestionador");//form1.busnombregestionador.value;
	var estges = OBTENERSELECCIONMULTIPLE("multiselect_busestadogestionador");//form1.busestadogestionador.value;
	var fecinigesdesde = form1.busfechainiciogestionador.value;
	var fecinigeshasta = form1.busfechafingestionador.value;
	var fecentgesdesde = form1.busfechaentregainiinfogestionador.value;
	var fecentgeshasta = form1.busfechaentregafininfogestionador.value;
	var diasges = form1.bustotaldiasgestionador.value;
	var notges = form1.busnotasgestionador.value;
	
	//PREFACTIBILIDAD
	var nomcancom = OBTENERSELECCIONMULTIPLE("multiselect_buscanalcomunicacion");
	var feciniteopredesde = form1.fechainicioteoricaprefactibilidadbancolombia.value;
	var fecfinteoprehasta = form1.fechafinteoricaprefactibilidadbancolombia.value;
	var fecentiniteopredesde = form1.fechaentregainiteoricaprefactibilidadbancolombia.value;
	var fecentfinteoprehasta = form1.fechaentregafinteoricaprefactibilidadbancolombia.value;
	var fecinipredesde = form1.fechaentregainiprefactibilidadbancolombia.value;
	var fecfinprehasta = form1.fechaentregafinprefactibilidadbancolombia.value;
	var preaprob = $('input[name=prefactibilidadaprobadobancolombia]:checked', '#form1').val();
	var notpre = form1.notaprefactibilidad.value;
	
	//PEDIDO MAQUINA
	var refmaq = OBTENERSELECCIONMULTIPLE("multiselect_referenciamaquina");
	var pedmaq = OBTENERSELECCIONMULTIPLE("multiselect_pedidomaquinabancolombia");
	var notped = form1.notapedidomaquina.value;
	
	//PROYECCI�N DE INSTALACION
	var feciniteoprodesde = form1.fechainicioteoricaproyeccionbancolombia.value;
	var fecfinteoprohasta = form1.fechafinteoricaproyeccionbancolombia.value;
	var fecentiniteoprodesde = form1.fechaentregainiteoricaproyeccion.value;
	var fecentfinteoprohasta = form1.fechaentregafinteoricaproyeccion.value;
	var notpro = form1.notaproyeccion.value;
	
	//CANAL
	var feciniteocandesde = form1.fechainicioteoricacanal.value;
	var fecfinteocanhasta = form1.fechafinteoricacanal.value;
	var fecentiniteocandesde = form1.fechaentregainiteoricacanal.value;
	var fecentfinteocanhasta = form1.fechaentregafinteoricacanal.value;
	var fecinicandesde = form1.fechainiciocanal.value;
	var fecfincanhasta = form1.fechafincanal.value;
	var fecentcandesde = form1.fechaentregainicanal.value;
	var fecentcanhasta = form1.fechaentregafincanal.value;
	var notcan = form1.notacanal.value;
	
	//INTERVENTORIA
	var nomint = OBTENERSELECCIONMULTIPLE("multiselect_busnombreinterventor");//form1.busnombreinterventor.value;
	var cancom = OBTENERSELECCIONMULTIPLE("multiselect_buscanalcomunicacion");//form1.buscanalcomunicacion.value;
	var feciniintdesde = form1.busfechainiciointerventor.value;
	var feciniinthasta = form1.busfechafininterventor.value;
	var fecentintdesde = form1.busfechaentregainiinfointerventor.value;
	var fecentinthasta = form1.busfechaentregafininfointerventor.value;
	var fecinipedintdesde = form1.busfechainipedidointerventor.value;
	var fecinipedinthasta = form1.busfechafinpedidointerventor.value;
	var notint = form1.busnotasinterventor.value;
	
	//CONSTRUCTOR
	var nomcons = OBTENERSELECCIONMULTIPLE("multiselect_busnombreconstructor");//form1.busnombreconstructor.value;
	var porava = OBTENERSELECCIONMULTIPLE("multiselect_busporcentajeavance");//form1.busporcentajeavance.value;
	var feciniconsdesde = form1.busfechainicioconstructor.value;
	var feciniconshasta = form1.busfechafinconstructor.value;
	var fecentconsdesde = form1.busfechaentregainiinfoconstructor.value;
	var fecentconshasta = form1.busfechaentregafininfoconstructor.value;
	var diascons = form1.bustotaldiasconstructor.value;
	var notcons = form1.busnotasconstructor.value;
	
	//SEGURIDAD
	var nomseg = OBTENERSELECCIONMULTIPLE("multiselect_busnombreseguridad");//form1.busnombreseguridad.value;
	var codmon = form1.buscodigomonitoreo.value;
	var fecinginiobra = form1.busfechaingresoiniobra.value;
	var fecingfinobra = form1.busfechaingresofinobra.value;
	var notseg = form1.busnotasseguridad.value;

	// ADJUNTO
	var c230 = OBTENERSELECCIONMULTIPLE("multiselect_c230");
	
	var swinfobasica = form1.ocuinfobasica.value;
	var swinmobiliaria = form1.ocuinmobiliaria.value;
	var swvisitalocal = form1.ocuvisitalocal.value;
	var swcomite = form1.ocucomite.value;
	var swcontrato = form1.ocucontrato.value;
	var swdiseno = form1.ocudiseno.value;
	var swlicencia = form1.oculicencia.value;
	var swprefactibilidad = form1.ocuprefactibilidad.value;
	var swpedidomaquina = form1.ocupedidomaquina.value;
	var swproyeccion = form1.ocuproyeccion.value;
	var swcanal= form1.ocucanal.value;
	var swinterventoria = form1.ocuinterventoria.value;
	var swconstructor = form1.ocuconstructor.value;
	var swseguridad = form1.ocuseguridad.value;
	var swadjunto = form1.ocuadjuntos.value;
	
	//VISTA INFO BASE
	var vernombrecajero = form1.vernombrecajero.checked;
	var verdireccion = form1.verdireccion.checked;
	var verestadocajero = form1.verestadocajero.checked;
	var veranocontable = form1.veranocontable.checked;
	var verregion = form1.verregion.checked;
	var verdepartamento = form1.verdepartamento.checked;
	var vermunicipio = form1.vermunicipio.checked;
	var vertipoproyecto = form1.vertipoproyecto.checked;
	var verfecini = form1.verfecini.checked;
	
	//INMOBILIARIA
	var verinmobiliaria = form1.verinmobiliaria1.checked;
	var verestadoinm = form1.verestadoinm.checked;
	var verfechainicioinm = form1.verfechainicioinm.checked;
	var verfechaentregainm = form1.verfechaentregainm.checked;
	var vertotaldiasinm = form1.vertotaldiasinm.checked;
	var vernotasinm = form1.vernotasinm.checked;
	
	//VISITA LOCAL
	var vervisita = form1.vervisita1.checked;
	var verestadovis = form1.verestadovis.checked;
	var verfechainiciovis = form1.verfechainiciovis.checked;
	var verfechaentregavis = form1.verfechaentregavis.checked;
	var vertotaldiasvis = form1.vertotaldiasvis.checked;
	var vernotasvis = form1.vernotasvis.checked;
	
	//COMITE
	var vercodigocajero = form1.vercodigocajero.checked;
	var vercentrocostos = form1.vercentrocostos.checked;
	var verubicacion = form1.verubicacion.checked;
	var vercodigosuc = form1.vercodigosuc.checked;
	var vernotascom = form1.vernotascom.checked;
	var veratiende = form1.veratiende.checked;
	var verriesgo = form1.verriesgo.checked;
	var verubicacionatm = form1.verubicacionatm.checked;
	var verfecapaatm = form1.verfechaapagado.checked;
	var veraprocom = form1.vercomiteaprobado.checked;
	var verarea = form1.verarea.checked;
	var vertipologia = form1.vertipologia.checked;
	var vertipointer = form1.verintervencion.checked;
	var vermoda = form1.vermodalidad.checked;
	var verfeciniteocom = form1.verfechainicioteoricacomite.checked;
	var verfecentteocom = form1.verfechaentregateoricacomite.checked;
	var verfeccom = form1.verfechacomite.checked;
	
	//CONTRATO
	var vercontratoaprobado = form1.vercontratoaprobado.checked;
	var verestadocontrato = form1.verestadocontrato.checked;
	var verfeciniteocontrato = form1.verfechainicioteoricacontrato.checked;
	var verfecentteocontrato = form1.verfechaentregateoricacontrato.checked;
	var verfecentcontrato = form1.verfechaentregacontrato.checked;
	var vernotascontrato = form1.vernotascontrato.checked;

	//DIENO
	var verdisenador = form1.verdisenador.checked;
	var verestadodis = form1.verestadodis.checked;
	var verfechainiciodis = form1.verfechainiciodis.checked;
	var verfechaentregadis = form1.verfechaentregadis.checked;
	var vertotaldiasdis = form1.vertotaldiasdis.checked;
	var vernotasdis = form1.vernotasdis.checked;
	
	//LICENCIA
	var vergestionador = form1.vergestionador.checked;
	var verestadolic = form1.verestadolic.checked;
	var verfechainiciolic = form1.verfechainiciolic.checked;
	var verfechaentregalic = form1.verfechaentregalic.checked;
	var vertotaldiaslic = form1.vertotaldiaslic.checked;
	var vernotaslic = form1.vernotaslic.checked;
	
	//PREFACTIBILIDAD CANAL
	var veroperadorcanal = form1.veroperadorcanal.checked;
	var verfeciniteoprefactibilidad = form1.verfechainiteoricaprefactibilidad.checked;
	var verfecentteoprefactibilidad = form1.verfechaentregateoricaprefactibilidad.checked;
	var verfecentprefactibilidad = form1.verfechaentregaprefactibilidad.checked;
	var veraprobadoprefactibilidad = form1.veraprobadoprefactibilidad.checked;
	var vernotasprefactibilidad = form1.vernotasprefactibilidad.checked;
	
	//PEDIDO MAQUINA
	var verrefmaquina = form1.verreferenciamaquina.checked;
	var vermaquina = form1.vermaquina.checked;
	var vernotaspedidomaquina = form1.vernotaspedidomaquina.checked;
	
	//PROYECCI�N INSTALACI�N
	var verfeciniteoproyeccion = form1.verfechainiteoricaproyeccion.checked;
	var verfecentteoproyeccion = form1.verfechaentregateoricaproyeccion.checked;
	var vernotasproyeccion = form1.vernotasproyeccion.checked;
	
	//CANAL
	var verfeciniteocanal = form1.verfechainicioteoricacanal.checked;
	var verfecentteocanal = form1.verfechaentregateoricacanal.checked;
	var verfecentcanal = form1.verfechaentregacanal.checked;
	var verfecinicanal = form1.verfechainiciocanal.checked;
	var vernotascanal = form1.vernotascanal.checked;
	
	//INTERVENTORIA
	var verinterventor = form1.verinterventor.checked;
	var veroperadorcanal = form1.veroperadorcanal.checked;
	var verfechainicioint = form1.verfechainicioint.checked;
	var verfechaentregaint = form1.verfechaentregaint.checked;
	var verfechapedido = form1.verfechapedido.checked;
	var vernotasint = form1.vernotasint.checked;
	
	//CONSTRUCTOR
	var verconstructor = form1.verconstructor1.checked;
	var veravance = form1.veravance.checked;
	var verfechainiciocons = form1.verfechainiciocons.checked;
	var verfechaentregacons = form1.verfechaentregacons.checked;
	var vertotaldiascons = form1.vertotaldiascons.checked;
	var vernotascons = form1.vernotascons.checked;
	var verinffincons = form1.verinformefinalcons.checked;
	
	//SEGURIDAD
	var verseguridad = form1.verseguridad1.checked;
	var vercodmon = form1.vercodigomonitoreo.checked;
	var verfecingobra = form1.verfechaingresoobra.checked;
	var vernotasseg = form1.vernotasseg.checked;
    
    //NOTAS GENERALES
	var vernotasgen = form1.vernotasgen.checked;

	var c229 = form1.c229.checked;
	if(v == 'CAJERO')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('busqueda').innerHTML=ajax.responseText;
		    }
		}
		if(comaprob != 1 && comaprob != 0){ comaprob = ''; }
		if(conaprob != 1 && conaprob != 0){ conaprob = ''; }
		if(preaprob != 1 && preaprob != 0){ preaprob = ''; }
		
		jQuery("#busqueda").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='50' width='50' />"); //loading gif will be overwrited when ajax have success
		if(p > 0)
		{
            ajax.open("GET","?buscar=si&c1="+cajeros+"&c2="+fii+"&c3="+ffi+"&c4="+fie+"&c5="+ffe+"&c6="+nomcaj+"&c7="+dircaj+"&c8="+est+"&c9="+anocon+"&c10="+reg+"&c11="+dep+"&c12="+mun+"&c13="+nominm+"&c14="+estinm+"&c15="+feciniinmdesde+"&c16="+feciniinmhasta+"&c17="+fecentinmdesde+"&c18="+fecentinmhasta+"&c19="+diasinm+"&c20="+notinm+"&c21="+nomvis+"&c22="+estvis+"&c23="+fecinivisdesde+"&c24="+fecinivishasta+"&c25="+fecentvisdesde+"&c26="+fecentvishasta+"&c27="+diasvis+"&c28="+notvis+"&c29="+cod+"&c30="+cencos+"&c31="+notcom+"&c32="+ubi+"&c33="+codsuc+"&c34="+ubiatm+"&c35="+ati+"&c36="+rie+"&c37="+feciniapaatm+"&c38="+fecfinapaatm+"&c39="+comaprob+"&c40="+are+"&c41="+tipol+"&c42="+tipinter+"&c43="+modal+"&c44="+feciniteocomdesde+"&c45="+fecfinteocomhasta+"&c46="+fecentiniteocomdesde+"&c47="+fecentfinteocomhasta+"&c48="+fecinicomdesde+"&c49="+fecfincomhasta+"&c50="+conaprob+"&c51="+estcon+"&c52="+feciniteocondesde+"&c53="+fecfinteoconhasta+"&c54="+fecentiniteocondesde+"&c55="+fecentfinteoconhasta+"&c56="+fecinicondesde+"&c57="+fecfinconhasta+"&c58="+nomdis+"&c59="+estdis+"&c60="+fecinidisdesde+"&c61="+fecinidishasta+"&c62="+fecentdisdesde+"&c63="+fecentdishasta+"&c64="+diasdis+"&c65="+notdis+"&c66="+nomges+"&c67="+estges+"&c68="+fecinigesdesde+"&c69="+fecinigeshasta+"&c70="+fecentgesdesde+"&c71="+fecentgeshasta+"&c72="+diasges+"&c73="+notges+"&c74="+nomcancom+"&c75="+feciniteopredesde+"&c76="+fecfinteoprehasta+"&c77="+fecentiniteopredesde+"&c78="+fecentfinteoprehasta+"&c79="+fecinipredesde+"&c80="+fecfinprehasta+"&c81="+preaprob+"&c82="+notpre+"&c83="+refmaq+"&c84="+pedmaq+"&c85="+notped+"&c86="+feciniteoprodesde+"&c87="+fecfinteoprohasta+"&c88="+fecentiniteoprodesde+"&c89="+fecentfinteoprohasta+"&c90="+notpro+"&c91="+feciniteocandesde+"&c92="+fecfinteocanhasta+"&c93="+fecentiniteocandesde+"&c94="+fecentfinteocanhasta+"&c95="+fecinicandesde+"&c96="+fecfincanhasta+"&c97="+fecentcandesde+"&c98="+fecentcanhasta+"&c99="+notcan+"&c100="+nomint+"&c101="+cancom+"&c102="+feciniintdesde+"&c103="+feciniinthasta+"&c104="+fecentintdesde+"&c105="+fecentinthasta+"&c106="+fecinipedintdesde+"&c107="+fecinipedinthasta+"&c108="+notint+"&c109="+nomcons+"&c110="+porava+"&c111="+feciniconsdesde+"&c112="+feciniconshasta+"&c113="+fecentconsdesde+"&c114="+fecentconshasta+"&c115="+diascons+"&c116="+notcons+"&c117="+nomseg+"&c118="+codmon+"&c119="+fecinginiobra+"&c120="+fecingfinobra+"&c121="+notseg+"&c122="+swinfobasica+"&c123="+swinmobiliaria+"&c124="+swvisitalocal+"&c125="+swcomite+"&c126="+swcontrato+"&c127="+swdiseno+"&c128="+swlicencia+"&c129="+swprefactibilidad+"&c130="+swpedidomaquina+"&c131="+swproyeccion+"&c132="+swcanal+"&c133="+swinterventoria+"&c134="+swconstructor+"&c135="+swseguridad+"&c136="+vernombrecajero+"&c137="+verdireccion+"&c138="+verestadocajero+"&c139="+veranocontable+"&c140="+verregion+"&c141="+verdepartamento+"&c142="+vermunicipio+"&c143="+verinmobiliaria+"&c144="+verestadoinm+"&c145="+verfechainicioinm+"&c146="+verfechaentregainm+"&c147="+vertotaldiasinm+"&c148="+vernotasinm+"&c149="+vervisita+"&c150="+verestadovis+"&c151="+verfechainiciovis+"&c152="+verfechaentregavis+"&c153="+vertotaldiasvis+"&c154="+vernotasvis+"&c155="+vercodigocajero+"&c156="+vercentrocostos+"&c157="+verubicacion+"&c158="+vercodigosuc+"&c159="+vernotascom+"&c160="+veratiende+"&c161="+verriesgo+"&c162="+verubicacionatm+"&c163="+verfecapaatm+"&c164="+veraprocom+"&c165="+verarea+"&c166="+vertipologia+"&c167="+vertipointer+"&c168="+vermoda+"&c169="+verfeciniteocom+"&c170="+verfecentteocom+"&c171="+verfeccom+"&c172="+vercontratoaprobado+"&c173="+verestadocontrato+"&c174="+verfeciniteocontrato+"&c175="+verfecentteocontrato+"&c176="+verfecentcontrato+"&c177="+vernotascontrato+"&c178="+verdisenador+"&c179="+verestadodis+"&c180="+verfechainiciodis+"&c181="+verfechaentregadis+"&c182="+vertotaldiasdis+"&c183="+vernotasdis+"&c184="+vergestionador+"&c185="+verestadolic+"&c186="+verfechainiciolic+"&c187="+verfechaentregalic+"&c188="+vertotaldiaslic+"&c189="+vernotaslic+"&c190="+veroperadorcanal+"&c191="+verfeciniteoprefactibilidad+"&c192="+verfecentteoprefactibilidad+"&c193="+verfecentprefactibilidad+"&c194="+veraprobadoprefactibilidad+"&c195="+vernotasprefactibilidad+"&c196="+verrefmaquina+"&c197="+vermaquina+"&c198="+vernotaspedidomaquina+"&c199="+verfeciniteoproyeccion+"&c200="+verfecentteoproyeccion+"&c201="+vernotasproyeccion+"&c202="+verfeciniteocanal+"&c203="+verfecentteocanal+"&c204="+verfecinicanal+"&c205="+verfecentcanal+"&c206="+vernotascanal+"&c207="+verinterventor+"&c208="+veroperadorcanal+"&c209="+verfechainicioint+"&c210="+verfechaentregaint+"&c211="+verfechapedido+"&c212="+vernotasint+"&c213="+verconstructor+"&c214="+veravance+"&c215="+verfechainiciocons+"&c216="+verfechaentregacons+"&c217="+vertotaldiascons+"&c218="+vernotascons+"&c219="+verinffincons+"&c220="+verseguridad+"&c221="+vercodmon+"&c222="+verfecingobra+"&c223="+vernotasseg+"&c224="+notcon+"&page="+p+"&c225="+vernotasgen+'&c226=' + vertipoproyecto + "&c227=" + tipro +"&c228=" + swadjunto + "&c229=" + c229 + "&c230=" + c230 + "&c231="+verfecini,true);
    	}
		else
		{
			ajax.open("GET","?buscar=si&c1="+cajeros+"&c2="+fii+"&c3="+ffi+"&c4="+fie+"&c5="+ffe+"&c6="+nomcaj+"&c7="+dircaj+"&c8="+est+"&c9="+anocon+"&c10="+reg+"&c11="+dep+"&c12="+mun+"&c13="+nominm+"&c14="+estinm+"&c15="+feciniinmdesde+"&c16="+feciniinmhasta+"&c17="+fecentinmdesde+"&c18="+fecentinmhasta+"&c19="+diasinm+"&c20="+notinm+"&c21="+nomvis+"&c22="+estvis+"&c23="+fecinivisdesde+"&c24="+fecinivishasta+"&c25="+fecentvisdesde+"&c26="+fecentvishasta+"&c27="+diasvis+"&c28="+notvis+"&c29="+cod+"&c30="+cencos+"&c31="+notcom+"&c32="+ubi+"&c33="+codsuc+"&c34="+ubiatm+"&c35="+ati+"&c36="+rie+"&c37="+feciniapaatm+"&c38="+fecfinapaatm+"&c39="+comaprob+"&c40="+are+"&c41="+tipol+"&c42="+tipinter+"&c43="+modal+"&c44="+feciniteocomdesde+"&c45="+fecfinteocomhasta+"&c46="+fecentiniteocomdesde+"&c47="+fecentfinteocomhasta+"&c48="+fecinicomdesde+"&c49="+fecfincomhasta+"&c50="+conaprob+"&c51="+estcon+"&c52="+feciniteocondesde+"&c53="+fecfinteoconhasta+"&c54="+fecentiniteocondesde+"&c55="+fecentfinteoconhasta+"&c56="+fecinicondesde+"&c57="+fecfinconhasta+"&c58="+nomdis+"&c59="+estdis+"&c60="+fecinidisdesde+"&c61="+fecinidishasta+"&c62="+fecentdisdesde+"&c63="+fecentdishasta+"&c64="+diasdis+"&c65="+notdis+"&c66="+nomges+"&c67="+estges+"&c68="+fecinigesdesde+"&c69="+fecinigeshasta+"&c70="+fecentgesdesde+"&c71="+fecentgeshasta+"&c72="+diasges+"&c73="+notges+"&c74="+nomcancom+"&c75="+feciniteopredesde+"&c76="+fecfinteoprehasta+"&c77="+fecentiniteopredesde+"&c78="+fecentfinteoprehasta+"&c79="+fecinipredesde+"&c80="+fecfinprehasta+"&c81="+preaprob+"&c82="+notpre+"&c83="+refmaq+"&c84="+pedmaq+"&c85="+notped+"&c86="+feciniteoprodesde+"&c87="+fecfinteoprohasta+"&c88="+fecentiniteoprodesde+"&c89="+fecentfinteoprohasta+"&c90="+notpro+"&c91="+feciniteocandesde+"&c92="+fecfinteocanhasta+"&c93="+fecentiniteocandesde+"&c94="+fecentfinteocanhasta+"&c95="+fecinicandesde+"&c96="+fecfincanhasta+"&c97="+fecentcandesde+"&c98="+fecentcanhasta+"&c99="+notcan+"&c100="+nomint+"&c101="+cancom+"&c102="+feciniintdesde+"&c103="+feciniinthasta+"&c104="+fecentintdesde+"&c105="+fecentinthasta+"&c106="+fecinipedintdesde+"&c107="+fecinipedinthasta+"&c108="+notint+"&c109="+nomcons+"&c110="+porava+"&c111="+feciniconsdesde+"&c112="+feciniconshasta+"&c113="+fecentconsdesde+"&c114="+fecentconshasta+"&c115="+diascons+"&c116="+notcons+"&c117="+nomseg+"&c118="+codmon+"&c119="+fecinginiobra+"&c120="+fecingfinobra+"&c121="+notseg+"&c122="+swinfobasica+"&c123="+swinmobiliaria+"&c124="+swvisitalocal+"&c125="+swcomite+"&c126="+swcontrato+"&c127="+swdiseno+"&c128="+swlicencia+"&c129="+swprefactibilidad+"&c130="+swpedidomaquina+"&c131="+swproyeccion+"&c132="+swcanal+"&c133="+swinterventoria+"&c134="+swconstructor+"&c135="+swseguridad+"&c136="+vernombrecajero+"&c137="+verdireccion+"&c138="+verestadocajero+"&c139="+veranocontable+"&c140="+verregion+"&c141="+verdepartamento+"&c142="+vermunicipio+"&c143="+verinmobiliaria+"&c144="+verestadoinm+"&c145="+verfechainicioinm+"&c146="+verfechaentregainm+"&c147="+vertotaldiasinm+"&c148="+vernotasinm+"&c149="+vervisita+"&c150="+verestadovis+"&c151="+verfechainiciovis+"&c152="+verfechaentregavis+"&c153="+vertotaldiasvis+"&c154="+vernotasvis+"&c155="+vercodigocajero+"&c156="+vercentrocostos+"&c157="+verubicacion+"&c158="+vercodigosuc+"&c159="+vernotascom+"&c160="+veratiende+"&c161="+verriesgo+"&c162="+verubicacionatm+"&c163="+verfecapaatm+"&c164="+veraprocom+"&c165="+verarea+"&c166="+vertipologia+"&c167="+vertipointer+"&c168="+vermoda+"&c169="+verfeciniteocom+"&c170="+verfecentteocom+"&c171="+verfeccom+"&c172="+vercontratoaprobado+"&c173="+verestadocontrato+"&c174="+verfeciniteocontrato+"&c175="+verfecentteocontrato+"&c176="+verfecentcontrato+"&c177="+vernotascontrato+"&c178="+verdisenador+"&c179="+verestadodis+"&c180="+verfechainiciodis+"&c181="+verfechaentregadis+"&c182="+vertotaldiasdis+"&c183="+vernotasdis+"&c184="+vergestionador+"&c185="+verestadolic+"&c186="+verfechainiciolic+"&c187="+verfechaentregalic+"&c188="+vertotaldiaslic+"&c189="+vernotaslic+"&c190="+veroperadorcanal+"&c191="+verfeciniteoprefactibilidad+"&c192="+verfecentteoprefactibilidad+"&c193="+verfecentprefactibilidad+"&c194="+veraprobadoprefactibilidad+"&c195="+vernotasprefactibilidad+"&c196="+verrefmaquina+"&c197="+vermaquina+"&c198="+vernotaspedidomaquina+"&c199="+verfeciniteoproyeccion+"&c200="+verfecentteoproyeccion+"&c201="+vernotasproyeccion+"&c202="+verfeciniteocanal+"&c203="+verfecentteocanal+"&c204="+verfecinicanal+"&c205="+verfecentcanal+"&c206="+vernotascanal+"&c207="+verinterventor+"&c208="+veroperadorcanal+"&c209="+verfechainicioint+"&c210="+verfechaentregaint+"&c211="+verfechapedido+"&c212="+vernotasint+"&c213="+verconstructor+"&c214="+veravance+"&c215="+verfechainiciocons+"&c216="+verfechaentregacons+"&c217="+vertotaldiascons+"&c218="+vernotascons+"&c219="+verinffincons+"&c220="+verseguridad+"&c221="+vercodmon+"&c222="+verfecingobra+"&c223="+vernotasseg+"&c224="+notcon+"&c225="+vernotasgen+'&c226=' + vertipoproyecto + "&c227=" + tipro +"&c228=" +swadjunto + "&c229=" + c229 + "&c230=" + c230 + "&c231=" + verfecini,true);
		}
		ajax.send(null);
	}
	OCULTARSCROLL();
}
function GUARDAR(o,l,cb)
{	
	var ley = prompt("Ingrese el nombre de la busqueda", l);
	if(ley != '' && ley != null)
	{
        //ENCABEZADO
	    var cajeros = OBTENERSELECCIONMULTIPLE("multiselect_buscajero");//form1.buscajero.value;
		var fii = form1.fechainicio.value;
		var ffi = form1.fechafin.value;
		var fie = form1.fechainicioentrega.value;
		var ffe = form1.fechafinentrega.value;
	
		//INFO BASE
		var nomcaj = form1.busnombre.value;
		var dircaj = form1.busdireccion.value;
		var est = OBTENERSELECCIONMULTIPLE("multiselect_busestado");//form1.busestado.value;
		var anocon = form1.busanocontable.value;
		var reg = OBTENERSELECCIONMULTIPLE("multiselect_busregion");//form1.busregion.value;
		var dep = OBTENERSELECCIONMULTIPLE("multiselect_busdepartamento");//form1.busdepartamento.value;
		var mun = OBTENERSELECCIONMULTIPLE("multiselect_busmunicipio");//form1.busmunicipio.value;
		var tipro = OBTENERSELECCIONMULTIPLE("multiselect_bustipoproyecto");
	
		//INMOBILIARIA
		var nominm = OBTENERSELECCIONMULTIPLE("multiselect_busnombreinmobiliaria");//form1.busnombreinmobiliaria.value;
		var estinm = OBTENERSELECCIONMULTIPLE("multiselect_busestadoinmobiliaria");//form1.busestadoinmobiliaria.value;
		var feciniinmdesde = form1.busfechainicioinmobiliaria.value;
		var feciniinmhasta = form1.busfechafininmobiliaria.value;
		var fecentinmdesde = form1.busfechaentregainiinfoinmobiliaria.value;
		var fecentinmhasta = form1.busfechaentregafininfoinmobiliaria.value;
		var diasinm = form1.bustotaldiasinmobiliaria.value;
		var notinm = form1.busnotasinmobiliaria.value;
		
		//VISITA LOCAL
		var nomvis = OBTENERSELECCIONMULTIPLE("multiselect_busnombrevisita");//form1.busnombrevisita.value;
		var estvis = OBTENERSELECCIONMULTIPLE("multiselect_busestadovisita");//form1.busestadovisita.value;
		var fecinivisdesde = form1.busfechainiciovisita.value;
		var fecinivishasta = form1.busfechafinvisita.value;
		var fecentvisdesde = form1.busfechaentregainiinfovisita.value;
		var fecentvishasta = form1.busfechaentregafininfovisita.value;
		var diasvis = form1.bustotaldiasvisita.value;
		var notvis = form1.busnotasvisita.value;
			
		//COMITE
		var cod = form1.buscodigo.value;
		var cencos =  OBTENERSELECCIONMULTIPLE("multiselect_buscentrocostos");// form1.buscentrocostos.value;
		var notcom = form1.busnotascomite.value;
		var ubi = OBTENERSELECCIONMULTIPLE("multiselect_busubicacion");//form1.busubicacion.value;
		var codsuc = form1.buscodigosuc.value;
		var ubiatm = OBTENERSELECCIONMULTIPLE("multiselect_busubicacionamt");//form1.busubicacionamt.value;
		var ati = OBTENERSELECCIONMULTIPLE("multiselect_busatiende");//form1.busatiende.value;
		var rie = OBTENERSELECCIONMULTIPLE("multiselect_busriesgo");//form1.busriesgo.value;
		var feciniapaatm = form1.busfechainiapagadoatm.value;
		var fecfinapaatm = form1.busfechafinapagadoatm.value;
		var comaprob = $('input[name=comiteaprobadobancolombia]:checked', '#form1').val();
		var are = OBTENERSELECCIONMULTIPLE("multiselect_busarea");//form1.busarea.value;
		var tipol = OBTENERSELECCIONMULTIPLE("multiselect_bustipologia");//form1.busarea.value;
		var tipinter = OBTENERSELECCIONMULTIPLE("multiselect_bustipointervencion");//form1.busarea.value;
		var modal = OBTENERSELECCIONMULTIPLE("multiselect_busmodalidad");//form1.busarea.value;
		var feciniteocomdesde = form1.fechainicioteoricacomitebancolombia.value;
		var fecfinteocomhasta = form1.fechafinalteoricacomitebancolombia.value;
		var fecentiniteocomdesde = form1.fechaentregainiteoricacomitebancolombia.value;
		var fecentfinteocomhasta = form1.fechaentregafinteoricacomitebancolombia.value;
		var fecinicomdesde = form1.fechainicomitebancolombia.value;
		var fecfincomhasta = form1.fechafincomitebancolombia.value;
	
		//CONTRATO
		var conaprob = $('input[name=contratoaprobadobancolombia]:checked', '#form1').val();
		var estcon = OBTENERSELECCIONMULTIPLE("multiselect_busestadocontratobancolombia");
		var feciniteocondesde = form1.fechainicioteoricacontratobancolombia.value;
		var fecfinteoconhasta = form1.fechainiciofinteoricacontratobancolombia.value;
		var fecentiniteocondesde = form1.fechaentregainiteoricacontratobancolombia.value;
		var fecentfinteoconhasta = form1.fechaentregafinteoricacontratobancolombia.value;
		var fecinicondesde = form1.fechaentregainicontratobancolombia.value;
		var fecfinconhasta = form1.fechaentregafincontratobancolombia.value;
		var notcon = form1.notacontrato.value;
		
		//DISENO
		var nomdis = OBTENERSELECCIONMULTIPLE("multiselect_busnombrediseno");//form1.busnombrediseno.value;
		var estdis = OBTENERSELECCIONMULTIPLE("multiselect_busestadodiseno");//form1.busestadodiseno.value;
		var fecinidisdesde = form1.busfechainiciodiseno.value;
		var fecinidishasta = form1.busfechafindiseno.value;
		var fecentdisdesde = form1.busfechaentregainiinfodiseno.value;
		var fecentdishasta = form1.busfechaentregafininfodiseno.value;
		var diasdis = form1.bustotaldiasdiseno.value;
		var notdis = form1.busnotasdiseno.value;
		
		//LICENCIA
		var nomges = OBTENERSELECCIONMULTIPLE("multiselect_busnombregestionador");//form1.busnombregestionador.value;
		var estges = OBTENERSELECCIONMULTIPLE("multiselect_busestadogestionador");//form1.busestadogestionador.value;
		var fecinigesdesde = form1.busfechainiciogestionador.value;
		var fecinigeshasta = form1.busfechafingestionador.value;
		var fecentgesdesde = form1.busfechaentregainiinfogestionador.value;
		var fecentgeshasta = form1.busfechaentregafininfogestionador.value;
		var diasges = form1.bustotaldiasgestionador.value;
		var notges = form1.busnotasgestionador.value;
		
		//PREFACTIBILIDAD
		var nomcancom = OBTENERSELECCIONMULTIPLE("multiselect_buscanalcomunicacion");
		var feciniteopredesde = form1.fechainicioteoricaprefactibilidadbancolombia.value;
		var fecfinteoprehasta = form1.fechafinteoricaprefactibilidadbancolombia.value;
		var fecentiniteopredesde = form1.fechaentregainiteoricaprefactibilidadbancolombia.value;
		var fecentfinteoprehasta = form1.fechaentregafinteoricaprefactibilidadbancolombia.value;
		var fecinipredesde = form1.fechaentregainiprefactibilidadbancolombia.value;
		var fecfinprehasta = form1.fechaentregafinprefactibilidadbancolombia.value;
		var preaprob = $('input[name=prefactibilidadaprobadobancolombia]:checked', '#form1').val();
		var notpre = form1.notaprefactibilidad.value;
		
		//PEDIDO MAQUINA
		var refmaq = OBTENERSELECCIONMULTIPLE("multiselect_referenciamaquina");
		var pedmaq = OBTENERSELECCIONMULTIPLE("multiselect_pedidomaquinabancolombia");
		var notped = form1.notapedidomaquina.value;
		
		//PROYECCI�N DE INSTALACION
		var feciniteoprodesde = form1.fechainicioteoricaproyeccionbancolombia.value;
		var fecfinteoprohasta = form1.fechafinteoricaproyeccionbancolombia.value;
		var fecentiniteoprodesde = form1.fechaentregainiteoricaproyeccion.value;
		var fecentfinteoprohasta = form1.fechaentregafinteoricaproyeccion.value;
		var notpro = form1.notaproyeccion.value;
		
		//CANAL
		var feciniteocandesde = form1.fechainicioteoricacanal.value;
		var fecfinteocanhasta = form1.fechafinteoricacanal.value;
		var fecentiniteocandesde = form1.fechaentregainiteoricacanal.value;
		var fecentfinteocanhasta = form1.fechaentregafinteoricacanal.value;
		var fecinicandesde = form1.fechainiciocanal.value;
		var fecfincanhasta = form1.fechafincanal.value;
		var fecentcandesde = form1.fechaentregainicanal.value;
		var fecentcanhasta = form1.fechaentregafincanal.value;
		var notcan = form1.notacanal.value;
		
		//INTERVENTORIA
		var nomint = OBTENERSELECCIONMULTIPLE("multiselect_busnombreinterventor");//form1.busnombreinterventor.value;
		var cancom = OBTENERSELECCIONMULTIPLE("multiselect_buscanalcomunicacion");//form1.buscanalcomunicacion.value;
		var feciniintdesde = form1.busfechainiciointerventor.value;
		var feciniinthasta = form1.busfechafininterventor.value;
		var fecentintdesde = form1.busfechaentregainiinfointerventor.value;
		var fecentinthasta = form1.busfechaentregafininfointerventor.value;
		var fecinipedintdesde = form1.busfechainipedidointerventor.value;
		var fecinipedinthasta = form1.busfechafinpedidointerventor.value;
		var notint = form1.busnotasinterventor.value;
		
		//CONSTRUCTOR
		var nomcons = OBTENERSELECCIONMULTIPLE("multiselect_busnombreconstructor");//form1.busnombreconstructor.value;
		var porava = OBTENERSELECCIONMULTIPLE("multiselect_busporcentajeavance");//form1.busporcentajeavance.value;
		var feciniconsdesde = form1.busfechainicioconstructor.value;
		var feciniconshasta = form1.busfechafinconstructor.value;
		var fecentconsdesde = form1.busfechaentregainiinfoconstructor.value;
		var fecentconshasta = form1.busfechaentregafininfoconstructor.value;
		var diascons = form1.bustotaldiasconstructor.value;
		var notcons = form1.busnotasconstructor.value;
		
		//SEGURIDAD
		var nomseg = OBTENERSELECCIONMULTIPLE("multiselect_busnombreseguridad");//form1.busnombreseguridad.value;
		var codmon = form1.buscodigomonitoreo.value;
		var fecinginiobra = form1.busfechaingresoiniobra.value;
		var fecingfinobra = form1.busfechaingresofinobra.value;
		var notseg = form1.busnotasseguridad.value;

		//aDJUNTO
        var c230 = OBTENERSELECCIONMULTIPLE("multiselect_c230");
		
		var swinfobasica = form1.ocuinfobasica.value;
		var swinmobiliaria = form1.ocuinmobiliaria.value;
		var swvisitalocal = form1.ocuvisitalocal.value;
		var swcomite = form1.ocucomite.value;
		var swcontrato = form1.ocucontrato.value;
		var swdiseno = form1.ocudiseno.value;
		var swlicencia = form1.oculicencia.value;
		var swprefactibilidad = form1.ocuprefactibilidad.value;
		var swpedidomaquina = form1.ocupedidomaquina.value;
		var swproyeccion = form1.ocuproyeccion.value;
		var swcanal= form1.ocucanal.value;
		var swinterventoria = form1.ocuinterventoria.value;
		var swconstructor = form1.ocuconstructor.value;
		var swseguridad = form1.ocuseguridad.value;
		var swadjunto = form1.ocuadjuntos.value;
		
		//VISTA INFO BASE
		var vernombrecajero = form1.vernombrecajero.checked;
		var verdireccion = form1.verdireccion.checked;
		var verestadocajero = form1.verestadocajero.checked;
		var veranocontable = form1.veranocontable.checked;
		var verregion = form1.verregion.checked;
		var verdepartamento = form1.verdepartamento.checked;
		var vermunicipio = form1.vermunicipio.checked;
		var vertipoproyecto = form1.vertipoproyecto.checked;
		var verfecini = form1.verfecini.checked;
		//INMOBILIARIA
		var verinmobiliaria = form1.verinmobiliaria1.checked;
		var verestadoinm = form1.verestadoinm.checked;
		var verfechainicioinm = form1.verfechainicioinm.checked;
		var verfechaentregainm = form1.verfechaentregainm.checked;
		var vertotaldiasinm = form1.vertotaldiasinm.checked;
		var vernotasinm = form1.vernotasinm.checked;
		
		//VISITA LOCAL
		var vervisita = form1.vervisita1.checked;
		var verestadovis = form1.verestadovis.checked;
		var verfechainiciovis = form1.verfechainiciovis.checked;
		var verfechaentregavis = form1.verfechaentregavis.checked;
		var vertotaldiasvis = form1.vertotaldiasvis.checked;
		var vernotasvis = form1.vernotasvis.checked;
		
		//COMITE
		var vercodigocajero = form1.vercodigocajero.checked;
		var vercentrocostos = form1.vercentrocostos.checked;
		var verubicacion = form1.verubicacion.checked;
		var vercodigosuc = form1.vercodigosuc.checked;
		var vernotascom = form1.vernotascom.checked;
		var veratiende = form1.veratiende.checked;
		var verriesgo = form1.verriesgo.checked;
		var verubicacionatm = form1.verubicacionatm.checked;
		var verfecapaatm = form1.verfechaapagado.checked;
		var veraprocom = form1.vercomiteaprobado.checked;
		var verarea = form1.verarea.checked;
		var vertipologia = form1.vertipologia.checked;
		var vertipointer = form1.verintervencion.checked;
		var vermoda = form1.vermodalidad.checked;
		var verfeciniteocom = form1.verfechainicioteoricacomite.checked;
		var verfecentteocom = form1.verfechaentregateoricacomite.checked;
		var verfeccom = form1.verfechacomite.checked;
		
		//CONTRATO
		var vercontratoaprobado = form1.vercontratoaprobado.checked;
		var verestadocontrato = form1.verestadocontrato.checked;
		var verfeciniteocontrato = form1.verfechainicioteoricacontrato.checked;
		var verfecentteocontrato = form1.verfechaentregateoricacontrato.checked;
		var verfecentcontrato = form1.verfechaentregacontrato.checked;
		var vernotascontrato = form1.vernotascontrato.checked;
	
		//DIENO
		var verdisenador = form1.verdisenador.checked;
		var verestadodis = form1.verestadodis.checked;
		var verfechainiciodis = form1.verfechainiciodis.checked;
		var verfechaentregadis = form1.verfechaentregadis.checked;
		var vertotaldiasdis = form1.vertotaldiasdis.checked;
		var vernotasdis = form1.vernotasdis.checked;
		
		//LICENCIA
		var vergestionador = form1.vergestionador.checked;
		var verestadolic = form1.verestadolic.checked;
		var verfechainiciolic = form1.verfechainiciolic.checked;
		var verfechaentregalic = form1.verfechaentregalic.checked;
		var vertotaldiaslic = form1.vertotaldiaslic.checked;
		var vernotaslic = form1.vernotaslic.checked;
		
		//PREFACTIBILIDAD CANAL
		var veroperadorcanal = form1.veroperadorcanal.checked;
		var verfeciniteoprefactibilidad = form1.verfechainiteoricaprefactibilidad.checked;
		var verfecentteoprefactibilidad = form1.verfechaentregateoricaprefactibilidad.checked;
		var verfecentprefactibilidad = form1.verfechaentregaprefactibilidad.checked;
		var veraprobadoprefactibilidad = form1.veraprobadoprefactibilidad.checked;
		var vernotasprefactibilidad = form1.vernotasprefactibilidad.checked;
		
		//PEDIDO MAQUINA
		var verrefmaquina = form1.verreferenciamaquina.checked;
		var vermaquina = form1.vermaquina.checked;
		var vernotaspedidomaquina = form1.vernotaspedidomaquina.checked;
		
		//PROYECCI�N INSTALACI�N
		var verfeciniteoproyeccion = form1.verfechainiteoricaproyeccion.checked;
		var verfecentteoproyeccion = form1.verfechaentregateoricaproyeccion.checked;
		var vernotasproyeccion = form1.vernotasproyeccion.checked;
		
		//CANAL
		var verfeciniteocanal = form1.verfechainicioteoricacanal.checked;
		var verfecentteocanal = form1.verfechaentregateoricacanal.checked;
		var verfecentcanal = form1.verfechaentregacanal.checked;
		var verfecinicanal = form1.verfechainiciocanal.checked;
		var vernotascanal = form1.vernotascanal.checked;
		
		//INTERVENTORIA
		var verinterventor = form1.verinterventor.checked;
		var veroperadorcanal = form1.veroperadorcanal.checked;
		var verfechainicioint = form1.verfechainicioint.checked;
		var verfechaentregaint = form1.verfechaentregaint.checked;
		var verfechapedido = form1.verfechapedido.checked;
		var vernotasint = form1.vernotasint.checked;
		
		//CONSTRUCTOR
		var verconstructor = form1.verconstructor1.checked;
		var veravance = form1.veravance.checked;
		var verfechainiciocons = form1.verfechainiciocons.checked;
		var verfechaentregacons = form1.verfechaentregacons.checked;
		var vertotaldiascons = form1.vertotaldiascons.checked;
		var vernotascons = form1.vernotascons.checked;
		var verinffincons = form1.verinformefinalcons.checked;
		
		//SEGURIDAD
		var verseguridad = form1.verseguridad1.checked;
		var vercodmon = form1.vercodigomonitoreo.checked;
		var verfecingobra = form1.verfechaingresoobra.checked;
		var vernotasseg = form1.vernotasseg.checked;
		
		//VERNOTASGENERALES
		var vernotasgen = form1.vernotasgen.checked;

		//ADJUNTO
		var c229 = form1.c229.checked;

		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('resultadoalguardar').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#resultadoalguardar").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		//ajax.open("GET","?guardar=si&caj="+cajeros+"&fii="+fii+"&ffi="+ffi+"&fie="+fie+"&ffe="+ffe+"&nomcaj="+nomcaj+"&dircaj="+dircaj+"&est="+est+"&anocon="+anocon+"&reg="+reg+"&dep="+dep+"&mun="+mun+"&tip="+tip+"&tipint="+tipint+"&moda="+moda+"&cod="+cod+"&cencos="+cencos+"&refmaq="+refmaq+"&ubi="+ubi+"&codsuc="+codsuc+"&ubiatm="+ubiatm+"&ati="+ati+"&rie="+rie+"&are="+are+"&fecapagadoatm="+fecapagadoatm+"&nominm="+nominm+"&estinm="+estinm+"&feciniinmdesde="+feciniinmdesde+"&feciniinmhasta="+feciniinmhasta+"&fecentinmdesde="+fecentinmdesde+"&fecentinmhasta="+fecentinmhasta+"&diasinm="+diasinm+"&notinm="+notinm+"&nomvis="+nomvis+"&estvis="+estvis+"&fecinivisdesde="+fecinivisdesde+"&fecinivishasta="+fecinivishasta+"&fecentvisdesde="+fecentvisdesde+"&fecentvishasta="+fecentvishasta+"&diasvis="+diasvis+"&notvis="+notvis+"&nomdis="+nomdis+"&estdis="+estdis+"&fecinidisdesde="+fecinidisdesde+"&fecinidishasta="+fecinidishasta+"&fecentdisdesde="+fecentdisdesde+"&fecentdishasta="+fecentdishasta+"&diasdis="+diasdis+"&notdis="+notdis+"&nomges="+nomges+"&estges="+estges+"&fecinigesdesde="+fecinigesdesde+"&fecinigeshasta="+fecinigeshasta+"&fecentgesdesde="+fecentgesdesde+"&fecentgeshasta="+fecentgeshasta+"&diasges="+diasges+"&notges="+notges+"&nomint="+nomint+"&cancom="+cancom+"&feciniintdesde="+feciniintdesde+"&feciniinthasta="+feciniinthasta+"&fecentintdesde="+fecentintdesde+"&fecentinthasta="+fecentinthasta+"&fecinipedintdesde="+fecinipedintdesde+"&fecinipedinthasta="+fecinipedinthasta+"&notint="+notint+"&nomcons="+nomcons+"&porava="+porava+"&feciniconsdesde="+feciniconsdesde+"&feciniconshasta="+feciniconshasta+"&fecentconsdesde="+fecentconsdesde+"&fecentconshasta="+fecentconshasta+"&diascons="+diascons+"&notcons="+notcons+"&nomseg="+nomseg+"&codmon="+codmon+"&fecinginiobra="+fecinginiobra+"&fecingfinobra="+fecingfinobra+"&notseg="+notseg+"&swinfobasica="+swinfobasica+"&swinfosecundaria="+swinfosecundaria+"&swinmobiliaria="+swinmobiliaria+"&swvisitalocal="+swvisitalocal+"&swdiseno="+swdiseno+"&swlicencia="+swlicencia+"&swinterventoria="+swinterventoria+"&swconstructor="+swconstructor+"&swseguridad="+swseguridad+"&vnomcaj="+vernombrecajero+"&vdir="+verdireccion+"&vestcaj="+verestadocajero+"&vanocont="+veranocontable+"&vreg="+verregion+"&vdep="+verdepartamento+"&vmun="+vermunicipio+"&vtipol="+vertipologia+"&vinter="+verintervencion+"&vmoda="+vermodalidad+"&vcodigocajero="+vercodigocajero+"&vcentrocostos="+vercentrocostos+"&vreferencia="+verreferencia+"&vubicacion="+verubicacion+"&vcodigosuc="+vercodigosuc+"&vubicacionatm="+verubicacionatm+"&vatiende="+veratiende+"&vriesgo="+verriesgo+"&varea="+verarea+"&vcodigorecibido="+vercodigorecibido+"&vinmobiliaria="+verinmobiliaria+"&vestadoinm="+verestadoinm+"&vfechainicioinm="+verfechainicioinm+"&vfechaentregainm="+verfechaentregainm+"&vtotaldiasinm="+vertotaldiasinm+"&vnotasinm="+vernotasinm+"&vvisita="+vervisita+"&vestadovis="+verestadovis+"&vfechainiciovis="+verfechainiciovis+"&vfechaentregavis="+verfechaentregavis+"&vtotaldiasvis="+vertotaldiasvis+"&vnotasvis="+vernotasvis+"&vdisenador="+verdisenador+"&vestadodis="+verestadodis+"&vfechainiciodis="+verfechainiciodis+"&vfechaentregadis="+verfechaentregadis+"&vtotaldiasdis="+vertotaldiasdis+"&vnotasdis="+vernotasdis+"&vgestionador="+vergestionador+"&vestadolic="+verestadolic+"&vfecinilic="+verfechainiciolic+"&vfecentlic="+verfechaentregalic+"&vtotaldiaslic="+vertotaldiaslic+"&vnotaslic="+vernotaslic+"&vinterventor="+verinterventor+"&voperadorcanal="+veroperadorcanal+"&vfeciniint="+verfechainicioint+"&vfecentint="+verfechaentregaint+"&vfecped="+verfechapedido+"&vnotasint="+vernotasint+"&vconstructor="+verconstructor+"&vavance="+veravance+"&vfecinicons="+verfechainiciocons+"&vfecentcons="+verfechaentregacons+"&vtotdiascons="+vertotaldiascons+"&vnotcons="+vernotascons+"&vseguridad="+verseguridad+"&vcodmon="+vercodmon+"&vnotasseg="+vernotasseg+"&vfecingobra="+verfecingobra+"&ley="+ley+"&opc="+o+"&clabug="+cb,true);
		//ajax.send(null);
		if(comaprob != 1 && comaprob != 0){ comaprob = ''; }
		if(conaprob != 1 && conaprob != 0){ conaprob = ''; }
		if(preaprob != 1 && preaprob != 0){ preaprob = ''; }
        var formData = "guardar=si&c1="+cajeros+"&c2="+fii+"&c3="+ffi+"&c4="+fie+"&c5="+ffe+"&c6="+nomcaj+"&c7="+dircaj+"&c8="+est+"&c9="+anocon+"&c10="+reg+"&c11="+dep+"&c12="+mun+"&c13="+nominm+"&c14="+estinm+"&c15="+feciniinmdesde+"&c16="+feciniinmhasta+"&c17="+fecentinmdesde+"&c18="+fecentinmhasta+"&c19="+diasinm+"&c20="+notinm+"&c21="+nomvis+"&c22="+estvis+"&c23="+fecinivisdesde+"&c24="+fecinivishasta+"&c25="+fecentvisdesde+"&c26="+fecentvishasta+"&c27="+diasvis+"&c28="+notvis+"&c29="+cod+"&c30="+cencos+"&c31="+notcom+"&c32="+ubi+"&c33="+codsuc+"&c34="+ubiatm+"&c35="+ati+"&c36="+rie+"&c37="+feciniapaatm+"&c38="+fecfinapaatm+"&c39="+comaprob+"&c40="+are+"&c41="+tipol+"&c42="+tipinter+"&c43="+modal+"&c44="+feciniteocomdesde+"&c45="+fecfinteocomhasta+"&c46="+fecentiniteocomdesde+"&c47="+fecentfinteocomhasta+"&c48="+fecinicomdesde+"&c49="+fecfincomhasta+"&c50="+conaprob+"&c51="+estcon+"&c52="+feciniteocondesde+"&c53="+fecfinteoconhasta+"&c54="+fecentiniteocondesde+"&c55="+fecentfinteoconhasta+"&c56="+fecinicondesde+"&c57="+fecfinconhasta+"&c58="+nomdis+"&c59="+estdis+"&c60="+fecinidisdesde+"&c61="+fecinidishasta+"&c62="+fecentdisdesde+"&c63="+fecentdishasta+"&c64="+diasdis+"&c65="+notdis+"&c66="+nomges+"&c67="+estges+"&c68="+fecinigesdesde+"&c69="+fecinigeshasta+"&c70="+fecentgesdesde+"&c71="+fecentgeshasta+"&c72="+diasges+"&c73="+notges+"&c74="+nomcancom+"&c75="+feciniteopredesde+"&c76="+fecfinteoprehasta+"&c77="+fecentiniteopredesde+"&c78="+fecentfinteoprehasta+"&c79="+fecinipredesde+"&c80="+fecfinprehasta+"&c81="+preaprob+"&c82="+notpre+"&c83="+refmaq+"&c84="+pedmaq+"&c85="+notped+"&c86="+feciniteoprodesde+"&c87="+fecfinteoprohasta+"&c88="+fecentiniteoprodesde+"&c89="+fecentfinteoprohasta+"&c90="+notpro+"&c91="+feciniteocandesde+"&c92="+fecfinteocanhasta+"&c93="+fecentiniteocandesde+"&c94="+fecentfinteocanhasta+"&c95="+fecinicandesde+"&c96="+fecfincanhasta+"&c97="+fecentcandesde+"&c98="+fecentcanhasta+"&c99="+notcan+"&c100="+nomint+"&c101="+cancom+"&c102="+feciniintdesde+"&c103="+feciniinthasta+"&c104="+fecentintdesde+"&c105="+fecentinthasta+"&c106="+fecinipedintdesde+"&c107="+fecinipedinthasta+"&c108="+notint+"&c109="+nomcons+"&c110="+porava+"&c111="+feciniconsdesde+"&c112="+feciniconshasta+"&c113="+fecentconsdesde+"&c114="+fecentconshasta+"&c115="+diascons+"&c116="+notcons+"&c117="+nomseg+"&c118="+codmon+"&c119="+fecinginiobra+"&c120="+fecingfinobra+"&c121="+notseg+"&c122="+swinfobasica+"&c123="+swinmobiliaria+"&c124="+swvisitalocal+"&c125="+swcomite+"&c126="+swcontrato+"&c127="+swdiseno+"&c128="+swlicencia+"&c129="+swprefactibilidad+"&c130="+swpedidomaquina+"&c131="+swproyeccion+"&c132="+swcanal+"&c133="+swinterventoria+"&c134="+swconstructor+"&c135="+swseguridad+"&c136="+vernombrecajero+"&c137="+verdireccion+"&c138="+verestadocajero+"&c139="+veranocontable+"&c140="+verregion+"&c141="+verdepartamento+"&c142="+vermunicipio+"&c143="+verinmobiliaria+"&c144="+verestadoinm+"&c145="+verfechainicioinm+"&c146="+verfechaentregainm+"&c147="+vertotaldiasinm+"&c148="+vernotasinm+"&c149="+vervisita+"&c150="+verestadovis+"&c151="+verfechainiciovis+"&c152="+verfechaentregavis+"&c153="+vertotaldiasvis+"&c154="+vernotasvis+"&c155="+vercodigocajero+"&c156="+vercentrocostos+"&c157="+verubicacion+"&c158="+vercodigosuc+"&c159="+vernotascom+"&c160="+veratiende+"&c161="+verriesgo+"&c162="+verubicacionatm+"&c163="+verfecapaatm+"&c164="+veraprocom+"&c165="+verarea+"&c166="+vertipologia+"&c167="+vertipointer+"&c168="+vermoda+"&c169="+verfeciniteocom+"&c170="+verfecentteocom+"&c171="+verfeccom+"&c172="+vercontratoaprobado+"&c173="+verestadocontrato+"&c174="+verfeciniteocontrato+"&c175="+verfecentteocontrato+"&c176="+verfecentcontrato+"&c177="+vernotascontrato+"&c178="+verdisenador+"&c179="+verestadodis+"&c180="+verfechainiciodis+"&c181="+verfechaentregadis+"&c182="+vertotaldiasdis+"&c183="+vernotasdis+"&c184="+vergestionador+"&c185="+verestadolic+"&c186="+verfechainiciolic+"&c187="+verfechaentregalic+"&c188="+vertotaldiaslic+"&c189="+vernotaslic+"&c190="+veroperadorcanal+"&c191="+verfeciniteoprefactibilidad+"&c192="+verfecentteoprefactibilidad+"&c193="+verfecentprefactibilidad+"&c194="+veraprobadoprefactibilidad+"&c195="+vernotasprefactibilidad+"&c196="+verrefmaquina+"&c197="+vermaquina+"&c198="+vernotaspedidomaquina+"&c199="+verfeciniteoproyeccion+"&c200="+verfecentteoproyeccion+"&c201="+vernotasproyeccion+"&c202="+verfeciniteocanal+"&c203="+verfecentteocanal+"&c204="+verfecinicanal+"&c205="+verfecentcanal+"&c206="+vernotascanal+"&c207="+verinterventor+"&c208="+veroperadorcanal+"&c209="+verfechainicioint+"&c210="+verfechaentregaint+"&c211="+verfechapedido+"&c212="+vernotasint+"&c213="+verconstructor+"&c214="+veravance+"&c215="+verfechainiciocons+"&c216="+verfechaentregacons+"&c217="+vertotaldiascons+"&c218="+vernotascons+"&c219="+verinffincons+"&c220="+verseguridad+"&c221="+vercodmon+"&c222="+verfecingobra+"&c223="+vernotasseg+"&c224="+notcon+"&ley="+ley+"&opc="+o+"&clabug="+cb+"&c225="+vernotasgen+'&c226=' + vertipoproyecto+"&c227=" + tipro + "&c228=" + swadjunto+"&c229="+c229 + "&c230=" + c230 + "&c231=" +verfecini ;

        $.ajax({
            url : "cajerossineliminar.php",
            type: "POST",
            data : formData,
            success: function(data, textStatus, jqXHR)
            {
                //data - response from server
                document.getElementById('resultadoalguardar').innerHTML=data;//ajax.responseText;
            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
	}
	else
	{
		alert('Debe ingresar el nombre de la busqueda. Proceso cancelado');
	}
}
function BUSQUEDAGUARDADA(v)
{
	if(v == '' || v == null)
	{
		BUSCAR('CAJERO','');
	}
	else
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('busqueda').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#busqueda").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?busquedaguardada=si&clabug="+v,true);
		ajax.send(null);
		OCULTARSCROLL();
		MOSTRARFILTRO(v);
		REFRESCARBUSQUEDACAJEROS();
		setTimeout("REFRESCARBUSQUEDACAJEROS()",1000);
		setTimeout("REFRESCARBUSQUEDACAJEROS()",1500);
		setTimeout("REFRESCARBUSQUEDACAJEROS()",2000);
		setTimeout("REFRESCARBUSQUEDACAJEROS()",2500);
		setTimeout("REFRESCARBUSQUEDACAJEROS()",3000);
		setTimeout("REFRESCARBUSQUEDACAJEROS()",3500);
		setTimeout("REFRESCARBUSQUEDACAJEROS()",4000);
		setTimeout("REFRESCARBUSQUEDACAJEROS()",4500);
		setTimeout("REFRESCARBUSQUEDACAJEROS()",5000);
		setTimeout("REFRESCARBUSQUEDACAJEROS()",5500);
		setTimeout("REFRESCARBUSQUEDACAJEROS()",6000);
		setTimeout("REFRESCARBUSQUEDACAJEROS()",6500);
		setTimeout("REFRESCARBUSQUEDACAJEROS()",7000);
		setTimeout("REFRESCARBUSQUEDACAJEROS()",7500);
		setTimeout("REFRESCARBUSQUEDACAJEROS()",8000);
	}
}
function MOSTRARFILTRO(v)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('cajeros').innerHTML=ajax.responseText;
            refreshSelect();
	    }
	}
	jQuery("#cajeros").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarfiltro=si&clabug="+v,true);
	ajax.send(null);
	OCULTARSCROLL();
	REFRESCARBUSQUEDACAJEROS();
	setTimeout("REFRESCARBUSQUEDACAJEROS()",1000);
	setTimeout("REFRESCARBUSQUEDACAJEROS()",1500);
	setTimeout("REFRESCARBUSQUEDACAJEROS()",2000);
	setTimeout("REFRESCARBUSQUEDACAJEROS()",2500);
	setTimeout("REFRESCARBUSQUEDACAJEROS()",3000);
}
function ELIMINARBUSQUEDA(v)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('resultadoalguardar').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#resultadoalguardar").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?eliminarbusqueda=si&clabug="+v,true);
	ajax.send(null);
}
function VERDEPARTAMENTOS(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('departamentos').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#departamentos").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?verdepartamentos=si&reg="+v,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTADEPARTAMENTOS()",800);
}
function VERCIUDADES(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('ciudades').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#ciudades").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?verciudades=si&dep="+v,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTACIUDADES()",800);

}

function EXPORTAR()
{
    //ENCABEZADO
    var cajeros = OBTENERSELECCIONMULTIPLE("multiselect_buscajero");//form1.buscajero.value;
	var fii = form1.fechainicio.value;
	var ffi = form1.fechafin.value;
	var fie = form1.fechainicioentrega.value;
	var ffe = form1.fechafinentrega.value;

	//INFO BASE
	var nomcaj = form1.busnombre.value;
	var dircaj = form1.busdireccion.value;
	var est = OBTENERSELECCIONMULTIPLE("multiselect_busestado");//form1.busestado.value;
	var anocon = form1.busanocontable.value;
	var reg = OBTENERSELECCIONMULTIPLE("multiselect_busregion");//form1.busregion.value;
	var dep = OBTENERSELECCIONMULTIPLE("multiselect_busdepartamento");//form1.busdepartamento.value;
	var mun = OBTENERSELECCIONMULTIPLE("multiselect_busmunicipio");//form1.busmunicipio.value;
	var tipro = OBTENERSELECCIONMULTIPLE("multiselect_bustipoproyecto");

	//INMOBILIARIA
	var nominm = OBTENERSELECCIONMULTIPLE("multiselect_busnombreinmobiliaria");//form1.busnombreinmobiliaria.value;
	var estinm = OBTENERSELECCIONMULTIPLE("multiselect_busestadoinmobiliaria");//form1.busestadoinmobiliaria.value;
	var feciniinmdesde = form1.busfechainicioinmobiliaria.value;
	var feciniinmhasta = form1.busfechafininmobiliaria.value;
	var fecentinmdesde = form1.busfechaentregainiinfoinmobiliaria.value;
	var fecentinmhasta = form1.busfechaentregafininfoinmobiliaria.value;
	var diasinm = form1.bustotaldiasinmobiliaria.value;
	var notinm = form1.busnotasinmobiliaria.value;
	
	//VISITA LOCAL
	var nomvis = OBTENERSELECCIONMULTIPLE("multiselect_busnombrevisita");//form1.busnombrevisita.value;
	var estvis = OBTENERSELECCIONMULTIPLE("multiselect_busestadovisita");//form1.busestadovisita.value;
	var fecinivisdesde = form1.busfechainiciovisita.value;
	var fecinivishasta = form1.busfechafinvisita.value;
	var fecentvisdesde = form1.busfechaentregainiinfovisita.value;
	var fecentvishasta = form1.busfechaentregafininfovisita.value;
	var diasvis = form1.bustotaldiasvisita.value;
	var notvis = form1.busnotasvisita.value;
		
	//COMITE
	var cod = form1.buscodigo.value;
	var cencos =  OBTENERSELECCIONMULTIPLE("multiselect_buscentrocostos");// form1.buscentrocostos.value;
	var notcom = form1.busnotascomite.value;
	var ubi = OBTENERSELECCIONMULTIPLE("multiselect_busubicacion");//form1.busubicacion.value;
	var codsuc = form1.buscodigosuc.value;
	var ubiatm = OBTENERSELECCIONMULTIPLE("multiselect_busubicacionamt");//form1.busubicacionamt.value;
	var ati = OBTENERSELECCIONMULTIPLE("multiselect_busatiende");//form1.busatiende.value;
	var rie = OBTENERSELECCIONMULTIPLE("multiselect_busriesgo");//form1.busriesgo.value;
	var feciniapaatm = form1.busfechainiapagadoatm.value;
	var fecfinapaatm = form1.busfechafinapagadoatm.value;
	var comaprob = $('input[name=comiteaprobadobancolombia]:checked', '#form1').val();
	var are = OBTENERSELECCIONMULTIPLE("multiselect_busarea");//form1.busarea.value;
	var tipol = OBTENERSELECCIONMULTIPLE("multiselect_bustipologia");//form1.busarea.value;
	var tipinter = OBTENERSELECCIONMULTIPLE("multiselect_bustipointervencion");//form1.busarea.value;
	var modal = OBTENERSELECCIONMULTIPLE("multiselect_busmodalidad");//form1.busarea.value;
	var feciniteocomdesde = form1.fechainicioteoricacomitebancolombia.value;
	var fecfinteocomhasta = form1.fechafinalteoricacomitebancolombia.value;
	var fecentiniteocomdesde = form1.fechaentregainiteoricacomitebancolombia.value;
	var fecentfinteocomhasta = form1.fechaentregafinteoricacomitebancolombia.value;
	var fecinicomdesde = form1.fechainicomitebancolombia.value;
	var fecfincomhasta = form1.fechafincomitebancolombia.value;

	//CONTRATO
	var conaprob = $('input[name=contratoaprobadobancolombia]:checked', '#form1').val();
	var estcon = OBTENERSELECCIONMULTIPLE("multiselect_busestadocontratobancolombia");
	var feciniteocondesde = form1.fechainicioteoricacontratobancolombia.value;
	var fecfinteoconhasta = form1.fechainiciofinteoricacontratobancolombia.value;
	var fecentiniteocondesde = form1.fechaentregainiteoricacontratobancolombia.value;
	var fecentfinteoconhasta = form1.fechaentregafinteoricacontratobancolombia.value;
	var fecinicondesde = form1.fechaentregainicontratobancolombia.value;
	var fecfinconhasta = form1.fechaentregafincontratobancolombia.value;
	var notcon = form1.notacontrato.value;
	
	//DISENO
	var nomdis = OBTENERSELECCIONMULTIPLE("multiselect_busnombrediseno");//form1.busnombrediseno.value;
	var estdis = OBTENERSELECCIONMULTIPLE("multiselect_busestadodiseno");//form1.busestadodiseno.value;
	var fecinidisdesde = form1.busfechainiciodiseno.value;
	var fecinidishasta = form1.busfechafindiseno.value;
	var fecentdisdesde = form1.busfechaentregainiinfodiseno.value;
	var fecentdishasta = form1.busfechaentregafininfodiseno.value;
	var diasdis = form1.bustotaldiasdiseno.value;
	var notdis = form1.busnotasdiseno.value;
	
	//LICENCIA
	var nomges = OBTENERSELECCIONMULTIPLE("multiselect_busnombregestionador");//form1.busnombregestionador.value;
	var estges = OBTENERSELECCIONMULTIPLE("multiselect_busestadogestionador");//form1.busestadogestionador.value;
	var fecinigesdesde = form1.busfechainiciogestionador.value;
	var fecinigeshasta = form1.busfechafingestionador.value;
	var fecentgesdesde = form1.busfechaentregainiinfogestionador.value;
	var fecentgeshasta = form1.busfechaentregafininfogestionador.value;
	var diasges = form1.bustotaldiasgestionador.value;
	var notges = form1.busnotasgestionador.value;
	
	//PREFACTIBILIDAD
	var nomcancom = OBTENERSELECCIONMULTIPLE("multiselect_buscanalcomunicacion");
	var feciniteopredesde = form1.fechainicioteoricaprefactibilidadbancolombia.value;
	var fecfinteoprehasta = form1.fechafinteoricaprefactibilidadbancolombia.value;
	var fecentiniteopredesde = form1.fechaentregainiteoricaprefactibilidadbancolombia.value;
	var fecentfinteoprehasta = form1.fechaentregafinteoricaprefactibilidadbancolombia.value;
	var fecinipredesde = form1.fechaentregainiprefactibilidadbancolombia.value;
	var fecfinprehasta = form1.fechaentregafinprefactibilidadbancolombia.value;
	var preaprob = $('input[name=prefactibilidadaprobadobancolombia]:checked', '#form1').val();
	var notpre = form1.notaprefactibilidad.value;
	
	//PEDIDO MAQUINA
	var refmaq = OBTENERSELECCIONMULTIPLE("multiselect_referenciamaquina");
	var pedmaq = OBTENERSELECCIONMULTIPLE("multiselect_pedidomaquinabancolombia");
	var notped = form1.notapedidomaquina.value;
	
	//PROYECCI�N DE INSTALACION
	var feciniteoprodesde = form1.fechainicioteoricaproyeccionbancolombia.value;
	var fecfinteoprohasta = form1.fechafinteoricaproyeccionbancolombia.value;
	var fecentiniteoprodesde = form1.fechaentregainiteoricaproyeccion.value;
	var fecentfinteoprohasta = form1.fechaentregafinteoricaproyeccion.value;
	var notpro = form1.notaproyeccion.value;
	
	//CANAL
	var feciniteocandesde = form1.fechainicioteoricacanal.value;
	var fecfinteocanhasta = form1.fechafinteoricacanal.value;
	var fecentiniteocandesde = form1.fechaentregainiteoricacanal.value;
	var fecentfinteocanhasta = form1.fechaentregafinteoricacanal.value;
	var fecinicandesde = form1.fechainiciocanal.value;
	var fecfincanhasta = form1.fechafincanal.value;
	var fecentcandesde = form1.fechaentregainicanal.value;
	var fecentcanhasta = form1.fechaentregafincanal.value;
	var notcan = form1.notacanal.value;
	
	//INTERVENTORIA
	var nomint = OBTENERSELECCIONMULTIPLE("multiselect_busnombreinterventor");//form1.busnombreinterventor.value;
	var cancom = OBTENERSELECCIONMULTIPLE("multiselect_buscanalcomunicacion");//form1.buscanalcomunicacion.value;
	var feciniintdesde = form1.busfechainiciointerventor.value;
	var feciniinthasta = form1.busfechafininterventor.value;
	var fecentintdesde = form1.busfechaentregainiinfointerventor.value;
	var fecentinthasta = form1.busfechaentregafininfointerventor.value;
	var fecinipedintdesde = form1.busfechainipedidointerventor.value;
	var fecinipedinthasta = form1.busfechafinpedidointerventor.value;
	var notint = form1.busnotasinterventor.value;
	
	//CONSTRUCTOR
	var nomcons = OBTENERSELECCIONMULTIPLE("multiselect_busnombreconstructor");//form1.busnombreconstructor.value;
	var porava = OBTENERSELECCIONMULTIPLE("multiselect_busporcentajeavance");//form1.busporcentajeavance.value;
	var feciniconsdesde = form1.busfechainicioconstructor.value;
	var feciniconshasta = form1.busfechafinconstructor.value;
	var fecentconsdesde = form1.busfechaentregainiinfoconstructor.value;
	var fecentconshasta = form1.busfechaentregafininfoconstructor.value;
	var diascons = form1.bustotaldiasconstructor.value;
	var notcons = form1.busnotasconstructor.value;
	
	//SEGURIDAD
	var nomseg = OBTENERSELECCIONMULTIPLE("multiselect_busnombreseguridad");//form1.busnombreseguridad.value;
	var codmon = form1.buscodigomonitoreo.value;
	var fecinginiobra = form1.busfechaingresoiniobra.value;
	var fecingfinobra = form1.busfechaingresofinobra.value;
	var notseg = form1.busnotasseguridad.value;

    var c230 = OBTENERSELECCIONMULTIPLE("multiselect_c230");//MULTIPPLE ADJUNTO
	
	var swinfobasica = form1.ocuinfobasica.value;
	var swinmobiliaria = form1.ocuinmobiliaria.value;
	var swvisitalocal = form1.ocuvisitalocal.value;
	var swcomite = form1.ocucomite.value;
	var swcontrato = form1.ocucontrato.value;
	var swdiseno = form1.ocudiseno.value;
	var swlicencia = form1.oculicencia.value;
	var swprefactibilidad = form1.ocuprefactibilidad.value;
	var swpedidomaquina = form1.ocupedidomaquina.value;
	var swproyeccion = form1.ocuproyeccion.value;
	var swcanal= form1.ocucanal.value;
	var swinterventoria = form1.ocuinterventoria.value;
	var swconstructor = form1.ocuconstructor.value;
	var swseguridad = form1.ocuseguridad.value;
	var swadjunto = form1.ocuadjuntos.value;
	
	//VISTA INFO BASE
	var vernombrecajero = form1.vernombrecajero.checked;
	var verdireccion = form1.verdireccion.checked;
	var verestadocajero = form1.verestadocajero.checked;
	var veranocontable = form1.veranocontable.checked;
	var verregion = form1.verregion.checked;
	var verdepartamento = form1.verdepartamento.checked;
	var vermunicipio = form1.vermunicipio.checked;
	var vertipoproyecto = form1.vertipoproyecto.checked;
	var verfecini = form1.verfecini.checked;
	
	//INMOBILIARIA
	var verinmobiliaria = form1.verinmobiliaria1.checked;
	var verestadoinm = form1.verestadoinm.checked;
	var verfechainicioinm = form1.verfechainicioinm.checked;
	var verfechaentregainm = form1.verfechaentregainm.checked;
	var vertotaldiasinm = form1.vertotaldiasinm.checked;
	var vernotasinm = form1.vernotasinm.checked;
	
	//VISITA LOCAL
	var vervisita = form1.vervisita1.checked;
	var verestadovis = form1.verestadovis.checked;
	var verfechainiciovis = form1.verfechainiciovis.checked;
	var verfechaentregavis = form1.verfechaentregavis.checked;
	var vertotaldiasvis = form1.vertotaldiasvis.checked;
	var vernotasvis = form1.vernotasvis.checked;
	
	//COMITE
	var vercodigocajero = form1.vercodigocajero.checked;
	var vercentrocostos = form1.vercentrocostos.checked;
	var verubicacion = form1.verubicacion.checked;
	var vercodigosuc = form1.vercodigosuc.checked;
	var vernotascom = form1.vernotascom.checked;
	var veratiende = form1.veratiende.checked;
	var verriesgo = form1.verriesgo.checked;
	var verubicacionatm = form1.verubicacionatm.checked;
	var verfecapaatm = form1.verfechaapagado.checked;
	var veraprocom = form1.vercomiteaprobado.checked;
	var verarea = form1.verarea.checked;
	var vertipologia = form1.vertipologia.checked;
	var vertipointer = form1.verintervencion.checked;
	var vermoda = form1.vermodalidad.checked;
	var verfeciniteocom = form1.verfechainicioteoricacomite.checked;
	var verfecentteocom = form1.verfechaentregateoricacomite.checked;
	var verfeccom = form1.verfechacomite.checked;
	
	//CONTRATO
	var vercontratoaprobado = form1.vercontratoaprobado.checked;
	var verestadocontrato = form1.verestadocontrato.checked;
	var verfeciniteocontrato = form1.verfechainicioteoricacontrato.checked;
	var verfecentteocontrato = form1.verfechaentregateoricacontrato.checked;
	var verfecentcontrato = form1.verfechaentregacontrato.checked;
	var vernotascontrato = form1.vernotascontrato.checked;

	//DIENO
	var verdisenador = form1.verdisenador.checked;
	var verestadodis = form1.verestadodis.checked;
	var verfechainiciodis = form1.verfechainiciodis.checked;
	var verfechaentregadis = form1.verfechaentregadis.checked;
	var vertotaldiasdis = form1.vertotaldiasdis.checked;
	var vernotasdis = form1.vernotasdis.checked;
	
	//LICENCIA
	var vergestionador = form1.vergestionador.checked;
	var verestadolic = form1.verestadolic.checked;
	var verfechainiciolic = form1.verfechainiciolic.checked;
	var verfechaentregalic = form1.verfechaentregalic.checked;
	var vertotaldiaslic = form1.vertotaldiaslic.checked;
	var vernotaslic = form1.vernotaslic.checked;
	
	//PREFACTIBILIDAD CANAL
	var veroperadorcanal = form1.veroperadorcanal.checked;
	var verfeciniteoprefactibilidad = form1.verfechainiteoricaprefactibilidad.checked;
	var verfecentteoprefactibilidad = form1.verfechaentregateoricaprefactibilidad.checked;
	var verfecentprefactibilidad = form1.verfechaentregaprefactibilidad.checked;
	var veraprobadoprefactibilidad = form1.veraprobadoprefactibilidad.checked;
	var vernotasprefactibilidad = form1.vernotasprefactibilidad.checked;
	
	//PEDIDO MAQUINA
	var verrefmaquina = form1.verreferenciamaquina.checked;
	var vermaquina = form1.vermaquina.checked;
	var vernotaspedidomaquina = form1.vernotaspedidomaquina.checked;
	
	//PROYECCI�N INSTALACI�N
	var verfeciniteoproyeccion = form1.verfechainiteoricaproyeccion.checked;
	var verfecentteoproyeccion = form1.verfechaentregateoricaproyeccion.checked;
	var vernotasproyeccion = form1.vernotasproyeccion.checked;
	
	//CANAL
	var verfeciniteocanal = form1.verfechainicioteoricacanal.checked;
	var verfecentteocanal = form1.verfechaentregateoricacanal.checked;
	var verfecentcanal = form1.verfechaentregacanal.checked;
	var verfecinicanal = form1.verfechainiciocanal.checked;
	var vernotascanal = form1.vernotascanal.checked;
	
	//INTERVENTORIA
	var verinterventor = form1.verinterventor.checked;
	var veroperadorcanal = form1.veroperadorcanal.checked;
	var verfechainicioint = form1.verfechainicioint.checked;
	var verfechaentregaint = form1.verfechaentregaint.checked;
	var verfechapedido = form1.verfechapedido.checked;
	var vernotasint = form1.vernotasint.checked;
	
	//CONSTRUCTOR
	var verconstructor = form1.verconstructor1.checked;
	var veravance = form1.veravance.checked;
	var verfechainiciocons = form1.verfechainiciocons.checked;
	var verfechaentregacons = form1.verfechaentregacons.checked;
	var vertotaldiascons = form1.vertotaldiascons.checked;
	var vernotascons = form1.vernotascons.checked;
	var verinffincons = form1.verinformefinalcons.checked;
	
	//SEGURIDAD
	var verseguridad = form1.verseguridad1.checked;
	var vercodmon = form1.vercodigomonitoreo.checked;
	var verfecingobra = form1.verfechaingresoobra.checked;
	var vernotasseg = form1.vernotasseg.checked;
	
	if(comaprob != 1 && comaprob != 0){ comaprob = ''; }
	if(conaprob != 1 && conaprob != 0){ conaprob = ''; }
	if(preaprob != 1 && preaprob != 0){ preaprob = ''; }

    var vernotasgen = form1.vernotasgen.checked;
	var c229 = form1.c229.checked;
		
	//window.location.href = "informes/informeexcel.php?caj="+cajeros+"&fii="+fii+"&ffi="+ffi+"&fie="+fie+"&ffe="+ffe+"&nomcaj="+nomcaj+"&dircaj="+dircaj+"&est="+est+"&anocon="+anocon+"&reg="+reg+"&dep="+dep+"&mun="+mun+"&tip="+tip+"&tipint="+tipint+"&moda="+moda+"&cod="+cod+"&cencos="+cencos+"&refmaq="+refmaq+"&ubi="+ubi+"&codsuc="+codsuc+"&ubiatm="+ubiatm+"&ati="+ati+"&rie="+rie+"&are="+are+"&fecapagadoatm="+fecapagadoatm+"&nominm="+nominm+"&estinm="+estinm+"&feciniinmdesde="+feciniinmdesde+"&feciniinmhasta="+feciniinmhasta+"&fecentinmdesde="+fecentinmdesde+"&fecentinmhasta="+fecentinmhasta+"&diasinm="+diasinm+"&notinm="+notinm+"&nomvis="+nomvis+"&estvis="+estvis+"&fecinivisdesde="+fecinivisdesde+"&fecinivishasta="+fecinivishasta+"&fecentvisdesde="+fecentvisdesde+"&fecentvishasta="+fecentvishasta+"&diasvis="+diasvis+"&notvis="+notvis+"&nomdis="+nomdis+"&estdis="+estdis+"&fecinidisdesde="+fecinidisdesde+"&fecinidishasta="+fecinidishasta+"&fecentdisdesde="+fecentdisdesde+"&fecentdishasta="+fecentdishasta+"&diasdis="+diasdis+"&notdis="+notdis+"&nomges="+nomges+"&estges="+estges+"&fecinigesdesde="+fecinigesdesde+"&fecinigeshasta="+fecinigeshasta+"&fecentgesdesde="+fecentgesdesde+"&fecentgeshasta="+fecentgeshasta+"&diasges="+diasges+"&notges="+notges+"&nomint="+nomint+"&cancom="+cancom+"&feciniintdesde="+feciniintdesde+"&feciniinthasta="+feciniinthasta+"&fecentintdesde="+fecentintdesde+"&fecentinthasta="+fecentinthasta+"&fecinipedintdesde="+fecinipedintdesde+"&fecinipedinthasta="+fecinipedinthasta+"&notint="+notint+"&nomcons="+nomcons+"&porava="+porava+"&feciniconsdesde="+feciniconsdesde+"&feciniconshasta="+feciniconshasta+"&fecentconsdesde="+fecentconsdesde+"&fecentconshasta="+fecentconshasta+"&diascons="+diascons+"&notcons="+notcons+"&nomseg="+nomseg+"&codmon="+codmon+"&finginiobra="+fecinginiobra+"&fingfinobra="+fecingfinobra+"&notseg="+notseg+"&swinfobasica="+swinfobasica+"&swinfosecundaria="+swinfosecundaria+"&swinmobiliaria="+swinmobiliaria+"&swvisitalocal="+swvisitalocal+"&swdiseno="+swdiseno+"&swlicencia="+swlicencia+"&swinterventoria="+swinterventoria+"&swconstructor="+swconstructor+"&swseguridad="+swseguridad+"&vnomcaj="+vernombrecajero+"&vdir="+verdireccion+"&vestcaj="+verestadocajero+"&vanocont="+veranocontable+"&vreg="+verregion+"&vdep="+verdepartamento+"&vmun="+vermunicipio+"&vtipol="+vertipologia+"&vinter="+verintervencion+"&vmoda="+vermodalidad+"&vcodigocajero="+vercodigocajero+"&vcentrocostos="+vercentrocostos+"&vreferencia="+verreferencia+"&vubicacion="+verubicacion+"&vcodigosuc="+vercodigosuc+"&vubicacionatm="+verubicacionatm+"&vatiende="+veratiende+"&vriesgo="+verriesgo+"&varea="+verarea+"&vcodigorecibido="+vercodigorecibido+"&vinmobiliaria="+verinmobiliaria+"&vestadoinm="+verestadoinm+"&vfechainicioinm="+verfechainicioinm+"&vfechaentregainm="+verfechaentregainm+"&vtotaldiasinm="+vertotaldiasinm+"&vnotasinm="+vernotasinm+"&vvisita="+vervisita+"&vestadovis="+verestadovis+"&vfechainiciovis="+verfechainiciovis+"&vfechaentregavis="+verfechaentregavis+"&vtotaldiasvis="+vertotaldiasvis+"&vnotasvis="+vernotasvis+"&vdisenador="+verdisenador+"&vestadodis="+verestadodis+"&vfechainiciodis="+verfechainiciodis+"&vfechaentregadis="+verfechaentregadis+"&vtotaldiasdis="+vertotaldiasdis+"&vnotasdis="+vernotasdis+"&vgestionador="+vergestionador+"&vestadolic="+verestadolic+"&vfechainiciolic="+verfechainiciolic+"&vfechaentregalic="+verfechaentregalic+"&vtotaldiaslic="+vertotaldiaslic+"&vnotaslic="+vernotaslic+"&vinterventor="+verinterventor+"&voperadorcanal="+veroperadorcanal+"&vfechainicioint="+verfechainicioint+"&vfechaentregaint="+verfechaentregaint+"&vfechapedido="+verfechapedido+"&vnotasint="+vernotasint+"&vconstructor="+verconstructor+"&vavance="+veravance+"&vfechainiciocons="+verfechainiciocons+"&vfechaentregacons="+verfechaentregacons+"&vtotaldiascons="+vertotaldiascons+"&vnotascons="+vernotascons+"&vinffincons="+verinffincons+"&vseguridad="+verseguridad+"&vcodmon="+vercodmon+"&vnotasseg="+vernotasseg+"&vfecingobra="+verfecingobra;
	window.open("informes/informeexcel.php?c1="+cajeros+"&c2="+fii+"&c3="+ffi+"&c4="+fie+"&c5="+ffe+"&c6="+nomcaj+"&c7="+dircaj+"&c8="+est+"&c9="+anocon+"&c10="+reg+"&c11="+dep+"&c12="+mun+"&c13="+nominm+"&c14="+estinm+"&c15="+feciniinmdesde+"&c16="+feciniinmhasta+"&c17="+fecentinmdesde+"&c18="+fecentinmhasta+"&c19="+diasinm+"&c20="+notinm+"&c21="+nomvis+"&c22="+estvis+"&c23="+fecinivisdesde+"&c24="+fecinivishasta+"&c25="+fecentvisdesde+"&c26="+fecentvishasta+"&c27="+diasvis+"&c28="+notvis+"&c29="+cod+"&c30="+cencos+"&c31="+notcom+"&c32="+ubi+"&c33="+codsuc+"&c34="+ubiatm+"&c35="+ati+"&c36="+rie+"&c37="+feciniapaatm+"&c38="+fecfinapaatm+"&c39="+comaprob+"&c40="+are+"&c41="+tipol+"&c42="+tipinter+"&c43="+modal+"&c44="+feciniteocomdesde+"&c45="+fecfinteocomhasta+"&c46="+fecentiniteocomdesde+"&c47="+fecentfinteocomhasta+"&c48="+fecinicomdesde+"&c49="+fecfincomhasta+"&c50="+conaprob+"&c51="+estcon+"&c52="+feciniteocondesde+"&c53="+fecfinteoconhasta+"&c54="+fecentiniteocondesde+"&c55="+fecentfinteoconhasta+"&c56="+fecinicondesde+"&c57="+fecfinconhasta+"&c58="+nomdis+"&c59="+estdis+"&c60="+fecinidisdesde+"&c61="+fecinidishasta+"&c62="+fecentdisdesde+"&c63="+fecentdishasta+"&c64="+diasdis+"&c65="+notdis+"&c66="+nomges+"&c67="+estges+"&c68="+fecinigesdesde+"&c69="+fecinigeshasta+"&c70="+fecentgesdesde+"&c71="+fecentgeshasta+"&c72="+diasges+"&c73="+notges+"&c74="+nomcancom+"&c75="+feciniteopredesde+"&c76="+fecfinteoprehasta+"&c77="+fecentiniteopredesde+"&c78="+fecentfinteoprehasta+"&c79="+fecinipredesde+"&c80="+fecfinprehasta+"&c81="+preaprob+"&c82="+notpre+"&c83="+refmaq+"&c84="+pedmaq+"&c85="+notped+"&c86="+feciniteoprodesde+"&c87="+fecfinteoprohasta+"&c88="+fecentiniteoprodesde+"&c89="+fecentfinteoprohasta+"&c90="+notpro+"&c91="+feciniteocandesde+"&c92="+fecfinteocanhasta+"&c93="+fecentiniteocandesde+"&c94="+fecentfinteocanhasta+"&c95="+fecinicandesde+"&c96="+fecfincanhasta+"&c97="+fecentcandesde+"&c98="+fecentcanhasta+"&c99="+notcan+"&c100="+nomint+"&c101="+cancom+"&c102="+feciniintdesde+"&c103="+feciniinthasta+"&c104="+fecentintdesde+"&c105="+fecentinthasta+"&c106="+fecinipedintdesde+"&c107="+fecinipedinthasta+"&c108="+notint+"&c109="+nomcons+"&c110="+porava+"&c111="+feciniconsdesde+"&c112="+feciniconshasta+"&c113="+fecentconsdesde+"&c114="+fecentconshasta+"&c115="+diascons+"&c116="+notcons+"&c117="+nomseg+"&c118="+codmon+"&c119="+fecinginiobra+"&c120="+fecingfinobra+"&c121="+notseg+"&c122="+swinfobasica+"&c123="+swinmobiliaria+"&c124="+swvisitalocal+"&c125="+swcomite+"&c126="+swcontrato+"&c127="+swdiseno+"&c128="+swlicencia+"&c129="+swprefactibilidad+"&c130="+swpedidomaquina+"&c131="+swproyeccion+"&c132="+swcanal+"&c133="+swinterventoria+"&c134="+swconstructor+"&c135="+swseguridad+"&c136="+vernombrecajero+"&c137="+verdireccion+"&c138="+verestadocajero+"&c139="+veranocontable+"&c140="+verregion+"&c141="+verdepartamento+"&c142="+vermunicipio+"&c143="+verinmobiliaria+"&c144="+verestadoinm+"&c145="+verfechainicioinm+"&c146="+verfechaentregainm+"&c147="+vertotaldiasinm+"&c148="+vernotasinm+"&c149="+vervisita+"&c150="+verestadovis+"&c151="+verfechainiciovis+"&c152="+verfechaentregavis+"&c153="+vertotaldiasvis+"&c154="+vernotasvis+"&c155="+vercodigocajero+"&c156="+vercentrocostos+"&c157="+verubicacion+"&c158="+vercodigosuc+"&c159="+vernotascom+"&c160="+veratiende+"&c161="+verriesgo+"&c162="+verubicacionatm+"&c163="+verfecapaatm+"&c164="+veraprocom+"&c165="+verarea+"&c166="+vertipologia+"&c167="+vertipointer+"&c168="+vermoda+"&c169="+verfeciniteocom+"&c170="+verfecentteocom+"&c171="+verfeccom+"&c172="+vercontratoaprobado+"&c173="+verestadocontrato+"&c174="+verfeciniteocontrato+"&c175="+verfecentteocontrato+"&c176="+verfecentcontrato+"&c177="+vernotascontrato+"&c178="+verdisenador+"&c179="+verestadodis+"&c180="+verfechainiciodis+"&c181="+verfechaentregadis+"&c182="+vertotaldiasdis+"&c183="+vernotasdis+"&c184="+vergestionador+"&c185="+verestadolic+"&c186="+verfechainiciolic+"&c187="+verfechaentregalic+"&c188="+vertotaldiaslic+"&c189="+vernotaslic+"&c190="+veroperadorcanal+"&c191="+verfeciniteoprefactibilidad+"&c192="+verfecentteoprefactibilidad+"&c193="+verfecentprefactibilidad+"&c194="+veraprobadoprefactibilidad+"&c195="+vernotasprefactibilidad+"&c196="+verrefmaquina+"&c197="+vermaquina+"&c198="+vernotaspedidomaquina+"&c199="+verfeciniteoproyeccion+"&c200="+verfecentteoproyeccion+"&c201="+vernotasproyeccion+"&c202="+verfeciniteocanal+"&c203="+verfecentteocanal+"&c204="+verfecinicanal+"&c205="+verfecentcanal+"&c206="+vernotascanal+"&c207="+verinterventor+"&c208="+veroperadorcanal+"&c209="+verfechainicioint+"&c210="+verfechaentregaint+"&c211="+verfechapedido+"&c212="+vernotasint+"&c213="+verconstructor+"&c214="+veravance+"&c215="+verfechainiciocons+"&c216="+verfechaentregacons+"&c217="+vertotaldiascons+"&c218="+vernotascons+"&c219="+verinffincons+"&c220="+verseguridad+"&c221="+vercodmon+"&c222="+verfecingobra+"&c223="+vernotasseg+"&c224="+notcon+'&c226=' + vertipoproyecto+"&c227=" + tipro+"&c228=" + vernotasgen + "&swadjunto=" + swadjunto + "&c229=" + c229 + "&c230=" + c230 + "&c231="+verfecini);
}
function VERMODALIDADES(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('modalidades').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#modalidades").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?vermodalidades=si&tii="+v,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTAMODALIDADES()",800);
}


function OBTENERSELECCIONMULTIPLE(control)
{
    var objCBarray = document.getElementsByName(control);
    valores = "";
    for (i = 0; i < objCBarray.length; i++)
    {
        if (objCBarray[i].checked && objCBarray[i].value != "")
        {
            valores += objCBarray[i].value + ",";
        }
    }

    return valores;
}
function MOSTRARNOTAS(c)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todaslasnotas').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todaslasnotas").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarnotas=si&clacaj="+c,true);
	ajax.send(null);
}