function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }

function VISOR(c,v)
{
    var url = c.replace('../../','www.pavas.com.co/Cajeros/');
    var mdl =  "<div class='embed-responsive embed-responsive-16by9'><iframe class='embed-responsive-item' src='https://docs.google.com/viewer?url="+url+"&embedded=true' frameborder='0' allowfullscreen></iframe></div>";
    $('#' + v).html(mdl);

}
function VISOR2(c,v)
{
	var url = c.replace('../../','www.pavas.com.co/Sucursales/');
    var mdl =  "<div class='embed-responsive embed-responsive-16by9'><iframe class='embed-responsive-item' src='https://docs.google.com/viewer?url="+url+"&embedded=true' frameborder='0' allowfullscreen></iframe></div>";
    $('#' + v).html(mdl);

}
function VERINFOCAJERO()
{
	var clacaj = $('#cajero').val();
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('infocajero').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#infocajero").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?verinfocajero=si&clacaj="+clacaj,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTA()",1500);
	OCULTARSCROLL();
}
function VERINFOCAJERO1()
{
	var clacaj = $('#cajero1').val();
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('infocajero').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#infocajero").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?verinfocajero=si&clacaj="+clacaj,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTA()",1500);
	OCULTARSCROLL();
}
function MOSTRARBITACORAADJUNTOS(c)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todoslosadjuntosbitacora').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todoslosadjuntosbitacora").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarbitacoraadjuntos=si&clacaj="+c,true);
	ajax.send(null);
}
function MOSTRARADJUNTOS(c,a)
{
    var ajax;
    ajax=new ajaxFunction();
    ajax.onreadystatechange=function()
    {
        if(ajax.readyState==4)
        {
            document.getElementById('adjuntoarchivos').innerHTML=ajax.responseText;
        }
    }
    jQuery("#adjuntoarchivos").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
    ajax.open("GET","?mostraradjuntos=si&clacaj="+c+"&actor="+a,true);
    ajax.send(null);
}
function MOSTRARBITACORA(c)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todaslasnotas').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todaslasnotas").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarbitacora=si&clacaj="+c,true);
	ajax.send(null);
}