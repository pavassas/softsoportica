function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function CAMBIAR()
{
	var ant = form1.anterior.value;
	var nue = form1.nueva.value;
	var conf = form1.confirmar.value;
    var longitud = false,
        minuscula = false,
        numero = false,
        mayuscula = false, space = false;


    if (nue.length < 8) {
        //$('#length').removeClass('valid').addClass('invalid');
        longitud = false;
    } else {
        //$('#length').removeClass('invalid').addClass('valid');
        longitud = true;
    }

    //validate letter
    if (nue.match(/[A-z]/)) {
        //$('#letter').removeClass('invalid').addClass('valid');
        minuscula = true;
    } else {
        // $('#letter').removeClass('valid').addClass('invalid');
        minuscula = false;
    }

    //validate capital letter
    if (nue.match(/[A-Z]/)) {
        // $('#capital').removeClass('invalid').addClass('valid');
        mayuscula = true;
    } else {
        //$('#capital').removeClass('valid').addClass('invalid');
        mayuscula = false;
    }

    //validate number
    if (nue.match(/\d/)) {
        // $('#number').removeClass('invalid').addClass('valid');
        numero = true;
    } else {
        //$('#number').removeClass('valid').addClass('invalid');
        numero = false;
    }

    if(/\s/.test(nue)){
        space = false;
    }
    else
    {
        space = true
    }
     if((longitud==false || minuscula==false || numero==false || mayuscula==false || space==false)){
     alert("No cumple con los parametros de seguridad de una contraseña");


     }
     else
     {

         var ajax;
         ajax = new ajaxFunction();
         ajax.onreadystatechange = function () {
             if (ajax.readyState == 4) {
                 document.getElementById('datos').innerHTML = ajax.responseText;
             }
         }

         jQuery("#datos").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
         ajax.open("GET", "?cambiarcon=si&ant=" + ant + "&nue=" + nue + "&conf=" + conf, true);
         ajax.send(null);
     }
}

function validarpass(id) {

    $('#'+id).keyup(function() {
        // set password variable
        var pswd = $(this).val();
        //validate the length
        if ( pswd.length < 8 ) {
            $('#length').html('Debería tener <strong>8 carácteres</strong> como mínimo');
            $('#length').css('color',"red");
        } else {
            $('#length').html('');
        }

        //validate letter
        if ( pswd.match(/[A-z]/) ) {
            $('#letter').html('');
        } else {
            $('#letter').html('Al menos debería tener <strong>una letra</strong>');
            $('#letter').css('color',"red");

        }

        //validate capital letter
        if ( pswd.match(/[A-Z]/) ) {
            $('#capital').html('');
        } else {
            $('#capital').html('Al menos debería tener <strong>una letra en mayúsculas</strong>');
            $('#capital').css('color',"red");

        }

        //validate number
        if ( pswd.match(/\d/) ) {
            $('#number').html('');
        } else {
            $('#number').html('Al menos debería tener <strong>un número</strong>');
            $('#number').css('color',"red");

        }

        if(/\s/.test(pswd)){
            $('#space').html('No puede contener  <strong>espacios vacios</strong>');
            $('#space').css('color',"red");
        }
        else
        {
            $('#space').html('');
        }

    }).focus(function() {
        $('#pswd_info').show();
    }).blur(function() {
        $('#pswd_info').hide();
    });
}