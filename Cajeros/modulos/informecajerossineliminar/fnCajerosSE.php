<?php
	include('../../data/Conexion.php');
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_COOKIE['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];
		
	// verifica si no se ha loggeado	
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");
	$opcion = $_POST['opcion'];
    function CORREGIRTEXTO($string)
    {
        $string = str_replace("REEMPLAZARNUMERAL", "#", $string);
        $string = str_replace("REEMPLAZARMAS", "+",$string);
        $string = str_replace("REEMPLAZARMENOS", "-", $string);
        return $string;
    }
    /*
    function CALCULARFECHA($fi,$d)
    {
        $conectar = mysqli_connect("localhost", "usrpavas", "9A12)WHFy$2p4v4s", "cajeros");
        $confec = mysqli_query($conectar,"select DATE_ADD('".$fi."', INTERVAL $d DAY) fecha from usuario LIMIT 1");
        $datofec = mysqli_fetch_array($confec);
        return $datofec['fecha'];
    }*/

function obtenerListaValores($valores)
{
    $seleccionados = explode(',',$valores);
    $num = count($seleccionados);
    $sqlIn = array();
    for($i = 0; $i < $num; $i++)
    {
        if($seleccionados[$i] != '')
        {
            $sqlIn[$i]=$seleccionados[$i];
        }
    }
    $valoresSQLIn = implode(',',$sqlIn);

    return $valoresSQLIn;
}

function obtenerSQL($campo, $valor)
{
    $cadenaSQL = "";

    if (trim($valor) == '') {
        //(caj_estado_proyecto = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') and
        $formato = "%s = '%s' OR '%s' IS NULL OR '%s' = ''";
        $cadenaSQL = sprintf($formato, $campo, $valor, $valor, $valor);
    } else {
        //c.caj_clave_int in (".$listacajeros.")
        $formato = "%s IN (%s)";
        $cadenaSQL = sprintf($formato, $campo, $valor);
    }

    return $cadenaSQL;
}

function obtenerItemSeleccionado($item, $valores)
{
    $seleccionados = explode(',',$valores);

    if (in_array($item, $seleccionados)) {
        return 'selected="selected"';
    }

    return '';
}

	if($opcion=="BUSCAR")
    {
        $ancho = $_POST['ancho'];
        $porc2  = $ancho * 0.04;

        $anchototal2 = $ancho - $porc2;
        $porc  = $ancho * 0.05;
        $anchototal = $anchototal2;// - $porc;

        ?>
        <fieldset name="Group1" style="width: <?php echo $anchototal2;?>px">
            <legend align="center">Informe Dinámico<br>
                <button name="Accion" value="excel" title="Exportar a Excel" onclick="EXPORTAR()" type="button" style="cursor:pointer;">
                    <img src="../../images/excel.png" height="25" width="25"></button>
            </legend>

            <div id="informacion" style="width: <?php echo $anchototal;?>px" >

        <table class="table table-bordered table-stripe dt-responsive"  id="tbBusquedaCajero"  style="font-size: 11px;" width="<?php echo $anchototal;?>px">
            <thead>
                <tr>
                    <th class="dt-head-center" data-priority="1"><strong>Cajero</strong></th>
                    <th class="dt-head-center" data-priority="1"><strong>Tipo Proyecto</strong></th>
                    <th class="dt-head-center" data-priority="1"><strong>Nombre</strong></th>
                    <th class="dt-head-center" data-priority="1"><strong>Dirección</strong></th>
                    <th class="dt-head-center" data-priority="1"><strong>Convenio</strong></th>
                    <th class="dt-head-center" data-priority="1"><strong>OT</strong></th>
                    <th class="dt-head-center" data-priority="1"><strong>Año Cont.</strong></th>
                    <th class="dt-head-center" data-priority="1"><strong>Región</strong></th>
                    <th class="dt-head-center" data-priority="1"><strong>Departamento</strong></th>
                    <th class="dt-head-center" data-priority="1"><strong>Municipio</strong></th>
                    <th class="dt-head-center" data-priority="1"><strong>Tipología</strong></th>
                    <th class="dt-head-center" data-priority="1"><strong>Tip. Intervención</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Modalidad</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Fec.Inicio</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Estado</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Código</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Cen.Cos</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Ref. Maquina</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Ubicación (SUC, CC, REMOTO)</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Cód. Suc.</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Ubicación ATM.</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Atiende</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Riesgo</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Area</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Fecha Apagado ATM</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Inmobiliaria</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Estado</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Fecha Inicio</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Fecha Entrega</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Total Dias</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Notas</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Visitante</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Estado</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Fecha Inicio</strong></th>
                    <th class="dt-head-center"  data-priority="2"><strong>Fecha Entrega</strong></th>
                    <th class="dt-head-center"  data-priority="2"><strong>Total Días</strong></th>
                    <th class="dt-head-center"  data-priority="2"><strong>Notas</strong></th>
                    <th class="dt-head-center"  data-priority="2"><strong>Diseñador</strong></th>
                    <th class="dt-head-center"  data-priority="2"><strong>Estado</strong></th>
                    <th class="dt-head-center"  data-priority="2"><strong>Fecha Inicio</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Fecha Entrega</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Total Días</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Notas</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Gestionador</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Estado</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Fecha Inicio</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Fecha Entrega</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Total Días</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Notas</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Interventor</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Operador Canal</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Fecha Inicio Obra</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Fecha Entrega</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Fecha Pedido Suministro</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Notas</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Constructor</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>%Avance</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Fecha Inicio</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Fecha Entrega</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Total Días</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Notas</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Informe Final</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Seguridad</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Cód. Monitoreo</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Fecha Ingreso Obra</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Notas</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Comite Aprobado</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Fec.Ini. Teorica Comite</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Fec.Ent Teorica Comite</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Fecha Comite</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Req.Diseño</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Req.Contrato</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Req.Canal</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Contrato Aprobado</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Estado Contrato</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Fec.Ini Teorica Contrato</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Fec.Ent Teorica Contrato</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Fecha Contrato</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Máquina</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Prefactibilidad Aprobada</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Fec.Ini Teorica Prefactibilidad</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Fec.Ent Teorica Prefactibilidad</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Fecha Prefactibilidad</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Fec.Ini Teorica Proyección</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Fec.Ent Teorica Proyección</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Fec.Ini Teorica Canal</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Fec.Ent Teorica Canal</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Fec.Ini Canal</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Fec.Ent Canal</strong></th>
                    <th class="dt-head-center" data-priority="2"><strong>Not.Generales</strong></th>
                    </tr>
                </thead>
            <tfoot>
            <tr>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong> </strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
                <th class="dt-head-center"><strong></strong></th>
            </tr>
            </tfoot>
             <tbody></tbody>
            </table>
                <script src="jsbusquedacajeros.js"></script>
            </div>
        </fieldset>


        
<?php

    }
    else if($opcion=="MOSTRARFILTRO")
    {
        $clabug = $_POST['clabug'];

        $conbug = mysqli_query($conectar,"select * from busqueda_guardada where bug_clave_int = " . $clabug . "");
        $datobug = mysqli_fetch_array($conbug);
        $ley = $datobug['bug_nombre'];
        $c1 = $datobug['c1'];
        $c2 = $datobug['c2'];
        $c3 = $datobug['c3'];
        $c4 = $datobug['c4'];
        $c5 = $datobug['c5'];
        $c6 = $datobug['c6'];
        $c7 = $datobug['c7'];
        $c8 = $datobug['c8'];
        $c9 = $datobug['c9'];
        $c10 = $datobug['c10'];
        $c11 = $datobug['c11'];
        $c12 = $datobug['c12'];
        $c13 = $datobug['c13'];
        $c14 = $datobug['c14'];
        $c15 = $datobug['c15'];
        $c16 = $datobug['c16'];
        $c17 = $datobug['c17'];
        $c18 = $datobug['c18'];
        $c19 = $datobug['c19'];
        $c20 = $datobug['c20'];
        $c21 = $datobug['c21'];
        $c22 = $datobug['c22'];
        $c23 = $datobug['c23'];
        $c24 = $datobug['c24'];
        $c25 = $datobug['c25'];
        $c26 = $datobug['c26'];
        $c27 = $datobug['c27'];
        $c28 = $datobug['c28'];
        $c29 = $datobug['c29'];
        $c30 = $datobug['c30'];
        $c31 = $datobug['c31'];
        $c32 = $datobug['c32'];
        $c33 = $datobug['c33'];
        $c34 = $datobug['c34'];
        $c35 = $datobug['c35'];
        $c36 = $datobug['c36'];
        $c37 = $datobug['c37'];
        $c38 = $datobug['c38'];
        $c39 = $datobug['c39'];
        $c40 = $datobug['c40'];
        $c41 = $datobug['c41'];
        $c42 = $datobug['c42'];
        $c43 = $datobug['c43'];
        $c44 = $datobug['c44'];
        $c45 = $datobug['c45'];
        $c46 = $datobug['c46'];
        $c47 = $datobug['c47'];
        $c48 = $datobug['c48'];
        $c49 = $datobug['c49'];
        $c50 = $datobug['c50'];
        $c51 = $datobug['c51'];
        $c52 = $datobug['c52'];
        $c53 = $datobug['c53'];
        $c54 = $datobug['c54'];
        $c55 = $datobug['c55'];
        $c56 = $datobug['c56'];
        $c57 = $datobug['c57'];
        $c58 = $datobug['c58'];
        $c59 = $datobug['c59'];
        $c60 = $datobug['c60'];
        $c61 = $datobug['c61'];
        $c62 = $datobug['c62'];
        $c63 = $datobug['c63'];
        $c64 = $datobug['c64'];
        $c65 = $datobug['c65'];
        $c66 = $datobug['c66'];
        $c67 = $datobug['c67'];
        $c68 = $datobug['c68'];
        $c69 = $datobug['c69'];
        $c70 = $datobug['c70'];
        $c71 = $datobug['c71'];
        $c72 = $datobug['c72'];
        $c73 = $datobug['c73'];
        $c74 = $datobug['c74'];
        $c75 = $datobug['c75'];
        $c76 = $datobug['c76'];
        $c77 = $datobug['c77'];
        $c78 = $datobug['c78'];
        $c79 = $datobug['c79'];
        $c80 = $datobug['c80'];
        $c81 = $datobug['c81'];
        $c82 = $datobug['c82'];
        $c83 = $datobug['c83'];
        $c84 = $datobug['c84'];
        $c85 = $datobug['c85'];
        $c86 = $datobug['c86'];
        $c87 = $datobug['c87'];
        $c88 = $datobug['c88'];
        $c89 = $datobug['c89'];
        $c90 = $datobug['c90'];
        $c91 = $datobug['c91'];
        $c92 = $datobug['c92'];
        $c93 = $datobug['c93'];
        $c94 = $datobug['c94'];
        $c95 = $datobug['c95'];
        $c96 = $datobug['c96'];
        $c97 = $datobug['c97'];
        $c98 = $datobug['c98'];
        $c99 = $datobug['c99'];
        $c100 = $datobug['c100'];
        $c101 = $datobug['c101'];
        $c102 = $datobug['c102'];
        $c103 = $datobug['c103'];
        $c104 = $datobug['c104'];
        $c105 = $datobug['c105'];
        $c106 = $datobug['c106'];
        $c107 = $datobug['c107'];
        $c108 = $datobug['c108'];
        $c109 = $datobug['c109'];
        $c110 = $datobug['c110'];
        $c111 = $datobug['c111'];
        $c112 = $datobug['c112'];
        $c113 = $datobug['c113'];
        $c114 = $datobug['c114'];
        $c115 = $datobug['c115'];
        $c116 = $datobug['c116'];
        $c117 = $datobug['c117'];
        $c118 = $datobug['c118'];
        $c119 = $datobug['c119'];
        $c120 = $datobug['c120'];
        $c121 = $datobug['c121'];
        $c122 = $datobug['c122'];
        $c123 = $datobug['c123'];
        $c124 = $datobug['c124'];
        $c125 = $datobug['c125'];
        $c126 = $datobug['c126'];
        $c127 = $datobug['c127'];
        $c128 = $datobug['c128'];
        $c129 = $datobug['c129'];
        $c130 = $datobug['c130'];
        $c131 = $datobug['c131'];
        $c132 = $datobug['c132'];
        $c133 = $datobug['c133'];
        $c134 = $datobug['c134'];
        $c135 = $datobug['c135'];
        $c136 = $datobug['c136'];
        $c137 = $datobug['c137'];
        $c138 = $datobug['c138'];
        $c139 = $datobug['c139'];
        $c140 = $datobug['c140'];
        $c141 = $datobug['c141'];
        $c142 = $datobug['c142'];
        $c143 = $datobug['c143'];
        $c144 = $datobug['c144'];
        $c145 = $datobug['c145'];
        $c146 = $datobug['c146'];
        $c147 = $datobug['c147'];
        $c148 = $datobug['c148'];
        $c149 = $datobug['c149'];
        $c150 = $datobug['c150'];
        $c151 = $datobug['c151'];
        $c152 = $datobug['c152'];
        $c153 = $datobug['c153'];
        $c154 = $datobug['c154'];
        $c155 = $datobug['c155'];
        $c156 = $datobug['c156'];
        $c157 = $datobug['c157'];
        $c158 = $datobug['c158'];
        $c159 = $datobug['c159'];
        $c160 = $datobug['c160'];
        $c161 = $datobug['c161'];
        $c162 = $datobug['c162'];
        $c163 = $datobug['c163'];
        $c164 = $datobug['c164'];
        $c165 = $datobug['c165'];
        $c166 = $datobug['c166'];
        $c167 = $datobug['c167'];
        $c168 = $datobug['c168'];
        $c169 = $datobug['c169'];
        $c170 = $datobug['c170'];
        $c171 = $datobug['c171'];
        $c172 = $datobug['c172'];
        $c173 = $datobug['c173'];
        $c174 = $datobug['c174'];
        $c175 = $datobug['c175'];
        $c176 = $datobug['c176'];
        $c177 = $datobug['c177'];
        $c178 = $datobug['c178'];
        $c179 = $datobug['c179'];
        $c180 = $datobug['c180'];
        $c181 = $datobug['c181'];
        $c182 = $datobug['c182'];
        $c183 = $datobug['c183'];
        $c184 = $datobug['c184'];
        $c185 = $datobug['c185'];
        $c186 = $datobug['c186'];
        $c187 = $datobug['c187'];
        $c188 = $datobug['c188'];
        $c189 = $datobug['c189'];
        $c190 = $datobug['c190'];
        $c191 = $datobug['c191'];
        $c192 = $datobug['c192'];
        $c193 = $datobug['c193'];
        $c194 = $datobug['c194'];
        $c195 = $datobug['c195'];
        $c196 = $datobug['c196'];
        $c197 = $datobug['c197'];
        $c198 = $datobug['c198'];
        $c199 = $datobug['c199'];
        $c200 = $datobug['c200'];
        $c201 = $datobug['c201'];
        $c202 = $datobug['c202'];
        $c203 = $datobug['c203'];
        $c204 = $datobug['c204'];
        $c205 = $datobug['c205'];
        $c206 = $datobug['c206'];
        $c207 = $datobug['c207'];
        $c208 = $datobug['c208'];
        $c209 = $datobug['c209'];
        $c210 = $datobug['c210'];
        $c211 = $datobug['c211'];
        $c212 = $datobug['c212'];
        $c213 = $datobug['c213'];
        $c214 = $datobug['c214'];
        $c215 = $datobug['c215'];
        $c216 = $datobug['c216'];
        $c217 = $datobug['c217'];
        $c218 = $datobug['c218'];
        $c219 = $datobug['c219'];
        $c220 = $datobug['c220'];
        $c221 = $datobug['c221'];
        $c222 = $datobug['c222'];
        $c223 = $datobug['c223'];
        $c224 = $datobug['c224'];
        $c225 = $datobug['c225'];
        $c226 = $datobug['c226'];//ver tipo proyecto
        $c227 = $datobug['c227'];//tipo proyecto

        $c228 = $datobug['c228'];
        $c229 = $datobug['c229'];
        $c230 = $datobug['c230'];

        $c231 = $datobug['c231'];
        $c232 = $datobug['c232'];//CONVENIO
        $c233 = $datobug['c233'];
        $c234 = $datobug['c234'];//OT
        $c235 = $datobug['c235'];
        $c236 = $datobug['c236'];//OT
        $c237 = $datobug['c237'];
        $c239 = $datobug['c239'];
        $c240 = $datobug['c240'];
        $c241 = $datobug['c241'];
        $c242 = $datobug['c242'];
        $c243 = $datobug['c243'];
        $c244 = $datobug['c244'];

        $listacajeros = obtenerListaValores($c1);

        if ($c122 == 0)//Info base
        {
            $c6 = '';
            $c7 = '';
            $c8 = '';
            $c9 = '';
            $c10 = '';
            $c11 = '';
            $c12 = '';
            $c227 = '';
            $c232 = '';
            $c234 = '';

            $c136 = 'false';
            $c137 = 'false';
            $c138 = 'false';
            $c139 = 'false';
            $c140 = 'false';
            $c141 = 'false';
            $c142 = 'false';
            $c226 = 'false';
            $c231 = 'false';
            $c233 = 'false';
            $c235 = '';
        }

        if ($c123 == 0)//Inmobiliaria
        {
            $c13 = '';
            $c14 = '';
            $c15 = '';
            $c16 = '';
            $c17 = '';
            $c18 = '';
            $c19 = '';
            $c20 = '';

            $c143 = 'false';
            $c144 = 'false';
            $c145 = 'false';
            $c146 = 'false';
            $c147 = 'false';
            $c148 = 'false';
            $c236 = 'false';
        }
        if ($c124 == 0)//Visita
        {
            $c21 = '';
            $c22 = '';
            $c23 = '';
            $c24 = '';
            $c25 = '';
            $c26 = '';
            $c27 = '';
            $c28 = '';

            $c149 = 'false';
            $c150 = 'false';
            $c151 = 'false';
            $c152 = 'false';
            $c153 = 'false';
            $c154 = 'false';
            $c237 = 'false';
        }
        if ($c125 == 0)//COMITE
        {
            $c29 = '';
            $c30 = '';
            $c31 = '';
            $c32 = '';
            $c33 = '';
            $c34 = '';
            $c35 = '';
            $c36 = '';
            $c37 = '';
            $c38 = '';
            $c39 = '';
            $c40 = '';
            $c41 = '';
            $c42 = '';
            $c43 = '';
            $c44 = '';
            $c45 = '';
            $c46 = '';
            $c47 = '';
            $c48 = '';
            $c49 = '';
            $c240 = '';
            $c242 = '';
            $c244 = '';

            $c155 = 'false';
            $c156 = 'false';
            $c157 = 'false';
            $c158 = 'false';
            $c159 = 'false';
            $c160 = 'false';
            $c161 = 'false';
            $c162 = 'false';
            $c163 = 'false';
            $c164 = 'false';
            $c165 = 'false';
            $c166 = 'false';
            $c167 = 'false';
            $c168 = 'false';
            $c169 = 'false';
            $c170 = 'false';
            $c171 = 'false';
            $c239 = 'false';
            $c241 = 'false';
            $c243 = 'false';
        }
        if ($c126 == 0)//CONTRATO
        {
            $c50 = '';
            $c51 = '';
            $c52 = '';
            $c53 = '';
            $c54 = '';
            $c55 = '';
            $c56 = '';
            $c57 = '';
            $c224 = '';

            $c172 = 'false';
            $c173 = 'false';
            $c174 = 'false';
            $c175 = 'false';
            $c176 = 'false';
            $c177 = 'false';
        }
        if ($c127 == 0)//DISENO
        {
            $c58 = '';
            $c59 = '';
            $c60 = '';
            $c61 = '';
            $c62 = '';
            $c63 = '';
            $c64 = '';
            $c65 = '';

            $c178 = 'false';
            $c179 = 'false';
            $c180 = 'false';
            $c181 = 'false';
            $c182 = 'false';
            $c183 = 'false';
        }
        if ($c128 == 0)//LICENCIA
        {
            $c66 = '';
            $c67 = '';
            $c68 = '';
            $c69 = '';
            $c70 = '';
            $c71 = '';
            $c72 = '';
            $c73 = '';

            $c184 = 'false';
            $c185 = 'false';
            $c186 = 'false';
            $c187 = 'false';
            $c188 = 'false';
            $c189 = 'false';
        }
        if ($c129 == 0)//PREFACTIBILIDAD
        {
            $c74 = '';
            $c75 = '';
            $c76 = '';
            $c77 = '';
            $c78 = '';
            $c79 = '';
            $c80 = '';
            $c81 = '';
            $c82 = '';

            $c190 = 'false';
            $c191 = 'false';
            $c192 = 'false';
            $c193 = 'false';
            $c194 = 'false';
            $c195 = 'false';
        }
        if ($c130 == 0)//PEDIDO MAQUINA
        {
            $c83 = '';
            $c84 = '';
            $c85 = '';

            $c196 = 'false';
            $c197 = 'false';
            $c198 = 'false';
        }
        if ($c131 == 0)//PROYECCION
        {
            $c86 = '';
            $c87 = '';
            $c88 = '';
            $c89 = '';
            $c90 = '';

            $c199 = 'false';
            $c200 = 'false';
            $c201 = 'false';
        }
        if ($c132 == 0)//CANAL
        {
            $c91 = '';
            $c92 = '';
            $c93 = '';
            $c94 = '';
            $c95 = '';
            $c96 = '';
            $c97 = '';
            $c98 = '';
            $c99 = '';

            $c202 = 'false';
            $c203 = 'false';
            $c204 = 'false';
            $c205 = 'false';
            $c206 = 'false';
        }
        if ($c133 == 0)//INTERVENTORIA
        {
            $c100 = '';
            $c101 = '';
            $c102 = '';
            $c103 = '';
            $c104 = '';
            $c105 = '';
            $c106 = '';
            $c107 = '';
            $c108 = '';

            $c207 = 'false';
            $c208 = 'false';
            $c209 = 'false';
            $c210 = 'false';
            $c211 = 'false';
            $c212 = 'false';
        }
        if ($c134 == 0)//CONSTRUCTOR
        {
            $c109 = '';
            $c110 = '';
            $c111 = '';
            $c112 = '';
            $c113 = '';
            $c114 = '';
            $c115 = '';
            $c116 = '';

            $c213 = 'false';
            $c214 = 'false';
            $c215 = 'false';
            $c216 = 'false';
            $c217 = 'false';
            $c218 = 'false';
            $c219 = 'false';
        }
        if ($c135 == 0)//SEGURIDAD
        {
            $c117 = '';
            $c118 = '';
            $c119 = '';
            $c120 = '';
            $c121 = '';

            $c220 = 'false';
            $c221 = 'false';
            $c222 = 'false';
            $c223 = 'false';
        }

        if ($c228 == 0) {
            $c229 = 'false';
            $c230 = '';
        }


        //**********************************************
        $listacajeros = $datobug['bug_cajeros'];

        $fii = $datobug['bug_fec_ini_d'];
        $ffi = $datobug['bug_fec_ini_h'];
        $fie = $datobug['bug_fec_ent_d'];
        $ffe = $datobug['bug_fec_ent_h'];
        if ($fii == '0000-00-00') {
            $fii = '';
        }
        if ($ffi == '0000-00-00') {
            $ffi = '';
        }
        if ($fie == '0000-00-00') {
            $fie = '';
        }
        if ($ffe == '0000-00-00') {
            $ffe = '';
        }
        $nomcaj = CORREGIRTEXTO($datobug['bug_nom']);
        $dircaj = CORREGIRTEXTO($datobug['bug_dir']);
        $est = $datobug['bug_est'];
        $anocon = $datobug['bug_ano_cont'];
        $reg = $datobug['bug_reg'];
        $dep = $datobug['bug_dep'];
        $mun = $datobug['bug_mun'];
        $tip = $datobug['bug_tip'];
        $tipint = $datobug['bug_tii'];
        $moda = $datobug['bug_moda'];
        if ($est == 0) {
            $est = '';
        }
        if ($anocon == 0) {
            $anocon = '';
        }
        if ($reg == 0) {
            $reg = '';
        }
        if ($dep == 0) {
            $dep = '';
        }
        if ($mun == 0) {
            $mun = '';
        }
        if ($tip == 0) {
            $tip = '';
        }
        if ($tipint == 0) {
            $tipint = '';
        }
        if ($moda == 0) {
            $moda = '';
        }

        $cod = $datobug['bug_cod'];
        $cencos = $datobug['bug_cen_cos'];
        $refmaq = $datobug['bug_ref_maq'];
        $ubi = $datobug['bug_ubi'];
        $codsuc = $datobug['bug_cod_suc'];
        $ubiatm = $datobug['bug_ubi_atm'];
        $ati = $datobug['bug_ati'];
        $rie = $datobug['bug_rie'];
        $are = $datobug['bug_area'];
        if ($cod == 0) {
            $cod = '';
        }
        if ($cencos == 0) {
            $cencos = '';
        }
        if ($refmaq == 0) {
            $refmaq = '';
        }
        if ($ubi == 0) {
            $ubi = '';
        }
        if ($ubiatm == 0) {
            $ubiatm = '';
        }
        if ($ati == 0) {
            $ati = '';
        }
        if ($rie == 0) {
            $rie = '';
        }
        if ($are == 0) {
            $are = '';
        }

        $nominm = $datobug['bug_inm'];
        $estinm = $datobug['bug_est_inm'];
        $feciniinmdesde = $datobug['bug_fec_ini_inm_d'];
        $feciniinmhasta = $datobug['bug_fec_ini_inm_h'];
        $fecentinmdesde = $datobug['bug_fec_ent_inm_d'];
        $fecentinmhasta = $datobug['bug_fec_ent_inm_h'];
        if ($feciniinmdesde == '0000-00-00') {
            $feciniinmdesde = '';
        }
        if ($feciniinmhasta == '0000-00-00') {
            $feciniinmhasta = '';
        }
        if ($fecentinmdesde == '0000-00-00') {
            $fecentinmdesde = '';
        }
        if ($fecentinmhasta == '0000-00-00') {
            $fecentinmhasta = '';
        }
        $diasinm = $datobug['bug_dias_inm'];
        $notinm = $datobug['bug_not_inm'];
        if ($nominm == 0) {
            $nominm = '';
        }
        if ($estinm == 0) {
            $estinm = '';
        }
        if ($diasinm == 0) {
            $diasinm = '';
        }

        $nomvis = $datobug['bug_sw_vis'];
        $estvis = $datobug['bug_est_vis'];
        $fecinivisdesde = $datobug['bug_fec_ini_vis_d'];
        $fecinivishasta = $datobug['bug_fec_ini_vis_h'];
        $fecentvisdesde = $datobug['bug_fec_ent_vis_d'];
        $fecentvishasta = $datobug['bug_fec_ent_vis_h'];
        if ($fecinivisdesde == '0000-00-00') {
            $fecinivisdesde = '';
        }
        if ($fecinivishasta == '0000-00-00') {
            $fecinivishasta = '';
        }
        if ($fecentvisdesde == '0000-00-00') {
            $fecentvisdesde = '';
        }
        if ($fecentvishasta == '0000-00-00') {
            $fecentvishasta = '';
        }
        $diasvis = $datobug['bug_dias_vis'];
        $notvis = $datobug['bug_not_vis'];
        if ($nomvis == 0) {
            $nomvis = '';
        }
        if ($estvis == 0) {
            $estvis = '';
        }
        if ($diasvis == 0) {
            $diasvis = '';
        }

        $nomdis = $datobug['bug_dis'];
        $estdis = $datobug['bug_est_dis'];
        $fecinidisdesde = $datobug['bug_fec_ini_dis_d'];
        $fecinidishasta = $datobug['bug_fec_ini_dis_h'];
        $fecentdisdesde = $datobug['bug_fec_ent_dis_d'];
        $fecentdishasta = $datobug['bug_fec_ent_dis_h'];
        if ($fecinidisdesde == '0000-00-00') {
            $fecinidisdesde = '';
        }
        if ($fecinidishasta == '0000-00-00') {
            $fecinidishasta = '';
        }
        if ($fecentdisdesde == '0000-00-00') {
            $fecentdisdesde = '';
        }
        if ($fecentdishasta == '0000-00-00') {
            $fecentdishasta = '';
        }
        $diasdis = $datobug['bug_dias_dis'];
        $notdis = $datobug['bug_not_dis'];
        if ($nomdis == 0) {
            $nomdis = '';
        }
        if ($estdis == 0) {
            $estdis = '';
        }
        if ($diasdis == 0) {
            $diasdis = '';
        }

        $nomges = $datobug['bug_lic'];
        $estges = $datobug['bug_est_lic'];
        $fecinigesdesde = $datobug['bug_fec_ini_lic_d'];
        $fecinigeshasta = $datobug['bug_fec_ini_lic_h'];
        $fecentgesdesde = $datobug['bug_fec_ent_lic_d'];
        $fecentgeshasta = $datobug['bug_fec_ent_lic_h'];
        if ($fecinigesdesde == '0000-00-00') {
            $fecinigesdesde = '';
        }
        if ($fecinigeshasta == '0000-00-00') {
            $fecinigeshasta = '';
        }
        if ($fecentgesdesde == '0000-00-00') {
            $fecentgesdesde = '';
        }
        if ($fecentgeshasta == '0000-00-00') {
            $fecentgeshasta = '';
        }
        $diasges = $datobug['bug_dias_lic'];
        $notges = $datobug['bug_not_lic'];
        if ($nomges == 0) {
            $nomges = '';
        }
        if ($estges == 0) {
            $estges = '';
        }
        if ($diasges == 0) {
            $diasges = '';
        }

        $nomint = $datobug['bug_int'];
        $cancom = $datobug['bug_ope_can'];
        $feciniintdesde = $datobug['bug_fec_ini_int_d'];
        $feciniinthasta = $datobug['bug_fec_ini_int_h'];
        $fecentintdesde = $datobug['bug_fec_ent_int_d'];
        $fecentinthasta = $datobug['bug_fec_ent_int_h'];
        $fecinipedintdesde = $datobug['bug_fec_ped_sum_d'];
        $fecinipedinthasta = $datobug['bug_fec_ped_sum_h'];
        if ($feciniintdesde == '0000-00-00') {
            $feciniintdesde = '';
        }
        if ($feciniinthasta == '0000-00-00') {
            $feciniinthasta = '';
        }
        if ($fecentintdesde == '0000-00-00') {
            $fecentintdesde = '';
        }
        if ($fecentinthasta == '0000-00-00') {
            $fecentinthasta = '';
        }
        if ($fecinipedintdesde == '0000-00-00') {
            $fecinipedintdesde = '';
        }
        if ($fecinipedinthasta == '0000-00-00') {
            $fecinipedinthasta = '';
        }
        $notint = $datobug['bug_not_int'];
        if ($nomint == 0) {
            $nomint = '';
        }
        if ($cancom == 0) {
            $cancom = '';
        }

        $nomcons = $datobug['bug_cons'];
        $porava = $datobug['bug_ava'];
        $feciniconsdesde = $datobug['bug_fec_ini_cons_d'];
        $feciniconshasta = $datobug['bug_fec_ini_cons_h'];
        $fecentconsdesde = $datobug['bug_fec_ent_cons_d'];
        $fecentconshasta = $datobug['bug_fec_ent_cons_h'];
        if ($feciniconsdesde == '0000-00-00') {
            $feciniconsdesde = '';
        }
        if ($feciniconshasta == '0000-00-00') {
            $feciniconshasta = '';
        }
        if ($fecentconsdesde == '0000-00-00') {
            $fecentconsdesde = '';
        }
        if ($fecentconshasta == '0000-00-00') {
            $fecentconshasta = '';
        }
        $diascons = $datobug['bug_dias_cons'];
        $notcons = $datobug['bug_not_cons'];
        if ($nomcons == 0) {
            $nomcons = '';
        }
        if ($porava == 0) {
            $porava = '';
        }
        if ($diascons == 0) {
            $diascons = '';
        }

        $nomseg = $datobug['bug_seg'];
        $codmon = $datobug['bug_cod_mon'];
        $fecinginiobra = $datobug['bug_fec_ing_seg_d'];
        $fecingfinobra = $datobug['bug_fec_ing_seg_h'];
        $notseg = $datobug['bug_not_seg'];
        if ($nomseg == 0) {
            $nomseg = '';
        }

        $swinfobasica = $datobug['bug_sw_ver_info_bas'];
        $swinfosecundaria = $datobug['bug_sw_ver_info_sec'];
        $swinmobiliaria = $datobug['bug_sw_ver_inm'];
        $swvisitalocal = $datobug['bug_sw_ver_vis'];
        $swdiseno = $datobug['bug_sw_ver_dis'];
        $swlicencia = $datobug['bug_sw_ver_lic'];
        $swinterventoria = $datobug['bug_sw_ver_int'];
        $swconstructor = $datobug['bug_sw_ver_cons'];
        $swseguridad = $datobug['bug_sw_ver_seg'];

        $vernombrecajero = $datobug['bug_sw_nom'];
        $verdireccion = $datobug['bug_sw_dir'];
        $verestadocajero = $datobug['bug_sw_est'];
        $veranocontable = $datobug['bug_sw_ano_cont'];
        $verregion = $datobug['bug_sw_reg'];
        $verdepartamento = $datobug['bug_sw_dep'];
        $vermunicipio = $datobug['bug_sw_mun'];
        $vertipologia = $datobug['bug_sw_tip'];
        $verintervencion = $datobug['bug_sw_tii'];
        $vermodalidad = $datobug['bug_sw_moda'];
        $vercodigocajero = $datobug['bug_sw_cod'];
        $vercentrocostos = $datobug['bug_sw_cen_cos'];
        $verreferencia = $datobug['bug_sw_ref_maq'];
        $verubicacion = $datobug['bug_sw_ubi'];
        $vercodigosuc = $datobug['bug_sw_cod_suc'];
        $verubicacionatm = $datobug['bug_sw_ubi_atm'];
        $veratiende = $datobug['bug_sw_ati'];
        $verriesgo = $datobug['bug_sw_rie'];
        $verarea = $datobug['bug_sw_area'];
        $verinmobiliaria = $datobug['bug_sw_inm'];
        $verestadoinm = $datobug['bug_sw_est_inm'];
        $verfechainicioinm = $datobug['bug_sw_fec_ini_inm'];
        $verfechaentregainm = $datobug['bug_sw_fec_ent_inm'];
        $vertotaldiasinm = $datobug['bug_sw_dias_inm'];
        $vernotasinm = $datobug['bug_sw_not_inm'];
        $vervisita = $datobug['bug_sw_vis'];
        $verestadovis = $datobug['bug_sw_est_vis'];
        $verfechainiciovis = $datobug['bug_sw_fec_ini_vis'];
        $verfechaentregavis = $datobug['bug_sw_fec_ent_vis'];
        $vertotaldiasvis = $datobug['bug_sw_dias_vis'];
        $vernotasvis = $datobug['bug_sw_not_vis'];
        $verdisenador = $datobug['bug_sw_dis'];
        $verestadodis = $datobug['bug_sw_est_dis'];
        $verfechainiciodis = $datobug['bug_sw_fec_ini_dis'];
        $verfechaentregadis = $datobug['bug_sw_fec_ent_dis'];
        $vertotaldiasdis = $datobug['bug_sw_dias_dis'];
        $vernotasdis = $datobug['bug_sw_not_dis'];
        $vergestionador = $datobug['bug_sw_lic'];
        $verestadolic = $datobug['bug_sw_est_lic'];
        $verfechainiciolic = $datobug['bug_sw_fec_ini_lic'];
        $verfechaentregalic = $datobug['bug_sw_fec_ent_lic'];
        $vertotaldiaslic = $datobug['bug_sw_dias_lic'];
        $vernotaslic = $datobug['bug_sw_not_lic'];
        $verinterventor = $datobug['bug_sw_int'];
        $veroperadorcanal = $datobug['bug_sw_ope_can'];
        $verfechainicioint = $datobug['bug_sw_fec_ini_int'];
        $verfechaentregaint = $datobug['bug_sw_fec_ent_int'];
        $verfechapedido = $datobug['bug_sw_fec_ped_sum'];
        $vernotasint = $datobug['bug_sw_not_int'];
        $verconstructor = $datobug['bug_sw_cons'];
        $veravance = $datobug['bug_sw_ava'];
        $verfechainiciocons = $datobug['bug_sw_fec_ini_cons'];
        $verfechaentregacons = $datobug['bug_sw_fec_ent_cons'];
        $vertotaldiascons = $datobug['bug_sw_dias_cons'];
        $vernotascons = $datobug['bug_sw_not_cons'];
        $verinffincons = $datobug['bug_sw_informe_final'];

        $verseguridad = $datobug['bug_sw_seg'];
        $vercodmon = $datobug['bug_sw_cod_mon'];
        $verfecingobra = $datobug['bug_sw_fec_ing_seg'];
        $vernotasseg = $datobug['bug_sw_not_seg'];

        if ($swinfobasica == 0) {
            $nomcaj = '';
            $dircaj = '';
            $est = '';
            $anocon = '';
            $reg = '';
            $dep = '';
            $mun = '';
            $tip = '';
            $tipint = '';
            $moda = '';
        }
        if ($swinfosecundaria == 0) {
            $cod = '';
            $cencos = '';
            $refmaq = '';
            $ubi = '';
            $codsuc = '';
            $ubiatm = '';
            $ati = '';
            $rie = '';
            $are = '';
            $fecapagadoatm = '';
        }
        if ($swinmobiliaria == 0) {
            $nominm = '';
            $estinm = '';
            $feciniinmdesde = '';
            $feciniinmhasta = '';
            $fecentinmdesde = '';
            $fecentinmhasta = '';
            $diasinm = '';
            $notinm = '';
        }
        if ($swvisitalocal == 0) {
            $nomvis = '';
            $estvis = '';
            $fecinivisdesde = '';
            $fecinivishasta = '';
            $fecentvisdesde = '';
            $fecentvishasta = '';
            $diasvis = '';
            $notvis = '';
        }
        if ($swdiseno == 0) {
            $nomdis = '';
            $estdis = '';
            $fecinidisdesde = '';
            $fecinidishasta = '';
            $fecentdisdesde = '';
            $fecentdishasta = '';
            $diasdis = '';
            $notdis = '';
        }
        if ($swlicencia == 0) {
            $nomges = '';
            $estges = '';
            $fecinigesdesde = '';
            $fecinigeshasta = '';
            $fecentgesdesde = '';
            $fecentgeshasta = '';
            $diasges = '';
            $notges = '';
        }
        if ($swinterventoria == 0) {
            $nomint = '';
            $cancom = '';
            $feciniintdesde = '';
            $feciniinthasta = '';
            $fecentintdesde = '';
            $fecentinthasta = '';
            $fecinipedintdesde = '';
            $fecinipedinthasta = '';
            $notint = '';
        }
        if ($swconstructor == 0) {
            $nomcons = '';
            $porava = '';
            $feciniconsdesde = '';
            $feciniconshasta = '';
            $fecentconsdesde = '';
            $fecentconshasta = '';
            $diascons = '';
            $notcons = '';
        }
        if ($swseguridad == 0) {
            $nomseg = '';
            $codmon = '';
            $fecinginiobra = '';
            $fecingfinobra = '';
            $notseg = '';

            $verseguridad = 0;
            $vercodmon = 0;
            $verfecingobra = 0;
            $vernotasseg = 0;
        }
        ?>
        <fieldset name="Group1">
            <legend align="center"><strong>FILTRO</strong></legend>
            <table style="width: 50%">
                <tr>
                    <td style="height: 31px;" class="alinearizq">
                        <table style="width: 90%">
                            <tr>
                                <td class="alinearizq"><strong>CAJERO:</strong></td>
                                <td>
                                    <select multiple="multiple" onchange="" name="buscajero" id="buscajero"
                                            style="width:320px">
                                        <?php
                                        $con = mysqli_query($conectar,"select * from cajero where caj_sw_eliminado = 0 order by caj_nombre");
                                        $num = mysqli_num_rows($con);
                                        for ($i = 0; $i < $num; $i++) {
                                            $dato = mysqli_fetch_array($con);
                                            $clave = $dato['caj_clave_int'];
                                            $nombre = $dato['caj_nombre'];
                                            ?>
                                            <option value="<?php echo $clave; ?>" <?php
                                            $seleccionados = explode(',', $c1);
                                            $num1 = count($seleccionados);
                                            $cajeros = array();
                                            for ($i1 = 0; $i1 < $num1; $i1++) {
                                                if ($seleccionados[$i1] == $clave) {
                                                    echo 'selected="selected"';
                                                }
                                            }
                                            ?>><?php echo $clave . " - " . $nombre; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </td>
                                <td class="alinearizq">

                                    <input name="verfecini" id="verfecini" type="checkbox" <?php if ($c231 == 'true') {
                                        echo 'checked="checked"';
                                    } ?> />
                                    <strong>FEC.INICIO:</strong></td>
                                <td><input name="fechainicio" id="fechainicio" value="<?php echo $c2; ?>"
                                           placeholder="Desde" onchange="" readonly tabindex="31"
                                           onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text"
                                           style="width: 140px;cursor:pointer"></td>
                                <td><input name="fechafin" id="fechafin" value="<?php echo $c3; ?>" placeholder="Hasta"
                                           onchange="" readonly tabindex="31"
                                           onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text"
                                           style="width: 140px;cursor:pointer"></td>
                                <td class="alinearizq"><strong>FEC.ENTREGA:</strong></td>
                                <td><input name="fechainicioentrega" id="fechainicioentrega" value="<?php echo $c4; ?>"
                                           placeholder="Desde" onchange="" readonly tabindex="31"
                                           onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text"
                                           style="width: 140px;cursor:pointer"></td>
                                <td><input name="fechafinentrega" id="fechafinentrega" value="<?php echo $c5; ?>"
                                           placeholder="Hasta" onchange="" readonly tabindex="31"
                                           onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text"
                                           style="width: 140px;cursor:pointer"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <hr>
                    </td>
                </tr>
                <tr>
                    <td style="height: 31px" class="alinearizq">
                        <fieldset name="Group1">
                            <legend><strong>INFORMACIÓN BÁSICA<input name="verinfobasica" id="verinfobasica"
                                                                     onclick="verfiltro('INFOBASICA')"
                                                                     style="<?php if ($c122 == 1) {
                                                                         echo 'display:none';
                                                                     } else {
                                                                         echo 'display:block';
                                                                     } ?>" type="checkbox"><input
                                            name="ocultarinfobasica" id="ocultarinfobasica"
                                            style="<?php if ($c122 == 1) {
                                                echo 'display:block';
                                            } else {
                                                echo 'display:none';
                                            } ?>" <?php if ($c122 == 1) {
                                        echo 'checked="checked"';
                                    } ?> onclick="ocultarfiltro('INFOBASICA')" type="checkbox"><input
                                            name="ocuinfobasica" id="ocuinfobasica" value="<?php if ($c122 == 1) {
                                        echo '1';
                                    } else {
                                        echo '0';
                                    } ?>" type="hidden"/></strong></legend>
                            <table style="width: 100%;<?php if ($c122 == 0) {
                                echo 'display:none';
                            } ?>" id="tabinfobasica">
                                <tr>
                                    <td style="width: 400px" class="alinearizq">Nombre:</td>
                                    <td style="width: 400px"><input name="vernombrecajero"
                                                                    id="vernombrecajero" <?php if ($c136 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/></td>
                                    <td colspan="4">
                                        <input name="busnombre" id="busnombre" value="<?php echo $c6; ?>" tabindex="1"
                                               class="auto-style5" type="text" style="width: 325px" size="20"></td>
                                    <td class="alinearizq">
                                        Dirección:
                                    </td>
                                    <td>
                                        <input name="verdireccion" id="verdireccion" <?php if ($c137 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td colspan="4">
                                        <input name="busdireccion" id="busdireccion" value="<?php echo $c7; ?>"
                                               tabindex="5" class="inputs" type="text" style="width: 390px"></td>
                                    <td>&nbsp;
                                        Tipo Proyecto:
                                    </td>
                                    <td>&nbsp;
                                        <input name="vertipoproyecto" id="vertipoproyecto" <?php if ($c226 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td>&nbsp;<select multiple="multiple" name="bustipoproyecto" id="bustipoproyecto"
                                                      class="inputs" style="width: 140px">
                                            <option value="">-Seleccione-</option>
                                            <?php
                                            $con = mysqli_query($conectar,"select * from tipo_proyecto order by tpr_nombre");
                                            $num = mysqli_num_rows($con);
                                            $seleccionados = explode(',', $c227);
                                            for ($i = 0; $i < $num; $i++) {
                                                $dato = mysqli_fetch_array($con);
                                                $clave = $dato['tpr_clave_int'];
                                                $nombre = $dato['tpr_nombre'];
                                                ?>
                                                <option value="<?php echo $clave; ?>" <?php if (in_array($clave, $seleccionados)) {
                                                    echo 'selected';
                                                } ?>><?php echo $nombre; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 400px" class="alinearizq">Estado
                                        Proyecto:
                                    </td>
                                    <td style="width: 400px"><input name="verestadocajero"
                                                                    id="verestadocajero" <?php if ($c138 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/></td>
                                    <td style="width: 168px">
                                        <select multiple="multiple" name="busestado" id="busestado"
                                                onchange="COLORESTADO()" tabindex="20" class="inputs"
                                                data-placeholder="-Seleccione-"
                                                style="width: 140px; color:green; font-weight:bold">
                                            <option value="">-Seleccione-</option>
                                            <option value="1" <?php echo obtenerItemSeleccionado(1, $c8); ?>>Activo
                                            </option>
                                            <option value="5" <?php echo obtenerItemSeleccionado(5, $c8); ?>>
                                                Programado
                                            </option>
                                            <option value="2" <?php echo obtenerItemSeleccionado(2, $c8); ?>>
                                                Suspendido
                                            </option>
                                            <option value="3" <?php echo obtenerItemSeleccionado(3, $c8); ?>>Entregado
                                            </option>
                                            <option value="4" <?php echo obtenerItemSeleccionado(4, $c8); ?>>Cancelado
                                            </option>
                                        </select></td>
                                    <td style="width: 98px" class="alinearizq">Año Contable:</td>
                                    <td style="width: 98px"><input name="veranocontable"
                                                                   id="veranocontable" <?php if ($c139 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/></td>
                                    <td style="width: 164px">
                                        <input name="busanocontable" id="busanocontable" value="<?php echo $c9; ?>"
                                               tabindex="2" class="inputs" type="text" style="width: 60px" size="20">
                                    </td>
                                    <td class="alinearizq">
                                        Región:
                                    </td>
                                    <td>
                                        <input name="verregion" id="verregion" <?php if ($c140 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td style="width: 159px">
                                        <select multiple="multiple" name="busregion" id="busregion"
                                                onchange="VERDEPARTAMENTOS(this.value)" tabindex="6"
                                                data-placeholder="-Seleccione-" class="inputs" style="width: 140px">
                                            <option value="">-Seleccione-</option>
                                            <?php
                                            $con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo IS NULL and pog_nieto IS NULL order by pog_nombre");
                                            $num = mysqli_num_rows($con);
                                            for ($i = 0; $i < $num; $i++) {
                                                $dato = mysqli_fetch_array($con);
                                                $clave = $dato['pog_clave_int'];
                                                $nombre = $dato['pog_nombre'];
                                                ?>
                                                <option value="<?php echo $clave; ?>" <?php echo obtenerItemSeleccionado($clave, obtenerListaValores($c10)); ?>><?php echo $nombre; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <td style="width: 74px" class="alinearizq">
                                        Departamento:
                                    </td>
                                    <td style="width: 74px">
                                        <input name="verdepartamento" id="verdepartamento" <?php if ($c141 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td>
                                        <div id="departamentos" style="float:left">
                                            <select multiple="multiple" name="busdepartamento" id="busdepartamento"
                                                    onchange="VERCIUDADES(this.value)" tabindex="7" class="inputs"
                                                    data-placeholder="-Seleccione-" style="width: 145px">
                                                <option value="">-Seleccione-</option>
                                                <?php
                                                $con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo IS NOT NULL and pog_nieto IS NULL order by pog_nombre");
                                                $num = mysqli_num_rows($con);
                                                for ($i = 0; $i < $num; $i++) {
                                                    $dato = mysqli_fetch_array($con);
                                                    $clave = $dato['pog_clave_int'];
                                                    $nombre = $dato['pog_nombre'];
                                                    ?>
                                                    <option value="<?php echo $clave; ?>" <?php echo obtenerItemSeleccionado($clave, obtenerListaValores($c11)); ?>><?php echo $nombre; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </td>
                                    <td class="alinearizq">
                                        Municipío:
                                    </td>
                                    <td>
                                        <input name="vermunicipio" id="vermunicipio" <?php if ($c142 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td>
                                        <div id="ciudades" style="float:left">
                                            <select multiple="multiple" name="busmunicipio" id="busmunicipio"
                                                    onchange="" tabindex="8" class="inputs"
                                                    data-placeholder="-Seleccione-" style="width: 140px">
                                                <option value="">-Seleccione-</option>
                                                <?php
                                                $con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo IS NULL and pog_nieto IS NOT NULL order by pog_nombre");
                                                $num = mysqli_num_rows($con);
                                                for ($i = 0; $i < $num; $i++) {
                                                    $dato = mysqli_fetch_array($con);
                                                    $clave = $dato['pog_clave_int'];
                                                    $nombre = $dato['pog_nombre'];
                                                    ?>
                                                    <option value="<?php echo $clave; ?>" <?php echo obtenerItemSeleccionado($clave, obtenerListaValores($c12)); ?>><?php echo $nombre; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Convenio:</td>

                                    <td>
                                        <input name="verconvenio" id="verconvenio" <?php if ($c233 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td>
                                        <select name="busconvenio" id="busconvenio" tabindex="20" class="inputs"
                                                data-placeholder="-Seleccione-"
                                                style="width: 60px; color:green; font-weight:bold">
                                            <option value=""></option>
                                            <option value="1" <?php if ($c232 == 1) {
                                                echo "selected";
                                            } ?>>NO
                                            </option>
                                            <option value="2" <?php if ($c232 == 2) {
                                                echo "selected";
                                            } ?>>SI
                                            </option>
                                        </select>
                                    </td>
                                    <td>OT:</td>

                                    <td>
                                        <input name="verot" id="verot" <?php if ($c235 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td>
                                        <input name="busot" id="busot" type="text" class="inputs" value="<?php echo $c234; ?>" tabindex="20" style="width: 60px; color:green; font-weight:bold"/>
                                    </td>

                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <hr>
                    </td>
                </tr>
                <tr>
                    <td style="height: 31px;" class="alinearizq">
                        <fieldset name="Group1">
                            <legend><strong>INMOBILIARIA<input name="verinmobiliaria" id="verinmobiliaria"
                                                               style="<?php if ($c123 == 1) {
                                                                   echo 'display:none';
                                                               } else {
                                                                   echo 'display:block';
                                                               } ?>" onclick="verfiltro('INMOBILIARIA')"
                                                               type="checkbox"><input name="ocultarinmobiliaria"
                                                                                      id="ocultarinmobiliaria"
                                                                                      style="<?php if ($c123 == 1) {
                                                                                          echo 'display:block';
                                                                                      } else {
                                                                                          echo 'display:none';
                                                                                      } ?>" <?php if ($c123 == 1) {
                                        echo 'checked="checked"';
                                    } ?> onclick="ocultarfiltro('INMOBILIARIA')" type="checkbox"><input
                                            name="ocuinmobiliaria" id="ocuinmobiliaria" value="<?php if ($c123 == 1) {
                                        echo '1';
                                    } else {
                                        echo '0';
                                    } ?>" type="hidden"/></strong></legend>
                            <table style="width: 100%;<?php if ($c123 == 0) {
                                echo 'display:none';
                            } ?>" id="tabinmobiliaria">
                                <tr>
                                    <td class="alinearizq" style="width: 90px">
                                        Nombre
                                        Inmobiliaria:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verinmobiliaria1"
                                               id="verinmobiliaria1" <?php if ($c143 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 6px">
                                        <select multiple="multiple" name="busnombreinmobiliaria"
                                                id="busnombreinmobiliaria" onchange="" tabindex="20" class="inputs"
                                                data-placeholder="-Seleccione-" style="width: 140px">
                                            <option value="">-Seleccione-</option>
                                            <?php
                                            $con = mysqli_query($conectar,"select * from usuario where (usu_categoria = 2 and usu_sw_inmobiliaria = 1 and usu_sw_activo = 1) OR (usu_clave_int NOT IN (select usu_clave_int from usuario_actor) and usu_sw_inmobiliaria = 1 and usu_sw_activo = 1) order by usu_nombre");
                                            $num = mysqli_num_rows($con);
                                            for ($i = 0; $i < $num; $i++) {
                                                $dato = mysqli_fetch_array($con);
                                                $clave = $dato['usu_clave_int'];
                                                $nombre = $dato['usu_nombre'];
                                                ?>
                                                <option value="<?php echo $clave; ?>" <?php echo obtenerItemSeleccionado($clave, obtenerListaValores($c13)); ?>><?php echo $nombre; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <td class="alinearizq" style="width: 81px">Estado:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verestadoinm" id="verestadoinm" <?php if ($c144 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 100px">
                                        <select multiple="multiple" name="busestadoinmobiliaria"
                                                id="busestadoinmobiliaria" onchange="" tabindex="17" class="inputs"
                                                style="width:148px">
                                            <option value="">-Seleccione-</option>
                                            <?php
                                            $con = mysqli_query($conectar,"select * from estado_inmobiliaria where esi_sw_activo = 1 order by esi_nombre");
                                            $num = mysqli_num_rows($con);
                                            for ($i = 0; $i < $num; $i++) {
                                                $dato = mysqli_fetch_array($con);
                                                $clave = $dato['esi_clave_int'];
                                                $nombre = $dato['esi_nombre'];
                                                ?>
                                                <option value="<?php echo $clave; ?>" <?php echo obtenerItemSeleccionado($clave, obtenerListaValores($c14)); ?>><?php echo $nombre; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <td class="alinearizq" style="width: 88px">
                                        Fecha Inicio:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verfechainicioinm"
                                               id="verfechainicioinm" <?php if ($c145 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 155px">
                                        <input name="busfechainicioinmobiliaria" id="busfechainicioinmobiliaria"
                                               value="<?php echo $c15; ?>" onchange="" placeholder="Desde" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                        <input name="busfechafininmobiliaria" id="busfechafininmobiliaria"
                                               value="<?php echo $c16; ?>" onchange="" placeholder="Hasta" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="alinearizq" style="width: 88px">
                                        Fecha Entrega:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verfechaentregainm"
                                               id="verfechaentregainm" <?php if ($c146 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 155px">
                                        <input name="busfechaentregainiinfoinmobiliaria"
                                               id="busfechaentregainiinfoinmobiliaria" value="<?php echo $c17; ?>"
                                               onchange="" placeholder="Desde" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                        <input name="busfechaentregafininfoinmobiliaria"
                                               id="busfechaentregafininfoinmobiliaria" value="<?php echo $c18; ?>"
                                               onchange="" placeholder="Hasta" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                    </td>
                                    <td class="alinearizq" style="width: 90px">Total Días:</td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="vertotaldiasinm" id="vertotaldiasinm" <?php if ($c147 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 6px">
                                        <input name="bustotaldiasinmobiliaria" id="bustotaldiasinmobiliaria"
                                               value="<?php echo $c19; ?>" tabindex="31" class="inputs" type="text"
                                               style="width: 140px">
                                    </td>
                                    <td class="alinearizq" style="width: 88px">
                                        Notas:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="vernotasinm" id="vernotasinm" <?php if ($c148 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 155px">
                                        <textarea cols="20" class="inputs" name="busnotasinmobiliaria"
                                                  id="busnotasinmobiliaria" rows="2"><?php echo $c20; ?></textarea></td>
                                </tr>
                                <tr>
                                    <td class="alinearizq" style="width: 90px">
                                        Ver Historico
                                        Inmobiliaria:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verhistoricoinm" id="verhistoricoinm" <?php if ($c236 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <hr>
                    </td>
                </tr>
                <tr>
                    <td style="height: 31px;" class="alinearizq">
                        <fieldset name="Group1">
                            <legend><strong>VISITA LOCAL<input name="vervisita" id="vervisita"
                                                               style="<?php if ($c124 == 1) {
                                                                   echo 'display:none';
                                                               } else {
                                                                   echo 'display:block';
                                                               } ?>" onclick="verfiltro('VISITA')"
                                                               type="checkbox"><input name="ocultarvisita"
                                                                                      id="ocultarvisita"
                                                                                      style="<?php if ($c124 == 1) {
                                                                                          echo 'display:block';
                                                                                      } else {
                                                                                          echo 'display:none';
                                                                                      } ?>" <?php if ($c123 == 1) {
                                        echo 'checked="checked"';
                                    } ?> onclick="ocultarfiltro('VISITA')" type="checkbox"><input name="ocuvisitalocal"
                                                                                                  id="ocuvisitalocal"
                                                                                                  value="<?php if ($c124 == 1) {
                                                                                                      echo '1';
                                                                                                  } else {
                                                                                                      echo '0';
                                                                                                  } ?>" type="hidden"/></strong>
                            </legend>
                            <table style="width: 100%;<?php if ($c124 == 0) {
                                echo 'display:none';
                            } ?>" id="tabvisita">
                                <tr>
                                    <td class="alinearizq" style="width: 90px">
                                        Nombre
                                        Visitante:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="vervisita1" id="vervisita1" <?php if ($c149 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox" style="text-decoration: underline"/>
                                    </td>
                                    <td class="alinearizq" style="width: 6px">
                                        <select multiple="multiple" name="busnombrevisita" id="busnombrevisita"
                                                onchange="" tabindex="20" class="inputs" data-placeholder="-Seleccione-"
                                                style="width: 140px">
                                            <option value="">-Seleccione-</option>
                                            <?php
                                            $con = mysqli_query($conectar,"select * from usuario where (usu_categoria = 2 and usu_sw_visita = 1 and usu_sw_activo = 1) OR (usu_clave_int NOT IN (select usu_clave_int from usuario_actor) and usu_sw_visita = 1 and usu_sw_activo = 1) order by usu_nombre");
                                            $num = mysqli_num_rows($con);
                                            for ($i = 0; $i < $num; $i++) {
                                                $dato = mysqli_fetch_array($con);
                                                $clave = $dato['usu_clave_int'];
                                                $nombre = $dato['usu_nombre'];
                                                ?>
                                                <option value="<?php echo $clave; ?>" <?php echo obtenerItemSeleccionado($clave, obtenerListaValores($c21)); ?>><?php echo $nombre; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <td class="alinearizq" style="width: 81px">Estado:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verestadovis" id="verestadovis" <?php if ($c150 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 100px">
                                        <select multiple="multiple" name="busestadovisita" id="busestadovisita"
                                                onchange="" tabindex="17" class="inputs" style="width:148px">
                                            <option value="">-Seleccione-</option>
                                            <option value="1" <?php echo obtenerItemSeleccionado(1, obtenerListaValores($c22)); ?>>
                                                Activo
                                            </option>
                                            <option value="2" <?php echo obtenerItemSeleccionado(2, obtenerListaValores($c22)); ?>>
                                                Entregado
                                            </option>
                                        </select>
                                    </td>
                                    <td class="alinearizq" style="width: 88px">
                                        Fecha Inicio:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verfechainiciovis"
                                               id="verfechainiciovis" <?php if ($c151 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 155px">
                                        <input name="busfechainiciovisita" id="busfechainiciovisita"
                                               value="<?php echo $c23; ?>" placeholder="Desde" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                        <input name="busfechafinvisita" id="busfechafinvisita"
                                               value="<?php echo $c24; ?>" placeholder="Hasta" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="alinearizq" style="width: 88px">
                                        Fecha Entrega:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verfechaentregavis"
                                               id="verfechaentregavis" <?php if ($c152 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 155px">
                                        <input name="busfechaentregainiinfovisita" id="busfechaentregainiinfovisita"
                                               value="<?php echo $c25; ?>" placeholder="Desde" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                        <input name="busfechaentregafininfovisita" id="busfechaentregafininfovisita"
                                               value="<?php echo $c26; ?>" placeholder="Hasta" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                    </td>
                                    <td class="alinearizq" style="width: 90px">Total Días:</td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="vertotaldiasvis" id="vertotaldiasvis" <?php if ($c153 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 6px">
                                        <input name="bustotaldiasvisita" id="bustotaldiasvisita"
                                               value="<?php echo $c27; ?>" tabindex="31" class="inputs" type="text"
                                               style="width: 140px">
                                    </td>
                                    <td class="alinearizq" style="width: 88px">
                                        Notas:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="vernotasvis" id="vernotasvis" <?php if ($c154 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 155px">
                                        <textarea cols="20" class="inputs" name="busnotasvisita" id="busnotasvisita"
                                                  rows="2"><?php echo $c28; ?></textarea></td>
                                </tr>
                                <tr>
                                    <td class="alinearizq" style="width: 90px">
                                        Ver Historico Visita:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verhistoricovis" id="verhistoricovis" <?php if ($c237 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <hr>
                    </td>
                </tr>
                <tr>
                    <td style="height: 31px;" class="alinearizq">
                        <fieldset name="Group1">
                            <legend><strong>COMITE<input name="vercomite" id="vercomite" style="<?php if ($c125 == 1) {
                                        echo 'display:none';
                                    } else {
                                        echo 'display:block';
                                    } ?>" onclick="verfiltro('COMITE')" type="checkbox"><input name="ocultarcomite"
                                                                                               id="ocultarcomite"
                                                                                               style="<?php if ($c125 == 1) {
                                                                                                   echo 'display:block';
                                                                                               } else {
                                                                                                   echo 'display:none';
                                                                                               } ?>" <?php if ($c125 == 1) {
                                        echo 'checked="checked"';
                                    } ?> onclick="ocultarfiltro('COMITE')" type="checkbox"><input name="ocucomite"
                                                                                                  id="ocucomite"
                                                                                                  value="<?php if ($c125 == 1) {
                                                                                                      echo '1';
                                                                                                  } else {
                                                                                                      echo '0';
                                                                                                  } ?>" type="hidden"/></strong>
                            </legend>
                            <table style="width: 100%;<?php if ($c125 == 0) {
                                echo 'display:none';
                            } ?>" id="tabcomite">
                                <tr>
                                    <td class="alinearizq" style="width: 90px">Código:</td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="vercodigocajero" id="vercodigocajero" <?php if ($c155 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 300px">
                                        <input name="buscodigo" id="buscodigo" value="<?php echo $c29; ?>" tabindex="2"
                                               class="inputs" type="text" style="width: 140px"></td>
                                    <td class="alinearizq" style="width: 80px">
                                        Centro Costos:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="vercentrocostos" id="vercentrocostos" <?php if ($c156 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 299px">
                                        <div id="micentrocostos">
                                            <select multiple="multiple" name="buscentrocostos" id="buscentrocostos"
                                                    tabindex="4" class="inputs" data-placeholder="-Seleccione-"
                                                    style="width: 140px">
                                                <option value="">-Seleccione-</option>
                                                <?php
                                                $con = mysqli_query($conectar,"select * from centrocostos where cco_sw_activo = 1 order by cco_nombre");
                                                $num = mysqli_num_rows($con);
                                                for ($i = 0; $i < $num; $i++) {
                                                    $dato = mysqli_fetch_array($con);
                                                    $clave = $dato['cco_clave_int'];
                                                    $nombre = $dato['cco_nombre'];
                                                    ?>
                                                    <option value="<?php echo $clave; ?>" <?php echo obtenerItemSeleccionado($clave, obtenerListaValores($c30)); ?>><?php echo $nombre; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </td>
                                    <td class="alinearizq" style="width: 88px">
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                    </td>
                                    <td class="alinearizq" style="width: 155px">

                                    </td>
                                </tr>
                                <tr>
                                    <td class="alinearizq" style="width: 90px">Ubicación
                                        (SUC, CC, REMOTO):
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verubicacion" id="verubicacion" <?php if ($c157 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 300px">
                                        <div id="miubicacion">
                                            <select multiple="multiple" name="busubicacion" id="busubicacion"
                                                    tabindex="11" class="inputs" data-placeholder="-Seleccione-"
                                                    style="width: 140px">
                                                <option value="">-Seleccione-</option>
                                                <?php
                                                $con = mysqli_query($conectar,"select * from ubicacion where ubi_sw_activo = 1 order by ubi_descripcion");
                                                $num = mysqli_num_rows($con);
                                                for ($i = 0; $i < $num; $i++) {
                                                    $dato = mysqli_fetch_array($con);
                                                    $clave = $dato['ubi_clave_int'];
                                                    $nombre = $dato['ubi_descripcion'];
                                                    ?>
                                                    <option value="<?php echo $clave; ?>" <?php echo obtenerItemSeleccionado($clave, obtenerListaValores($c32)); ?>><?php echo $nombre; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </td>
                                    <td class="alinearizq" style="width: 80px">Código de
                                        Suc:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="vercodigosuc" id="vercodigosuc" <?php if ($c158 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 299px">
                                        <input name="buscodigosuc" id="buscodigosuc" value="<?php echo $c33; ?>"
                                               tabindex="12" class="inputs" type="text" style="width: 140px"></td>
                                    <td class="alinearizq" style="width: 88px">
                                        Notas:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="vernotascom" id="vernotascom" <?php if ($c159 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 155px">
                                        <textarea name="busnotascomite" id="busnotascomite" class="inputs" cols="20"
                                                  rows="2"><?php echo $c31; ?></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="alinearizq" style="width: 90px">Atiende:</td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="veratiende" id="veratiende" <?php if ($c160 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 300px">
                                        <div id="miatiende">
                                            <select multiple="multiple" name="busatiende" id="busatiende" tabindex="14"
                                                    class="inputs" data-placeholder="-Seleccione-" style="width: 140px">
                                                <option value="">-Seleccione-</option>
                                                <?php
                                                $con = mysqli_query($conectar,"select * from atiende where ati_sw_activo = 1 order by ati_nombre");
                                                $num = mysqli_num_rows($con);
                                                for ($i = 0; $i < $num; $i++) {
                                                    $dato = mysqli_fetch_array($con);
                                                    $clave = $dato['ati_clave_int'];
                                                    $nombre = $dato['ati_nombre'];
                                                    ?>
                                                    <option value="<?php echo $clave; ?>" <?php echo obtenerItemSeleccionado($clave, obtenerListaValores($c35)); ?>><?php echo $nombre; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </td>
                                    <td class="alinearizq" style="width: 80px">Riesgo:</td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verriesgo" id="verriesgo" <?php if ($c161 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 299px">
                                        <select multiple="multiple" name="busriesgo" id="busriesgo" tabindex="15"
                                                class="inputs" style="width: 140px">
                                            <option value="">-Seleccione-</option>
                                            <option value="1" <?php echo obtenerItemSeleccionado(1, obtenerListaValores($c36)); ?>>
                                                ALTO
                                            </option>
                                            <option value="2" <?php echo obtenerItemSeleccionado(2, obtenerListaValores($c36)); ?>>
                                                BAJO
                                            </option>
                                            <option value="0" <?php echo obtenerItemSeleccionado(0, obtenerListaValores($c36)); ?>>
                                                N/A
                                            </option>
                                        </select></td>
                                    <td class="alinearizq" style="width: 88px">
                                        Ubicación ATM:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verubicacionatm" id="verubicacionatm" <?php if ($c162 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 155px">
                                        <select multiple="multiple" name="busubicacionamt" id="busubicacionamt"
                                                tabindex="13" class="inputs" style="width: 140px">
                                            <option value="">-Seleccione-</option>
                                            <option value="1" <?php echo obtenerItemSeleccionado(1, obtenerListaValores($c34)); ?>>
                                                EXTERNO
                                            </option>
                                            <option value="2" <?php echo obtenerItemSeleccionado(2, obtenerListaValores($c34)); ?>>
                                                INTERNO
                                            </option>
                                            <option value="0" <?php echo obtenerItemSeleccionado(0, obtenerListaValores($c34)); ?>>
                                                N/A
                                            </option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="alinearizq" style="width: 90px">
                                        Fecha Apagado ATM:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verfechaapagado" id="verfechaapagado" <?php if ($c163 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 300px">
                                        <input name="busfechainiapagadoatm" id="busfechainiapagadoatm"
                                               value="<?php echo $c37; ?>" placeholder="Desde" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px"><br>
                                        <input name="busfechafinapagadoatm" id="busfechafinapagadoatm"
                                               value="<?php echo $c38; ?>" placeholder="Hasta" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                    </td>
                                    <td class="alinearizq" style="width: 80px">
                                        Aprobado:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="vercomiteaprobado"
                                               id="vercomiteaprobado" <?php if ($c164 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/></td>
                                    <td class="alinearizq" style="width: 299px">
                                        Si<input name="comiteaprobadobancolombia"
                                                 id="comiteaprobadobancolombia" <?php if ($c39 == 1) {
                                            echo 'checked="checked"';
                                        } ?> value="1" tabindex="18" type="radio">
                                        No<input name="comiteaprobadobancolombia" title="<?php echo $c39; ?>"
                                                 id="comiteaprobadobancolombia" <?php if ($c39 == 0 and $c39 != '' and $c39 != NULL) {
                                            echo 'checked="checked"';
                                        } ?> value="0" tabindex="19" type="radio">
                                    </td>
                                    <td class="alinearizq borrar" style="width: 88px">
                                        Area:
                                    </td>
                                    <td class="alinearizq borrar" style="width: 5px">
                                        <input name="verarea" id="verarea" <?php if ($c165 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/></td>
                                    <td class="alinearizq" style="width: 155px">
                                        <div id="miarea">
                                            <select multiple="multiple" name="busarea" id="busarea" tabindex="16"
                                                    class="inputs" data-placeholder="-Seleccione-" style="width: 140px">
                                                <option value="">-Seleccione-</option>
                                                <?php
                                                $con = mysqli_query($conectar,"select * from area where are_sw_activo = 1 order by are_nombre");
                                                $num = mysqli_num_rows($con);
                                                for ($i = 0; $i < $num; $i++) {
                                                    $dato = mysqli_fetch_array($con);
                                                    $clave = $dato['are_clave_int'];
                                                    $nombre = $dato['are_nombre'];
                                                    ?>
                                                    <option value="<?php echo $clave; ?>" <?php echo obtenerItemSeleccionado($clave, obtenerListaValores($c40)); ?>><?php echo $nombre; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="alinearizq" style="width: 90px">
                                        Tipología:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="vertipologia" id="vertipologia" <?php if ($c166 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 300px">
                                        <div id="mitipologia">
                                            <select multiple="multiple" name="bustipologia" id="bustipologia"
                                                    onchange="" tabindex="9" data-placeholder="-Seleccione-"
                                                    class="inputs" style="width: 140px">
                                                <option value="">-Seleccione-</option>
                                                <?php
                                                $con = mysqli_query($conectar,"select * from tipologia where tip_sw_activo = 1 order by tip_nombre");
                                                $num = mysqli_num_rows($con);
                                                for ($i = 0; $i < $num; $i++) {
                                                    $dato = mysqli_fetch_array($con);
                                                    $clave = $dato['tip_clave_int'];
                                                    $nombre = $dato['tip_nombre'];
                                                    ?>
                                                    <option value="<?php echo $clave; ?>" <?php echo obtenerItemSeleccionado($clave, obtenerListaValores($c41)); ?>><?php echo $nombre; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </td>
                                    <td class="alinearizq" style="width: 80px">
                                        Tipo
                                        Intervención:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verintervencion" id="verintervencion" <?php if ($c167 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 299px">
                                        <div id="mitipointervencion">
                                            <select multiple="multiple" name="bustipointervencion"
                                                    id="bustipointervencion" onchange="VERMODALIDADES(this.value)"
                                                    tabindex="17" data-placeholder="-Seleccione-" class="inputs"
                                                    style="width: 140px">
                                                <option value="">-Seleccione-</option>
                                                <?php
                                                $con = mysqli_query($conectar,"select * from tipointervencion order by tii_nombre");
                                                $num = mysqli_num_rows($con);
                                                for ($i = 0; $i < $num; $i++) {
                                                    $dato = mysqli_fetch_array($con);
                                                    $clave = $dato['tii_clave_int'];
                                                    $nombre = $dato['tii_nombre'];
                                                    ?>
                                                    <option value="<?php echo $clave; ?>" <?php echo obtenerItemSeleccionado($clave, obtenerListaValores($c42)); ?>><?php echo $nombre; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </td>
                                    <td class="alinearizq" style="width: 88px">
                                        Modalidad:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="vermodalidad" id="vermodalidad" <?php if ($c168 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 155px">
                                        <div id="modalidades" style="float:left">
                                            <select multiple="multiple" name="busmodalidad" id="busmodalidad"
                                                    onchange="" tabindex="8" class="inputs"
                                                    data-placeholder="-Seleccione-" style="width: 140px">
                                                <option value="">-Seleccione-</option>
                                                <?php
                                                $con = mysqli_query($conectar,"select * from modalidad order by mod_nombre");
                                                $num = mysqli_num_rows($con);
                                                for ($i = 0; $i < $num; $i++) {
                                                    $dato = mysqli_fetch_array($con);
                                                    $clave = $dato['mod_clave_int'];
                                                    $nombre = $dato['mod_nombre'];
                                                    ?>
                                                    <option value="<?php echo $clave; ?>" <?php echo obtenerItemSeleccionado($clave, obtenerListaValores($c43)); ?>><?php echo $nombre; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div id="modalidadactor">
                                            <!-- VALIDACIONES DE FECHAS CON BASE A LA MODALIDAD -->
                                            <input name="modainm" id="modainm" value="0" type="hidden"/>
                                            <input name="diasinm" id="diasinm" value="0" type="hidden"/>
                                            <input name="modavis" id="modavis" value="0" type="hidden"/>
                                            <input name="diasvis" id="diasvis" value="0" type="hidden"/>
                                            <input name="modacom" id="modacom" value="0" type="hidden"/>
                                            <input name="diascom" id="diascom" value="0" type="hidden"/>
                                            <input name="modacon" id="modacon" value="0" type="hidden"/>
                                            <input name="diascon" id="diascon" value="0" type="hidden"/>
                                            <input name="modadis" id="modadis" value="0" type="hidden"/>
                                            <input name="diasdis" id="diasdis" value="0" type="hidden"/>
                                            <input name="modalic" id="modalic" value="0" type="hidden"/>
                                            <input name="diaslic" id="diaslic" value="0" type="hidden"/>
                                            <input name="modapre" id="modapre" value="0" type="hidden"/>
                                            <input name="diaspre" id="diaspre" value="0" type="hidden"/>
                                            <input name="modaint" id="modaint" value="0" type="hidden"/>
                                            <input name="modacons" id="modacons" value="0" type="hidden"/>
                                            <input name="diascons" id="diascons" value="0" type="hidden"/>
                                            <input name="modaseg" id="modaseg" value="0" type="hidden"/>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="alinearizq borrar" style="width: 90px">
                                        Fecha Inicio Teorica:
                                    </td>
                                    <td class="alinearizq borrar" style="width: 5px">
                                        <input name="verfechainicioteoricacomite"
                                               id="verfechainicioteoricacomite" <?php if ($c169 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 300px">
                                        <input name="fechainicioteoricacomitebancolombia"
                                               id="fechainicioteoricacomitebancolombia" value="<?php echo $c44; ?>"
                                               tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)"
                                               class="inputs" type="text" style="width: 140px"><br>
                                        <input name="fechafinalteoricacomitebancolombia"
                                               id="fechafinalteoricacomitebancolombia" value="<?php echo $c45; ?>"
                                               tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)"
                                               class="inputs" type="text" style="width: 140px">
                                    </td>
                                    <td class="alinearizq borrar" style="width: 80px">
                                        Fecha Entrega Teorica:
                                    </td>
                                    <td class="alinearizq borrar" style="width: 5px">
                                        <input name="verfechaentregateoricacomite"
                                               id="verfechaentregateoricacomite" <?php if ($c170 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/></td>
                                    <td class="alinearizq" style="width: 299px">
                                        <input name="fechaentregainiteoricacomitebancolombia"
                                               id="fechaentregainiteoricacomitebancolombia" value="<?php echo $c46; ?>"
                                               tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)"
                                               class="inputs" type="text" style="width: 140px"><br>
                                        <input name="fechaentregafinteoricacomitebancolombia"
                                               id="fechaentregafinteoricacomitebancolombia" value="<?php echo $c47; ?>"
                                               tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)"
                                               class="inputs" type="text" style="width: 140px">
                                    </td>
                                    <td class="alinearizq borrar" style="width: 88px">
                                        Fecha Comite:
                                    </td>
                                    <td class="alinearizq borrar" style="width: 5px">
                                        <input name="verfechacomite" id="verfechacomite" <?php if ($c171 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/></td>
                                    <td class="alinearizq" style="width: 155px">
                                        <input name="fechainicomitebancolombia" id="fechainicomitebancolombia"
                                               value="<?php echo $c48; ?>" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px"><br>
                                        <input name="fechafincomitebancolombia" id="fechafincomitebancolombia"
                                               value="<?php echo $c49; ?>" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="alinearizq" style="width: 80px">
                                        Requiere diseño:</td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verrequierediseno" id="verrequierediseno" <?php if($c239 == 'true'){ echo 'checked="checked"'; } ?> type="checkbox"  /></td>
                                    <td class="alinearizq" style="width: 299px">
                                        Si<input name="requierediseno" id="requierediseno" <?php if($c240 == 1){ echo 'checked="checked"'; } ?> value="1" tabindex="18" type="radio">
                                        No<input name="requierediseno" title="<?php echo $c240;?>" id="requierediseno" <?php if($c240 == 0 and $c240!=""){ echo 'checked="checked"'; } ?> value="0" tabindex="19" type="radio">
                                    </td>
                                    <td class="alinearizq" style="width: 80px">
                                        Requiere contrato:</td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verrequierecontrato" id="verrequierecontrato" <?php if($c241 == 'true'){ echo 'checked="checked"'; } ?> type="checkbox"  /></td>
                                    <td class="alinearizq" style="width: 299px">
                                        Si<input name="requierecontrato" id="requierecontrato" <?php if($c242 == 1){ echo 'checked="checked"'; } ?> value="1" tabindex="18" type="radio">
                                        No<input name="requierecontrato" title="<?php echo $c242;?>" id="requierecontrato" <?php if($c242 == 0 and $c242!=""){ echo 'checked="checked"'; } ?> value="0" tabindex="19" type="radio">
                                    </td>
                                    <td class="alinearizq" style="width: 80px">
                                        Requiere Canal:</td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verrequierecanal" id="verrequierecanal" <?php if($c243 == 'true'){ echo 'checked="checked"'; } ?> type="checkbox"  /></td>
                                    <td class="alinearizq" style="width: 299px">
                                        Si<input name="requierecanal" id="requierecanal" <?php if($c244 == 1){ echo 'checked="checked"'; } ?> value="1" tabindex="18" type="radio">
                                        No<input name="requierecanal" title="<?php echo $c244;?>" id="requierecanal" <?php if($c244 == 0 and $c244!=""){ echo 'checked="checked"'; } ?> value="0" tabindex="19" type="radio">
                                    </td
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <hr>
                    </td>
                </tr>
                <tr>
                    <td style="height: 31px;" class="alinearizq">
                        <fieldset name="Group1">
                            <legend><strong>CONTRATO<input name="vercontrato" id="vercontrato"
                                                           style="<?php if ($c126 == 1) {
                                                               echo 'display:none';
                                                           } else {
                                                               echo 'display:block';
                                                           } ?>" onclick="verfiltro('CONTRATO')" type="checkbox"><input
                                            name="ocultarcontrato" id="ocultarcontrato" style="<?php if ($c126 == 1) {
                                        echo 'display:block';
                                    } else {
                                        echo 'display:none';
                                    } ?>" <?php if ($c126 == 1) {
                                        echo 'checked="checked"';
                                    } ?> onclick="ocultarfiltro('CONTRATO')" type="checkbox"><input name="ocucontrato"
                                                                                                    id="ocucontrato"
                                                                                                    value="<?php if ($c126 == 1) {
                                                                                                        echo '1';
                                                                                                    } else {
                                                                                                        echo '0';
                                                                                                    } ?>"
                                                                                                    type="hidden"/></strong>
                            </legend>
                            <table style="width: 100%;<?php if ($c126 == 0) {
                                echo 'display:none';
                            } ?>" id="tabcontrato" class="borrar">
                                <tr>
                                    <td class="alinearizq" style="width: 90px">Aprobado:</td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="vercontratoaprobado"
                                               id="vercontratoaprobado" <?php if ($c172 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/></td>
                                    <td class="alinearizq" style="width: 300px">
                                        Si<input name="contratoaprobadobancolombia"
                                                 id="contratoaprobadobancolombia" <?php if ($c50 == 1) {
                                            echo 'checked="checked"';
                                        } ?> value="1" tabindex="18" type="radio">
                                        No<input name="contratoaprobadobancolombia"
                                                 id="contratoaprobadobancolombia" <?php if ($c50 == 0) {
                                            echo 'checked="checked"';
                                        } ?> value="0" tabindex="19" type="radio">
                                    </td>
                                    <td class="alinearizq" style="width: 80px">
                                        Estado:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verestadocontrato"
                                               id="verestadocontrato" <?php if ($c173 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/></td>
                                    <td class="alinearizq" style="width: 300px">
                                        <select multiple="multiple" name="busestadocontratobancolombia"
                                                id="busestadocontratobancolombia" tabindex="15" class="inputs"
                                                style="width: 140px">
                                            <option value="">-Seleccione-</option>
                                            <?php
                                            $con = mysqli_query($conectar,"select * from bancolombia_estado order by bes_nombre");
                                            $num = mysqli_num_rows($con);
                                            for ($i = 0; $i < $num; $i++) {
                                                $dato = mysqli_fetch_array($con);
                                                $clave = $dato['bes_clave_int'];
                                                $nombre = $dato['bes_nombre'];
                                                ?>
                                                <option value="<?php echo $clave; ?>" <?php echo obtenerItemSeleccionado($clave, obtenerListaValores($c51)); ?>><?php echo $nombre; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <td class="alinearizq" style="width: 88px">
                                        Fecha Inicio Teorica:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verfechainicioteoricacontrato"
                                               id="verriesgo11" <?php if ($c174 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/></td>
                                    <td class="alinearizq" style="width: 90px">
                                        <input name="fechainicioteoricacontratobancolombia"
                                               id="fechainicioteoricacontratobancolombia" value="<?php echo $c52; ?>"
                                               tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)"
                                               class="inputs" type="text" style="width: 140px"><br>
                                        <input name="fechainiciofinteoricacontratobancolombia"
                                               id="fechainiciofinteoricacontratobancolombia" value="<?php echo $c53; ?>"
                                               tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)"
                                               class="inputs" type="text" style="width: 140px">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="alinearizq" style="width: 90px">
                                        Fecha Entrega Teorica:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verfechaentregateoricacontrato"
                                               id="verfechaentregateoricacontrato" <?php if ($c175 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/></td>
                                    <td class="alinearizq" style="width: 300px">
                                        <input name="fechaentregainiteoricacontratobancolombia"
                                               id="fechaentregainiteoricacontratobancolombia"
                                               value="<?php echo $c54; ?>" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px"><br>
                                        <input name="fechaentregafinteoricacontratobancolombia"
                                               id="fechaentregafinteoricacontratobancolombia"
                                               value="<?php echo $c55; ?>" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                    </td>
                                    <td class="alinearizq" style="width: 80px">
                                        Fecha Entrega:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verfechaentregacontrato"
                                               id="verfechaentregacontrato" <?php if ($c176 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/></td>
                                    <td class="alinearizq" style="width: 300px">
                                        <input name="fechaentregainicontratobancolombia"
                                               id="fechaentregainicontratobancolombia" value="<?php echo $c56; ?>"
                                               tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)"
                                               class="inputs" type="text" style="width: 140px"><br>
                                        <input name="fechaentregafincontratobancolombia"
                                               id="fechaentregafincontratobancolombia" value="<?php echo $c57; ?>"
                                               tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)"
                                               class="inputs" type="text" style="width: 140px">
                                    </td>
                                    <td class="alinearizq borrar" style="width: 88px">
                                        Notas:
                                    </td>
                                    <td class="alinearizq borrar" style="width: 5px">
                                        <input name="vernotascontrato"
                                               id="vernotascontrato" <?php if ($c177 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/></td>
                                    <td class="alinearizq" style="width: 90px">
                                        <textarea name="notacontrato" id="notacontrato" class="inputs" cols="20"
                                                  rows="2"><?php echo $c224; ?></textarea></td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <hr>
                    </td>
                </tr>
                <tr>
                    <td style="height: 31px;" class="alinearizq">
                        <fieldset name="Group1">
                            <legend><strong>DISEÑO<input name="verdiseno" id="verdiseno" style="<?php if ($c127 == 1) {
                                        echo 'display:none';
                                    } else {
                                        echo 'display:block';
                                    } ?>" onclick="verfiltro('DISENO')" type="checkbox"><input name="ocultardiseno"
                                                                                               id="ocultardiseno"
                                                                                               style="<?php if ($c127 == 1) {
                                                                                                   echo 'display:block';
                                                                                               } else {
                                                                                                   echo 'display:none';
                                                                                               } ?>" <?php if ($c127 == 1) {
                                        echo 'checked="checked"';
                                    } ?> onclick="ocultarfiltro('DISENO')" type="checkbox"><input name="ocudiseno"
                                                                                                  id="ocudiseno"
                                                                                                  value="<?php if ($c127 == 1) {
                                                                                                      echo '1';
                                                                                                  } else {
                                                                                                      echo '0';
                                                                                                  } ?>" type="hidden"/></strong>
                            </legend>
                            <table style="width: 100%;<?php if ($c127 == 0) {
                                echo 'display:none';
                            } ?>" id="tabdiseno">
                                <tr>
                                    <td class="alinearizq" style="width: 90px">
                                        Nombre
                                        Diseñador:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verdisenador" id="verdisenador" <?php if ($c178 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 6px">
                                        <select multiple="multiple" name="busnombrediseno" id="busnombrediseno"
                                                onchange="" tabindex="20" class="inputs" data-placeholder="-Seleccione-"
                                                style="width: 140px">
                                            <option value="">-Seleccione-</option>
                                            <?php
                                            $con = mysqli_query($conectar,"select * from usuario where (usu_categoria = 2 and usu_sw_diseno = 1 and usu_sw_activo = 1) OR (usu_clave_int NOT IN (select usu_clave_int from usuario_actor) and usu_sw_diseno = 1 and usu_sw_activo = 1) order by usu_nombre");
                                            $num = mysqli_num_rows($con);
                                            for ($i = 0; $i < $num; $i++) {
                                                $dato = mysqli_fetch_array($con);
                                                $clave = $dato['usu_clave_int'];
                                                $nombre = $dato['usu_nombre'];
                                                ?>
                                                <option value="<?php echo $clave; ?>" <?php echo obtenerItemSeleccionado($clave, obtenerListaValores($c58)); ?>><?php echo $nombre; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <td class="alinearizq" style="width: 81px">Estado:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verestadodis" id="verestadodis" <?php if ($c179 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 100px">
                                        <select multiple="multiple" name="busestadodiseno" id="busestadodiseno"
                                                onchange="" tabindex="17" class="inputs" style="width:148px">
                                            <option value="">-Seleccione-</option>
                                            <?php
                                            $con = mysqli_query($conectar,"select * from estado_diseno where esd_sw_activo = 1 order by esd_nombre");
                                            $num = mysqli_num_rows($con);
                                            for ($i = 0; $i < $num; $i++) {
                                                $dato = mysqli_fetch_array($con);
                                                $clave = $dato['esd_clave_int'];
                                                $nombre = $dato['esd_nombre'];
                                                ?>
                                                <option value="<?php echo $clave; ?>" <?php echo obtenerItemSeleccionado($clave, obtenerListaValores($c59)); ?>><?php echo $nombre; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <td class="alinearizq" style="width: 88px">
                                        Fecha Inicio:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verfechainiciodis"
                                               id="verfechainiciodis" <?php if ($c180 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 155px">
                                        <input name="busfechainiciodiseno" id="busfechainiciodiseno"
                                               value="<?php echo $c60; ?>" placeholder="Desde" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                        <input name="busfechafindiseno" id="busfechafindiseno"
                                               value="<?php echo $c61; ?>" placeholder="Hasta" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="alinearizq" style="width: 88px">
                                        Fecha Entrega:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verfechaentregadis"
                                               id="verfechaentregadis" <?php if ($c181 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 155px">
                                        <input name="busfechaentregainiinfodiseno" id="busfechaentregainiinfodiseno"
                                               value="<?php echo $c62; ?>" placeholder="Desde" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                        <input name="busfechaentregafininfodiseno" id="busfechaentregafininfodiseno"
                                               value="<?php echo $c63; ?>" placeholder="Hasta" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                    </td>
                                    <td class="alinearizq" style="width: 90px">Total Días:</td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="vertotaldiasdis" id="vertotaldiasdis" <?php if ($c182 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 6px">
                                        <input name="bustotaldiasdiseno" id="bustotaldiasdiseno"
                                               value="<?php echo $c64; ?>" tabindex="31" class="inputs" type="text"
                                               style="width: 140px">
                                    </td>
                                    <td class="alinearizq" style="width: 88px">
                                        Notas:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="vernotasdis" id="vernotasdis" <?php if ($c183 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 155px">
                                        <textarea cols="20" class="inputs" name="busnotasdiseno" id="busnotasdiseno"
                                                  rows="2"><?php echo $c65; ?></textarea></td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <hr>
                    </td>
                </tr>
                <tr>
                    <td style="height: 31px;" class="alinearizq">
                        <fieldset name="Group1">
                            <legend><strong>LICENCIA<input name="verlicencia" id="verlicencia"
                                                           style="<?php if ($c128 == 1) {
                                                               echo 'display:none';
                                                           } else {
                                                               echo 'display:block';
                                                           } ?>" onclick="verfiltro('LICENCIA')" type="checkbox"><input
                                            name="ocultarlicencia" id="ocultarlicencia" style="<?php if ($c128 == 1) {
                                        echo 'display:block';
                                    } else {
                                        echo 'display:none';
                                    } ?>" <?php if ($c128 == 1) {
                                        echo 'checked="checked"';
                                    } ?> onclick="ocultarfiltro('LICENCIA')" type="checkbox"><input name="oculicencia"
                                                                                                    id="oculicencia"
                                                                                                    value="<?php if ($c128 == 1) {
                                                                                                        echo '1';
                                                                                                    } else {
                                                                                                        echo '0';
                                                                                                    } ?>"
                                                                                                    type="hidden"/></strong>
                            </legend>
                            <table style="width: 100%;<?php if ($c128 == 0) {
                                echo 'display:none';
                            } ?>" id="tablicencia">
                                <tr>
                                    <td class="alinearizq" style="width: 90px">
                                        Nombre
                                        Gestionador:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="vergestionador" id="vergestionador" <?php if ($c184 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 6px">
                                        <select multiple="multiple" name="busnombregestionador"
                                                id="busnombregestionador" onchange="" tabindex="20" class="inputs"
                                                data-placeholder="-Seleccione-" style="width: 140px">
                                            <option value="">-Seleccione-</option>
                                            <?php
                                            $con = mysqli_query($conectar,"select * from usuario where (usu_categoria = 2 and usu_sw_licencia = 1 and usu_sw_activo = 1) OR (usu_clave_int NOT IN (select usu_clave_int from usuario_actor) and usu_sw_licencia = 1 and usu_sw_activo = 1) order by usu_nombre");
                                            $num = mysqli_num_rows($con);
                                            for ($i = 0; $i < $num; $i++) {
                                                $dato = mysqli_fetch_array($con);
                                                $clave = $dato['usu_clave_int'];
                                                $nombre = $dato['usu_nombre'];
                                                ?>
                                                <option value="<?php echo $clave; ?>" <?php echo obtenerItemSeleccionado($clave, obtenerListaValores($c66)); ?>><?php echo $nombre; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <td class="alinearizq" style="width: 81px">Estado:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verestadolic" id="verestadolic" <?php if ($c185 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 100px">
                                        <select multiple="multiple" name="busestadogestionador"
                                                id="busestadogestionador" onchange="" tabindex="17" class="inputs"
                                                style="width:148px">
                                            <option value="">-Seleccione-</option>
                                            <?php
                                            $con = mysqli_query($conectar,"select * from estado_licencia where esl_sw_activo = 1 order by esl_nombre");
                                            $num = mysqli_num_rows($con);
                                            for ($i = 0; $i < $num; $i++) {
                                                $dato = mysqli_fetch_array($con);
                                                $clave = $dato['esl_clave_int'];
                                                $nombre = $dato['esl_nombre'];
                                                ?>
                                                <option value="<?php echo $clave; ?>" <?php echo obtenerItemSeleccionado($clave, obtenerListaValores($c67)); ?>><?php echo $nombre; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <td class="alinearizq" style="width: 88px">
                                        Fecha Inicio:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verfechainiciolic"
                                               id="verfechainiciolic" <?php if ($c186 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 155px">
                                        <input name="busfechainiciogestionador" id="busfechainiciogestionador"
                                               value="<?php echo $c68; ?>" placeholder="Desde" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                        <input name="busfechafingestionador" id="busfechafingestionador"
                                               value="<?php echo $c69; ?>" placeholder="Hasta" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="alinearizq" style="width: 88px">
                                        Fecha Entrega:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verfechaentregalic"
                                               id="verfechaentregalic" <?php if ($c187 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 155px">
                                        <input name="busfechaentregainiinfogestionador"
                                               id="busfechaentregainiinfogestionador" value="<?php echo $c70; ?>"
                                               placeholder="Desde" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                        <input name="busfechaentregafininfogestionador"
                                               id="busfechaentregafininfogestionador" value="<?php echo $c71; ?>"
                                               placeholder="Hasta" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                    </td>
                                    <td class="alinearizq" style="width: 90px">Total Días:</td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="vertotaldiaslic" id="vertotaldiaslic" <?php if ($c188 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 6px">
                                        <input name="bustotaldiasgestionador" id="bustotaldiasgestionador"
                                               value="<?php echo $c72; ?>" tabindex="31" class="inputs" type="text"
                                               style="width: 140px">
                                    </td>
                                    <td class="alinearizq" style="width: 88px">
                                        Notas:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="vernotaslic" id="vernotaslic" <?php if ($c189 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 155px">
                                        <textarea cols="20" class="inputs" name="busnotasgestionador"
                                                  id="busnotasgestionador" rows="2"><?php echo $c73; ?></textarea></td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <hr>
                    </td>
                </tr>
                <tr>
                    <td style="height: 31px;" class="alinearizq">
                        <fieldset name="Group1">
                            <legend><strong>CANAL Y LÍNEA TELEFÓNICA<input name="verprefactibilidad"
                                                                           id="verprefactibilidad"
                                                                           style="<?php if ($c129 == 1) {
                                                                               echo 'display:none';
                                                                           } else {
                                                                               echo 'display:block';
                                                                           } ?>" onclick="verfiltro('PREFACTIBILIDAD')"
                                                                           type="checkbox"><input
                                            name="ocultarprefactibilidad" id="ocultarprefactibilidad"
                                            style="<?php if ($c129 == 1) {
                                                echo 'display:block';
                                            } else {
                                                echo 'display:none';
                                            } ?>" <?php if ($c129 == 1) {
                                        echo 'checked="checked"';
                                    } ?> onclick="ocultarfiltro('PREFACTIBILIDAD')" type="checkbox"><input
                                            name="ocuprefactibilidad" id="ocuprefactibilidad"
                                            value="<?php if ($c129 == 1) {
                                                echo '1';
                                            } else {
                                                echo '0';
                                            } ?>" type="hidden"/></strong></legend>
                            <table style="width: 100%;<?php if ($c129 == 0) {
                                echo 'display:none';
                            } ?>" class="borrar" id="tabprefactibilidad">
                                <tr>
                                    <td class="alinearizq" style="width: 90px">
                                        Operador
                                        Canal Comunicaciones:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="veroperadorcanal"
                                               id="veroperadorcanal" <?php if ($c190 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/></td>
                                    <td class="alinearizq" style="width: 300px">
                                        <select multiple="multiple" name="buscanalcomunicacion"
                                                id="buscanalcomunicacion" tabindex="20" class="inputs"
                                                data-placeholder="-Seleccione-" style="width: 140px">
                                            <option value="">-Seleccione-</option>
                                            <?php
                                            $con = mysqli_query($conectar,"select * from canal_comunicacion where can_sw_activo = 1 order by can_nombre");
                                            $num = mysqli_num_rows($con);
                                            for ($i = 0; $i < $num; $i++) {
                                                $dato = mysqli_fetch_array($con);
                                                $clave = $dato['can_clave_int'];
                                                $nombre = $dato['can_nombre'];
                                                ?>
                                                <option value="<?php echo $clave; ?>" <?php echo obtenerItemSeleccionado($clave, obtenerListaValores($c74)); ?>><?php echo $nombre; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <td class="alinearizq" style="width: 80px">
                                        Fecha Inicio Teorica:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verfechainiteoricaprefactibilidad"
                                               id="verfechainiteoricaprefactibilidad" <?php if ($c191 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/></td>
                                    <td class="alinearizq" style="width: 300px">
                                        <input name="fechainicioteoricaprefactibilidadbancolombia"
                                               id="fechainicioteoricaprefactibilidadbancolombia"
                                               value="<?php echo $c75; ?>" placeholder="Desde" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px"><br>
                                        <input name="fechafinteoricaprefactibilidadbancolombia"
                                               id="fechafinteoricaprefactibilidadbancolombia"
                                               value="<?php echo $c76; ?>" placeholder="Hasta" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                    </td>
                                    <td class="alinearizq" style="width: 88px">
                                        Fecha Entrega Teorica:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verfechaentregateoricaprefactibilidad"
                                               id="verfechaentregateoricaprefactibilidad" <?php if ($c192 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/></td>
                                    <td class="alinearizq" style="width: 90px">
                                        <input name="fechaentregainiteoricaprefactibilidadbancolombia"
                                               id="fechaentregainiteoricaprefactibilidadbancolombia"
                                               value="<?php echo $c77; ?>" placeholder="Desde" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px"><br>
                                        <input name="fechaentregafinteoricaprefactibilidadbancolombia"
                                               id="fechaentregafinteoricaprefactibilidadbancolombia"
                                               value="<?php echo $c78; ?>" placeholder="Hasta" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="alinearizq" style="width: 90px">
                                        Fecha Entrega:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verfechaentregaprefactibilidad"
                                               id="verfechaentregaprefactibilidad" <?php if ($c193 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/></td>
                                    <td class="alinearizq" style="width: 300px">
                                        <input name="fechaentregainiprefactibilidadbancolombia"
                                               id="fechaentregainiprefactibilidadbancolombia"
                                               value="<?php echo $c79; ?>" placeholder="Desde" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px"><br>
                                        <input name="fechaentregafinprefactibilidadbancolombia"
                                               id="fechaentregafinprefactibilidadbancolombia"
                                               value="<?php echo $c80; ?>" placeholder="Hasta" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                    </td>
                                    <td class="alinearizq" style="width: 80px">
                                        Aprobado:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="veraprobadoprefactibilidad"
                                               id="veraprobadoprefactibilidad" <?php if ($c194 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/></td>
                                    <td class="alinearizq" style="width: 300px">
                                        Si<input name="prefactibilidadaprobadobancolombia"
                                                 id="prefactibilidadaprobadobancolombia" <?php if ($c81 == 1) {
                                            echo 'checked="checked"';
                                        } ?> value="1" tabindex="18" type="radio">
                                        No<input name="prefactibilidadaprobadobancolombia"
                                                 id="prefactibilidadaprobadobancolombia" <?php if ($c81 == 0) {
                                            echo 'checked="checked"';
                                        } ?> value="0" tabindex="19" type="radio">
                                    <td class="alinearizq" style="width: 88px">
                                        Notas:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="vernotasprefactibilidad"
                                               id="vernotasprefactibilidad" <?php if ($c195 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/></td>
                                    <td class="alinearizq" style="width: 90px">
                                        <textarea name="notaprefactibilidad" id="notaprefactibilidad" class="inputs"
                                                  cols="20" rows="2"><?php echo $c82; ?></textarea></td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <hr>
                    </td>
                </tr>
                <tr>
                    <td style="height: 31px;" class="alinearizq">
                        <fieldset name="Group1">
                            <legend><strong>PEDIDO MAQUINA<input name="verpedidomaquina" id="verpedidomaquina"
                                                                 style="<?php if ($c130 == 1) {
                                                                     echo 'display:none';
                                                                 } else {
                                                                     echo 'display:block';
                                                                 } ?>" onclick="verfiltro('PEDIDOMAQUINA')"
                                                                 type="checkbox"><input name="ocultarpedidomaquina"
                                                                                        id="ocultarpedidomaquina"
                                                                                        style="<?php if ($c130 == 1) {
                                                                                            echo 'display:block';
                                                                                        } else {
                                                                                            echo 'display:none';
                                                                                        } ?>" <?php if ($c130 == 1) {
                                        echo 'checked="checked"';
                                    } ?> onclick="ocultarfiltro('PEDIDOMAQUINA')" type="checkbox"><input
                                            name="ocupedidomaquina" id="ocupedidomaquina" value="<?php if ($c130 == 1) {
                                        echo '1';
                                    } else {
                                        echo '0';
                                    } ?>" type="hidden"/></strong></legend>
                            <table style="width: 100%;<?php if ($c130 == 0) {
                                echo 'display:none';
                            } ?>" class="borrar" id="tabpedidomaquina">
                                <tr>
                                    <td class="alinearizq" style="width: 90px">Referencia Maquina:</td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verreferenciamaquina"
                                               id="verreferenciamaquina" <?php if ($c196 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/></td>
                                    <td class="alinearizq" style="width: 300px">
                                        <div id="mireferenciamaquina">
                                            <select multiple="multiple" name="referenciamaquina" id="referenciamaquina"
                                                    onchange="VERIFICAROTRO('REFERENCIAMAQUINA')" tabindex="10"
                                                    class="inputs" data-placeholder="-Seleccione-" style="width: 140px">
                                                <option value="">-Seleccione-</option>
                                                <?php
                                                $con = mysqli_query($conectar,"select * from referenciamaquina where rem_sw_activo = 1 order by rem_nombre");
                                                $num = mysqli_num_rows($con);
                                                for ($i = 0; $i < $num; $i++) {
                                                    $dato = mysqli_fetch_array($con);
                                                    $clave = $dato['rem_clave_int'];
                                                    $nombre = $dato['rem_nombre'];
                                                    ?>
                                                    <option value="<?php echo $clave; ?>" <?php echo obtenerItemSeleccionado($clave, obtenerListaValores($c83)); ?>><?php echo $nombre; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </td>
                                    <td class="alinearizq" style="width: 80px">
                                        Maquina:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="vermaquina" id="vermaquina" <?php if ($c197 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/></td>
                                    <td class="alinearizq" style="width: 300px">
                                        <select multiple="multiple" name="pedidomaquinabancolombia"
                                                id="pedidomaquinabancolombia"
                                                onchange="VERIFICAROTRO('REFERENCIAMAQUINA')" tabindex="10"
                                                class="inputs" data-placeholder="-Seleccione-" style="width: 140px">
                                            <option value="">-Seleccione-</option>
                                            <?php
                                            $con = mysqli_query($conectar,"select * from bancolombia_maquina order by bma_nombre");
                                            $num = mysqli_num_rows($con);
                                            for ($i = 0; $i < $num; $i++) {
                                                $dato = mysqli_fetch_array($con);
                                                $clave = $dato['bma_clave_int'];
                                                $nombre = $dato['bma_nombre'];
                                                ?>
                                                <option value="<?php echo $clave; ?>" <?php echo obtenerItemSeleccionado($clave, obtenerListaValores($c84)); ?>><?php echo $nombre; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <td class="alinearizq" style="width: 88px">
                                        Notas:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="vernotaspedidomaquina"
                                               id="vernotaspedidomaquina" <?php if ($c198 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/></td>
                                    <td class="alinearizq" style="width: 90px">
                                        <textarea name="notapedidomaquina" id="notapedidomaquina" class="inputs"
                                                  cols="20" rows="2"><?php echo $c85; ?></textarea>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <hr>
                    </td>
                </tr>
                <tr>
                    <td style="height: 31px;" class="alinearizq">
                        <fieldset name="Group1">
                            <legend><strong>PROYECCIÓN INSTALACIÓN<input name="verproyeccion" id="verproyeccion"
                                                                         style="<?php if ($c131 == 1) {
                                                                             echo 'display:none';
                                                                         } else {
                                                                             echo 'display:block';
                                                                         } ?>" onclick="verfiltro('PROYECCION')"
                                                                         type="checkbox"><input name="ocultarproyeccion"
                                                                                                id="ocultarproyeccion"
                                                                                                style="<?php if ($c131 == 1) {
                                                                                                    echo 'display:block';
                                                                                                } else {
                                                                                                    echo 'display:none';
                                                                                                } ?>" <?php if ($c131 == 1) {
                                        echo 'checked="checked"';
                                    } ?> onclick="ocultarfiltro('PROYECCION')" type="checkbox"><input
                                            name="ocuproyeccion" id="ocuproyeccion" value="<?php if ($c131 == 1) {
                                        echo '1';
                                    } else {
                                        echo '0';
                                    } ?>" type="hidden"/></strong></legend>
                            <table style="width: 100%;<?php if ($c131 == 0) {
                                echo 'display:none';
                            } ?>" id="tabproyeccion" class="borrar">
                                <tr>
                                    <td class="alinearizq" style="width: 90px">Fecha Inicio Teorica:</td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verfechainiteoricaproyeccion"
                                               id="verfechainiteoricaproyeccion" <?php if ($c199 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/><br>
                                    </td>
                                    <td class="alinearizq" style="width: 300px">
                                        <input name="fechainicioteoricaproyeccionbancolombia"
                                               id="fechainicioteoricaproyeccionbancolombia" value="<?php echo $c86; ?>"
                                               placeholder="Desde" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px"><br>
                                        <input name="fechafinteoricaproyeccionbancolombia"
                                               id="fechafinteoricaproyeccionbancolombia" value="<?php echo $c87; ?>"
                                               placeholder="Hasta" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                    </td>
                                    <td class="alinearizq" style="width: 80px">
                                        Fecha Entrega Teorica:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verfechaentregateoricaproyeccion"
                                               id="verfechaentregainiteoricaproyeccion" <?php if ($c200 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/></td>
                                    <td class="alinearizq" style="width: 90px">
                                        <input name="fechaentregainiteoricaproyeccion"
                                               id="fechaentregainiteoricaproyeccion" value="<?php echo $c88; ?>"
                                               placeholder="Desde" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px"><br>
                                        <input name="fechaentregafinteoricaproyeccion"
                                               id="fechaentregafinteoricaproyeccion" value="<?php echo $c89; ?>"
                                               placeholder="Hasta" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                    </td>
                                    <td class="alinearizq" style="width: 90px">
                                        Notas:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="vernotasproyeccion"
                                               id="vernotasproyeccion" <?php if ($c201 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/></td>
                                    <td class="alinearizq" style="width: 90px">
                                        <textarea name="notaproyeccion" id="notaproyeccion" class="inputs" cols="20"
                                                  rows="2"><?php echo $c90; ?></textarea></td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <hr>
                    </td>
                </tr>
                <tr>
                    <td style="height: 31px;" class="alinearizq">
                        <fieldset name="Group1">
                            <legend><strong>CANAL<input name="vercanal" id="vercanal" style="<?php if ($c132 == 1) {
                                        echo 'display:none';
                                    } else {
                                        echo 'display:block';
                                    } ?>" onclick="verfiltro('CANAL')" type="checkbox"><input name="ocultarcanal"
                                                                                              id="ocultarcanal"
                                                                                              style="<?php if ($c132 == 1) {
                                                                                                  echo 'display:block';
                                                                                              } else {
                                                                                                  echo 'display:none';
                                                                                              } ?>" <?php if ($c132 == 1) {
                                        echo 'checked="checked"';
                                    } ?> onclick="ocultarfiltro('CANAL')" type="checkbox"><input name="ocucanal"
                                                                                                 id="ocucanal"
                                                                                                 value="<?php if ($c132 == 1) {
                                                                                                     echo '1';
                                                                                                 } else {
                                                                                                     echo '0';
                                                                                                 } ?>"
                                                                                                 type="hidden"/></strong>
                            </legend>
                            <table style="width: 100%;<?php if ($c132 == 0) {
                                echo 'display:none';
                            } ?>" class="borrar" id="tabcanal">
                                <tr>
                                    <td class="alinearizq" style="width: 90px">Fecha Inicio Teorica:</td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verfechainicioteoricacanal"
                                               id="verfechainicioteoricacanal" <?php if ($c202 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/></td>
                                    <td class="alinearizq" style="width: 300px">
                                        <input name="fechainicioteoricacanal" id="fechainicioteoricacanal"
                                               value="<?php echo $c91; ?>" placeholder="Desde" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px"><br>
                                        <input name="fechafinteoricacanal" id="fechafinteoricacanal"
                                               value="<?php echo $c92; ?>" placeholder="Hasta" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                    </td>
                                    <td class="alinearizq" style="width: 80px">
                                        Fecha Entrega Teorica:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verfechaentregateoricacanal"
                                               id="verfechaentregateoricacanal" <?php if ($c203 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/></td>
                                    <td class="alinearizq" style="width: 90px">
                                        <input name="fechaentregainiteoricacanal" id="fechaentregainiteoricacanal"
                                               value="<?php echo $c93; ?>" placeholder="Desde" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px"><br>
                                        <input name="fechaentregafinteoricacanal" id="fechaentregafinteoricacanal"
                                               value="<?php echo $c94; ?>" placeholder="Hasta" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                    </td>
                                    <td class="alinearizq" style="width: 90px">&nbsp;
                                    </td>
                                    <td class="alinearizq" style="width: 5px">&nbsp;
                                    </td>
                                    <td class="alinearizq" style="width: 90px">&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="alinearizq" style="width: 90px">
                                        Fecha Inicio:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verfechainiciocanal"
                                               id="verfechainiciocanal" <?php if ($c204 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/></td>
                                    <td class="alinearizq" style="width: 300px">
                                        <input name="fechainiciocanal" id="fechainiciocanal" value="<?php echo $c95; ?>"
                                               placeholder="Desde" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px"><br>
                                        <input name="fechafincanal" id="fechafincanal" value="<?php echo $c96; ?>"
                                               placeholder="Hasta" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                    </td>
                                    <td class="alinearizq" style="width: 80px">
                                        Fecha Entrega:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verfechaentregacanal"
                                               id="verfechaentregacanal" <?php if ($c205 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/></td>
                                    <td class="alinearizq" style="width: 90px">
                                        <input name="fechaentregainicanal" id="fechaentregainicanal"
                                               value="<?php echo $c97; ?>" placeholder="Desde" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px"><br>
                                        <input name="fechaentregafincanal" id="fechaentregafincanal"
                                               value="<?php echo $c98; ?>" placeholder="Hasta" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                    </td>
                                    <td class="alinearizq" style="width: 90px">
                                        Notas:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="vernotascanal" id="vernotascanal" <?php if ($c206 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/></td>
                                    <td class="alinearizq" style="width: 90px">
                                        <textarea name="notacanal" id="notacanal" class="inputs" cols="20"
                                                  rows="2"><?php echo $c99; ?></textarea>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <hr>
                    </td>
                </tr>
                <tr>
                    <td style="height: 31px;" class="alinearizq">
                        <fieldset name="Group1">
                            <legend><strong>INTERVENTORIA<input name="verinterventoria" id="verinterventoria"
                                                                style="<?php if ($c133 == 1) {
                                                                    echo 'display:none';
                                                                } else {
                                                                    echo 'display:block';
                                                                } ?>" onclick="verfiltro('INTERVENTORIA')"
                                                                type="checkbox"><input name="ocultarinterventoria"
                                                                                       id="ocultarinterventoria"
                                                                                       style="<?php if ($c133 == 1) {
                                                                                           echo 'display:block';
                                                                                       } else {
                                                                                           echo 'display:none';
                                                                                       } ?>" <?php if ($c133 == 1) {
                                        echo 'checked="checked"';
                                    } ?> onclick="ocultarfiltro('INTERVENTORIA')" type="checkbox"><input
                                            name="ocuinterventoria" id="ocuinterventoria" value="<?php if ($c133 == 1) {
                                        echo '1';
                                    } else {
                                        echo '0';
                                    } ?>" type="hidden"/></strong></legend>
                            <table style="width: 100%;<?php if ($c133 == 0) {
                                echo 'display:none';
                            } ?>" id="tabinterventoria">
                                <tr>
                                    <td class="alinearizq" style="width: 90px">
                                        Nombre
                                        Interventor:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verinterventor" id="verinterventor" <?php if ($c207 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 6px">
                                        <select multiple="multiple" name="busnombreinterventor"
                                                id="busnombreinterventor" onchange="" tabindex="20" class="inputs"
                                                data-placeholder="-Seleccione-" style="width: 140px">
                                            <option value="">-Seleccione-</option>
                                            <?php
                                            $con = mysqli_query($conectar,"select * from usuario where (usu_categoria = 2 and usu_sw_interventoria = 1 and usu_sw_activo = 1) OR (usu_clave_int NOT IN (select usu_clave_int from usuario_actor) and usu_sw_interventoria = 1 and usu_sw_activo = 1) order by usu_nombre");
                                            $num = mysqli_num_rows($con);
                                            for ($i = 0; $i < $num; $i++) {
                                                $dato = mysqli_fetch_array($con);
                                                $clave = $dato['usu_clave_int'];
                                                $nombre = $dato['usu_nombre'];
                                                ?>
                                                <option value="<?php echo $clave; ?>" <?php echo obtenerItemSeleccionado($clave, obtenerListaValores($c100)); ?>><?php echo $nombre; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <td class="alinearizq" style="width: 81px">Fecha Inicio Obra:</td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verfechainicioint"
                                               id="verfechainicioint" <?php if ($c209 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/></td>
                                    <td class="alinearizq" style="width: 100px">
                                        <input name="busfechainiciointerventor" id="busfechainiciointerventor"
                                               value="<?php echo $c102; ?>" placeholder="Desde" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                        <input name="busfechafininterventor" id="busfechafininterventor"
                                               value="<?php echo $c103; ?>" placeholder="Hasta" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                    </td>
                                    <td class="alinearizq" style="width: 88px">
                                    </td>
                                    <td class="alinearizq" style="width: 4px">

                                    </td>
                                    <td class="alinearizq" style="width: 155px">

                                    </td>
                                </tr>
                                <tr>
                                    <td class="alinearizq" style="width: 88px">
                                        Fecha Entrega:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verfechaentregaint"
                                               id="verfechaentregaint" <?php if ($c210 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 155px">
                                        <input name="busfechaentregainiinfointerventor"
                                               id="busfechaentregainiinfointerventor" value="<?php echo $c104; ?>"
                                               placeholder="Desde" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                        <input name="busfechaentregafininfointerventor"
                                               id="busfechaentregafininfointerventor" value="<?php echo $c105; ?>"
                                               placeholder="Hasta" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                    </td>
                                    <td class="alinearizq" style="width: 90px">Fecha Pedido Suministro:</td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verfechapedido" id="verfechapedido" <?php if ($c211 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 6px">
                                        <input name="busfechainipedidointerventor" id="busfechainipedidointerventor"
                                               value="<?php echo $c106; ?>" placeholder="Desde" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                        <input name="busfechafinpedidointerventor" id="busfechafinpedidointerventor"
                                               value="<?php echo $c107; ?>" placeholder="Hasta" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                    </td>
                                    <td class="alinearizq" style="width: 88px">
                                        Notas:
                                    </td>
                                    <td class="alinearizq" style="width: 4px">
                                        <input name="vernotasint" id="vernotasint" <?php if ($c212 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 155px">
                                        <textarea cols="20" class="inputs" name="busnotasinterventor"
                                                  id="busnotasinterventor" rows="2"><?php echo $c108; ?></textarea></td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <hr>
                    </td>
                </tr>
                <tr>
                    <td style="height: 31px;" class="alinearizq">
                        <fieldset name="Group1">
                            <legend><strong>CONSTRUCTOR<input name="verconstructor" id="verconstructor"
                                                              style="<?php if ($c134 == 1) {
                                                                  echo 'display:none';
                                                              } else {
                                                                  echo 'display:block';
                                                              } ?>" onclick="verfiltro('CONSTRUCTOR')"
                                                              type="checkbox"><input name="ocultarconstructor"
                                                                                     id="ocultarconstructor"
                                                                                     style="<?php if ($c134 == 1) {
                                                                                         echo 'display:block';
                                                                                     } else {
                                                                                         echo 'display:none';
                                                                                     } ?>" <?php if ($c134 == 1) {
                                        echo 'checked="checked"';
                                    } ?> onclick="ocultarfiltro('CONSTRUCTOR')" type="checkbox"><input
                                            name="ocuconstructor" id="ocuconstructor" value="<?php if ($c134 == 1) {
                                        echo '1';
                                    } else {
                                        echo '0';
                                    } ?>" type="hidden"/></strong></legend>
                            <table style="width: 100%;<?php if ($c134 == 0) {
                                echo 'display:none';
                            } ?>" id="tabconstructor">
                                <tr>
                                    <td class="alinearizq" style="width: 90px">
                                        Nombre
                                        Constructor:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verconstructor1" id="verconstructor1" <?php if ($c213 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 6px">
                                        <select multiple="multiple" name="busnombreconstructor"
                                                id="busnombreconstructor" onchange="" tabindex="20" class="inputs"
                                                data-placeholder="-Seleccione-" style="width: 140px">
                                            <option value="">-Seleccione-</option>
                                            <?php
                                            $con = mysqli_query($conectar,"select * from usuario where (usu_categoria = 2 and usu_sw_constructor = 1 and usu_sw_activo = 1) OR (usu_clave_int NOT IN (select usu_clave_int from usuario_actor) and usu_sw_constructor = 1 and usu_sw_activo = 1) order by usu_nombre");
                                            $num = mysqli_num_rows($con);
                                            for ($i = 0; $i < $num; $i++) {
                                                $dato = mysqli_fetch_array($con);
                                                $clave = $dato['usu_clave_int'];
                                                $nombre = $dato['usu_nombre'];
                                                ?>
                                                <option value="<?php echo $clave; ?>" <?php echo obtenerItemSeleccionado($clave, obtenerListaValores($c109)); ?>><?php echo $nombre; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <td class="alinearizq" style="width: 81px">%Avance:
                                    </td>
                                    <td class="alinearizq" style="width: 5px"><input name="veravance"
                                                                                     id="veravance" <?php if ($c214 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/></td>
                                    <td class="alinearizq" style="width: 100px">
                                        <select multiple="multiple" name="busporcentajeavance" id="busporcentajeavance"
                                                onchange="" tabindex="33" class="inputs" data-placeholder="-Seleccione-"
                                                style="width: 140px">
                                            <option value="">-Seleccione-</option>
                                            <?php
                                            for ($i = 10; $i <= 100; $i = $i + 10) {
                                                ?>
                                                <option value="<?php echo $i; ?>" <?php echo obtenerItemSeleccionado($i, obtenerListaValores($c110)); ?>><?php echo $i; ?>
                                                    %
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <td class="alinearizq" style="width: 88px">
                                        Fecha Inicio:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verfechainiciocons"
                                               id="verfechainiciocons" <?php if ($c215 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 155px">
                                        <input name="busfechainicioconstructor" id="busfechainicioconstructor"
                                               value="<?php echo $c111; ?>" placeholder="Desde" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                        <input name="busfechafinconstructor" id="busfechafinconstructor"
                                               value="<?php echo $c112; ?>" placeholder="Hasta" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="alinearizq" style="width: 88px">
                                        Fecha Entrega:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verfechaentregacons"
                                               id="verfechaentregacons" <?php if ($c216 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 155px">
                                        <input name="busfechaentregainiinfoconstructor"
                                               id="busfechaentregainiinfoconstructor" value="<?php echo $c113; ?>"
                                               placeholder="Desde" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                        <input name="busfechaentregafininfoconstructor"
                                               id="busfechaentregafininfoconstructor" value="<?php echo $c114; ?>"
                                               placeholder="Hasta" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                    </td>
                                    <td class="alinearizq" style="width: 90px">Total Días:</td>
                                    <td class="alinearizq" style="width: 5px"><input name="vertotaldiascons"
                                                                                     id="vertotaldiascons" <?php if ($c217 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/></td>
                                    <td class="alinearizq" style="width: 6px">
                                        <input name="bustotaldiasconstructor" id="bustotaldiasconstructor"
                                               value="<?php echo $c115; ?>" tabindex="31" class="inputs" type="text"
                                               style="width: 140px">
                                    </td>
                                    <td class="alinearizq" style="width: 88px">
                                        Notas:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="vernotascons" id="vernotascons" <?php if ($c218 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 155px">
                                        <textarea cols="20" class="inputs" name="busnotasconstructor"
                                                  id="busnotasconstructor" rows="2"><?php echo $c116; ?></textarea></td>
                                </tr>
                                <tr>
                                    <td class="alinearizq" style="width: 88px">
                                        Informe Final:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verinformefinalcons"
                                               id="verinformefinalcons" <?php if ($c219 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 155px">&nbsp;
                                    </td>
                                    <td class="alinearizq" style="width: 90px">&nbsp;</td>
                                    <td class="alinearizq" style="width: 5px">&nbsp;</td>
                                    <td class="alinearizq" style="width: 6px">&nbsp;
                                    </td>
                                    <td class="alinearizq" style="width: 88px">&nbsp;
                                    </td>
                                    <td class="alinearizq" style="width: 5px">&nbsp;
                                    </td>
                                    <td class="alinearizq" style="width: 155px">&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td style="height: 31px;" class="alinearizq">
                        <fieldset name="Group1">
                            <legend><strong>SEGURIDAD<input name="verseguridad" id="verseguridad"
                                                            style="<?php if ($c135 == 1) {
                                                                echo 'display:none';
                                                            } else {
                                                                echo 'display:block';
                                                            } ?>" onclick="verfiltro('SEGURIDAD')"
                                                            type="checkbox"><input name="ocultarseguridad"
                                                                                   id="ocultarseguridad"
                                                                                   style="<?php if ($c135 == 1) {
                                                                                       echo 'display:block';
                                                                                   } else {
                                                                                       echo 'display:none';
                                                                                   } ?>" <?php if ($c135 == 1) {
                                        echo 'checked="checked"';
                                    } ?> onclick="ocultarfiltro('SEGURIDAD')" type="checkbox"><input name="ocuseguridad"
                                                                                                     id="ocuseguridad"
                                                                                                     value="<?php if ($c135 == 1) {
                                                                                                         echo '1';
                                                                                                     } else {
                                                                                                         echo '0';
                                                                                                     } ?>"
                                                                                                     type="hidden"/></strong>
                            </legend>
                            <table style="width: 100%;<?php if ($c135 == 0) {
                                echo 'display:none';
                            } ?>" id="tabseguridad">
                                <tr>
                                    <td class="alinearizq" style="width: 90px">
                                        Nombre
                                        Instalador:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verseguridad1" id="verseguridad1" <?php if ($c220 == 'true') {
                                            echo 'checked="checked"';
                                        } ?>b type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 6px">
                                        <select multiple="multiple" name="busnombreseguridad" id="busnombreseguridad"
                                                onchange="" tabindex="20" class="inputs" data-placeholder="-Seleccione-"
                                                style="width: 140px">
                                            <option value="">-Seleccione-</option>
                                            <?php
                                            $con = mysqli_query($conectar,"select * from usuario where (usu_categoria = 2 and usu_sw_seguridad = 1 and usu_sw_activo = 1) OR (usu_clave_int NOT IN (select usu_clave_int from usuario_actor) and usu_sw_seguridad = 1 and usu_sw_activo = 1) order by usu_nombre");
                                            $num = mysqli_num_rows($con);
                                            for ($i = 0; $i < $num; $i++) {
                                                $dato = mysqli_fetch_array($con);
                                                $clave = $dato['usu_clave_int'];
                                                $nombre = $dato['usu_nombre'];
                                                ?>
                                                <option value="<?php echo $clave; ?>" <?php echo obtenerItemSeleccionado($clave, obtenerListaValores($c117)); ?>><?php echo $nombre; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <td class="alinearizq" style="width: 81px">
                                        Código Monitoreo:
                                    </td>
                                    <td class="alinearizq" style="width: 5px"><input name="vercodigomonitoreo"
                                                                                     id="vercodigomonitoreo" <?php if ($c221 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/></td>
                                    <td class="alinearizq" style="width: 100px">
                                        <input name="buscodigomonitoreo" id="buscodigomonitoreo"
                                               value="<?php echo $c118; ?>" tabindex="31" class="inputs" type="text"
                                               style="width: 140px">
                                    </td>
                                    <td class="alinearizq" style="width: 88px">
                                        Fecha Ingreso Obra:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="verfechaingresoobra"
                                               id="verfechaingresoobra" <?php if ($c222 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 155px">
                                        <input name="busfechaingresoiniobra" id="busfechaingresoiniobra"
                                               value="<?php echo $c119; ?>" placeholder="Desde" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                        <input name="busfechaingresofinobra" id="busfechaingresofinobra"
                                               value="<?php echo $c120; ?>" placeholder="Hasta" tabindex="31"
                                               onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs"
                                               type="text" style="width: 140px">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="alinearizq" style="width: 90px">
                                        Notas:
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="vernotasseg" id="vernotasseg" <?php if ($c223 == 'true') {
                                            echo 'checked="checked"';
                                        } ?> type="checkbox"/>
                                    </td>
                                    <td class="alinearizq" style="width: 6px">
                                        <textarea cols="20" class="inputs" name="busnotasseguridad"
                                                  id="busnotasseguridad" rows="2"><?php echo $c121; ?></textarea>
                                    </td>
                                    <td class="alinearizq" style="width: 81px">&nbsp;
                                    </td>
                                    <td class="alinearizq" style="width: 5px">&nbsp;</td>
                                    <td class="alinearizq" style="width: 100px">&nbsp;
                                    </td>
                                    <td class="alinearizq" style="width: 88px">&nbsp;
                                    </td>
                                    <td class="alinearizq" style="width: 5px">&nbsp;
                                    </td>
                                    <td class="alinearizq" style="width: 155px">&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td style="height: 31px;" class="alinearizq">
                        <fieldset name="Group1">
                            <legend><strong>NOTAS GENERALES<input name="vernotasgen"
                                                                  id="vernotasgen" <?php if ($c225 == 'true') {
                                        echo 'checked="checked"';
                                    } ?> type="checkbox"/></strong></legend>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td style="height: 31px;" class="alinearizq">
                        <fieldset name="Group1">
                            <legend><strong>ADJUNTOS
                                    <input name="veradjunto" id="veradjunto" onclick="verfiltro('ADJUNTO')"
                                           type="checkbox" style="<?php if ($c228 == 1) {
                                        echo 'display:none';
                                    } else {
                                        echo 'display:block';
                                    } ?>"><input name="ocultaradjuntos" id="ocultaradjuntos"
                                                 style="<?php if ($c228 == 1) {
                                                     echo 'display:block';
                                                 } else {
                                                     echo 'display:none';
                                                 } ?>" <?php if ($c228 == 1) {
                                        echo 'checked="checked"';
                                    } ?> onclick="ocultarfiltro('ADJUNTO')" type="checkbox"><input name="ocuadjuntos"
                                                                                                   id="ocuadjuntos"
                                                                                                   value="<?php if ($c228 == 1) {
                                                                                                       echo "1";
                                                                                                   } else {
                                                                                                       echo "0";
                                                                                                   } ?>" type="hidden"/></strong>
                            </legend>
                            <table style="width: 100%;<?php if ($c228 == 1) {
                                echo "display:block";
                            } else {
                                echo "display:none";
                            } ?>" id="tabadjuntos">
                                <tr>
                                    <td class="alinearizq" style="width: 90px">
                                        Tipo Adjunto
                                        :
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        <input name="c229" id="c229" type="checkbox" <?php if ($c229 == 'true') {
                                            echo "checked='checked'";
                                        } ?> />
                                    </td>
                                    <td class="alinearizq" style="width: 6px">
                                        <select multiple="multiple" name="c230" id="c230" onchange="" tabindex="20"
                                                class="inputs" data-placeholder="-Seleccione-" style="width: 140px">
                                            <option value="">-Seleccione-</option>
                                            <?php
                                            $seleccionados = explode(',', $c230);
                                            $con = mysqli_query($conectar,"select * from tipo_adjunto order by tad_nombre");
                                            $num = mysqli_num_rows($con);
                                            for ($i = 0; $i < $num; $i++) {
                                                $dato = mysqli_fetch_array($con);
                                                $clave = $dato['tad_clave_int'];
                                                $nombre = $dato['tad_nombre'];
                                                ?>
                                                <option <?php if (in_array($clave, $seleccionados)) {
                                                    echo "selected";
                                                } ?> value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <td class="alinearizq" style="width: 81px">

                                    </td>
                                    <td class="alinearizq" style="width: 5px"
                                    </td>
                                    <td class="alinearizq" style="width: 100px">

                                    </td>
                                    <td class="alinearizq" style="width: 88px">

                                    </td>
                                    <td class="alinearizq" style="width: 5px">

                                    </td>
                                    <td class="alinearizq" style="width: 155px">

                                    </td>
                                </tr>
                                <tr>
                                    <td class="alinearizq" style="width: 90px">
                                    </td>
                                    <td class="alinearizq" style="width: 5px">

                                    </td>
                                    <td class="alinearizq" style="width: 6px">

                                    </td>
                                    <td class="alinearizq" style="width: 81px">
                                        &nbsp;
                                    </td>
                                    <td class="alinearizq" style="width: 5px">&nbsp;</td>
                                    <td class="alinearizq" style="width: 100px">
                                        &nbsp;
                                    </td>
                                    <td class="alinearizq" style="width: 88px">
                                        &nbsp;
                                    </td>
                                    <td class="alinearizq" style="width: 5px">
                                        &nbsp;
                                    </td>
                                    <td class="alinearizq" style="width: 155px">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td style="height: 31px;" align="center">

                        &nbsp;

                        <table style="width: 30%" align="center">
                            <tr>
                                <td align="center">
                                    <input name="Button1" type="button" class="inputs" value="BUSCAR"
                                           onclick="BUSCAR('CAJERO','')" style="width: 120px;cursor:pointer"></td>
                                <td align="center">
                                    <input name="Button2" type="button" class="inputs" value="ACTUALIZAR"
                                           onclick="GUARDAR('ACTUALIZAR','<?php echo $ley; ?>','<?php echo $clabug; ?>');BUSCAR('CAJERO','')"
                                           style="width: 120px;cursor:pointer"></td>
                                <td align="center">
                                    <input name="Button2" type="button" class="inputs" value="ELIMINAR"
                                           onclick="ELIMINARBUSQUEDA('<?php echo $clabug; ?>')"
                                           style="width: 120px;cursor:pointer">
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="3">
                                    <div id="resultadoalguardar">
                                        <br>
                                        <select name="Select1" class="inputs" style="width:85%"
                                                onchange="MOSTRARFILTRO(this.value)">
                                            <option value="">-Búsquedas Guardadas-</option>
                                            <?php
                                            $con = mysqli_query($conectar,"select * from busqueda_guardada order by bug_nombre");
                                            $num = mysqli_num_rows($con);
                                            for ($i = 0; $i < $num; $i++) {
                                                $dato = mysqli_fetch_array($con);
                                                $clave = $dato['bug_clave_int'];
                                                $nombre = $dato['bug_nombre'];
                                                ?>
                                                <option value="<?php echo $clave; ?>" <?php if ($clabug == $clave) {
                                                    echo 'selected="selected"';
                                                } ?>><?php echo $nombre; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </fieldset>
        <?php
        echo "<style onload=REFRESCARBUSQUEDACAJEROS();></style>";
        echo "<style onload=refreshSelect()></style>";
        echo "<style onload=BUSCAR('CAJERO','')></style>";
        echo "<style onload=OCULTARSCROLL();></style>";

    }
?>