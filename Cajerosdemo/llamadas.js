function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function MODULO(v)
{	
	if(v == 'INICIO')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?moduloinicio=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'USUARIOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulousuarios=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'CONFIGURACIONES')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?moduloconfiguraciones=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'CAMBIARCONTRASENA')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulocancambiarcontrasena=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'CAJEROS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulocajeros=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'POSICIONGEOGRAFICA')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?moduloposiciongeografica=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'CONSTRUCTOR')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?moduloconstructor=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'INMOBILIARIA')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?moduloinmobiliaria=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'DISENO')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulodiseno=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'INTERVENTORIA')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulointerventoria=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'INSTALADORSEGURIDAD')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?moduloinstaladorseguridad=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'CENTROCOSTOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulocentrocostos=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'TIPOLOGIA')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulotipologia=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'REFERENCIAMAQUINA')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?moduloreferenciamaquina=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'UBICACION')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?moduloubicacion=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'ATIENDE')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?moduloatiende=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'TIPOINTERVENCION')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulotipointervencion=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'AREA')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?moduloarea=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'VISITALOCAL')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulovisitalocal=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'LICENCIA')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulolicencia=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'FACTURACION')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulofacturacion=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'CANALCOMUNICACIONES')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulocanalcomunicaciones=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'CONSULTA')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?moduloconsulta=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'PERMISOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulopermisos=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'REGISTROACTIVIDADES')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?moduloregistroactividades=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'ALERTAS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?moduloalertas=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'INFORMECAJEROELIMINADO')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulocajeroseliminados=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'INFORMECAJEROSSINELIMINAR')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulocajerossineliminar=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'MODALIDAD')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?modulomodalidad=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
	else
	if(v == 'PLANEACION')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modulos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modulos").html("<img alt='cargando' src='images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?moduloplaneacion=si",true);
		ajax.send(null);
		//setTimeout("OPCIONACTIVA('ADMINISTRADOR')",5);
	}
}
function OPCIONACTIVA(v)
{	
	if(v == 'ADMINISTRADOR')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('tabContent_5').innerHTML=ajax.responseText;
		    }
		}
		//jQuery("#tabContent_5").html("<img alt='cargando' src='img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?opcionadministrador=si",true);
		ajax.send(null);
	}
	else
	if(v == 'TICKETS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('tabContent_1').innerHTML=ajax.responseText;
		    }
		}
		//jQuery("#tabContent_5").html("<img alt='cargando' src='img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?opciontickets=si",true);
		ajax.send(null);
	}
	else
	if(v == 'REPORTES')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('tabContent_2').innerHTML=ajax.responseText;
		    }
		}
		//jQuery("#tabContent_5").html("<img alt='cargando' src='img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?opcionreportes=si",true);
		ajax.send(null);
	}
}
function RECUPERAR()
{	
	var rec = $("#recuperarcontrasena").val();
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('recu').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#recu").html("<img alt='cargando' src='images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?recuperar=si&dat="+rec,true);
	ajax.send(null);
}
function ENVIARNOTIFICACION()
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('').innerHTML=ajax.responseText;
	    }
	}
	//jQuery("#recu").html("<img alt='cargando' src='img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?enviarnotificacion=si",true);
	ajax.send(null);
}
function RESTABLECER(v)
{
	var con1 = $("#contrasena1").val();
	var con2 = $("#contrasena2").val();
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('clear').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#clear").html("<img alt='cargando' src='img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?restablecer=si&cod="+v+"&con1="+con1+"&con2="+con2,true);
	ajax.send(null);
}
function NOPERMISO(v)
{
	alert("Usted no tiene permiso a la opci�n "+v);
}