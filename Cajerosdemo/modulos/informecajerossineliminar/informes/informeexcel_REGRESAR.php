<?php
error_reporting(0);
include('../../../data/Conexion.php');
require_once('../../../Classes/PHPExcel.php');
date_default_timezone_set('America/Bogota');
session_start();
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_SESSION['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];
	
// verifica si no se ha loggeado
if(!isset($_SESSION["persona"]))
{
  session_destroy();
  header("LOCATION:index.php");
}else{
}

function CORREGIRTEXTO($string)
{
    $string = str_replace("REEMPLAZARNUMERAL", "#", $string);
    $string = str_replace("REEMPLAZARMAS", "+",$string);
    $string = str_replace("REEMPLAZARMENOS", "-", $string);
    return $string;
}

function obtenerListaValores($valores)
{
    $seleccionados = explode(',',$valores);
    $num = count($seleccionados);
    $sqlIn = array();
    for($i = 0; $i < $num; $i++)
    {
        if($seleccionados[$i] != '')
        {
            $sqlIn[$i]=$seleccionados[$i];
        }
    }
    $valoresSQLIn = implode(',',$sqlIn);

    return $valoresSQLIn;
}

function obtenerSQL($campo, $valor)
{
    $cadenaSQL = "";

    if (trim($valor) == '') {
        //(caj_estado_proyecto = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') and
        $formato = "%s = '%s' OR '%s' IS NULL OR '%s' = ''";
        $cadenaSQL = sprintf($formato, $campo, $valor, $valor, $valor);
    } else {
        //c.caj_clave_int in (".$listacajeros.")
        $formato = "%s IN (%s)";
        $cadenaSQL = sprintf($formato, $campo, $valor);
    }

    return $cadenaSQL;
}


$caj = $_GET['caj'];
$seleccionados = explode(',',$caj);
$num = count($seleccionados);
$cajeros = array();
for($i = 0; $i < $num; $i++)
{
	if($seleccionados[$i] != '')
	{
		$cajeros[$i]=$seleccionados[$i];
	}
}
$listacajeros=implode(',',$cajeros);




$fii = $_GET['fii'];
$ffi = $_GET['ffi'];
$fie = $_GET['fie'];
$ffe = $_GET['ffe'];
$nomcaj = $_GET['nomcaj'];
$dircaj = $_GET['dircaj'];
$est = $_GET['est'];
$anocon = $_GET['anocon'];
$reg = $_GET['reg'];
$dep = $_GET['dep'];
$mun = $_GET['mun'];
$tip = $_GET['tip'];
$tipint = $_GET['tipint'];
$moda = $_GET['moda'];

$cod = $_GET['cod'];
$cencos = $_GET['cencos'];
$refmaq = $_GET['refmaq'];
$ubi = $_GET['ubi'];
$codsuc = $_GET['codsuc'];
$ubiatm = $_GET['ubiatm'];
$ati = $_GET['ati'];
$rie = $_GET['rie'];
$are = $_GET['are'];
$fecapagadoatm = $_GET['fecapagadoatm'];

$nominm = $_GET['nominm'];
$estinm = $_GET['estinm'];
$feciniinmdesde = $_GET['feciniinmdesde'];
$feciniinmhasta = $_GET['feciniinmhasta'];
$fecentinmdesde = $_GET['fecentinmdesde'];
$fecentinmhasta = $_GET['fecentinmhasta'];
$diasinm = $_GET['diasinm'];
$notinm = $_GET['notinm'];

$nomvis = $_GET['nomvis'];
$estvis = $_GET['estvis'];
$fecinivisdesde = $_GET['fecinivisdesde'];
$fecinivishasta = $_GET['fecinivishasta'];
$fecentvisdesde = $_GET['fecentvisdesde'];
$fecentvishasta = $_GET['fecentvishasta'];
$diasvis = $_GET['diasvis'];
$notvis = $_GET['notvis'];

$nomdis = $_GET['nomdis'];
$estdis = $_GET['estdis'];
$fecinidisdesde = $_GET['fecinidisdesde'];
$fecinidishasta = $_GET['fecinidishasta'];
$fecentdisdesde = $_GET['fecentdisdesde'];
$fecentdishasta = $_GET['fecentdishasta'];
$diasdis = $_GET['diasdis'];
$notdis = $_GET['notdis'];

$nomges = $_GET['nomges'];
$estges = $_GET['estges'];
$fecinigesdesde = $_GET['fecinigesdesde'];
$fecinigeshasta = $_GET['fecinigeshasta'];
$fecentgesdesde = $_GET['fecentgesdesde'];
$fecentgeshasta = $_GET['fecentgeshasta'];
$diasges = $_GET['diasges'];
$notges = $_GET['notges'];

$nomint = $_GET['nomint'];
$cancom = $_GET['cancom'];
$feciniintdesde = $_GET['feciniintdesde'];
$feciniinthasta = $_GET['feciniinthasta'];
$fecentintdesde = $_GET['fecentintdesde'];
$fecentinthasta = $_GET['fecentinthasta'];
$fecinipedintdesde = $_GET['fecinipedintdesde'];
$fecinipedinthasta = $_GET['fecinipedinthasta'];
$notint = $_GET['notint'];

$nomcons = $_GET['nomcons'];
$porava = $_GET['porava'];
$feciniconsdesde = $_GET['feciniconsdesde'];
$feciniconshasta = $_GET['feciniconshasta'];
$fecentconsdesde = $_GET['fecentconsdesde'];
$fecentconshasta = $_GET['fecentconshasta'];
$diascons = $_GET['diascons'];
$notcons = $_GET['notcons'];

$nomseg = $_GET['nomseg'];
$codmon = $_GET['codmon'];
$fecinginiobra = $_GET['finginiobra'];
$fecingfinobra = $_GET['fingfinobra'];
$notseg = $_GET['notseg'];

$swinfobasica = $_GET['swinfobasica'];
$swinfosecundaria = $_GET['swinfosecundaria'];
$swinmobiliaria = $_GET['swinmobiliaria'];
$swvisitalocal = $_GET['swvisitalocal'];
$swdiseno = $_GET['swdiseno'];
$swlicencia = $_GET['swlicencia'];
$swinterventoria = $_GET['swinterventoria'];
$swconstructor = $_GET['swconstructor'];
$swseguridad = $_GET['swseguridad'];

$vernombrecajero = $_GET['vnomcaj'];
$verdireccion = $_GET['vdir'];
$verestadocajero = $_GET['vestcaj'];
$veranocontable = $_GET['vanocont'];
$verregion = $_GET['vreg'];
$verdepartamento = $_GET['vdep'];
$vermunicipio = $_GET['vmun'];
$vertipologia = $_GET['vtipol'];
$verintervencion = $_GET['vinter'];
$vermodalidad = $_GET['vmoda'];
$vercodigocajero = $_GET['vcodigocajero'];
$vercentrocostos = $_GET['vcentrocostos'];
$verreferencia = $_GET['vreferencia'];
$verubicacion = $_GET['vubicacion'];
$vercodigosuc = $_GET['vcodigosuc'];
$verubicacionatm = $_GET['vubicacionatm'];
$veratiende = $_GET['vatiende'];
$verriesgo = $_GET['vriesgo'];
$verarea = $_GET['varea'];
$vercodigorecibido = $_GET['vcodigorecibido'];
$verinmobiliaria = $_GET['vinmobiliaria'];
$verestadoinm = $_GET['vestadoinm'];
$verfechainicioinm = $_GET['vfechainicioinm'];
$verfechaentregainm = $_GET['vfechaentregainm'];
$vertotaldiasinm = $_GET['vtotaldiasinm'];
$vernotasinm = $_GET['vnotasinm'];
$vervisita = $_GET['vvisita'];
$verestadovis = $_GET['vestadovis'];
$verfechainiciovis = $_GET['vfechainiciovis'];
$verfechaentregavis = $_GET['vfechaentregavis'];
$vertotaldiasvis = $_GET['vtotaldiasvis'];
$vernotasvis = $_GET['vnotasvis'];
$verdisenador = $_GET['vdisenador'];
$verestadodis = $_GET['vestadodis'];
$verfechainiciodis = $_GET['vfechainiciodis'];
$verfechaentregadis = $_GET['vfechaentregadis'];
$vertotaldiasdis = $_GET['vtotaldiasdis'];
$vernotasdis = $_GET['vnotasdis'];
$vergestionador = $_GET['vgestionador'];
$verestadolic = $_GET['vestadolic'];
$verfechainiciolic = $_GET['vfechainiciolic'];
$verfechaentregalic = $_GET['vfechaentregalic'];
$vertotaldiaslic = $_GET['vtotaldiaslic'];
$vernotaslic = $_GET['vnotaslic'];
$verinterventor = $_GET['vinterventor'];
$veroperadorcanal = $_GET['voperadorcanal'];
$verfechainicioint = $_GET['vfechainicioint'];
$verfechaentregaint = $_GET['vfechaentregaint'];
$verfechapedido = $_GET['vfechapedido'];
$vernotasint = $_GET['vnotasint'];
$verconstructor = $_GET['vconstructor'];
$veravance = $_GET['vavance'];
$verfechainiciocons = $_GET['vfechainiciocons'];
$verfechaentregacons = $_GET['vfechaentregacons'];
$vertotaldiascons = $_GET['vtotaldiascons'];
$vernotascons = $_GET['vnotascons'];
$verinffincons = $_GET['vinffincons'];
$verseguridad = $_GET['vseguridad'];
$vercodmon = $_GET['vcodmon'];
$verfecingobra = $_GET['vfecingobra'];
$vernotasseg = $_GET['vnotasseg'];

function numeros_a_letras($numero) 
{
    // Convierte un numero en una letra de la A a la Z en el alfabeto latin
    // Utilizado para las columnas de excel
    // A = 0, si queremos que A = 1, modificaremos 26 por 27 (en los 2 sitios que esta)
    // Si le pasamos el valor 0 nos devolvera A, si pasamos 27 nos devolvera AB
    $res = "";
    while ($numero > -1) {
        // Cargaremos la letra actual
        $letter = $numero % 26;
        $res = chr(65 + $letter) . $res;  // A 65 en ASCII (A) le sumaremos el valor de la letra y lo convertiremos a texto (65 + 0 = A)
        $numero = intval($numero / 26) - 1; // Le quitamos la letra para ir a la siguiente y le restamos 1 si no se saltara una serie
    }
    return $res;
}
$a = 0;$b = 0;$c = 0;$d = 0;$e = 0;$f = 0;$g = 0;$h = 0;$i = 0;$j = 0;$k = 0;$l = 0;$m = 0;$n = 0;$o = 0;$p = 0;$q = 0;$r = 0;$s = 0;$t = 0;
$u = 0;$v = 0;$w = 0;$x = 0;$y = 0;$z = 0;
$aa = 0;$ab = 0;$ac = 0;$ad = 0;$ae = 0;$af = 0;$ag = 0;$ah = 0;$ai = 0;$aj = 0;$ak = 0;$al = 0;$am = 0;$an = 0;$ao = 0;$ap = 0;$aq = 0;$ar = 0;
$as = 0;$at = 0;$au = 0;$av = 0;$aw = 0;$ax = 0;$ay = 0;$az = 0;
$ba = 0;$bb = 0;$bc = 0;$bd = 0;$be = 0;$bf = 0;$bg = 0;$bh = 0;$bi = 0;$bj = 0;$bk = 0;$bl = 0;$bm = 0;$bn = 0;$bo = 0;$bp = 0;$bq = 0;$br = 0;
$bs = 0;$bt = 0;$bu = 0;$bv = 0;$bw = 0;$bx = 0;$by = 0;$bz = 0;

if($swinfobasica == 0)
{
	$nomcaj = '';
	$dircaj = '';
	$est = '';
	$anocon = '';
	$reg = '';
	$dep = '';
	$mun = '';
	$tip = '';
	$tipint = '';
	$moda = '';
}
if($swinfosecundaria == 0)
{
	$cod = '';
	$cencos = '';
	$refmaq = '';
	$ubi = '';
	$codsuc = '';
	$ubiatm = '';
	$ati = '';
	$rie = '';
	$are = '';
	$fecapagadoatm = '';
}
if($swinmobiliaria == 0)
{
	$nominm = '';
	$estinm = '';
	$feciniinmdesde = '';
	$feciniinmhasta = '';
	$fecentinmdesde = '';
	$fecentinmhasta = '';
	$diasinm = '';
	$notinm = '';
}
if($swvisitalocal == 0)
{
	$nomvis = '';
	$estvis = '';
	$fecinivisdesde = '';
	$fecinivishasta = '';
	$fecentvisdesde = '';
	$fecentvishasta = '';
	$diasvis = '';
	$notvis = '';
}
if($swdiseno == 0)
{
	$nomdis = '';
	$estdis = '';
	$fecinidisdesde = '';
	$fecinidishasta = '';
	$fecentdisdesde = '';
	$fecentdishasta = '';
	$diasdis = '';
	$notdis = '';
}
if($swlicencia == 0)
{
	$nomges = '';
	$estges = '';
	$fecinigesdesde = '';
	$fecinigeshasta = '';
	$fecentgesdesde = '';
	$fecentgeshasta = '';
	$diasges = '';
	$notges = '';
}
if($swinterventoria == 0)
{
	$nomint = '';
	$cancom = '';
	$feciniintdesde = '';
	$feciniinthasta = '';
	$fecentintdesde = '';
	$fecentinthasta = '';
	$fecinipedintdesde = '';
	$fecinipedinthasta = '';
	$notint = '';
}
if($swconstructor == 0)
{
	$nomcons = '';
	$porava = '';
	$feciniconsdesde = '';
	$feciniconshasta = '';
	$fecentconsdesde = '';
	$fecentconshasta = '';
	$diascons = '';
	$notcons = '';
}
if($swseguridad == 0)
{
	$nomseg = '';
	$codmon = '';
	$fecinginiobra = '';
	$fecingfinobra = '';
	$notseg = '';
}

$fecha=date("d/m/Y");
$fechaact=date("Y/m/d H:i:s");
mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,obr_clave_int,loa_usu_actualiz,loa_fec_actualiz) values(null,29,71,'".$ultimaobra."','".$usuario."','".$fechaact."')");//Tercer campo tia_clave_int. 71=Documento Impreso
//************ESTILOS******************

$styleA1 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => '000000'),
    'size'  => 12,
    'name'  => 'Calibri'
));
$styleA2 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => 'C83000'),
    'size'  => 10,
    'name'  => 'Arial'
));
$styleA3 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => '000000'),
    'size'  => 10,
    'name'  => 'Arial'
));
$styleA4 = array(
'font'  => array(
    'bold'  => false,
    'color' => array('rgb' => '000000'),
    'size'  => 10,
    'name'  => 'Arial'
));
$styleA3p1 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => '000000'),
    'size'  => 9,
    'name'  => 'Arial'
));
$styleA4p1 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => '000000'),
    'size'  => 9,
    'name'  => 'Arial'
));

$borders = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '000000'),
		)
	),
);

//*************************************

$objPHPExcel = new PHPExcel();
$archivo = 'PAVAS - INFORME CAJEROS.xls';

//Propiedades de la hoja de excel
$objPHPExcel->getProperties()
		->setCreator("PAVAS TECNOLOGIA")
		->setLastModifiedBy("PAVAS TECNOLOGIA")
		->setTitle("Informe cajeros eliminados")
		->setSubject("Informe cajeros eliminados")
		->setDescription("Documento generado con el software Control de Materiales")
		->setKeywords("Control de Materiales")
		->setCategory("Reportes");
		
//Ancho de las Columnas
//Info basica
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
//Info secundaria
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(23);
//Inmobiliaria
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(20);
//Visita
$objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setWidth(20);
//Diseño
$objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AN')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AO')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AP')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AQ')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AR')->setWidth(20);
//Licencia
$objPHPExcel->getActiveSheet()->getColumnDimension('AS')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AT')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AU')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AV')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AW')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AX')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AY')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AZ')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BA')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BB')->setWidth(20);
//INTERVENTORIA
$objPHPExcel->getActiveSheet()->getColumnDimension('BC')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BD')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BE')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BF')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BG')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BH')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BI')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BJ')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BK')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BL')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BM')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BN')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BO')->setWidth(20);
//CONSTRUCTOR
$objPHPExcel->getActiveSheet()->getColumnDimension('BP')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BQ')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BR')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BS')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BT')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BU')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BV')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BW')->setWidth(20);
//SEGURIDAD
$objPHPExcel->getActiveSheet()->getColumnDimension('BX')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BY')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BZ')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('CA')->setWidth(20);
//FACTURAZION
$objPHPExcel->getActiveSheet()->getColumnDimension('CB')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CC')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CD')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CE')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CF')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CG')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CH')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CI')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CJ')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CK')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CL')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CM')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CN')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CO')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CP')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CQ')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CR')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CS')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CT')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CU')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CV')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CW')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CX')->setWidth(30);

//************A1**************
$objPHPExcel->getActiveSheet()->getStyle('A1')-> applyFromArray($styleA1);//
$objPHPExcel->getActiveSheet()->getCell('A1')->setValue("LISTADO GENERAL DE CAJEROS");
$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setWrapText(true); //Crea un enter entre palabras
$objPHPExcel->getActiveSheet()->mergeCells('A1:CX1');//Conbinar celdas
$objPHPExcel->getActiveSheet()->getStyle('A1:CX1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('00D8D8D8');//COLOR DE FONDO
//****************************

$swnombrecajero = 0;
$swdireccion = 0;
$swestadocajero = 0;
$swanocontable = 0;
$swregion = 0;
$swdepartamento = 0;
$swmunicipio = 0;
$swtipologia = 0;
$swintervencion = 0;
$swmodalidad = 0;
$swcodigocajero = 0;
$swcentrocostos = 0;
$swreferencia = 0;
$swubicacion = 0;
$swcodigosuc = 0;
$swubicacionatm = 0;
$swatiende = 0;
$swriesgo = 0;
$swarea = 0;
$swcodigorecibido = 0;
$swinmobiliaria = 0;
$swestadoinm = 0;
$swfechainicioinm = 0;
$swfechaentregainm = 0;
$swtotaldiasinm = 0;
$swnotasinm = 0;
$swvisita = 0;
$swestadovis = 0;
$swfechainiciovis = 0;
$swfechaentregavis = 0;
$swtotaldiasvis = 0;
$swnotasvis = 0;
$swdisenador = 0;
$swestadodis = 0;
$swfechainiciodis = 0;
$swfechaentregadis = 0;
$swtotaldiasdis = 0;
$swnotasdis = 0;
$swgestionador = 0;
$swestadolic = 0;
$swfechainiciolic = 0;
$swfechaentregalic = 0;
$swtotaldiaslic = 0;
$swnotaslic = 0;
$swinterventor = 0;
$swoperadorcanal = 0;
$swfechainicioint = 0;
$swfechaentregaint = 0;
$swfechapedido = 0;
$swnotasint = 0;
$swconstructor = 0;
$swavance = 0;
$swfechainiciocons = 0;
$swfechaentregacons = 0;
$swtotaldiascons = 0;
$swnotascons = 0;
$swinffincons = 0;
$swseguridad = 0;
$swcodigomonitoreo = 0;
$swfechaingresoobra = 0;
$swnotasseg = 0;

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('A2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('A2')->setValue("Cajero");
$objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

for($i = 1; $i <= 60; $i++)
{
	$letra = numeros_a_letras($i);
	
	if($vernombrecajero == 'true' and $swnombrecajero == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Nombre");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swnombrecajero = 1;
	}
	else
	if($verdireccion == 'true' and $swdireccion == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Dirección");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swdireccion = 1;
	}
	else
	if($veranocontable == 'true' and $swanocontable == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Año Contable");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swanocontable = 1;
	}
	else
	if($verregion == 'true' and $swregion == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Región");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swregion = 1;
	}
	else
	if($verdepartamento == 'true' and $swdepartamento == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Departamento");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swdepartamento = 1;
	}
	else
	if($vermunicipio == 'true' and $swmunicipio == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Municipío");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swmunicipio = 1;
	}
	else
	if($vertipologia == 'true' and $swtipologia == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Tipología");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swtipologia = 1;
	}
	else
	if($verintervencion == 'true' and $swintervencion == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Tipo Intervención");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swintervencion = 1;
	}
	else
	if($vermodalidad == 'true' and $swmodalidad == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Modalidad");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swmodalidad = 1;
	}
	else
	if($verestadocajero == 'true' and $swestadocajero == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Estado Proyecto");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swestadocajero = 1;
	}
	else
	if($vercodigocajero == 'true' and $swcodigocajero == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Código Cajero");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swcodigocajero = 1;
	}
	else
	if($vercentrocostos == 'true' and $swcentrocostos == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Centro Costos");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swcentrocostos = 1;
	}
	else
	if($verreferencia == 'true' and $swreferencia == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Referencia Maquina");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swreferencia = 1;
	}
	else
	if($verubicacion == 'true' and $swubicacion == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Ubicación (SUC, CC, REMOTO)");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swubicacion = 1;
	}
	else
	if($vercodigosuc == 'true' and $swcodigosuc == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Código de Suc");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swcodigosuc = 1;
	}
	else
	if($verubicacionatm == 'true' and $swubicacionatm == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Ubicación ATM");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swubicacionatm = 1;
	}
	else
	if($veratiende == 'true' and $swatiende == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Atiende");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swatiende = 1;
	}
	else
	if($verriesgo == 'true' and $swriesgo == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Riesgo");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swriesgo = 1;
	}
	else
	if($verarea == 'true' and $swarea == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Area");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swarea = 1;
	}
	else 
	if($vercodigorecibido == 'true' and $swcodigorecibido == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Fecha Apagado ATM");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swcodigorecibido = 1;
	}
	else	
	if($verinmobiliaria == 'true' and $swinmobiliaria  == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Nombre Inmobiliaria");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swinmobiliaria = 1;
	}
	else
	if($verestadoinm == 'true' and $swestadoinm == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Estado Inmobiliaria");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swestadoinm = 1;
	}
	else
	if($verfechainicioinm == 'true' and $swfechainicioinm == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Fecha Inicio Inmobiliaria");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swfechainicioinm = 1;
	}
	else
	if($verfechaentregainm == 'true' and $swfechaentregainm == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Fecha Entrega Info Inmobiliaria");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swfechaentregainm = 1;
	}
	else
	if($vertotaldiasinm == 'true' and $swtotaldiasinm == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Total Días Inmobiliaria");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swtotaldiasinm = 1;
	}
	else
	if($vernotasinm == 'true' and $swnotasinm == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Notas Inmobiliaria");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swnotasinm = 1;
	}
	else
	if($vervisita == 'true' and $swvisita == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Nombre Visitante");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swvisita = 1;
	}
	else
	if($verestadovis == 'true' and $swestadovis == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Estado Visita");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swestadovis = 1;
	}
	else
	if($verfechainiciovis == 'true' and $swfechainiciovis == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Fecha Inicio Visita");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swfechainiciovis = 1;
	}
	else
	if($verfechaentregavis == 'true' and $swfechaentregavis == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Fecha Entrega Info Visita");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swfechaentregavis = 1;
	}
	else
	if($vertotaldiasvis == 'true' and $swtotaldiasvis == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Total Días Visita");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swtotaldiasvis = 1;
	}
	else
	if($vernotasvis == 'true' and $swnotasvis == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Notas Visita");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swnotasvis = 1;
	}
	else
	if($verdisenador == 'true' and $swdisenador == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Nombre Diseñador");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swdisenador = 1;
	}
	else
	if($verestadodis == 'true' and $swestadodis == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Estado Diseño");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swestadodis = 1;
	}
	else
	if($verfechainiciodis == 'true' and $swfechainiciodis == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Fecha Inicio Diseño");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swfechainiciodis = 1;
	}
	else
	if($verfechaentregadis == 'true' and $swfechaentregadis == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Fecha Entrega Info Diseño");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swfechaentregadis = 1;
	}
	else
	if($vertotaldiasdis == 'true' and $swtotaldiasdis == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Total Días Diseño");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swtotaldiasdis = 1;
	}
	else
	if($vernotasdis == 'true' and $swnotasdis == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Notas Diseño");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swnotasdis = 1;
	}
	else
	if($vergestionador == 'true' and $swgestionador == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Nombre Gestionador");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swgestionador = 1;
	}
	else
	if($verestadolic == 'true' and $swestadolic == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Estado Licencia");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swestadolic = 1;
	}
	else
	if($verfechainiciolic == 'true' and $swfechainiciolic == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Fecha Inicio Licencia");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swfechainiciolic = 1;
	}
	else
	if($verfechaentregalic == 'true' and $swfechaentregalic == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Fecha Entrega Info Licencia");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swfechaentregalic = 1;
	}
	else
	if($vertotaldiaslic == 'true' and $swtotaldiaslic == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Total Días Licencia");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swtotaldiaslic = 1;
	}
	else
	if($vernotaslic == 'true' and $swnotaslic == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Notas Licencia");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swnotaslic = 1;
	}
	else
	if($verinterventor == 'true' and $swinterventor == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Nombre Interventor");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swinterventor = 1;
	}
	else
	if($veroperadorcanal == 'true' and $swoperadorcanal == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Operador Canal Comunicaciones");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swoperadorcanal = 1;
	}
	else
	if($verfechainicioint == 'true' and $swfechainicioint == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Fecha Inicio Obra");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swfechainicioint = 1;
	}
	else
	if($verfechaentregaint == 'true' and $swfechaentregaint == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Fecha Teorica Entrega");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swfechaentregaint = 1;
	}
	else
	if($verfechapedido == 'true' and $swfechapedido == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Fecha Pedido Suministro");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swfechapedido = 1;
	}
	else
	if($vernotasint == 'true' and $swnotasint == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Notas Interventor");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swnotasint = 1;
	}
	else
	if($verconstructor == 'true' and $swconstructor == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Nombre Constructor");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swconstructor = 1;
	}
	else
	if($veravance == 'true' and $swavance == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("% Avance Constructor");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swavance = 1;
	}
	else
	if($verfechainiciocons == 'true' and $swfechainiciocons == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Fecha Inicio Obra");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swfechainiciocons = 1;
	}
	else
	if($verfechaentregacons == 'true' and $swfechaentregacons == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Fecha Real Entrega ATM");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swfechaentregacons = 1;
	}
	else
	if($vertotaldiascons == 'true' and $swtotaldiascons == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Total Días Constructor");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swtotaldiascons = 1;
	}
	else
	if($vernotascons == 'true' and $swnotascons == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Notas Constructor");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swnotascons = 1;
	}
	else
	if($verinffincons == 'true' and $swinffincons == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Informe Final Constructor");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swinffincons = 1;
	}
	else
	if($verseguridad == 'true' and $swseguridad == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Nombre Instalador Seguridad");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swseguridad  = 1;
	}
	else
	if($vercodmon == 'true' and $swcodigomonitoreo == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Código Monitoreo");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swcodigomonitoreo  = 1;
	}
	else
	if($verfecingobra == 'true' and $swfechaingresoobra == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Fecha Ingreso Obra");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swfechaingresoobra = 1;
	}
	else
	if($vernotasseg == 'true' and $swnotasseg == 0)
	{
		//************A2**************
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')-> applyFromArray($styleA3);//
		$objPHPExcel->getActiveSheet()->getCell($letra.'2')->setValue("Notas seguridad");
		$objPHPExcel->getActiveSheet()->getStyle($letra.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
		//****************************
		$swnotasseg = 1;
	}
}


//DB->2015-07-26
$caj_estado_proyecto = obtenerSQL("caj_estado_proyecto", obtenerListaValores($est));
$caj_region = obtenerSQL("caj_region", obtenerListaValores($reg));
$caj_departamento = obtenerSQL("caj_departamento", obtenerListaValores($dep));
$caj_municipio = obtenerSQL("caj_municipio", obtenerListaValores($mun));
$tip_clave_int = obtenerSQL("tip_clave_int", obtenerListaValores($tip));
$tii_clave_int = obtenerSQL("tii_clave_int", obtenerListaValores($tipint));
$mod_clave_int = obtenerSQL("mod_clave_int", obtenerListaValores($moda));
$cco_clave_int = obtenerSQL("cco_clave_int", obtenerListaValores($cencos));
$rem_clave_int = obtenerSQL("rem_clave_int", obtenerListaValores($refmaq));
$ubi_clave_int = obtenerSQL("ubi_clave_int", obtenerListaValores($ubi));
$caj_ubicacion_atm = obtenerSQL("caj_ubicacion_atm", obtenerListaValores($ubiatm));
$ati_clave_int = obtenerSQL("ati_clave_int", obtenerListaValores($ati));
$caj_riesgo = obtenerSQL("caj_riesgo", obtenerListaValores($rie));
$are_clave_int = obtenerSQL("are_clave_int", obtenerListaValores($are));
$cai_inmobiliaria = obtenerSQL("cai_inmobiliaria", obtenerListaValores($nominm));
$esi_clave_int = obtenerSQL("esi_clave_int", obtenerListaValores($estinm));
$cav_visitante = obtenerSQL("cav_visitante", obtenerListaValores($nomvis));
$cav_estado = obtenerSQL("cav_estado", obtenerListaValores($estvis));
$cad_disenador = obtenerSQL("cad_disenador", obtenerListaValores($nomdis));
$esd_clave_int = obtenerSQL("esd_clave_int", obtenerListaValores($estdis));
$cal_gestionador = obtenerSQL("cal_gestionador", obtenerListaValores($nomges));
$esl_clave_int = obtenerSQL("esl_clave_int", obtenerListaValores($estges));
$cin_interventor = obtenerSQL("cin_interventor", obtenerListaValores($nomint));
$can_clave_int = obtenerSQL("can_clave_int", obtenerListaValores($cancom));
$cac_constructor = obtenerSQL("cac_constructor", obtenerListaValores($nomcons));
$cac_porcentaje_avance = obtenerSQL("cac_porcentaje_avance", obtenerListaValores($porava));
$cas_instalador = obtenerSQL("cas_instalador", obtenerListaValores($nomseg));

$sql = '';
if($listacajeros == '')
{
	$sql = "((c.caj_fecha_inicio = '".$fii."'  OR '".$fii."' IS NULL OR '".$fii."' = '') or ((c.caj_fecha_inicio BETWEEN '".$fii." 00:00:00' AND '".$ffi." 23:59:59') or ('".$fii."' Is Null and '".$ffi."' Is Null) or ('".$fii."' = '' and '".$ffi."' = ''))) and (((cajcons.cac_fecha_entrega_atm = '".$fie."'  OR '".$fie."' IS NULL OR '".$fie."' = '') or ((cajcons.cac_fecha_entrega_atm BETWEEN '".$fie." 00:00:00' AND '".$ffe." 23:59:59') or ('".$fie."' Is Null and '".$ffe."' Is Null) or ('".$fie."' = '' and '".$ffe."' = ''))) or ((cajint.cin_fecha_teorica_entrega = '".$fie."'  OR '".$fie."' IS NULL OR '".$fie."' = '') or ((cajint.cin_fecha_teorica_entrega BETWEEN '".$fie." 00:00:00' AND '".$ffe." 23:59:59') or ('".$fie."' Is Null and '".$ffe."' Is Null) or ('".$fie."' = '' and '".$ffe."' = '')))) and 
	(caj_nombre LIKE REPLACE('%".$nomcaj."%',' ','%') OR '".$nomcaj."' IS NULL OR '".$nomcaj."' = '') and 
	(caj_direccion LIKE REPLACE('%".$dircaj."%',' ','%') OR '".$dircaj."' IS NULL OR '".$dircaj."' = '') and 
	($caj_estado_proyecto) and
	(caj_ano_contable = '".$anocon."' OR '".$anocon."' IS NULL OR '".$anocon."' = '') and 
	($caj_region) and
	($caj_departamento) and
	($caj_municipio) and
	($tip_clave_int) and
	($tii_clave_int) and
	($mod_clave_int) and
	
	(caj_codigo_cajero LIKE REPLACE('%".$cod."%',' ','%') OR '".$cod."' IS NULL OR '".$cod."' = '') and 
	($cco_clave_int) and
	($rem_clave_int) and
	($ubi_clave_int) and
	(caj_codigo_suc LIKE REPLACE('%".$codsuc."%',' ','%') OR '".$codsuc."' IS NULL OR '".$codsuc."' = '') and 
	($caj_ubicacion_atm) and
	($ati_clave_int) and
	($caj_riesgo) and
	($are_clave_int) and
	(caj_fec_apagado_atm LIKE REPLACE('%".$fecapagadoatm."%',' ','%') OR '".$fecapagadoatm."' IS NULL OR '".$fecapagadoatm."' = '') and
	
	($cai_inmobiliaria) and
	($esi_clave_int) and
	((cai_fecha_ini_inmobiliaria = '".$feciniinmdesde."'  OR '".$feciniinmdesde."' IS NULL OR '".$feciniinmdesde."' = '') or ((cai_fecha_ini_inmobiliaria BETWEEN '".$feciniinmdesde." 00:00:00' AND '".$feciniinmhasta." 23:59:59') or ('".$feciniinmdesde."' Is Null and '".$feciniinmhasta."' Is Null) or ('".$feciniinmdesde."' = '' and '".$feciniinmhasta."' = ''))) and 
	((cai_fecha_entrega_info_inmobiliaria = '".$fecentinmdesde."'  OR '".$fecentinmdesde."' IS NULL OR '".$fecentinmdesde."' = '') or ((cai_fecha_entrega_info_inmobiliaria BETWEEN '".$fecentinmdesde." 00:00:00' AND '".$fecentinmhasta." 23:59:59') or ('".$fecentinmdesde."' Is Null and '".$fecentinmhasta."' Is Null) or ('".$fecentinmdesde."' = '' and '".$fecentinmhasta."' = ''))) and 
	(DATEDIFF(cai_fecha_entrega_info_inmobiliaria,cai_fecha_ini_inmobiliaria) = '".$diasinm."' OR '".$diasinm."' IS NULL OR '".$diasinm."' = '') and 
	
	($cav_visitante) and
	($cav_estado) and
	((cav_fecha_visita = '".$fecinivisdesde."'  OR '".$fecinivisdesde."' IS NULL OR '".$fecinivisdesde."' = '') or ((cav_fecha_visita BETWEEN '".$fecinivisdesde." 00:00:00' AND '".$fecinivishasta." 23:59:59') or ('".$fecinivisdesde."' Is Null and '".$fecinivishasta."' Is Null) or ('".$fecinivisdesde."' = '' and '".$fecinivishasta."' = ''))) and 
	((cav_fecha_entrega_informe = '".$fecentvisdesde."'  OR '".$fecentvisdesde."' IS NULL OR '".$fecentvisdesde."' = '') or ((cav_fecha_entrega_informe BETWEEN '".$fecentvisdesde." 00:00:00' AND '".$fecentvishasta." 23:59:59') or ('".$fecentvisdesde."' Is Null and '".$fecentvishasta."' Is Null) or ('".$fecentvisdesde."' = '' and '".$fecentvishasta."' = ''))) and 
	(DATEDIFF(cav_fecha_entrega_informe,cav_fecha_visita) = '".$diasvis."' OR '".$diasvis."' IS NULL OR '".$diasvis."' = '') and 

	($cad_disenador) and
	($esd_clave_int) and
	((cad_fecha_inicio_diseno = '".$fecinidisdesde."'  OR '".$fecinidisdesde."' IS NULL OR '".$fecinidisdesde."' = '') or ((cad_fecha_inicio_diseno BETWEEN '".$fecinidisdesde." 00:00:00' AND '".$fecinidishasta." 23:59:59') or ('".$fecinidisdesde."' Is Null and '".$fecinidishasta."' Is Null) or ('".$fecinidisdesde."' = '' and '".$fecinidishasta."' = ''))) and 
	((cad_fecha_entrega_info_diseno = '".$fecentdisdesde."'  OR '".$fecentdisdesde."' IS NULL OR '".$fecentdisdesde."' = '') or ((cad_fecha_entrega_info_diseno BETWEEN '".$fecentdisdesde." 00:00:00' AND '".$fecentdishasta." 23:59:59') or ('".$fecentdisdesde."' Is Null and '".$fecentdishasta."' Is Null) or ('".$fecentdisdesde."' = '' and '".$fecentdishasta."' = ''))) and 
	(DATEDIFF(cad_fecha_entrega_info_diseno,cad_fecha_inicio_diseno) = '".$diasdis."' OR '".$diasdis."' IS NULL OR '".$diasdis."' = '') and 
	
	($cal_gestionador) and
	($esl_clave_int) and
	((cal_fecha_inicio_licencia = '".$fecinigesdesde."'  OR '".$fecinigesdesde."' IS NULL OR '".$fecinigesdesde."' = '') or ((cal_fecha_inicio_licencia BETWEEN '".$fecinigesdesde." 00:00:00' AND '".$fecinigeshasta." 23:59:59') or ('".$fecinigesdesde."' Is Null and '".$fecinigeshasta."' Is Null) or ('".$fecinigesdesde."' = '' and '".$fecinigeshasta."' = ''))) and 
	((cal_fecha_entrega_info_licencia = '".$fecentgesdesde."'  OR '".$fecentgesdesde."' IS NULL OR '".$fecentgesdesde."' = '') or ((cal_fecha_entrega_info_licencia BETWEEN '".$fecentgesdesde." 00:00:00' AND '".$fecentgeshasta." 23:59:59') or ('".$fecentgesdesde."' Is Null and '".$fecentgeshasta."' Is Null) or ('".$fecentgesdesde."' = '' and '".$fecentgeshasta."' = ''))) and 
	(DATEDIFF(cal_fecha_entrega_info_licencia,cal_fecha_inicio_licencia) = '".$diasges."' OR '".$diasges."' IS NULL OR '".$diasges."' = '') and 
				
	($cin_interventor) and
	($can_clave_int) and
	((cin_fecha_inicio_obra = '".$feciniintdesde."'  OR '".$feciniintdesde."' IS NULL OR '".$feciniintdesde."' = '') or ((cin_fecha_inicio_obra BETWEEN '".$feciniintdesde." 00:00:00' AND '".$feciniinthasta." 23:59:59') or ('".$feciniintdesde."' Is Null and '".$feciniinthasta."' Is Null) or ('".$feciniintdesde."' = '' and '".$feciniinthasta."' = ''))) and 
	((cin_fecha_teorica_entrega = '".$fecentintdesde."'  OR '".$fecentintdesde."' IS NULL OR '".$fecentintdesde."' = '') or ((cin_fecha_teorica_entrega BETWEEN '".$fecentintdesde." 00:00:00' AND '".$fecentinthasta." 23:59:59') or ('".$fecentintdesde."' Is Null and '".$fecentinthasta."' Is Null) or ('".$fecentintdesde."' = '' and '".$fecentinthasta."' = ''))) and 
	((cin_fecha_pedido_suministro = '".$fecinipedintdesde."'  OR '".$fecinipedintdesde."' IS NULL OR '".$fecinipedintdesde."' = '') or ((cin_fecha_pedido_suministro BETWEEN '".$fecinipedintdesde." 00:00:00' AND '".$fecinipedinthasta." 23:59:59') or ('".$fecinipedintdesde."' Is Null and '".$fecinipedinthasta."' Is Null) or ('".$fecinipedintdesde."' = '' and '".$fecinipedinthasta."' = ''))) and 
				
	($cac_constructor) and
	($cac_porcentaje_avance) and
	((cin_fecha_inicio_obra = '".$feciniconsdesde."'  OR '".$feciniconsdesde."' IS NULL OR '".$feciniconsdesde."' = '') or ((cin_fecha_inicio_obra BETWEEN '".$feciniconsdesde." 00:00:00' AND '".$feciniconshasta." 23:59:59') or ('".$feciniconsdesde."' Is Null and '".$feciniconshasta."' Is Null) or ('".$feciniconsdesde."' = '' and '".$feciniconshasta."' = ''))) and 
	((cac_fecha_entrega_atm = '".$fecentconsdesde."'  OR '".$fecentconsdesde."' IS NULL OR '".$fecentconsdesde."' = '') or ((cac_fecha_entrega_atm BETWEEN '".$fecentconsdesde." 00:00:00' AND '".$fecentconshasta." 23:59:59') or ('".$fecentconsdesde."' Is Null and '".$fecentconshasta."' Is Null) or ('".$fecentconsdesde."' = '' and '".$fecentconshasta."' = ''))) and 
	(DATEDIFF(cac_fecha_entrega_atm,cin_fecha_inicio_obra) = '".$diascons."' OR '".$diascons."' IS NULL OR '".$diascons."' = '') and 
	
	($cas_instalador) and
	((cas_fecha_ingreso_obra_inst = '".$fecinginiobra."'  OR '".$fecinginiobra."' IS NULL OR '".$fecinginiobra."' = '') or ((cas_fecha_ingreso_obra_inst BETWEEN '".$fecinginiobra." 00:00:00' AND '".$fecingfinobra." 23:59:59') or ('".$fecinginiobra."' Is Null and '".$fecingfinobra."' Is Null) or ('".$fecinginiobra."' = '' and '".$fecingfinobra."' = ''))) and 
	(cas_codigo_monitoreo LIKE REPLACE('%".$codmon."%',' ','%') OR '".$codmon."' IS NULL OR '".$codmon."' = '') and 
	
	c.caj_sw_eliminado = 0";
	
	if($notinm != '')
	{
		$sql1 = "and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 7 and nou_nota LIKE REPLACE('%".$notinm."%',' ','%')))";
	}
	if($notvis != '')
	{
		$sql1 = "and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 18 and nou_nota LIKE REPLACE('%".$notvis."%',' ','%')))";
	}
	if($notdis != '')
	{
		$sql1 = "and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 8 and nou_nota LIKE REPLACE('%".$notdis."%',' ','%')))";
	}
	if($notges != '')
	{
		if($sql1 != ''){ $sql1 = $sql1." and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 19 and nou_nota LIKE REPLACE('%".$notges."%',' ','%')))"; }else{ $sql1 = "and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 19 and nou_nota LIKE REPLACE('%".$notges."%',' ','%')))"; }
	}
	if($notint != '')
	{
		if($sql1 != ''){ $sql1 = $sql1." and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 9 and nou_nota LIKE REPLACE('%".$notint."%',' ','%')))"; }else{ $sql1 = "and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 9 and nou_nota LIKE REPLACE('%".$notint."%',' ','%')))"; }
	}
	if($notcons != '')
	{
		if($sql1 != ''){ $sql1 = $sql1." and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 6 and nou_nota LIKE REPLACE('%".$notcons."%',' ','%')))"; }else{ $sql1 = "and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 6 and nou_nota LIKE REPLACE('%".$notcons."%',' ','%')))"; }
	}
	if($notseg != '')
	{
		if($sql1 != ''){ $sql1 = $sql1." and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 10 and nou_nota LIKE REPLACE('%".$notseg."%',' ','%')))"; }else{ $sql1 = "and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 10 and nou_nota LIKE REPLACE('%".$notseg."%',' ','%')))"; }
	}
	if($notdis == '' and $notges == '' and $notint == '' and $notcons == '')
	{
		$sql1 = "and c.caj_clave_int IS NOT NULL";
	}
}
else
{
	$sql = "c.caj_clave_int in (".$listacajeros.") and ((c.caj_fecha_inicio = '".$fii."'  OR '".$fii."' IS NULL OR '".$fii."' = '') or ((c.caj_fecha_inicio BETWEEN '".$fii." 00:00:00' AND '".$ffi." 23:59:59') or ('".$fii."' Is Null and '".$ffi."' Is Null) or ('".$fii."' = '' and '".$ffi."' = ''))) and (((cajcons.cac_fecha_entrega_atm = '".$fie."'  OR '".$fie."' IS NULL OR '".$fie."' = '') or ((cajcons.cac_fecha_entrega_atm BETWEEN '".$fie." 00:00:00' AND '".$ffe." 23:59:59') or ('".$fie."' Is Null and '".$ffe."' Is Null) or ('".$fie."' = '' and '".$ffe."' = ''))) or ((cajint.cin_fecha_teorica_entrega = '".$fie."'  OR '".$fie."' IS NULL OR '".$fie."' = '') or ((cajint.cin_fecha_teorica_entrega BETWEEN '".$fie." 00:00:00' AND '".$ffe." 23:59:59') or ('".$fie."' Is Null and '".$ffe."' Is Null) or ('".$fie."' = '' and '".$ffe."' = '')))) and 
	(caj_nombre LIKE REPLACE('%".$nomcaj."%',' ','%') OR '".$nomcaj."' IS NULL OR '".$nomcaj."' = '') and 
	(caj_direccion LIKE REPLACE('%".$dircaj."%',' ','%') OR '".$dircaj."' IS NULL OR '".$dircaj."' = '') and 
	($caj_estado_proyecto) and
	(caj_ano_contable = '".$anocon."' OR '".$anocon."' IS NULL OR '".$anocon."' = '') and 
	($caj_region) and
	($caj_departamento) and
	($caj_municipio) and
	($tip_clave_int) and
	($tii_clave_int) and
	($mod_clave_int) and
	
	(caj_codigo_cajero LIKE REPLACE('%".$cod."%',' ','%') OR '".$cod."' IS NULL OR '".$cod."' = '') and 
	($cco_clave_int) and
	($rem_clave_int) and
	($ubi_clave_int) and
	(caj_codigo_suc LIKE REPLACE('%".$codsuc."%',' ','%') OR '".$codsuc."' IS NULL OR '".$codsuc."' = '') and 
	(caj_ubicacion_atm = '".$ubiatm."' OR '".$ubiatm."' IS NULL OR '".$ubiatm."' = '') and 
	($ati_clave_int) and
	($caj_riesgo) and
	($are_clave_int) and
	(caj_fec_apagado_atm LIKE REPLACE('%".$fecapagadoatm."%',' ','%') OR '".$fecapagadoatm."' IS NULL OR '".$fecapagadoatm."' = '') and
	
	($cai_inmobiliaria) and
	($esi_clave_int) and
	((cai_fecha_ini_inmobiliaria = '".$feciniinmdesde."'  OR '".$feciniinmdesde."' IS NULL OR '".$feciniinmdesde."' = '') or ((cai_fecha_ini_inmobiliaria BETWEEN '".$feciniinmdesde." 00:00:00' AND '".$feciniinmhasta." 23:59:59') or ('".$feciniinmdesde."' Is Null and '".$feciniinmhasta."' Is Null) or ('".$feciniinmdesde."' = '' and '".$feciniinmhasta."' = ''))) and 
	((cai_fecha_entrega_info_inmobiliaria = '".$fecentinmdesde."'  OR '".$fecentinmdesde."' IS NULL OR '".$fecentinmdesde."' = '') or ((cai_fecha_entrega_info_inmobiliaria BETWEEN '".$fecentinmdesde." 00:00:00' AND '".$fecentinmhasta." 23:59:59') or ('".$fecentinmdesde."' Is Null and '".$fecentinmhasta."' Is Null) or ('".$fecentinmdesde."' = '' and '".$fecentinmhasta."' = ''))) and 
	(DATEDIFF(cai_fecha_entrega_info_inmobiliaria,cai_fecha_ini_inmobiliaria) = '".$diasinm."' OR '".$diasinm."' IS NULL OR '".$diasinm."' = '') and 
	
	($cav_visitante) and
	($cav_estado) and
	((cav_fecha_visita = '".$fecinivisdesde."'  OR '".$fecinivisdesde."' IS NULL OR '".$fecinivisdesde."' = '') or ((cav_fecha_visita BETWEEN '".$fecinivisdesde." 00:00:00' AND '".$fecinivishasta." 23:59:59') or ('".$fecinivisdesde."' Is Null and '".$fecinivishasta."' Is Null) or ('".$fecinivisdesde."' = '' and '".$fecinivishasta."' = ''))) and 
	((cav_fecha_entrega_informe = '".$fecentvisdesde."'  OR '".$fecentvisdesde."' IS NULL OR '".$fecentvisdesde."' = '') or ((cav_fecha_entrega_informe BETWEEN '".$fecentvisdesde." 00:00:00' AND '".$fecentvishasta." 23:59:59') or ('".$fecentvisdesde."' Is Null and '".$fecentvishasta."' Is Null) or ('".$fecentvisdesde."' = '' and '".$fecentvishasta."' = ''))) and 
	(DATEDIFF(cav_fecha_entrega_informe,cav_fecha_visita) = '".$diasvis."' OR '".$diasvis."' IS NULL OR '".$diasvis."' = '') and 
	
	($cad_disenador) and
	($esd_clave_int) and
	((cad_fecha_inicio_diseno = '".$fecinidisdesde."'  OR '".$fecinidisdesde."' IS NULL OR '".$fecinidisdesde."' = '') or ((cad_fecha_inicio_diseno BETWEEN '".$fecinidisdesde." 00:00:00' AND '".$fecinidishasta." 23:59:59') or ('".$fecinidisdesde."' Is Null and '".$fecinidishasta."' Is Null) or ('".$fecinidisdesde."' = '' and '".$fecinidishasta."' = ''))) and 
	((cad_fecha_entrega_info_diseno = '".$fecentdisdesde."'  OR '".$fecentdisdesde."' IS NULL OR '".$fecentdisdesde."' = '') or ((cad_fecha_entrega_info_diseno BETWEEN '".$fecentdisdesde." 00:00:00' AND '".$fecentdishasta." 23:59:59') or ('".$fecentdisdesde."' Is Null and '".$fecentdishasta."' Is Null) or ('".$fecentdisdesde."' = '' and '".$fecentdishasta."' = ''))) and 
	(DATEDIFF(cad_fecha_entrega_info_diseno,cad_fecha_inicio_diseno) = '".$diasdis."' OR '".$diasdis."' IS NULL OR '".$diasdis."' = '') and 
	
	($cal_gestionador) and
	($esl_clave_int) and
	((cal_fecha_inicio_licencia = '".$fecinigesdesde."'  OR '".$fecinigesdesde."' IS NULL OR '".$fecinigesdesde."' = '') or ((cal_fecha_inicio_licencia BETWEEN '".$fecinigesdesde." 00:00:00' AND '".$fecinigeshasta." 23:59:59') or ('".$fecinigesdesde."' Is Null and '".$fecinigeshasta."' Is Null) or ('".$fecinigesdesde."' = '' and '".$fecinigeshasta."' = ''))) and 
	((cal_fecha_entrega_info_licencia = '".$fecentgesdesde."'  OR '".$fecentgesdesde."' IS NULL OR '".$fecentgesdesde."' = '') or ((cal_fecha_entrega_info_licencia BETWEEN '".$fecentgesdesde." 00:00:00' AND '".$fecentgeshasta." 23:59:59') or ('".$fecentgesdesde."' Is Null and '".$fecentgeshasta."' Is Null) or ('".$fecentgesdesde."' = '' and '".$fecentgeshasta."' = ''))) and 
	(DATEDIFF(cal_fecha_entrega_info_licencia,cal_fecha_inicio_licencia) = '".$diasges."' OR '".$diasges."' IS NULL OR '".$diasges."' = '') and 
				
	($cin_interventor) and
	($can_clave_int) and
	((cin_fecha_inicio_obra = '".$feciniintdesde."'  OR '".$feciniintdesde."' IS NULL OR '".$feciniintdesde."' = '') or ((cin_fecha_inicio_obra BETWEEN '".$feciniintdesde." 00:00:00' AND '".$feciniinthasta." 23:59:59') or ('".$feciniintdesde."' Is Null and '".$feciniinthasta."' Is Null) or ('".$feciniintdesde."' = '' and '".$feciniinthasta."' = ''))) and 
	((cin_fecha_teorica_entrega = '".$fecentintdesde."'  OR '".$fecentintdesde."' IS NULL OR '".$fecentintdesde."' = '') or ((cin_fecha_teorica_entrega BETWEEN '".$fecentintdesde." 00:00:00' AND '".$fecentinthasta." 23:59:59') or ('".$fecentintdesde."' Is Null and '".$fecentinthasta."' Is Null) or ('".$fecentintdesde."' = '' and '".$fecentinthasta."' = ''))) and 
	((cin_fecha_pedido_suministro = '".$fecinipedintdesde."'  OR '".$fecinipedintdesde."' IS NULL OR '".$fecinipedintdesde."' = '') or ((cin_fecha_pedido_suministro BETWEEN '".$fecinipedintdesde." 00:00:00' AND '".$fecinipedinthasta." 23:59:59') or ('".$fecinipedintdesde."' Is Null and '".$fecinipedinthasta."' Is Null) or ('".$fecinipedintdesde."' = '' and '".$fecinipedinthasta."' = ''))) and 
				
	($cac_constructor) and
	($cac_porcentaje_avance) and
	((cin_fecha_inicio_obra = '".$feciniconsdesde."'  OR '".$feciniconsdesde."' IS NULL OR '".$feciniconsdesde."' = '') or ((cin_fecha_inicio_obra BETWEEN '".$feciniconsdesde." 00:00:00' AND '".$feciniconshasta." 23:59:59') or ('".$feciniconsdesde."' Is Null and '".$feciniconshasta."' Is Null) or ('".$feciniconsdesde."' = '' and '".$feciniconshasta."' = ''))) and 
	((cac_fecha_entrega_atm = '".$fecentconsdesde."'  OR '".$fecentconsdesde."' IS NULL OR '".$fecentconsdesde."' = '') or ((cac_fecha_entrega_atm BETWEEN '".$fecentconsdesde." 00:00:00' AND '".$fecentconshasta." 23:59:59') or ('".$fecentconsdesde."' Is Null and '".$fecentconshasta."' Is Null) or ('".$fecentconsdesde."' = '' and '".$fecentconshasta."' = ''))) and 
	(DATEDIFF(cac_fecha_entrega_atm,cin_fecha_inicio_obra) = '".$diascons."' OR '".$diascons."' IS NULL OR '".$diascons."' = '') and 
	
	($cas_instalador) and
	((cas_fecha_ingreso_obra_inst = '".$fecinginiobra."'  OR '".$fecinginiobra."' IS NULL OR '".$fecinginiobra."' = '') or ((cas_fecha_ingreso_obra_inst BETWEEN '".$fecinginiobra." 00:00:00' AND '".$fecingfinobra." 23:59:59') or ('".$fecinginiobra."' Is Null and '".$fecingfinobra."' Is Null) or ('".$fecinginiobra."' = '' and '".$fecingfinobra."' = ''))) and 
	(cas_codigo_monitoreo LIKE REPLACE('%".$codmon."%',' ','%') OR '".$codmon."' IS NULL OR '".$codmon."' = '') and 
	
	c.caj_sw_eliminado = 0";
	
	if($notinm != '')
	{
		$sql1 = "and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 7 and nou_nota LIKE REPLACE('%".$notinm."%',' ','%')))";
	}
	if($notvis != '')
	{
		$sql1 = "and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 18 and nou_nota LIKE REPLACE('%".$notvis."%',' ','%')))";
	}
	if($notdis != '')
	{
		$sql1 = "and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 8 and nou_nota LIKE REPLACE('%".$notdis."%',' ','%')))";
	}
	if($notges != '')
	{
		if($sql1 != ''){ $sql1 = $sql1." and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 19 and nou_nota LIKE REPLACE('%".$notges."%',' ','%')))"; }else{ $sql1 = "and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 19 and nou_nota LIKE REPLACE('%".$notges."%',' ','%')))"; }
	}
	if($notint != '')
	{
		if($sql1 != ''){ $sql1 = $sql1." and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 9 and nou_nota LIKE REPLACE('%".$notint."%',' ','%')))"; }else{ $sql1 = "and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 9 and nou_nota LIKE REPLACE('%".$notint."%',' ','%')))"; }
	}
	if($notcons != '')
	{
		if($sql1 != ''){ $sql1 = $sql1." and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 6 and nou_nota LIKE REPLACE('%".$notcons."%',' ','%')))"; }else{ $sql1 = "and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 6 and nou_nota LIKE REPLACE('%".$notcons."%',' ','%')))"; }
	}
	if($notseg != '')
	{
		if($sql1 != ''){ $sql1 = $sql1." and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 10 and nou_nota LIKE REPLACE('%".$notseg."%',' ','%')))"; }else{ $sql1 = "and (c.caj_clave_int in (select caj_clave_int from notausuario where ven_clave_int = 10 and nou_nota LIKE REPLACE('%".$notseg."%',' ','%')))"; }
	}
	if($notdis == '' and $notges == '' and $notint == '' and $notcons == '')
	{
		$sql1 = "and c.caj_clave_int IS NOT NULL";
	}
}

$con = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int) inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where ".$sql." ".$sql1."");
$num = mysqli_num_rows($con);

$j = 3;
for($i = 0; $i < $num; $i++)
{
	$dato = mysqli_fetch_array($con);
	//Info Base
	$clacaj = $dato['clacaj'];
	$nomcaj = $dato['caj_nombre'];
	$dir = $dato['caj_direccion'];
	$anocon = $dato['caj_ano_contable'];
	$reg = $dato['caj_region'];
	$dep = $dato['caj_departamento'];
	$mun = $dato['caj_municipio'];
	$tip = $dato['tip_clave_int'];
	$tipint = $dato['tii_clave_int'];
	$mod = $dato['mod_clave_int'];
	$conmod = mysqli_query($conectar,"select * from modalidad where mod_clave_int = '".$mod."'");
	$datomod = mysqli_fetch_array($conmod);
	$moda = $datomod['mod_nombre'];
	$swlic = $datomod['mod_sw_licencia'];
	$dinm = $datomod['mod_dias_inmobiliaria'];
	$dvis = $datomod['mod_dias_visita'];
	$ddis = $datomod['mod_dias_diseno'];
	$dlic = $datomod['mod_dias_licencia'];
	$dcom = $datomod['mod_dias_aprocomite'];
	$dcon = $datomod['mod_dias_contrato'];
	$dpre = $datomod['mod_dias_preliminar'];
	$dcons = $datomod['mod_dias_constructor'];
	if($dinm == ''){ $dinm = 0; }
	if($dvis == ''){ $dvis = 0; }
	if($ddis == ''){ $ddis = 0; }
	if($dlic == ''){ $dlic = 0; }
	if($dcom == ''){ $dcom = 0; }
	if($dcon == ''){ $dcon = 0; }
	if($dpre == ''){ $dpre = 0; }
	if($dcons == ''){ $dcons = 0; }
	//Info Secun
	$codcaj = $dato['caj_codigo_cajero'];
	$cencos = $dato['cco_clave_int'];
	$refmaq = $dato['rem_clave_int'];
	$ubi = $dato['ubi_clave_int'];
	$codsuc = $dato['caj_codigo_suc'];
	$ubiamt = $dato['caj_ubicacion_atm'];
	$ati = $dato['ati_clave_int'];
	$rie = $dato['caj_riesgo'];
	$are = $dato['are_clave_int'];
	$fecapagadoatm = $dato['caj_fec_apagado_atm'];
	$feccreacion = $dato['caj_fecha_creacion'];
	//Info Inmobiliaria
	$reqinm = $dato['cai_req_inmobiliaria'];
	$inmob = $dato['cai_inmobiliaria'];
	$feciniinmob = $dato['cai_fecha_ini_inmobiliaria'];
	$estinmob = $dato['esi_clave_int'];
	$fecentinmob = $dato['cai_fecha_entrega_info_inmobiliaria'];
	if($fecentinmob != '' and $fecentinmob != '0000-00-00')
	{
		$condiasinm = mysqli_query($conectar,"select DATEDIFF('".$fecentinmob."','".$feciniinmob."') dias from usuario LIMIT 1");
		$datodiasinm = mysqli_fetch_array($condiasinm);
		$diasinm = $datodiasinm['dias'];			
	}
	else
	{
		$confecteoinm = mysqli_query($conectar,"select ADDDATE('".$feciniinmob."', INTERVAL ".$dinm." DAY) dias from usuario LIMIT 1");
		$datofecteoinm = mysqli_fetch_array($confecteoinm);
		$fecteoinm = $datofecteoinm['dias'];

		$condiasinm = mysqli_query($conectar,"select DATEDIFF('".$fecteoinm."','".$feciniinmob."') dias from usuario LIMIT 1");
		$datodiasinm = mysqli_fetch_array($condiasinm);
		$diasinm = $datodiasinm['dias'];
	}
	//Info Visita Local
	$reqvis = $dato['cav_req_visita_local'];
	$vis = $dato['cav_visitante'];
	$fecvis = $dato['cav_fecha_visita'];
	$fecentvis = $dato['cav_fecha_entrega_informe'];
	$estvis = $dato['cav_estado'];
	if($fecentvis != '' and $fecentvis != '0000-00-00')
	{
		$condiasvis = mysqli_query($conectar,"select DATEDIFF('".$fecentvis."','".$fecvis."') dias from usuario LIMIT 1");
		$datodiasvis = mysqli_fetch_array($condiasvis);
		$diasvis = $datodiasvis['dias'];
	}
	else
	{
		$confecteovis = mysqli_query($conectar,"select ADDDATE('".$fecvis."', INTERVAL ".$dvis." DAY) dias from usuario LIMIT 1");
		$datofecteovis = mysqli_fetch_array($confecteovis);
		$fecteovis = $datofecteovis['dias'];

		$condiasvis = mysqli_query($conectar,"select DATEDIFF('".$fecteovis."','".$fecvis."') dias from usuario LIMIT 1");
		$datodiasvis = mysqli_fetch_array($condiasvis);
		$diasvis = $datodiasvis['dias'];
	}
	
	//Info Diseño
	$reqdis = $dato['cad_req_diseno'];
	$dis = $dato['cad_disenador'];
	$fecinidis = $dato['cad_fecha_inicio_diseno'];
	$estdis = $dato['esd_clave_int'];
	$fecentdis = $dato['cad_fecha_entrega_info_diseno'];
	if($fecentdis != '' and $fecentdis != '0000-00-00')
	{
		$condiasdis = mysqli_query($conectar,"select DATEDIFF('".$fecentdis."','".$fecinidis."') dias from usuario LIMIT 1");
	}
	else
	{
		$condiasdis = mysqli_query($conectar,"select DATEDIFF('".$fecteodis."','".$fecinidis."') dias from usuario LIMIT 1");
	}
	$datodiasdis = mysqli_fetch_array($condiasdis);
	$diasdis = $datodiasdis['dias'];
	//Info Licencia
	$reqlic = $dato['cal_req_licencia'];
	$lic = $dato['cal_gestionador'];
	$fecinilic = $dato['cal_fecha_inicio_licencia'];
	$estlic = $dato['esl_clave_int'];
	$fecentlic = $dato['cal_fecha_entrega_info_licencia'];
	if($fecentlic != '' and $fecentlic != '0000-00-00')
	{
		$condiaslic = mysqli_query($conectar,"select DATEDIFF('".$fecentlic."','".$fecinilic."') dias from usuario LIMIT 1");
	}
	else
	{
		$confecteolic = mysqli_query($conectar,"select ADDDATE('".$fecinilic."', INTERVAL ".$dlic." DAY) dias from usuario LIMIT 1");
		$datofecteolic = mysqli_fetch_array($confecteolic);
		$fecteolic = $datofecteolic['dias'];

		$condiaslic = mysqli_query($conectar,"select DATEDIFF('".$fecteolic."','".$fecinilic."') dias from usuario LIMIT 1");
	}
	$datodiaslic = mysqli_fetch_array($condiaslic);
	$diaslic = $datodiaslic['dias'];
	//Info Interventoria
	$int = $dato['cin_interventor'];
	$fecteoent = $dato['cin_fecha_teorica_entrega'];
	$feciniobra = $dato['cin_fecha_inicio_obra'];
	$fecpedsum = $dato['cin_fecha_pedido_suministro'];
	$aprovcotiz = $dato['cin_sw_aprov_cotizacion'];
	$cancom = $dato['can_clave_int'];
	$aprovliqui = $dato['cin_sw_aprov_liquidacion'];
	$swimgnex = $dato['cin_sw_img_nexos'];
	$swfot = $dato['cin_sw_fotos'];
	$swact = $dato['cin_sw_actas'];
	$estpro = $dato['caj_estado_proyecto'];
	//Info Constructor
	$cons = $dato['cac_constructor'];
	$fecentcons = $dato['cac_fecha_entrega_atm'];
	$poravance = $dato['cac_porcentaje_avance'];
	$fecentcot = $dato['cac_fecha_entrega_cotizacion'];
	$fecentliq = $dato['cac_fecha_entrega_liquidacion'];
	if($fecentcons != '' and $fecentcons != '0000-00-00')
	{
		$condiascons = mysqli_query($conectar,"select DATEDIFF('".$fecentcons."','".$feciniobra."') dias from usuario LIMIT 1");
		$datodiascons = mysqli_fetch_array($condiascons);
		$dcons = $datodiascons['dias'];
	}
	//Info Instalador Seguridad
	$seg = $dato['cas_instalador'];
	$fecingseg = $dato['cas_fecha_ingreso_obra_inst'];
	$codmon = $dato['cas_codigo_monitoreo'];
	
	$sql = mysqli_query($conectar,"select pog_nombre from posicion_geografica where pog_clave_int = '".$reg."'");
	$datosql = mysqli_fetch_array($sql);
	$nomreg = $datosql['pog_nombre'];
	
	$sql = mysqli_query($conectar,"select pog_nombre from posicion_geografica where pog_clave_int = '".$dep."'");
	$datosql = mysqli_fetch_array($sql);
	$nomdep = $datosql['pog_nombre'];
	
	$sql = mysqli_query($conectar,"select pog_nombre from posicion_geografica where pog_clave_int = '".$mun."'");
	$datosql = mysqli_fetch_array($sql);
	$nommun = $datosql['pog_nombre'];
	
	$sql = mysqli_query($conectar,"select tip_nombre from tipologia where tip_clave_int = '".$tip."'");
	$datosql = mysqli_fetch_array($sql);
	$nomtip = $datosql['tip_nombre'];
				
	$sql = mysqli_query($conectar,"select tii_nombre from tipointervencion where tii_clave_int = '".$tipint."'");
	$datosql = mysqli_fetch_array($sql);
	$nomtii = $datosql['tii_nombre'];
				
	if($mod <= 0){ $modalidad = "Sin Definir"; }else{ $modalidad = $moda; }
	
	if($estpro == 1){ $estadocajero = "ACTIVO"; }elseif($estpro == 2){ $estadocajero = "SUSPENDIDO"; }elseif($estpro == 3){ $estadocajero = "ENTREGADO"; }elseif($estpro == 4){ $estadocajero = "CANCELADO"; }elseif($estpro == 5){ $estadocajero = "PROGRAMADO"; }
	
	$concc = mysqli_query($conectar,"select cco_nombre from centrocostos where cco_clave_int = '".$cencos."'");
	$datocc = mysqli_fetch_array($concc);
	$nomcc = $datocc['cco_nombre'];
	
	$conref = mysqli_query($conectar,"select rem_nombre from referenciamaquina where rem_clave_int = '".$refmaq."'");
	$datoref = mysqli_fetch_array($conref);
	$nomref = $datoref['rem_nombre'];
	
	$conubi = mysqli_query($conectar,"select ubi_descripcion from ubicacion where ubi_clave_int = '".$ubi."'");
	$datoubi = mysqli_fetch_array($conubi);
	$nomubi = $datoubi['ubi_descripcion'];
	
	if($ubiamt == 1){ $nomubiatm = 'EXTERNO'; }elseif($ubiamt == 2){ $nomubiatm = 'INTERNO'; }else{ $nomubiatm = 'N/A'; }
	
	$conati = mysqli_query($conectar,"select ati_nombre from atiende where ati_clave_int = '".$ati."'");
	$datoati = mysqli_fetch_array($conati);
	$nomatiende = $datoati['ati_nombre'];
	
	if($rie == 1){ $riesgo = 'ALTO'; }elseif($rie == 2){ $riesgo = 'BAJO'; }else{ $riesgo = 'N/A'; }
	
	$conare = mysqli_query($conectar,"select are_nombre from area where are_clave_int = '".$are."'");
	$datoare = mysqli_fetch_array($conare);
	$nomarea = $datoare['are_nombre'];
	
	$coninm = mysqli_query($conectar,"select usu_nombre from usuario where usu_clave_int = '".$inmob."'");
	$datoinm = mysqli_fetch_array($coninm);
	$nominmobiliaria = $datoinm['usu_nombre'];
	
	$conestinmob = mysqli_query($conectar,"select esi_nombre from estado_inmobiliaria where esi_clave_int = '".$estinmob."'");
	$datoestinmob = mysqli_fetch_array($conestinmob);
	$estinmobiliaria = $datoestinmob['esi_nombre'];
	
	$connotinm = mysqli_query($conectar,"select nou_nota from notausuario where ven_clave_int = 7 and caj_clave_int = ".$clacaj."");
	$numnotinm = mysqli_num_rows($connotinm);
	$mennotinm = "";
	for($jj = 1; $jj <= $numnotinm; $jj++)
	{
		$datonotinm = mysqli_fetch_array($connotinm);
		$mennotinm = $jj.".".$datonotinm['nou_nota'].", ".$mennotinm;
	}
	
	$convis = mysqli_query($conectar,"select usu_nombre from usuario where usu_clave_int = '".$vis."'");
	$datovis = mysqli_fetch_array($coninm);
	$nomvisitante = $datovis['usu_nombre'];
	
	if($estvis == 1){ $estvisita = 'Activo'; }elseif($estvis == 2){ $estvisita = 'Entregado'; }
	
	$connotvis = mysqli_query($conectar,"select nou_nota from notausuario where ven_clave_int = 18 and caj_clave_int = ".$clacaj."");
	$numnotvis = mysqli_num_rows($connotvis);
	$mennotvis = "";
	for($jj = 1; $jj <= $numnotvis; $jj++)
	{
		$datonotvis = mysqli_fetch_array($connotvis);
		$mennotvis = $jj.".".$datonotvis['nou_nota'].", ".$mennotvis;
	}
	
	$condis = mysqli_query($conectar,"select usu_nombre from usuario where usu_clave_int = '".$dis."'");
	$datodis = mysqli_fetch_array($condis);
	$nomdisenador = $datodis['usu_nombre'];
	
	$conestdis = mysqli_query($conectar,"select esd_nombre from estado_diseno where esd_clave_int = '".$estdis."'");
	$datoestdis = mysqli_fetch_array($conestdis);
	$estdiseno = $datoestdis['esd_nombre'];
	
	$connotdis = mysqli_query($conectar,"select nou_nota from notausuario where ven_clave_int = 8 and caj_clave_int = ".$clacaj."");
	$numnotdis = mysqli_num_rows($connotdis);
	$mennotdis = "";
	for($jj = 1; $jj <= $numnotdis; $jj++)
	{
		$datonotdis = mysqli_fetch_array($connotdis);
		$mennotdis = $jj.".".$datonotdis['nou_nota'].", ".$mennotdis;
	}
	
	$conlic = mysqli_query($conectar,"select usu_nombre from usuario where usu_clave_int = '".$lic."'");
	$datolic = mysqli_fetch_array($conlic);
	$nomgestionador = $datolic['usu_nombre'];
	
	$conestlic = mysqli_query($conectar,"select esl_nombre from estado_licencia where esl_clave_int = '".$estlic."'");
	$datoestlic = mysqli_fetch_array($conestlic);
	$estlicencia = $datoestlic['esl_nombre'];
	
	$connotlic = mysqli_query($conectar,"select nou_nota from notausuario where ven_clave_int = 19 and caj_clave_int = ".$clacaj."");
	$numnotlic = mysqli_num_rows($connotlic);
	$mennotlic = "";
	for($jj = 1; $jj <= $numnotlic; $jj++)
	{
		$datonotlic = mysqli_fetch_array($connotlic);
		$mennotlic = $jj.".".$datonotlic['nou_nota'].", ".$mennotlic;
	}
	
	$conint = mysqli_query($conectar,"select usu_nombre from usuario where usu_clave_int = '".$int."'");
	$datoint = mysqli_fetch_array($conint);
	$nominterventor = $datoint['usu_nombre'];
	
	$concancom = mysqli_query($conectar,"select can_nombre from canal_comunicacion where can_clave_int = '".$cancom."'");
	$datocancom = mysqli_fetch_array($concancom);
	$canalcomunicacion = $datocancom['can_nombre'];
	
	$connotint = mysqli_query($conectar,"select nou_nota from notausuario where ven_clave_int = 9 and caj_clave_int = ".$clacaj."");
	$numnotint = mysqli_num_rows($connotint);
	$mennotint = "";
	for($jj = 1; $jj <= $numnotint; $jj++)
	{
		$datonotint = mysqli_fetch_array($connotint);
		$mennotint = $jj.".".$datonotint['nou_nota'].", ".$mennotint;
	}
	
	$concons = mysqli_query($conectar,"select usu_nombre from usuario where usu_clave_int = '".$cons."'");
	$datocons = mysqli_fetch_array($concons);
	$nomconstructor = $datocons['usu_nombre'];
	
	$connotcons = mysqli_query($conectar,"select nou_nota from notausuario where ven_clave_int = 6 and caj_clave_int = ".$clacaj."");
	$numnotcons = mysqli_num_rows($connotcons);
	$mennotcons = "";
	for($jj = 1; $jj <= $numnotcons; $jj++)
	{
		$datonotcons = mysqli_fetch_array($connotcons);
		$mennotcons = $jj.".".$datonotcons['nou_nota'].", ".$mennotcons;
	}
	
	$conseg = mysqli_query($conectar,"select usu_nombre from usuario where usu_clave_int = '".$seg."'");
	$datoseg = mysqli_fetch_array($conseg);
	$nomseguridad = $datoseg['usu_nombre'];
	
	$connotseg = mysqli_query($conectar,"select nou_nota from notausuario where ven_clave_int = 10 and caj_clave_int = ".$clacaj."");
	$numnotseg = mysqli_num_rows($connotseg);
	$mennotseg = "";
	for($jj = 1; $jj <= $numnotseg; $jj++)
	{
		$datonotseg = mysqli_fetch_array($connotseg);
		$mennotseg = $jj.".".$datonotseg['nou_nota'].", ".$mennotseg;
	}
	
	$coninf = mysqli_query($conectar,"select * from adjunto_actor where tad_clave_int = 16 and caj_clave_int = '".$clacaj."'");
	$numinf = mysqli_num_rows($coninf);
	if($numinf > 0){ $sinocons = 'SI'; }else{ $sinocons = 'NO'; }
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('A'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("A".$j, $clacaj);
	//****************************
	
	$swnombrecajero = 0;
	$swdireccion = 0;
	$swestadocajero = 0;
	$swanocontable = 0;
	$swregion = 0;
	$swdepartamento = 0;
	$swmunicipio = 0;
	$swtipologia = 0;
	$swintervencion = 0;
	$swmodalidad = 0;
	$swcodigocajero = 0;
	$swcentrocostos = 0;
	$swreferencia = 0;
	$swubicacion = 0;
	$swcodigosuc = 0;
	$swubicacionatm = 0;
	$swatiende = 0;
	$swriesgo = 0;
	$swarea = 0;
	$swcodigorecibido = 0;
	$swinmobiliaria = 0;
	$swestadoinm = 0;
	$swfechainicioinm = 0;
	$swfechaentregainm = 0;
	$swtotaldiasinm = 0;
	$swnotasinm = 0;
	$swvisita = 0;
	$swestadovis = 0;
	$swfechainiciovis = 0;
	$swfechaentregavis = 0;
	$swtotaldiasvis = 0;
	$swnotasvis = 0;
	$swdisenador = 0;
	$swestadodis = 0;
	$swfechainiciodis = 0;
	$swfechaentregadis = 0;
	$swtotaldiasdis = 0;
	$swnotasdis = 0;
	$swgestionador = 0;
	$swestadolic = 0;
	$swfechainiciolic = 0;
	$swfechaentregalic = 0;
	$swtotaldiaslic = 0;
	$swnotaslic = 0;
	$swinterventor = 0;
	$swoperadorcanal = 0;
	$swfechainicioint = 0;
	$swfechaentregaint = 0;
	$swfechapedido = 0;
	$swnotasint = 0;
	$swconstructor = 0;
	$swavance = 0;
	$swfechainiciocons = 0;
	$swfechaentregacons = 0;
	$swtotaldiascons = 0;
	$swnotascons = 0;
	$swinffincons = 0;
	$swseguridad = 0;
	$swcodigomonitoreo = 0;
	$swfechaingresoobra = 0;
	$swnotasseg = 0;


	for($jj = 1; $jj <= 60; $jj++)
	{
		$letra = numeros_a_letras($jj);
		
		if($vernombrecajero == 'true' and $swnombrecajero == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $nomcaj);	
			//****************************
			$swnombrecajero = 1;
		}
		else
		if($verdireccion == 'true' and $swdireccion == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $dir);	
			//****************************
			$swdireccion = 1;
		}
		else
		if($veranocontable == 'true' and $swanocontable == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $anocon);	
			//****************************
			$swanocontable = 1;
		}
		else
		if($verregion == 'true' and $swregion == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $nomreg);	
			//****************************
			$swregion = 1;
		}
		else
		if($verdepartamento == 'true' and $swdepartamento == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $nomdep);	
			//****************************
			$swdepartamento = 1;
		}
		else
		if($vermunicipio == 'true' and $swmunicipio == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $nommun);	
			//****************************
			$swmunicipio = 1;
		}
		else
		if($vertipologia == 'true' and $swtipologia == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $nomtip);	
			//****************************
			$swtipologia = 1;
		}
		else
		if($verintervencion == 'true' and $swintervencion == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $nomtii);	
			//****************************
			$swintervencion = 1;
		}
		else
		if($vermodalidad == 'true' and $swmodalidad == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $modalidad);	
			//****************************
			$swmodalidad = 1;
		}
		else
		if($verestadocajero == 'true' and $swestadocajero == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $estadocajero);	
			//****************************
			$swestadocajero = 1;
		}
		else
		if($vercodigocajero == 'true' and $swcodigocajero == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $codcaj);	
			//****************************
			$swcodigocajero = 1;
		}
		else
		if($vercentrocostos == 'true' and $swcentrocostos == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $nomcc);	
			//****************************
			$swcentrocostos = 1;
		}
		else
		if($verreferencia == 'true' and $swreferencia == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $nomref);	
			//****************************
			$swreferencia = 1;
		}
		else
		if($verubicacion == 'true' and $swubicacion == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $nomubi);	
			//****************************
			$swubicacion = 1;
		}
		else
		if($vercodigosuc == 'true' and $swcodigosuc == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $codsuc);	
			//****************************
			$swcodigosuc = 1;
		}
		else
		if($verubicacionatm == 'true' and $swubicacionatm == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $nomubiatm);	
			//****************************
			$swubicacionatm = 1;
		}
		else
		if($veratiende == 'true' and $swatiende == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $nomatiende);	
			//****************************
			$swatiende = 1;
		}
		else
		if($verriesgo == 'true' and $swriesgo == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $riesgo);	
			//****************************
			$swriesgo = 1;
		}
		else
		if($verarea == 'true' and $swarea == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $nomarea);	
			//****************************
			$swarea = 1;
		}
		else
		if($vercodigorecibido == 'true' and $swcodigorecibido == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $fecapagadoatm);	
			//****************************				
			$swcodigorecibido = 1;
		}
		else		
		if($verinmobiliaria == 'true' and $swinmobiliaria  == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $nominmobiliaria);	
			//****************************
			$swinmobiliaria = 1;
		}
		else
		if($verestadoinm == 'true' and $swestadoinm == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $estinmobiliaria);	
			//****************************
			$swestadoinm = 1;
		}
		else
		if($verfechainicioinm == 'true' and $swfechainicioinm == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $feciniinmob);	
			//****************************
			$swfechainicioinm = 1;
		}
		else
		if($verfechaentregainm == 'true' and $swfechaentregainm == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $fecentinmob);	
			//****************************
			$swfechaentregainm = 1;
		}
		else
		if($vertotaldiasinm == 'true' and $swtotaldiasinm == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $diasinm);	
			//****************************
			$swtotaldiasinm = 1;
		}
		else
		if($vernotasinm == 'true' and $swnotasinm == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $mennotinm);	
			//****************************
			$swnotasinm = 1;
		}
		else
		if($vervisita == 'true' and $swvisita == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $nomvisitante);	
			//****************************
			$swvisita = 1;
		}
		else
		if($verestadovis == 'true' and $swestadovis == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $estvisita);	
			//****************************
			$swestadovis = 1;
		}
		else
		if($verfechainiciovis == 'true' and $swfechainiciovis == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $fecvis);	
			//****************************
			$swfechainiciovis = 1;
		}
		else
		if($verfechaentregavis == 'true' and $swfechaentregavis == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $fecentvis);	
			//****************************
			$swfechaentregavis = 1;
		}
		else
		if($vertotaldiasvis == 'true' and $swtotaldiasvis == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $diasvis);	
			//****************************
			$swtotaldiasvis = 1;
		}
		else
		if($vernotasvis == 'true' and $swnotasvis == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $mennotvis);	
			//****************************
			$swnotasvis = 1;
		}
		else
		if($verdisenador == 'true' and $swdisenador == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $nomdisenador);	
			//****************************
			$swdisenador = 1;
		}
		else
		if($verestadodis == 'true' and $swestadodis == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $estdiseno);	
			//****************************
			$swestadodis = 1;
		}
		else
		if($verfechainiciodis == 'true' and $swfechainiciodis == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $fecinidis);	
			//****************************
			$swfechainiciodis = 1;
		}
		else
		if($verfechaentregadis == 'true' and $swfechaentregadis == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $fecentdis);	
			//****************************
			$swfechaentregadis = 1;
		}
		else
		if($vertotaldiasdis == 'true' and $swtotaldiasdis == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $diasdis);	
			//****************************
			$swtotaldiasdis = 1;
		}
		else
		if($vernotasdis == 'true' and $swnotasdis == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $mennotdis);	
			//****************************
			$swnotasdis = 1;
		}
		else
		if($vergestionador == 'true' and $swgestionador == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $nomgestionador);	
			//****************************
			$swgestionador = 1;
		}
		else
		if($verestadolic == 'true' and $swestadolic == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $estlicencia);	
			//****************************
			$swestadolic = 1;
		}
		else
		if($verfechainiciolic == 'true' and $swfechainiciolic == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $fecinilic);	
			//****************************
			$swfechainiciolic = 1;
		}
		else
		if($verfechaentregalic == 'true' and $swfechaentregalic == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $fecentlic);	
			//****************************
			$swfechaentregalic = 1;
		}
		else
		if($vertotaldiaslic == 'true' and $swtotaldiaslic == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $diaslic);	
			//****************************
			$swtotaldiaslic = 1;
		}
		else
		if($vernotaslic == 'true' and $swnotaslic == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $mennotlic);	
			//****************************
			$swnotaslic = 1;
		}
		else
		if($verinterventor == 'true' and $swinterventor == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $nominterventor);	
			//****************************
			$swinterventor = 1;
		}
		else
		if($veroperadorcanal == 'true' and $swoperadorcanal == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $canalcomunicacion);	
			//****************************
			$swoperadorcanal = 1;
		}
		else
		if($verfechainicioint == 'true' and $swfechainicioint == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $feciniobra);	
			//****************************
			$swfechainicioint = 1;
		}
		else
		if($verfechaentregaint == 'true' and $swfechaentregaint == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $fecteoent);	
			//****************************
			$swfechaentregaint = 1;
		}
		else
		if($verfechapedido == 'true' and $swfechapedido == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $fecpedsum);	
			//****************************
			$swfechapedido = 1;
		}
		else
		if($vernotasint == 'true' and $swnotasint == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $mennotint);	
			//****************************
			$swnotasint = 1;
		}
		else
		if($verconstructor == 'true' and $swconstructor == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $nomconstructor);	
			//****************************
			$swconstructor = 1;
		}
		else
		if($veravance == 'true' and $swavance == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $poravance);	
			//****************************
			$swavance = 1;
		}
		else
		if($verfechainiciocons == 'true' and $swfechainiciocons == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $feciniobra);	
			//****************************
			$swfechainiciocons = 1;
		}
		else
		if($verfechaentregacons == 'true' and $swfechaentregacons == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $fecentcons);	
			//****************************
			$swfechaentregacons = 1;
		}
		else
		if($vertotaldiascons == 'true' and $swtotaldiascons == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $dcons);	
			//****************************
			$swtotaldiascons = 1;
		}
		else
		if($vernotascons == 'true' and $swnotascons == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $mennotcons);	
			//****************************
			$swnotascons = 1;
		}
		else
		if($verinffincons == 'true' and $swinffincons == 0)
		{
			//************A2**************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $sinocons);
			//****************************
			$swinffincons = 1;
		}
		else
		if($verseguridad == 'true' and $swseguridad == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $nomseguridad);	
			//****************************
			$swseguridad  = 1;
		}
		else
		if($vercodmon == 'true' and $swcodigomonitoreo == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $codmon);	
			//****************************
			$swcodigomonitoreo  = 1;
		}
		else
		if($verfecingobra == 'true' and $swfechaingresoobra == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $fecingseg);	
			//****************************
			$swfechaingresoobra = 1;
		}
		else
		if($vernotasseg == 'true' and $swnotasseg == 0)
		{
			//****************************
			$objPHPExcel->getActiveSheet()->getStyle($letra.$j)-> applyFromArray($styleA4);//
			$objPHPExcel->setActiveSheetIndex(0)->SetCellValue($letra.$j, $mennotseg);	
			//****************************
			$swnotasseg = 1;
		}
	}								

	$j++;
}
//DATOS DE SALIDA DEL EXCEL
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$archivo.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
?>