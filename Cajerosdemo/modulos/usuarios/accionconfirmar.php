<?php
include('../../data/Conexion.php');
session_start();
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_SESSION['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];
	
	date_default_timezone_set('America/Bogota');    
    // verifica si no se ha loggeado
    if(isset($_SESSION['nw']))
    {
        if($_SESSION['nw']<time())
        {
            unset($_SESSION['nw']);
            //echo "<script>alert('Tiempo agotado - Loguearse nuevamente');</script>";
            header("LOCATION:../../data/logout.php");
        }
        else
        {
            $_SESSION['nw'] = time() + (60 * 60);
        }
    }
    else
    {
        //echo "<script>alert('Tiempo agotado - Loguearse nuevamente');</script>";
        header("LOCATION:../../data/logout.php");
    }
$fecha=date("Y/m/d H:i:s");
$idcats=$_POST['idcat'];
$contador=0;
if(is_array($idcats))
{
        for($i=0;$i<count($idcats);$i++)
        {
	        if($_GET['accion'] == 'Activar')
        	{
        		if($idcats[$i] <> 0)
				{
        			$con = mysqli_query($conectar,"update usuario set usu_sw_activo = 1,usu_usu_actualiz = '".$usuario."',usu_fec_actualiz = '".$fecha."' where usu_clave_int = '".$idcats[$i]."'");
					mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'1',58,'".$t."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 58=Activar usuario
        		}
        	}
        	else
        	if($_GET['accion'] == 'Inactivar')
        	{
				if($idcats[$i] <> 0)
				{
        			$con = mysqli_query($conectar,"update usuario set usu_sw_activo = 0,usu_usu_actualiz = '".$usuario."',usu_fec_actualiz = '".$fecha."' where usu_clave_int = '".$idcats[$i]."'");
					mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'1',59,'".$t."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 59=Inactivar usuario
				}
        	}
        	else
        	if($_GET['accion'] == 'Eliminar')
        	{
				if($idcats[$i] <> 0)
				{
					$con1 = mysqli_query($conectar,"select * from cajero_constructor where cac_constructor = '".$idcats[$i]."'");
	        		$num1 = mysqli_num_rows($con1);
	        		$con2 = mysqli_query($conectar,"select * from cajero_diseno where cad_disenador = '".$idcats[$i]."'");
	        		$num2 = mysqli_num_rows($con2);
	        		$con3 = mysqli_query($conectar,"select * from cajero_inmobiliaria where cai_inmobiliaria = '".$idcats[$i]."'");
	        		$num3 = mysqli_num_rows($con3);
	        		$con4 = mysqli_query($conectar,"select * from cajero_interventoria where cin_interventor = '".$idcats[$i]."'");
	        		$num4 = mysqli_num_rows($con4);
	        		$con5 = mysqli_query($conectar,"select * from cajero_licencia where cal_gestionador = '".$idcats[$i]."'");
	        		$num5 = mysqli_num_rows($con5);
	        		$con6 = mysqli_query($conectar,"select * from cajero_seguridad where cas_instalador = '".$idcats[$i]."'");
	        		$num6 = mysqli_num_rows($con6);
	        		$con7 = mysqli_query($conectar,"select * from cajero_visita where cav_visitante = '".$idcats[$i]."'");
	        		$num7 = mysqli_num_rows($con7);
	        		
	        		$numtot = $num1+$num2+$num3+$num4+$num5+$num6+$num7;
			
	        		/*if($numtot <= 0)
	        		{*/
						//mysqli_query($conectar,"delete from usuario_actor where usa_actor = '".$idcats[$i]."'");
	        			$sql = mysqli_query($conectar,"update  usuario set usu_sw_activo = 2,usu_usu_actualiz = '".$usuario."',usu_fec_actualiz = '".$fecha."' where usu_clave_int = '".$idcats[$i]."'");
						mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'1',57,'".$t."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 57=Eliminación usuario
					//}
				}
        	}
        	else
        	if($_GET['accion'] == 'Eliminar Perfil')
        	{
				if($idcats[$i] <> 0)
				{
					$con = mysqli_query($conectar,"select * from usuario u join perfil p ON p.prf_clave_int  = u.prf_clave_int where p.prf_clave_int='".$idcats[$i]."'");
	        		$num = mysqli_num_rows($con);
	        		if($num > 0)
	        		{
	        			$dato = mysqli_fetch_array($con);
	        			$usu = $dato['usu_usuario'];
	        			$men = $men.",".$usu."(".$dato['prf_descripcion'].")";
	        		}
	        		else
	        		{
	        			//Elimino primero los permisos asignados a ese perfil
	        			$sql = mysqli_query($conectar,"delete from permiso where prf_clave_int = '".$idcats[$i]."'");
        				$sql = mysqli_query($conectar,"delete from perfil where prf_clave_int = '".$idcats[$i]."'");
        				
						mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'1',62,'".$cod."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 62=Eliminación perfil
        			}
				}
        	}
        	$contador++;
		}
		//echo "Se han eliminado $contador Registros de la base de datos";
		if($_GET['accion'] == 'Activar')
        {
        	echo '<script>alert("Los Usuarios seleccionados han sido Activados correctamente"); window.location.href="usuarios.php";</script>';
        }
        else
        if($_GET['accion'] == 'Inactivar')
        {
        	echo '<script>alert("Los Usuarios seleccionados han sido Inactivados correctamente"); window.location.href="usuarios.php";</script>';
        }
        else
        if($_GET['accion'] == 'Eliminar')
        {
        	echo '<script>alert("Los Usuarios seleccionados han sido Eliminados correctamente"); window.location.href="usuarios.php";</script>';//.implode(",",$idcats).'
        }
        else
        if($_GET['accion'] == 'Eliminar Perfil')
        {
        	if($men != '')
    		{
    			echo '<script>alert("Los Perfiles seleccionados han sido Eliminados correctamente, excepto los Perfiles que estan asignados a los siguientes usuarios: '.$men.'"); window.location.href="usuarios.php";</script>';
    		}
    		else
    		{
        		echo '<script>alert("Los Perfiles seleccionados han sido Eliminados correctamente"); window.location.href="usuarios.php";</script>';
        	}
        }
}else{
	echo '<script>alert("No se enviaron registros para eliminar"); window.location.href="usuarios.php";</script>';
}

?>