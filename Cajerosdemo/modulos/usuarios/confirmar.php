<?php
	include('../../data/Conexion.php');
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_SESSION['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];
		
	date_default_timezone_set('America/Bogota');    
    // verifica si no se ha loggeado
    if(isset($_SESSION['nw']))
    {
        if($_SESSION['nw']<time())
        {
            unset($_SESSION['nw']);
            //echo "<script>alert('Tiempo agotado - Loguearse nuevamente');</script>";
            header("LOCATION:../../data/logout.php");
        }
        else
        {
            $_SESSION['nw'] = time() + (60 * 60);
        }
    }
    else
    {
        //echo "<script>alert('Tiempo agotado - Loguearse nuevamente');</script>";
        header("LOCATION:../../data/logout.php");
    }
?>
<!DOCTYPE HTML>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

	
<title>PAVAS TECNOLOGÍA S.A.S.</title>
<meta name="description" content="Service Desk">
<meta name="author" content="InvGate S.R.L.">

<link rel="shortcut icon" href="img/favicon.ico">
<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">

<link rel="stylesheet" href="css/clean.7e4f11af0c688412dd8aa25b28b0db84.css" type="text/css">
<link rel="stylesheet" href="css/fonts.d87620aa29437ec1a4c2364858fa3607.css" type="text/css">
<link rel="stylesheet" href="css/toolbar.e73e3ba4378b1f2b1de350ef323d2c10.css" type="text/css">
<link rel="stylesheet" href="css/forms.7ba5dbaa170d790e06dc3e64d16df6ec.css" type="text/css">
<link rel="stylesheet" href="css/dropdowns.a3e95e26feafcd09b87732e381224c45.css" type="text/css">
<link rel="stylesheet" href="css/tables.d22651b7bc6ede923c3140e803446613.css" type="text/css">
<link rel="stylesheet" href="css/plugins/validationengine/validationengine.2c0953b5103027332146a5934721da3d.css" type="text/css">
<link rel="stylesheet" href="css/plugins/colorbox/colorbox.bf6245d8fbdbd182e63b3d30efb331bc.css" type="text/css">
<link rel="stylesheet" href="css/plugins/gritter/jquery.gritter.9561c37e5099df25b9dbd6b50002fae9.css" type="text/css">
<link rel="stylesheet" href="css/plugins/invgate/popup.bcac3d52c26fe8813f96890668274834.css" type="text/css">
<link rel="stylesheet" href="css/plugins/invgate/inputlist.a4daf29140fb7fd16c87914a472d76b4.css" type="text/css">
<link rel="stylesheet" href="css/plugins/invgate/multipleselector.537b0771c40de9f3a64e193ba36a57c7.css" type="text/css">
<link rel="stylesheet" href="css/plugins.26ab202f1d786ec744d7f587444f18f3.css" type="text/css">
<link rel="stylesheet" href="css/gamification/main.74d81eedeec7466de677ed5cbb818d69.css" type="text/css">
<link rel="stylesheet" href="css/scrollbar.da62bc7ef54aebbdb4e52272528a44d0.css" type="text/css">
<link rel="stylesheet" href="css/main.e41c890c8f72625912855cdc11dffd33.css" type="text/css">

<link href="css/requests/list.b2b381c1e243bbdeb809bdd354fa55a7.css" media="screen" rel="stylesheet" type="text/css" >
<!--[if lte IE 7]>
<link rel="stylesheet" href="css/ie.c32bc18afe0e2883ee4912a51f86c119.css" type="text/css" />
<![endif]-->

<style type="text/css">
.auto-style1 {
	text-align: center;
}
.auto-style2 {
	font-size: 12px;
	font-family: Arial, helvetica;
	outline: none;
/*transition: all 0.75s ease-in-out;*/ /*-webkit-transition: all 0.75s ease-in-out;*/ /*-moz-transition: all 0.75s ease-in-out;*/border-radius: 3px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 3px;
	border: 1px solid rgba(0,0,0, 0.2);
	
	background-color: #eee;
	padding: 3px;
	box-shadow: 0 0 10px #aaa;
	-webkit-box-shadow: 0 0 10px #aaa;
	-moz-box-shadow: 0 0 10px #aaa;
	border: 1px solid #999;
	background-color: white;
	text-align: center;
}
.auto-style3 {
	text-align: left;
}
.inputs{
	font-size:14px;
    font-family: Arial, helvetica;
    outline:none;
    border-radius:3px;
    -webkit-border-radius:6px;
    -moz-border-radius:3px;
    border:1px solid rgba(0,0,0, 0.5);
    padding: 3px;
}
.validaciones{
	font-size:14px;
    font-family: Arial, helvetica;
    outline:none;
    border-radius:3px;
    -webkit-border-radius:6px;
    -moz-border-radius:3px;
    border:1px solid rgba(0,0,0, 0.2);
    color:maroon;
    background-color:#eee;
    padding: 3px;
    text-align:center;
}
.ok{
	font-size:14px;
    font-family: Arial, helvetica;
    outline:none;
    border-radius:3px;
    -webkit-border-radius:6px;
    -moz-border-radius:3px;
    border:1px solid rgba(0,0,0, 0.2);
    color:green;
    background-color:#eee;
    padding: 3px;
    text-align:center;
}
</style>

<script type="text/javascript" language="javascript">
	selecteds=0;
	
	function CheckUncheck(total,check){
		checkbox=null;
		for(i=1;i<=total;i++){
			checkbox=document.getElementById("idcat"+i);
			//alert(checkbox.value);
			checkbox.checked=check.checked;
		}
		
		if(check.checked){
			selecteds=total;
		}else{
			selecteds=0;
		}
		
	}
	
	function contadorVals(check){
		if(check.checked){
			selecteds=selecteds+1;
		}else{
			selecteds=selecteds-1;
		}
	}
	
	function selectedVals(){
		if(selecteds==0){
			alert("Seleccione al menos un registro.");
			return false;
		}else{
			return true;
		}
	}
</script>

<?php //VALIDACIONES ?>
<script type="text/javascript" src="llamadas.js"></script>
<script type="text/javascript" src="js/jquery-1.6.min.js"></script>

<?php //VENTANA EMERGENTE ?>
<link rel="stylesheet" href="css/reveal.css" />
<script type="text/javascript" src="js/jquery-1.6.min.js"></script>
<script type="text/javascript" src="js/jquery.reveal.js"></script>
</head>


<body>
<?php
$rows=mysqli_query($conectar,"select * from usuario");
$total=mysqli_num_rows($rows);
$idcats=$_POST['idcat'];
?>
<form action="accionconfirmar.php?accion=<?php echo $_POST['Accion']; ?>" method="post">
	<input  type="hidden" name="idcat[]" id="idcat" value="<?php echo $row['usu_clave_int'];?>" />
<!--[if lte IE 7]>
<div class="ieWarning">Este navegador no es compatible con el sistema. Por favor, use Chrome, Safari, Firefox o Internet Explorer 8 o superior.</div>
<![endif]-->
<div class="requestListFiltersButton requestListFiltersButtonClear">
	<table style="width: 105%">
		<tr>
			<td class="auto-style2" style="width: 60px; cursor:pointer">
			Todos
			<?php
				$con = mysqli_query($conectar,"select COUNT(*) cant from usuario");
				$dato = mysqli_fetch_array($con);
				echo $dato['cant'];
			?>
			</td>
			<td class="auto-style1" style="width: 117px; cursor:pointer">&nbsp;
			</td>
			<td class="auto-style1" style="width: 80px; cursor:pointer">&nbsp;
			</td>
			<td class="auto-style1" style="width: 80px; cursor:pointer">&nbsp;
			</td>
			<td class="auto-style1" style="width: 80px; cursor:pointer">&nbsp;
			</td>
			<td class="auto-style1">&nbsp;</td>
			<td class="auto-style2" style="width: 55px; cursor:pointer">
			<table style="width: 100%">
				<tr>
					<td><a data-reveal-id="nuevousuario" data-animation="fade" style="cursor:pointer"><img alt="" src="../../img/add2.png"></a></td>
					<td><a data-reveal-id="nuevousuario" data-animation="fade" style="cursor:pointer">Añadir</a></td>
				</tr>
			</table>
			</td>
			<td class="auto-style1" style="width: 55px"></td>
		</tr>
		<tr>
			<td colspan="7" class="auto-style2">
			<?php
			if(isset($_POST['Accion']))
			{
				$accion = $_POST['Accion'];
			}
			if($accion == 'Activar')
			{
			?>
				<table style="width: 800px">
				<tr>
					<td class="auto-style3" style="width: 170px">&nbsp;</td>
					<td class="auto-style3" style="width: 130px">&nbsp;</td>
					<td class="auto-style3" style="width: 130px">&nbsp;</td>
					<td class="auto-style3" style="width: 130px">&nbsp;</td>
					<td class="auto-style3" style="width: 130px">&nbsp;</td>
				</tr>
				<tr>
					<td class="auto-style3" style="width: 170px"><strong>Nombre</strong></td>
					<td class="auto-style3" style="width: 130px"><strong>Usuario</strong></td>
					<td class="auto-style3" style="width: 130px"><strong>Perfil</strong></td>
					<td class="auto-style3" style="width: 130px"><strong>
				E-mail</strong></td>
					<td class="auto-style3" style="width: 130px"><strong>
				Activo</strong></td>
				</tr>
				<?php
					if(isset($_POST['Accion']))
					{
						$accion = $_POST['Accion'];
					}
					if(is_array($idcats)){
					$con = mysqli_query($conectar,"select * from usuario where usu_usuario = '".$usuario."'");
					$dato = mysqli_fetch_array($con);
					$acc = $dato['acc_clave_int'];
					
				    if($accion == 'Activar')
					{
						$con = mysqli_query($conectar,"select * from usuario where usu_clave_int = '".$usu."'");
						$dato = mysqli_fetch_array($con);
						$nom = $dato['usu_nombre'];
					?>
				    <p class="auto-style3">
				    	
				    	<strong>Esta seguro de Activar los siguientes usuarios?
				    	<input type="hidden" name="usuarioequipo" value="<?php echo $usu; ?>">
				    	<input type="submit" value="SI" name="si1" onmouseover="this.style.backgroundColor='#445B74';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="userNameIcon" /></strong>
				    	<a href="usuarios.php"><strong>
						<input type="button" value="NO" name="no1" onmouseover="this.style.backgroundColor='#445B74';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="userNameIcon" /></strong></a>
				    </p>
				    <?php
				    }
				    ?>
				    <?php for($i=0;$i<count($idcats);$i++)
			        	{
							$rows=mysqli_query($conectar,"select * from usuario u inner join perfil prf ON (prf.prf_clave_int = u.prf_clave_int) where u.usu_clave_int= '".$idcats[$i]."' order by u.usu_nombre");
							if(mysqli_num_rows($rows)){
							$row=mysqli_fetch_array($rows);
							$clausu = $row['usu_clave_int'];
							$nom = $row['usu_nombre'];
							$usu = $row['usu_usuario'];
							$pernom = $row['prf_descripcion'];
							$act = $row['usu_sw_activo'];
							$ema = $row['usu_email'];
			        ?>
				<tr>
					<input  type="hidden" name="idcat[]" id="idcat" value="<?php echo $clausu; ?>" />
					<td class="auto-style3" style="width: 170px"><?php echo $nom; ?></td>
					<td class="auto-style3" style="width: 130px"><?php echo $usu; ?></td>
					<td class="auto-style3" style="width: 130px"><?php echo $pernom; ?></td>
					<td class="auto-style3" style="width: 130px"><?php echo $ema; ?></td>
					<td class="auto-style3" style="width: 130px">
					<input name="activarinactivar" id="activarinactivar" disabled="disabled" type="checkbox" <?php if($act == 1){ echo 'checked="checked"'; } ?> disabled="disabled" >
					</td>
				</tr>
				<?php
							}
						}
		        	}
		        ?>
				<tr>
					<td class="auto-style3" style="width: 170px">&nbsp;</td>
					<td class="auto-style3" style="width: 130px">&nbsp;</td>
					<td class="auto-style3" style="width: 130px">&nbsp;</td>
					<td class="auto-style3" style="width: 130px">&nbsp;</td>
					<td class="auto-style3" style="width: 130px">&nbsp;</td>
				</tr>
			</table>
			<?php
			}
			?>
			<?php
			if($accion == 'Inactivar')
			{
			?>
				<table style="width: 800px">
				<tr>
					<td class="auto-style3" style="width: 170px">&nbsp;</td>
					<td class="auto-style3" style="width: 130px">&nbsp;</td>
					<td class="auto-style3" style="width: 130px">&nbsp;</td>
					<td class="auto-style3" style="width: 130px">&nbsp;</td>
					<td class="auto-style3" style="width: 130px">&nbsp;</td>
				</tr>
				<tr>
					<td class="auto-style3" style="width: 170px"><strong>Nombre</strong></td>
					<td class="auto-style3" style="width: 130px"><strong>Usuario</strong></td>
					<td class="auto-style3" style="width: 130px"><strong>Perfil</strong></td>
					<td class="auto-style3" style="width: 130px"><strong>
				E-mail</strong></td>
					<td class="auto-style3" style="width: 130px"><strong>
				Activo</strong></td>
				</tr>
				<?php
					if(isset($_POST['Accion']))
					{
						$accion = $_POST['Accion'];
					}
					if(is_array($idcats)){
					$con = mysqli_query($conectar,"select * from usuario where usu_usuario = '".$usuario."'");
					$dato = mysqli_fetch_array($con);
					$acc = $dato['acc_clave_int'];
					
				    if($accion == 'Inactivar')
					{
						if(isset($_POST['usuarioequipo']))
						{
							$usu = $_POST['usuarioequipo'];
						}
						$con = mysqli_query($conectar,"select * from usuario where usu_clave_int = '".$usu."'");
						$dato = mysqli_fetch_array($con);
						$nom = $dato['usu_nombre'];
					?>
				    <p class="auto-style3">
				    	
				    	<strong>Esta seguro de Inactivar los siguientes usuarios?
				    	<input type="submit" value="SI" name="si1" onmouseover="this.style.backgroundColor='#445B74';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="userNameIcon" /></strong>
				    	<a href="usuarios.php"><strong>
						<input type="button" value="NO" name="no1" onmouseover="this.style.backgroundColor='#445B74';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="userNameIcon" /></strong></a>
				    </p>
				    <?php
				    }
				    ?>
				    <?php for($i=0;$i<count($idcats);$i++)
			        	{
							$rows=mysqli_query($conectar,"select * from usuario u inner join perfil prf ON (prf.prf_clave_int = u.prf_clave_int) where u.usu_clave_int= '".$idcats[$i]."' order by u.usu_nombre");
							if(mysqli_num_rows($rows)){
							$row=mysqli_fetch_array($rows);
							$clausu = $row['usu_clave_int'];
							$nom = $row['usu_nombre'];
							$usu = $row['usu_usuario'];
							$pernom = $row['prf_descripcion'];
							$act = $row['usu_sw_activo'];
							$ema = $row['usu_email'];
			        ?>
				<tr>
					<input  type="hidden" name="idcat[]" id="idcat" value="<?php echo $clausu; ?>" />
					<td class="auto-style3" style="width: 170px"><?php echo $nom; ?></td>
					<td class="auto-style3" style="width: 130px"><?php echo $usu; ?></td>
					<td class="auto-style3" style="width: 130px"><?php echo $pernom; ?></td>
					<td class="auto-style3" style="width: 130px"><?php echo $ema; ?></td>
					<td class="auto-style3" style="width: 130px">
					<input name="activarinactivar" id="activarinactivar" disabled="disabled" type="checkbox" <?php if($act == 1){ echo 'checked="checked"'; } ?> disabled="disabled" >
					</td>
				</tr>
				<?php
							}
						}
		        	}
		        ?>
				<tr>
					<td class="auto-style3" style="width: 170px">&nbsp;</td>
					<td class="auto-style3" style="width: 130px">&nbsp;</td>
					<td class="auto-style3" style="width: 130px">&nbsp;</td>
					<td class="auto-style3" style="width: 130px">&nbsp;</td>
					<td class="auto-style3" style="width: 130px">&nbsp;</td>
				</tr>
			</table>
			<?php
			}
			?>
			<?php
			if($accion == 'Eliminar')
			{
			?>
				<table style="width: 800px">
				<tr>
					<td class="auto-style3" style="width: 170px">&nbsp;</td>
					<td class="auto-style3" style="width: 130px">&nbsp;</td>
					<td class="auto-style3" style="width: 130px">&nbsp;</td>
					<td class="auto-style3" style="width: 130px">&nbsp;</td>
					<td class="auto-style3" style="width: 130px">&nbsp;</td>
				</tr>
				<tr>
					<td class="auto-style3" style="width: 170px"><strong>Nombre</strong></td>
					<td class="auto-style3" style="width: 130px"><strong>Usuario</strong></td>
					<td class="auto-style3" style="width: 130px"><strong>Perfil</strong></td>
					<td class="auto-style3" style="width: 130px"><strong>
				E-mail</strong></td>
					<td class="auto-style3" style="width: 130px"><strong>
				Activo</strong></td>
				</tr>
				<?php
					if(isset($_POST['Accion']))
					{
						$accion = $_POST['Accion'];
					}
					if(is_array($idcats)){
					$con = mysqli_query($conectar,"select * from usuario where usu_usuario = '".$usuario."'");
					$dato = mysqli_fetch_array($con);
					$acc = $dato['acc_clave_int'];
					
				    if($accion == 'Eliminar')
					{
						if(isset($_POST['usuarioequipo']))
						{
							$usu = $_POST['usuarioequipo'];
						}
						$con = mysqli_query($conectar,"select * from usuario where usu_clave_int = '".$usu."'");
						$dato = mysqli_fetch_array($con);
						$nom = $dato['usu_nombre'];
					?>
				    <p class="auto-style3">
				    	
				    	<strong>Esta seguro de Eliminar los siguientes usuarios?
				    	<input type="hidden" name="usuarioequipo" value="<?php echo $usu; ?>">
				    	<input type="submit" value="SI" name="si1" onmouseover="this.style.backgroundColor='#445B74';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="userNameIcon" /></strong>
				    	<a href="usuarios.php"><strong>
						<input type="button" value="NO" name="no1" onmouseover="this.style.backgroundColor='#445B74';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="userNameIcon" /></strong></a>
				    </p>
				    <?php
				    }
				    ?>
				    <?php for($i=0;$i<count($idcats);$i++)
			        	{
							$rows=mysqli_query($conectar,"select * from usuario u inner join perfil prf ON (prf.prf_clave_int = u.prf_clave_int) where u.usu_clave_int= '".$idcats[$i]."' order by u.usu_nombre");
							if(mysqli_num_rows($rows)){
							$row=mysqli_fetch_array($rows);
							$clausu = $row['usu_clave_int'];
							$nom = $row['usu_nombre'];
							$usu = $row['usu_usuario'];
							$pernom = $row['prf_descripcion'];
							$act = $row['usu_sw_activo'];
							$ema = $row['usu_email'];
			        ?>
				<tr>
					<input  type="hidden" name="idcat[]" id="idcat" value="<?php echo $clausu; ?>" />
					<td class="auto-style3" style="width: 170px"><?php echo $nom; ?></td>
					<td class="auto-style3" style="width: 130px"><?php echo $usu; ?></td>
					<td class="auto-style3" style="width: 130px"><?php echo $pernom; ?></td>
					<td class="auto-style3" style="width: 130px"><?php echo $ema; ?></td>
					<td class="auto-style3" style="width: 130px">
					<input name="activarinactivar" id="activarinactivar" disabled="disabled" type="checkbox" <?php if($act == 1){ echo 'checked="checked"'; } ?> disabled="disabled" >
					</td>
				</tr>
				<?php
							}
						}
		        	}
		        ?>
				<tr>
					<td class="auto-style3" style="width: 170px">&nbsp;</td>
					<td class="auto-style3" style="width: 130px">&nbsp;</td>
					<td class="auto-style3" style="width: 130px">&nbsp;</td>
					<td class="auto-style3" style="width: 130px">&nbsp;</td>
					<td class="auto-style3" style="width: 130px">&nbsp;</td>
				</tr>
			</table>
			<?php
			}
			if($accion == 'Eliminar Perfil')
			{
			?>
				<table style="width: 300px">
				<tr>
					<td class="auto-style3" style="width: 170px">&nbsp;</td>
					<td class="auto-style3" style="width: 130px">&nbsp;</td>
				</tr>
				<tr>
					<td class="auto-style3" style="width: 170px"><strong>Código</strong></td>
					<td class="auto-style3" style="width: 130px"><strong>
					Descripción</strong></td>
				</tr>
				<?php
					if(isset($_POST['Accion']))
					{
						$accion = $_POST['Accion'];
					}
					if(is_array($idcats)){
					?>
				    <p class="auto-style3">
				    	
				    	<strong>Esta seguro de Eliminar los siguientes Perfiles?
				    	<input type="submit" value="SI" name="si1" onmouseover="this.style.backgroundColor='#445B74';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="userNameIcon" /></strong>
				    	<a href="usuarios.php"><strong>
						<input type="button" value="NO" name="no1" onmouseover="this.style.backgroundColor='#445B74';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="userNameIcon" /></strong></a>
				    </p>
				    <?php
				    }
				    ?>
				    <?php for($i=0;$i<count($idcats);$i++)
			        	{
							$rows=mysqli_query($conectar,"select * from perfil where prf_clave_int = '".$idcats[$i]."' order by prf_descripcion");
							if(mysqli_num_rows($rows)){
							$row=mysqli_fetch_array($rows);
							$cla = $row['prf_clave_int'];
							$cod = $row['prf_codigo'];
							$des = $row['prf_descripcion'];
			        ?>
				<tr>
					<input  type="hidden" name="idcat[]" id="idcat" value="<?php echo $cla; ?>" />
					<td class="auto-style3" style="width: 170px"><?php echo $cod; ?></td>
					<td class="auto-style3" style="width: 130px"><?php echo $des; ?></td>
				</tr>
				<?php
						}
					}
		        ?>
				<tr>
					<td class="auto-style3" style="width: 170px">&nbsp;</td>
					<td class="auto-style3" style="width: 130px">&nbsp;</td>
				</tr>
			</table>
			<?php
			}
			?>
			</td>
			<td style="width: 55px">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 60px">&nbsp;</td>
			<td style="width: 117px">&nbsp;</td>
			<td style="width: 80px">&nbsp;</td>
			<td style="width: 80px">&nbsp;</td>
			<td style="width: 80px">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td style="width: 55px">&nbsp;</td>
		</tr>
		<tr>
			<td style="width: 60px">&nbsp;</td>
			<td style="width: 117px">&nbsp;</td>
			<td style="width: 80px">&nbsp;</td>
			<td style="width: 80px">&nbsp;</td>
			<td style="width: 80px">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td style="width: 55px">&nbsp;</td>
		</tr>
	</table>
</div>
</form>
</body>
</html>