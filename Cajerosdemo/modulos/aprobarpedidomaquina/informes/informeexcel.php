<?php
include('../../../data/Conexion.php');
require_once('../../../Classes/PHPExcel.php');
date_default_timezone_set('America/Bogota');
session_start();
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_SESSION['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];
	
// verifica si no se ha loggeado
if(!isset($_SESSION["persona"]))
{
  session_destroy();
  header("LOCATION:index.php");
}else{
}

$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
$dato = mysqli_fetch_array($con);
$ultimaobra = $dato['obr_clave_int'];

$caj = $_GET['caj'];
$anocon = $_GET['anocon'];
$cencos = $_GET['cencos'];
$cod = $_GET['cod'];
$reg = $_GET['reg'];
$mun = $_GET['mun'];
$tip = $_GET['tip'];
$tipint = $_GET['tipint'];
$actor = $_GET['actor'];
$est = $_GET['est'];

$seleccionados = explode(',',$caj);
$num = count($seleccionados);
$cajeros = array();
for($i = 0; $i < $num; $i++)
{
	if($seleccionados[$i] != '')
	{
		$cajeros[$i]=$seleccionados[$i];
	}
}
$listacajeros=implode(',',$cajeros);

function CORREGIRTEXTO($string)
{
    $string = str_replace("REEMPLAZARNUMERAL", "#", $string);
    $string = str_replace("REEMPLAZARMAS", "+",$string);
    $string = str_replace("REEMPLAZARMENOS", "-", $string);
    return $string;
}
$fecha=date("d/m/Y");
$fechaact=date("Y/m/d H:i:s");
mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,obr_clave_int,loa_usu_actualiz,loa_fec_actualiz) values(null,29,71,'".$ultimaobra."','".$usuario."','".$fechaact."')");//Tercer campo tia_clave_int. 71=Documento Impreso
//************ESTILOS******************

$styleA1 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => '000000'),
    'size'  => 12,
    'name'  => 'Calibri'
));
$styleA2 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => 'C83000'),
    'size'  => 10,
    'name'  => 'Arial'
));
$styleA3 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => '000000'),
    'size'  => 10,
    'name'  => 'Arial'
));
$styleA4 = array(
'font'  => array(
    'bold'  => false,
    'color' => array('rgb' => '000000'),
    'size'  => 10,
    'name'  => 'Arial'
));
$styleA3p1 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => '000000'),
    'size'  => 9,
    'name'  => 'Arial'
));
$styleA4p1 = array(
'font'  => array(
    'bold'  => true,
    'color' => array('rgb' => '000000'),
    'size'  => 9,
    'name'  => 'Arial'
));

$borders = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '000000'),
		)
	),
);

//*************************************

$objPHPExcel = new PHPExcel();
$archivo = 'PAVAS - Informe cajeros eliminados.xls';

//Propiedades de la hoja de excel
$objPHPExcel->getProperties()
		->setCreator("PAVAS TECNOLOGIA")
		->setLastModifiedBy("PAVAS TECNOLOGIA")
		->setTitle("Informe cajeros eliminados")
		->setSubject("Informe cajeros eliminados")
		->setDescription("Documento generado con el software Control de Materiales")
		->setKeywords("Control de Materiales")
		->setCategory("Reportes");
		
//Ancho de las Columnas
//Info basica
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
//Info secundaria
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(23);
//Inmobiliaria
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(20);
//Visita
$objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setWidth(20);
//Diseño
$objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AN')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AO')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AP')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AQ')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AR')->setWidth(20);
//Licencia
$objPHPExcel->getActiveSheet()->getColumnDimension('AS')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AT')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AU')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AV')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AW')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AX')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AY')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AZ')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BA')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BB')->setWidth(20);
//INTERVENTORIA
$objPHPExcel->getActiveSheet()->getColumnDimension('BC')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BD')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BE')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BF')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BG')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BH')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BI')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BJ')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BK')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BL')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BM')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BN')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BO')->setWidth(20);
//CONSTRUCTOR
$objPHPExcel->getActiveSheet()->getColumnDimension('BP')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BQ')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BR')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BS')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BT')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BU')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BV')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BW')->setWidth(20);
//SEGURIDAD
$objPHPExcel->getActiveSheet()->getColumnDimension('BX')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BY')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('BZ')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('CA')->setWidth(20);
//FACTURAZION
$objPHPExcel->getActiveSheet()->getColumnDimension('CB')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CC')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CD')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CE')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CF')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CG')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CH')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CI')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CJ')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CK')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CL')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CM')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CN')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CO')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CP')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CQ')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CR')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CS')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CT')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CU')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CV')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CW')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('CX')->setWidth(30);

//************A1**************
$objPHPExcel->getActiveSheet()->getStyle('A1')-> applyFromArray($styleA1);//
$objPHPExcel->getActiveSheet()->getCell('A1')->setValue("LISTADO GENERAL DE CAJEROS");
$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setWrapText(true); //Crea un enter entre palabras
$objPHPExcel->getActiveSheet()->mergeCells('A1:CX1');//Conbinar celdas
$objPHPExcel->getActiveSheet()->getStyle('A1:CX1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('00D8D8D8');//COLOR DE FONDO
//****************************

/**************** COLUMNAS ****************/

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('A2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('A2')->setValue("Cajero");
$objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('B2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('B2')->setValue("Nombre");
$objPHPExcel->getActiveSheet()->getStyle('B2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('C2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('C2')->setValue("Dirección");
$objPHPExcel->getActiveSheet()->getStyle('C2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('D2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('D2')->setValue("Año Contable");
$objPHPExcel->getActiveSheet()->getStyle('D2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('E2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('E2')->setValue("Región");
$objPHPExcel->getActiveSheet()->getStyle('E2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('F2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('F2')->setValue("Departamento");
$objPHPExcel->getActiveSheet()->getStyle('F2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('G2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('G2')->setValue("Municipío");
$objPHPExcel->getActiveSheet()->getStyle('G2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('H2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('H2')->setValue("Tipología");
$objPHPExcel->getActiveSheet()->getStyle('H2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('I2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('I2')->setValue("Tipo Intervención");
$objPHPExcel->getActiveSheet()->getStyle('I2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('J2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('J2')->setValue("Modalidad");
$objPHPExcel->getActiveSheet()->getStyle('J2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('K2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('K2')->setValue("Estado Proyecto");
$objPHPExcel->getActiveSheet()->getStyle('K2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('L2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('L2')->setValue("Código Cajero");
$objPHPExcel->getActiveSheet()->getStyle('L2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('M2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('M2')->setValue("Centro Costos");
$objPHPExcel->getActiveSheet()->getStyle('M2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('N2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('N2')->setValue("Referencia Maquina");
$objPHPExcel->getActiveSheet()->getStyle('N2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('O2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('O2')->setValue("Ubicación (SUC, CC, REMOTO)");
$objPHPExcel->getActiveSheet()->getStyle('O2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('P2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('P2')->setValue("Código de Suc");
$objPHPExcel->getActiveSheet()->getStyle('P2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('Q2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('Q2')->setValue("Ubicación ATM");
$objPHPExcel->getActiveSheet()->getStyle('Q2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('R2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('R2')->setValue("Atiende");
$objPHPExcel->getActiveSheet()->getStyle('R2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('S2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('S2')->setValue("Riesgo");
$objPHPExcel->getActiveSheet()->getStyle('S2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('T2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('T2')->setValue("Area");
$objPHPExcel->getActiveSheet()->getStyle('T2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('U2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('U2')->setValue("Fecha Apagado ATM");
$objPHPExcel->getActiveSheet()->getStyle('U2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('V2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('V2')->setValue("Requiere Inmobiliaria");
$objPHPExcel->getActiveSheet()->getStyle('V2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('W2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('W2')->setValue("Nombre Inmobiliaria");
$objPHPExcel->getActiveSheet()->getStyle('W2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('X2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('X2')->setValue("Fecha Inicio");
$objPHPExcel->getActiveSheet()->getStyle('X2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('Y2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('Y2')->setValue("Fecha Teorica Entrega (45d)");
$objPHPExcel->getActiveSheet()->getStyle('Y2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('Z2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('Z2')->setValue("Fecha Entrega Info");
$objPHPExcel->getActiveSheet()->getStyle('Z2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AA2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AA2')->setValue("Total Días");
$objPHPExcel->getActiveSheet()->getStyle('AA2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AB2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AB2')->setValue("Estado");
$objPHPExcel->getActiveSheet()->getStyle('AB2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AC2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AC2')->setValue("Notas");
$objPHPExcel->getActiveSheet()->getStyle('AC2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AD2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AD2')->setValue("Requiere Visita Local");
$objPHPExcel->getActiveSheet()->getStyle('AD2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AE2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AE2')->setValue("Nombre Visitante");
$objPHPExcel->getActiveSheet()->getStyle('AE2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AF2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AF2')->setValue("Fecha Inicio");
$objPHPExcel->getActiveSheet()->getStyle('AF2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AG2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AG2')->setValue("Fecha Teorica Entrega (7d)");
$objPHPExcel->getActiveSheet()->getStyle('AG2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AH2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AH2')->setValue("Fecha Entrega Info");
$objPHPExcel->getActiveSheet()->getStyle('AH2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AI2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AI2')->setValue("Total Días");
$objPHPExcel->getActiveSheet()->getStyle('AI2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AJ2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AJ2')->setValue("Estado");
$objPHPExcel->getActiveSheet()->getStyle('AJ2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AK2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AK2')->setValue("Notas");
$objPHPExcel->getActiveSheet()->getStyle('AK2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AL2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AL2')->setValue("Requiere Diseño");
$objPHPExcel->getActiveSheet()->getStyle('AL2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AM2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AM2')->setValue("Nombre Diseñador");
$objPHPExcel->getActiveSheet()->getStyle('AM2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AN2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AN2')->setValue("Fecha Inicio");
$objPHPExcel->getActiveSheet()->getStyle('AN2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AO2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AO2')->setValue("Fecha Teorica Entrega (7d)");
$objPHPExcel->getActiveSheet()->getStyle('AO2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AP2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AP2')->setValue("Fecha Entrega Info");
$objPHPExcel->getActiveSheet()->getStyle('AP2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AQ2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AQ2')->setValue("Total Días");
$objPHPExcel->getActiveSheet()->getStyle('AQ2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AR2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AR2')->setValue("Estado");
$objPHPExcel->getActiveSheet()->getStyle('AR2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AS2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AS2')->setValue("Notas");
$objPHPExcel->getActiveSheet()->getStyle('AS2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AT2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AT2')->setValue("Requiere Licencia");
$objPHPExcel->getActiveSheet()->getStyle('AT2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AU2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AU2')->setValue("Nombre Gestionador");
$objPHPExcel->getActiveSheet()->getStyle('AU2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AV2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AV2')->setValue("Fecha Inicio");
$objPHPExcel->getActiveSheet()->getStyle('AV2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AW2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AW2')->setValue("Fecha Teorica Entrega (90d)");
$objPHPExcel->getActiveSheet()->getStyle('AW2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AX2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AX2')->setValue("Fecha Entrega Info");
$objPHPExcel->getActiveSheet()->getStyle('AX2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AY2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AY2')->setValue("Total Días");
$objPHPExcel->getActiveSheet()->getStyle('AY2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('AZ2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('AZ2')->setValue("Estado");
$objPHPExcel->getActiveSheet()->getStyle('AZ2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BA2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BA2')->setValue("Curaduria");
$objPHPExcel->getActiveSheet()->getStyle('BA2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BB2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BB2')->setValue("N°Radicado");
$objPHPExcel->getActiveSheet()->getStyle('BB2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BC2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BC2')->setValue("Notas");
$objPHPExcel->getActiveSheet()->getStyle('BC2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BD2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BD2')->setValue("Nombre Interventor");
$objPHPExcel->getActiveSheet()->getStyle('BD2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BE2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BE2')->setValue("Fecha Teorica Entrega");
$objPHPExcel->getActiveSheet()->getStyle('BE2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BF2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BF2')->setValue("Fecha Inicio Obra");
$objPHPExcel->getActiveSheet()->getStyle('BF2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BG2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BG2')->setValue("Fecha Pedido Suministro");
$objPHPExcel->getActiveSheet()->getStyle('BG2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BH2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BH2')->setValue("Fecha Aprobación Comité");
$objPHPExcel->getActiveSheet()->getStyle('BH2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BI2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BI2')->setValue("Aprobar Cotización");
$objPHPExcel->getActiveSheet()->getStyle('BI2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BJ2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BJ2')->setValue("Operador Canal Comunicaciones");
$objPHPExcel->getActiveSheet()->getStyle('BJ2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BK2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BK2')->setValue("Fecha Entrega Canal");
$objPHPExcel->getActiveSheet()->getStyle('BK2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BL2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BL2')->setValue("Aprobar Liquidación");
$objPHPExcel->getActiveSheet()->getStyle('BL2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BM2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BM2')->setValue("Image Nexos");
$objPHPExcel->getActiveSheet()->getStyle('BM2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BN2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BN2')->setValue("Fotos");
$objPHPExcel->getActiveSheet()->getStyle('BN2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BO2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BO2')->setValue("Actas");
$objPHPExcel->getActiveSheet()->getStyle('BO2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BP2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BP2')->setValue("Notas");
$objPHPExcel->getActiveSheet()->getStyle('BP2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BQ2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BQ2')->setValue("Nombre Constructor");
$objPHPExcel->getActiveSheet()->getStyle('BQ2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BR2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BR2')->setValue("Fecha Inicio Obra");
$objPHPExcel->getActiveSheet()->getStyle('BR2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BS2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BS2')->setValue("Fecha Teorica Entrega");
$objPHPExcel->getActiveSheet()->getStyle('BS2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BT2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BT2')->setValue("Fecha Real Entrega ATM");
$objPHPExcel->getActiveSheet()->getStyle('BT2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BU2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BU2')->setValue("% Avance");
$objPHPExcel->getActiveSheet()->getStyle('BU2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BV2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BV2')->setValue("Fecha Entrega Cotización (3d)");
$objPHPExcel->getActiveSheet()->getStyle('BV2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BW2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BW2')->setValue("Fecha Entrega Liquidación (7d)");
$objPHPExcel->getActiveSheet()->getStyle('BW2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BX2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BX2')->setValue("Notas");
$objPHPExcel->getActiveSheet()->getStyle('BX2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BY2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BY2')->setValue("Nombre Instalador");
$objPHPExcel->getActiveSheet()->getStyle('BY2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('BZ2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('BZ2')->setValue("Fecha Ingreso Obra");
$objPHPExcel->getActiveSheet()->getStyle('BZ2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CA2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CA2')->setValue("Código Monitoreo");
$objPHPExcel->getActiveSheet()->getStyle('CA2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CB2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CB2')->setValue("Notas");
$objPHPExcel->getActiveSheet()->getStyle('CB2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CC2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CC2')->setValue("Número OT. Inmobiliaria");
$objPHPExcel->getActiveSheet()->getStyle('CC2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CD2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CD2')->setValue("Valor Inmobiliaria");
$objPHPExcel->getActiveSheet()->getStyle('CD2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CE2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CE2')->setValue("Liquidación o Cotización Inmobiliaria");
$objPHPExcel->getActiveSheet()->getStyle('CE2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CF2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CF2')->setValue("Número OT. Visita Local");
$objPHPExcel->getActiveSheet()->getStyle('CF2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CG2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CG2')->setValue("Valor Visita Local");
$objPHPExcel->getActiveSheet()->getStyle('CG2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CH2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CH2')->setValue("Liquidación o Cotización Visita Local");
$objPHPExcel->getActiveSheet()->getStyle('CH2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CI2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CI2')->setValue("Número OT. Diseño");
$objPHPExcel->getActiveSheet()->getStyle('CI2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CJ2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CJ2')->setValue("Valor Diseño");
$objPHPExcel->getActiveSheet()->getStyle('CJ2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CK2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CK2')->setValue("Liquidación o Cotización Diseño");
$objPHPExcel->getActiveSheet()->getStyle('CK2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CL2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CL2')->setValue("Número OT. Licencia");
$objPHPExcel->getActiveSheet()->getStyle('CL2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CM2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CM2')->setValue("Valor Licencia");
$objPHPExcel->getActiveSheet()->getStyle('CM2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CN2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CN2')->setValue("Liquidación o Cotización Licencia");
$objPHPExcel->getActiveSheet()->getStyle('CN2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CO2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CO2')->setValue("Número OT. Interventoria");
$objPHPExcel->getActiveSheet()->getStyle('CO2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CP2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CP2')->setValue("Valor Interventoria");
$objPHPExcel->getActiveSheet()->getStyle('CP2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CQ2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CQ2')->setValue("Liquidación o Cotización Interventoria");
$objPHPExcel->getActiveSheet()->getStyle('CQ2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CR2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CR2')->setValue("Número OT. Construcción");
$objPHPExcel->getActiveSheet()->getStyle('CR2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CS2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CS2')->setValue("Valor Construcción");
$objPHPExcel->getActiveSheet()->getStyle('CS2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CT2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CT2')->setValue("Liquidación o Cotización Construcción");
$objPHPExcel->getActiveSheet()->getStyle('CT2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CU2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CU2')->setValue("Número OT. Instalador Seguridad");
$objPHPExcel->getActiveSheet()->getStyle('CU2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CV2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CV2')->setValue("Valor Instalador Seguridad");
$objPHPExcel->getActiveSheet()->getStyle('CV2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CW2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CW2')->setValue("Liquidación o Cotización Instalador Seguridad");
$objPHPExcel->getActiveSheet()->getStyle('CW2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

//************A2**************
$objPHPExcel->getActiveSheet()->getStyle('CX2')-> applyFromArray($styleA3);//
$objPHPExcel->getActiveSheet()->getCell('CX2')->setValue("$Total Cajero");
$objPHPExcel->getActiveSheet()->getStyle('CX2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//Centrar texto
//****************************

if($listacajeros == '')
{
	$sql = "(c.caj_clave_int = '".$consec."' OR '".$consec."' IS NULL OR '".$consec."' = '') and (caj_nombre LIKE REPLACE('%".$nom."%',' ','%') OR '".$nom."' IS NULL OR '".$nom."' = '') and (caj_ano_contable = '".$anocon."' OR '".$anocon."' IS NULL OR '".$anocon."' = '') and (cco_clave_int = '".$cencos."' OR '".$cencos."' IS NULL OR '".$cencos."' = '') and (caj_codigo_cajero LIKE REPLACE('%".$cod."%',' ','%') OR '".$cod."' IS NULL OR '".$cod."' = '') and (caj_region = '".$reg."' OR '".$reg."' IS NULL OR '".$reg."' = '') and (caj_municipio = '".$mun."' OR '".$mun."' IS NULL OR '".$mun."' = '') and (tip_clave_int = '".$tip."' OR '".$tip."' IS NULL OR '".$tip."' = '') and (tii_clave_int = '".$tipint."' OR '".$tipint."' IS NULL OR '".$tipint."' = '') and (caj_estado_proyecto = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') and (cai_inmobiliaria = '".$actor."' or cav_visitante = '".$actor."' or cad_disenador = '".$actor."' or cal_gestionador = '".$actor."' or cin_interventor = '".$actor."' or cac_constructor = '".$actor."' or cas_instalador = '".$actor."' or '".$actor."' IS NULL or '".$actor."' = '') and c.caj_sw_eliminado = 1";
}
else
{
	$sql = "(c.caj_clave_int = '".$consec."' OR '".$consec."' IS NULL OR '".$consec."' = '') and (caj_nombre LIKE REPLACE('%".$nom."%',' ','%') OR '".$nom."' IS NULL OR '".$nom."' = '') and (caj_ano_contable = '".$anocon."' OR '".$anocon."' IS NULL OR '".$anocon."' = '') and (cco_clave_int = '".$cencos."' OR '".$cencos."' IS NULL OR '".$cencos."' = '') and (caj_codigo_cajero LIKE REPLACE('%".$cod."%',' ','%') OR '".$cod."' IS NULL OR '".$cod."' = '') and (caj_region = '".$reg."' OR '".$reg."' IS NULL OR '".$reg."' = '') and (caj_municipio = '".$mun."' OR '".$mun."' IS NULL OR '".$mun."' = '') and (tip_clave_int = '".$tip."' OR '".$tip."' IS NULL OR '".$tip."' = '') and (tii_clave_int = '".$tipint."' OR '".$tipint."' IS NULL OR '".$tipint."' = '') and (caj_estado_proyecto = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') and (cai_inmobiliaria = '".$actor."' or cav_visitante = '".$actor."' or cad_disenador = '".$actor."' or cal_gestionador = '".$actor."' or cin_interventor = '".$actor."' or cac_constructor = '".$actor."' or cas_instalador = '".$actor."' or '".$actor."' IS NULL or '".$actor."' = '') and c.caj_sw_eliminado = 1 and c.caj_clave_int in (".$listacajeros.")";
}

$con = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int) inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where ".$sql."");
$num = mysqli_num_rows($con);

$j = 3;
for($i = 0; $i < $num; $i++)
{
	$dato = mysqli_fetch_array($con);
	//Info Base
	$clacaj = $dato['clacaj']; 
	$nomcaj = CORREGIRTEXTO($dato['caj_nombre']); //Nombre Cajero
	$dir = CORREGIRTEXTO($dato['caj_direccion']); //Dirección
	$anocon = $dato['caj_ano_contable']; //Año Contable
	$reg = $dato['caj_region']; //Región
	$dep = $dato['caj_departamento']; //Departamento
	$mun = $dato['caj_municipio']; //Municipío
	$tip = $dato['tip_clave_int']; //Tipología
	$tipint = $dato['tii_clave_int']; //Tipo Intervención
	$moda = $dato['mod_clave_int']; //Modalidad
	$estpro = $dato['caj_estado_proyecto']; //Estado Proyecto
	if($estpro == 1){ $estpro = 'Activo'; }elseif($estpro == 2){ $estpro = 'Suspendido'; }elseif($estpro == 3){ $estpro = 'Entregado'; }elseif($estpro == 4){ $estpro = 'Cancelado'; }elseif($estpro == 5){ echo "Programado"; }
	$con1 = mysqli_query($conectar,"select * from posicion_geografica where pog_clave_int = '".$reg."'");
	$dato1 = mysqli_fetch_array($con1);
	$reg = $dato1['pog_nombre'];
	$con1 = mysqli_query($conectar,"select * from posicion_geografica where pog_clave_int = '".$dep."'");
	$dato1 = mysqli_fetch_array($con1);
	$dep = $dato1['pog_nombre'];
	$con1 = mysqli_query($conectar,"select * from posicion_geografica where pog_clave_int = '".$mun."'");
	$dato1 = mysqli_fetch_array($con1);
	$mun = $dato1['pog_nombre'];
	$con1 = mysqli_query($conectar,"select * from tipologia where tip_clave_int = '".$tip."'");
	$dato1 = mysqli_fetch_array($con1);
	$tip = $dato1['tip_nombre'];
	$con1 = mysqli_query($conectar,"select * from tipointervencion where tii_clave_int = '".$tipint."'");
	$dato1 = mysqli_fetch_array($con1);
	$tipint = $dato1['tii_nombre'];
	
	$con1 = mysqli_query($conectar,"select * from modalidad where mod_clave_int = '".$moda."'");
	$dato1 = mysqli_fetch_array($con1);
	$moda = $dato1['mod_nombre'];
	//Info Secun
	$codcaj = $dato['caj_codigo_cajero']; //Código Cajero
	$cencos = $dato['cco_clave_int']; //Centro Costos
	$refmaq = $dato['rem_clave_int']; //Referencia Maquina
	$ubi = $dato['ubi_clave_int']; //Ubicación (SUC, CC, REMOTO)
	$codsuc = $dato['caj_codigo_suc']; //Código de Suc
	$ubiamt = $dato['caj_ubicacion_atm']; //Ubicación ATM
	$ati = $dato['ati_clave_int']; //Atiende
	$rie = $dato['caj_riesgo']; //Riesgo
	$are = $dato['are_clave_int']; //Area
    //DB->2015-07-27
	//$codrec = $dato['caj_codigo_recibido_monto']; //Código Recibido Monto
	$fecapagadoatm = $dato['caj_fec_apagado_atm'];
	$con1 = mysqli_query($conectar,"select * from centrocostos where cco_clave_int = '".$cencos."'");
	$dato1 = mysqli_fetch_array($con1);
	$cencos = $dato1['cco_nombre'];
	$con1 = mysqli_query($conectar,"select * from referenciamaquina where rem_clave_int = '".$refmaq."'");
	$dato1 = mysqli_fetch_array($con1);
	$refmaq = $dato1['rem_nombre'];
	$con1 = mysqli_query($conectar,"select * from ubicacion where ubi_clave_int = '".$ubi."'");
	$dato1 = mysqli_fetch_array($con1);
	$ubi = $dato1['ubi_descripcion'];
	if($ubiamt == 1){ $ubiamt = 'EXTERNO'; }elseif($ubiamt == 2){ $ubiamt = 'INTERNO'; }elseif($ubiamt == 0){ $ubiamt = 'N/A'; }
	$con1 = mysqli_query($conectar,"select * from atiende where ati_clave_int = '".$ati."'");
	$dato1 = mysqli_fetch_array($con1);
	$ati = $dato1['ati_nombre'];
	if($rie == 1){ $rie = 'ALTO'; }elseif($rie == 2){ $rie = 'BAJO'; }elseif($rie == 0){ $rie = 'N/A'; }
	$con1 = mysqli_query($conectar,"select * from area where are_clave_int = '".$are."'");
	$dato1 = mysqli_fetch_array($con1);
	$are = $dato1['are_nombre'];
	//Info Inmobiliaria
	$reqinm = $dato['cai_req_inmobiliaria']; //Requiere Inmobiliaria
	$inmob = $dato['cai_inmobiliaria']; //Nombre Inmobiliaria
	$feciniinmob = $dato['cai_fecha_ini_inmobiliaria']; //Fecha Inicio
	$estinmob = $dato['esi_clave_int']; //Estado
	$fecentinmob = $dato['cai_fecha_entrega_info_inmobiliaria']; //Fecha Entrega Info
	if($reqinm == 1){ $reqinm = 'Si'; }elseif($reqinm == 2){ $reqinm = 'No'; }
	$con1 = mysqli_query($conectar,"select * from usuario where usu_clave_int = '".$inmob."'");
	$dato1 = mysqli_fetch_array($con1);
	$inmob = $dato1['usu_nombre'];
	$con1 = mysqli_query($conectar,"select * from estado_inmobiliaria where esi_clave_int = '".$estinmob."'");
	$dato1 = mysqli_fetch_array($con1);
	$estinmob = $dato1['esi_nombre'];
	
	//Fecha teorica y total días inmobiliaria
	$confecteoinm = mysqli_query($conectar,"select ADDDATE('".$feciniinmob."', INTERVAL 45 DAY) dias from usuario LIMIT 1");
	$datofecteoinm = mysqli_fetch_array($confecteoinm);
	$fecteoinm = $datofecteoinm['dias'];
	if($fecentinmob != '' and $fecentinmob != '0000-00-00')
	{
		$condiasinm = mysqli_query($conectar,"select DATEDIFF('".$fecentinmob."','".$feciniinmob."') dias from usuario LIMIT 1");
	}
	else
	{
		$condiasinm = mysqli_query($conectar,"select DATEDIFF('".$fecteoinm."','".$feciniinmob."') dias from usuario LIMIT 1");
	}
	$datodiasinm = mysqli_fetch_array($condiasinm);
	$diasinm = $datodiasinm['dias'];
	
	$con1 = mysqli_query($conectar,"select * from notausuario where ven_clave_int = 7 and caj_clave_int = '".$clacaj."'");
	$num1 = mysqli_num_rows($con1);
	for($k = 1; $k <= $num1; $k++)
	{
		$dato1 = mysqli_fetch_array($con1);
		$not = $dato1['nou_nota'];
		$notasinm .= $k.". ".$not.". \n";
	}
	
	//Info Visita Local
	$reqvis = $dato['cav_req_visita_local']; //Requiere Visita Local
	$vis = $dato['cav_visitante']; //Nombre Visitante
	$fecvis = $dato['cav_fecha_visita']; //Fecha Inicio
	$fecentvis = $dato['cav_fecha_entrega_informe']; //Fecha Entrega Info
	$estvis = $dato['cav_estado']; //Estado
	if($reqvis == 1){ $reqvis = 'Si'; }elseif($reqvis == 2){ $reqvis = 'No'; }
	$con1 = mysqli_query($conectar,"select * from usuario where usu_clave_int = '".$vis."'");
	$dato1 = mysqli_fetch_array($con1);
	$vis = $dato1['usu_nombre'];
	if($estvis == 1){ $estvis = 'Activo'; }elseif($estvis == 2){ $estvis = 'Entregado'; }

	//Fecha teorica y total días visita
	$confecteovis = mysqli_query($conectar,"select ADDDATE('".$fecvis."', INTERVAL 7 DAY) dias from usuario LIMIT 1");
	$datofecteovis = mysqli_fetch_array($confecteovis);
	$fecteovis = $datofecteovis['dias'];
	if($fecentvis != '' and $fecentvis != '0000-00-00')
	{
		$condiasvis = mysqli_query($conectar,"select DATEDIFF('".$fecentvis."','".$fecvis."') dias from usuario LIMIT 1");
	}
	else
	{
		$condiasvis = mysqli_query($conectar,"select DATEDIFF('".$fecteovis."','".$fecvis."') dias from usuario LIMIT 1");
	}
	$datodiasvis = mysqli_fetch_array($condiasvis);
	$diasvis = $datodiasvis['dias'];
	
	$con1 = mysqli_query($conectar,"select * from notausuario where ven_clave_int = 18 and caj_clave_int = '".$clacaj."'");
	$num1 = mysqli_num_rows($con1);
	for($k = 1; $k <= $num1; $k++)
	{
		$dato1 = mysqli_fetch_array($con1);
		$not = $dato1['nou_nota'];
		$notasvis .= $k.". ".$not.". \n";
	}
	
	//Info Diseño
	$reqdis = $dato['cad_req_diseno']; //Requiere Diseño
	$dis = $dato['cad_disenador']; //Nombre Visitante
	$fecinidis = $dato['cad_fecha_inicio_diseno']; //Fecha Inicio
	$estdis = $dato['esd_clave_int']; //Estado
	$fecentdis = $dato['cad_fecha_entrega_info_diseno']; //Fecha Entrega Info
	if($reqdis == 1){ $reqdis = 'Si'; }elseif($reqdis == 2){ $reqdis = 'No'; }
	$con1 = mysqli_query($conectar,"select * from usuario where usu_clave_int = '".$dis."'");
	$dato1 = mysqli_fetch_array($con1);
	$dis = $dato1['usu_nombre'];
	$con1 = mysqli_query($conectar,"select * from estado_diseno where esd_clave_int = '".$estdis."'");
	$dato1 = mysqli_fetch_array($con1);
	$estdis = $dato1['esd_nombre'];
	
	//Fecha teorica y total días diseno
	$confecteodis = mysqli_query($conectar,"select ADDDATE('".$fecinidis."', INTERVAL 7 DAY) dias from usuario LIMIT 1");
	$datofecteodis = mysqli_fetch_array($confecteodis);
	$fecteodis = $datofecteodis['dias'];
	if($fecentdis != '' and $fecentdis != '0000-00-00')
	{
		$condiasdis = mysqli_query($conectar,"select DATEDIFF('".$fecentdis."','".$fecinidis."') dias from usuario LIMIT 1");
	}
	else
	{
		$condiasdis = mysqli_query($conectar,"select DATEDIFF('".$fecteodis."','".$fecinidis."') dias from usuario LIMIT 1");
	}
	$datodiasdis = mysqli_fetch_array($condiasdis);
	$diasdis = $datodiasdis['dias'];
	
	$con1 = mysqli_query($conectar,"select * from notausuario where ven_clave_int = 8 and caj_clave_int = '".$clacaj."'");
	$num1 = mysqli_num_rows($con1);
	for($k = 1; $k <= $num1; $k++)
	{
		$dato1 = mysqli_fetch_array($con1);
		$not = $dato1['nou_nota'];
		$notasdis .= $k.". ".$not.". \n";
	}
	
	//Info Licencia
	$reqlic = $dato['cal_req_licencia']; //Requiere Licencia
	$lic = $dato['cal_gestionador']; //Nombre Gestionador
	$fecinilic = $dato['cal_fecha_inicio_licencia']; //Fecha Inicio
	$fecentlic = $dato['cal_fecha_entrega_info_licencia'];
	$estlic = $dato['esl_clave_int']; //Estado
	$cura = $dato['cal_curaduria']; //Curaduria
	$radi = $dato['cal_radicado']; //N°Radicado
	if($reqlic == 1){ $reqlic = 'Si'; }elseif($reqlic == 2){ $reqlic = 'No'; }
	$con1 = mysqli_query($conectar,"select * from usuario where usu_clave_int = '".$lic."'");
	$dato1 = mysqli_fetch_array($con1);
	$lic = $dato1['usu_nombre'];
	$con1 = mysqli_query($conectar,"select * from estado_licencia where esl_clave_int = '".$estlic."'");
	$dato1 = mysqli_fetch_array($con1);
	$estlic = $dato1['esl_nombre'];
	
	//Fecha teorica y total días licencia
	$confecteolic = mysqli_query($conectar,"select ADDDATE('".$fecinilic."', INTERVAL 90 DAY) dias from usuario LIMIT 1");
	$datofecteolic = mysqli_fetch_array($confecteolic);
	$fecteolic = $datofecteolic['dias'];
	if($fecentlic != '' and $fecentlic != '0000-00-00')
	{
		$condiaslic = mysqli_query($conectar,"select DATEDIFF('".$fecentlic."','".$fecinilic."') dias from usuario LIMIT 1");
	}
	else
	{
		$condiaslic = mysqli_query($conectar,"select DATEDIFF('".$fecteolic."','".$fecinilic."') dias from usuario LIMIT 1");
	}
	$datodiaslic = mysqli_fetch_array($condiaslic);
	$diaslic = $datodiaslic['dias'];
	
	$con1 = mysqli_query($conectar,"select * from notausuario where ven_clave_int = 19 and caj_clave_int = '".$clacaj."'");
	$num1 = mysqli_num_rows($con1);
	for($k = 1; $k <= $num1; $k++)
	{
		$dato1 = mysqli_fetch_array($con1);
		$not = $dato1['nou_nota'];
		$notaslic .= $k.". ".$not.". \n";
	}
	
	//Info Interventoria
	$int = $dato['cin_interventor']; //Interventor
	$fecteoent = $dato['cin_fecha_teorica_entrega']; //Fecha Teorica Entrega
	$feciniobra = $dato['cin_fecha_inicio_obra']; //Fecha Inicio Obra
	$fecpedsum = $dato['cin_fecha_pedido_suministro']; //Fecha Pedido Suministro
	$fecaprocom = $dato['cin_fecha_aprov_comite']; //Fecha Aprobación Comité
	$aprovcotiz = $dato['cin_sw_aprov_cotizacion']; //Aprobar Cotización
	$cancom = $dato['can_clave_int']; //Operador Canal Comunicaciones
	$aprovliqui = $dato['cin_sw_aprov_liquidacion']; //Aprobar Liquidación
	$fecentcan = $dato['cin_fecha_entrega_canal']; //Fecha Entrega Canal
	$swimgnex = $dato['cin_sw_img_nexos']; //Image Nexos
	$swfot = $dato['cin_sw_fotos']; //Fotos
	$swact = $dato['cin_sw_actas']; //Actas
	if($aprovcotiz == 1){ $aprovcotiz = 'Si'; }elseif($aprovcotiz == 2){ $aprovcotiz = 'No'; }else{ $aprovcotiz = 'Sin definir'; }
	if($aprovliqui == 1){ $aprovliqui = 'Si'; }elseif($aprovliqui == 2){ $aprovliqui = 'No'; }else{ $aprovliqui = 'Sin definir'; }
	if($swimgnex == 1){ $swimgnex = 'Si'; }elseif($swimgnex == 2){ $swimgnex = 'No'; }else{ $swimgnex = 'Sin definir'; }
	if($swfot == 1){ $swfot = 'Si'; }elseif($swfot == 2){ $swfot = 'No'; }else{ $swfot = 'Sin definir'; }
	if($swact == 1){ $swact = 'Si'; }elseif($swact == 2){ $swact = 'No'; }else{ $swact = 'Sin definir'; }
	$con1 = mysqli_query($conectar,"select * from usuario where usu_clave_int = '".$int."'");
	$dato1 = mysqli_fetch_array($con1);
	$int = $dato1['usu_nombre'];
	$con1 = mysqli_query($conectar,"select * from canal_comunicacion where can_clave_int = '".$cancom ."'");
	$dato1 = mysqli_fetch_array($con1);
	$cancom = $dato1['can_nombre'];
	
	$con1 = mysqli_query($conectar,"select * from notausuario where ven_clave_int = 9 and caj_clave_int = '".$clacaj."'");
	$num1 = mysqli_num_rows($con1);
	for($k = 1; $k <= $num1; $k++)
	{
		$dato1 = mysqli_fetch_array($con1);
		$not = $dato1['nou_nota'];
		$notasint .= $k.". ".$not.". \n";
	}
	
	//Info Constructor
	$cons = $dato['cac_constructor']; //Constructor
	$feciniobra = $dato['cin_fecha_inicio_obra']; //Fecha Inicio Obra
	$fecteoent = $dato['cin_fecha_teorica_entrega'];
	$fecentatm = $dato['cac_fecha_entrega_atm']; //Fecha Entrega ATM
	$porava = $dato['cac_porcentaje_avance']; //%Avance
	$fecentcotiz = $dato['cac_fecha_entrega_cotizacion']; //Fecha Entrega Cotización
	$fecentliq = $dato['cac_fecha_entrega_liquidacion']; //Fecha Entrega Liquidación 
	$con1 = mysqli_query($conectar,"select * from usuario where usu_clave_int = '".$cons."'");
	$dato1 = mysqli_fetch_array($con1);
	$cons = $dato1['usu_nombre'];
	
	$con1 = mysqli_query($conectar,"select * from notausuario where ven_clave_int = 6 and caj_clave_int = '".$clacaj."'");
	$num1 = mysqli_num_rows($con1);
	for($k = 1; $k <= $num1; $k++)
	{
		$dato1 = mysqli_fetch_array($con1);
		$not = $dato1['nou_nota'];
		$notascons .= $k.". ".$not.". \n";
	}
	
	//Info Instalador Seguridad
	$seg = $dato['cas_instalador'];
	$fecingseg = $dato['cas_fecha_ingreso_obra_inst'];
	$codmon = $dato['cas_codigo_monitoreo'];
	$con1 = mysqli_query($conectar,"select * from usuario where usu_clave_int = '".$seg."'");
	$dato1 = mysqli_fetch_array($con1);
	$seg = $dato1['usu_nombre'];
	
	$con1 = mysqli_query($conectar,"select * from notausuario where ven_clave_int = 10 and caj_clave_int = '".$clacaj."'");
	$num1 = mysqli_num_rows($con1);
	for($k = 1; $k <= $num1; $k++)
	{
		$dato1 = mysqli_fetch_array($con1);
		$not = $dato1['nou_nota'];
		$notasseg .= $k.". ".$not.". \n";
	}
	
	//Info Facturación
	$numotinm = $dato['caf_num_ot_inmobiliaria'];
	$vrinm = $dato['caf_valor_inmobiliaria'];
	$swcotizliquiinm = $dato['caf_sw_cotiz_liqui_inmobiliaria'];
	$numotvis = $dato['caf_num_ot_visita'];
	$vrvis = $dato['caf_valor_visita'];
	$swcotizliquivis = $dato['caf_sw_cotiz_liqui_visita'];
	$numotdis = $dato['caf_num_ot_diseno'];
	$vrdis = $dato['caf_valor_diseno'];
	$swcotizliquidis = $dato['caf_sw_cotiz_liqui_diseno'];
	$numotlic = $dato['caf_num_ot_licencia'];
	$vrlic = $dato['caf_valor_licencia'];
	$swcotizliquilic = $dato['caf_sw_cotiz_liqui_licencia'];
	$numotint = $dato['caf_num_ot_interventoria'];
	$vrint = $dato['caf_valor_interventoria'];
	$swcotizliquiint = $dato['caf_sw_cotiz_liqui_interventoria'];
	$numotcons = $dato['caf_num_ot_constructor'];
	$vrcons = $dato['caf_valor_constructor'];
	$swcotizliquicons = $dato['caf_sw_cotiz_liqui_constructor'];
	$numotseg = $dato['caf_num_ot_seguridad'];
	$vrseg = $dato['caf_valor_seguridad'];
	$swcotizliquiseg = $dato['caf_sw_cotiz_liqui_seguridad'];
	
	if($swcotizliquiinm == 1){ $swcotizliquiinm = 'COTIZACIÓN'; }elseif($swcotizliquiinm == 2){ $swcotizliquiinm = 'LIQUIDACIÓN'; }else{ $swcotizliquiinm = 'SIN DEFINIR'; }
	if($swcotizliquivis == 1){ $swcotizliquivis = 'COTIZACIÓN'; }elseif($swcotizliquivis == 2){ $swcotizliquivis = 'LIQUIDACIÓN'; }else{ $swcotizliquivis = 'SIN DEFINIR'; }
	if($swcotizliquidis == 1){ $swcotizliquidis = 'COTIZACIÓN'; }elseif($swcotizliquidis == 2){ $swcotizliquidis = 'LIQUIDACIÓN'; }else{ $swcotizliquidis = 'SIN DEFINIR'; }
	if($swcotizliquilic == 1){ $swcotizliquilic = 'COTIZACIÓN'; }elseif($swcotizliquilic == 2){ $swcotizliquilic = 'LIQUIDACIÓN'; }else{ $swcotizliquilic = 'SIN DEFINIR'; }
	if($swcotizliquiint == 1){ $swcotizliquiint = 'COTIZACIÓN'; }elseif($swcotizliquiint == 2){ $swcotizliquiint = 'LIQUIDACIÓN'; }else{ $swcotizliquiint = 'SIN DEFINIR'; }
	if($swcotizliquicons == 1){ $swcotizliquicons = 'COTIZACIÓN'; }elseif($swcotizliquicons == 2){ $swcotizliquicons = 'LIQUIDACIÓN'; }else{ $swcotizliquicons = 'SIN DEFINIR'; }
	if($swcotizliquiseg == 1){ $swcotizliquiseg = 'COTIZACIÓN'; }elseif($swcotizliquiseg == 2){ $swcotizliquiseg = 'LIQUIDACIÓN'; }else{ $swcotizliquiseg = 'SIN DEFINIR'; }
	
	if($feciniinmob == '0000-00-00'){ $feciniinmob = ''; }
	if($fecentinmob == '0000-00-00'){ $fecentinmob = ''; }
	if($fecvis == '0000-00-00'){ $fecvis = ''; }
	if($fecentvis == '0000-00-00'){ $fecentvis = ''; }
	if($fecinidis == '0000-00-00'){ $fecinidis = ''; }
	if($fecentdis == '0000-00-00'){ $fecentdis = ''; }
	if($fecinilic == '0000-00-00'){ $fecinilic = ''; }
	if($fecentlic == '0000-00-00'){ $fecentlic = ''; }
	if($fecteoent == '0000-00-00'){ $fecteoent = ''; }
	if($feciniobra == '0000-00-00'){ $feciniobra = ''; }
	if($fecpedsum == '0000-00-00'){ $fecpedsum = ''; }
	if($fecentatm == '0000-00-00'){ $fecentatm = ''; }
	if($fecentcotiz == '0000-00-00'){ $fecentcotiz = ''; }
	if($fecentliq == '0000-00-00'){ $fecentliq = ''; }
	if($fecingseg == '0000-00-00'){ $fecingseg = ''; }
	if($fecteoinm == '0000-00-00'){ $fecteoinm = ''; }
	if($fecteovis == '0000-00-00'){ $fecteovis = ''; }
	if($fecteodis == '0000-00-00'){ $fecteodis = ''; }
	if($fecteolic == '0000-00-00'){ $fecteolic = ''; }
											
	/**************** DATOS DE LAS COLUMNAS ****************/ 

	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('A'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("A".$j, $clacaj);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('B'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("B".$j, $nomcaj);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('C'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("C".$j, $dir);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('D'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("D".$j, $anocon);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('E'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("E".$j, $reg);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('F'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("F".$j, $dep);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('G'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("G".$j, $mun);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('H'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("H".$j, $tip);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('I'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("I".$j, $tipint);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('J'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("J".$j, $moda);
	//***************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('K'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("K".$j, $estpro);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('L'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("L".$j, $codcaj);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('M'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("M".$j, $cencos);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('N'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("N".$j, $refmaq);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('O'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("O".$j, $ubi);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('P'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("P".$j, $codsuc);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('Q'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("Q".$j, $ubiamt);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('R'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("R".$j, $ati);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('S'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("S".$j, $rie);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('T'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("T".$j, $are);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('U'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("U".$j, $fecapagadoatm);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('V'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("V".$j, $reqinm);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('W'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("W".$j, $inmob);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('X'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("X".$j, $feciniinmob);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('Y'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("Y".$j, $fecteoinm);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('Z'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("Z".$j, $fecentinmob);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AA'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AA".$j, $diasinm);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AB'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AB".$j, $estinmob);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AC'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AC".$j, $notasinm);
	//****************************

	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AD'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AD".$j, $reqvis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AE'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AE".$j, $vis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AF'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AF".$j, $fecvis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AG'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AG".$j, $fecteovis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AH'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AH".$j, $fecentvis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AI'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AI".$j, $diasvis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AJ'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AJ".$j, $estvis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AK'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AK".$j, $notasvis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AL'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AL".$j, $reqdis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AM'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AM".$j, $dis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AN'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AN".$j, $fecinidis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AO'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AO".$j, $fecteodis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AP'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AP".$j, $fecentdis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AQ'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AQ".$j, $diasdis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AR'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AR".$j, $estdis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AS'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AS".$j, $notasdis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AT'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AT".$j, $reqlic);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AU'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AU".$j, $lic);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AV'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AV".$j, $fecinilic);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AW'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AW".$j, $fecteolic);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AX'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AX".$j, $fecentlic);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AY'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AY".$j, $diaslic);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('AZ'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("AZ".$j, $estlic);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BA'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BA".$j, $cura);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BB'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BB".$j, $radi);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BC'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BC".$j, $notaslic);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BD'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BD".$j, $int);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BE'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BE".$j, $fecteoent);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BF'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BF".$j, $feciniobra);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BG'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BG".$j, $fecpedsum);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BH'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BH".$j, $fecaprocom);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BI'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BI".$j, $aprovcotiz);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BJ'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BJ".$j, $cancom);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BK'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BK".$j, $fecentcan);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BL'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BL".$j, $aprovliqui);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BM'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BM".$j, $swimgnex);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BN'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BN".$j, $swfot);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BO'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BO".$j, $swact);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BP'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BP".$j, $notasint);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BQ'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BQ".$j, $cons);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BR'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BR".$j, $feciniobra);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BS'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BS".$j, $fecteoent);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BT'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BT".$j, $fecentatm);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BU'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BU".$j, $porava);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BV'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BV".$j, $fecentcotiz);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BW'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BW".$j, $fecentliq);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BX'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BX".$j, $notascons);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BY'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BY".$j, $seg);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('BZ'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("BZ".$j, $fecingseg);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CA'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CA".$j, $codmon);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CB'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CB".$j, $notasseg);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CC'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CC".$j, $numotinm);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CD'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CD".$j, $vrinm);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CE'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CE".$j, $swcotizliquiinm);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CF'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CF".$j, $numotvis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CG'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CG".$j, $vrvis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CH'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CH".$j, $swcotizliquivis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CI'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CI".$j, $numotdis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CJ'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CJ".$j, $vrdis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CK'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CK".$j, $swcotizliquidis);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CL'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CL".$j, $numotlic);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CM'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CM".$j, $vrlic);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CN'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CN".$j, $swcotizliquilic);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CO'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CO".$j, $numotint);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CP'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CP".$j, $vrint);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CQ'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CQ".$j, $swcotizliquiint);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CR'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CR".$j, $numotcons);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CS'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CS".$j, $vrcons);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CT'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CT".$j, $swcotizliquicons);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CU'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CU".$j, $numotseg);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CV'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CV".$j, $vrseg);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CW'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CW".$j, $swcotizliquiseg);
	//****************************
	
	//************A2**************
	$objPHPExcel->getActiveSheet()->getStyle('CX'.$J)-> applyFromArray($styleA4);//
	$objPHPExcel->setActiveSheetIndex(0)->SetCellValue("CX".$j, $vrinm+$vrvis+$vrdis+$vrlic+$vrint+$vrcons+$vrseg);
	//****************************

	$j++;
}
//DATOS DE SALIDA DEL EXCEL
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$archivo.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
?>