<?php
	include('../../data/Conexion.php');
	session_start();
	error_reporting(0);
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_SESSION['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];
		
	// verifica si no se ha loggeado	
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");
	$opcion = $_POST['opcion'];
	if($opcion=="ASIGNARUSUARIOS")
    {
        $ida  = $_POST['ida'];
        $cona = mysqli_query($conectar,"select tas_nombre from tipo_alerta_sistema where tas_clave_int = '".$ida."'");
        $data = mysqli_fetch_array($cona);
        $noma = $data['tas_nombre'];
        ?>
        <fieldset><legend><strong><?php echo $noma;?></strong></legend>
            <input type="hidden" id="idalerta" value="<?php echo $ida;?>">
            <table style="width:100%">
                <tr>
                    <td align="left" class="auto-style2">
                        <table style="width: 80%">
                            <tr>
                                <td class="auto-style1"><strong>Filtro:<img src="../../img/buscar.png" alt="" height="18" width="15"></strong></td>

                                <td class="auto-style1">
                                    <input class="inputs" onkeyup="CRUDALERTAS('VERUSUARIOS','','')" name="nombre2" id="nombre2" maxlength="70" type="text" value="Nombre" onblur="if(this.value=='') this.value='Nombre'" onfocus="if(this.value =='Nombre' ) this.value=''" style="width: 150px"></td>
                                <td class="auto-style1">
                                    <input class="inputs" onkeyup="CRUDALERTAS('VERUSUARIOS','','')" name="email2" id="email2" maxlength="70" type="text" value="E-mail" onblur="if(this.value=='') this.value='E-mail'" onfocus="if(this.value =='E-mail' ) this.value=''" style="width: 150px"></td>
                                <td class="auto-style1">
                                    <input class="inputs" onkeyup="CRUDALERTAS('VERUSUARIOS','','')" name="usuario2" id="usuario2" maxlength="70" type="text" value="Usuario" onblur="if(this.value=='') this.value='Usuario'" onfocus="if(this.value =='Usuario' ) this.value=''"></td>
                                <td class="auto-style1">
                                    <select name="activo2" id="activo2" onchange="CRUDALERTAS('VERUSUARIOS','','')" class="inputs">
                                        <option value="">Todos</option>
                                        <option value="1">Asignados</option>
                                        <option value="2">Sin Asignar</option>
                                    </select>

                                </td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        <div id="busquedausuarios">

                        </div>
                    </td>
                </tr>
            </table>
        </fieldset>
        <?php
        echo "<style onload=CRUDALERTAS('VERUSUARIOS','','')></style>";
    }
    else
	if($opcion=="VERUSUARIOS")
	{
        $ida = $_POST['ida'];


        $busnom = $_POST['nom'];
        $busema = $_POST['ema'];
        $bususu = $_POST['usu'];

        if($busnom == 'Nombre'){ $busnom = ''; }
        if($busema == 'E-mail'){ $busema = ''; }
        if($bususu == 'Usuario'){ $bususu = ''; }

        $act = $_POST['act'];
        if($act==1){ $w = " and u.usu_clave_int in (select usu_clave_int from asignacion_alerta where tas_clave_int = '".$ida."')" ;}
        else if($act==2){ $w = " and u.usu_clave_int not in (select usu_clave_int from asignacion_alerta where tas_clave_int = '".$ida."')" ;}else{$w="";}

        $con1 = mysqli_query($conectar,"select * from usuario u inner join perfil prf ON (prf.prf_clave_int = u.prf_clave_int) where (u.usu_nombre LIKE REPLACE('%".$busnom."%',' ','%') OR '".$busnom."' IS NULL OR '".$busnom."' = '') and (u.usu_email LIKE REPLACE('".$busema."%',' ','%')  OR '".$busema."' IS NULL OR '".$busema."' = '') and (u.usu_usuario LIKE '".$bususu."%' OR '".$bususu."' IS NULL OR '".$bususu."' = '') ".$w." order by u.usu_nombre");
        $num1 = mysqli_num_rows($con1);
        ?>
        <table style="width: 100%">
            <tr>

                <td class="auto-style13" style="width: 160px"><strong>Nombre</strong></td>
                <td class="auto-style13" style="width: 130px"><strong>Usuario</strong></td>
                <td class="auto-style13" style="width: 100px"><strong>Perfil</strong></td>
                <td class="auto-style13" style="width: 150px"><strong>E-mail</strong></td>

                <td class="auto-style13" style="width: 157px"><strong>Asignar</strong></td>

            </tr>
            <?php
            for($k = 0; $k < $num1; $k++)
            {
                $dato = mysqli_fetch_array($con1);
                $clausu = $dato['usu_clave_int'];
                $cat = $dato['usu_categoria'];
                $nom = $dato['usu_nombre'];
                $usu = $dato['usu_usuario'];
                $pernom = $dato['prf_descripcion'];
                $act = $dato['usu_sw_activo'];
                $ema = $dato['usu_email'];
                $usuact = $dato['usu_usu_actualiz'];
                $fecact = $dato['usu_fec_actualiz'];
                $veri = mysqli_query($conectar,"select * from asignacion_alerta where usu_clave_int = '".$clausu."' and tas_clave_int = '".$ida."'");
                $numv = mysqli_num_rows($veri);

                ?>
                <tr>

                    <td class="auto-style13" style="width: 160px"><?php echo ucfirst(strtolower($nom)); ; ?></td>
                    <td class="auto-style13" style="width: 130px"><?php echo $usu; ?></td>
                    <td class="auto-style13" style="width: 100px"><?php echo $pernom; ?></td>
                    <td class="auto-style13" style="width: 150px"><?php echo $ema; ?></td>

                    <td class="auto-style13" style="width: 157px">
                        <input name="asignar_<?php echo $clausu;?>" id="asignar_<?php echo $clausu;?>" type="checkbox" <?php if($numv>0){ echo 'checked="checked"'; } ?> onclick="CRUDALERTAS('GUARDARASIGNAR','','<?PHP echo $clausu;?>')" value="1">
                    </td>

                </tr>
                <?php
            }
            ?>
        </table>
        <?php

		echo "<style onload=autoResize('iframe43')></style>";

	}
    else if($opcion=="GUARDARASIGNAR")
    {
        $ck = $_POST['ck'];
        $idu = $_POST['idu'];
        $ida = $_POST['ida'];
          if($ck!=1)
          {
              $con = mysqli_query($conectar,"DELETE FROM asignacion_alerta where tas_clave_int = '".$ida."' and usu_clave_int = '".$idu."'");
          }
          else
          {
              $con = mysqli_query($conectar,"INSERT INTO asignacion_alerta(usu_clave_int,tas_clave_int,asi_usu_actualiz,asi_fec_actualiz) VALUES('".$idu."','".$ida."','".$usuario."','".$fecha."') ");

          }
          if($con>0)
          {
              $res = 1;
          }
          else
          {
              $res = 2;
          }
        $conca = mysqli_query($conectar,"select count(asi_clave_int) canta from asignacion_alerta where tas_clave_int = '".$ida."'");
        $datca = mysqli_fetch_array($conca);
        $canta = $datca['canta']; if($canta=="" || $canta==NULL){ $canta = 0; }
        $datos[] = array("res"=>$res,"canta"=>$canta);
        echo json_encode($datos);
    }
?>