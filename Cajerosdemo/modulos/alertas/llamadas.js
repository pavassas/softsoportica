function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function CANTIDAD(v)
{
	if(v == 'TODOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('cant').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#cant").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?cantidad=si",true);
		ajax.send(null);
	}
}
function CONSULTAMODULO(v)
{
	if(v == 'HORAS')
	{
		var div = document.getElementById('filtro');
    	div.style.display = 'none';
		window.location.href = "horasextras.php";
	}
	else
	if(v == 'TODOS')
	{
		var div = document.getElementById('filtro');
    	div.style.display = 'block';
    	form1.filtro.value = "TODOS";
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('usuarios').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#usuarios").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?todos=si",true);
		ajax.send(null);
	}
}
function BUSCAR(v)
{
	var cajeros = "";
	var objCBarray = document.getElementsByName('multiselect_buscajero');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	cajeros += objCBarray[i].value + ",";
	    }
	}
	
	var alertas = "";
	var objCBarray = document.getElementsByName('multiselect_busalerta');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	alertas += objCBarray[i].value + ",";
	    }
	}
	
	var actores = "";
	var objCBarray = document.getElementsByName('multiselect_busactor');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	actores += objCBarray[i].value + ",";
	    }
	}
	
	var fid = form1.busfecinidesde.value;
	var fih = form1.busfecinihasta.value;
	var ftd = form1.busfecteodesde.value;
	var fth = form1.busfecteohasta.value;
	var fed = form1.busfecentdesde.value;
	var feh = form1.busfecenthasta.value;
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('alertasactivas').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#alertasactivas").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
	
	if(v > 0)
	{
		ajax.open("GET","?buscarale=si&caj="+cajeros+"&ale="+alertas+"&act="+actores+"&fid="+fid+"&fih="+fih+"&ftd="+ftd+"&fth="+fth+"&fed="+fed+"&feh="+feh+"&page="+v,true);
	}
	else
	{
		ajax.open("GET","?buscarale=si&caj="+cajeros+"&ale="+alertas+"&act="+actores+"&fid="+fid+"&fih="+fih+"&ftd="+ftd+"&fth="+fth+"&fed="+fed+"&feh="+feh,true);	
	}
	ajax.send(null);
	setTimeout("OCULTARSCROLL();",1500);
}
function CARGARMAS(p,c)
{
	var strChoices = "";
	var strChoices1 = "";
	var objCBarray = document.getElementsByName('metodoseleccionado');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	strChoices += objCBarray[i].value + ",";
	    	strChoices1 = objCBarray[i].value;
	    }
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('masdatos'+c).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#masdatos"+c).html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarmasdatos=si&per="+p+"&clahoe="+strChoices,true);
	ajax.send(null);
	
	setTimeout("OCULTARSCROLL();",1000);
}
function EDITAR(c)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('datosperiodo').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#datosperiodo").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrardatosperiodo=si&cla="+c,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTA();",500);
}
function ACTUALIZARPERIODO(c)
{
	var per = form1.periodo1.value;
	var emp = form1.empleado1.value;
	var fec = form1.fecha1.value;
	var hor = form1.horas1.value;
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
	    	document.getElementById('horasagg').innerHTML=ajax.responseText;
	    }
	}
	//jQuery("#agregados").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?guardarperiodo=si&per="+per+"&emp="+emp+"&fec="+fec+"&hor="+hor+"&cla="+c+"&act=si",true);
	ajax.send(null);
}
function eliminar(v)
{
	if(confirm('Esta seguro/a de Eliminar este registro?'))
	{
		var dataString = 'id='+v;
	
		$.ajax({
	        type: "POST",
	        url: "delete.php",
	        data: dataString,
	        success: function() {
				$('#delete-ok').empty();
				$('#delete-ok').append('<div class="correcto">Se ha eliminado correctamente el registro con Id='+v+'.</div>').fadeIn("slow");
				$('#service'+v).fadeOut("slow");
				//$('#'+v).remove();
	        }
	    });
	}
}
function advertencia(v)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('estadoorden').innerHTML=ajax.responseText;
	    }
	}
	//jQuery("#foto").html("<img alt='cargando' src='images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?advertencia=si&clauce="+v,true);
	ajax.send(null);
}
function VALIDARUSUARIO()
{
	var ced = form1.cedula.value;
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('avisousuario').innerHTML=ajax.responseText;
	    }
	}
	//jQuery("#foto").html("<img alt='cargando' src='images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?validarusuario=si&ced="+ced,true);
	ajax.send(null);
}
function VERPERIODO(v)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('horasagg').innerHTML=ajax.responseText;
	    }
	}
	//jQuery("#foto").html("<img alt='cargando' src='images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?verperiodo=si&per="+v,true);
	ajax.send(null);
}
function EVACUARALERTA(v)
{
	if(confirm('Esta seguro/a de Evacuar esta alerta?'))
	{
		var dataString = 'id='+v;
	
		$.ajax({
	        type: "POST",
	        url: "evacuar.php",
	        data: dataString,
	        success: function() {
				$('#delete-ok').empty();
				$('#delete-ok').append('<div class="correcto">Se ha evacuado correctamente la alerta.</div>').fadeIn("slow");
				$('#service'+v).fadeOut("slow");
				//$('#'+v).remove();
	        }
	    });
	    OCULTARSCROLL();
	}
}
function EXPORTAR()
{
	var cajeros = "";
	var objCBarray = document.getElementsByName('multiselect_buscajero');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	cajeros += objCBarray[i].value + ",";
	    }
	}
	
	var alertas = "";
	var objCBarray = document.getElementsByName('multiselect_busalerta');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	alertas += objCBarray[i].value + ",";
	    }
	}
	
	var actores = "";
	var objCBarray = document.getElementsByName('multiselect_busactor');
	
	for (i = 0; i < objCBarray.length; i++) 
	{
		if (objCBarray[i].checked) 
		{
	    	actores += objCBarray[i].value + ",";
	    }
	}
	
	var fid = form1.busfecinidesde.value;
	var fih = form1.busfecinihasta.value;
	var ftd = form1.busfecteodesde.value;
	var fth = form1.busfecteohasta.value;
	var fed = form1.busfecentdesde.value;
	var feh = form1.busfecenthasta.value;
	
	window.location.href = "informes/informeexcel.php?caj="+cajeros+"&ale="+alertas+"&act="+actores+"&fid="+fid+"&fih="+fih+"&ftd="+ftd+"&fth="+fth+"&fed="+fed+"&feh="+feh;
}

function CRUDALERTAS(o,ida,idu)
{
	if(o=="ASIGNARUSUARIOS")
	{
		$('#divasignar').html("<img alt='cargando' src='images/ajax-loader.gif' height='20' width='20' />");
		$.post('fnAlertas.php',{opcion:o,ida:ida},function(data){
			$('#divasignar').html(data);
		});
	}
	else if(o=="VERUSUARIOS")
	{
		var ida =  $('#idalerta').val();
		var nom = $('#nombre2').val();
		var ema = $('#email2').val();
		var usu = $('#usuario2').val();
		var act = $('#activo2').val();

        $('#busquedausuarios').html("<img alt='cargando' src='images/ajax-loader.gif' height='20' width='20' />");
        $.post('fnAlertas.php',{opcion:o,ida:ida,nom:nom,ema:ema,usu:usu,act:act},function(data){
            $('#busquedausuarios').html(data);
        });
	}
	else if(o=="GUARDARASIGNAR")
	{
		var ida = $('#idalerta').val();
        var ck = $('input:checkbox[name=asignar_'+idu+']:checked').val();
        $.post('fnAlertas.php',{opcion:o,ida:ida,idu:idu,ck:ck},function(data){
            var res = data[0].res;
            $('#canta_' + ida).html(data[0].canta)
            if(res==2)
			{

				alert("Surgio error al asignar usuario. Verificar");
			}
        },"json");
	}
}