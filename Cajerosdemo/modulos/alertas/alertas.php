<?php
	include('../../data/Conexion.php');
	error_reporting(0);
	header("Cache-Control: no-store, no-cache, must-revalidate");
	date_default_timezone_set('America/Bogota');
	header('Content-Type: text/html; charset=UTF-8');
	$fecha=date("Y/m/d H:i:s");
?>
	<div style="display:none">
	<?php
	session_start();
	$usuario= $_SESSION['usuario'];
	?>
	</div>
<?php

   if(isset($_SESSION['nw']))
    {
        if($_SESSION['nw']<time())
        {
            unset($_SESSION['nw']);
            //echo "<script>alert('Tiempo agotado - Loguearse nuevamente');</script>";
            header("LOCATION:../../data/logout.php");
        }
        else
        {
            $_SESSION['nw'] = time() + (60 * 60);
        }
    }
    else
    {
       //echo "<script>alert('Tiempo agotado - Loguearse nuevamente');</script>";
       header("LOCATION:../../data/logout.php");
    }

	require_once("lib/Zebra_Pagination.php");
	$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
	$dato = mysqli_fetch_array($con);
	$perfil = $dato['prf_descripcion'];
	$claveusuario = $dato['usu_clave_int'];
	
	if($_GET['buscarale'] == 'si')
	{
		$caj = $_GET['caj'];
		$ale = $_GET['ale'];
		$act = $_GET['act'];
		$fid = $_GET['fid'];
		$fih = $_GET['fih'];
		$ftd = $_GET['ftd'];
		$fth = $_GET['fth'];
		$fed = $_GET['fed'];
		$feh = $_GET['feh'];
		
		$seleccionados = explode(',',$caj);
		$num = count($seleccionados);
		$cajeros = array();
		for($i = 0; $i < $num; $i++)
		{
			if($seleccionados[$i] != '')
			{
				$cajeros[$i]=$seleccionados[$i];
			}
		}
		$listacajeros=implode(',',$cajeros);
		
		$seleccionados = explode(',',$ale);
		$num = count($seleccionados);
		$alertas = array();
		for($i = 0; $i < $num; $i++)
		{
			if($seleccionados[$i] != '')
			{
				$alertas[$i]=$seleccionados[$i];
			}
		}
		$listaalertas=implode(',',$alertas);
		
		$seleccionados = explode(',',$act);
		$num = count($seleccionados);
		$actores = array();
		for($i = 0; $i < $num; $i++)
		{
			if($seleccionados[$i] != '')
			{
				$actores[$i]=$seleccionados[$i];
			}
		}
		$listaactores=implode(',',$actores);
		
		if($listacajeros != ''){ if($sql1 == ''){	$sql1 = "a.caj_clave_int in (".$listacajeros.")"; }else{ $sql1 = $sql1." and a.caj_clave_int in (".$listacajeros.")"; } }
		
		if($listaalertas != ''){ if($sql1 == ''){	$sql1 = "a.tal_clave_int in (".$listaalertas.")"; }else{ $sql1 = $sql1." and a.tal_clave_int in (".$listaalertas.")"; } }
		
		if($listaactores != ''){ if($sql1 == ''){	$sql1 = "act.act_clave_int in (".$listaactores.")"; }else{ $sql1 = $sql1." and act.act_clave_int in (".$listaactores.")"; } }
		
		if($sql1 == ''){ $sql1 = "((a.ale_fecha_inicio BETWEEN '".$fid." 00:00:00' AND '".$fih." 23:59:59') or ('".$fid."' Is Null and '".$fih."' Is Null) or ('".$fid."' = '' and '".$fih."' = ''))"; }else{ $sql1 = $sql1." and ((a.ale_fecha_inicio BETWEEN '".$fid." 00:00:00' AND '".$fih." 23:59:59') or ('".$fid."' Is Null and '".$fih."' Is Null) or ('".$fid."' = '' and '".$fih."' = ''))"; }
		
		if($sql1 == ''){ $sql1 = "(CASE a.tal_clave_int 
		WHEN 6 THEN (a.ale_fecha_entrega BETWEEN '".$ftd." 00:00:00' AND '".$fth." 23:59:59') or ('".$ftd."' Is Null and '".$fth."' Is Null) or ('".$ftd."' = '' and '".$fth."' = '') 
		WHEN 1 THEN (ADDDATE(a.ale_fecha_inicio, INTERVAL 45 DAY) BETWEEN '".$ftd." 00:00:00' AND '".$fth." 23:59:59') or ('".$ftd."' Is Null and '".$fth."' Is Null) or ('".$ftd."' = '' and '".$fth."' = '') 
		WHEN 2 THEN (ADDDATE(a.ale_fecha_inicio, INTERVAL 7 DAY) BETWEEN '".$ftd." 00:00:00' AND '".$fth." 23:59:59') or ('".$ftd."' Is Null and '".$fth."' Is Null) or ('".$ftd."' = '' and '".$fth."' = '') 
		WHEN 3 THEN (ADDDATE(a.ale_fecha_inicio, INTERVAL 7 DAY) BETWEEN '".$ftd." 00:00:00' AND '".$fth." 23:59:59') or ('".$ftd."' Is Null and '".$fth."' Is Null) or ('".$ftd."' = '' and '".$fth."' = '') 
		WHEN 4 THEN (ADDDATE(a.ale_fecha_inicio, INTERVAL 90 DAY) BETWEEN '".$ftd." 00:00:00' AND '".$fth." 23:59:59') or ('".$ftd."' Is Null and '".$fth."' Is Null) or ('".$ftd."' = '' and '".$fth."' = '') 
		WHEN 5 THEN (a.ale_fecha_entrega BETWEEN '".$ftd." 00:00:00' AND '".$fth." 23:59:59') or ('".$ftd."' Is Null and '".$fth."' Is Null) or ('".$ftd."' = '' and '".$fth."' = '') 
		END)"; }else{ $sql1 = $sql1." and (CASE a.tal_clave_int 
		WHEN 6 THEN (a.ale_fecha_entrega BETWEEN '".$ftd." 00:00:00' AND '".$fth." 23:59:59') or ('".$ftd."' Is Null and '".$fth."' Is Null) or ('".$ftd."' = '' and '".$fth."' = '') 
		WHEN 1 THEN (ADDDATE(a.ale_fecha_inicio, INTERVAL 45 DAY) BETWEEN '".$ftd." 00:00:00' AND '".$fth." 23:59:59') or ('".$ftd."' Is Null and '".$fth."' Is Null) or ('".$ftd."' = '' and '".$fth."' = '') 
		WHEN 2 THEN (ADDDATE(a.ale_fecha_inicio, INTERVAL 7 DAY) BETWEEN '".$ftd." 00:00:00' AND '".$fth." 23:59:59') or ('".$ftd."' Is Null and '".$fth."' Is Null) or ('".$ftd."' = '' and '".$fth."' = '') 
		WHEN 3 THEN (ADDDATE(a.ale_fecha_inicio, INTERVAL 7 DAY) BETWEEN '".$ftd." 00:00:00' AND '".$fth." 23:59:59') or ('".$ftd."' Is Null and '".$fth."' Is Null) or ('".$ftd."' = '' and '".$fth."' = '') 
		WHEN 4 THEN (ADDDATE(a.ale_fecha_inicio, INTERVAL 90 DAY) BETWEEN '".$ftd." 00:00:00' AND '".$fth." 23:59:59') or ('".$ftd."' Is Null and '".$fth."' Is Null) or ('".$ftd."' = '' and '".$fth."' = '') 
		WHEN 5 THEN (a.ale_fecha_entrega BETWEEN '".$ftd." 00:00:00' AND '".$fth." 23:59:59') or ('".$ftd."' Is Null and '".$fth."' Is Null) or ('".$ftd."' = '' and '".$fth."' = '') END)"; }
		
		if($sql1 == ''){ $sql1 = "(CASE a.tal_clave_int 
		WHEN 6 THEN (a.ale_fecha_entrega_cajero BETWEEN '".$fed." 00:00:00' AND '".$feh." 23:59:59') or ('".$fed."' Is Null and '".$feh."' Is Null) or ('".$fed."' = '' and '".$feh."' = '') 
		ELSE
		(a.ale_fecha_entrega BETWEEN '".$fed." 00:00:00' AND '".$feh." 23:59:59') or ('".$fed."' Is Null and '".$feh."' Is Null) or ('".$fed."' = '' and '".$feh."' = '')
		END)"; }else{ $sql1 = $sql1." and (CASE a.tal_clave_int 
		WHEN 6 THEN (a.ale_fecha_entrega_cajero BETWEEN '".$fed." 00:00:00' AND '".$feh." 23:59:59') or ('".$fed."' Is Null and '".$feh."' Is Null) or ('".$fed."' = '' and '".$feh."' = '') 
		ELSE
		(a.ale_fecha_entrega BETWEEN '".$fed." 00:00:00' AND '".$feh." 23:59:59') or ('".$fed."' Is Null and '".$feh."' Is Null) or ('".$fed."' = '' and '".$feh."' = '')
		END)"; }
?>
		<fieldset name="Group1">
		<legend align="center">LISTA DE ALERTAS ACTIVAS
		<br>
		<button name="accion" value="excel" type="button" onclick="EXPORTAR()" style="cursor:pointer;">
		<img src="../../images/excel.png" height="30" width="30">
		</button>
		</legend>
		<table style="width: 100%" align="center">
			<tr>
				<td class="auto-style15" style="height: 15px; width: 70px;">
				&nbsp;</td>
				<td class="auto-style15" style="height: 15px; width: 200px;">
				&nbsp;</td>
				<td class="auto-style15" style="height: 15px; width: 300px;">
				</td>
				<td class="auto-style15" style="height: 15px; width: 120px;">
				&nbsp;</td>
				<td class="auto-style15" style="height: 15px; width: 100px;">
				&nbsp;</td>
				<td class="auto-style15" style="height: 15px;">
				&nbsp;</td>
				<td class="auto-style15" style="height: 15px;">
				&nbsp;</td>
				<td class="auto-style15" style="height: 15px;">
				&nbsp;</td>
				<td class="auto-style15" style="height: 15px;">
				&nbsp;</td>
				</tr>
				<tr>
				<td class="auto-style15" style="height: 23px; width: 70px;">
				<strong>Cajero</strong></td>
				<td class="auto-style15" style="height: 23px; width: 200px;">
				<strong>Nombre</strong></td>
				<td class="auto-style15" style="height: 23px; width: 300px;"><strong>
				Alerta</strong></td>
				<td class="auto-style15" style="height: 23px; width: 120px;">
				<strong>Actor</strong></td>
				<td class="auto-style15" style="height: 23px; width: 100px;">
				<strong>Fecha Inicio</strong></td>
				<td class="auto-style15" style="height: 23px; width: 150px;">
				<strong>Fecha teorica entrega</strong></td>
				<td class="auto-style15" style="height: 23px; width: 150px;">
				<strong>Fecha entrega</strong></td>
				<td class="auto-style15" style="height: 23px; width: 150px;">
				<strong>Días retraso</strong></td>
				<td class="auto-style15" style="height: 23px; width: 150px;">
				&nbsp;</td>
			</tr>
			<?php
			$query = mysqli_query($conectar,"select ale_clave_int,a.caj_clave_int,tal_nombre,act_nombre,a.tal_clave_int,ale_fecha_inicio,ale_fecha_entrega,ale_fecha_entrega_cajero from alerta a inner join cajero c on (c.caj_clave_int = a.caj_clave_int) inner join tipo_alerta ta on (ta.tal_clave_int = a.tal_clave_int) inner join actor act on (act.act_clave_int = ta.act_clave_int) where ".$sql1." and a.ale_sw_eliminado = 0 and a.ale_sw_inactivo = 0");
			//$res = $con->query($query);
			$num_registros = mysqli_num_rows($query);
	
			$resul_x_pagina = 100;
			//Paginar:
			$paginacion = new Zebra_Pagination();
			$paginacion->records($num_registros);
			$paginacion->records_per_page($resul_x_pagina);
			
			$con = mysqli_query($conectar,"select ale_clave_int,a.caj_clave_int,caj_nombre,tal_nombre,act_nombre,a.tal_clave_int,ale_fecha_inicio,ale_fecha_entrega,ale_fecha_entrega_cajero from alerta a inner join cajero c on (c.caj_clave_int = a.caj_clave_int) inner join tipo_alerta ta on (ta.tal_clave_int = a.tal_clave_int) inner join actor act on (act.act_clave_int = ta.act_clave_int) where ".$sql1." and a.ale_sw_eliminado = 0 LIMIT ".(($paginacion->get_page() - 1) * $resul_x_pagina). ',' .$resul_x_pagina);
			$num = mysqli_num_rows($con);
			
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$cla = $dato['ale_clave_int'];
				$cajnom = $dato['caj_nombre'];
				$caj = $dato['caj_clave_int'];
				$ale = $dato['tal_nombre'];
				$act = $dato['act_nombre'];
				$tip = $dato['tal_clave_int'];
				$fi = $dato['ale_fecha_inicio'];
				$fe = $dato['ale_fecha_entrega'];
				$fecaj = $dato['ale_fecha_entrega_cajero'];
				
				if($tip == 1)
				{
					$con1 = mysqli_query($conectar,"select ADDDATE('".$fi."', INTERVAL 45 DAY) fecteo from usuario LIMIT 1");
					$dato1 = mysqli_fetch_array($con1);
					$fecteo = $dato1['fecteo'];
				}
				else
				if($tip == 2 or $tip == 3)
				{
					$con1 = mysqli_query($conectar,"select ADDDATE('".$fi."', INTERVAL 7 DAY) fecteo from usuario LIMIT 1");
					$dato1 = mysqli_fetch_array($con1);
					$fecteo = $dato1['fecteo'];
				}
				else
				if($tip == 4)
				{
					$con1 = mysqli_query($conectar,"select ADDDATE('".$fi."', INTERVAL 90 DAY) fecteo from usuario LIMIT 1");
					$dato1 = mysqli_fetch_array($con1);
					$fecteo = $dato1['fecteo'];
				}
				else
				if($tip == 5)
				{
					$fecteo = $fe;
				}
				
				if($tip == 6)
				{
					if($fecaj == '0000-00-00')
					{
						$condias = mysqli_query($conectar,"select DATEDIFF(CURDATE(),'".$fe."') dias from usuario LIMIT 1");
						$datodias = mysqli_fetch_array($condias);
						$dias = $datodias['dias'];
					}
					else
					{
						$condias = mysqli_query($conectar,"select DATEDIFF('".$fecaj."','".$fe."') dias from usuario LIMIT 1");
						$datodias = mysqli_fetch_array($condias);
						$dias = $datodias['dias'];
					}
				}
				else
				if($fe == '0000-00-00' or $tip == 5)
				{
					$condias = mysqli_query($conectar,"select DATEDIFF(CURDATE(),'".$fecteo."') dias from usuario LIMIT 1");
					$datodias = mysqli_fetch_array($condias);
					$dias = $datodias['dias'];
				}
				else
				{
					$condias = mysqli_query($conectar,"select DATEDIFF('".$fe."','".$fecteo."') dias from usuario LIMIT 1");
					$datodias = mysqli_fetch_array($condias);
					$dias = $datodias['dias'];
				}	
			?>
			<tr id="service<?php echo $cla; ?>" data="<?php echo $cla; ?>">
				<td class="auto-style21" style="width: 70px"><?php echo $caj; ?></td>
				<td class="auto-style21" style="width: 200px"><?php echo $cajnom; ?></td>
				<td class="auto-style21" style="width: 300px"><?php echo $ale; ?></td>
				<td class="auto-style21" style="width: 120px"><?php echo $act; ?></td>
				<td class="auto-style21" style="width: 100px"><?php echo $fi; ?></td>
				<td class="auto-style21" style="width: 150px"><?php if($tip == 6){ echo $fe; }else { echo $fecteo; } ?></td>
				<td class="auto-style21" style="width: 150px"><?php if($tip == 6){ echo $fecaj; }else { echo $fe; } ?></td>
				<td class="auto-style21" style="width: 150px"><?php echo $dias; ?></td>
				<td class="auto-style21" style="width: 150px">
				<input name="evacuar" id="evacuar" type="button" onclick="EVACUARALERTA('<?php echo $cla; ?>')" value="Omitir" />
				</td>
			</tr>
			<tr>
				<td class="auto-style21" colspan="9">
				<hr>
				</td>
			</tr>
			<?php
			}
			?>
		</table>
		<?php
		$paginacion->render();
		?>
		</fieldset>
<?php
		exit();
	}
?>
<!DOCTYPE HTML>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>CONTROL DE OBRAS</title>
<meta name="description" content="Service Desk">
<meta name="author" content="InvGate S.R.L.">

<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
<!--[if lte IE 7]>
<link rel="stylesheet" href="css/ie.c32bc18afe0e2883ee4912a51f86c119.css" type="text/css" />
<![endif]-->

<link rel="stylesheet" href="css/style.css" type="text/css" />

<script>
function OCULTARSCROLL()
{
	parent.autoResize('iframe28');
	setTimeout("parent.autoResize('iframe28')",500);
	setTimeout("parent.autoResize('iframe28')",1000);
	setTimeout("parent.autoResize('iframe28')",1500);
	setTimeout("parent.autoResize('iframe28')",2000);
	setTimeout("parent.autoResize('iframe28')",2500);
	setTimeout("parent.autoResize('iframe28')",3000);
	setTimeout("parent.autoResize('iframe28')",3500);
	setTimeout("parent.autoResize('iframe28')",4000);
	setTimeout("parent.autoResize('iframe28')",4500);
	setTimeout("parent.autoResize('iframe28')",5000);
	setTimeout("parent.autoResize('iframe28')",5500);
	setTimeout("parent.autoResize('iframe28')",6000);
	setTimeout("parent.autoResize('iframe28')",6500);
	setTimeout("parent.autoResize('iframe28')",7000);
	setTimeout("parent.autoResize('iframe28')",7500);
	setTimeout("parent.autoResize('iframe28')",8000);
	setTimeout("parent.autoResize('iframe28')",8500);
	setTimeout("parent.autoResize('iframe28')",9000);
	setTimeout("parent.autoResize('iframe28')",9500);
	setTimeout("parent.autoResize('iframe28')",10000);
}
parent.autoResize('iframe28');
setTimeout("parent.autoResize('iframe28')",500);
setTimeout("parent.autoResize('iframe28')",1000);
setTimeout("parent.autoResize('iframe28')",1500);
setTimeout("parent.autoResize('iframe28')",2000);
setTimeout("parent.autoResize('iframe28')",2500);
setTimeout("parent.autoResize('iframe28')",3000);
setTimeout("parent.autoResize('iframe28')",3500);
setTimeout("parent.autoResize('iframe28')",4000);
setTimeout("parent.autoResize('iframe28')",4500);
setTimeout("parent.autoResize('iframe28')",5000);
setTimeout("parent.autoResize('iframe28')",5500);
setTimeout("parent.autoResize('iframe28')",6000);
setTimeout("parent.autoResize('iframe28')",6500);
setTimeout("parent.autoResize('iframe28')",7000);
setTimeout("parent.autoResize('iframe28')",7500);
setTimeout("parent.autoResize('iframe28')",8000);
setTimeout("parent.autoResize('iframe28')",8500);
setTimeout("parent.autoResize('iframe28')",9000);
setTimeout("parent.autoResize('iframe28')",9500);
setTimeout("parent.autoResize('iframe28')",10000);
</script>
<?php //VALIDACIONES ?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="llamadas.js"></script>

<?php //CALENDARIO ?>
<link type="text/css" rel="stylesheet" href="../../css/dhtmlgoodies_calendar.css?random=20051112" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../../js/dhtmlgoodies_calendar.js?random=20060118"></script>

<?php //********ESTAS LIBRERIAS JS Y CSS SIRVEN PARA HACER LA BUSQUEDA DINAMICA CON CHECKLIST************//?>
<link rel="stylesheet" type="text/css" href="../../css/checklist/jquery.multiselect.css" />
<link rel="stylesheet" type="text/css" href="../../css/checklist/jquery.multiselect.filter.css" />
<link rel="stylesheet" type="text/css" href="../../css/checklist/styleselect.css" />
<link rel="stylesheet" type="text/css" href="../../css/checklist/prettify.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script type="text/javascript" src="../../js/checklist/jquery.multiselect.js"></script>
<script type="text/javascript" src="../../js/checklist/jquery.multiselect.filter.js"></script>
<script type="text/javascript" src="../../js/checklist/prettify.js"></script>
<?php //**************************************************************************************************//?>
</head>
<body>
<form name="form1" id="form1" action="informes/informeexcel.php" method="post" onsubmit="return selectedVals();">
<!--[if lte IE 7]>
<div class="ieWarning">Este navegador no es compatible con el sistema. Por favor, use Chrome, Safari, Firefox o Internet Explorer 8 o superior.</div>
<![endif]-->
<table style="width: 100%">
	<tr>
		<td class="auto-style1" style="cursor:pointer" colspan="8">
		<strong>ALERTAS</strong></td>
	</tr>
	<tr>
		<td colspan="8" class="auto-style2">
		
		<form name="formulario" method="post">
		<div id="usuarios" style="width:100%">
				<table style="width: 100%" align="center">
					<tr>
						<td class="auto-style3" colspan="11">
						<table style="width: 40%" align="center" class="inputs">
							<tr>
								<td class="auto-style1" style="width: 132px" align="center">
								&nbsp;</td>
								<td class="auto-style3">
								<strong>Cajero:</strong></td>
								<td class="auto-style3" style="width: 80px">
								<strong>Alerta:</strong></td>
								<td class="auto-style3" style="width: 80px">
								<strong>Actor:</strong></td>
							</tr>
							<tr>
								<td class="auto-style1" style="width: 132px" align="center" rowspan="2"><strong>
								<span class="auto-style13">Filtro:</span><img src="../../img/buscar.png" alt="" height="18" width="15" /></strong></td>
								<td class="auto-style3">
								<select class="inputs" multiple="multiple" name="buscajero" id="buscajero" onchange="BUSCAR('')" style="width: 400px">
								<?php
									$con = mysqli_query($conectar,"select * from cajero where caj_clave_int in (select caj_clave_int from alerta) order by caj_nombre");
									$num = mysqli_num_rows($con);
									for($i = 0; $i < $num; $i++)
									{
										$dato = mysqli_fetch_array($con);
										$clave = $dato['caj_clave_int'];
										$nom = $dato['caj_nombre'];
								?>
									<option value="<?php echo $clave; ?>"><?php echo $clave." - ".$nom; ?></option>
								<?php
									}
								?>
								</select>
								</td>
								<td class="auto-style3" style="width: 80px">
								<select class="inputs" multiple="multiple" name="busalerta" id="busalerta" onchange="BUSCAR('')" style="width: 370px">
								<?php
									$con = mysqli_query($conectar,"select * from tipo_alerta order by tal_nombre");
									$num = mysqli_num_rows($con);
									for($i = 0; $i < $num; $i++)
									{
										$dato = mysqli_fetch_array($con);
										$clave = $dato['tal_clave_int'];
										$nom = $dato['tal_nombre'];
								?>
									<option value="<?php echo $clave; ?>"><?php echo $nom; ?></option>
								<?php
									}
								?>
								</select>
								</td>
								<td class="auto-style3" style="width: 80px">
								<select class="inputs" multiple="multiple" name="busactor" id="busactor" onchange="BUSCAR('')" style="width: 370px">
								<?php
									$con = mysqli_query($conectar,"select * from actor order by act_nombre");
									$num = mysqli_num_rows($con);
									for($i = 0; $i < $num; $i++)
									{
										$dato = mysqli_fetch_array($con);
										$clave = $dato['act_clave_int'];
										$nom = $dato['act_nombre'];
								?>
									<option value="<?php echo $clave; ?>"><?php echo $nom; ?></option>
								<?php
									}
								?>
								</select></td>
							</tr>
							<tr>
								<td class="auto-style3">
								<table style="width: 50%">
									<tr>
										<td>
								<input class="inputs" name="busfecinidesde" id="busfecinidesde" onkeyup="BUSCAR('')" onchange="BUSCAR('')" onclick="displayCalendar(document.forms[0].busfecinidesde,'yyyy-mm-dd',this)" maxlength="70" type="text" placeholder="Fecha Inicio Desde" style="width: 150px" /></td>
										<td>-</td>
										<td>
								<input class="inputs" name="busfecinihasta" id="busfecinihasta" onkeyup="BUSCAR('')" onchange="BUSCAR('')" onclick="displayCalendar(document.forms[0].busfecinihasta,'yyyy-mm-dd',this)" maxlength="70" type="text" placeholder="Fecha Inicio Hasta" style="width: 150px" /></td>
									</tr>
								</table>
								</td>
								<td class="auto-style3" style="width: 80px">
								<table style="width: 50%">
									<tr>
										<td>
								<input class="inputs" name="busfecteodesde" id="busfecteodesde" onkeyup="BUSCAR('')" onchange="BUSCAR('')" onclick="displayCalendar(document.forms[0].busfecteodesde,'yyyy-mm-dd',this)" maxlength="70" type="text" placeholder="Fecha Teorica Desde" style="width: 150px" /></td>
										<td>-</td>
										<td>
								<input class="inputs" name="busfecteohasta" id="busfecteohasta" onkeyup="BUSCAR('')" onchange="BUSCAR('')" onclick="displayCalendar(document.forms[0].busfecteohasta,'yyyy-mm-dd',this)" maxlength="70" type="text" placeholder="Fecha Teorica Hasta" style="width: 150px" /></td>
									</tr>
								</table>
								</td>
								<td class="auto-style3" style="width: 80px">
								<table style="width: 50%">
									<tr>
										<td>
								<input class="inputs" name="busfecentdesde" id="busfecentdesde" onkeyup="BUSCAR('')" onchange="BUSCAR('')" onclick="displayCalendar(document.forms[0].busfecentdesde,'yyyy-mm-dd',this)" maxlength="70" type="text" placeholder="Fecha Entrega Desde" style="width: 150px" /></td>
										<td>-</td>
										<td>
								<input class="inputs" name="busfecenthasta" id="busfecenthasta" onkeyup="BUSCAR('')" onchange="BUSCAR('')" onclick="displayCalendar(document.forms[0].busfecenthasta,'yyyy-mm-dd',this)" maxlength="70" type="text" placeholder="Fecha Entrega Hasta" style="width: 150px" /></td>
									</tr>
								</table>
								</td>
							</tr>
							</table>
							<br>
						<div id="alertasactivas" align="center" style="width:100%">
						<fieldset name="Group1">
						<legend align="center">LISTA DE ALERTAS ACTIVAS
						<br>
						<button name="accion" value="excel" type="button" onclick="EXPORTAR()" style="cursor:pointer;">
						<img src="../../images/excel.png" height="30" width="30">
						</button>
						</legend>
						<table style="width: 100%" align="center">
							<tr>
								<td class="auto-style15" style="height: 15px; width: 70px;">
								&nbsp;</td>
								<td class="auto-style15" style="height: 15px; width: 200px;">
								&nbsp;</td>
								<td class="auto-style15" style="height: 15px; width: 300px;">
								</td>
								<td class="auto-style15" style="height: 15px; width: 120px;">
								&nbsp;</td>
								<td class="auto-style15" style="height: 15px; width: 100px;">
								&nbsp;</td>
								<td class="auto-style15" style="height: 15px;">
								&nbsp;</td>
								<td class="auto-style15" style="height: 15px;">
								&nbsp;</td>
								<td class="auto-style15" style="height: 15px;">
								&nbsp;</td>
								<td class="auto-style15" style="height: 15px;">
								&nbsp;</td>
								</tr>
								<tr>
								<td class="auto-style15" style="height: 23px; width: 70px;">
								<strong>Cajero</strong></td>
								<td class="auto-style15" style="height: 23px; width: 200px;">
								<strong>Nombre</strong></td>
								<td class="auto-style15" style="height: 23px; width: 300px;"><strong>
								Alerta</strong></td>
								<td class="auto-style15" style="height: 23px; width: 120px;">
								<strong>Actor</strong></td>
								<td class="auto-style15" style="height: 23px; width: 100px;">
								<strong>Fecha Inicio</strong></td>
								<td class="auto-style15" style="height: 23px; width: 150px;">
								<strong>Fecha teorica entrega</strong></td>
								<td class="auto-style15" style="height: 23px; width: 150px;">
								<strong>Fecha entrega</strong></td>
								<td class="auto-style15" style="height: 23px; width: 150px;">
								<strong>Días retraso</strong></td>
								<td class="auto-style15" style="height: 23px; width: 150px;">
								&nbsp;</td>
							</tr>
							<?php
							$query = mysqli_query($conectar,"select ale_clave_int,a.caj_clave_int,tal_nombre,act_nombre,a.tal_clave_int,ale_fecha_inicio,ale_fecha_entrega,ale_fecha_entrega_cajero from alerta a inner join cajero c on (c.caj_clave_int = a.caj_clave_int) inner join tipo_alerta ta on (ta.tal_clave_int = a.tal_clave_int) inner join actor act on (act.act_clave_int = ta.act_clave_int) where a.ale_sw_eliminado = 0");
							//$res = $con->query($query);
							$num_registros = mysqli_num_rows($query);
			
							$resul_x_pagina = 100;
							//Paginar:
							$paginacion = new Zebra_Pagination();
							$paginacion->records($num_registros);
							$paginacion->records_per_page($resul_x_pagina);
				
							$con = mysqli_query($conectar,"select ale_clave_int,a.caj_clave_int,caj_nombre,tal_nombre,act_nombre,a.tal_clave_int,ale_fecha_inicio,ale_fecha_entrega,ale_fecha_entrega_cajero from alerta a inner join cajero c on (c.caj_clave_int = a.caj_clave_int) inner join tipo_alerta ta on (ta.tal_clave_int = a.tal_clave_int) inner join actor act on (act.act_clave_int = ta.act_clave_int) where a.ale_sw_eliminado = 0 LIMIT ".(($paginacion->get_page() - 1) * $resul_x_pagina). ',' .$resul_x_pagina);
							$num = mysqli_num_rows($con);
							
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$cla = $dato['ale_clave_int'];
								$caj = $dato['caj_clave_int'];
								$cajnom = $dato['caj_nombre'];
								$ale = $dato['tal_nombre'];
								$act = $dato['act_nombre'];
								$tip = $dato['tal_clave_int'];
								$fi = $dato['ale_fecha_inicio'];
								$fe = $dato['ale_fecha_entrega'];
								$fecaj = $dato['ale_fecha_entrega_cajero'];
								
								if($tip == 1)
								{
									$con1 = mysqli_query($conectar,"select ADDDATE('".$fi."', INTERVAL 45 DAY) fecteo from usuario LIMIT 1");
									$dato1 = mysqli_fetch_array($con1);
									$fecteo = $dato1['fecteo'];
								}
								else
								if($tip == 2 or $tip == 3)
								{
									$con1 = mysqli_query($conectar,"select ADDDATE('".$fi."', INTERVAL 7 DAY) fecteo from usuario LIMIT 1");
									$dato1 = mysqli_fetch_array($con1);
									$fecteo = $dato1['fecteo'];
								}
								else
								if($tip == 4)
								{
									$con1 = mysqli_query($conectar,"select ADDDATE('".$fi."', INTERVAL 90 DAY) fecteo from usuario LIMIT 1");
									$dato1 = mysqli_fetch_array($con1);
									$fecteo = $dato1['fecteo'];
								}
								else
								if($tip == 5)
								{
									$fecteo = $fe;
								}
								
								if($tip == 6)
								{
									if($fecaj == '0000-00-00')
									{
										$condias = mysqli_query($conectar,"select DATEDIFF(CURDATE(),'".$fe."') dias from usuario LIMIT 1");
										$datodias = mysqli_fetch_array($condias);
										$dias = $datodias['dias'];
									}
									else
									{
										$condias = mysqli_query($conectar,"select DATEDIFF('".$fecaj."','".$fe."') dias from usuario LIMIT 1");
										$datodias = mysqli_fetch_array($condias);
										$dias = $datodias['dias'];
									}
								}
								else
								if($fe == '0000-00-00' or $tip == 5)
								{
									$condias = mysqli_query($conectar,"select DATEDIFF(CURDATE(),'".$fecteo."') dias from usuario LIMIT 1");
									$datodias = mysqli_fetch_array($condias);
									$dias = $datodias['dias'];
								}
								else
								{
									$condias = mysqli_query($conectar,"select DATEDIFF('".$fe."','".$fecteo."') dias from usuario LIMIT 1");
									$datodias = mysqli_fetch_array($condias);
									$dias = $datodias['dias'];
								}	
							?>
							<tr id="service<?php echo $cla; ?>" data="<?php echo $cla; ?>">
								<td class="auto-style21" style="width: 70px"><?php echo $caj; ?></td>
								<td class="auto-style21" style="width: 200px"><?php echo $cajnom; ?></td>
								<td class="auto-style21" style="width: 300px"><?php echo $ale; ?></td>
								<td class="auto-style21" style="width: 120px"><?php echo $act; ?></td>
								<td class="auto-style21" style="width: 100px"><?php echo $fi; ?></td>
								<td class="auto-style21" style="width: 150px"><?php if($tip == 6){ echo $fe; }else { echo $fecteo; } ?></td>
								<td class="auto-style21" style="width: 150px"><?php if($tip == 6){ echo $fecaj; }else { echo $fe; } ?></td>
								<td class="auto-style21" style="width: 150px"><?php echo $dias; ?></td>
								<td class="auto-style21" style="width: 150px">
								<input name="evacuar" id="evacuar" type="button" onclick="EVACUARALERTA('<?php echo $cla; ?>')" value="Omitir" />
								</td>
							</tr>
							<tr>
								<td class="auto-style21" colspan="9">
								<hr>
								</td>
							</tr>
							<?php
							}
							?>
							</table>
							<?php
							$paginacion->render();
							?>
						</fieldset>
						</div>
						</td>
					</tr>
					<tr>
						<td class="auto-style3" style="width: 150px; height: 23px;">
						&nbsp;</td>
						<td class="auto-style3" style="width: 149px; height: 23px;">
						&nbsp;</td>
						<td class="auto-style3" style="width: 150px; height: 23px;">
						&nbsp;</td>
						<td class="auto-style3" style="width: 150px; height: 23px;">
						&nbsp;</td>
						<td class="auto-style3" style="width: 150px; height: 23px;">
						&nbsp;</td>
						<td class="auto-style3" style="width: 150px; height: 23px;">
						&nbsp;</td>
						<td class="auto-style3" style="width: 100px; height: 23px;">
						&nbsp;</td>
						<td class="auto-style3" style="width: 100px; height: 23px;">
						&nbsp;</td>
						<td class="auto-style3" style="width: 61px; height: 23px;">
						&nbsp;</td>
						<td class="auto-style3" style="width: 252px; height: 23px;">
						&nbsp;</td>
						<td class="auto-style3" style="width: 382px; height: 23px;">
						&nbsp;</td>
					</tr>
					<tr>
						<td class="auto-style3" style="width: 150px; height: 23px;">
							&nbsp;</td>
						<td class="auto-style3" style="width: 150px; height: 23px;">
							&nbsp;</td>
						<td class="auto-style3" style="width: 150px; height: 23px;">
							&nbsp;</td>
						<td class="auto-style3" style="width: 150px; height: 23px;">
							&nbsp;</td>
						<td class="auto-style3" style="width: 150px; height: 23px;">
							&nbsp;</td>
						<td class="auto-style3" style="width: 150px; height: 23px;">
							&nbsp;</td>
						<td class="auto-style3" style="width: 100px; height: 23px;">
							&nbsp;</td>
						<td class="auto-style3" style="width: 100px; height: 23px;">
							&nbsp;</td>
						<td class="auto-style3" style="width: 61px; height: 23px;">
							&nbsp;</td>
						<td class="auto-style3" style="width: 252px; height: 23px;">
							&nbsp;</td>
						<td class="auto-style3" style="width: 382px; height: 23px;">
							&nbsp;</td>
					</tr>
					</table>
		</div>
		</form>
		</td>
	</tr>
	<tr>
		<td style="width: 100px">&nbsp;</td>
		<td style="width: 210px">&nbsp;</td>
		<td style="width: 90px">&nbsp;</td>
		<td style="width: 100px">&nbsp;</td>
		<td style="width: 80px">&nbsp;</td>
		<td style="width: 100px">&nbsp;</td>
		<td style="width: 50px">&nbsp;</td>
		<td style="width: 49px">&nbsp;</td>
	</tr>
	<tr>
		<td style="width: 100px">&nbsp;</td>
		<td style="width: 210px">&nbsp;</td>
		<td style="width: 90px">&nbsp;</td>
		<td style="width: 100px">&nbsp;</td>
		<td style="width: 80px">&nbsp;</td>
		<td style="width: 100px">&nbsp;</td>
		<td style="width: 50px">&nbsp;</td>
		<td style="width: 49px">&nbsp;</td>
	</tr>
</table>
<script type="text/javascript">
$("#buscajero").multiselect().multiselectfilter();
$("#busalerta").multiselect().multiselectfilter();
$("#busactor").multiselect().multiselectfilter();
</script>
</form>
</body>
</html>