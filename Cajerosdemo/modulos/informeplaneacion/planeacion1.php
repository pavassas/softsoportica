<?php
	include('../../data/Conexion.php');
	require("../../Classes/PHPMailer-master/class.phpmailer.php");
	
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_SESSION['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];
		
	date_default_timezone_set('America/Bogota');	
	// verifica si no se ha loggeado
	if(isset($_SESSION['nw']))
	{
	    if($_SESSION['nw']<time())
	    {
	        unset($_SESSION['nw']);
	        //echo "<script>alert('Tiempo agotado - Loguearse nuevamente');</script>";
	        header("LOCATION:../../data/logout.php");
	    }
	    else
	    {
	        $_SESSION['nw'] = time() + (60 * 60);
	    }
	}
	else
	{
	   //echo "<script>alert('Tiempo agotado - Loguearse nuevamente');</script>";
	   header("LOCATION:../../data/logout.php");
	}
	$fecha=date("Y/m/d H:i:s");
	$anocont = date("Y");
	require_once("lib/Zebra_Pagination.php");
	
	$con = mysqli_query($conectar,"select u.usu_clave_int,p.prf_clave_int,p.prf_descripcion,p.prf_sw_actualiza_info from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
	$dato = mysqli_fetch_array($con);
	$clausu = $dato['usu_clave_int'];
	$claprf = $dato['prf_clave_int'];
	$perfil = $dato['prf_descripcion'];
	$actualizainfo = $dato['prf_sw_actualiza_info'];
	
	$con = mysqli_query($conectar,"select per_metodo from permiso where prf_clave_int = '".$claprf."' and ven_clave_int = 4");
	$dato = mysqli_fetch_array($con);
	$metodo = $dato['per_metodo'];
	
	if($_GET['buscar'] == 'si')
	{
		$caj = $_GET['caj'];
		$anocon = $_GET['anocon'];
		$cencos = $_GET['cencos'];
		$cod = $_GET['cod'];
		$reg = $_GET['reg'];
		$mun = $_GET['mun'];
		$tip = $_GET['tip'];
		$tipint = $_GET['tipint'];
		$moda = $_GET['moda'];
		$actor = $_GET['actor'];
		$est = $_GET['est'];
		$fii = $_GET['fii'];
		$ffi = $_GET['ffi'];
		$fie = $_GET['fie'];
		$ffe = $_GET['ffe'];
		
		$seleccionados = explode(',',$caj);
		$num = count($seleccionados);
		$cajeros = array();
		for($i = 0; $i < $num; $i++)
		{
			if($seleccionados[$i] != '')
			{
				$cajeros[$i]=$seleccionados[$i];
			}
		}
		$listacajeros=implode(',',$cajeros);
?>
		<fieldset name="Group1">
		<legend align="center">LISTADO DE CAJEROS<br>
		<button name="Accion" value="excel" title="Exportar a Excel" onclick="EXPORTAR()" type="button" style="cursor:pointer;">
		<img src="../../images/excel.png" height="25" width="25"></button>
		</legend>
		<table style="width: 100%">
		<tr>
			<td class="alinearizq"><strong>Cajero</strong></td>
			<td class="alinearizq"><strong>Nombre</strong></td>
			<td class="alinearizq"><strong>Duración</strong></td>
			<td class="alinearizq"><strong>Comienzo</strong></td>
			<td class="alinearizq"><strong>Fin</strong></td>
			<td class="alinearizq"><strong>Tip. Intervención</strong></td>
			<td class="alinearizq"><strong>Modalidad</strong></td>
			<td class="alinearizq"><strong>Estado</strong></td>
		</tr>
		<?php
		$sql = '';
		if($listacajeros == '')
		{
			$sql = "(c.caj_clave_int = '".$consec."' OR '".$consec."' IS NULL OR '".$consec."' = '') and (caj_nombre LIKE REPLACE('%".$nom."%',' ','%') OR '".$nom."' IS NULL OR '".$nom."' = '') and (caj_ano_contable = '".$anocon."' OR '".$anocon."' IS NULL OR '".$anocon."' = '') and (cco_clave_int = '".$cencos."' OR '".$cencos."' IS NULL OR '".$cencos."' = '') and (caj_codigo_cajero LIKE REPLACE('%".$cod."%',' ','%') OR '".$cod."' IS NULL OR '".$cod."' = '') and (caj_region = '".$reg."' OR '".$reg."' IS NULL OR '".$reg."' = '') and (caj_municipio = '".$mun."' OR '".$mun."' IS NULL OR '".$mun."' = '') and (tip_clave_int = '".$tip."' OR '".$tip."' IS NULL OR '".$tip."' = '') and (tii_clave_int = '".$tipint."' OR '".$tipint."' IS NULL OR '".$tipint."' = '') and (mod_clave_int = '".$moda."' OR '".$moda."' IS NULL OR '".$moda."' = '') and (caj_estado_proyecto = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') and (cai_inmobiliaria = '".$actor."' or cav_visitante = '".$actor."' or cad_disenador = '".$actor."' or cal_gestionador = '".$actor."' or cin_interventor = '".$actor."' or cac_constructor = '".$actor."' or cas_instalador = '".$actor."' or '".$actor."' IS NULL or '".$actor."' = '') and (((cajinm.cai_fecha_ini_inmobiliaria BETWEEN '".$fii." 00:00:00' AND '".$ffi." 23:59:59') or ('".$fii."' Is Null and '".$ffi."' Is Null) or ('".$fii."' = '' and '".$ffi."' = '')) or ((cajvis.cav_fecha_visita BETWEEN '".$fii." 00:00:00' AND '".$ffi." 23:59:59') or ('".$fii."' Is Null and '".$ffi."' Is Null) or ('".$fii."' = '' and '".$ffi."' = '')) or ((cajdis.cad_fecha_inicio_diseno BETWEEN '".$fii." 00:00:00' AND '".$ffi." 23:59:59') or ('".$fii."' Is Null and '".$ffi."' Is Null) or ('".$fii."' = '' and '".$ffi."' = '')) or ((cajlic.cal_fecha_inicio_licencia BETWEEN '".$fii." 00:00:00' AND '".$ffi." 23:59:59') or ('".$fii."' Is Null and '".$ffi."' Is Null) or ('".$fii."' = '' and '".$ffi."' = '')) or ((cajint.cin_fecha_inicio_obra BETWEEN '".$fii." 00:00:00' AND '".$ffi." 23:59:59') or ('".$fii."' Is Null and '".$ffi."' Is Null) or ('".$fii."' = '' and '".$ffi."' = '')) or ((cajinm.cai_fecha_entrega_info_inmobiliaria BETWEEN '".$fie." 00:00:00' AND '".$ffe." 23:59:59') or ('".$fie."' Is Null and '".$ffe."' Is Null) or ('".$fie."' = '' and '".$ffe."' = '')) or ((cajvis.cav_fecha_entrega_informe BETWEEN '".$fie." 00:00:00' AND '".$ffe." 23:59:59') or ('".$fie."' Is Null and '".$ffe."' Is Null) or ('".$fie."' = '' and '".$ffe."' = '')) or ((cajdis.cad_fecha_entrega_info_diseno BETWEEN '".$fie." 00:00:00' AND '".$ffe." 23:59:59') or ('".$fie."' Is Null and '".$ffe."' Is Null) or ('".$fie."' = '' and '".$ffe."' = '')) or ((cajlic.cal_fecha_entrega_info_licencia BETWEEN '".$fie." 00:00:00' AND '".$ffe." 23:59:59') or ('".$fie."' Is Null and '".$ffe."' Is Null) or ('".$fie."' = '' and '".$ffe."' = '')) or ((cajcons.cac_fecha_entrega_atm BETWEEN '".$fie." 00:00:00' AND '".$ffe." 23:59:59') or ('".$fie."' Is Null and '".$ffe."' Is Null) or ('".$fie."' = '' and '".$ffe."' = ''))) and c.caj_sw_eliminado = 0";
		}
		else
		{
			$sql = "(c.caj_clave_int = '".$consec."' OR '".$consec."' IS NULL OR '".$consec."' = '') and (caj_nombre LIKE REPLACE('%".$nom."%',' ','%') OR '".$nom."' IS NULL OR '".$nom."' = '') and (caj_ano_contable = '".$anocon."' OR '".$anocon."' IS NULL OR '".$anocon."' = '') and (cco_clave_int = '".$cencos."' OR '".$cencos."' IS NULL OR '".$cencos."' = '') and (caj_codigo_cajero LIKE REPLACE('%".$cod."%',' ','%') OR '".$cod."' IS NULL OR '".$cod."' = '') and (caj_region = '".$reg."' OR '".$reg."' IS NULL OR '".$reg."' = '') and (caj_municipio = '".$mun."' OR '".$mun."' IS NULL OR '".$mun."' = '') and (tip_clave_int = '".$tip."' OR '".$tip."' IS NULL OR '".$tip."' = '') and (tii_clave_int = '".$tipint."' OR '".$tipint."' IS NULL OR '".$tipint."' = '') and (mod_clave_int = '".$moda."' OR '".$moda."' IS NULL OR '".$moda."' = '') and (caj_estado_proyecto = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') and (cai_inmobiliaria = '".$actor."' or cav_visitante = '".$actor."' or cad_disenador = '".$actor."' or cal_gestionador = '".$actor."' or cin_interventor = '".$actor."' or cac_constructor = '".$actor."' or cas_instalador = '".$actor."' or '".$actor."' IS NULL or '".$actor."' = '') and (((cajinm.cai_fecha_ini_inmobiliaria BETWEEN '".$fii." 00:00:00' AND '".$ffi." 23:59:59') or ('".$fii."' Is Null and '".$ffi."' Is Null) or ('".$fii."' = '' and '".$ffi."' = '')) or ((cajvis.cav_fecha_visita BETWEEN '".$fii." 00:00:00' AND '".$ffi." 23:59:59') or ('".$fii."' Is Null and '".$ffi."' Is Null) or ('".$fii."' = '' and '".$ffi."' = '')) or ((cajdis.cad_fecha_inicio_diseno BETWEEN '".$fii." 00:00:00' AND '".$ffi." 23:59:59') or ('".$fii."' Is Null and '".$ffi."' Is Null) or ('".$fii."' = '' and '".$ffi."' = '')) or ((cajlic.cal_fecha_inicio_licencia BETWEEN '".$fii." 00:00:00' AND '".$ffi." 23:59:59') or ('".$fii."' Is Null and '".$ffi."' Is Null) or ('".$fii."' = '' and '".$ffi."' = '')) or ((cajint.cin_fecha_inicio_obra BETWEEN '".$fii." 00:00:00' AND '".$ffi." 23:59:59') or ('".$fii."' Is Null and '".$ffi."' Is Null) or ('".$fii."' = '' and '".$ffi."' = '')) or ((cajinm.cai_fecha_entrega_info_inmobiliaria BETWEEN '".$fie." 00:00:00' AND '".$ffe." 23:59:59') or ('".$fie."' Is Null and '".$ffe."' Is Null) or ('".$fie."' = '' and '".$ffe."' = '')) or ((cajvis.cav_fecha_entrega_informe BETWEEN '".$fie." 00:00:00' AND '".$ffe." 23:59:59') or ('".$fie."' Is Null and '".$ffe."' Is Null) or ('".$fie."' = '' and '".$ffe."' = '')) or ((cajdis.cad_fecha_entrega_info_diseno BETWEEN '".$fie." 00:00:00' AND '".$ffe." 23:59:59') or ('".$fie."' Is Null and '".$ffe."' Is Null) or ('".$fie."' = '' and '".$ffe."' = '')) or ((cajlic.cal_fecha_entrega_info_licencia BETWEEN '".$fie." 00:00:00' AND '".$ffe." 23:59:59') or ('".$fie."' Is Null and '".$ffe."' Is Null) or ('".$fie."' = '' and '".$ffe."' = '')) or ((cajcons.cac_fecha_entrega_atm BETWEEN '".$fie." 00:00:00' AND '".$ffe." 23:59:59') or ('".$fie."' Is Null and '".$ffe."' Is Null) or ('".$fie."' = '' and '".$ffe."' = ''))) and c.caj_sw_eliminado = 0 and c.caj_clave_int in (".$listacajeros.")";
		}
		//echo "select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int) inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where ".$sql."";
		$query = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int) inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where ".$sql."");
		//$res = $con->query($query);
		$num_registros = mysqli_num_rows($query);

		$resul_x_pagina = 100;
		//Paginar:
		$paginacion = new Zebra_Pagination();
		$paginacion->records($num_registros);
		$paginacion->records_per_page($resul_x_pagina);
		
		$con = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int) inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where ".$sql." LIMIT ".(($paginacion->get_page() - 1) * $resul_x_pagina). ',' .$resul_x_pagina);
		$num = mysqli_num_rows($con);
		for($i = 0; $i < $num; $i++)
		{
			$dato = mysqli_fetch_array($con);
			//Info Base
			$clacaj = $dato['clacaj'];
			$nomcaj = $dato['caj_nombre'];
			$dir = $dato['caj_direccion'];
			$anocon = $dato['caj_ano_contable'];
			$reg = $dato['caj_region'];
			$dep = $dato['caj_departamento'];
			$mun = $dato['caj_municipio'];
			$tip = $dato['tip_clave_int'];
			$tipint = $dato['tii_clave_int'];
			$mod = $dato['mod_clave_int'];
			$conmod = mysqli_query($conectar,"select mod_nombre from modalidad where mod_clave_int = '".$mod."'");
			$datomod = mysqli_fetch_array($conmod);
			$moda = $datomod['mod_nombre'];
			//Info Secun
			$codcaj = $dato['caj_codigo_cajero'];
			$cencos = $dato['cco_clave_int'];
			$refmaq = $dato['rem_clave_int'];
			$ubi = $dato['ubi_clave_int'];
			$codsuc = $dato['caj_codigo_suc'];
			$ubiamt = $dato['caj_ubicacion_atm'];
			$ati = $dato['ati_clave_int'];
			$rie = $dato['caj_riesgo'];
			$are = $dato['are_clave_int'];
			$codrec = $dato['caj_cod_recibido_monto'];
			$codrec = $dato['caj_fecha_creacion'];
			//Info Inmobiliaria
			$reqinm = $dato['cai_req_inmobiliaria'];
			$inmob = $dato['cai_inmobiliaria'];
			$feciniinmob = $dato['cai_fecha_ini_inmobiliaria'];
			$estinmob = $dato['esi_clave_int'];
			$fecentinmob = $dato['cai_fecha_entrega_info_inmobiliaria'];
			//Info Visita Local
			$reqvis = $dato['cav_req_visita_local'];
			$vis = $dato['cav_visitante'];
			$fecvis = $dato['cav_fecha_visita'];
			//Info Diseño
			$reqdis = $dato['cad_req_diseno'];
			$dis = $dato['cad_disenador'];
			$fecinidis = $dato['cad_fecha_inicio_diseno'];
			$estdis = $dato['esd_clave_int'];
			$fecentdis = $dato['cad_fecha_entrega_info_diseno'];
			//Info Licencia
			$reqlic = $dato['cal_req_licencia'];
			$lic = $dato['cal_gestionador'];
			$fecinilic = $dato['cal_fecha_inicio_licencia'];
			$estlic = $dato['esl_clave_int'];
			$fecentlic = $dato['cal_fecha_entrega_info_licencia'];
			//Info Interventoria
			$int = $dato['cin_interventor'];
			$fecteoent = $dato['cin_fecha_teorica_entrega'];
			$feciniobra = $dato['cin_fecha_inicio_obra'];
			$fecpedsum = $dato['cin_fecha_pedido_suministro'];
			$aprovcotiz = $dato['cin_sw_aprov_cotizacion'];
			$cancom = $dato['can_clave_int'];
			$aprovliqui = $dato['cin_sw_aprov_liquidacion'];
			$swimgnex = $dato['cin_sw_img_nexos'];
			$swfot = $dato['cin_sw_fotos'];
			$swact = $dato['cin_sw_actas'];
			$estpro = $dato['caj_estado_proyecto'];
			//Info Constructor
			$cons = $dato['cac_constructor'];
			$cons = $dato['cac_fecha_entrega_atm'];
			$cons = $dato['cac_porcentaje_avance'];
			$cons = $dato['cac_fecha_entrega_cotizacion'];
			$cons = $dato['cac_fecha_entrega_liquidacion'];
			//Info Instalador Seguridad
			$seg = $dato['cas_instalador'];
			$fecingseg = $dato['cas_fecha_ingreso_obra_inst'];
			$codmon = $dato['cas_codigo_monitoreo'];
		?>
		<tr style="cursor:pointer;background-color:#eeeeee" onmouseover="this.style.backgroundColor='#D7D7D7';this.style.color='#000000';" onmouseout="this.style.backgroundColor='#eeeeee';this.style.color='#000000';" onclick="MOSTRARMOVIMIENTO('<?php echo $clacaj; ?>');OCULTARSCROLL()">
			<td class="alinearizq"><?php echo $clacaj; ?></td>
			<td class="alinearizq"><?php echo $nomcaj; ?></td>
			<td class="alinearizq">
			<?php 
				$condur = mysqli_query($conectar,"select DATEDIFF('".$fecteoent."','".$feciniinmob."') duracion from usuario limit 1");
				$datodur = mysqli_fetch_array($condur);
				if($datodur['duracion'] != ''){ echo $datodur['duracion']." Días"; }else{ echo "0 Días"; }
			?>
			</td>
			<td class="alinearizq">
			<?php 
				if($feciniinmob == '' || $feciniinmob == '0000-00-00'){ echo 'Sin definir'; }else{ echo $feciniinmob; }
			?>
			</td>
			<td class="alinearizq">
			<?php 
				if($fecteoent == '' || $fecteoent == '0000-00-00'){ echo 'Sin definir'; }else{ echo $fecteoent; }
			?>
			</td>
			<td class="alinearizq">
			<?php 
				$sql = mysqli_query($conectar,"select tii_nombre from tipointervencion where tii_clave_int = '".$tipint."'");
				$datosql = mysqli_fetch_array($sql);
				$nomtii = $datosql['tii_nombre'];
				echo $nomtii;
			?>
			</td>
			<td class="alinearizq">
			<?php if($mod <= 0){ echo "Sin Definir "; }else{ echo $moda; } ?></td>
			<td class="alinearizq"><?php if($estpro == 1){ echo "ACTIVO"; }elseif($estpro == 2){ echo "SUSPENDIDO"; }elseif($estpro == 3){ echo "ENTREGADO"; }elseif($estpro == 4){ echo "CANCELADO"; } ?></td>
		</tr>
		<tr>
			<td class="alinearizq" colspan="8">
			<div id="movimiento<?php echo $clacaj; ?>" align="left">
			</div>
			</td>
		</tr>
		<tr>
			<td class="alinearizq" colspan="8">
			<hr>
			</td>
		</tr>
		<?php
		}
		?>
		</table>
		</fieldset>
<?php
		$paginacion->render();
		exit();
	}
	if($_GET['verciudades'] == 'si')
	{
		$reg = $_GET['reg'];
?>
		<select name="busmunicipio" id="busmunicipio" onchange="BUSCAR('CAJERO','')" tabindex="8" data-placeholder="-Seleccione-" class="inputs" style="width: 140px">
		<option value="">-Municipio-</option>
		<?php
			$con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo IS NULL and pog_nieto IS NOT NULL and pog_nieto IN (select pog_clave_int from posicion_geografica where pog_hijo = '".$reg."') order by pog_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clave = $dato['pog_clave_int'];
				$nombre = $dato['pog_nombre'];
		?>
			<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
		<?php
			}
		?>
		</select>
<?php
		exit();
	}

if($_GET['vermodalidades1'] == 'si')
	{
		$tii = $_GET['tii'];
?>
		<select name="busmodalidad" id="busmodalidad" onchange="BUSCAR('CAJERO','')" tabindex="8" class="inputs" data-placeholder="-Seleccione-" style="width: 140px">
		<option value="">-Seleccione-</option>
		<?php
			$con = mysqli_query($conectar,"select * from modalidad where mod_clave_int in (select mod_clave_int from intervencion_modalidad where tii_clave_int = ".$tii.") order by mod_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clave = $dato['mod_clave_int'];
				$nombre = $dato['mod_nombre'];
		?>
			<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
		<?php
			}
		?>
		</select>
<?php
		exit();
	}
	if($_GET['mostrarmovimiento'] == 'si')
	{
		$cc = $_GET['cc'];
		$con = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int)  inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where c.caj_clave_int = ".$cc."");
		$dato = mysqli_fetch_array($con);
		$mod = $dato['mod_clave_int'];
		$conmod = mysqli_query($conectar,"select * from modalidad where mod_clave_int = ".$mod."");
		$datomod = mysqli_fetch_array($conmod);
		$modinm = $datomod['mod_sw_inmobiliaria'];
		$dinm = $datomod['mod_dias_inmobiliaria'];
		$modvis = $datomod['mod_sw_visita'];
		$dvis = $datomod['mod_dias_visita'];
		$modcom = $datomod['mod_sw_aprocomite'];
		$dcom = $datomod['mod_dias_aprocomite'];
		$modcon = $datomod['mod_sw_contrato'];
		$dcon = $datomod['mod_dias_contrato'];
		$moddis = $datomod['mod_sw_diseno'];
		$ddis = $datomod['mod_dias_diseno'];
		$modlic = $datomod['mod_sw_licencia'];
		$dlic = $datomod['mod_dias_licencia'];
		$modpre = $datomod['mod_sw_preliminar'];
		$dpre = $datomod['mod_dias_preliminar'];
		$dcons = $datomod['mod_dias_constructor'];
		$modint = $datomod['mod_sw_interventoria'];
		
		if($dinm == ''){ $dinm = 0; }
		if($dvis == ''){ $dvis = 0; }
		if($ddis == ''){ $ddis = 0; }
		if($dlic == ''){ $dlic = 0; }
		if($dcom == ''){ $dcom = 0; }
		if($dcon == ''){ $dcon = 0; }
		if($dpre == ''){ $dpre = 0; }
		if($dcons == ''){ $dcons = 0; }
		
		//Info Inmobiliaria
		$feciniinmob = $dato['cai_fecha_ini_inmobiliaria'];
		//Info Visita Local
		$fecvis = $dato['cav_fecha_visita'];
		//Info Diseño
		$fecinidis = $dato['cad_fecha_inicio_diseno'];
		//Info Licencia
		$fecinilic = $dato['cal_fecha_inicio_licencia'];
		//Info Interventoria
		$fecteoent = $dato['cin_fecha_teorica_entrega'];
		
		$conlin = mysqli_query($conectar,"select * from linea_base where caj_clave_int = ".$cc."");
		$datolin = mysqli_fetch_array($conlin);
		$fii = $datolin['lib_fec_inmobiliaria'];
		$fti = $datolin['lib_fecteo_inmobiliaria'];
		$fiv = $datolin['lib_fec_visita'];
		$ftv = $datolin['lib_fecteo_visita'];
		$ficom = $datolin['lib_fec_comite'];
		$ftcom = $datolin['lib_fecteo_comite'];
		$ficon = $datolin['lib_fec_contrato'];
		$ftcon = $datolin['lib_fecteo_contrato'];
		$fid = $datolin['lib_fec_diseno'];
		$ftd = $datolin['lib_fecteo_diseno'];
		$fil = $datolin['lib_fec_licencia'];
		$ftl = $datolin['lib_fecteo_licencia'];
		$fipre = $datolin['lib_fec_preliminar'];
		$ftpre = $datolin['lib_fecteo_preliminar'];
		$fiint = $datolin['lib_fec_interventoria'];
		$ftint = $datolin['lib_fecteo_interventoria'];
		
		if(strtotime($fecteoent) < strtotime($ftint)){ $fecteodias = $ftint; }else{ $fecteodias = $fecteoent; }
		if(strtotime($feciniinmob) < strtotime($fii)){ $fecinidias = $feciniinmob; }else{ $fecinidias = $fii; }
		
		$con = mysqli_query($conectar,"select DATEDIFF('".$fecteodias."','".$fecinidias."') dias from usuario limit 1");
		$dato = mysqli_fetch_array($con);
		$dias = $dato['dias'];
		
		$semcom = abs(date("W",strtotime($feciniinmob))-date("W",strtotime($fecteoent)));
		?>
		<fieldset name="Group1">
				<legend align="center"><strong>PLANEACIÓN</strong></legend>
			<div style="float:left; width:25%">
			<table style="width: 100%; overflow:auto" class="auto-style8">
				<tr>
					<td colspan="4">
					<div style="width: 100px;">
					<table style="width: 100%">
						<tr>
							<td><div style="width: 8px; word-wrap: break-word; text-align: center; font-size:x-small; border:1px; border-style: solid; border-color:white; color:white"><strong><?php echo "L30 En"; ?></strong></div></td>
						</tr>
					</table>
					</div>
					</td>
				</tr>
				<tr>
					<td style="width: 110px"><strong>NOMBRE TAREA</strong></td>
					<td style="width: 70px"><strong>DURACIÓN</strong></td>
					<td style="width: 85px"><strong>COMIENZO</strong></td>
					<td style="width: 85px"><strong>FIN</strong></td>
				</tr>
				<?php
				$auxsemcor = 0;
				$auxsemlin = 0;
				$swcor = 1;
				$swlin = 1;
				for($i = 0; $i < 8; $i++)
				{
					if($i == 0)
					{
						$nomtar = "Inmobiliaria";
						$con = mysqli_query($conectar,"select ADDDATE('".$feciniinmob."', INTERVAL ".$dinm." DAY) dias from usuario LIMIT 1");
						$dato = mysqli_fetch_array($con);
						$fecteo = $dato['dias'];
						$fecini = $feciniinmob;
						$fi = $fii;
						$ft = $fti;
					}
					else
					if($i == 1)
					{
						$nomtar = "Visita";
						$con = mysqli_query($conectar,"select ADDDATE('".$fecvis."', INTERVAL ".$dvis." DAY) dias from usuario LIMIT 1");
						$dato = mysqli_fetch_array($con);
						$fecteo = $dato['dias'];
						$fecini = $fecvis;
						$fi = $fiv;
						$ft = $ftv;
					}
					else
					if($i == 2)
					{
						$nomtar = "Aprob. comité";
						$con = mysqli_query($conectar,"select ADDDATE('".$fecvis."', INTERVAL ".$dvis." DAY) dias from usuario LIMIT 1");
						$dato = mysqli_fetch_array($con);
						$fecteovis = $dato['dias'];
						
						$con = mysqli_query($conectar,"select ADDDATE('".$fecteovis."', INTERVAL ".$dcom." DAY) dias from usuario LIMIT 1");
						$dato = mysqli_fetch_array($con);
						$fecteo = $dato['dias'];
						$fecini = $fecteovis;
						$fi = $ficom;
						$ft = $ftcom;
					}
					else
					if($i == 3)
					{
						$nomtar = "Contrato";						
						$con = mysqli_query($conectar,"select ADDDATE('".$fecvis."', INTERVAL ".$dvis." DAY) dias from usuario LIMIT 1");
						$dato = mysqli_fetch_array($con);
						$fecteovis = $dato['dias'];
						
						$con = mysqli_query($conectar,"select ADDDATE('".$fecteovis."', INTERVAL ".$dcom." DAY) dias from usuario LIMIT 1");
						$dato = mysqli_fetch_array($con);
						$fecteocom = $dato['dias'];
						
						if($modlic == 1)
						{
							$con = mysqli_query($conectar,"select ADDDATE('".$fecteocom."', INTERVAL ".$dcon." DAY) dias from usuario LIMIT 1");
							$dato = mysqli_fetch_array($con);
							$fecteo = $dato['dias'];
						}
						else
						{
							$con = mysqli_query($conectar,"select ADDDATE('".$fecteocom."', INTERVAL ".$dcon." DAY) dias from usuario LIMIT 1");
							$dato = mysqli_fetch_array($con);
							$fecteo = $dato['dias'];
						}
						
						$fecini = $fecteocom;
						$fi = $ficon;
						$ft = $ftcon;
					}
					else
					if($i == 4)
					{
						$nomtar = "Diseño";
						$con = mysqli_query($conectar,"select ADDDATE('".$fecinidis."', INTERVAL ".$ddis." DAY) dias from usuario LIMIT 1");
						$dato = mysqli_fetch_array($con);
						$fecteo = $dato['dias'];
						$fecini = $fecinidis;
						$fi = $fid;
						$ft = $ftd;
					}
					else
					if($i == 5)
					{
						$nomtar = "Licencia";
						$con = mysqli_query($conectar,"select ADDDATE('".$fecinilic."', INTERVAL ".$dlic." DAY) dias from usuario LIMIT 1");
						$dato = mysqli_fetch_array($con);
						$fecteo = $dato['dias'];
						$fecini = $fecinilic;
						$fi = $fil;
						$ft = $ftl;
					}
					else
					if($i == 6)
					{
						$nomtar = "Preliminar";
						if($modlic == 1)
						{
							$con = mysqli_query($conectar,"select ADDDATE('".$fecinilic."', INTERVAL ".$dlic." DAY) dias from usuario LIMIT 1");
							$dato = mysqli_fetch_array($con);
							$fecteolic = $dato['dias'];
							
							$con = mysqli_query($conectar,"select ADDDATE('".$fecteolic."', INTERVAL ".$dpre." DAY) dias from usuario LIMIT 1");
							$dato = mysqli_fetch_array($con);
							$fecteo = $dato['dias'];
							$fecini = $fecteolic;
						}
						else
						{
							$con = mysqli_query($conectar,"select ADDDATE('".$fecvis."', INTERVAL ".$dvis." DAY) dias from usuario LIMIT 1");
							$dato = mysqli_fetch_array($con);
							$fecteovis = $dato['dias'];
							
							$con = mysqli_query($conectar,"select ADDDATE('".$fecteovis."', INTERVAL ".$dcom." DAY) dias from usuario LIMIT 1");
							$dato = mysqli_fetch_array($con);
							$fecteocom = $dato['dias'];
													
							$con = mysqli_query($conectar,"select ADDDATE('".$fecteocom."', INTERVAL ".$dcon." DAY) dias from usuario LIMIT 1");
							$dato = mysqli_fetch_array($con);
							$fecteocon = $dato['dias'];
							
							$con = mysqli_query($conectar,"select ADDDATE('".$fecteocon."', INTERVAL ".$dpre." DAY) dias from usuario LIMIT 1");
							$dato = mysqli_fetch_array($con);
							$fecteo = $dato['dias'];
							$fecini = $fecteocon;
						}
						$fi = $fipre;
						$ft = $ftpre;
					}
					else
					if($i == 7)
					{
						$nomtar = "Interventoria";
						$fecteo = $fecteoent;
						$con = mysqli_query($conectar,"select ADDDATE('".$fecteo."', INTERVAL -".$dcons." DAY) dias from usuario LIMIT 1");
						$dato = mysqli_fetch_array($con);
						$fecini = $dato['dias'];
						
						//Linea base
						$fi = $fiint;
						$ft = $ftint;
					}
				?>
				<tr>
					<td style="width: 110px;<?php if($i == 2 or $i == 3 or $i == 6){ echo "color:green"; } ?>"><?php echo $nomtar; ?></td>
					<td style="width: 70px;<?php if($i == 2 or $i == 3 or $i == 6){ echo "color:green"; } ?>">
					<?php
						$condur = mysqli_query($conectar,"select DATEDIFF('".$fecteo."','".$fecini."') duracion from usuario limit 1");
						$datodur = mysqli_fetch_array($condur);
						if($datodur['duracion'] != ''){ echo $datodur['duracion']." Días"; }else{ echo "0 Días"; }
					?>
					</td>
					<td style="width: 85px;<?php if($i == 2 or $i == 3 or $i == 6){ echo "color:green"; } ?>"><?php echo $fecini; ?></td>
					<td style="width: 85px;<?php if($i == 2 or $i == 3 or $i == 6){ echo "color:green"; } ?>"><?php if($fecteo != ''){ echo $fecteo; }else{ echo '0000-00-00'; } ?></td>
					<?php 
					if($i > 0)
					{
						$menos = abs(date("W",strtotime($fecini))-date("W",strtotime($fecteo)));
					}
					$auxsemcor = $auxsemcor+abs(date("W",strtotime($fecini))-date("W",strtotime($fecteo)));
					if($i > 0)
					{
						for($j = 1; $j <= $auxsemcor-$menos; $j++)
						{
						?>
						<?php 
							$swcor = 0;
						}
					}
					$menos = 0;
					?>
				</tr>
				<tr>
					<td style="width: 110px; color:blue"><strong><?php echo $nomtar; ?></strong></td>
					<td style="width: 70px; color:blue">
					<strong>
					<?php
						$condur = mysqli_query($conectar,"select DATEDIFF('".$ft."','".$fi."') duracion from usuario limit 1");
						$datodur = mysqli_fetch_array($condur);
						if($datodur['duracion'] != ''){ echo $datodur['duracion']." Días"; }else{ echo "0 Días"; }
					?>
					</strong>
					</td>
					<td style="width: 85px; color:blue"><strong><?php echo $fi; ?></strong></td>
					<td style="width: 85px; color:blue"><strong><?php if($ft != ''){ echo $ft; }else{ echo '0000-00-00'; } ?>
					</strong></td>
					<?php 
					if($i > 0)
					{
						$menos = abs(date("W",strtotime($fecini))-date("W",strtotime($fecteo)));
					}
					$auxsemlin = $auxsemlin+abs(date("W",strtotime($fi))-date("W",strtotime($ft)));
					
					if($i > 0)
					{
						for($j = 1; $j <= $auxsemlin-$menos; $j++)
						{
						?>
						<?php 
							$swlin = 0;
						}
					}
					$menos = 0;
					?>
				</tr>
				<tr>
					<td colspan="4">
					<hr>
					</td>
				</tr>
				<?php
				}
				?>
				</table>
			</div>
			<div style="float:left; width:70%; overflow:auto">
			<table style="width: 100%; overflow:auto" class="auto-style8">
				<tr>
					<td>
					&nbsp;</td>
					<td>
					<div style="width: 800px;">
					<table style="width: 100%">
						<tr>
					<?php 
					for($i = 0; $i <= $dias; $i++)
					{
						$con1 = mysqli_query($conectar,"select ADDDATE('".$fecinidias."', INTERVAL $i DAY) fec,DATE_FORMAT(ADDDATE('".$fecinidias."', INTERVAL $i DAY), '%d') dia,DATE_FORMAT(ADDDATE('".$fecinidias."', INTERVAL $i DAY), '%b') mes from usuario limit 1");
						$dato1 = mysqli_fetch_array($con1);
						$fec = $dato1['fec'];
						$dia = $dato1['dia'];
						$mes = $dato1['mes'];
						
						$vecfec[$i] = $fec;
						
						if($mes == 'Jan'){ $mes = 'En'; }elseif($mes == 'Feb'){ $mes = 'Fb'; }elseif($mes == 'Mar'){ $mes = 'Mr'; }elseif($mes == 'Apr'){ $mes = 'Ab'; }elseif($mes == 'May'){ $mes = 'My'; }elseif($mes == 'Jun'){ $mes = 'Jn'; }elseif($mes == 'Jul'){ $mes = 'Jl'; }elseif($mes == 'Aug'){ $mes = 'Ag'; }elseif($mes == 'Sep'){ $mes = 'St'; }elseif($mes == 'Oct'){ $mes = 'Ot'; }elseif($mes == 'Nov'){ $mes = 'Nv'; }elseif($mes == 'Dec'){ $mes = 'Dc'; }
						
						$con1 = mysqli_query($conectar,"SELECT DATE_FORMAT('".$fec."', '%W') sem from usuario limit 1");
						$dato1 = mysqli_fetch_array($con1);
						$sem = $dato1['sem'];
						
						if($sem == 'Monday'){ $sem = 'L'; }elseif($sem == 'Tuesday'){ $sem = 'M'; }elseif($sem == 'Wednesday'){ $sem = 'X'; }elseif($sem == 'Thursday'){ $sem = 'J'; }elseif($sem == 'Friday'){ $sem = 'V'; }elseif($sem == 'Saturday'){ $sem = 'S'; }elseif($sem == 'Sunday'){ $sem = 'D'; }
					?>
					<td>
					<div style="width: 8px; word-wrap: break-word; text-align: center; font-size:x-small; border:1px; border-style: solid;"><strong><?php echo $sem."".$dia."".$mes; ?></strong></div></td>
					<?php 
					}
					$tamano = sizeof($vecfec);
					?>
					</tr>
					</table>
					</div>
					</td>
				</tr>
				<tr>
					<td style="width: 110px">&nbsp;</td>
					<td style="width: 110px">&nbsp;</td>
				</tr>
				<?php
				$acu1 = 0;
				$acu11 = 0;
				$acu2 = 0;
				$acu22 = 0;
				$d = 0;
				for($i = 0; $i < 8; $i++)
				{
					$acu11 = 0;
					$acu22 = 0;
					if($i == 0)
					{
						$nomtar = "Inmobiliaria";
						$con = mysqli_query($conectar,"select ADDDATE('".$feciniinmob."', INTERVAL ".$dinm." DAY) dias from usuario LIMIT 1");
						$dato = mysqli_fetch_array($con);
						$fecteo = $dato['dias'];
						$fecini = $feciniinmob;
						$fi = $fii;
						$ft = $fti;
					}
					else
					if($i == 1)
					{
						$nomtar = "Visita";
						$con = mysqli_query($conectar,"select ADDDATE('".$fecvis."', INTERVAL ".$dvis." DAY) dias from usuario LIMIT 1");
						$dato = mysqli_fetch_array($con);
						$fecteo = $dato['dias'];
						$fecini = $fecvis;
						$fi = $fiv;
						$ft = $ftv;
					}
					else
					if($i == 2)
					{
						$nomtar = "Aprob. comité";
						$con = mysqli_query($conectar,"select ADDDATE('".$fecvis."', INTERVAL ".$dvis." DAY) dias from usuario LIMIT 1");
						$dato = mysqli_fetch_array($con);
						$fecteovis = $dato['dias'];
						
						$con = mysqli_query($conectar,"select ADDDATE('".$fecteovis."', INTERVAL ".$dcom." DAY) dias from usuario LIMIT 1");
						$dato = mysqli_fetch_array($con);
						$fecteo = $dato['dias'];
						$fecini = $fecteovis;
						$fi = $ficom;
						$ft = $ftcom;
					}
					else
					if($i == 3)
					{
						$nomtar = "Contrato";						
						$con = mysqli_query($conectar,"select ADDDATE('".$fecvis."', INTERVAL ".$dvis." DAY) dias from usuario LIMIT 1");
						$dato = mysqli_fetch_array($con);
						$fecteovis = $dato['dias'];
						
						$con = mysqli_query($conectar,"select ADDDATE('".$fecteovis."', INTERVAL ".$dcom." DAY) dias from usuario LIMIT 1");
						$dato = mysqli_fetch_array($con);
						$fecteocom = $dato['dias'];
						
						if($modlic == 1)
						{
							$con = mysqli_query($conectar,"select ADDDATE('".$fecteocom."', INTERVAL ".$dcon." DAY) dias from usuario LIMIT 1");
							$dato = mysqli_fetch_array($con);
							$fecteo = $dato['dias'];
						}
						else
						{
							$con = mysqli_query($conectar,"select ADDDATE('".$fecteocom."', INTERVAL ".$dcon." DAY) dias from usuario LIMIT 1");
							$dato = mysqli_fetch_array($con);
							$fecteo = $dato['dias'];
						}
						
						$fecini = $fecteocom;
						$fi = $ficon;
						$ft = $ftcon;
					}
					else
					if($i == 4)
					{
						$nomtar = "Diseño";
						$con = mysqli_query($conectar,"select ADDDATE('".$fecinidis."', INTERVAL ".$ddis." DAY) dias from usuario LIMIT 1");
						$dato = mysqli_fetch_array($con);
						$fecteo = $dato['dias'];
						$fecini = $fecinidis;
						$fi = $fid;
						$ft = $ftd;
					}
					else
					if($i == 5)
					{
						$nomtar = "Licencia";
						$con = mysqli_query($conectar,"select ADDDATE('".$fecinilic."', INTERVAL ".$dlic." DAY) dias from usuario LIMIT 1");
						$dato = mysqli_fetch_array($con);
						$fecteo = $dato['dias'];
						$fecini = $fecinilic;
						$fi = $fil;
						$ft = $ftl;
					}
					else
					if($i == 6)
					{
						$nomtar = "Preliminar";
						if($modlic == 1)
						{
							$con = mysqli_query($conectar,"select ADDDATE('".$fecinilic."', INTERVAL ".$dlic." DAY) dias from usuario LIMIT 1");
							$dato = mysqli_fetch_array($con);
							$fecteolic = $dato['dias'];
							
							$con = mysqli_query($conectar,"select ADDDATE('".$fecteolic."', INTERVAL ".$dpre." DAY) dias from usuario LIMIT 1");
							$dato = mysqli_fetch_array($con);
							$fecteo = $dato['dias'];
							$fecini = $fecteolic;
						}
						else
						{
							$con = mysqli_query($conectar,"select ADDDATE('".$fecvis."', INTERVAL ".$dvis." DAY) dias from usuario LIMIT 1");
							$dato = mysqli_fetch_array($con);
							$fecteovis = $dato['dias'];
							
							$con = mysqli_query($conectar,"select ADDDATE('".$fecteovis."', INTERVAL ".$dcom." DAY) dias from usuario LIMIT 1");
							$dato = mysqli_fetch_array($con);
							$fecteocom = $dato['dias'];
													
							$con = mysqli_query($conectar,"select ADDDATE('".$fecteocom."', INTERVAL ".$dcon." DAY) dias from usuario LIMIT 1");
							$dato = mysqli_fetch_array($con);
							$fecteocon = $dato['dias'];
							
							$con = mysqli_query($conectar,"select ADDDATE('".$fecteocon."', INTERVAL ".$dpre." DAY) dias from usuario LIMIT 1");
							$dato = mysqli_fetch_array($con);
							$fecteo = $dato['dias'];
							$fecini = $fecteocon;
						}
						$fi = $fipre;
						$ft = $ftpre;
					}
					else
					if($i == 7)
					{
						$nomtar = "Interventoria";
						$fecteo = $fecteoent;
						$con = mysqli_query($conectar,"select ADDDATE('".$fecteo."', INTERVAL -".$dcons." DAY) dias from usuario LIMIT 1");
						$dato = mysqli_fetch_array($con);
						$fecini = $dato['dias'];
						
						//Linea base
						$fi = $fiint;
						$ft = $ftint;
					}
				?>
				<tr>
					<td style="width: 110px">
					&nbsp;</td>
					<td style="width: 110px">
					<div style="float:left; width: 15px;">
					<table style="width: 100%">
						<tr>
					<?php
					if($fecini <> '0000-00-00')
					{
						$con = mysqli_query($conectar,"select DATEDIFF('".$fecteo."','".$fecini."') dias from usuario limit 1");
						$dato = mysqli_fetch_array($con);
						$dias = $dato['dias'];
						$pas1 = 0;
						$pas2 = 0;
						$com = 0;
						$cq = 0;
						for($j = 0; $j <= $dias; $j++)
						{
							//echo "FECINI: ".$fecini;
							$con1 = mysqli_query($conectar,"select ADDDATE('".$fecini."', INTERVAL $j DAY) fec from usuario limit 1");
							$dato1 = mysqli_fetch_array($con1);
							$fec = $dato1['fec'];
							//echo "FECINI1: ".$fec;
							for($q = $cq; $q < $tamano; $q++)
							{
								if(strtotime($vecfec[$q]) == strtotime($fec))
								{
									$acu1 = $acu1+1;
									$cq = $q+1;
									break;
								}
								else
								{
									$acu11 = $acu11+1;
								}
								
							}
						}
						for($k = 0; $k < $acu11; $k++)
						{
						?>
							<td>
							<div style="width: 8px; font-size:x-small; border:1px; border-style: solid; border-color:white"><strong></strong></div>
							</td>
						<?php
						}
						for($k = 0; $k < $acu1; $k++)
						{
						?>
							<td>
							<div style="width: 8px; word-wrap: break-word; text-align: center; font-size:x-small; border:1px; border-style: solid;"><strong></strong></div>
							</td>
						<?php
						}
						$acu1 = 0;
						$acu11 = 0;
					}
					?>
					</tr>
					</table>
					</div>
					</td>
				</tr>
				<tr>
					<td style="width: 110px">
					&nbsp;</td>
					<td style="width: 110px">
					<div style="float:left; width: 15px;">
					<table style="width: 100%">
						<tr>
					<?php 
					if($fi <> '0000-00-00')
					{
						$con = mysqli_query($conectar,"select DATEDIFF('".$ft."','".$fi."') dias from usuario limit 1");
						$dato = mysqli_fetch_array($con);
						$dias = $dato['dias'];
						$com = 0;
						$cq = 0;
						for($j = 0; $j <= $dias; $j++)
						{
							//echo "FECINI: ".$fecini;
							$con1 = mysqli_query($conectar,"select ADDDATE('".$fi."', INTERVAL $j DAY) fec from usuario limit 1");
							$dato1 = mysqli_fetch_array($con1);
							$fec = $dato1['fec'];
							//echo "FECINI1: ".$fec;
							for($q = $cq; $q < $tamano; $q++)
							{
								if(strtotime($vecfec[$q]) == strtotime($fec))
								{
									$acu2 = $acu2+1;
									$cq = $q+1;
									break;
								}
								else
								{
									$acu22 = $acu22+1;
								}
								
							}
						}
						for($k = 0; $k < $acu22; $k++)
						{
						?>
							<td>
							<div style="width: 8px; font-size:x-small; border:1px; border-style: solid; border-color:white"><strong></strong></div>
							</td>
						<?php
						}
						for($k = 0; $k < $acu2; $k++)
						{
						?>
							<td>
							<div style="width: 8px; word-wrap: break-word; text-align: center; font-size:x-small; border:1px; border-style: solid; background-color:blue; border-color:blue"><strong></strong></div>
							</td>
						<?php
						}
						$acu2 = 0;
						$acu22 = 0;
					}
					?>
					</tr>
					</table>
					</div>
					</td>
				</tr>
				<tr>
					<td>
					&nbsp;</td>
					<td>
					<hr>
					</td>
				</tr>
				<?php
				}
				?>
				</table>
			</div>
		</fieldset>
		<?php
		exit();
	}
?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>SEGUIMIENTO CAJEROS</title>
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />

<?php //VALIDACIONES ?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="llamadas.js"></script>

<script type="text/javascript" src="../../js/jquery.searchabledropdown-1.0.8.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#buscentrocostos").searchable();
	$("#busregion").searchable();
	$("#busmunicipio").searchable();
	$("#bustipologia").searchable();
	$("#bustipointervencion").searchable();
	$("#busmodalidad").searchable();
	$("#busactor").searchable();
	$("#busestado").searchable();
});
function REFRESCARLISTACIUDADES()
{
	$(document).ready(function() {
		$("#busmunicipio").searchable();
	});
}
function REFRESCARLISTAMODALIDADES1()
{
	$(document).ready(function() {
		$("#busmodalidad").searchable();
	});
}
</script>

<?php //CALENDARIO ?>
<link type="text/css" rel="stylesheet" href="../../css/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<SCRIPT type="text/javascript" src="../../js/dhtmlgoodies_calendar.js?random=20060118"></script>

<link rel="stylesheet" href="../../css/jquery-ui.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

<script>
function OCULTARSCROLL()
{
	parent.autoResize('iframe30');
	setTimeout("parent.autoResize('iframe30')",500);
	setTimeout("parent.autoResize('iframe30')",1000);
	setTimeout("parent.autoResize('iframe30')",1500);
	setTimeout("parent.autoResize('iframe30')",2000);
	setTimeout("parent.autoResize('iframe30')",2500);
	setTimeout("parent.autoResize('iframe30')",3000);
	setTimeout("parent.autoResize('iframe30')",3500);
	setTimeout("parent.autoResize('iframe30')",4000);
	setTimeout("parent.autoResize('iframe30')",4500);
	setTimeout("parent.autoResize('iframe30')",5000);
	setTimeout("parent.autoResize('iframe30')",5500);
	setTimeout("parent.autoResize('iframe30')",6000);
	setTimeout("parent.autoResize('iframe30')",6500);
	setTimeout("parent.autoResize('iframe30')",7000);
	setTimeout("parent.autoResize('iframe30')",7500);
	setTimeout("parent.autoResize('iframe30')",8000);
	setTimeout("parent.autoResize('iframe30')",8500);
	setTimeout("parent.autoResize('iframe30')",9000);
	setTimeout("parent.autoResize('iframe30')",9500);
	setTimeout("parent.autoResize('iframe30')",10000);
}
parent.autoResize('iframe30');
setTimeout("parent.autoResize('iframe30')",500);
setTimeout("parent.autoResize('iframe30')",1000);
setTimeout("parent.autoResize('iframe30')",1500);
setTimeout("parent.autoResize('iframe30')",2000);
setTimeout("parent.autoResize('iframe30')",2500);
setTimeout("parent.autoResize('iframe30')",3000);
setTimeout("parent.autoResize('iframe30')",3500);
setTimeout("parent.autoResize('iframe30')",4000);
setTimeout("parent.autoResize('iframe30')",4500);
setTimeout("parent.autoResize('iframe30')",5000);
setTimeout("parent.autoResize('iframe30')",5500);
setTimeout("parent.autoResize('iframe30')",6000);
setTimeout("parent.autoResize('iframe30')",6500);
setTimeout("parent.autoResize('iframe30')",7000);
setTimeout("parent.autoResize('iframe30')",7500);
setTimeout("parent.autoResize('iframe30')",8000);
setTimeout("parent.autoResize('iframe30')",8500);
setTimeout("parent.autoResize('iframe30')",9000);
setTimeout("parent.autoResize('iframe30')",9500);
setTimeout("parent.autoResize('iframe30')",10000);
</script>
<?php //********ESTAS LIBRERIAS JS Y CSS SIRVEN PARA HACER LA BUSQUEDA DINAMICA CON CHECKLIST************//?>
<link rel="stylesheet" type="text/css" href="../../css/checklist/jquery.multiselect.css" />
<link rel="stylesheet" type="text/css" href="../../css/checklist/jquery.multiselect.filter.css" />
<link rel="stylesheet" type="text/css" href="../../css/checklist/styleselect.css" />
<link rel="stylesheet" type="text/css" href="../../css/checklist/prettify.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script type="text/javascript" src="../../js/checklist/jquery.multiselect.js"></script>
<script type="text/javascript" src="../../js/checklist/jquery.multiselect.filter.js"></script>
<script type="text/javascript" src="../../js/checklist/prettify.js"></script>
<?php //**************************************************************************************************//?>

</head>
<body style="background-color:#FDFDFC" id="content">
<form name="form1" id="form1" method="post">
<!--[if lte IE 7]>
<div class="ieWarning">Este navegador no es compatible con el sistema. Por favor, use Chrome, Safari, Firefox o Internet Explorer 8 o superior.</div>
<![endif]-->
	<input name="ocultoestado" id="ocultoestado" value="" type="hidden" />
	<table style="width: 100%">
		<tr>
			<td class="auto-style2" colspan="5" align="center">
			<div id="cajeros">
				<table style="width:100%">
			<tr>
				<td align="left" class="auto-style2">
					<table style="width: 50%">
						<tr>
							<td style="width: 150px; height: 31px;" align="center">
							<strong>CAJERO:</strong></td>
							<td style="height: 31px;" colspan="2">
							<select multiple="multiple" onchange="BUSCAR('CAJERO','')" name="buscajero" id="buscajero" style="width:320px">
							<?php
								$con = mysqli_query($conectar,"select * from cajero where caj_sw_eliminado = 0 order by caj_nombre");
								$num = mysqli_num_rows($con);
								for($i = 0; $i < $num; $i++)
								{
									$dato = mysqli_fetch_array($con);
									$clave = $dato['caj_clave_int'];
									$nombre = $dato['caj_nombre'];
							?>
								<option value="<?php echo $clave; ?>"><?php echo $clave." - ".$nombre; ?></option>
							<?php
								}
							?>
							</select>
							</td>
							<td style="width: 150px; height: 31px;" class="alinearizq">
							<input class="inputs" onkeyup="BUSCAR('CAJERO','')" name="busanocontable" id="busanocontable" maxlength="70" type="text" placeholder="Año Contable" style="width: 150px" /></td>
							<td align="left" style="height: 31px">
							<select name="buscentrocostos" id="buscentrocostos" onchange="BUSCAR('CAJERO','')" tabindex="4" class="inputs" style="width: 158px">
							<option value="">-Centro Costos-</option>
							<?php
								$con = mysqli_query($conectar,"select * from centrocostos where cco_sw_activo = 1 order by cco_nombre");
								$num = mysqli_num_rows($con);
								for($i = 0; $i < $num; $i++)
								{
									$dato = mysqli_fetch_array($con);
									$clave = $dato['cco_clave_int'];
									$nombre = $dato['cco_nombre'];
							?>
								<option value="<?php echo $clave; ?>" <?php if($clave == $cencos){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
							<?php
								}
							?>
							</select></td>
							<td align="left" style="height: 31px">
							<input class="auto-style5" onkeyup="BUSCAR('CAJERO','')" name="buscodigo" id="buscodigo" maxlength="70" type="text" placeholder="Código Cajero" style="width: 150px" /></td>
							<td align="left" style="height: 31px">
							&nbsp;</td>
						</tr>
						<tr>
							<td style="width: 150px; height: 31px;">
							<select name="busregion" id="busregion" onchange="BUSCAR('CAJERO','');VERCIUDADES(this.value)" tabindex="6" class="inputs" style="width: 148px">
								<option value="">-Región-</option>
								<?php
									$con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo IS NULL and pog_nieto IS NULL order by pog_nombre");
									$num = mysqli_num_rows($con);
									for($i = 0; $i < $num; $i++)
									{
										$dato = mysqli_fetch_array($con);
										$clave = $dato['pog_clave_int'];
										$nombre = $dato['pog_nombre'];
								?>
									<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
								<?php
									}
								?>
								</select></td>
							<td style="width: 150px; height: 31px;">
								<div id="ciudades" style="float:left">
								<select name="busmunicipio" id="busmunicipio" onchange="BUSCAR('CAJERO','')" tabindex="8" class="inputs" style="width: 148px">
								<option value="">-Municipio-</option>
								<?php
									$con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo IS NULL and pog_nieto IS NOT NULL order by pog_nombre");
									$num = mysqli_num_rows($con);
									for($i = 0; $i < $num; $i++)
									{
										$dato = mysqli_fetch_array($con);
										$clave = $dato['pog_clave_int'];
										$nombre = $dato['pog_nombre'];
								?>
									<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
								<?php
									}
								?>
								</select>
								</div>
								</td>
							<td style="width: 150px; height: 31px;">
								<select name="bustipologia" id="bustipologia" onchange="BUSCAR('CAJERO','')" tabindex="9" class="inputs" style="width: 148px">
								<option value="">-Tipología-</option>
								<?php
									$con = mysqli_query($conectar,"select * from tipologia where tip_sw_activo = 1 order by tip_nombre");
									$num = mysqli_num_rows($con);
									for($i = 0; $i < $num; $i++)
									{
										$dato = mysqli_fetch_array($con);
										$clave = $dato['tip_clave_int'];
										$nombre = $dato['tip_nombre'];
								?>
									<option value="<?php echo $clave; ?>" <?php if($clave == $tip){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
								<?php
									}
								?>
								</select></td>
							<td style="width: 150px; height: 31px;" class="alinearizq">
							<select name="bustipointervencion" id="bustipointervencion" onchange="BUSCAR('CAJERO','');VERMODALIDADES1(this.value)" tabindex="10" class="inputs" style="width: 158px">
							<option value="">Tipo Intervención</option>
							<?php
								$con = mysqli_query($conectar,"select * from tipointervencion order by tii_nombre");
								$num = mysqli_num_rows($con);
								for($i = 0; $i < $num; $i++)
								{
									$dato = mysqli_fetch_array($con);
									$clave = $dato['tii_clave_int'];
									$nombre = $dato['tii_nombre'];
							?>
								<option value="<?php echo $clave; ?>" <?php if($clave == $tipint){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
							<?php
								}
							?>
							</select></td>
							<td align="left" style="height: 31px">
							<div id="modalidades1" style="float:left">
							<select name="busmodalidad" id="busmodalidad" onchange="BUSCAR('CAJERO','')" tabindex="8" class="inputs" data-placeholder="-Seleccione-" style="width: 160px">
							<option value="">-Modalidad-</option>
							<?php
								$con = mysqli_query($conectar,"select * from modalidad order by mod_nombre");
								$num = mysqli_num_rows($con);
								for($i = 0; $i < $num; $i++)
								{
									$dato = mysqli_fetch_array($con);
									$clave = $dato['mod_clave_int'];
									$nombre = $dato['mod_nombre'];
							?>
								<option value="<?php echo $clave; ?>" <?php if($moda == $clave){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
							<?php
								}
							?>
							</select>
							</div>
							</td>
							<td align="left" style="height: 31px">
							<select name="busactor" id="busactor" onchange="BUSCAR('CAJERO','')" tabindex="20" class="inputs" style="width: 150px">
							<option value="">-Actor-</option>
							<?php
								$con = mysqli_query($conectar,"select * from usuario where (usu_sw_inmobiliaria = 1 or usu_sw_visita = 1 or usu_sw_diseno = 1 or usu_sw_licencia = 1 or usu_sw_interventoria = 1 or usu_sw_constructor = 1 or usu_sw_seguridad = 1) and usu_clave_int NOT IN (select usu_clave_int from usuario_actor) order by usu_nombre");
								$num = mysqli_num_rows($con);
								for($i = 0; $i < $num; $i++)
								{
									$dato = mysqli_fetch_array($con);
									$clave = $dato['usu_clave_int'];
									$nombre = $dato['usu_nombre'];
							?>
								<option value="<?php echo $clave; ?>" <?php if($clave == $inmob){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
							<?php
								}
							?>
							</select>
							</td>
							<td align="left" style="height: 31px">
							<select name="busestado" id="busestado" onchange="BUSCAR('CAJERO','')" tabindex="20" class="inputs" style="width: 150px">
							<option value="">-Estado-</option>
							<option value="1">Activos</option>
							<option value="4">Cancelados</option>
							<option value="3">Entregados</option>
							<option value="2">Suspendidos</option>
							</select>
							</td>
						</tr>
						<tr style="display:none">
							<td style="width: 150px; height: 31px;">
							&nbsp;<strong>FECHA INICIO:</strong></td>
							<td style="width: 150px; height: 31px;">
							&nbsp;<input name="fechainicio" id="fechainicio" placeholder="Fecha Inicio" onchange="BUSCAR('CAJERO','')" readonly="readonly" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px;cursor:pointer"></td>
							<td style="width: 150px; height: 31px;">
							<input name="fechafin" id="fechafin" placeholder="Fecha Fin" onchange="BUSCAR('CAJERO','')" readonly="readonly" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px;cursor:pointer"></td>
							<td style="width: 150px; height: 31px;" class="alinearizq">
							<strong>FECHA ENTREGA:</strong></td>
							<td align="left" style="height: 31px">
							<input name="fechainicioentrega" id="fechainicioentrega" placeholder="Fecha Inicio" onchange="BUSCAR('CAJERO','')" readonly="readonly" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px;cursor:pointer"></td>
							<td align="left" style="height: 31px">
							<input name="fechafinentrega" id="fechafinentrega" placeholder="Fecha Fin" onchange="BUSCAR('CAJERO','')" readonly="readonly" tabindex="31" onclick="displayCalendar(this,'yyyy-mm-dd',this)" class="inputs" type="text" style="width: 140px;cursor:pointer"></td>
							<td align="left" style="height: 31px">
							&nbsp;</td>
						</tr>
						</table>
				</td>
			</tr>
			<tr>
				<td class="auto-style2">
				<div id="busqueda">
				<fieldset name="Group1">
				<legend align="center">LISTADO DE CAJEROS<br>
				<button name="Accion" value="excel" title="Exportar a Excel" onclick="EXPORTAR()" type="button" style="cursor:pointer;">
				<img src="../../images/excel.png" height="25" width="25"></button>
				</legend>
				<table style="width: 100%">
				<tr>
					<td class="alinearizq" style="width: 65px"><strong>Cajero</strong></td>
					<td class="alinearizq"><strong>Nombre</strong></td>
					<td class="alinearizq" style="width: 80px"><strong>Duración</strong></td>
					<td class="alinearizq" style="width: 100px"><strong>Comienzo</strong></td>
					<td class="alinearizq" style="width: 100px"><strong>Fin</strong></td>
					<td class="alinearizq"><strong>Tip. Intervención</strong></td>
					<td class="alinearizq"><strong>Modalidad</strong></td>
					<td class="alinearizq"><strong>Estado</strong></td>
				</tr>
				<?php
				$query = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int)  inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where c.caj_sw_eliminado = 0");
				//$res = $con->query($query);
				$num_registros = mysqli_num_rows($query);
		
				$resul_x_pagina = 100;
				//Paginar:
				$paginacion = new Zebra_Pagination();
				$paginacion->records($num_registros);
				$paginacion->records_per_page($resul_x_pagina);
						
				$con = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int)  inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where c.caj_sw_eliminado = 0 LIMIT ".(($paginacion->get_page() - 1) * $resul_x_pagina). ',' .$resul_x_pagina);
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					//Info Base
					$clacaj = $dato['clacaj'];
					$nomcaj = $dato['caj_nombre'];
					$dir = $dato['caj_direccion'];
					$anocon = $dato['caj_ano_contable'];
					$reg = $dato['caj_region'];
					$dep = $dato['caj_departamento'];
					$mun = $dato['caj_municipio'];
					$tip = $dato['tip_clave_int'];
					$tipint = $dato['tii_clave_int'];
					$mod = $dato['mod_clave_int'];
					$conmod = mysqli_query($conectar,"select mod_nombre from modalidad where mod_clave_int = '".$mod."'");
					$datomod = mysqli_fetch_array($conmod);
					$moda = $datomod['mod_nombre'];
					//Info Secun
					$codcaj = $dato['caj_codigo_cajero'];
					$cencos = $dato['cco_clave_int'];
					$refmaq = $dato['rem_clave_int'];
					$ubi = $dato['ubi_clave_int'];
					$codsuc = $dato['caj_codigo_suc'];
					$ubiamt = $dato['caj_ubicacion_atm'];
					$ati = $dato['ati_clave_int'];
					$rie = $dato['caj_riesgo'];
					$are = $dato['are_clave_int'];
					$codrec = $dato['caj_cod_recibido_monto'];
					$codrec = $dato['caj_fecha_creacion'];
					//Info Inmobiliaria
					$reqinm = $dato['cai_req_inmobiliaria'];
					$inmob = $dato['cai_inmobiliaria'];
					$feciniinmob = $dato['cai_fecha_ini_inmobiliaria'];
					$estinmob = $dato['esi_clave_int'];
					$fecentinmob = $dato['cai_fecha_entrega_info_inmobiliaria'];
					//Info Visita Local
					$reqvis = $dato['cav_req_visita_local'];
					$vis = $dato['cav_visitante'];
					$fecvis = $dato['cav_fecha_visita'];
					//Info Diseño
					$reqdis = $dato['cad_req_diseno'];
					$dis = $dato['cad_disenador'];
					$fecinidis = $dato['cad_fecha_inicio_diseno'];
					$estdis = $dato['esd_clave_int'];
					$fecentdis = $dato['cad_fecha_entrega_info_diseno'];
					//Info Licencia
					$reqlic = $dato['cal_req_licencia'];
					$lic = $dato['cal_gestionador'];
					$fecinilic = $dato['cal_fecha_inicio_licencia'];
					$estlic = $dato['esl_clave_int'];
					$fecentlic = $dato['cal_fecha_entrega_info_licencia'];
					//Info Interventoria
					$int = $dato['cin_interventor'];
					$fecteoent = $dato['cin_fecha_teorica_entrega'];
					$feciniobra = $dato['cin_fecha_inicio_obra'];
					$fecpedsum = $dato['cin_fecha_pedido_suministro'];
					$aprovcotiz = $dato['cin_sw_aprov_cotizacion'];
					$cancom = $dato['can_clave_int'];
					$aprovliqui = $dato['cin_sw_aprov_liquidacion'];
					$swimgnex = $dato['cin_sw_img_nexos'];
					$swfot = $dato['cin_sw_fotos'];
					$swact = $dato['cin_sw_actas'];
					$estpro = $dato['caj_estado_proyecto'];
					//Info Constructor
					$cons = $dato['cac_constructor'];
					$cons = $dato['cac_fecha_entrega_atm'];
					$cons = $dato['cac_porcentaje_avance'];
					$cons = $dato['cac_fecha_entrega_cotizacion'];
					$cons = $dato['cac_fecha_entrega_liquidacion'];
					//Info Instalador Seguridad
					$seg = $dato['cas_instalador'];
					$fecingseg = $dato['cas_fecha_ingreso_obra_inst'];
					$codmon = $dato['cas_codigo_monitoreo'];
				?>
				<tr style="cursor:pointer;background-color:#eeeeee" onmouseover="this.style.backgroundColor='#D7D7D7';this.style.color='#000000';" onmouseout="this.style.backgroundColor='#eeeeee';this.style.color='#000000';" onclick="MOSTRARMOVIMIENTO('<?php echo $clacaj; ?>');OCULTARSCROLL()">
					<td class="alinearizq" style="width: 65px"><?php echo $clacaj; ?></td>
					<td class="alinearizq"><?php echo $nomcaj; ?></td>
					<td class="alinearizq" style="width: 80px">
					<?php 
						$condur = mysqli_query($conectar,"select DATEDIFF('".$fecteoent."','".$feciniinmob."') duracion from usuario limit 1");
						$datodur = mysqli_fetch_array($condur);
						if($datodur['duracion'] != ''){ echo $datodur['duracion']." Días"; }else{ echo "0 Días"; }
					?>
					</td>
					<td class="alinearizq" style="width: 100px">
					<?php 
						if($feciniinmob == '' || $feciniinmob == '0000-00-00'){ echo 'Sin definir'; }else{ echo $feciniinmob; }
					?>
					</td>
					<td class="alinearizq" style="width: 100px">
					<?php 
						if($fecteoent == '' || $fecteoent == '0000-00-00'){ echo 'Sin definir'; }else{ echo $fecteoent; }
					?>
					</td>
					<td class="alinearizq">
					<?php 
						$sql = mysqli_query($conectar,"select tii_nombre from tipointervencion where tii_clave_int = '".$tipint."'");
						$datosql = mysqli_fetch_array($sql);
						$nomtii = $datosql['tii_nombre'];
						echo $nomtii;
					?>
					</td>
					<td class="alinearizq">
					<?php if($mod <= 0){ echo "Sin Definir "; }else{ echo $moda; } ?></td>
					<td class="alinearizq"><?php if($estpro == 1){ echo "ACTIVO"; }elseif($estpro == 2){ echo "SUSPENDIDO"; }elseif($estpro == 3){ echo "ENTREGADO"; }elseif($estpro == 4){ echo "CANCELADO"; } ?></td>
				</tr>
				<tr>
					<td class="alinearizq" colspan="8">
					<div id="movimiento<?php echo $clacaj; ?>" align="left">
					</div>
					</td>
				</tr>
				<tr>
					<td class="alinearizq" colspan="8"><hr></td>
				</tr>
				<?php
				}
				?>
				</table>
				</fieldset>
				<?php $paginacion->render(); ?>
				</div>
			</td>
			</tr>
			</table>		
			</div>
			</td>
		</tr>
		<tr>
			<td style="width: 100px">&nbsp;</td>
			<td style="width: 100px">&nbsp;</td>
			<td style="width: 100px">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
<script type="text/javascript" language="javascript">
    $("#buscajero").multiselect().multiselectfilter();
</script>
</form>
</body>
</html>