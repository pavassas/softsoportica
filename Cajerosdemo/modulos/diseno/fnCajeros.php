<?php
	include('../../data/Conexion.php');
    require("../../Classes/PHPMailer-master/class.phpmailer.php");
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_SESSION['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];
		
	// verifica si no se ha loggeado	
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");
	$opcion = $_POST['opcion'];
    function CORREGIRTEXTO($string)
    {
        $string = str_replace("REEMPLAZARNUMERAL", "#", $string);
        $string = str_replace("REEMPLAZARMAS", "+",$string);
        $string = str_replace("REEMPLAZARMENOS", "-", $string);
        return $string;
    }
   /* function CALCULARFECHA($fi,$d)
    {
        $confec = mysqli_query($conectar,"select DATE_ADD('".$fi."', INTERVAL $d DAY) fecha from usuario LIMIT 1");
        $datofec = mysqli_fetch_array($confec);
        return $datofec['fecha'];
    }*/


if($opcion=="TODOS")
{
    $est = $_POST['est'];
   ?>
    <table style="width: 100%">
        <tr>
            <td align="left" class="auto-style2">
                <table style="width: 50%">
                    <tr>
                        <td style="width: 60px" rowspan="2"><strong>
                                <span class="auto-style13">Filtro:</span><img src="../../img/buscar.png" alt="" height="18" width="15" /></strong></td>
                        <td style="width: 150px; height: 31px;">
                            <input class="inputs" onkeyup="BUSCAR('CAJERO','')" name="busconsecutivo" id="busconsecutivo" maxlength="70" type="text" placeholder="Consecutivo" style="width: 150px" />
                        </td>
                        <td style="width: 150px; height: 31px;">
                            <input class="inputs" onkeyup="BUSCAR('CAJERO','')" name="busnombre" id="busnombre" maxlength="70" type="text" placeholder="Nombre Cajero" style="width: 150px" />
                        </td>
                        <td style="width: 150px; height: 31px;">
                            <input class="inputs" onkeyup="BUSCAR('CAJERO','')" name="busanocontable" id="busanocontable" maxlength="70" type="text" placeholder="Año Contable" style="width: 150px" />
                        </td>
                        <td style="width: 150px; height: 31px;" class="alinearizq">
                            <select name="buscentrocostos" id="buscentrocostos" onchange="BUSCAR('CAJERO','')" tabindex="4" class="inputs" style="width: 158px">
                                <option value="">-Centro Costos-</option>
                                <?php
                                $con = mysqli_query($conectar,"select * from centrocostos where cco_sw_activo = 1 order by cco_nombre");
                                $num = mysqli_num_rows($con);
                                for($i = 0; $i < $num; $i++)
                                {
                                    $dato = mysqli_fetch_array($con);
                                    $clave = $dato['cco_clave_int'];
                                    $nombre = $dato['cco_nombre'];
                                    ?>
                                    <option value="<?php echo $clave; ?>" <?php if($clave == $cencos){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </td>
                        <td align="left" style="height: 31px">
                            <input class="auto-style5" onkeyup="BUSCAR('CAJERO','')" name="buscodigo" id="buscodigo" maxlength="70" type="text" placeholder="Código Cajero" style="width: 150px" />
                        </td>
                        <td align="left" style="height: 31px">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 150px; height: 31px;">
                            <select name="busregion" id="busregion" onchange="BUSCAR('CAJERO','')" tabindex="6" class="inputs buslista" style="width: 148px">
                                <option value="">-Región-</option>
                                <?php
                                $con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo IS NULL and pog_nieto IS NULL order by pog_nombre");
                                $num = mysqli_num_rows($con);
                                for($i = 0; $i < $num; $i++)
                                {
                                    $dato = mysqli_fetch_array($con);
                                    $clave = $dato['pog_clave_int'];
                                    $nombre = $dato['pog_nombre'];
                                    ?>
                                    <option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
                                    <?php
                                }
                                ?>
                            </select></td>
                        <td style="width: 150px; height: 31px;">
                            <select name="busmunicipio" id="busmunicipio" onchange="BUSCAR('CAJERO','')" tabindex="8" class="inputs buslista" style="width: 148px">
                                <option value="">-Municipio-</option>
                                <?php
                                $con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo IS NULL and pog_nieto IS NOT NULL order by pog_nombre");
                                $num = mysqli_num_rows($con);
                                for($i = 0; $i < $num; $i++)
                                {
                                    $dato = mysqli_fetch_array($con);
                                    $clave = $dato['pog_clave_int'];
                                    $nombre = $dato['pog_nombre'];
                                    ?>
                                    <option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
                                    <?php
                                }
                                ?>
                            </select></td>
                        <td style="width: 150px; height: 31px;">
                            <select name="bustipologia" id="bustipologia" onchange="BUSCAR('CAJERO','')" tabindex="9" class="inputs buslista" style="width: 148px">
                                <option value="">-Tipología-</option>
                                <?php
                                $con = mysqli_query($conectar,"select * from tipologia where tip_sw_activo = 1 order by tip_nombre");
                                $num = mysqli_num_rows($con);
                                for($i = 0; $i < $num; $i++)
                                {
                                    $dato = mysqli_fetch_array($con);
                                    $clave = $dato['tip_clave_int'];
                                    $nombre = $dato['tip_nombre'];
                                    ?>
                                    <option value="<?php echo $clave; ?>" <?php if($clave == $tip){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </td>
                        <td style="width: 150px; height: 31px;" class="alinearizq">
                            <select name="bustipointervencion" id="bustipointervencion" onchange="BUSCAR('CAJERO','')" tabindex="10" class="inputs buslista" style="width: 158px">
                                <option value="">-Tipo Intervención-</option>
                                <?php
                                $con = mysqli_query($conectar,"select * from tipointervencion order by tii_nombre");
                                $num = mysqli_num_rows($con);
                                for($i = 0; $i < $num; $i++)
                                {
                                    $dato = mysqli_fetch_array($con);
                                    $clave = $dato['tii_clave_int'];
                                    $nombre = $dato['tii_nombre'];
                                    ?>
                                    <option value="<?php echo $clave; ?>" <?php if($clave == $tipint){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
                                    <?php
                                }
                                ?>
                            </select></td>
                        <td align="left" style="height: 31px">
                            <div id="modalidades1" style="float:left">
                                <select name="busmodalidad" id="busmodalidad" onchange="BUSCAR('CAJERO','');VERMODALIDADES1(this.value)" tabindex="8" class="inputs buslista" data-placeholder="-Seleccione-" style="width: 160px">
                                    <option value="">-Modalidad-</option>
                                    <?php
                                    $con = mysqli_query($conectar,"select * from modalidad order by mod_nombre");
                                    $num = mysqli_num_rows($con);
                                    for($i = 0; $i < $num; $i++)
                                    {
                                        $dato = mysqli_fetch_array($con);
                                        $clave = $dato['mod_clave_int'];
                                        $nombre = $dato['mod_nombre'];
                                        ?>
                                        <option value="<?php echo $clave; ?>" <?php if($moda == $clave){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </td>
                        <td align="left" style="height: 31px">
                            <select name="busactor" id="busactor" onchange="BUSCAR('CAJERO','')" tabindex="20" class="inputs buslista" style="width: 150px">
                                <option value="">-Actor-</option>
                                <?php
                                $con = mysqli_query($conectar,"select * from usuario where (usu_categoria = 2 and usu_sw_visita = 1 and usu_sw_activo = 1) OR (usu_clave_int NOT IN (select usu_clave_int from usuario_actor) and usu_sw_visita = 1 and usu_sw_activo = 1) order by usu_nombre");
                                $num = mysqli_num_rows($con);
                                for($i = 0; $i < $num; $i++)
                                {
                                    $dato = mysqli_fetch_array($con);
                                    $clave = $dato['usu_clave_int'];
                                    $nombre = $dato['usu_nombre'];
                                    ?>
                                    <option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">
                <div id="busqueda" >

        </td>
    </tr>
    </table>
    <?php
    echo "<style onload=INICIALIZARLISTAS()></style>";
    echo "<style onload=BUSCAR('CAJERO','')></style>";
}
else if($opcion=="BUSCAR")
{
    ?>
    <script src="jslistacajeros.js"></script>
    <table style="width: 100%" class="table stripe table-striped" id="tbCajeros">
        <thead>
        <tr>
            <th class="alinearizq">&nbsp;</th>
            <th class="alinearizq"><strong>Consecutivo</strong></th>
            <th class="alinearizq"><strong>Código</strong></th>
            <th class="alinearizq"><strong>Nombre</strong></th>
            <th class="alinearizq"><strong>Padre/Hijo</strong></th>
            <th class="alinearizq"><strong>Región</strong></th>
            <th class="alinearizq"><strong>Tipología</strong></th>
            <th class="alinearizq"><strong>Tip. Intervención</strong></th>
            <th class="alinearizq"><strong>Estado</strong></th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        </tfoot>
        <tbody></tbody>
    </table>
    <?php
}
else if($opcion=="VERMODALIDADES")
{
    $tii = $_POST['tii'];
    if($tii=="")
    {
        $con = mysqli_query($conectar,"select * from modalidad order by mod_nombre");
    }
    else {
        $con = mysqli_query($conectar,"select * from modalidad where mod_clave_int in (select mod_clave_int from intervencion_modalidad where tii_clave_int = " . $tii . " or '" . $tii . "' IS NULL or '" . $tii . "' = '') order by mod_nombre");
    }
    $num = mysqli_num_rows($con);
    for($i = 0; $i < $num; $i++) {
        $dato = mysqli_fetch_array($con);
        $clave = $dato['mod_clave_int'];
        $nombre = $dato['mod_nombre'];
        $datos[] = array("id"=>$clave,"literal"=>$nombre);
    }
    echo json_encode($datos);
}
else if($opcion=="GUARDARDATOS")
{
        $fecha=date("Y/m/d H:i:s");
        $caj = $_POST['caj'];
        $est = $_POST['est'];
        $fecent = $_POST['fecent'];
        $fecest = $_POST['fecest'];
        $cambioshijos =$_POST['cambioshijos'];

        

        $conpad = mysqli_query($conectar,"select caj_padre,(select c.caj_nombre from cajero c where c.caj_clave_int = cajero.caj_padre) nomcaj from cajero where caj_clave_int = ".$caj."");
        $datopad = mysqli_fetch_array($conpad);
        $mipad = $datopad['caj_padre'];
        $nomcaj = CORREGIRTEXTO($datopad['nomcaj']);


        $veriadj = mysqli_query($conectar, "SELECT * FROM adjunto_actor aa JOIN tipo_adjunto ta ON ta.tad_clave_int = aa.tad_clave_int WHERE ta.act_clave_int = '4' and aa.caj_clave_int = '".$caj."'");
        $numadj = mysqli_num_rows($veriadj);

        if($est==4 and ($fecent=="" || $fecent==NULL || $fecent=="0000-00-00"))
        {
            echo "<div class='validaciones'>Debe ingresara la fecha de entrega de info, para poder pasar a estado entregado. Verificar</div>";
        }
        else if($est==4 and $numadj<=0)
        {
            echo "<div class='validaciones'>Debe tener al menos un adjunto, para poder pasar a estado entregado. Verificar</div>";
        }
        else
        {
            /*if($mipad != 0 and $mipad != '')
            {
                echo "<div class='validaciones'>No puede editar porque Este cajero es hijo del cajero: $mipad - $nomcaj</div>";
            }
            else
            {*/
                $con = mysqli_query($conectar,"update cajero_diseno set esd_clave_int = '".$est."', cad_fecha_entrega_info_diseno = '".$fecent."' where caj_clave_int = '".$caj."'");
                
                //DB->20150711: Valida si el cajero tiene alerta para identificar las fechas y actualizar la alerta
                //validarAletas($caj, $fecent);

                if($con > 0)
                {
                    //VERIFICO FECHAS DE ESTADOS
                    if($cambioshijos=="si")
                    {
                        $conh = mysqli_query($conectar,"select caj_clave_int from cajero where caj_padre ='".$caj."' and caj_sw_eliminado = 0");
                        $numh = mysqli_num_rows($conh);
                        for($i = 0; $i < $numh; $i++)
                        {
                            $datoh = mysqli_fetch_array($conh);
                            $claveh = $datoh['caj_clave_int'];

                            mysqli_query($conectar,"delete from cajero_diseno where caj_clave_int = ".$claveh."");

                            $insh = mysqli_query($conectar,"insert into cajero_diseno select null,".$claveh.",cad_req_diseno,cad_disenador,cad_fecha_inicio_diseno,esd_clave_int,cad_sw_envioestado,cad_sw_envio_entregado,cad_sw_aprob_esquema,cad_sw_envio_esquema,cad_sw_aprob_ppt,cad_sw_envio_ppt,cad_sw_aprob_entregado,cad_sw_envio_aprobentregado,cad_fecha_entrega_info_diseno from cajero_diseno where caj_clave_int = ".$caj."");
                            if($insh>0)
                            {
                                 $deleteconsolidarh = mysqli_query($conectar,"DELETE FROM cajero_consolidado WHERE caj_clave_int = '".$claveh."'");
                                 $consolidarcajeroh = mysqli_query($conectar,"INSERT INTO cajero_consolidado (caj_clave_int ,  `caj_nombre` ,  `caj_direccion` ,`caj_ano_contable` ,`caj_region` ,`tpr_clave_int`, `caj_departamento` ,  `caj_municipio` , `tip_clave_int` ,`tii_clave_int` ,`mod_clave_int`,  `caj_codigo_cajero` ,  `cco_clave_int`,  `rem_clave_int` ,
                                `ubi_clave_int` ,  `caj_codigo_suc` ,  `caj_ubicacion_atm` ,  `ati_clave_int`,  `caj_riesgo` ,  `are_clave_int` ,  `caj_fecha_inicio` ,  `caj_fecha_creacion` ,  `caj_estado_proyecto`,  `caj_sw_eliminado` ,  `caj_sw_padre`,  `caj_padre` ,  `caj_fec_apagado_atm` ,  `caj_sw_bancolombia`,  `caj_modalidad` ,  `caj_convenio`,  `cac_constructor`,  `cac_fecha_entrega_atm`,  `cac_porcentaje_avance`,  `cac_adjunto_avance`,  `cac_fecha_entrega_cotizacion`, `cac_fecha_entrega_liquidacion`,  `cac_fecha_inicio_constructor`,  `cad_req_diseno` ,  `cad_disenador` ,  `cad_fecha_inicio_diseno` ,  `esd_clave_int` ,  `cad_sw_envioestado` ,
                                `cad_sw_envio_entregado`,  `cad_sw_aprob_esquema`,  `cad_sw_envio_esquema`,  `cad_sw_aprob_ppt`,  `cad_sw_envio_ppt` ,  `cad_sw_aprob_entregado` ,  `cad_sw_envio_aprobentregado` ,  `cad_fecha_entrega_info_diseno` , `cai_req_inmobiliaria`,  `cai_inmobiliaria`,  `cai_fecha_ini_inmobiliaria` ,  `esi_clave_int` ,  `cai_fecha_entrega_info_inmobiliaria` ,   `cin_interventor` ,  `cin_fecha_teorica_entrega` ,  `cin_fecha_inicio_obra`,  `cin_fecha_pedido_suministro`,
                                `cin_sw_aprov_cotizacion` ,  `can_clave_int` ,  `lit_clave_int` ,  `cin_fecha_entrega_canal` , `cin_fecha_aprov_comite` ,  `cin_sw_aprov_liquidacion` ,  `cin_sw_img_nexos` ,  `cin_sw_fotos` ,  `cin_sw_actas`,  `cin_fecha_inicio_interventoria` ,  `cal_req_licencia` , `cal_gestionador` , `cal_fecha_inicio_licencia` ,  `esl_clave_int` ,  `cal_fecha_entrega_info_licencia`, `cal_curaduria` ,  `cal_radicado` ,  `cas_instalador`,  `cas_fecha_ingreso_obra_inst` ,  `cas_codigo_monitoreo` ,  `cas_fecha_inicio_seguridad` ,  `cav_req_visita_local` ,  `cav_visitante` ,  `cav_fecha_visita`,  `cav_fecha_entrega_informe` ,
                                `cav_estado` ,  `caf_num_ot_inmobiliaria`,  `caf_valor_inmobiliaria` ,  `caf_sw_cotiz_liqui_inmobiliaria`,  `caf_num_ot_visita` ,  `caf_valor_visita`,  `caf_sw_cotiz_liqui_visita` ,  `caf_num_ot_diseno`,  `caf_valor_diseno` ,  `caf_sw_cotiz_liqui_diseno` ,  `caf_num_ot_licencia` ,
                                `caf_valor_licencia`,  `caf_sw_cotiz_liqui_licencia` ,  `caf_num_ot_interventoria` ,  `caf_valor_interventoria`,  `caf_sw_cotiz_liqui_interventoria` , `caf_num_ot_constructor` ,  `caf_valor_constructor` ,  `caf_sw_cotiz_liqui_constructor` ,  `caf_num_ot_seguridad`,  `caf_valor_seguridad`, `caf_sw_cotiz_liqui_seguridad` ,  `bac_sw_aprobado` ,  `bac_fecha_inicio`,  `bac_fec_ini_teorica` ,  `bac_fec_ent_teorica` ,  `bac_fec_comite`, `bac_sw_contrato`,  `bac_sw_canal` ,  `bac_sw_diseno`,  `bac_sw_envio_contrato`,  `bac_sw_envio_canal` ,  `bac_usu_actualiz` ,  `bac_fec_actualiz` ,  `bco_sw_aprobado` ,  `bes_clave_int` ,  `bco_fec_ini_teorica` ,  `bco_fec_ent_teorica`,  `bco_fec_entrega`,  `bco_usu_actualiz`,  `bco_fec_actualiz`,  `bca_fec_ini_teorica`,  `bca_fec_ent_teorica`,  `bca_fec_inicio` ,  `bca_fec_entrega` ,  `bca_usu_actualiz`,  `bca_fec_actualiz`,   `bma_clave_int`,  `bpm_sw_aprobado` ,  `bpm_usu_actualiz` ,  `bpm_fec_actualiz` ,  `bpr_sw_aprobado`,  `bpr_fec_ini_teorica` ,  `bpr_fec_ent_teorica` ,  `bpr_fecha_entrega` ,
                                `bpr_sw_envio_fecha` ,  `bpr_usu_actualiz`,  `bpr_fec_actualiz`,  `bpi_fec_ini_teorica`,  `bpi_fec_ent_teorica`,  `bpi_usu_actualiz`,  `bpi_fec_actualiz`,caj_ot,tit_clave_int) 
                                SELECT c.caj_clave_int ,  `caj_nombre` ,  `caj_direccion` ,`caj_ano_contable` ,`caj_region` ,`tpr_clave_int`, `caj_departamento` ,  `caj_municipio` , `tip_clave_int` ,`tii_clave_int` ,`mod_clave_int`,  `caj_codigo_cajero` ,  `cco_clave_int`,  `rem_clave_int` ,
                                `ubi_clave_int` ,  `caj_codigo_suc` ,  `caj_ubicacion_atm` ,  `ati_clave_int`,  `caj_riesgo` ,  `are_clave_int` ,  `caj_fecha_inicio` ,  `caj_fecha_creacion` ,  `caj_estado_proyecto`,  `caj_sw_eliminado` ,  `caj_sw_padre`,  `caj_padre` ,  `caj_fec_apagado_atm` ,  `caj_sw_bancolombia`,  `caj_modalidad` ,  `caj_convenio`,  `cac_constructor`,  `cac_fecha_entrega_atm`,  `cac_porcentaje_avance`,  `cac_adjunto_avance`,  `cac_fecha_entrega_cotizacion`, `cac_fecha_entrega_liquidacion`,  `cac_fecha_inicio_constructor`,  `cad_req_diseno` ,  `cad_disenador` ,  `cad_fecha_inicio_diseno` ,  `esd_clave_int` ,  `cad_sw_envioestado` ,
                                `cad_sw_envio_entregado`,  `cad_sw_aprob_esquema`,  `cad_sw_envio_esquema`,  `cad_sw_aprob_ppt`,  `cad_sw_envio_ppt` ,  `cad_sw_aprob_entregado` ,  `cad_sw_envio_aprobentregado` ,  `cad_fecha_entrega_info_diseno` , `cai_req_inmobiliaria`,  `cai_inmobiliaria`,  `cai_fecha_ini_inmobiliaria` ,  `esi_clave_int` ,  `cai_fecha_entrega_info_inmobiliaria` ,   `cin_interventor` ,  `cin_fecha_teorica_entrega` ,  `cin_fecha_inicio_obra`,  `cin_fecha_pedido_suministro`,
                                `cin_sw_aprov_cotizacion` ,  `can_clave_int` ,  `lit_clave_int` ,  `cin_fecha_entrega_canal` , `cin_fecha_aprov_comite` ,  `cin_sw_aprov_liquidacion` ,  `cin_sw_img_nexos` ,  `cin_sw_fotos` ,  `cin_sw_actas`,  `cin_fecha_inicio_interventoria` ,  `cal_req_licencia` , `cal_gestionador` , `cal_fecha_inicio_licencia` ,  `esl_clave_int` ,  `cal_fecha_entrega_info_licencia`, `cal_curaduria` ,  `cal_radicado` ,  `cas_instalador`,  `cas_fecha_ingreso_obra_inst` ,  `cas_codigo_monitoreo` ,  `cas_fecha_inicio_seguridad` ,  `cav_req_visita_local` ,  `cav_visitante` ,  `cav_fecha_visita`,  `cav_fecha_entrega_informe` ,
                                `cav_estado` ,  `caf_num_ot_inmobiliaria`,  `caf_valor_inmobiliaria` ,  `caf_sw_cotiz_liqui_inmobiliaria`,  `caf_num_ot_visita` ,  `caf_valor_visita`,  `caf_sw_cotiz_liqui_visita` ,  `caf_num_ot_diseno`,  `caf_valor_diseno` ,  `caf_sw_cotiz_liqui_diseno` ,  `caf_num_ot_licencia` ,
                                `caf_valor_licencia`,  `caf_sw_cotiz_liqui_licencia` ,  `caf_num_ot_interventoria` ,  `caf_valor_interventoria`,  `caf_sw_cotiz_liqui_interventoria` , `caf_num_ot_constructor` ,  `caf_valor_constructor` ,  `caf_sw_cotiz_liqui_constructor` ,  `caf_num_ot_seguridad`,  `caf_valor_seguridad`, `caf_sw_cotiz_liqui_seguridad` ,  `bac_sw_aprobado` ,  `bac_fecha_inicio`,  `bac_fec_ini_teorica` ,  `bac_fec_ent_teorica` ,  `bac_fec_comite`, `bac_sw_contrato`,  `bac_sw_canal` ,  `bac_sw_diseno`,  `bac_sw_envio_contrato`,  `bac_sw_envio_canal` ,  `bac_usu_actualiz` ,  `bac_fec_actualiz` ,  `bco_sw_aprobado` ,  `bes_clave_int` ,  `bco_fec_ini_teorica` ,  `bco_fec_ent_teorica`,  `bco_fec_entrega`,  `bco_usu_actualiz`,  `bco_fec_actualiz`,  `bca_fec_ini_teorica`,  `bca_fec_ent_teorica`,  `bca_fec_inicio` ,  `bca_fec_entrega` ,  `bca_usu_actualiz`,  `bca_fec_actualiz`,   `bma_clave_int`,  `bpm_sw_aprobado` ,  `bpm_usu_actualiz` ,  `bpm_fec_actualiz` ,  `bpr_sw_aprobado`,  `bpr_fec_ini_teorica` ,  `bpr_fec_ent_teorica` ,  `bpr_fecha_entrega` ,
                                `bpr_sw_envio_fecha` ,  `bpr_usu_actualiz`,  `bpr_fec_actualiz`,  `bpi_fec_ini_teorica`,  `bpi_fec_ent_teorica`,  `bpi_usu_actualiz`,  `bpi_fec_actualiz`,caj_ot,tit_clave_int
                                FROM cajero c LEFT OUTER JOIN cajero_inmobiliaria cajinm ON (   cajinm.caj_clave_int = c.caj_clave_int)
                                LEFT OUTER JOIN cajero_visita cajvis ON (cajvis.caj_clave_int = c.caj_clave_int)
                                LEFT OUTER JOIN cajero_diseno cajdis ON (cajdis.caj_clave_int = c.caj_clave_int)
                                LEFT OUTER JOIN cajero_licencia cajlic ON (cajlic.caj_clave_int = c.caj_clave_int)
                                LEFT OUTER JOIN cajero_interventoria cajint ON (cajint.caj_clave_int = c.caj_clave_int)
                                LEFT OUTER JOIN cajero_constructor cajcons ON (cajcons.caj_clave_int = c.caj_clave_int)
                                LEFT OUTER JOIN cajero_seguridad cajseg ON (cajseg.caj_clave_int = c.caj_clave_int)
                                LEFT OUTER JOIN cajero_facturacion cajfac ON (cajfac.caj_clave_int = c.caj_clave_int)
                                LEFT OUTER JOIN bancolombia_canal bcan ON (bcan.caj_clave_int = c.caj_clave_int)
                                LEFT OUTER JOIN bancolombia_comite bcom ON (bcom.caj_clave_int = c.caj_clave_int)
                                LEFT OUTER JOIN bancolombia_contrato bcon ON (bcon.caj_clave_int = c.caj_clave_int)
                                LEFT OUTER JOIN bancolombia_pedidomaquina bped ON (bped.caj_clave_int = c.caj_clave_int)
                                LEFT OUTER JOIN bancolombia_prefactibilidad bpre ON (bpre.caj_clave_int = c.caj_clave_int)
                                LEFT OUTER JOIN bancolombia_proyeccioninstalacion bpro ON (bpro.caj_clave_int = c.caj_clave_int)
                                WHERE c.caj_clave_int  ='".$claveh."' GROUP BY c.caj_clave_int ORDER BY c.caj_nombre ASC");
                            }
                        }
                    }
                    $con = mysqli_query($conectar,"select * from fecha_estado where esd_clave_int = '".$est."' and caj_clave_int = '".$caj."' and ven_clave_int = '8'");
                    $num = mysqli_num_rows($con);
                    if($num > 0 and $fecest != '')
                    {
                        mysqli_query($conectar,"update fecha_estado set fee_fecha = '".$fecest."'  where caj_clave_int = '".$caj."' and ven_clave_int = 8 and  esd_clave_int = '".$est."'");
                    }
                    else
                    if($num <= 0 and $fecest != '')
                    {
                        mysqli_query($conectar,"insert into fecha_estado(caj_clave_int,esd_clave_int,fee_fecha,fee_usu_actualiz,fee_fec_actualiz,ven_clave_int) values('".$caj."','".$est."','".$fecest."','".$usuario."','".$fecha."','8')");
                    }
                    
                    echo "<div class='ok' style='width: 100%' align='center'>Datos grabados correctamente</div>";
                    
                    //Envió correo de alerta en caso de que el estado pase a entregado
                    $con1 = mysqli_query($conectar,"select * from estado_diseno where esd_clave_int = '".$est."'");
                    $dato1 = mysqli_fetch_array($con1);
                    $nomest = $dato1['esd_nombre'];
                    
                    $con1 = mysqli_query($conectar,"select cd.cad_sw_envioestado,cd.cad_sw_envio_entregado,u.usu_nombre from cajero_diseno cd left outer join usuario u on (u.usu_clave_int = cd.cad_disenador) where cd.caj_clave_int = '".$caj."'");
                    $dato1 = mysqli_fetch_array($con1);
                    $swenvioestado = $dato1['cad_sw_envioestado'];
                    $swenvio = $dato1['cad_sw_envio_entregado'];
                    $disenador = $dato1['usu_nombre'];
                    
                    if(strtoupper($nomest) == strtoupper("Esquema") and $est != $swenvioestado)
                    {
                        //alerta12
                        $conu = mysqli_query($conectar,"select usu_clave_int from asignacion_alerta where tas_clave_int = '10'");
                        $numu = mysqli_num_rows($conu);
                        if($numu>0)
                        {
                            $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario where usu_clave_int in (select usu_clave_int from asignacion_alerta where tas_clave_int = '10')");
                        }
                        else
                        {
                            $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario where usu_usuario in ('superadmin','marta.lopez','jorge.bermudez')");
                        }
                        //$con = mysqli_query($conectar,"select usu_nombre,usu_email from usuario where usu_usuario in ('superadmin','marta.lopez','jorge.bermudez')");
                        $num = mysqli_num_rows($conema);
                        for($i = 0; $i < $num; $i++)
                        {
                            $mail = new PHPMailer();
                            
                            $dato1 = mysqli_fetch_array($conema);
                            $mail->Body = '';
                            $ema = '';
                            $nom = $dato1['usu_nombre'];
                            $ema = $dato1['usu_email'];
                            
                            $concaj = mysqli_query($conectar,"select caj_nombre from cajero where caj_clave_int = '".$caj."'");
                            $datocaj = mysqli_fetch_array($concaj);
                            $nomcaj = CORREGIRTEXTO($datocaj['caj_nombre']);
                            
                            $mail->From = "adminpavas@pavas.com.co";
                            $mail->FromName = "Soportica";
                            $mail->AddAddress($ema, "Destino");
                            $mail->Subject = "Cajero Nro. ".$caj." Ha pasado de estado a esquemas";
                            
                            // Cuerpo del mensaje
                            $mail->Body .= "Hola ".$nom."!\n\n";    
                            $mail->Body .= "CONTROL CAJEROS le informa que este cajero ha pasado de estado a esquemas.\n";
                            $mail->Body .= "CAJERO: ".$nomcaj."\n";
                            $mail->Body .= "DISENADOR: ".$disenador."\n";
                            $mail->Body .= date("d/m/Y H:m:s")."\n\n";
                            $mail->Body .= "Este mensaje es generado automáticamente por CONTROL CAJEROS, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Cajeros \n";
                            
                            if(!$mail->Send())
                            {
                                echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                                echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                            }
                            else
                            {
                                mysqli_query($conectar,"update cajero_diseno set cad_sw_envioestado = '".$est."' where caj_clave_int = '".$caj."'");
                            }
                        }
                    }
                    if(strtoupper($nomest) == strtoupper("PPT") and $est != $swenvioestado)
                    {
                        //alerta13
                        $conu = mysqli_query($conectar,"select usu_clave_int from asignacion_alerta where tas_clave_int = '11'");
                        $numu = mysqli_num_rows($conu);
                        if($numu>0)
                        {
                            $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario where usu_clave_int in (select usu_clave_int from asignacion_alerta where tas_clave_int = '11')");
                        }
                        else
                        {
                            $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario where usu_usuario in ('superadmin','marta.lopez','jorge.bermudez')");
                        }
                        $num = mysqli_num_rows($conema);
                        for($i = 0; $i < $num; $i++)
                        {
                            $mail = new PHPMailer();
                            
                            $dato1 = mysqli_fetch_array($conema);
                            $mail->Body = '';
                            $ema = '';
                            $nom = $dato1['usu_nombre'];
                            $ema = $dato1['usu_email'];
                            
                            $concaj = mysqli_query($conectar,"select caj_nombre from cajero where caj_clave_int = '".$caj."'");
                            $datocaj = mysqli_fetch_array($concaj);
                            $nomcaj = CORREGIRTEXTO($datocaj['caj_nombre']);
                            
                            $mail->From = "adminpavas@pavas.com.co";
                            $mail->FromName = "Soportica";
                            $mail->AddAddress($ema, "Destino");
                            $mail->Subject = "Cajero Nro. ".$caj." Ha pasado de estado a PPT";
                            
                            // Cuerpo del mensaje
                            $mail->Body .= "Hola ".$nom."!\n\n";    
                            $mail->Body .= "CONTROL CAJEROS le informa que este cajero ha pasado de estado a PPT.\n";
                            $mail->Body .= "CAJERO: ".$nomcaj."\n";
                            $mail->Body .= "DISENADOR: ".$disenador."\n";
                            $mail->Body .= date("d/m/Y H:m:s")."\n\n";
                            $mail->Body .= "Este mensaje es generado automáticamente por CONTROL CAJEROS, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Cajeros \n";
                            
                            if(!$mail->Send())
                            {
                                echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                                echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                            }
                            else
                            {
                                mysqli_query($conectar,"update cajero_diseno set cad_sw_envioestado = '".$est."' where caj_clave_int = '".$caj."'");
                            }
                        }
                    }
                    if((strtoupper($nomest) == strtoupper("entregado") or strtoupper($nomest) == strtoupper("entregados") or strtoupper($nomest) == strtoupper("entregada")) and ($swenvio == 0 or $swenvio == ''))
                    {
                        //alerta14

                        $conu = mysqli_query($conectar,"select usu_clave_int from asignacion_alerta where tas_clave_int = '12'");
                        $numu = mysqli_num_rows($conu);
                        if($numu>0)
                        {
                            $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario where usu_clave_int in (select usu_clave_int from asignacion_alerta where tas_clave_int = '12')");
                        }
                        else
                        {
                            $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario where usu_usuario in ('superadmin','marta.lopez')");
                        }
                        //$con = mysqli_query($conectar,"select usu_nombre,usu_email from usuario where usu_usuario in ('superadmin','marta.lopez')");
                        $num = mysqli_num_rows($conema);
                        for($i = 0; $i < $num; $i++)
                        {
                            $mail = new PHPMailer();
                            
                            $dato1 = mysqli_fetch_array($conema);
                            $mail->Body = '';
                            $ema = '';
                            $nom = $dato1['usu_nombre'];
                            $ema = $dato1['usu_email'];
                            
                            $concaj = mysqli_query($conectar,"select caj_nombre from cajero where caj_clave_int = '".$caj."'");
                            $datocaj = mysqli_fetch_array($concaj);
                            $nomcaj = CORREGIRTEXTO($datocaj['caj_nombre']);
                            
                            $mail->From = "adminpavas@pavas.com.co";
                            $mail->FromName = "Soportica";
                            $mail->AddAddress($ema, "Destino");
                            $mail->Subject = "Cajero Nro. ".$caj." Ha sido entregado por el disenador";
                            
                            // Cuerpo del mensaje
                            $mail->Body .= "Hola ".$nom."!\n\n";    
                            $mail->Body .= "CONTROL CAJEROS le informa que este cajero ha sido entregado por el disenador.\n";
                            $mail->Body .= "CAJERO: ".$nomcaj."\n";
                            $mail->Body .= "DISENADOR: ".$disenador."\n";
                            $mail->Body .= date("d/m/Y H:m:s")."\n\n";
                            $mail->Body .= "Este mensaje es generado automáticamente por CONTROL CAJEROS, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Cajeros \n";
                            
                            if(!$mail->Send())
                            {
                                echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                                echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                            }
                            else
                            {
                                mysqli_query($conectar,"update cajero_diseno set cad_sw_envio_entregado = 1 where caj_clave_int = '".$caj."'");
                            }
                        }
                    }
                    
                    $con = mysqli_query($conectar,"select caj_clave_int from cajero where caj_padre = ".$caj."");
                    $num = mysqli_num_rows($con);
                    for($i = 0; $i < $num; $i++)
                    {
                        $dato = mysqli_fetch_array($con);
                        $clahij = $dato['caj_clave_int'];
                        $con1 = mysqli_query($conectar,"update cajero_diseno set esd_clave_int = '".$est."', cad_fecha_entrega_info_diseno = '".$fecent."' where caj_clave_int = '".$clahij."'");

                        //DB->20150711: Valida si el cajero tiene alerta para identificar las fechas y actualizar la alerta
                        //validarAletas($caj, $fecent);
                    }
                    
                    mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'8',3,'".$caj."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 3=Actualización cajero
                }
                else
                {
                    echo "<div class='validaciones' style='width: 100%' align='center'>No se han podido guardar los datos, porfavor intentelo nuevamente</div>";
                }
            //}
            echo "<style onload=MOSTRARFECHAS('".$caj."')></style>";
            $deleteconsolidar = mysqli_query($conectar,"DELETE FROM cajero_consolidado WHERE caj_clave_int = '".$caj."'");
            $consolidarcajero = mysqli_query($conectar,"INSERT INTO cajero_consolidado (caj_clave_int ,  `caj_nombre` ,  `caj_direccion` ,`caj_ano_contable` ,`caj_region` ,`tpr_clave_int`, `caj_departamento` ,  `caj_municipio` , `tip_clave_int` ,`tii_clave_int` ,`mod_clave_int`,  `caj_codigo_cajero` ,  `cco_clave_int`,  `rem_clave_int` ,
      `ubi_clave_int` ,  `caj_codigo_suc` ,  `caj_ubicacion_atm` ,  `ati_clave_int`,  `caj_riesgo` ,  `are_clave_int` ,  `caj_fecha_inicio` ,  `caj_fecha_creacion` ,  `caj_estado_proyecto`,  `caj_sw_eliminado` ,  `caj_sw_padre`,  `caj_padre` ,  `caj_fec_apagado_atm` ,  `caj_sw_bancolombia`,  `caj_modalidad` ,  `caj_convenio`,  `cac_constructor`,  `cac_fecha_entrega_atm`,  `cac_porcentaje_avance`,  `cac_adjunto_avance`,  `cac_fecha_entrega_cotizacion`, `cac_fecha_entrega_liquidacion`,  `cac_fecha_inicio_constructor`,  `cad_req_diseno` ,  `cad_disenador` ,  `cad_fecha_inicio_diseno` ,  `esd_clave_int` ,  `cad_sw_envioestado` ,
      `cad_sw_envio_entregado`,  `cad_sw_aprob_esquema`,  `cad_sw_envio_esquema`,  `cad_sw_aprob_ppt`,  `cad_sw_envio_ppt` ,  `cad_sw_aprob_entregado` ,  `cad_sw_envio_aprobentregado` ,  `cad_fecha_entrega_info_diseno` , `cai_req_inmobiliaria`,  `cai_inmobiliaria`,  `cai_fecha_ini_inmobiliaria` ,  `esi_clave_int` ,  `cai_fecha_entrega_info_inmobiliaria` ,   `cin_interventor` ,  `cin_fecha_teorica_entrega` ,  `cin_fecha_inicio_obra`,  `cin_fecha_pedido_suministro`,
      `cin_sw_aprov_cotizacion` ,  `can_clave_int` ,  `lit_clave_int` ,  `cin_fecha_entrega_canal` , `cin_fecha_aprov_comite` ,  `cin_sw_aprov_liquidacion` ,  `cin_sw_img_nexos` ,  `cin_sw_fotos` ,  `cin_sw_actas`,  `cin_fecha_inicio_interventoria` ,  `cal_req_licencia` , `cal_gestionador` , `cal_fecha_inicio_licencia` ,  `esl_clave_int` ,  `cal_fecha_entrega_info_licencia`, `cal_curaduria` ,  `cal_radicado` ,  `cas_instalador`,  `cas_fecha_ingreso_obra_inst` ,  `cas_codigo_monitoreo` ,  `cas_fecha_inicio_seguridad` ,  `cav_req_visita_local` ,  `cav_visitante` ,  `cav_fecha_visita`,  `cav_fecha_entrega_informe` ,
      `cav_estado` ,  `caf_num_ot_inmobiliaria`,  `caf_valor_inmobiliaria` ,  `caf_sw_cotiz_liqui_inmobiliaria`,  `caf_num_ot_visita` ,  `caf_valor_visita`,  `caf_sw_cotiz_liqui_visita` ,  `caf_num_ot_diseno`,  `caf_valor_diseno` ,  `caf_sw_cotiz_liqui_diseno` ,  `caf_num_ot_licencia` ,
      `caf_valor_licencia`,  `caf_sw_cotiz_liqui_licencia` ,  `caf_num_ot_interventoria` ,  `caf_valor_interventoria`,  `caf_sw_cotiz_liqui_interventoria` , `caf_num_ot_constructor` ,  `caf_valor_constructor` ,  `caf_sw_cotiz_liqui_constructor` ,  `caf_num_ot_seguridad`,  `caf_valor_seguridad`, `caf_sw_cotiz_liqui_seguridad` ,  `bac_sw_aprobado` ,  `bac_fecha_inicio`,  `bac_fec_ini_teorica` ,  `bac_fec_ent_teorica` ,  `bac_fec_comite`, `bac_sw_contrato`,  `bac_sw_canal` ,  `bac_sw_diseno`,  `bac_sw_envio_contrato`,  `bac_sw_envio_canal` ,  `bac_usu_actualiz` ,  `bac_fec_actualiz` ,  `bco_sw_aprobado` ,  `bes_clave_int` ,  `bco_fec_ini_teorica` ,  `bco_fec_ent_teorica`,  `bco_fec_entrega`,  `bco_usu_actualiz`,  `bco_fec_actualiz`,  `bca_fec_ini_teorica`,  `bca_fec_ent_teorica`,  `bca_fec_inicio` ,  `bca_fec_entrega` ,  `bca_usu_actualiz`,  `bca_fec_actualiz`,   `bma_clave_int`,  `bpm_sw_aprobado` ,  `bpm_usu_actualiz` ,  `bpm_fec_actualiz` ,  `bpr_sw_aprobado`,  `bpr_fec_ini_teorica` ,  `bpr_fec_ent_teorica` ,  `bpr_fecha_entrega` ,
      `bpr_sw_envio_fecha` ,  `bpr_usu_actualiz`,  `bpr_fec_actualiz`,  `bpi_fec_ini_teorica`,  `bpi_fec_ent_teorica`,  `bpi_usu_actualiz`,  `bpi_fec_actualiz`,caj_ot,tit_clave_int) 
    SELECT c.caj_clave_int ,  `caj_nombre` ,  `caj_direccion` ,`caj_ano_contable` ,`caj_region` ,`tpr_clave_int`, `caj_departamento` ,  `caj_municipio` , `tip_clave_int` ,`tii_clave_int` ,`mod_clave_int`,  `caj_codigo_cajero` ,  `cco_clave_int`,  `rem_clave_int` ,
      `ubi_clave_int` ,  `caj_codigo_suc` ,  `caj_ubicacion_atm` ,  `ati_clave_int`,  `caj_riesgo` ,  `are_clave_int` ,  `caj_fecha_inicio` ,  `caj_fecha_creacion` ,  `caj_estado_proyecto`,  `caj_sw_eliminado` ,  `caj_sw_padre`,  `caj_padre` ,  `caj_fec_apagado_atm` ,  `caj_sw_bancolombia`,  `caj_modalidad` ,  `caj_convenio`,  `cac_constructor`,  `cac_fecha_entrega_atm`,  `cac_porcentaje_avance`,  `cac_adjunto_avance`,  `cac_fecha_entrega_cotizacion`, `cac_fecha_entrega_liquidacion`,  `cac_fecha_inicio_constructor`,  `cad_req_diseno` ,  `cad_disenador` ,  `cad_fecha_inicio_diseno` ,  `esd_clave_int` ,  `cad_sw_envioestado` ,
      `cad_sw_envio_entregado`,  `cad_sw_aprob_esquema`,  `cad_sw_envio_esquema`,  `cad_sw_aprob_ppt`,  `cad_sw_envio_ppt` ,  `cad_sw_aprob_entregado` ,  `cad_sw_envio_aprobentregado` ,  `cad_fecha_entrega_info_diseno` , `cai_req_inmobiliaria`,  `cai_inmobiliaria`,  `cai_fecha_ini_inmobiliaria` ,  `esi_clave_int` ,  `cai_fecha_entrega_info_inmobiliaria` ,   `cin_interventor` ,  `cin_fecha_teorica_entrega` ,  `cin_fecha_inicio_obra`,  `cin_fecha_pedido_suministro`,
      `cin_sw_aprov_cotizacion` ,  `can_clave_int` ,  `lit_clave_int` ,  `cin_fecha_entrega_canal` , `cin_fecha_aprov_comite` ,  `cin_sw_aprov_liquidacion` ,  `cin_sw_img_nexos` ,  `cin_sw_fotos` ,  `cin_sw_actas`,  `cin_fecha_inicio_interventoria` ,  `cal_req_licencia` , `cal_gestionador` , `cal_fecha_inicio_licencia` ,  `esl_clave_int` ,  `cal_fecha_entrega_info_licencia`, `cal_curaduria` ,  `cal_radicado` ,  `cas_instalador`,  `cas_fecha_ingreso_obra_inst` ,  `cas_codigo_monitoreo` ,  `cas_fecha_inicio_seguridad` ,  `cav_req_visita_local` ,  `cav_visitante` ,  `cav_fecha_visita`,  `cav_fecha_entrega_informe` ,
      `cav_estado` ,  `caf_num_ot_inmobiliaria`,  `caf_valor_inmobiliaria` ,  `caf_sw_cotiz_liqui_inmobiliaria`,  `caf_num_ot_visita` ,  `caf_valor_visita`,  `caf_sw_cotiz_liqui_visita` ,  `caf_num_ot_diseno`,  `caf_valor_diseno` ,  `caf_sw_cotiz_liqui_diseno` ,  `caf_num_ot_licencia` ,
      `caf_valor_licencia`,  `caf_sw_cotiz_liqui_licencia` ,  `caf_num_ot_interventoria` ,  `caf_valor_interventoria`,  `caf_sw_cotiz_liqui_interventoria` , `caf_num_ot_constructor` ,  `caf_valor_constructor` ,  `caf_sw_cotiz_liqui_constructor` ,  `caf_num_ot_seguridad`,  `caf_valor_seguridad`, `caf_sw_cotiz_liqui_seguridad` ,  `bac_sw_aprobado` ,  `bac_fecha_inicio`,  `bac_fec_ini_teorica` ,  `bac_fec_ent_teorica` ,  `bac_fec_comite`, `bac_sw_contrato`,  `bac_sw_canal` ,  `bac_sw_diseno`,  `bac_sw_envio_contrato`,  `bac_sw_envio_canal` ,  `bac_usu_actualiz` ,  `bac_fec_actualiz` ,  `bco_sw_aprobado` ,  `bes_clave_int` ,  `bco_fec_ini_teorica` ,  `bco_fec_ent_teorica`,  `bco_fec_entrega`,  `bco_usu_actualiz`,  `bco_fec_actualiz`,  `bca_fec_ini_teorica`,  `bca_fec_ent_teorica`,  `bca_fec_inicio` ,  `bca_fec_entrega` ,  `bca_usu_actualiz`,  `bca_fec_actualiz`,   `bma_clave_int`,  `bpm_sw_aprobado` ,  `bpm_usu_actualiz` ,  `bpm_fec_actualiz` ,  `bpr_sw_aprobado`,  `bpr_fec_ini_teorica` ,  `bpr_fec_ent_teorica` ,  `bpr_fecha_entrega` ,
      `bpr_sw_envio_fecha` ,  `bpr_usu_actualiz`,  `bpr_fec_actualiz`,  `bpi_fec_ini_teorica`,  `bpi_fec_ent_teorica`,  `bpi_usu_actualiz`,  `bpi_fec_actualiz`,caj_ot,tit_clave_int
    FROM cajero c LEFT OUTER JOIN cajero_inmobiliaria cajinm ON (   cajinm.caj_clave_int = c.caj_clave_int)
    LEFT OUTER JOIN cajero_visita cajvis ON (cajvis.caj_clave_int = c.caj_clave_int)
    LEFT OUTER JOIN cajero_diseno cajdis ON (cajdis.caj_clave_int = c.caj_clave_int)
    LEFT OUTER JOIN cajero_licencia cajlic ON (cajlic.caj_clave_int = c.caj_clave_int)
    LEFT OUTER JOIN cajero_interventoria cajint ON (cajint.caj_clave_int = c.caj_clave_int)
    LEFT OUTER JOIN cajero_constructor cajcons ON (cajcons.caj_clave_int = c.caj_clave_int)
    LEFT OUTER JOIN cajero_seguridad cajseg ON (cajseg.caj_clave_int = c.caj_clave_int)
    LEFT OUTER JOIN cajero_facturacion cajfac ON (cajfac.caj_clave_int = c.caj_clave_int)
    LEFT OUTER JOIN bancolombia_canal bcan ON (bcan.caj_clave_int = c.caj_clave_int)
    LEFT OUTER JOIN bancolombia_comite bcom ON (bcom.caj_clave_int = c.caj_clave_int)
    LEFT OUTER JOIN bancolombia_contrato bcon ON (bcon.caj_clave_int = c.caj_clave_int)
    LEFT OUTER JOIN bancolombia_pedidomaquina bped ON (bped.caj_clave_int = c.caj_clave_int)
    LEFT OUTER JOIN bancolombia_prefactibilidad bpre ON (bpre.caj_clave_int = c.caj_clave_int)
    LEFT OUTER JOIN bancolombia_proyeccioninstalacion bpro ON (bpro.caj_clave_int = c.caj_clave_int)
    WHERE c.caj_clave_int  ='".$caj."' GROUP BY c.caj_clave_int ORDER BY c.caj_nombre ASC");
            if($consolidarcajero>0){

            }else{
                echo "<div class='validaciones'>Se ha producido un error al actualizar el consolidado del cajeros.</div>";
            }
        }
}
?>