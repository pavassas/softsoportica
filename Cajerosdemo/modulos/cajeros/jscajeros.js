// JavaScript Document/* Formatting function for row details - modify as you need */

$(document).ready(function(e) {

		var selected = [];
		var est = $('#busestado');
	
		
		var table2 = $('#tbCajeros').DataTable( { 
		"dom": '<"top"i>rt<"bottom"flp><"clear">',      
        
		"ordering": true,
		"info": true,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[20,30,50,-1], [20,30,50,"Todos"]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "Anterior","next":"Siguiente"}
		},		
		"processing": true,
        "serverSide": true,
        "ajax": {
                    "url": "consultacajeros.php",
                    "data": {est:est},
				},
			"columns": [
			
			{ "data" : "Editar", "orderable": false },	
			{ "data" : "consec" , "className" : "dt-center"},
			{ "data" : "Codigocajero", "className" : "dt-center"},			
			{ "data" : "Nombre", "className" : "dt-left"},
			{ "data" : "hijo", "className" : "dt-left" },
			{ "data" : "Region", "className" : "dt-left" },
			{ "data" : "Clavetip", "className" : "dt-left" },
			{ "data" : "Clavetii", "className" : "dt-left" },
			{ "data" : "Estadoproyecto" , "className" : "dt-center"}				
		],
		"order": [[2, 'asc']]
    } );
     
	 $('#tbCajeros tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );
	
	   var table2 = $('#tbCajeros').DataTable();
 
    // Apply the search
    table2.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
    
	
	//detalle
	// Array to track the ids of the details displayed rows
    var detailRows = [];
 
    $('#tbCajeros tbody').on( 'click', 'tr td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table2.row( tr );
        var idx = $.inArray( tr.attr('id'), detailRows );
 
        if ( row.child.isShown() ) {
            tr.removeClass( 'shown' );
            row.child.hide();
 
            // Remove from the 'open' array
            detailRows.splice( idx, 1 );
        }
        else {
            tr.addClass( 'shown' );
            row.child( format( row.data() ) ).show();
 
            // Add to the 'open' array
            if ( idx === -1 ) {
                detailRows.push( tr.attr('id') );
            }
        }
    } );
 
    // On each draw, loop over the `detailRows` array and show any child rows
    table2.on( 'draw', function () {
        $.each( detailRows, function ( i, id ) {
            $('#'+id+' td.details-control').trigger( 'click' );
        } );
    } );
});

function format ( d ) {
 
	return '<div class="row"><div class="col-md-3"><strong>Usuario: </strong>'+d.Cliente+'<br/>'+
	'<strong>Direccion: </strong>'+d.Direccion+'&nbsp;&nbsp;'+
	'<strong>Telefono: </strong>'+d.Telefono+'<br/>'+
	'<strong>Descripción: </strong><br/>'+d.Descripcion+'<br/>'+
	'<strong>Creado Por: </strong>'+d.Usuario+'&nbsp;&nbsp;'+
	'<strong>Actualización: </strong>'+d.Fecha+d.Correo+d.Cotizacion+
	'</div>'+
	d.Soluciones+
	'</div>';
        
}


