// JavaScript Document/* Formatting function for row details - modify as you need */

function updateDataTableSelectAllCtrl(table){

    var $table             = table.table().node();
    var $chkbox_all        = $('tbody .sele', $table);
    var $chkbox_checked    = $('tbody .sele:checked', $table);
    var chkbox_select_all  = $('thead input[name="select_all"]', $table).get(0);

    // If none of the checkboxes are checked
    if($chkbox_checked.length === 0)
    {
        console.log("Por aca 1");
        chkbox_select_all.checked = false;
        if('indeterminate' in chkbox_select_all){
            chkbox_select_all.indeterminate = false;
        }

        // If all of the checkboxes are checked
    } else if ($chkbox_checked.length === $chkbox_all.length){

        console.log("Por aca 2");
        chkbox_select_all.checked = true;
        if('indeterminate' in chkbox_select_all){
            chkbox_select_all.indeterminate = false;
        }

        // If some of the checkboxes are checked
    } else {

        console.log("Por aca 3");
        chkbox_select_all.checked = true;
        if('indeterminate' in chkbox_select_all){
            chkbox_select_all.indeterminate = true;
        }
    }
    if($chkbox_checked.length>0)
    {
        $('#divnuevoestado').show(100);
    }
    else
    {
        $('#divnuevoestado').hide(100);
    }
    console.log("Checked:" + $chkbox_checked.length);
}
var selected = [];
var codselected = [];

$(document).ready(function(e) {

        var consec = form1.busconsecutivo.value;
        var nom = form1.busnombre.value;
        var anocon = form1.busanocontable.value;
        var cencos = form1.buscentrocostos.value;
        var cod = form1.buscodigo.value;
        var reg = form1.busregion.value;
        var mun = form1.busmunicipio.value;
        var tip = form1.bustipologia.value;
        var tipint = form1.bustipointervencion.value;
        var moda = form1.busmodalidad.value;
        var actor = form1.busactor.value;
        var est = form1.ocultoestado.value;
		
		var table = $('#tbCajeros').DataTable( {

            "dom": '<"top"f>rt<"bottom"lp><"clear">',
            "columnDefs": [{
                'targets': 0,
                'searchable': false,
                'orderable': false,
                'width': '1%',
                'className': 'dt-body-center',
                'render': function (data, type, full, meta) {
                    return '<input type="checkbox" class="sele" value="2">';
                }
            }],
            select: {
                style:    'multi',
                selector: 'td:first-child'
            },

            "ordering": true,
		    "info": true,
		    "autoWidth": true,
		    "pagingType": "simple_numbers",
		    "lengthMenu": [[20,30,50,-1], [20,30,50,"Todos"]],
		    "language": {
                "sProcessing": "Buscando datos..",
		    "lengthMenu": "Ver _MENU_ registros",
		    "zeroRecords": "No se encontraron datos",
		    "info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		    "infoEmpty": "No se encontraron datos",
		    "infoFiltered": "",
		    "paginate": {"previous": "Anterior","next":"Siguiente"}

		    },
            "searching":  false,
		    "processing": true,
            "serverSide": true,
            "ajax": {
                    "url": "busquedacajeros.php",
                    "data": {
                        consec:consec,nom:nom,anocon:anocon,cencos:cencos,cod:cod,reg:reg,mun:mun,tip:tip,tipint:tipint,moda:moda,est:est,actor:actor
                    },
                "type": "POST"
				},
			"columns": [
                {
                    //"class":          "select-checkbox",
                    "orderable":      false,
                    "data":           null,
                    "defaultContent": ""
                },
                { "data" : "Editar", "orderable": false },
			    { "data" : "Cajero", "orderable": true },
                { "data" : "Codigo" , "className" : "dt-left" },
                { "data" : "Nombre", "className" : "dt-left"},
                { "data" : "Hijo", "className" : "dt-left"},
                { "data" : "Region", "className" : "dt-left" },
                { "data" : "Tipologia" , "className" : "dt-left"},
                { "data" : "Intervencion" , "className" : "dt-left" },
                { "data" : "Estado" , "className" : "dt-left" }
            ],

		    "order": [[2, 'asc']],
            "rowCallback": function( row, data ) {
                if ( $.inArray(data.DT_RowId, selected) !== -1 )
                {
                    $(row).find('.sele').prop('checked', true);
                   // $(row).addClass('selected');

                }
                // $('td:eq(1)', row).css('background-color',data.Color);
           }
        } );

    $('#tbCajeros tbody').on('click','.sele', function (e) {
        // console.log("Hola");
        var $row = $(this).closest('tr');

        id = $row.attr('id');

        // Get row data
        //var data = table.row($row).data();

        // Get row ID
        //var rowId = data[0];

        // Determine whether row ID is in the list of selected row IDs
        var index = $.inArray(id, selected);

        // If checkbox is checked and row ID is not in list of selected row IDs
        if(this.checked && index === -1){
            selected.push(id);

            // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
        } else if (!this.checked && index !== -1){
            selected.splice(index, 1);
        }

        if(this.checked){
            //console.log(this.value);
            $row.addClass('selected');
        } else {

            $row.removeClass('selected');
        }

        // Update state of "Select all" control
        updateDataTableSelectAllCtrl(table);

        // Prevent click event from propagating to parent
        e.stopPropagation();
    } );

    $('#tbCajeros').on('click', 'tbody td, thead th:first-child', function(e){
        //$(this).parent().find('.sele').trigger('click');
    });

    $('thead input[name="select_all"]', table.table().container()).on('click', function(e){
        if(this.checked){
            $('#tbCajeros tbody .sele:not(:checked)').trigger('click');
        } else {
            $('#tbCajeros tbody .sele:checked').trigger('click');
        }

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle table draw event
    table.on('draw', function(){
        // Update state of "Select all" control
        updateDataTableSelectAllCtrl(table);
    });


   /* $('#tbCajeros tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
         OCULTARSCROLL();
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );
	*/
	   var table2 = $('#tbCajeros').DataTable();
	
	//detalle
	// Array to track the ids of the details displayed rows
    var detailRows = [];
    // On each draw, loop over the `detailRows` array and show any child rows
    table2.on( 'draw', function () {
        $.each( detailRows, function ( i, id ) {
            $('#'+id+' td.details-control').trigger( 'click' );
        } );
    } );
});

function format ( d ) {
 
	return '<div class="row"><div class="col-md-3"><strong>Usuario: </strong>'+d.Cliente+'<br/>'+
	'<strong>Direccion: </strong>'+d.Direccion+'&nbsp;&nbsp;'+
	'<strong>Telefono: </strong>'+d.Telefono+'<br/>'+
	'<strong>Descripción: </strong><br/>'+d.Descripcion+'<br/>'+
	'<strong>Creado Por: </strong>'+d.Usuario+'&nbsp;&nbsp;'+
	'<strong>Actualización: </strong>'+d.Fecha+d.Correo+d.Cotizacion+
	'</div>'+
	d.Soluciones+
	'</div>';
        
}


    function CAMBIARESTADOMASIVO()
    {
        var table = $('#tbCajeros').DataTable();
        var col = table.rows('.selected').data().length;
        // var count = table.rows( { selected: true } ).count();
        var selecte1 = $.map(selected,function(elemento,i) { return elemento.replace('row_',''); } );
        var esta = $('#ocultoestado').val();
        var estn = $('#estadonuevo').val();

        if(estn=="" || estn==null)
        {
            alert("Seleccionar el estado nuevo . Verificar");
        }
        else
        if(col<=0)
        {
            alert("No ha seleccionado ningun cajero a cambiar de estado. Verificar");
        }
        else
        {

            $("#divlistaerrores").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />");
             var selecte = selecte1.join('-');
            $.post('fnCajeros.php',{opcion:'MODIFICARESTADOMASIVO',cajeros:selecte,estn:estn,esta:esta},
            function(data)
            {
                var pos = data[0].positivos;
                var err = data[0].errores;
                var lis = data[0].listaerrores;
                var msn = "";
                if(err>0 && pos>0){
                    msn="<div class='ok'>Cambio de estado realizado correctamente. <strong>Surgieron los siguientes errores:</strong><br></div>";
                }
                else if(pos>0){
                    msn="<div class='ok'>Cambio de estado realizado correctamente.</div>";
                    setTimeout(function(){
                        REFRESCARACTIVOS();
                    },1000);
                }
                var table = $('#tbCajeros').DataTable();
                table.draw('full-hold');
                //table.draw(false);
                selected.length = 0;
                $('#divlistaerrores').html(msn + lis);
                //selected.length = 0;

            },"json");
        }
    }

function OCULTARMENSAJEMASIVO(){
    $("#divlistaerrores").html("");
}

