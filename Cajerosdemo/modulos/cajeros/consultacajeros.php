<?php 
session_start();
	error_reporting(0);
	include('../../data/Conexion.php');
	/*define('WP_MEMORY_LIMIT', '1000M');
ini_set('memory_limit', '2000M');
ini_set('max_execution_time', 9000);
ini_set('upload_max_filesize', '1000M');
ini_set('post_max_size', '1000M');*/

	
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_SESSION['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];
		
	
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");
	$anocont = date("Y");
	
	$con = mysqli_query($conectar,"select u.usu_clave_int,p.prf_clave_int,p.prf_descripcion,p.prf_sw_actualiza_info from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
	$dato = mysqli_fetch_array($con);
	$clausu = $dato['usu_clave_int'];
	$claprf = $dato['prf_clave_int'];
	$perfil = $dato['prf_descripcion'];
	$actualizainfo = $dato['prf_sw_actualiza_info'];
	
	$con = mysqli_query($conectar,"select per_metodo from permiso where prf_clave_int = '".$claprf."' and ven_clave_int = 4");
	$dato = mysqli_fetch_array($con);
	$metodo = $dato['per_metodo'];

$est = $_GET['est'];
$consec = $_GET['consec'];
	$nom = $_GET['nom'];
	$anocon = $_GET['anocon'];
	$cencos = $_GET['cencos'];
	$cod = $_GET['cod'];
	$reg = $_GET['reg'];
	$mun = $_GET['mun'];
	$tip = $_GET['tip'];
	$tipint = $_GET['tipint'];
	$moda = $_GET['moda'];
	$actor = $_GET['actor'];

// DB table to use
$table = 'cajero';
// Table's primary key
$primaryKey = 'c.caj_clave_int';

// Array of database columns which should be read and sent back to DataTables.
// The db parameter represents the column name in the database, while the dt
// parameter represents the DataTables column identifier - in this case object


// parameter names
$columns = array(
	array(
		'db' => 'c.caj_clave_int',
		'dt' => 'DT_RowId', 'field' => 'caj_clave_int',
		'formatter' => function( $d, $row ) {
			// Technically a DOM id cannot start with an integer, so we prefix
			// a string. This can also be useful if you have multiple tables
			// to ensure that the id is unique with a different prefix
			return 'row_'.$d;
		}
	),
	array(
		'db' => 'c.caj_clave_int',
		'dt' => 'UD_Id', 'field' => 'caj_clave_int',
		'formatter' => function( $d, $row ) {
			// Technically a DOM id cannot start with an integer, so we prefix
			// a string. This can also be useful if you have multiple tables
			// to ensure that the id is unique with a different prefix
			return $d;
		}
	),
	array( 'db' => 'c.caj_nombre', 'dt' => 'Nombre', 'field' => 'caj_nombre' ),
		
	array( 'db' => 'c.caj_direccion', 'dt' => 'Direccion', 'field' => 'caj_direccion' ),
	array( 'db' => 'c.caj_ano_contable', 'dt' => 'Contable', 'field' => 'caj_ano_contable' ),
	array( 'db' => 'c.caj_region', 'dt' => 'Region', 'field' => 'caj_region' ,'formatter' => function( $d, $row ) {
		global $conectar;
			$sql = mysqli_query($conectar,"select pog_nombre from posicion_geografica where pog_clave_int = '".$d."'");
				$datosql = mysqli_fetch_array($sql);
				$nomreg = $datosql['pog_nombre'];
			  return $nomreg; 
		}),
	array( 'db' => 'c.caj_departamento', 'dt' => 'Departamento', 'field' => 'caj_departamento' ),
	array( 'db' => 'c.caj_municipio', 'dt' => 'Municipio', 'field' => 'caj_municipio' ),
	array( 'db' => 'c.tip_clave_int', 'dt' => 'Clavetip', 'field' => 'tip_clave_int' ,'formatter' => function( $d, $row ) {
		global $conectar;
		$sql = mysqli_query($conectar,"select tip_nombre from tipologia where tip_clave_int = '".$d."'");
				$datosql = mysqli_fetch_array($sql);
				$nomtip = $datosql['tip_nombre'];
			  return $nomtip; 
		}),
	array( 'db' => 'c.tii_clave_int', 'dt' => 'Clavetii', 'field' => 'tii_clave_int' ,'formatter' => function( $d, $row ) {
		global $conectar;
		$sql = mysqli_query($conectar,"select tii_nombre from tipointervencion where tii_clave_int = '".$d."'");
				$datosql = mysqli_fetch_array($sql);
				$nomtii = $datosql['tii_nombre'];
			  return $nomtii; 
		}),
	array( 'db' => 'c.mod_clave_int', 'dt' => 'Clavemod', 'field' => 'mod_clave_int' ),
	array( 'db' => 'c.caj_codigo_cajero', 'dt' => 'Codigocajero', 'field' => 'caj_codigo_cajero' ),
	array( 'db' => 'c.cco_clave_int', 'dt' => 'Clavecco', 'field' => 'cco_clave_int' ),
	array( 'db' => 'c.rem_clave_int', 'dt' => 'Claverem', 'field' => 'rem_clave_int' ),
	array( 'db' => 'c.ubi_clave_int', 'dt' => 'Claveubi', 'field' => 'ubi_clave_int' ),
	array( 'db' => 'c.caj_codigo_suc', 'dt' => 'Codigosuc', 'field' => 'caj_codigo_suc' ),
	array( 'db' => 'c.caj_ubicacion_atm', 'dt' => 'Ubicacionatm', 'field' => 'caj_ubicacion_atm' ),
	array( 'db' => 'c.ati_clave_int', 'dt' => 'Claveati', 'field' => 'ati_clave_int' ),
	array( 'db' => 'c.caj_riesgo', 'dt' => 'Riesgo', 'field' => 'caj_riesgo' ),
	array( 'db' => 'c.are_clave_int', 'dt' => 'Claveint', 'field' => 'are_clave_int' ),
	array( 'db' => 'c.caj_fec_apagado_atm', 'dt' => 'Apagado', 'field' => 'caj_fec_apagado_atm' ),
	array( 'db' => 'cajinm.cai_req_inmobiliaria', 'dt' => 'Inmobiliaria', 'field' => 'cai_req_inmobiliaria' ),
	array( 'db' => 'cajinm.cai_inmobiliaria', 'dt' => 'Inmobiliariacai', 'field' => 'cai_inmobiliaria' ),
	array( 'db' => 'cajinm.cai_fecha_ini_inmobiliaria', 'dt' => 'Inmobiliariafecha', 'field' => 'cai_fecha_ini_inmobiliaria' ),
	array( 'db' => 'cajinm.esi_clave_int', 'dt' => 'Esiclaveint', 'field' => 'esi_clave_int' ),
	array( 'db' => 'cajinm.cai_fecha_entrega_info_inmobiliaria', 'dt' => 'Inmobiliariaentrega', 'field' => 'cai_fecha_entrega_info_inmobiliaria' ),
	array( 'db' => 'cajvis.cav_req_visita_local', 'dt' => 'Visitalocal', 'field' => 'cav_req_visita_local' ),
	array( 'db' => 'cajvis.cav_visitante', 'dt' => 'Visitante', 'field' => 'cav_visitante' ),
	array( 'db' => 'cajvis.cav_fecha_visita', 'dt' => 'Fechavisita', 'field' => 'cav_fecha_visita' ),
	array( 'db' => 'cajdis.cad_req_diseno', 'dt' => 'Reqdiseno', 'field' => 'cad_req_diseno' ),
	array( 'db' => 'cajdis.cad_disenador', 'dt' => 'Caddisenador', 'field' => 'cad_disenador' ),
	array( 'db' => 'cajdis.cad_fecha_inicio_diseno', 'dt' => 'Disenofechinicio', 'field' => 'cad_fecha_inicio_diseno' ),
	array( 'db' => 'cajdis.esd_clave_int', 'dt' => 'Claveesd', 'field' => 'esd_clave_int' ),
	array( 'db' => 'cajdis.cad_fecha_entrega_info_diseno', 'dt' => 'Disenofechentrega', 'field' => 'cad_fecha_entrega_info_diseno' ),
	array( 'db' => 'cajlic.cal_req_licencia', 'dt' => 'Licencia', 'field' => 'cal_req_licencia' ),
	array( 'db' => 'cajlic.cal_fecha_inicio_licencia', 'dt' => 'Iniciolicencia', 'field' => 'cal_fecha_inicio_licencia' ),
	array( 'db' => 'cajlic.esl_clave_int', 'dt' => 'Claveesl', 'field' => 'esl_clave_int' ),
	array( 'db' => 'cajlic.cal_fecha_entrega_info_licencia', 'dt' => 'Licenciaentrega', 'field' => 'cal_fecha_entrega_info_licencia' ),
	array( 'db' => 'cajint.cin_interventor', 'dt' => 'Interventor', 'field' => 'cin_interventor' ),
	array( 'db' => 'cajint.cin_fecha_teorica_entrega', 'dt' => 'Teoricaentrega', 'field' => 'cin_fecha_teorica_entrega' ),
	array( 'db' => 'cajint.cin_fecha_inicio_obra', 'dt' => 'Obrafechinicio', 'field' => 'cin_fecha_inicio_obra' ),
	array( 'db' => 'cajint.cin_fecha_pedido_suministro', 'dt' => 'Pedidosuministro', 'field' => 'cin_fecha_pedido_suministro' ),
	array( 'db' => 'cajint.cin_sw_aprov_cotizacion', 'dt' => 'Cotizacion', 'field' => 'cin_sw_aprov_cotizacion' ),
	array( 'db' => 'cajint.can_clave_int', 'dt' => 'Clavecan', 'field' => 'can_clave_int' ),
	array( 'db' => 'cajint.cin_sw_aprov_liquidacion', 'dt' => 'Liquidacion', 'field' => 'cin_sw_aprov_liquidacion' ),
	array( 'db' => 'cajint.cin_sw_img_nexos', 'dt' => 'Nexos', 'field' => 'cin_sw_img_nexos' ),
	array( 'db' => 'cajint.cin_sw_fotos', 'dt' => 'Fotos', 'field' => 'cin_sw_fotos' ),
	array( 'db' => 'cajint.cin_sw_actas', 'dt' => 'Actas', 'field' => 'cin_sw_actas' ),
	array( 'db' => 'c.caj_estado_proyecto', 'dt' => 'Estadoproyecto', 'field' => 'caj_estado_proyecto' ,'formatter' => function( $d, $row ) {
		if($d == 1){ return "ACTIVO"; }elseif($d == 2){ return "SUSPENDIDO"; }elseif($d == 3){ return "ENTREGADO"; }elseif($d == 4){ return "CANCELADO"; }elseif($d == 5){ return "PROGRAMADO"; }
			  
		}),
	array( 'db' => 'cajcons.cac_constructor', 'dt' => 'Constructor', 'field' => 'cac_constructor' ),
	array( 'db' => 'cajcons.cac_fecha_entrega_atm', 'dt' => 'Entregafecharm', 'field' => 'cac_fecha_entrega_atm' ),
	array( 'db' => 'cajcons.cac_porcentaje_avance', 'dt' => 'Porcentaje', 'field' => 'cac_porcentaje_avance' ),
	array( 'db' => 'cajcons.cac_fecha_entrega_cotizacion', 'dt' => 'Cotizacionfechentrega', 'field' => 'cac_fecha_entrega_cotizacion' ),
	array( 'db' => 'cajcons.cac_fecha_entrega_liquidacion', 'dt' => 'Liquidacionfechentrega', 'field' => 'cac_fecha_entrega_liquidacion' ),
	array( 'db' => 'cajseg.cas_instalador', 'dt' => 'Instalador', 'field' => 'cas_instalador' ),
	array( 'db' => 'cajseg.cas_fecha_ingreso_obra_inst', 'dt' => 'Obrafechingreso', 'field' => 'cas_fecha_ingreso_obra_inst' ),
	array( 'db' => 'cajseg.cas_codigo_monitoreo', 'dt' => 'Monitoreo', 'field' => 'cas_codigo_monitoreo' ),
	array( 'db' => 'c.caj_sw_padre', 'dt' => 'Padresw', 'field' => 'caj_sw_padre' ), 
	array( 'db' => 'c.caj_padre', 'dt' => 'hijo', 'field' => 'caj_padre', 'formatter' => function( $d, $row ) {
		
		global $conectar;
		if($row[57] == 1 and ($d == 0 or $d == ''))
			{
				$conhij = mysqli_query($conectar,"select caj_clave_int,caj_nombre from cajero where caj_padre = ".$row[0]."");
				$numhij = mysqli_num_rows($conhij);
			?>
				<div style="cursor:pointer" onmouseover="javascript: VentanaFlotante('<?php for($j = 0; $j < $numhij; $j++){ $datohij = mysqli_fetch_array($conhij); return $datohij['caj_clave_int']." - ".$datohij['caj_nombre']."<br>"; } ?>', 300, <?php return 35*$numhij; ?>)" onmouseout="javascript: quitarDiv();">Padre</div>
			<?php
			}
			else
			if($d > 0)
			{
				$conpad = mysqli_query($conectar,"select caj_clave_int,caj_nombre from cajero where caj_clave_int = ".$d."");
				$datopad = mysqli_fetch_array($conpad);
			?>
				<div style="cursor:pointer" onmouseover="javascript: VentanaFlotante('<?php return $datopad['caj_clave_int']." - ".$datopad['caj_nombre']."<br>"; ?>', 200, 35)" onmouseout="javascript: quitarDiv();">Hijo</div>
			<?php
			}
			else
			{
				return "<div>Ninguno</div>";
			}
			  
		}),//57
	array( 'db' => "c.caj_clave_int", 'dt' => 'consec', 'field' => 'caj_clave_int' ),
	array( 'db' => "c.caj_clave_int", 'dt' => 'Editar', 'field' => 'caj_clave_int' ,'formatter' => function( $d, $row ) {
			  return "<img src='../../img/editar.png' onclick=EDITAR('".$d."','0') alt='' height='25' width='25' style='cursor:pointer'>"; 
		})

		
);

$sql_details = array(
	'user' => 'usrpavas',
	'pass' => '9A12)WHFy$2p4v4s',
	'db'   => 'cajeros',
	'host' => '127.0.0.1'
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( '../../data/ssp.class.php' );
$whereAll = "";// customerid =".$customerid." AND date( orderdate ) >= '".$startdate."' AND date( orderdate ) <= '".$enddate."'";
$groupBy = ' c.caj_clave_int';
$with = '';
$joinQuery = "FROM  cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int)  inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) ";

$extraWhere = " (c.caj_clave_int = '".$consec."' OR '".$consec."' IS NULL OR '".$consec."' = '') and (caj_nombre LIKE REPLACE('%".$nom."%',' ','%') OR '".$nom."' IS NULL OR '".$nom."' = '') and (caj_ano_contable = '".$anocon."' OR '".$anocon."' IS NULL OR '".$anocon."' = '') and (cco_clave_int = '".$cencos."' OR '".$cencos."' IS NULL OR '".$cencos."' = '') and (caj_codigo_cajero LIKE REPLACE('%".$cod."%',' ','%') OR '".$cod."' IS NULL OR '".$cod."' = '') and (caj_region = '".$reg."' OR '".$reg."' IS NULL OR '".$reg."' = '') and (caj_municipio = '".$mun."' OR '".$mun."' IS NULL OR '".$mun."' = '') and (tip_clave_int = '".$tip."' OR '".$tip."' IS NULL OR '".$tip."' = '') and (tii_clave_int = '".$tipint."' OR '".$tipint."' IS NULL OR '".$tipint."' = '') and (mod_clave_int = '".$moda."' OR '".$moda."' IS NULL OR '".$moda."' = '') and (caj_estado_proyecto = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') and (cai_inmobiliaria = '".$actor."' or cav_visitante = '".$actor."' or cad_disenador = '".$actor."' or cal_gestionador = '".$actor."' or cin_interventor = '".$actor."' or cac_constructor = '".$actor."' or cas_instalador = '".$actor."' or '".$actor."' IS NULL or '".$actor."' = '') and c.caj_sw_eliminado = 0";
		//and (WEEK(CURDATE()) = WEEK(caj.tic_fecha_cumplimiento) or WEEK(caj.tic_fecha_cumplimiento) < WEEK(CURDATE()))

echo  json_encode(
	SSP::simple( $_GET,$sql_details,$table,$primaryKey,$columns,$joinQuery,$extraWhere,$groupBy,$with )

	);

