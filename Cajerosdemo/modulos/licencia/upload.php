<?php
include('../../data/Conexion.php');
require_once('../../Classes/PHPMailer-master/class.phpmailer.php');
header("Cache-Control: no-store, no-cache, must-revalidate");
session_start();
date_default_timezone_set('America/Bogota');

// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_SESSION['usuario'];
$idUsuario= $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];

$fecha=date("Ymd");
$fechaact=date("Y/m/d H:i:s");
$clacaj = $_POST['clacaj'];
$actor = $_POST['actor'];
$arc = $_POST['archivo'];
$claada = $_POST['claada'];
$max=8000000;//8MB
function CORREGIRTEXTO($string)
{
    $string = str_replace("REEMPLAZARNUMERAL", "#", $string);
    $string = str_replace("REEMPLAZARMAS", "+",$string);
    $string = str_replace("REEMPLAZARMENOS", "-", $string);
    return $string;
}

if($clacaj != '' and $clacaj != 0 and $actor != '')
{
	if(is_array($_FILES)) 
	{
		mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'19',5,'".$clacaj."','".$usuario."','".$fechaact."')");//Tercer campo tia_clave_int. 5=Adjuntar archivo
		
		$con = mysqli_query($conectar,"select MAX(ada_clave_int) max from adjunto_actor where caj_clave_int = '".$clacaj."'");
		$dato = mysqli_fetch_array($con);
		$num = $dato['max'];
		
		if($num == 0 || $num == '')
		{
			$num = 1;
		}
		else
		{
			$num = $num+1;
		}
		
		if($actor == 'INFORME')
		{
			if(is_uploaded_file($_FILES['adjuntoinf']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntoinf']['tmp_name'];
				$archivo = basename($_FILES['adjuntoinf']['name']);
						
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
				$prefijo = "LICENCIA - INFORME";
				$destino =  "../../adjuntos/licencia/".$clacaj."-".$prefijo."".$num.".".$extension;
                $filesize = $_FILES['adjuntoinf']['size'];
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and caj_clave_int = '".$clacaj."' and tad_clave_int = '11' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "error4";
                }
                else
                if($filesize>$max)
                {
                    echo "error3";
                }
                else
                if(strtoupper($extension)=="DWG" || strtoupper($extension)=="RAR" || strtoupper($extension)=="ZIP")
                {
                    echo "error1";
                }
                else
                {
                    if(move_uploaded_file($sourcePath,$destino))
                    {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,caj_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'".$clacaj."','".$fechaact."','".$destino."','".$archivo."',11,'".$usuario."','".$fechaact."')");



                        $concaj = mysqli_query($conectar,"select caj_nombre from cajero where caj_clave_int = '".$clacaj."'");
                        $datocaj = mysqli_fetch_array($concaj);
                        $nomcaj = CORREGIRTEXTO($datocaj['caj_nombre']);

                        $conact = mysqli_query($conectar,"select usu_nombre from usuario where usu_usuario = '".$usuario."'");
                        $datoact = mysqli_fetch_array($conact);
                        $nomact = $datoact['usu_nombre'];

                        //alerta18
                        //Envio correo de alerta a Yurany incapie cuando el actor constructor adjunte informe final
                        $conu = mysqli_query($conectar,"select usu_clave_int from asignacion_alerta where tas_clave_int = '16'");
                        $numu = mysqli_num_rows($conu);
                        if($numu>0)
                        {
                            $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario where usu_clave_int in (select usu_clave_int from asignacion_alerta where tas_clave_int = '16')");
                        }
                        else
                        {
                            $conema = mysqli_query($conectar,"select usu_nombre,usu_email from usuario where usu_usuario in('yurany.hincapie','juanpablo.mejia')");
                        }


                        //$datoema = mysqli_fetch_array($conema);
                        //$nom = $datoema['usu_nombre'];
                        //$ema = $datoema['usu_email'];
                        $num = mysqli_num_rows($conema);
                        for($i = 0; $i < $num; $i++)
                        {
                            $datoema = mysqli_fetch_array($conema);
                            $nom = $datoema['usu_nombre'];
                            $ema = $datoema['usu_email'];
                            $mail = new PHPMailer();

                            $mail->From = "adminpavas@pavas.com.co";
                            $mail->FromName = "Soportica";
                            $mail->AddAddress($ema, "Destino");
                            $mail->Subject = "Alerta Adjunto Informe LICENCIA. Cajero Nro. ".$clacaj;

                            // Cuerpo del mensaje
                            $mail->Body .= "Hola ".$nom."!\n\n";
                            $mail->Body .= "CONTROL CAJEROS registra que el actor licencia ha adjuntado el informe.\n";
                            $mail->Body .= "CAJERO: ".$nomcaj."\n";
                            $mail->Body .= "GESTIONADOR: ".$nomact."\n";
                            $mail->Body .= date("d/m/Y H:m:s")."\n\n";
                            $mail->Body .= "Este mensaje es generado automáticamente por CONTROL CAJEROS, por favor no responda a este correo, cualquier duda adicional puede resolverla ingresando a nuestro sitio www.pavas.com.co/Cajeros \n";

                            if(!$mail->Send())
                            {
                                //echo "<div class='validaciones'>Se ha producido un error al enviar el correo.</div>";
                                //echo "<div class='validaciones'>Mailer Error: " . $mail->ErrorInfo."</div>";
                            }
                            else
                            {
                                //mysqli_query($conectar,"update alerta set ale_sw_enviado = 1 where ale_clave_int = '".$a."'");
                            }
                        }
                    }
                    else
                    {
                        echo "error2";
                    }
                }
			}
		}
		else
		if($actor == 'FACTURACION')
		{
			if(is_uploaded_file($_FILES['adjuntofac']['tmp_name'])) 
			{
				$sourcePath = $_FILES['adjuntofac']['tmp_name'];
				$archivo = basename($_FILES['adjuntofac']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				/*if($extension == 'jpg')
				{
					$extension = 'png';
				}*/
				$prefijo = "LICENCIA - FACTURACION";
				$destino =  "../../adjuntos/licencia/".$clacaj."-".$prefijo."".$num.".".$extension;
                $filesize = $_FILES['adjuntofac']['size'];
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and caj_clave_int = '".$clacaj."' and tad_clave_int = '12' and ada_sw_eliminado = 0");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "error4";
                }
                else
                if($filesize>$max)
                {
                    echo "error3";
                }
                else
                if(strtoupper($extension)=="DWG" || strtoupper($extension)=="RAR" || strtoupper($extension)=="ZIP")
                {
                    echo "error1";
                }
                else
                {
                    if(move_uploaded_file($sourcePath,$destino))
                    {
                        $sql = mysqli_query($conectar,"insert into adjunto_actor(ada_clave_int,caj_clave_int,ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_usu_actualiz,ada_fec_actualiz) values(null,'".$clacaj."','".$fechaact."','".$destino."','".$archivo."',12,'".$usuario."','".$fechaact."')");
                    }
                    else
                    {
                        echo "error2";
                    }
                }
			}
		}
		else
		if($actor == 'ACTUALIZAARCHIVO')
		{
			if(is_uploaded_file($_FILES['archivoadjunto']['tmp_name'])) 
			{
				$sourcePath = $_FILES['archivoadjunto']['tmp_name'];
				$archivo = basename($_FILES['archivoadjunto']['name']);
				
				$array_nombre = explode('.',$archivo);
				$cuenta_arr_nombre = count($array_nombre);
				$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
				$con = mysqli_query($conectar,"select * from adjunto_actor where ada_clave_int = '".$claada."'");
				$dato = mysqli_fetch_array($con);
				$adj = $dato['ada_adjunto'];
				
				if($arc == 11)
				{
					$prefijo = "LICENCIA - INFORME";
					$destino =  "../../adjuntos/visita/".$clacaj."-".$prefijo."".$claada.".".$extension;
				}
				if($arc == 12)
				{
					$prefijo = "LICENCIA - FACTURACION";
					$destino =  "../../adjuntos/visita/".$clacaj."-".$prefijo."".$claada.".".$extension;
				}
                $filesize = $_FILES['archivoadjunto']['size'];
                $veri = mysqli_query($conectar, "select * from adjunto_actor where UPPER(ada_nombre_adjunto) = UPPER('".$archivo."') and caj_clave_int = '".$clacaj."' and tad_clave_int = '".$arc."' and ada_sw_eliminado = 0 and ada_clave_int!='".$claada."'");
                $numv = mysqli_num_rows($veri);
                if($numv>0){
                    echo "error4";
                }
                else
                if($filesize>$max)
                {
                    echo "error3";
                }
                else
                if(strtoupper($extension)=="DWG" || strtoupper($extension)=="RAR" || strtoupper($extension)=="ZIP")
                {
                    echo "error1";
                }
                else
                {
                    if(move_uploaded_file($sourcePath,$destino))
                    {
                        $sql = mysqli_query($conectar,"update adjunto_actor set ada_usu_actualiz = '".$usuario."',ada_fec_actualiz = '".$fechaact."', ada_adjunto = '".$destino."', ada_nombre_adjunto = '".$archivo."' where ada_clave_int = '".$claada."'");
                    }
                    else
                    {
                        echo "error2";
                    }
                }
			}
		}
	}
	/*
	$conhij = mysqli_query($conectar,"select * from cajero where caj_padre = ".$clacaj."");
	$numhij = mysqli_num_rows($conhij);
	for($i = 0; $i < $numhij; $i++)
	{
		$datohij = mysqli_fetch_array($conhij);
		$clahij = $datohij['caj_clave_int'];
		
		mysqli_query($conectar,"delete from adjunto_actor where caj_clave_int = ".$clahij."");
		mysqli_query($conectar,"insert into adjunto_actor select null,".$clahij.",ada_fecha_creacion,ada_adjunto,ada_nombre_adjunto,tad_clave_int,ada_sw_eliminado,ada_usu_actualiz,ada_fec_actualiz from adjunto_actor where caj_clave_int = ".$clacaj."");
	}*/
}
?>