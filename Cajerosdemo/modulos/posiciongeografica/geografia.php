<?php
	include('../../data/Conexion.php');
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_SESSION['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];
		
	date_default_timezone_set('America/Bogota');    
    // verifica si no se ha loggeado
    if(isset($_SESSION['nw']))
    {
        if($_SESSION['nw']<time())
        {
            unset($_SESSION['nw']);
            //echo "<script>alert('Tiempo agotado - Loguearse nuevamente');</script>";
            header("LOCATION:../../data/logout.php");
        }
        else
        {
            $_SESSION['nw'] = time() + (60 * 60);
        }
    }
    else
    {
        //echo "<script>alert('Tiempo agotado - Loguearse nuevamente');</script>";
        header("LOCATION:../../data/logout.php");
    }
	$fecha=date("Y/m/d H:i:s");
	
	$con = mysqli_query($conectar,"select u.usu_clave_int,p.prf_clave_int,p.prf_descripcion from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
	$dato = mysqli_fetch_array($con);
	$claprf = $dato['prf_clave_int'];
	
	$con = mysqli_query($conectar,"select per_metodo from permiso where prf_clave_int = '".$claprf."' and ven_clave_int = 5");
	$dato = mysqli_fetch_array($con);
	$metodo = $dato['per_metodo'];
	
	if($_GET['editaract'] == 'si')
	{
		$actedi = $_GET['actedi'];
		$con = mysqli_query($conectar,"select * from posicion_geografica where pog_clave_int = '".$actedi."'"); 
		$dato = mysqli_fetch_array($con); 
		$nom = $dato['pog_nombre'];
		$act = $dato['pog_sw_activo'];
?>
		<table style="width: 38%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Nombre Posición:</td>
			<td class="auto-style3"><input class="inputs" name="nombre1" maxlength="50" value="<?php echo $nom; ?>" type="text" style="width: 200px" />
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3" colspan="2">Activa:<input class="inputs" <?php if($act == 1){ echo 'checked="checked"'; } ?> name="activo1" type="checkbox" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4">
			<input name="submit" type="button" value="Guardar" onclick="GUARDAR('POSICION','<?php echo $actedi; ?>')"  style="width: 348px; height: 25px; cursor:pointer" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">
			<div id="datos">
			</div>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
<?php
		exit();
	}
	
	if($_GET['guardaract'] == 'si')
	{
		sleep(1);
		$nom = $_GET['nom'];
		$act = $_GET['act'];
		$lu = $_GET['la'];
		$a = $_GET['a'];
		
		$sql = mysqli_query($conectar,"select * from posicion_geografica where (UPPER(pog_nombre) = UPPER('".$nom."')) AND pog_clave_int <> '".$a."'");
		$dato = mysqli_fetch_array($sql);
		$conact = $dato['pog_nombre'];
		
		if($nom == '')
		{
			echo "<div class='validaciones'>Debe ingresar el nombre de la Posición Geográfica</div>";
		}
		else
		if(STRTOUPPER($conact) == STRTOUPPER($nom))
		{
			echo "<div class='validaciones'>La Posición Geográfica ingresada ya existe</div>";
		}
		else
		if($lu < 6)
		{
			echo "<div class='validaciones'>La Posición Geográfica debe ser mí­nimo de 6 dijitos $act</div>";
		}
		else
		{			
			if($act == 'false')
			{
				$swact = 0;
			}
			else
			if($act == 'true')
			{
				$swact = 1;
			}
			
			$con = mysqli_query($conectar,"update posicion_geografica set pog_nombre = '".$nom."', pog_sw_activo = '".$swact."', pog_usu_actualiz = '".$usuario."', pog_fec_actualiz = '".$fecha."' where pog_clave_int = '".$a."'");
			
			if($con >= 1)
			{
				echo "<div class='ok'>Datos grabados correctamente</div>";
				mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'5',26,'".$cla."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 26=Actualización posición geográfica
			}
			else
			{
				echo "<div class='validaciones'>No se han podido guardar los datos</div>";
			}	
		}
		exit();
	}
	
	if($_GET['guardarnivel1'] == 'si')
	{
		sleep(1);
		$niv1 = $_GET['niv1'];
		$fecha=date("Y/m/d H:i:s");
		$sql = mysqli_query($conectar,"select * from posicion_geografica where (UPPER(pog_nombre) = UPPER('".$niv1."'))");
		$dato = mysqli_fetch_array($sql);
		$conact = $dato['pog_nombre'];
		
		if($niv1 == '')
		{
			echo "<div class='validaciones'>Debe ingresar la Región</div>";
		}
		else
		if(STRTOUPPER($conact) == STRTOUPPER($niv1))
		{
			echo "<div class='validaciones'>La Región ingresado ya existe</div>";
		}
		else
		{			
			$con = mysqli_query($conectar,"insert into posicion_geografica (pog_nombre,pog_usu_actualiz,pog_fec_actualiz) values('".$niv1."','".$usuario."','".$fecha."')");				
			
			if($con >= 1)
			{
				echo "<div class='ok'>Datos grabados correctamente</div>";
				$con = mysqli_query($conectar,"select max(pog_clave_int) max from posicion_geografica");
				$dato = mysqli_fetch_array($con);
				$max = $dato['max'];
				mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'5',25,'".$max."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 25=Creación posición geográfica
			}
			else
			{
				echo "<div class='validaciones'>No se han podido guardar los datos</div>";
			}	
		}
		exit();
	}
	
	if($_GET['guardarnivel2'] == 'si')
	{
		sleep(1);
		$niv1 = $_GET['niv1'];
		$niv2 = $_GET['niv2'];
		$fecha=date("Y/m/d H:i:s");

		if($niv1 == '')
		{
			echo "<div class='validaciones'>Debe ingresar la Región</div>";
		}
		else
		if($niv2 == '')
		{
			echo "<div class='validaciones'>Debe ingresar el Departamento</div>";
		}
		else
		{			
			$con = mysqli_query($conectar,"insert into posicion_geografica (pog_nombre,pog_hijo,pog_usu_actualiz,pog_fec_actualiz) values('".$niv2."','".$niv1."','".$usuario."','".$fecha."')");				
			
			if($con >= 1)
			{
				echo "<div class='ok'>Datos grabados correctamente</div>";
				$con = mysqli_query($conectar,"select max(pog_clave_int) max from posicion_geografica");
				$dato = mysqli_fetch_array($con);
				$max = $dato['max'];
				mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'5',25,'".$max."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 25=Creación posición geográfica
			}
			else
			{
				echo "<div class='validaciones'>No se han podido guardar los datos</div>";
			}	
		}
		exit();
	}
	
	if($_GET['guardarnivel3'] == 'si')
	{
		sleep(1);
		$niv1 = $_GET['niv1'];
		$niv2 = $_GET['niv2'];
		$niv3 = $_GET['niv3'];
		$fecha=date("Y/m/d H:i:s");
		
		if($niv1 == '')
		{
			echo "<div class='validaciones'>Debe ingresar la Región</div>";
		}
		else
		if($niv2 == '')
		{
			echo "<div class='validaciones'>Debe ingresar el Departamento</div>";
		}
		else
		if($niv3 == '')
		{
			echo "<div class='validaciones'>Debe ingresar el Municipio o Ciudadddd</div>";
		}
		else
		{			
			$con = mysqli_query($conectar,"insert into posicion_geografica (pog_nombre,pog_nieto,pog_usu_actualiz,pog_fec_actualiz) values('".$niv3."','".$niv2."','".$usuario."','".$fecha."')");				
			
			if($con >= 1)
			{
				echo "<div class='ok'>Datos grabados correctamente</div>";
				$con = mysqli_query($conectar,"select max(pog_clave_int) max from posicion_geografica");
				$dato = mysqli_fetch_array($con);
				$max = $dato['max'];
				mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'5',25,'".$max."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 25=Creación posición geográfica
			}
			else
			{
				echo "<div class='validaciones'>No se han podido guardar los datos</div>";
			}	
		}
		exit();
	}
	
	if($_GET['nuevaposicion'] == 'si')
	{
		sleep(1);
		$nom = $_GET['nom'];
		$act = $_GET['act'];
		$la = $_GET['la'];
		$fecha=date("Y/m/d H:i:s");
		
		$sql = mysqli_query($conectar,"select * from posicion_geografica where (UPPER(pog_nombre) = UPPER('".$nom."'))");
		$dato = mysqli_fetch_array($sql);
		$conact = $dato['pog_nombre'];
		
		if($nom == '')
		{
			echo "<div class='validaciones'>Debe ingresar el Nombre</div>";
		}
		else
		if(STRTOUPPER($conact) == STRTOUPPER($nom))
		{
			echo "<div class='validaciones'>La Posición Geográfica ingresada ya existe</div>";
		}
		else
		if($la < 6)
		{
			echo "<div class='validaciones'>La Posición Geográfica debe ser mí­nimo de 6 dijitos</div>";
		}
		else
		{
			if($act == 'false')
			{
				$swact = 0;
			}
			else
			if($act == 'true')
			{
				$swact = 1;
			}
			$con = mysqli_query($conectar,"insert into posicion_geografica (pog_nombre,pog_sw_activo,pog_usu_actualiz,pog_fec_actualiz) values('".$nom."','".$swact."','".$usuario."','".$fecha."')");				
			
			if($con >= 1)
			{
				echo "<div class='ok'>Datos grabados correctamente</div>";
				$con = mysqli_query($conectar,"select max(pog_clave_int) max from posicion_geografica");
				$dato = mysqli_fetch_array($con);
				$max = $dato['max'];
				mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'5',25,'".$max."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 25=Creación posición geográfica
			}
			else
			{
				echo "<div class='validaciones'>No se han podido guardar los datos</div>";
			}
		}
		exit();
	}
	if($_GET['todos'] == 'si')
	{
		sleep(1);
		$rows=mysqli_query($conectar,"select * from posicion_geografica where pog_hijo is null and pog_nieto is null");
		$total=mysqli_num_rows($rows);
?>
		<table id="table1" class="controller" style="width: 60%">
	    <tr data-level="header" class="header">
			<td class="auto-style3" style="width: 3px"><input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<?php echo $total;?>,this);" class="auto-style6" /></td>
			<td class="auto-style3">
			<?php
			if($metodo == 1)
			{
			?>
			<table style="width: 20%; text-align:left" align="left">
				<tr>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/activo.png" alt="" class="auto-style6" /><input type="submit" value="Activar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/inactivo.png" alt="" class="auto-style6" /><input type="submit" value="Inactivar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1">&nbsp;</td>
				</tr>
			</table>
			<?php
			}
			?>
			</td>
			<td class="auto-style3" style="width: 50px">&nbsp;</td>
			<td class="auto-style3">&nbsp;</td></tr>
	    <?php
			$contador=0;
			$con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo is null and pog_nieto is null order by pog_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clan1 = $dato['pog_clave_int'];
				$nom = $dato['pog_nombre'];
				$act = $dato['pog_sw_activo'];
				$usuact = $dato['pog_usu_actualiz'];
				$fecact = $dato['pog_fec_actualiz'];
				$contador=$contador+1;
		?>
			    <tr data-level="1" id="<?php echo $clan1; ?>">
					<td class="auto-style3" colspan="3">
					<div class="inputs" style="height:100%; width:1210px; overflow-x:hidden;resize:both" align="left">
					<div class="wfm">
					    <ul class="auto-style10" style="width: 200%">
					    	<?php
								$contador=0;
								$con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo is null and pog_nieto is null order by pog_nombre");
								$num = mysqli_num_rows($con);
								for($i = 0; $i < $num; $i++)
								{
									$dato = mysqli_fetch_array($con);
									$clan1 = $dato['pog_clave_int'];
									$nom = $dato['pog_nombre'];
									$act = $dato['pog_sw_activo'];
									$usuact = $dato['pog_usu_actualiz'];
									$fecact = $dato['pog_fec_actualiz'];
									$contador=$contador+1;
							?>
					        <li>
							<div style="float:left;height:50px">
								<?php
									if($i == 0)
									{
										echo "<br>";
									}
								?>
								<img alt="" class="expand" src="../../images/Minus.png" />
								<img alt="" class="collapse" onclick="DEPARTAMENTOSCIUDADES('<?php echo $clan1; ?>')" src="../../images/Plus.png" />
							</div>
							<div style="float:left; width:95%">
								<table>
									<?php
									if($i == 0)
									{
									?>
									<tr>
										<td class="auto-style3" style="width: 21px">&nbsp;
										</td>
										<td class="auto-style3"><strong>Región</strong></td>
										<td class="auto-style3"><strong>Usuario</strong></td>
										<td class="auto-style3"><strong>Actualización</strong></td>
										<td class="auto-style3" style="width: 50px"><strong>Activo</strong></td>
										<td class="auto-style3" style="width: 50px">&nbsp;
										</td>
									</tr>
									<?php
									}
									?>
									<tr>
										<td class="auto-style21" style="width: 21px">
										<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<?php echo $contador;?>" value="<?php echo $dato['pog_clave_int'];?>" class="auto-style6" />
										</td>
										<td class="auto-style21" style="width: 150px"><?php echo $nom; ?></td>
										<td class="auto-style21" style="width: 200px"><?php echo $usuact; ?></td>
										<td class="auto-style21" style="width: 200px"><?php echo $fecact; ?></td>
										<td class="auto-style21" style="width: 200px"><input name="activarinactivar" id="activarinactivar" type="checkbox" <?php if($act == 1){ echo 'checked="checked"'; } ?> disabled="disabled" class="auto-style6" ></td>
										<td class="auto-style21" style="width: 200px">
										<?php
										if($metodo == 1)
										{
										?>
										<a data-reveal-id="nuevaposicion" data-animation="fade" style="cursor:pointer" onclick="EDITARNIVEL('NIVEL1','<?php echo $dato['pog_clave_int']; ?>')"><img src="../../img/editar.png" alt="" height="22" width="21" /></a>
										<?php
										}
										?>
										</td>
									</tr>
								</table>
							</div>
							<div>
					        </div>
							<ul style="display:none">
					            <div id="depciu<?php echo $clan1; ?>">
											            
								</div>
					            </ul>
					        </li>
					        <?php
								}
							?>
					    </ul>
					</div>
					</div>
					</td>
					<td class="auto-style3">&nbsp;</td></tr>
			<?php
				$con1 = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo = '".$clan1."' order by pog_nombre");
				$num1 = mysqli_num_rows($con1);
				for($j = 0; $j < $num1; $j++)
				{
					$dato1 = mysqli_fetch_array($con1);
					$clan2 = $dato1['pog_clave_int'];
					$usuact = $dato1['pog_usu_actualiz'];
					$fecact = $dato1['pog_fec_actualiz'];
					$nom1 = $dato1['pog_nombre'];
			?>
			    	<?php
						$con2 = mysqli_query($conectar,"select * from posicion_geografica where pog_nieto = '".$clan2."' order by pog_nombre");
						$num2 = mysqli_num_rows($con2);
						for($k = 0; $k < $num2; $k++)
						{
							$dato2 = mysqli_fetch_array($con2);
							$clan3 = $dato2['pog_clave_int'];
							$nom2 = $dato2['pog_nombre'];
							$usuact = $dato2['pog_usu_actualiz'];
							$fecact = $dato2['pog_fec_actualiz'];
					?>
				<?php
					}
				?>
	    	<?php
				}
			?>
	    <?php
			}
		?>
	    
	</table>
<?php
		exit();
	}
?>
<?php
	if($_GET['buscarpos'] == 'si')
	{
		$pad = $_GET['pad'];
		$hij = $_GET['hij'];
		$nie = $_GET['nie'];
		$act = $_GET['act'];
		
		$rows=mysqli_query($conectar,"select * from posicion_geografica where ((pog_nombre LIKE REPLACE('".$pad."%',' ','%') OR '".$pad."' IS NULL OR '".$pad."' = '' and pog_hijo is null and pog_nieto is null) or (pog_nombre LIKE REPLACE('".$hij."%',' ','%') OR '".$hij."' IS NULL OR '".$hij."' = '' and pog_hijo is not null and pog_nieto is null) or (pog_nombre LIKE REPLACE('".$nie."%',' ','%') OR '".$nie."' IS NULL OR '".$nie."' = '' and pog_hijo is null and pog_nieto is not null)) and (pog_sw_activo = '".$act."' OR '".$act."' IS NULL OR '".$act."' = '')");
		$total=mysqli_num_rows($rows);
?>
		<table id="table1" class="controller" style="width: 60%">
	    <tr data-level="header" class="header">
			<td class="auto-style3" style="width: 3px"><input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<?php echo $total;?>,this);" class="auto-style6" /></td>
			<td class="auto-style3">
			<?php
			if($metodo == 1)
			{
			?>
			<table style="width: 20%" align="left">
				<tr>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/activo.png" alt="" class="auto-style6" /><input type="submit" value="Activar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/inactivo.png" alt="" class="auto-style6" /><input type="submit" value="Inactivar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1">&nbsp;</td>
				</tr>
			</table>
			<?php
			}
			?>
			</td>
			<td class="auto-style3" style="width: 50px">&nbsp;</td>
			<td class="auto-style3">&nbsp;</td></tr>
			    <tr data-level="1" id="<?php echo $clan1; ?>">
					<td class="auto-style3" colspan="3">
					<div class="inputs" style="height:100%; width:1210px; overflow-x:hidden;resize:both" align="left">
					<div class="wfm">
					    <ul class="auto-style10" style="width: 200%">
					    	<?php
								$contador=0;
								$nie = mysqli_query($conectar,"SELECT pog_hijo from posicion_geografica WHERE pog_clave_int IN (SELECT pog_nieto from posicion_geografica WHERE (pog_nombre LIKE REPLACE(  'Nieto2%',  ' ',  '%' ) OR  'Nieto2' IS NULL OR  'Nieto2' =  '' AND pog_hijo IS NULL AND pog_nieto IS NOT NULL))");
								$datonie = mysqli_fetch_array($nie);
								$clanie = $datonie['pog_hijo'];
								$con = mysqli_query($conectar,"select * from posicion_geografica where ((pog_nombre LIKE REPLACE('".$pad."%',' ','%') OR '".$pad."' IS NULL OR '".$pad."' = '' and pog_hijo is null and pog_nieto is null) or pog_clave_int in (select pog_hijo from posicion_geografica where (pog_nombre LIKE REPLACE('".$hij."%',' ','%') OR '".$hij."' IS NULL OR '".$hij."' = '' and pog_hijo is not null and pog_nieto is null)) or pog_clave_int in ('".$clanie."') and pog_hijo is null and pog_nieto is null) and (pog_sw_activo = '".$act."' OR '".$act."' IS NULL OR '".$act."' = '') and pog_hijo is null and pog_nieto is null order by pog_nombre");
								$num = mysqli_num_rows($con);
								for($i = 0; $i < $num; $i++)
								{
									$dato = mysqli_fetch_array($con);
									$clan1 = $dato['pog_clave_int'];
									$nom = $dato['pog_nombre'];
									$act = $dato['pog_sw_activo'];
									$usuact = $dato['pog_usu_actualiz'];
									$fecact = $dato['pog_fec_actualiz'];
									$contador=$contador+1;
							?>
					        <li>
							<div style="float:left;height:50px">
								<?php
									if($i == 0)
									{
										echo "<br>";
									}
								?>
								<img alt="" class="expand" src="../../images/Minus.png" />
								<img alt="" class="collapse" onclick="DEPARTAMENTOSCIUDADES('<?php echo $clan1; ?>')" src="../../images/Plus.png" />
							</div>
							<div style="float:left; width:95%">
								<table>
									<?php
									if($i == 0)
									{
									?>
									<tr>
										<td class="auto-style3" style="width: 21px">&nbsp;
										</td>
										<td class="auto-style3"><strong>Región</strong></td>
										<td class="auto-style3"><strong>Usuario</strong></td>
										<td class="auto-style3"><strong>Actualización</strong></td>
										<td class="auto-style3" style="width: 50px"><strong>Activo</strong></td>
										<td class="auto-style3" style="width: 50px">&nbsp;
										</td>
									</tr>
									<?php
									}
									?>
									<tr>
										<td class="auto-style21" style="width: 21px">
										<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<?php echo $contador;?>" value="<?php echo $dato['pog_clave_int'];?>" class="auto-style6" />
										</td>
										<td class="auto-style21" style="width: 150px"><?php echo $nom; ?></td>
										<td class="auto-style21" style="width: 200px"><?php echo $usuact; ?></td>
										<td class="auto-style21" style="width: 200px"><?php echo $fecact; ?></td>
										<td class="auto-style21" style="width: 200px"><input name="activarinactivar" id="activarinactivar" type="checkbox" <?php if($act == 1){ echo 'checked="checked"'; } ?> disabled="disabled" class="auto-style6" ></td>
										<td class="auto-style21" style="width: 200px">
										<?php
										if($metodo == 1)
										{
										?>
										<a data-reveal-id="nuevaposicion" data-animation="fade" style="cursor:pointer" onclick="EDITARNIVEL('NIVEL1','<?php echo $dato['pog_clave_int']; ?>')"><img src="../../img/editar.png" alt="" height="22" width="21" /></a>
										<?php
										}
										?>
										</td>
									</tr>
								</table>
							</div>
							<div>
					        </div>
							<ul style="display:none">
					            <div id="depciu<?php echo $clan1; ?>">
											            
								</div>
					            </ul>
					        </li>
					        <?php
								}
							?>
					    </ul>
					</div>
					</div>
					</td>
					</tr>
	</table>
<?php
		exit();
	}
	if($_GET['listan1'] == 'si')
	{
		sleep(1);
?>
		<select name="listanivel1" id="listanivel1" size="5" style="width: 2000px">
			<?php
			$sql = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo is null and pog_nieto is null");
			$num = mysqli_num_rows($sql);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($sql);
				$cla = $dato['pog_clave_int'];
				$nom = $dato['pog_nombre'];
			?>
			<option value="<?php echo $cla; ?>" ondblclick="EDITARNIVEL('NIVEL1','<?php echo $cla; ?>')"><?php echo $nom; ?></option>
			<?php
			}
			?>
		</select>
<?php
		exit();
	}
	if($_GET['listan2'] == 'si')
	{
		sleep(1);
?>
		<select name="listanivel2" id="listanivel2" size="5" style="width: 2000px">
			<?php
			$sql = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo is not null and pog_nieto is null");
			$num = mysqli_num_rows($sql);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($sql);
				$cla = $dato['pog_clave_int'];
				$nom = $dato['pog_nombre'];
				$hij = $dato['pog_hijo'];
				$sql1 = mysqli_query($conectar,"select * from posicion_geografica where pog_clave_int = '".$hij."'");
				$dato1 = mysqli_fetch_array($sql1);
				$pad = $dato1['pog_nombre'];
			?>
			<option value="<?php echo $cla; ?>" ondblclick="EDITARNIVEL('NIVEL2','<?php echo $cla; ?>')"><?php echo $pad." - ".$nom; ?></option>
			<?php
			}
			?>
		</select>
<?php
		exit();
	}
	if($_GET['listan3'] == 'si')
	{
		sleep(1);
?>
		<select name="nivellista3" id="nivellista3" size="5" style="width: 2000px;">
			<?php
			$sql = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo is null and pog_nieto is not null");
			$num = mysqli_num_rows($sql);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($sql);
				$cla = $dato['pog_clave_int'];
				$nom = $dato['pog_nombre'];
				$nie = $dato['pog_nieto'];
				$sql1 = mysqli_query($conectar,"select * from posicion_geografica where pog_clave_int = '".$nie."'");
				$dato1 = mysqli_fetch_array($sql1);
				$hij = $dato1['pog_nombre'];
				$clahij = $dato1['pog_hijo'];
				
				$sql2 = mysqli_query($conectar,"select * from posicion_geografica where pog_clave_int = '".$clahij."'");
				$dato2 = mysqli_fetch_array($sql2);
				$pad = $dato2['pog_nombre'];
				
			?>
			<option value="<?php echo $cla; ?>" ondblclick="EDITARNIVEL('NIVEL3','<?php echo $cla; ?>')"><?php echo $pad." - ".$hij." - ".$nom; ?></option>
			<?php
			}
			?>
		</select>
<?php
		exit();
	}
	if($_GET['niveln1'] == 'si')
	{
		sleep(1);
?>
		<select name="nivel21" id="nivel21" style="width: 130px">
			<option value="">-Seleccione-</option>
			<?php
			$sql = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo is null and pog_nieto is null");
			$num = mysqli_num_rows($sql);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($sql);
				$cla = $dato['pog_clave_int'];
				$nom = $dato['pog_nombre'];
			?>
			<option value="<?php echo $cla; ?>"><?php echo $nom; ?></option>
			<?php
			}
			?>
		</select>
<?php
		exit();
	}
	if($_GET['niveln11'] == 'si')
	{
		sleep(1);
?>
		<select name="nivel31" id="nivel31" style="width: 130px">
			<option value="">-Seleccione-</option>
			<?php
			$sql = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo is null and pog_nieto is null");
			$num = mysqli_num_rows($sql);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($sql);
				$cla = $dato['pog_clave_int'];
				$nom = $dato['pog_nombre'];
			?>
			<option value="<?php echo $cla; ?>"><?php echo $nom; ?></option>
			<?php
			}
			?>
		</select>
<?php
		exit();
	}
	if($_GET['niveln2'] == 'si')
	{
		sleep(1);
?>
		<select name="nivel32" id="nivel32" style="width: 131px">
			<option value="">-Seleccione-</option>
			<?php
			$sql = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo is not null and pog_nieto is null");
			$num = mysqli_num_rows($sql);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($sql);
				$cla = $dato['pog_clave_int'];
				$nom = $dato['pog_nombre'];
			?>
			<option value="<?php echo $cla; ?>"><?php echo $nom; ?></option>
			<?php
			}
			?>
		</select>
<?php
		exit();
	}
	if($_GET['editarnivel1'] == 'si')
	{
		sleep(1);
		$cla = $_GET['niv1'];
		$con = mysqli_query($conectar,"select * from posicion_geografica where pog_clave_int = '".$cla."'");
		$dato = mysqli_fetch_array($con);
		$nom = $dato['pog_nombre'];
?>
		 <table style="width: 50%">
			<tr>
				<td>
		<input name="edinivel1" id="edinivel1" type="text" value="<?php echo $nom; ?>" style="width: 130px"></td>
				<td style="width: 153px">
					<input name="Button4" onclick="EDITAR('<?php echo $cla; ?>','NIVEL1')" type="button" value="Guardar">
				</td>
				<td><input name="Button5" onclick="ELIMINAR('<?php echo $cla; ?>','NIVEL1')" type="button" value="Eliminar"></td>
			</tr>
		</table>
<?php
		exit();
	}
	if($_GET['editarnivel2'] == 'si')
	{
		sleep(1);
		$clan1 = $_GET['niv2'];
		$con = mysqli_query($conectar,"select * from posicion_geografica where pog_clave_int = '".$clan1."'");
		$dato = mysqli_fetch_array($con);
		$nom1 = $dato['pog_nombre'];
		$pad = $dato['pog_hijo'];
?>
		<table style="width: 50%">
			<tr>
				<td>
				<select name="edinivel21" id="edinivel21" style="width: 130px">
				<option value="">-Seleccione-</option>
				<?php
				$sql = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo is null and pog_nieto is null");
				$num = mysqli_num_rows($sql);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($sql);
					$cla = $dato['pog_clave_int'];
					$nom = $dato['pog_nombre'];
				?>
				<option value="<?php echo $cla; ?>" <?php if($pad == $cla){ echo 'selected="selected"'; } ?>><?php echo $nom; ?></option>
				<?php
				}
				?>
				</select>
				</td>
				<td>
		<input name="edinivel2" id="edinivel2" type="text" value="<?php echo $nom1; ?>" style="width: 130px"></td>
				<td style="width: 153px">
					<input name="Button4" onclick="EDITAR('<?php echo $clan1; ?>','NIVEL2')" type="button" value="Guardar">
				</td>
				<td><input name="Button5" onclick="ELIMINAR('<?php echo $clan1; ?>','NIVEL2')" type="button" value="Eliminar"></td>
			</tr>
		</table>
<?php
		exit();
	}
	if($_GET['editarnivel3'] == 'si')
	{
		sleep(1);
		$clan1 = $_GET['niv3'];
		$con = mysqli_query($conectar,"select * from posicion_geografica where pog_clave_int = '".$clan1."'");
		$dato = mysqli_fetch_array($con);
		$nom = $dato['pog_nombre'];
		$nie = $dato['pog_nieto'];
		
		$con2 = mysqli_query($conectar,"select * from posicion_geografica where pog_clave_int = '".$nie."'");
		$dato2 = mysqli_fetch_array($con2);
		$pad = $dato2['pog_hijo'];
?>
		<table style="width: 50%">
			<tr>
				<td>
				<select name="edinivel31" id="edinivel31" style="width: 110px">
				<option value="">-Seleccione-</option>
				<?php
				$sql = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo is null and pog_nieto is null");
				$num = mysqli_num_rows($sql);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($sql);
					$cla = $dato['pog_clave_int'];
					$nom1 = $dato['pog_nombre'];
				?>
				<option value="<?php echo $cla; ?>" <?php if($pad == $cla){ echo 'selected="selected"'; } ?>><?php echo $nom1; ?></option>
				<?php
				}
				?>
				</select>
				</td>
				<td>
				<select name="edinivel32" id="edinivel32" style="width: 110px">
				<option value="">-Seleccione-</option>
				<?php
				$sql = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo is not null and pog_nieto is null");
				$num = mysqli_num_rows($sql);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($sql);
					$cla = $dato['pog_clave_int'];
					$nom2 = $dato['pog_nombre'];
				?>
				<option value="<?php echo $cla; ?>" <?php if($nie == $cla){ echo 'selected="selected"'; } ?>><?php echo $nom2; ?></option>
				<?php
				}
				?>
				</select>
				</td>
				<td>
		<input name="edinivel3" id="edinivel3" type="text" value="<?php echo $nom; ?>" style="width: 110px"></td>
				<td style="width: 153px">
					<input name="Button4" onclick="EDITAR('<?php echo $clan1; ?>','NIVEL3')" type="button" value="Guardar">
				</td>
				<td><input name="Button5" onclick="ELIMINAR('<?php echo $clan1; ?>','NIVEL3')" type="button" value="Eliminar"></td>
			</tr>
		</table>
<?php
		exit();
	}
	if($_GET['editaractn1'] == 'si')
	{
		sleep(1);
		$cla = $_GET['actedi'];
		$nom = $_GET['nom'];

		$con = mysqli_query($conectar,"update posicion_geografica set pog_nombre = '".$nom."' where pog_clave_int = '".$cla."'");
		
		if($con >= 1)
		{
			echo "<div class='ok'>Datos grabados correctamente</div>";
			mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'5',26,'".$cla."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 26=Actualización posición geográfica
		}
		else
		{
			echo "<div class='validaciones'>No se han podido guardar los datos</div>";
		}
		exit();
	}
	if($_GET['editaractn2'] == 'si')
	{
		sleep(1);
		$cla = $_GET['actedi'];
		$pad = $_GET['pad'];
		$nom = $_GET['nom'];

		$con = mysqli_query($conectar,"update posicion_geografica set pog_hijo = '".$pad."',pog_nombre = '".$nom."' where pog_clave_int = '".$cla."'");
		
		if($con >= 1)
		{
			echo "<div class='ok'>Datos grabados correctamente</div>";
			mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'5',26,'".$cla."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 26=Actualización posición geográfica
		}
		else
		{
			echo "<div class='validaciones'>No se han podido guardar los datos</div>";
		}
		exit();
	}
	if($_GET['editaractn3'] == 'si')
	{
		sleep(1);
		$cla = $_GET['actedi'];
		$pad = $_GET['pad'];
		$hij = $_GET['hij'];
		$nom = $_GET['nom'];

		$con = mysqli_query($conectar,"update posicion_geografica set pog_nieto = '".$hij."',pog_nombre = '".$nom."' where pog_clave_int = '".$cla."'");
		
		if($con >= 1)
		{
			echo "<div class='ok'>Datos grabados correctamente</div>";
			mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'5',26,'".$cla."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 26=Actualización posición geográfica
		}
		else
		{
			echo "<div class='validaciones'>No se han podido guardar los datos</div>";
		}
		exit();
	}
	if($_GET['eliminaractn1'] == 'si')
	{
		sleep(1);
		$cla = $_GET['actedi'];
		$sql = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo = '".$cla."'");
		$num = mysqli_num_rows($sql);
		
		/*$con = mysqli_query($conectar,"select * from programacion where pog_clave_int = '".$cla."'");
		$num1 = mysqli_num_rows($con);
		
		if($num1 > 0)
		{
			for($i = 0; $i < $num1; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$pro = $dato['pro_clave_int'];
				$men = $men.", ".$pro;
			}
			echo "<div class='validaciones'>Este nivel está siendo usado en las siguientes programaciones: $men</div>";
		}
		else*/
		if($num > 0)
		{
			echo "<div class='validaciones'>Este nivel está siendo usado</div>";
		}
		else
		{
			$con = mysqli_query($conectar,"delete from posicion_geografica where pog_clave_int = '".$cla."'");
			
			if($con >= 1)
			{
				echo "<div class='ok'>Registro borrado correctamente</div>";
				mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'5',27,'".$cla."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 27=Eliminación posición geográfica
			}
			else
			{
				echo "<div class='validaciones'>No se han podido borrar los datos</div>";
			}
		}
		exit();
	}
	if($_GET['eliminaractn2'] == 'si')
	{
		sleep(1);
		$cla = $_GET['actedi'];
		$sql = mysqli_query($conectar,"select * from posicion_geografica where pog_nieto = '".$cla."'");
		$num = mysqli_num_rows($sql);
		
		/*$con = mysqli_query($conectar,"select * from programacion where pog_clave_int = '".$cla."'");
		$num1 = mysqli_num_rows($con);
		
		if($num1 > 0)
		{
			for($i = 0; $i < $num1; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$pro = $dato['pro_clave_int'];
				$men = $men.", ".$pro;
			}
			echo "<div class='validaciones'>Este nivel está siendo usado en las siguientes programaciones: $men</div>";
		}
		else*/
		if($num > 0)
		{
			echo "<div class='validaciones'>Este nivel está siendo usado</div>";
		}
		else
		{
			$con = mysqli_query($conectar,"delete from posicion_geografica where pog_clave_int = '".$cla."'");
			
			if($con >= 1)
			{
				echo "<div class='ok'>Registro borrado correctamente</div>";
				mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'5',27,'".$cla."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 27=Eliminación posición geográfica
			}
			else
			{
				echo "<div class='validaciones'>No se han podido borrar los datos</div>";
			}
		}
		exit();
	}
	if($_GET['eliminaractn3'] == 'si')
	{
		sleep(1);
		$cla = $_GET['actedi'];
		
		/*$con = mysqli_query($conectar,"select * from programacion where pog_clave_int = '".$cla."'");
		$num1 = mysqli_num_rows($con);
		
		if($num1 > 0)
		{
			for($i = 0; $i < $num1; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$pro = $dato['pro_clave_int'];
				$men = $men.", ".$pro;
			}
			echo "<div class='validaciones'>Este nivel está siendo usado en las siguientes programaciones: $men</div>";
		}
		else
		{*/
			$con = mysqli_query($conectar,"delete from posicion_geografica where pog_clave_int = '".$cla."'");
			
			if($con >= 1)
			{
				echo "<div class='ok'>Registro borrado correctamente</div>";
				mysqli_query($conectar,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,tia_registro,loa_usu_actualiz,loa_fec_actualiz) values(null,'5',27,'".$cla."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 27=Eliminación posición geográfica
			}
			else
			{
				echo "<div class='validaciones'>No se han podido borrar los datos</div>";
			}
		//}
		exit();
	}
	if($_GET['mostrardepartamentos'] == 'si')
	{
		$reg = $_GET['reg'];
?>
		<select name="nivel32" id="nivel32" style="width: 131px">
		<option value="">-Seleccione-</option>
		<?php
		$sql = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo is not null and pog_nieto is null and pog_hijo = '".$reg."'");
		$num = mysqli_num_rows($sql);
		for($i = 0; $i < $num; $i++)
		{
			$dato = mysqli_fetch_array($sql);
			$cla = $dato['pog_clave_int'];
			$nom = $dato['pog_nombre'];
		?>
		<option value="<?php echo $cla; ?>"><?php echo $nom; ?></option>
		<?php
		}
		?>
		</select>
<?php
		exit();
	}	
	if($_GET['departamentosciudades'] == 'si')
	{
		$clan1 = $_GET['clan1'];
		$con1 = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo = '".$clan1."' order by pog_nombre");
		$num1 = mysqli_num_rows($con1);
		
		for($j = 0; $j < $num1; $j++)
		{
			$dato1 = mysqli_fetch_array($con1);
			$clan2 = $dato1['pog_clave_int'];
			$usuact = $dato1['pog_usu_actualiz'];
			$fecact = $dato1['pog_fec_actualiz'];
			$nom1 = $dato1['pog_nombre'];
	?>
        <li>
		<div style="float:left">
			<img alt="" class="expand" src="../../images/Minus.png" />
			<img alt="" class="collapse" onclick="OCULTARSCROLL()" src="../../images/Plus.png" />
		</div>
		<?php
		if($j % 2 == 0)
		{
		?>
			<div style="border:2px; outline:none; border-radius:3px; -webkit-border-radius:6px; -moz-border-radius:3px; border:1px solid rgba(0,0,0, 0.5); padding: 3px; background-color: #CCCCCC; color:black; font-size:small">
				<?php
					echo "<strong>Departamento:</strong> ".$nom1." <strong>Usuario:</strong> ".$usuact." <strong>Actualización:</strong> ".$fecact; 
				?>
				<?php
				if($metodo == 1)
				{
				?>
				<a data-reveal-id="nuevaposicion" data-animation="fade" style="cursor:pointer" onclick="EDITARNIVEL('NIVEL2','<?php echo $dato1['pog_clave_int']; ?>')"><img src="../../img/editar.png" alt="" height="22" width="21" /></a>
				<?php
				}
				?>
            </div>
        <?php
        }
        else
        {
        ?>
        	<div style="border:2px; outline:none; border-radius:3px; -webkit-border-radius:6px; -moz-border-radius:3px; border:1px solid rgba(0,0,0, 0.5); padding: 3px; background-color: #F0F0F0; color:black; font-size:small">
			<?php
				echo "<strong>Departamento:</strong> ".$nom1." <strong>Usuario:</strong> ".$usuact." <strong>Actualización:</strong> ".$fecact; 
			?>
			<?php
			if($metodo == 1)
			{
			?>
			<a data-reveal-id="nuevaposicion" data-animation="fade" style="cursor:pointer" onclick="EDITARNIVEL('NIVEL2','<?php echo $dato1['pog_clave_int']; ?>')"><img src="../../img/editar.png" alt="" height="22" width="21" /></a>
			<?php
			}
			?>
        	</div>
        <?php
        }
        ?>
		<ul style="display:none">
            <?php
				$con2 = mysqli_query($conectar,"select * from posicion_geografica where pog_nieto = '".$clan2."' order by pog_nombre");
				$num2 = mysqli_num_rows($con2);
				for($k = 0; $k < $num2; $k++)
				{
					$dato2 = mysqli_fetch_array($con2);
					$clan3 = $dato2['pog_clave_int'];
					$nom2 = $dato2['pog_nombre'];
					$usuact = $dato2['pog_usu_actualiz'];
					$fecact = $dato2['pog_fec_actualiz'];
			?>
                <li>
				<div>
					<table style="width: 100%">
						<tr>
							<td class="auto-style13" style="width: 160px"><strong>Municipio o Ciudad: </strong></td>
							<td class="auto-style13" style="width: 80px"><?php echo $nom2; ?></td>
							<td class="auto-style13" style="width: 55px"><strong>Usuario: </strong></td>
							<td class="auto-style13" style="width: 101px"><?php echo $usuact; ?></td>
							<td class="auto-style13" style="width: 90px"><strong>Actualización: </strong></td>
							<td class="auto-style13" style="width: 157px"><?php echo $fecact; ?></td>
							<td class="auto-style13" style="width: 50px">
							<?php
							if($metodo == 1)
							{
							?>
							<a data-reveal-id="nuevaposicion" data-animation="fade" style="cursor:pointer" onclick="EDITARNIVEL('NIVEL3','<?php echo $dato2['pog_clave_int']; ?>')"><img src="../../img/editar.png" alt="" height="22" width="21" /></a>
							<?php
							}
							?>
							</td>
						</tr>
					</table>
					<?php //echo "<strong>Artículo:</strong> ".$codart." <strong>Nombre Artículo:</strong> ".$nomart." <strong>Cantidad:</strong> ".$canart." <strong>Devoluciones: </strong>".$candev; ?>
                </div>
                </li>
            <?php
				}
			?>
            </ul>
        </li>
        <?php
			}
		exit();
	}
?>
<!DOCTYPE HTML>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>CONTROL DE OBRAS</title>
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
<meta name="description" content="Service Desk">
<meta name="author" content="InvGate S.R.L.">

<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
<!--[if lte IE 7]>
<link rel="stylesheet" href="css/ie.c32bc18afe0e2883ee4912a51f86c119.css" type="text/css" />
<![endif]-->

<script type="text/javascript" language="javascript">
	selecteds=0;
	
	function CheckUncheck(total,check){
		checkbox=null;
		for(i=1;i<=total;i++){
			checkbox=document.getElementById("idcat"+i);
			//alert(checkbox.value);
			checkbox.checked=check.checked;
		}
		
		if(check.checked){
			selecteds=total;
		}else{
			selecteds=0;
		}
		
	}
	
	function contadorVals(check){
		if(check.checked){
			selecteds=selecteds+1;
		}else{
			selecteds=selecteds-1;
		}
	}
	
	function selectedVals(){
		if(selecteds==0){
			alert("Seleccione al menos un registro.");
			return false;
		}else{
			return true;
		}
	}
</script>
<?php //VALIDACIONES ?>
<script type="text/javascript" src="llamadas.js"></script>
<script language="javascript" type="text/javascript" src="../../js/jquery-1.7.2.min.js"></script>

<?php //VENTANA EMERGENTE ?>
<link rel="stylesheet" href="../../css/reveal.css" />
<script type="text/javascript" src="../../js/jquery.reveal.js"></script>
<script>
function OCULTARSCROLL()
{
	setTimeout("parent.autoResize('iframe5')",500);
}
</script>

</head>
<body>
<?php
$rows=mysqli_query($conectar,"select * from posicion_geografica where pog_hijo is null and pog_nieto is null");
$total=mysqli_num_rows($rows);
?>
<form name="form1" id="form1" action="confirmar.php" method="post" onsubmit="return selectedVals();">
<!--[if lte IE 7]>
<div class="ieWarning">Este navegador no es compatible con el sistema. Por favor, use Chrome, Safari, Firefox o Internet Explorer 8 o superior.</div>
<![endif]-->
<table style="width: 100%">
	<tr>
		<td class="auto-style2" onclick="CONSULTAMODULO('TODOS')" onmouseover="this.style.backgroundColor='#445B74';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="width: 60px; cursor:pointer">
		Todos
		<?php
			$con = mysqli_query($conectar,"select COUNT(*) cant from posicion_geografica");
			$dato = mysqli_fetch_array($con);
			echo $dato['cant'];
		?>
		</td>
		<td class="auto-style7" style="cursor:pointer" colspan="4">
		MAESTRA DE POSICIÓN GEOGRÁFICA</td>
		<td class="auto-style2" onmouseover="this.style.backgroundColor='#CCCCCC';this.style.color='#0F213C';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="width: 55px; cursor:pointer">
		<?php
		if($metodo == 1)
		{
		?>
		<a data-reveal-id="nuevaposicion" data-animation="fade" style="cursor:pointer"><table style="width: 100%">
			<tr>
				<td style="width: 14px"><a data-reveal-id="nuevaposicion" data-animation="fade" style="cursor:pointer"><img alt="" src="../../img/add2.png"></a></td>
				<td>Añadir</td>
			</tr>
		</table></a>
		<?php
		}
		?>
		</td>
	</tr>
	<tr>
		<td class="auto-style2" colspan="6">
		<div id="filtro">
			<table style="width: 25%">
				<tr>
					<td class="auto-style1"><strong>Filtro:<img src="../../img/buscar.png" alt="" height="18" width="15" /></strong></td>
					<td class="auto-style1">
			<input class="inputs" onkeyup="BUSCAR('GEOGRAFIA')" name="nombrepad" maxlength="70" type="text" placeholder="Región" style="width: 150px" /></td>
					<td class="auto-style1">
					<input class="inputs" onkeyup="BUSCAR('GEOGRAFIA')" name="nombrehij" maxlength="70" type="text" placeholder="Departamento" style="width: 150px" />
					</td>
					<td class="auto-style1">
					<input class="inputs" onkeyup="BUSCAR('GEOGRAFIA')" name="nombrenie" maxlength="70" type="text" placeholder="Municipio o Ciudad" style="width: 150px" />
					</td>
					<td class="auto-style1">
					Activo:</td>
					<td class="auto-style1">
					<select name="buscaractivos" class="inputs" onchange="BUSCAR('GEOGRAFIA')" >
						<option value="">Todos</option>
						<option value="1">Activos</option>
						<option value="0">Inactivo</option>
					</select>
					</td>
				</tr>
			</table>
		</div>
		</td>
	</tr>
	<tr>
		<td colspan="6" class="auto-style2">
		<div id="editarposicion" class="reveal-modal" style="left: 57%; top: 50px; height: 100px; width: 350px;">
			<table style="width: 38%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Nombre:</td>
			<td class="auto-style3"><input class="inputs" name="nombre1" maxlength="50" value="<?php echo $nom; ?>" type="text" style="width: 200px" />
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Activo:</td>
			<td class="auto-style3"><input class="inputs" <?php if($act == 1){ echo 'checked="checked"'; } ?> name="activo1" type="checkbox" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4">
			<input name="submit" type="button" value="Guardar" onclick="GUARDAR('POSICION','<?php echo $actedi; ?>')"  style="width: 348px; height: 25px; cursor:pointer" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">
			<div id="datos">
			</div>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
			<a class="close-reveal-modal">&#215;</a>
		</div>
		<div id="posiciones">
		<table id="table1" class="controller" style="width: 60%">
	    <tr data-level="header" class="header">
			<td class="auto-style3" style="width: 2px"><input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<?php echo $total;?>,this);" class="auto-style6" /></td>
			<td class="auto-style3">
			<?php
			if($metodo == 1)
			{
			?>
			<table style="width: 20%; text-align:left" align="left">
				<tr>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/activo.png" alt="" class="auto-style6" /><input type="submit" value="Activar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p>
					</td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/inactivo.png" alt="" class="auto-style6" /><input type="submit" value="Inactivar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1">&nbsp;</td>
				</tr>
			</table>
			<?php
			}
			?>
			</td>
			<td class="auto-style3" style="width: 50px">&nbsp;</td>
			<td class="auto-style3">&nbsp;</td></tr>
		    <tr>
				<td class="auto-style3" colspan="3">
				<div class="inputs" style="height:100%; width:1210px; overflow-x:hidden;resize:both" align="left">
				<div class="wfm">
				    <ul class="auto-style10" style="width: 200%">
				    	<?php
							$contador=0;
							$con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo is null and pog_nieto is null order by pog_nombre");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$clan1 = $dato['pog_clave_int'];
								$nom = $dato['pog_nombre'];
								$act = $dato['pog_sw_activo'];
								$usuact = $dato['pog_usu_actualiz'];
								$fecact = $dato['pog_fec_actualiz'];
								$contador=$contador+1;
						?>
				        <li>
						<div style="float:left;height:50px">
							<?php
								if($i == 0)
								{
									echo "<br>";
								}
							?>
							<img alt="" class="expand" src="../../images/Minus.png" />
							<img alt="" class="collapse" onclick="DEPARTAMENTOSCIUDADES('<?php echo $clan1; ?>')" src="../../images/Plus.png" />
						</div>
						<div style="float:left; width:95%">
							<table>
								<?php
								if($i == 0)
								{
								?>
								<tr>
									<td class="auto-style3" style="width: 21px">&nbsp;
									</td>
									<td class="auto-style3"><strong>Región</strong></td>
									<td class="auto-style3"><strong>Usuario</strong></td>
									<td class="auto-style3"><strong>Actualización</strong></td>
									<td class="auto-style3" style="width: 50px"><strong>Activo</strong></td>
									<td class="auto-style3" style="width: 50px">&nbsp;
									</td>
								</tr>
								<?php
								}
								?>
								<tr>
									<td class="auto-style21" style="width: 21px">
									<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<?php echo $contador;?>" value="<?php echo $dato['pog_clave_int'];?>" class="auto-style6" />
									</td>
									<td class="auto-style21" style="width: 150px"><?php echo $nom; ?></td>
									<td class="auto-style21" style="width: 200px"><?php echo $usuact; ?></td>
									<td class="auto-style21" style="width: 200px"><?php echo $fecact; ?></td>
									<td class="auto-style21" style="width: 200px"><input name="activarinactivar" id="activarinactivar" type="checkbox" <?php if($act == 1){ echo 'checked="checked"'; } ?> disabled="disabled" class="auto-style6" ></td>
									<td class="auto-style21" style="width: 200px">
									<?php
									if($metodo == 1)
									{
									?>
									<a data-reveal-id="nuevaposicion" data-animation="fade" style="cursor:pointer" onclick="EDITARNIVEL('NIVEL1','<?php echo $dato['pog_clave_int']; ?>')"><img src="../../img/editar.png" alt="" height="22" width="21" /></a>
									<?php
									}
									?>
									</td>
								</tr>
							</table>
						</div>
						<div>
				        </div>
						<ul style="display:none">
				            <div id="depciu<?php echo $clan1; ?>">
										            
							</div>
				            </ul>
				        </li>
				        <?php
							}
						?>
				    </ul>
				</div>
				</div>
				</td>
				<td class="auto-style3">&nbsp;</td>
			</tr>
	</table>
</div>
<div id="nuevaposicion" class="auto-style4" style="left: 50%; top: 50px; height: 480px; width: 550px; overflow:auto">
<table style="width: 55%">
	<tr>
		<td class="auto-style1" colspan="3">REGIONES</td>
	</tr>
	<tr>
		<td>Región:</td>
		<td>
			<input name="nivel1" id="nivel1" type="text" style="width: 130px"></td>
		<td rowspan="2">
		<div style="overflow:auto;width:300px;height:110px;" align="left" id="listan1">
			<select name="listanivel1" id="listanivel1" size="5" style="width: 2000px">
			<?php
			$sql = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo is null and pog_nieto is null");
			$num = mysqli_num_rows($sql);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($sql);
				$cla = $dato['pog_clave_int'];
				$nom = $dato['pog_nombre'];
			?>
			<option value="<?php echo $cla; ?>" ondblclick="EDITARNIVEL('NIVEL1','<?php echo $cla; ?>')"><?php echo $nom; ?></option>
			<?php
			}
			?>
			</select>
		</div>
			</td>
	</tr>
	<tr>
		<td>
			<input name="Button1" onclick="GUARDARNIVEL('NIVEL1')" type="button" value="Guardar"></td>
		<td>
		<div id="nivel1">
		</div>
		</td>
	</tr>
	<tr>
		<td colspan="3">
		<div id="editarnivel1">
		</div>
		<hr>
		</td>
	</tr>
	<tr>
		<td class="auto-style1" colspan="3">DEPARTAMENTOS</td>
	</tr>
	<tr>
		<td>Región:</td>
		<td>
		<div id="n1">
			<select name="nivel21" id="nivel21" style="width: 130px">
			<option value="">-Seleccione-</option>
			<?php
			$sql = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo is null and pog_nieto is null");
			$num = mysqli_num_rows($sql);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($sql);
				$cla = $dato['pog_clave_int'];
				$nom = $dato['pog_nombre'];
			?>
			<option value="<?php echo $cla; ?>"><?php echo $nom; ?></option>
			<?php
			}
			?>
			</select>
		</div>
		</td>
		<td rowspan="3">
		<div style="overflow:auto;width:300px;height:110px;" align="left" id="listan2">
			<select name="listanivel2" id="listanivel2" size="5" style="width: 2000px">
			<?php
			$sql = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo is not null and pog_nieto is null");
			$num = mysqli_num_rows($sql);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($sql);
				$cla = $dato['pog_clave_int'];
				$nom = $dato['pog_nombre'];
				$hij = $dato['pog_hijo'];
				$sql1 = mysqli_query($conectar,"select * from posicion_geografica where pog_clave_int = '".$hij."'");
				$dato1 = mysqli_fetch_array($sql1);
				$pad = $dato1['pog_nombre'];
			?>
			<option value="<?php echo $cla; ?>" ondblclick="EDITARNIVEL('NIVEL2','<?php echo $cla; ?>')"><?php echo $pad." - ".$nom; ?></option>
			<?php
			}
			?>
			</select>
		</div>	
		</td>
	</tr>
	<tr>
		<td>Departamento:</td>
		<td>
			<input name="nivel2" id="nivel2" type="text" style="width: 130px"></td>
	</tr>
	<tr>
		<td>
			<input name="Button2" onclick="GUARDARNIVEL('NIVEL2')" type="button" value="Guardar"></td>
		<td>
		<div id="nivel2">
		</div>
		</td>
	</tr>
	<tr>
		<td class="auto-style1" colspan="3">
		<div id="editarnivel2">
		</div>
		<hr>
		</td>
	</tr>
	<tr>
		<td class="auto-style1" colspan="3">MUNICIPIOS O CIUDADES</td>
	</tr>
	<tr>
		<td>Región:</td>
		<td>
		<div id="n11">
			<select name="nivel31" id="nivel31" onchange="MOSTRARDEPARTAMENTOS(this.value)" style="width: 130px">
			<option value="">-Seleccione-</option>
			<?php
			$sql = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo is null and pog_nieto is null");
			$num = mysqli_num_rows($sql);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($sql);
				$cla = $dato['pog_clave_int'];
				$nom = $dato['pog_nombre'];
			?>
			<option value="<?php echo $cla; ?>"><?php echo $nom; ?></option>
			<?php
			}
			?>
			</select>
		</div>	
		</td>
		<td rowspan="4">
			<div style="overflow:auto;width:300px;height:110px;" align="left" id="listan3">
			<select name="nivellista3" id="nivellista3" size="5" style="width: 2000px;">
			<?php
			$sql = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo is null and pog_nieto is not null");
			$num = mysqli_num_rows($sql);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($sql);
				$cla = $dato['pog_clave_int'];
				$nom = $dato['pog_nombre'];
				$nie = $dato['pog_nieto'];
				$sql1 = mysqli_query($conectar,"select * from posicion_geografica where pog_clave_int = '".$nie."'");
				$dato1 = mysqli_fetch_array($sql1);
				$hij = $dato1['pog_nombre'];
				$clahij = $dato1['pog_hijo'];
				
				$sql2 = mysqli_query($conectar,"select * from posicion_geografica where pog_clave_int = '".$clahij."'");
				$dato2 = mysqli_fetch_array($sql2);
				$pad = $dato2['pog_nombre'];
				
			?>
			<option value="<?php echo $cla; ?>" ondblclick="EDITARNIVEL('NIVEL3','<?php echo $cla; ?>')"><?php echo $pad." - ".$hij." - ".$nom; ?></option>
			<?php
			}
			?>
			</select>
			</div>
			</td>
	</tr>
	<tr>
		<td>Departamento:</td>
		<td>
		<div id="n2">
			<select name="nivel32" id="nivel32" style="width: 131px">
			<option value="">-Seleccione-</option>
			<?php
			$sql = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo is not null and pog_nieto is null");
			$num = mysqli_num_rows($sql);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($sql);
				$cla = $dato['pog_clave_int'];
				$nom = $dato['pog_nombre'];
			?>
			<option value="<?php echo $cla; ?>"><?php echo $nom; ?></option>
			<?php
			}
			?>
			</select>
		</div>
		</td>
		</tr>
	<tr>
		<td>Municipio o Ciudad:</td>
		<td>
			<input name="nivel3" id="nivel3" type="text" style="width: 130px"></td>
		</tr>
	<tr>
		<td>
			<input name="Button3" onclick="GUARDARNIVEL('NIVEL3')" type="button" value="Guardar"></td>
		<td>
		<div id="nivel3">
		</div>
		</td>
		</tr>
	<tr>
		<td colspan="3">
		<div id="editarnivel3">
		</div>
		<hr>
		</td>
		</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;
			</td>
		<td>&nbsp;</td>
	</tr>
</table>
</div>
		</td>
	</tr>
	<tr>
		<td style="width: 60px">&nbsp;</td>
		<td style="width: 117px">&nbsp;</td>
		<td style="width: 65px">&nbsp;</td>
		<td style="width: 70px">&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td style="width: 60px">&nbsp;</td>
		<td style="width: 117px">&nbsp;</td>
		<td style="width: 65px">&nbsp;</td>
		<td style="width: 70px">&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>
<script type="text/javascript" language="javascript">
    $("#posiciones").on("click",".expand",function(){
        $(this).toggle();
        $(this).next().toggle();
        $(this).parent().parent().children().last().toggle();
    });
    $("#posiciones").on("click",".collapse",function(){
        $(this).toggle();
        $(this).prev().toggle();
        $(this).parent().parent().children().last().toggle();
    });
</script>

</form>
</body>
</html>