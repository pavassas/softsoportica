function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function MODULO(v,e)
{	
	if(v == 'CAJEROS')
	{
		window.location.href = "cajeros.php";
	}
	else
	if(v == 'TODOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('cajeros').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#cajeros").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?todos=si&est="+e,true);
		ajax.send(null);
		setTimeout("REFRESCARLISTAS()",500);
	}
	OCULTARSCROLL();
}
function BUSCAR(v,p)
{	
    var cajeros = OBTENERSELECCIONMULTIPLE("multiselect_buscajero");//form1.buscajero.value;

	var fii = form1.fechainicio.value;
	var ffi = form1.fechafin.value;
	var fie = form1.fechainicioentrega.value;
	var ffe = form1.fechafinentrega.value;
	
	var nomcaj = form1.busnombre.value;
	var dircaj = form1.busdireccion.value;
	var est = OBTENERSELECCIONMULTIPLE("multiselect_busestado");//form1.busestado.value;
	var anocon = form1.busanocontable.value;
	var reg = OBTENERSELECCIONMULTIPLE("multiselect_busregion");//form1.busregion.value;
	var dep = OBTENERSELECCIONMULTIPLE("multiselect_busdepartamento");//form1.busdepartamento.value;
	var mun = OBTENERSELECCIONMULTIPLE("multiselect_busmunicipio");//form1.busmunicipio.value;
	var tip = OBTENERSELECCIONMULTIPLE("multiselect_bustipologia");//form1.bustipologia.value;
	var tipint = OBTENERSELECCIONMULTIPLE("multiselect_bustipointervencion");//form1.bustipointervencion.value;
	var moda = OBTENERSELECCIONMULTIPLE("multiselect_busmodalidad");//form1.busmodalidad.value;
	
	var cod = form1.buscodigo.value;
	var cencos =  OBTENERSELECCIONMULTIPLE("multiselect_buscentrocostos");// form1.buscentrocostos.value;
	var refmaq = OBTENERSELECCIONMULTIPLE("multiselect_busreferenciamaquina");//form1.busreferenciamaquina.value;
	var ubi = OBTENERSELECCIONMULTIPLE("multiselect_busubicacion");//form1.busubicacion.value;
	var codsuc = form1.buscodigosuc.value;
	var ubiatm = OBTENERSELECCIONMULTIPLE("multiselect_busubicacionamt");//form1.busubicacionamt.value;
	var ati = OBTENERSELECCIONMULTIPLE("multiselect_busatiende");//form1.busatiende.value;
	var rie = OBTENERSELECCIONMULTIPLE("multiselect_busriesgo");//form1.busriesgo.value;
	var are = OBTENERSELECCIONMULTIPLE("multiselect_busarea");//form1.busarea.value;
	var fecapagadoatm = form1.busfechaapagadoatm.value;
	
	var nominm = OBTENERSELECCIONMULTIPLE("multiselect_busnombreinmobiliaria");//form1.busnombreinmobiliaria.value;
	var estinm = OBTENERSELECCIONMULTIPLE("multiselect_busestadoinmobiliaria");//form1.busestadoinmobiliaria.value;
	var feciniinmdesde = form1.busfechainicioinmobiliaria.value;
	var feciniinmhasta = form1.busfechafininmobiliaria.value;
	var fecentinmdesde = form1.busfechaentregainiinfoinmobiliaria.value;
	var fecentinmhasta = form1.busfechaentregafininfoinmobiliaria.value;
	var diasinm = form1.bustotaldiasinmobiliaria.value;
	var notinm = form1.busnotasinmobiliaria.value;
	
	var nomvis = OBTENERSELECCIONMULTIPLE("multiselect_busnombrevisita");//form1.busnombrevisita.value;
	var estvis = OBTENERSELECCIONMULTIPLE("multiselect_busestadovisita");//form1.busestadovisita.value;
	var fecinivisdesde = form1.busfechainiciovisita.value;
	var fecinivishasta = form1.busfechafinvisita.value;
	var fecentvisdesde = form1.busfechaentregainiinfovisita.value;
	var fecentvishasta = form1.busfechaentregafininfovisita.value;
	var diasvis = form1.bustotaldiasvisita.value;
	var notvis = form1.busnotasvisita.value;
	
	var nomdis = OBTENERSELECCIONMULTIPLE("multiselect_busnombrediseno");//form1.busnombrediseno.value;
	var estdis = OBTENERSELECCIONMULTIPLE("multiselect_busestadodiseno");//form1.busestadodiseno.value;
	var fecinidisdesde = form1.busfechainiciodiseno.value;
	var fecinidishasta = form1.busfechafindiseno.value;
	var fecentdisdesde = form1.busfechaentregainiinfodiseno.value;
	var fecentdishasta = form1.busfechaentregafininfodiseno.value;
	var diasdis = form1.bustotaldiasdiseno.value;
	var notdis = form1.busnotasdiseno.value;
	
	var nomges = OBTENERSELECCIONMULTIPLE("multiselect_busnombregestionador");//form1.busnombregestionador.value;
	var estges = OBTENERSELECCIONMULTIPLE("multiselect_busestadogestionador");//form1.busestadogestionador.value;
	var fecinigesdesde = form1.busfechainiciogestionador.value;
	var fecinigeshasta = form1.busfechafingestionador.value;
	var fecentgesdesde = form1.busfechaentregainiinfogestionador.value;
	var fecentgeshasta = form1.busfechaentregafininfogestionador.value;
	var diasges = form1.bustotaldiasgestionador.value;
	var notges = form1.busnotasgestionador.value;
	
	var nomint = OBTENERSELECCIONMULTIPLE("multiselect_busnombreinterventor");//form1.busnombreinterventor.value;
	var cancom = OBTENERSELECCIONMULTIPLE("multiselect_buscanalcomunicacion");//form1.buscanalcomunicacion.value;
	var feciniintdesde = form1.busfechainiciointerventor.value;
	var feciniinthasta = form1.busfechafininterventor.value;
	var fecentintdesde = form1.busfechaentregainiinfointerventor.value;
	var fecentinthasta = form1.busfechaentregafininfointerventor.value;
	var fecinipedintdesde = form1.busfechainipedidointerventor.value;
	var fecinipedinthasta = form1.busfechafinpedidointerventor.value;
	var notint = form1.busnotasinterventor.value;
	
	var nomcons = OBTENERSELECCIONMULTIPLE("multiselect_busnombreconstructor");//form1.busnombreconstructor.value;
	var porava = OBTENERSELECCIONMULTIPLE("multiselect_busporcentajeavance");//form1.busporcentajeavance.value;
	var feciniconsdesde = form1.busfechainicioconstructor.value;
	var feciniconshasta = form1.busfechafinconstructor.value;
	var fecentconsdesde = form1.busfechaentregainiinfoconstructor.value;
	var fecentconshasta = form1.busfechaentregafininfoconstructor.value;
	var diascons = form1.bustotaldiasconstructor.value;
	var notcons = form1.busnotasconstructor.value;
	
	var nomseg = OBTENERSELECCIONMULTIPLE("multiselect_busnombreseguridad");//form1.busnombreseguridad.value;
	var codmon = form1.buscodigomonitoreo.value;
	var fecinginiobra = form1.busfechaingresoiniobra.value;
	var fecingfinobra = form1.busfechaingresofinobra.value;
	var notseg = form1.busnotasseguridad.value;
	
	var swinfobasica = form1.ocuinfobasica.value;
	var swinfosecundaria = form1.ocuinfosecundaria.value;
	var swinmobiliaria = form1.ocuinmobiliaria.value;
	var swvisitalocal = form1.ocuvisitalocal.value;
	var swdiseno = form1.ocudiseno.value;
	var swlicencia = form1.oculicencia.value;
	var swinterventoria = form1.ocuinterventoria.value;
	var swconstructor = form1.ocuconstructor.value;
	var swseguridad = form1.ocuseguridad.value;
	
	var vernombrecajero = form1.vernombrecajero.checked;
	var verdireccion = form1.verdireccion.checked;
	var verestadocajero = form1.verestadocajero.checked;
	var veranocontable = form1.veranocontable.checked;
	var verregion = form1.verregion.checked;
	var verdepartamento = form1.verdepartamento.checked;
	var vermunicipio = form1.vermunicipio.checked;
	var vertipologia = form1.vertipologia.checked;
	var verintervencion = form1.verintervencion.checked;
	var vermodalidad = form1.vermodalidad.checked;
	
	var vercodigocajero = form1.vercodigocajero.checked;
	var vercentrocostos = form1.vercentrocostos.checked;
	var verreferencia = form1.verreferencia.checked;
	var verubicacion = form1.verubicacion.checked;
	var vercodigosuc = form1.vercodigosuc.checked;
	var verubicacionatm = form1.verubicacionatm.checked;
	var veratiende = form1.veratiende.checked;
	var verriesgo = form1.verriesgo.checked;
	var verarea = form1.verarea.checked;
	var vercodigorecibido = form1.vercodigorecibido.checked;
	
	var verinmobiliaria = form1.verinmobiliaria1.checked;
	var verestadoinm = form1.verestadoinm.checked;
	var verfechainicioinm = form1.verfechainicioinm.checked;
	var verfechaentregainm = form1.verfechaentregainm.checked;
	var vertotaldiasinm = form1.vertotaldiasinm.checked;
	var vernotasinm = form1.vernotasinm.checked;
	
	var vervisita = form1.vervisita1.checked;
	var verestadovis = form1.verestadovis.checked;
	var verfechainiciovis = form1.verfechainiciovis.checked;
	var verfechaentregavis = form1.verfechaentregavis.checked;
	var vertotaldiasvis = form1.vertotaldiasvis.checked;
	var vernotasvis = form1.vernotasvis.checked;
	
	var verdisenador = form1.verdisenador.checked;
	var verestadodis = form1.verestadodis.checked;
	var verfechainiciodis = form1.verfechainiciodis.checked;
	var verfechaentregadis = form1.verfechaentregadis.checked;
	var vertotaldiasdis = form1.vertotaldiasdis.checked;
	var vernotasdis = form1.vernotasdis.checked;
	
	var vergestionador = form1.vergestionador.checked;
	var verestadolic = form1.verestadolic.checked;
	var verfechainiciolic = form1.verfechainiciolic.checked;
	var verfechaentregalic = form1.verfechaentregalic.checked;
	var vertotaldiaslic = form1.vertotaldiaslic.checked;
	var vernotaslic = form1.vernotaslic.checked;
	
	var verinterventor = form1.verinterventor.checked;
	var veroperadorcanal = form1.veroperadorcanal.checked;
	var verfechainicioint = form1.verfechainicioint.checked;
	var verfechaentregaint = form1.verfechaentregaint.checked;
	var verfechapedido = form1.verfechapedido.checked;
	var vernotasint = form1.vernotasint.checked;
	
	var verconstructor = form1.verconstructor1.checked;
	var veravance = form1.veravance.checked;
	var verfechainiciocons = form1.verfechainiciocons.checked;
	var verfechaentregacons = form1.verfechaentregacons.checked;
	var vertotaldiascons = form1.vertotaldiascons.checked;
	var vernotascons = form1.vernotascons.checked;
	
	var verseguridad = form1.verseguridad1.checked;
	var vercodmon = form1.vercodigomonitoreo.checked;
	var verfecingobra = form1.verfechaingresoobra.checked;
	var vernotasseg = form1.vernotasseg.checked;
		
	if(v == 'CAJERO')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('busqueda').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#busqueda").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='50' width='50' />"); //loading gif will be overwrited when ajax have success
		if(p > 0)
		{
                ajax.open("GET","?buscar=si&caj="+cajeros+"&fii="+fii+"&ffi="+ffi+"&fie="+fie+"&ffe="+ffe+"&nomcaj="+nomcaj+"&dircaj="+dircaj+"&est="+est+"&anocon="+anocon+"&reg="+reg+"&dep="+dep+"&mun="+mun+"&tip="+tip+"&tipint="+tipint+"&moda="+moda+"&cod="+cod+"&cencos="+cencos+"&refmaq="+refmaq+"&ubi="+ubi+"&codsuc="+codsuc+"&ubiatm="+ubiatm+"&ati="+ati+"&rie="+rie+"&are="+are+"&fecapagadoatm="+fecapagadoatm+"&nominm="+nominm+"&estinm="+estinm+"&finiinmdes="+feciniinmdesde+"&finiinmhas="+feciniinmhasta+"&fentinmdes="+fecentinmdesde+"&fentinmhas="+fecentinmhasta+"&diasinm="+diasinm+"&notinm="+notinm+"&nomvis="+nomvis+"&estvis="+estvis+"&finivisdes="+fecinivisdesde+"&finivishas="+fecinivishasta+"&fentvisdes="+fecentvisdesde+"&fentvishas="+fecentvishasta+"&diasvis="+diasvis+"&notvis="+notvis+"&nomdis="+nomdis+"&estdis="+estdis+"&finidisdes="+fecinidisdesde+"&finidishas="+fecinidishasta+"&fentdisdes="+fecentdisdesde+"&fentdishas="+fecentdishasta+"&diasdis="+diasdis+"&notdis="+notdis+"&nomges="+nomges+"&estges="+estges+"&finigesdes="+fecinigesdesde+"&finigeshas="+fecinigeshasta+"&fentgesdes="+fecentgesdesde+"&fentgeshas="+fecentgeshasta+"&diasges="+diasges+"&notges="+notges+"&nomint="+nomint+"&cancom="+cancom+"&finiintdes="+feciniintdesde+"&finiinthas="+feciniinthasta+"&fentintdes="+fecentintdesde+"&fentinthas="+fecentinthasta+"&finipedintdes="+fecinipedintdesde+"&finipedinthas="+fecinipedinthasta+"&notint="+notint+"&nomcons="+nomcons+"&porava="+porava+"&finiconsdes="+feciniconsdesde+"&finiconshas="+feciniconshasta+"&fentconsdes="+fecentconsdesde+"&fentconshas="+fecentconshasta+"&diascons="+diascons+"&notcons="+notcons+"&nomseg="+nomseg+"&codmon="+codmon+"&finginiobra="+fecinginiobra+"&fingfinobra="+fecingfinobra+"&notseg="+notseg+"&swinfobasica="+swinfobasica+"&swinfosecundaria="+swinfosecundaria+"&swinmobiliaria="+swinmobiliaria+"&swvisitalocal="+swvisitalocal+"&swdiseno="+swdiseno+"&swlicencia="+swlicencia+"&swinterventoria="+swinterventoria+"&swconstructor="+swconstructor+"&swseguridad="+swseguridad+"&vnomcaj="+vernombrecajero+"&vdir="+verdireccion+"&vestcaj="+verestadocajero+"&vanocont="+veranocontable+"&vreg="+verregion+"&vdep="+verdepartamento+"&vmun="+vermunicipio+"&vtipol="+vertipologia+"&vinter="+verintervencion+"&vmoda="+vermodalidad+"&vcodigocajero="+vercodigocajero+"&vcentrocostos="+vercentrocostos+"&vreferencia="+verreferencia+"&vubicacion="+verubicacion+"&vcodigosuc="+vercodigosuc+"&vubicacionatm="+verubicacionatm+"&vatiende="+veratiende+"&vriesgo="+verriesgo+"&varea="+verarea+"&vcodigorecibido="+vercodigorecibido+"&vinmobiliaria="+verinmobiliaria+"&vestadoinm="+verestadoinm+"&vfechainicioinm="+verfechainicioinm+"&vfechaentregainm="+verfechaentregainm+"&vtotaldiasinm="+vertotaldiasinm+"&vnotasinm="+vernotasinm+"&vvisita="+vervisita+"&vestadovis="+verestadovis+"&vfechainiciovis="+verfechainiciovis+"&vfechaentregavis="+verfechaentregavis+"&vtotaldiasvis="+vertotaldiasvis+"&vnotasvis="+vernotasvis+"&vdisenador="+verdisenador+"&vestadodis="+verestadodis+"&vfechainiciodis="+verfechainiciodis+"&vfechaentregadis="+verfechaentregadis+"&vtotaldiasdis="+vertotaldiasdis+"&vnotasdis="+vernotasdis+"&vgestionador="+vergestionador+"&vestadolic="+verestadolic+"&vfechainiciolic="+verfechainiciolic+"&vfechaentregalic="+verfechaentregalic+"&vtotaldiaslic="+vertotaldiaslic+"&vnotaslic="+vernotaslic+"&vinterventor="+verinterventor+"&voperadorcanal="+veroperadorcanal+"&vfechainicioint="+verfechainicioint+"&vfechaentregaint="+verfechaentregaint+"&vfechapedido="+verfechapedido+"&vnotasint="+vernotasint+"&vconstructor="+verconstructor+"&vavance="+veravance+"&vfechainiciocons="+verfechainiciocons+"&vfechaentregacons="+verfechaentregacons+"&vtotaldiascons="+vertotaldiascons+"&vnotascons="+vernotascons+"&vseguridad="+verseguridad+"&vcodmon="+vercodmon+"&vnotasseg="+vernotasseg+"&vfecingobra="+verfecingobra+"&page="+p,true);
    }
		else
		{
			ajax.open("GET","?buscar=si&caj="+cajeros+"&fii="+fii+"&ffi="+ffi+"&fie="+fie+"&ffe="+ffe+"&nomcaj="+nomcaj+"&dircaj="+dircaj+"&est="+est+"&anocon="+anocon+"&reg="+reg+"&dep="+dep+"&mun="+mun+"&tip="+tip+"&tipint="+tipint+"&moda="+moda+"&cod="+cod+"&cencos="+cencos+"&refmaq="+refmaq+"&ubi="+ubi+"&codsuc="+codsuc+"&ubiatm="+ubiatm+"&ati="+ati+"&rie="+rie+"&are="+are+"&fecapagadoatm="+fecapagadoatm+"&nominm="+nominm+"&estinm="+estinm+"&finiinmdes="+feciniinmdesde+"&finiinmhas="+feciniinmhasta+"&fentinmdes="+fecentinmdesde+"&fentinmhas="+fecentinmhasta+"&diasinm="+diasinm+"&notinm="+notinm+"&nomvis="+nomvis+"&estvis="+estvis+"&finivisdes="+fecinivisdesde+"&finivishas="+fecinivishasta+"&fentvisdes="+fecentvisdesde+"&fentvishas="+fecentvishasta+"&diasvis="+diasvis+"&notvis="+notvis+"&nomdis="+nomdis+"&estdis="+estdis+"&finidisdes="+fecinidisdesde+"&finidishas="+fecinidishasta+"&fentdisdes="+fecentdisdesde+"&fentdishas="+fecentdishasta+"&diasdis="+diasdis+"&notdis="+notdis+"&nomges="+nomges+"&estges="+estges+"&finigesdes="+fecinigesdesde+"&finigeshas="+fecinigeshasta+"&fentgesdes="+fecentgesdesde+"&fentgeshas="+fecentgeshasta+"&diasges="+diasges+"&notges="+notges+"&nomint="+nomint+"&cancom="+cancom+"&finiintdes="+feciniintdesde+"&finiinthas="+feciniinthasta+"&fentintdes="+fecentintdesde+"&fentinthas="+fecentinthasta+"&finipedintdes="+fecinipedintdesde+"&finipedinthas="+fecinipedinthasta+"&notint="+notint+"&nomcons="+nomcons+"&porava="+porava+"&finiconsdes="+feciniconsdesde+"&finiconshas="+feciniconshasta+"&fentconsdes="+fecentconsdesde+"&fentconshas="+fecentconshasta+"&diascons="+diascons+"&notcons="+notcons+"&nomseg="+nomseg+"&codmon="+codmon+"&finginiobra="+fecinginiobra+"&fingfinobra="+fecingfinobra+"&notseg="+notseg+"&swinfobasica="+swinfobasica+"&swinfosecundaria="+swinfosecundaria+"&swinmobiliaria="+swinmobiliaria+"&swvisitalocal="+swvisitalocal+"&swdiseno="+swdiseno+"&swlicencia="+swlicencia+"&swinterventoria="+swinterventoria+"&swconstructor="+swconstructor+"&swseguridad="+swseguridad+"&vnomcaj="+vernombrecajero+"&vdir="+verdireccion+"&vestcaj="+verestadocajero+"&vanocont="+veranocontable+"&vreg="+verregion+"&vdep="+verdepartamento+"&vmun="+vermunicipio+"&vtipol="+vertipologia+"&vinter="+verintervencion+"&vmoda="+vermodalidad+"&vcodigocajero="+vercodigocajero+"&vcentrocostos="+vercentrocostos+"&vreferencia="+verreferencia+"&vubicacion="+verubicacion+"&vcodigosuc="+vercodigosuc+"&vubicacionatm="+verubicacionatm+"&vatiende="+veratiende+"&vriesgo="+verriesgo+"&varea="+verarea+"&vcodigorecibido="+vercodigorecibido+"&vinmobiliaria="+verinmobiliaria+"&vestadoinm="+verestadoinm+"&vfechainicioinm="+verfechainicioinm+"&vfechaentregainm="+verfechaentregainm+"&vtotaldiasinm="+vertotaldiasinm+"&vnotasinm="+vernotasinm+"&vvisita="+vervisita+"&vestadovis="+verestadovis+"&vfechainiciovis="+verfechainiciovis+"&vfechaentregavis="+verfechaentregavis+"&vtotaldiasvis="+vertotaldiasvis+"&vnotasvis="+vernotasvis+"&vdisenador="+verdisenador+"&vestadodis="+verestadodis+"&vfechainiciodis="+verfechainiciodis+"&vfechaentregadis="+verfechaentregadis+"&vtotaldiasdis="+vertotaldiasdis+"&vnotasdis="+vernotasdis+"&vgestionador="+vergestionador+"&vestadolic="+verestadolic+"&vfechainiciolic="+verfechainiciolic+"&vfechaentregalic="+verfechaentregalic+"&vtotaldiaslic="+vertotaldiaslic+"&vnotaslic="+vernotaslic+"&vinterventor="+verinterventor+"&voperadorcanal="+veroperadorcanal+"&vfechainicioint="+verfechainicioint+"&vfechaentregaint="+verfechaentregaint+"&vfechapedido="+verfechapedido+"&vnotasint="+vernotasint+"&vconstructor="+verconstructor+"&vavance="+veravance+"&vfechainiciocons="+verfechainiciocons+"&vfechaentregacons="+verfechaentregacons+"&vtotaldiascons="+vertotaldiascons+"&vnotascons="+vernotascons+"&vseguridad="+verseguridad+"&vcodmon="+vercodmon+"&vnotasseg="+vernotasseg+"&vfecingobra="+verfecingobra,true);
		}
		ajax.send(null);
	}
	OCULTARSCROLL();
}
function GUARDAR(o,l,cb)
{	
	var ley = prompt("Ingrese el nombre de la busqueda", l);
	if(ley != '')
	{
        var cajeros = OBTENERSELECCIONMULTIPLE("multiselect_buscajero");//form1.buscajero.value;
		var fii = form1.fechainicio.value;
		var ffi = form1.fechafin.value;
		var fie = form1.fechainicioentrega.value;
		var ffe = form1.fechafinentrega.value;
		
		var nomcaj = form1.busnombre.value;
		var dircaj = form1.busdireccion.value;
		var est = OBTENERSELECCIONMULTIPLE("multiselect_busestado");//form1.busestado.value;
		var anocon = form1.busanocontable.value;
		var reg = OBTENERSELECCIONMULTIPLE("multiselect_busregion");//form1.busregion.value;
		var dep = OBTENERSELECCIONMULTIPLE("multiselect_busdepartamento");//form1.busdepartamento.value;
		var mun = OBTENERSELECCIONMULTIPLE("multiselect_busmunicipio");//form1.busmunicipio.value;
		var tip = OBTENERSELECCIONMULTIPLE("multiselect_bustipologia");//form1.bustipologia.value;
		var tipint = OBTENERSELECCIONMULTIPLE("multiselect_bustipointervencion");//form1.bustipointervencion.value;
		var moda = OBTENERSELECCIONMULTIPLE("multiselect_busmodalidad");//form1.busmodalidad.value;
		
		var cod = form1.buscodigo.value;
		var cencos =  OBTENERSELECCIONMULTIPLE("multiselect_buscentrocostos");//form1.buscentrocostos.value;
		var refmaq = OBTENERSELECCIONMULTIPLE("multiselect_busreferenciamaquina");//form1.busreferenciamaquina.value;
		var ubi = OBTENERSELECCIONMULTIPLE("multiselect_busubicacion");//form1.busubicacion.value;
		var codsuc = form1.buscodigosuc.value;
		var ubiatm = OBTENERSELECCIONMULTIPLE("multiselect_busubicacionamt");//form1.busubicacionamt.value;
		var ati = OBTENERSELECCIONMULTIPLE("multiselect_busatiende");//form1.busatiende.value;
		var rie = OBTENERSELECCIONMULTIPLE("multiselect_busriesgo");//form1.busriesgo.value;
		var are = OBTENERSELECCIONMULTIPLE("multiselect_busarea");//form1.busarea.value;
		var fecapagadoatm = form1.busfechaapagadoatm.value;
		
		var nominm = OBTENERSELECCIONMULTIPLE("multiselect_busnombreinmobiliaria");//form1.busnombreinmobiliaria.value;
		var estinm = OBTENERSELECCIONMULTIPLE("multiselect_busestadoinmobiliaria");//form1.busestadoinmobiliaria.value;
		var feciniinmdesde = form1.busfechainicioinmobiliaria.value;
		var feciniinmhasta = form1.busfechafininmobiliaria.value;
		var fecentinmdesde = form1.busfechaentregainiinfoinmobiliaria.value;
		var fecentinmhasta = form1.busfechaentregafininfoinmobiliaria.value;
		var diasinm = form1.bustotaldiasinmobiliaria.value;
		var notinm = form1.busnotasinmobiliaria.value;
		
		var nomvis = OBTENERSELECCIONMULTIPLE("multiselect_busnombrevisita");//form1.busnombrevisita.value;
		var estvis = OBTENERSELECCIONMULTIPLE("multiselect_busestadovisita");//form1.busestadovisita.value;
		var fecinivisdesde = form1.busfechainiciovisita.value;
		var fecinivishasta = form1.busfechafinvisita.value;
		var fecentvisdesde = form1.busfechaentregainiinfovisita.value;
		var fecentvishasta = form1.busfechaentregafininfovisita.value;
		var diasvis = form1.bustotaldiasvisita.value;
		var notvis = form1.busnotasvisita.value;
		
		var nomdis = OBTENERSELECCIONMULTIPLE("multiselect_busnombrediseno");//form1.busnombrediseno.value;
		var estdis = OBTENERSELECCIONMULTIPLE("multiselect_busestadodiseno");//form1.busestadodiseno.value;
		var fecinidisdesde = form1.busfechainiciodiseno.value;
		var fecinidishasta = form1.busfechafindiseno.value;
		var fecentdisdesde = form1.busfechaentregainiinfodiseno.value;
		var fecentdishasta = form1.busfechaentregafininfodiseno.value;
		var diasdis = form1.bustotaldiasdiseno.value;
		var notdis = form1.busnotasdiseno.value;
		
		var nomges = OBTENERSELECCIONMULTIPLE("multiselect_busnombregestionador");//form1.busnombregestionador.value;
		var estges = OBTENERSELECCIONMULTIPLE("multiselect_busestadogestionador");//form1.busestadogestionador.value;
		var fecinigesdesde = form1.busfechainiciogestionador.value;
		var fecinigeshasta = form1.busfechafingestionador.value;
		var fecentgesdesde = form1.busfechaentregainiinfogestionador.value;
		var fecentgeshasta = form1.busfechaentregafininfogestionador.value;
		var diasges = form1.bustotaldiasgestionador.value;
		var notges = form1.busnotasgestionador.value;
		
		var nomint = OBTENERSELECCIONMULTIPLE("multiselect_busnombreinterventor");//form1.busnombreinterventor.value;
		var cancom = OBTENERSELECCIONMULTIPLE("multiselect_buscanalcomunicacion");//form1.buscanalcomunicacion.value;
		var feciniintdesde = form1.busfechainiciointerventor.value;
		var feciniinthasta = form1.busfechafininterventor.value;
		var fecentintdesde = form1.busfechaentregainiinfointerventor.value;
		var fecentinthasta = form1.busfechaentregafininfointerventor.value;
		var fecinipedintdesde = form1.busfechainipedidointerventor.value;
		var fecinipedinthasta = form1.busfechafinpedidointerventor.value;
		var notint = form1.busnotasinterventor.value;
		
		var nomcons = OBTENERSELECCIONMULTIPLE("multiselect_busnombreconstructor");//form1.busnombreconstructor.value;
		var porava = OBTENERSELECCIONMULTIPLE("multiselect_busporcentajeavance");//form1.busporcentajeavance.value;
		var feciniconsdesde = form1.busfechainicioconstructor.value;
		var feciniconshasta = form1.busfechafinconstructor.value;
		var fecentconsdesde = form1.busfechaentregainiinfoconstructor.value;
		var fecentconshasta = form1.busfechaentregafininfoconstructor.value;
		var diascons = form1.bustotaldiasconstructor.value;
		var notcons = form1.busnotasconstructor.value;
		
		var nomseg = OBTENERSELECCIONMULTIPLE("multiselect_busnombreseguridad");//form1.busnombreseguridad.value;
		var codmon = form1.buscodigomonitoreo.value;
		var fecinginiobra = form1.busfechaingresoiniobra.value;
		var fecingfinobra = form1.busfechaingresofinobra.value;
		var notseg = form1.busnotasseguridad.value;
		
		var swinfobasica = form1.ocuinfobasica.value;
		var swinfosecundaria = form1.ocuinfosecundaria.value;
		var swinmobiliaria = form1.ocuinmobiliaria.value;
		var swvisitalocal = form1.ocuvisitalocal.value;
		var swdiseno = form1.ocudiseno.value;
		var swlicencia = form1.oculicencia.value;
		var swinterventoria = form1.ocuinterventoria.value;
		var swconstructor = form1.ocuconstructor.value;
		var swseguridad = form1.ocuseguridad.value;
		
		var vernombrecajero = form1.vernombrecajero.checked;
		var verdireccion = form1.verdireccion.checked;
		var verestadocajero = form1.verestadocajero.checked;
		var veranocontable = form1.veranocontable.checked;
		var verregion = form1.verregion.checked;
		var verdepartamento = form1.verdepartamento.checked;
		var vermunicipio = form1.vermunicipio.checked;
		var vertipologia = form1.vertipologia.checked;
		var verintervencion = form1.verintervencion.checked;
		var vermodalidad = form1.vermodalidad.checked;
		
		var vercodigocajero = form1.vercodigocajero.checked;
		var vercentrocostos = form1.vercentrocostos.checked;
		var verreferencia = form1.verreferencia.checked;
		var verubicacion = form1.verubicacion.checked;
		var vercodigosuc = form1.vercodigosuc.checked;
		var verubicacionatm = form1.verubicacionatm.checked;
		var veratiende = form1.veratiende.checked;
		var verriesgo = form1.verriesgo.checked;
		var verarea = form1.verarea.checked;
		var vercodigorecibido = form1.vercodigorecibido.checked;
		
		var verinmobiliaria = form1.verinmobiliaria1.checked;
		var verestadoinm = form1.verestadoinm.checked;
		var verfechainicioinm = form1.verfechainicioinm.checked;
		var verfechaentregainm = form1.verfechaentregainm.checked;
		var vertotaldiasinm = form1.vertotaldiasinm.checked;
		var vernotasinm = form1.vernotasinm.checked;
		
		var vervisita = form1.vervisita1.checked;
		var verestadovis = form1.verestadovis.checked;
		var verfechainiciovis = form1.verfechainiciovis.checked;
		var verfechaentregavis = form1.verfechaentregavis.checked;
		var vertotaldiasvis = form1.vertotaldiasvis.checked;
		var vernotasvis = form1.vernotasvis.checked;
		
		var verdisenador = form1.verdisenador.checked;
		var verestadodis = form1.verestadodis.checked;
		var verfechainiciodis = form1.verfechainiciodis.checked;
		var verfechaentregadis = form1.verfechaentregadis.checked;
		var vertotaldiasdis = form1.vertotaldiasdis.checked;
		var vernotasdis = form1.vernotasdis.checked;
		
		var vergestionador = form1.vergestionador.checked;
		var verestadolic = form1.verestadolic.checked;
		var verfechainiciolic = form1.verfechainiciolic.checked;
		var verfechaentregalic = form1.verfechaentregalic.checked;
		var vertotaldiaslic = form1.vertotaldiaslic.checked;
		var vernotaslic = form1.vernotaslic.checked;
		
		var verinterventor = form1.verinterventor.checked;
		var veroperadorcanal = form1.veroperadorcanal.checked;
		var verfechainicioint = form1.verfechainicioint.checked;
		var verfechaentregaint = form1.verfechaentregaint.checked;
		var verfechapedido = form1.verfechapedido.checked;
		var vernotasint = form1.vernotasint.checked;
		
		var verconstructor = form1.verconstructor1.checked;
		var veravance = form1.veravance.checked;
		var verfechainiciocons = form1.verfechainiciocons.checked;
		var verfechaentregacons = form1.verfechaentregacons.checked;
		var vertotaldiascons = form1.vertotaldiascons.checked;
		var vernotascons = form1.vernotascons.checked;
		
		var verseguridad = form1.verseguridad1.checked;
		var vercodmon = form1.vercodigomonitoreo.checked;
		var verfecingobra = form1.verfechaingresoobra.checked;
		var vernotasseg = form1.vernotasseg.checked;

		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('resultadoalguardar').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#resultadoalguardar").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		//ajax.open("GET","?guardar=si&caj="+cajeros+"&fii="+fii+"&ffi="+ffi+"&fie="+fie+"&ffe="+ffe+"&nomcaj="+nomcaj+"&dircaj="+dircaj+"&est="+est+"&anocon="+anocon+"&reg="+reg+"&dep="+dep+"&mun="+mun+"&tip="+tip+"&tipint="+tipint+"&moda="+moda+"&cod="+cod+"&cencos="+cencos+"&refmaq="+refmaq+"&ubi="+ubi+"&codsuc="+codsuc+"&ubiatm="+ubiatm+"&ati="+ati+"&rie="+rie+"&are="+are+"&fecapagadoatm="+fecapagadoatm+"&nominm="+nominm+"&estinm="+estinm+"&feciniinmdesde="+feciniinmdesde+"&feciniinmhasta="+feciniinmhasta+"&fecentinmdesde="+fecentinmdesde+"&fecentinmhasta="+fecentinmhasta+"&diasinm="+diasinm+"&notinm="+notinm+"&nomvis="+nomvis+"&estvis="+estvis+"&fecinivisdesde="+fecinivisdesde+"&fecinivishasta="+fecinivishasta+"&fecentvisdesde="+fecentvisdesde+"&fecentvishasta="+fecentvishasta+"&diasvis="+diasvis+"&notvis="+notvis+"&nomdis="+nomdis+"&estdis="+estdis+"&fecinidisdesde="+fecinidisdesde+"&fecinidishasta="+fecinidishasta+"&fecentdisdesde="+fecentdisdesde+"&fecentdishasta="+fecentdishasta+"&diasdis="+diasdis+"&notdis="+notdis+"&nomges="+nomges+"&estges="+estges+"&fecinigesdesde="+fecinigesdesde+"&fecinigeshasta="+fecinigeshasta+"&fecentgesdesde="+fecentgesdesde+"&fecentgeshasta="+fecentgeshasta+"&diasges="+diasges+"&notges="+notges+"&nomint="+nomint+"&cancom="+cancom+"&feciniintdesde="+feciniintdesde+"&feciniinthasta="+feciniinthasta+"&fecentintdesde="+fecentintdesde+"&fecentinthasta="+fecentinthasta+"&fecinipedintdesde="+fecinipedintdesde+"&fecinipedinthasta="+fecinipedinthasta+"&notint="+notint+"&nomcons="+nomcons+"&porava="+porava+"&feciniconsdesde="+feciniconsdesde+"&feciniconshasta="+feciniconshasta+"&fecentconsdesde="+fecentconsdesde+"&fecentconshasta="+fecentconshasta+"&diascons="+diascons+"&notcons="+notcons+"&nomseg="+nomseg+"&codmon="+codmon+"&fecinginiobra="+fecinginiobra+"&fecingfinobra="+fecingfinobra+"&notseg="+notseg+"&swinfobasica="+swinfobasica+"&swinfosecundaria="+swinfosecundaria+"&swinmobiliaria="+swinmobiliaria+"&swvisitalocal="+swvisitalocal+"&swdiseno="+swdiseno+"&swlicencia="+swlicencia+"&swinterventoria="+swinterventoria+"&swconstructor="+swconstructor+"&swseguridad="+swseguridad+"&vnomcaj="+vernombrecajero+"&vdir="+verdireccion+"&vestcaj="+verestadocajero+"&vanocont="+veranocontable+"&vreg="+verregion+"&vdep="+verdepartamento+"&vmun="+vermunicipio+"&vtipol="+vertipologia+"&vinter="+verintervencion+"&vmoda="+vermodalidad+"&vcodigocajero="+vercodigocajero+"&vcentrocostos="+vercentrocostos+"&vreferencia="+verreferencia+"&vubicacion="+verubicacion+"&vcodigosuc="+vercodigosuc+"&vubicacionatm="+verubicacionatm+"&vatiende="+veratiende+"&vriesgo="+verriesgo+"&varea="+verarea+"&vcodigorecibido="+vercodigorecibido+"&vinmobiliaria="+verinmobiliaria+"&vestadoinm="+verestadoinm+"&vfechainicioinm="+verfechainicioinm+"&vfechaentregainm="+verfechaentregainm+"&vtotaldiasinm="+vertotaldiasinm+"&vnotasinm="+vernotasinm+"&vvisita="+vervisita+"&vestadovis="+verestadovis+"&vfechainiciovis="+verfechainiciovis+"&vfechaentregavis="+verfechaentregavis+"&vtotaldiasvis="+vertotaldiasvis+"&vnotasvis="+vernotasvis+"&vdisenador="+verdisenador+"&vestadodis="+verestadodis+"&vfechainiciodis="+verfechainiciodis+"&vfechaentregadis="+verfechaentregadis+"&vtotaldiasdis="+vertotaldiasdis+"&vnotasdis="+vernotasdis+"&vgestionador="+vergestionador+"&vestadolic="+verestadolic+"&vfecinilic="+verfechainiciolic+"&vfecentlic="+verfechaentregalic+"&vtotaldiaslic="+vertotaldiaslic+"&vnotaslic="+vernotaslic+"&vinterventor="+verinterventor+"&voperadorcanal="+veroperadorcanal+"&vfeciniint="+verfechainicioint+"&vfecentint="+verfechaentregaint+"&vfecped="+verfechapedido+"&vnotasint="+vernotasint+"&vconstructor="+verconstructor+"&vavance="+veravance+"&vfecinicons="+verfechainiciocons+"&vfecentcons="+verfechaentregacons+"&vtotdiascons="+vertotaldiascons+"&vnotcons="+vernotascons+"&vseguridad="+verseguridad+"&vcodmon="+vercodmon+"&vnotasseg="+vernotasseg+"&vfecingobra="+verfecingobra+"&ley="+ley+"&opc="+o+"&clabug="+cb,true);
		//ajax.send(null);

        var formData = "guardar=si&caj="+cajeros+"&fii="+fii+"&ffi="+ffi+"&fie="+fie+"&ffe="+ffe+"&nomcaj="+nomcaj+"&dircaj="+dircaj+"&est="+est+"&anocon="+anocon+"&reg="+reg+"&dep="+dep+"&mun="+mun+"&tip="+tip+"&tipint="+tipint+"&moda="+moda+"&cod="+cod+"&cencos="+cencos+"&refmaq="+refmaq+"&ubi="+ubi+"&codsuc="+codsuc+"&ubiatm="+ubiatm+"&ati="+ati+"&rie="+rie+"&are="+are+"&fecapagadoatm="+fecapagadoatm+"&nominm="+nominm+"&estinm="+estinm+"&feciniinmdesde="+feciniinmdesde+"&feciniinmhasta="+feciniinmhasta+"&fecentinmdesde="+fecentinmdesde+"&fecentinmhasta="+fecentinmhasta+"&diasinm="+diasinm+"&notinm="+notinm+"&nomvis="+nomvis+"&estvis="+estvis+"&fecinivisdesde="+fecinivisdesde+"&fecinivishasta="+fecinivishasta+"&fecentvisdesde="+fecentvisdesde+"&fecentvishasta="+fecentvishasta+"&diasvis="+diasvis+"&notvis="+notvis+"&nomdis="+nomdis+"&estdis="+estdis+"&fecinidisdesde="+fecinidisdesde+"&fecinidishasta="+fecinidishasta+"&fecentdisdesde="+fecentdisdesde+"&fecentdishasta="+fecentdishasta+"&diasdis="+diasdis+"&notdis="+notdis+"&nomges="+nomges+"&estges="+estges+"&fecinigesdesde="+fecinigesdesde+"&fecinigeshasta="+fecinigeshasta+"&fecentgesdesde="+fecentgesdesde+"&fecentgeshasta="+fecentgeshasta+"&diasges="+diasges+"&notges="+notges+"&nomint="+nomint+"&cancom="+cancom+"&feciniintdesde="+feciniintdesde+"&feciniinthasta="+feciniinthasta+"&fecentintdesde="+fecentintdesde+"&fecentinthasta="+fecentinthasta+"&fecinipedintdesde="+fecinipedintdesde+"&fecinipedinthasta="+fecinipedinthasta+"&notint="+notint+"&nomcons="+nomcons+"&porava="+porava+"&feciniconsdesde="+feciniconsdesde+"&feciniconshasta="+feciniconshasta+"&fecentconsdesde="+fecentconsdesde+"&fecentconshasta="+fecentconshasta+"&diascons="+diascons+"&notcons="+notcons+"&nomseg="+nomseg+"&codmon="+codmon+"&fecinginiobra="+fecinginiobra+"&fecingfinobra="+fecingfinobra+"&notseg="+notseg+"&swinfobasica="+swinfobasica+"&swinfosecundaria="+swinfosecundaria+"&swinmobiliaria="+swinmobiliaria+"&swvisitalocal="+swvisitalocal+"&swdiseno="+swdiseno+"&swlicencia="+swlicencia+"&swinterventoria="+swinterventoria+"&swconstructor="+swconstructor+"&swseguridad="+swseguridad+"&vnomcaj="+vernombrecajero+"&vdir="+verdireccion+"&vestcaj="+verestadocajero+"&vanocont="+veranocontable+"&vreg="+verregion+"&vdep="+verdepartamento+"&vmun="+vermunicipio+"&vtipol="+vertipologia+"&vinter="+verintervencion+"&vmoda="+vermodalidad+"&vcodigocajero="+vercodigocajero+"&vcentrocostos="+vercentrocostos+"&vreferencia="+verreferencia+"&vubicacion="+verubicacion+"&vcodigosuc="+vercodigosuc+"&vubicacionatm="+verubicacionatm+"&vatiende="+veratiende+"&vriesgo="+verriesgo+"&varea="+verarea+"&vcodigorecibido="+vercodigorecibido+"&vinmobiliaria="+verinmobiliaria+"&vestadoinm="+verestadoinm+"&vfechainicioinm="+verfechainicioinm+"&vfechaentregainm="+verfechaentregainm+"&vtotaldiasinm="+vertotaldiasinm+"&vnotasinm="+vernotasinm+"&vvisita="+vervisita+"&vestadovis="+verestadovis+"&vfechainiciovis="+verfechainiciovis+"&vfechaentregavis="+verfechaentregavis+"&vtotaldiasvis="+vertotaldiasvis+"&vnotasvis="+vernotasvis+"&vdisenador="+verdisenador+"&vestadodis="+verestadodis+"&vfechainiciodis="+verfechainiciodis+"&vfechaentregadis="+verfechaentregadis+"&vtotaldiasdis="+vertotaldiasdis+"&vnotasdis="+vernotasdis+"&vgestionador="+vergestionador+"&vestadolic="+verestadolic+"&vfecinilic="+verfechainiciolic+"&vfecentlic="+verfechaentregalic+"&vtotaldiaslic="+vertotaldiaslic+"&vnotaslic="+vernotaslic+"&vinterventor="+verinterventor+"&voperadorcanal="+veroperadorcanal+"&vfeciniint="+verfechainicioint+"&vfecentint="+verfechaentregaint+"&vfecped="+verfechapedido+"&vnotasint="+vernotasint+"&vconstructor="+verconstructor+"&vavance="+veravance+"&vfecinicons="+verfechainiciocons+"&vfecentcons="+verfechaentregacons+"&vtotdiascons="+vertotaldiascons+"&vnotcons="+vernotascons+"&vseguridad="+verseguridad+"&vcodmon="+vercodmon+"&vnotasseg="+vernotasseg+"&vfecingobra="+verfecingobra+"&ley="+ley+"&opc="+o+"&clabug="+cb;

        $.ajax({
            url : "cajerossineliminar.php",
            type: "POST",
            data : formData,
            success: function(data, textStatus, jqXHR)
            {
                //data - response from server
                document.getElementById('resultadoalguardar').innerHTML=data;//ajax.responseText;
            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
	}
	else
	{
		alert('Debe ingresar el nombre de la busqueda. Proceso cancelado');
	}
}
function BUSQUEDAGUARDADA(v)
{
	if(v == '' || v == null)
	{
		BUSCAR('CAJERO','');
	}
	else
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('busqueda').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#busqueda").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?busquedaguardada=si&clabug="+v,true);
		ajax.send(null);
		OCULTARSCROLL();
		MOSTRARFILTRO(v);
		REFRESCARBUSQUEDACAJEROS();
		setTimeout("REFRESCARBUSQUEDACAJEROS()",1000);
		setTimeout("REFRESCARBUSQUEDACAJEROS()",1500);
		setTimeout("REFRESCARBUSQUEDACAJEROS()",2000);
		setTimeout("REFRESCARBUSQUEDACAJEROS()",2500);
		setTimeout("REFRESCARBUSQUEDACAJEROS()",3000);
		setTimeout("REFRESCARBUSQUEDACAJEROS()",3500);
		setTimeout("REFRESCARBUSQUEDACAJEROS()",4000);
		setTimeout("REFRESCARBUSQUEDACAJEROS()",4500);
		setTimeout("REFRESCARBUSQUEDACAJEROS()",5000);
		setTimeout("REFRESCARBUSQUEDACAJEROS()",5500);
		setTimeout("REFRESCARBUSQUEDACAJEROS()",6000);
		setTimeout("REFRESCARBUSQUEDACAJEROS()",6500);
		setTimeout("REFRESCARBUSQUEDACAJEROS()",7000);
		setTimeout("REFRESCARBUSQUEDACAJEROS()",7500);
		setTimeout("REFRESCARBUSQUEDACAJEROS()",8000);
	}
}
function MOSTRARFILTRO(v)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('cajeros').innerHTML=ajax.responseText;
            refreshSelect();
	    }
	}
	jQuery("#cajeros").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarfiltro=si&clabug="+v,true);
	ajax.send(null);
	OCULTARSCROLL();
	REFRESCARBUSQUEDACAJEROS();
	setTimeout("REFRESCARBUSQUEDACAJEROS()",1000);
	setTimeout("REFRESCARBUSQUEDACAJEROS()",1500);
	setTimeout("REFRESCARBUSQUEDACAJEROS()",2000);
	setTimeout("REFRESCARBUSQUEDACAJEROS()",2500);
	setTimeout("REFRESCARBUSQUEDACAJEROS()",3000);
}
function ELIMINARBUSQUEDA(v)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('resultadoalguardar').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#resultadoalguardar").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?eliminarbusqueda=si&clabug="+v,true);
	ajax.send(null);
}
function VERDEPARTAMENTOS(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('departamentos').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#departamentos").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?verdepartamentos=si&reg="+v,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTADEPARTAMENTOS()",800);
}
function VERCIUDADES(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('ciudades').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#ciudades").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?verciudades=si&dep="+v,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTACIUDADES()",800);

}

function EXPORTAR()
{
    var cajeros = OBTENERSELECCIONMULTIPLE("multiselect_buscajero");//form1.buscajero.value;
	var fii = form1.fechainicio.value;
	var ffi = form1.fechafin.value;
	var fie = form1.fechainicioentrega.value;
	var ffe = form1.fechafinentrega.value;
	
	var nomcaj = form1.busnombre.value;
	var dircaj = form1.busdireccion.value;
	var est = OBTENERSELECCIONMULTIPLE("multiselect_busestado");//form1.busestado.value;
	var anocon = form1.busanocontable.value;
	var reg = OBTENERSELECCIONMULTIPLE("multiselect_busregion");//form1.busregion.value;
	var dep = OBTENERSELECCIONMULTIPLE("multiselect_busdepartamento");//form1.busdepartamento.value;
	var mun = OBTENERSELECCIONMULTIPLE("multiselect_busmunicipio");//form1.busmunicipio.value;
	var tip = OBTENERSELECCIONMULTIPLE("multiselect_bustipologia");//form1.bustipologia.value;
	var tipint = OBTENERSELECCIONMULTIPLE("multiselect_bustipointervencion");//form1.bustipointervencion.value;
	var moda = OBTENERSELECCIONMULTIPLE("multiselect_busmodalidad");//form1.busmodalidad.value;
	
	var cod = form1.buscodigo.value;
	var cencos =  OBTENERSELECCIONMULTIPLE("multiselect_buscentrocostos");//form1.buscentrocostos.value;
	var refmaq = OBTENERSELECCIONMULTIPLE("multiselect_busreferenciamaquina");//form1.busreferenciamaquina.value;
	var ubi = OBTENERSELECCIONMULTIPLE("multiselect_busubicacion");//form1.busubicacion.value;
	var codsuc = form1.buscodigosuc.value;
	var ubiatm = OBTENERSELECCIONMULTIPLE("multiselect_busubicacionamt");//form1.busubicacionamt.value;
	var ati = OBTENERSELECCIONMULTIPLE("multiselect_busatiende");//form1.busatiende.value;
	var rie = OBTENERSELECCIONMULTIPLE("multiselect_busriesgo");//form1.busriesgo.value;
	var are = OBTENERSELECCIONMULTIPLE("multiselect_busarea");//form1.busarea.value;
	var fecapagadoatm = form1.busfechaapagadoatm.value;
	
	var nominm = OBTENERSELECCIONMULTIPLE("multiselect_busnombreinmobiliaria");//form1.busnombreinmobiliaria.value;
	var estinm = OBTENERSELECCIONMULTIPLE("multiselect_busestadoinmobiliaria");//form1.busestadoinmobiliaria.value;
	var feciniinmdesde = form1.busfechainicioinmobiliaria.value;
	var feciniinmhasta = form1.busfechafininmobiliaria.value;
	var fecentinmdesde = form1.busfechaentregainiinfoinmobiliaria.value;
	var fecentinmhasta = form1.busfechaentregafininfoinmobiliaria.value;
	var diasinm = form1.bustotaldiasinmobiliaria.value;
	var notinm = form1.busnotasinmobiliaria.value;
	
	var nomvis = OBTENERSELECCIONMULTIPLE("multiselect_busnombrevisita");//form1.busnombrevisita.value;
	var estvis = OBTENERSELECCIONMULTIPLE("multiselect_busestadovisita");//form1.busestadovisita.value;
	var fecinivisdesde = form1.busfechainiciovisita.value;
	var fecinivishasta = form1.busfechafinvisita.value;
	var fecentvisdesde = form1.busfechaentregainiinfovisita.value;
	var fecentvishasta = form1.busfechaentregafininfovisita.value;
	var diasvis = form1.bustotaldiasvisita.value;
	var notvis = form1.busnotasvisita.value;
	
	var nomdis = OBTENERSELECCIONMULTIPLE("multiselect_busnombrediseno");//form1.busnombrediseno.value;
	var estdis = OBTENERSELECCIONMULTIPLE("multiselect_busestadodiseno");//form1.busestadodiseno.value;
	var fecinidisdesde = form1.busfechainiciodiseno.value;
	var fecinidishasta = form1.busfechafindiseno.value;
	var fecentdisdesde = form1.busfechaentregainiinfodiseno.value;
	var fecentdishasta = form1.busfechaentregafininfodiseno.value;
	var diasdis = form1.bustotaldiasdiseno.value;
	var notdis = form1.busnotasdiseno.value;
	
	var nomges = OBTENERSELECCIONMULTIPLE("multiselect_busnombregestionador");//form1.busnombregestionador.value;
	var estges = OBTENERSELECCIONMULTIPLE("multiselect_busestadogestionador");//form1.busestadogestionador.value;
	var fecinigesdesde = form1.busfechainiciogestionador.value;
	var fecinigeshasta = form1.busfechafingestionador.value;
	var fecentgesdesde = form1.busfechaentregainiinfogestionador.value;
	var fecentgeshasta = form1.busfechaentregafininfogestionador.value;
	var diasges = form1.bustotaldiasgestionador.value;
	var notges = form1.busnotasgestionador.value;
	
	var nomint = OBTENERSELECCIONMULTIPLE("multiselect_busnombreinterventor");//form1.busnombreinterventor.value;
	var cancom = OBTENERSELECCIONMULTIPLE("multiselect_buscanalcomunicacion");//form1.buscanalcomunicacion.value;
	var feciniintdesde = form1.busfechainiciointerventor.value;
	var feciniinthasta = form1.busfechafininterventor.value;
	var fecentintdesde = form1.busfechaentregainiinfointerventor.value;
	var fecentinthasta = form1.busfechaentregafininfointerventor.value;
	var fecinipedintdesde = form1.busfechainipedidointerventor.value;
	var fecinipedinthasta = form1.busfechafinpedidointerventor.value;
	var notint = form1.busnotasinterventor.value;
	
	var nomcons = OBTENERSELECCIONMULTIPLE("multiselect_busnombreconstructor");//form1.busnombreconstructor.value;
	var porava = OBTENERSELECCIONMULTIPLE("multiselect_busporcentajeavance");//form1.busporcentajeavance.value;
	var feciniconsdesde = form1.busfechainicioconstructor.value;
	var feciniconshasta = form1.busfechafinconstructor.value;
	var fecentconsdesde = form1.busfechaentregainiinfoconstructor.value;
	var fecentconshasta = form1.busfechaentregafininfoconstructor.value;
	var diascons = form1.bustotaldiasconstructor.value;
	var notcons = form1.busnotasconstructor.value;
	
	var nomseg = OBTENERSELECCIONMULTIPLE("multiselect_busnombreseguridad");//form1.busnombreseguridad.value;
	var codmon = form1.buscodigomonitoreo.value;
	var fecinginiobra = form1.busfechaingresoiniobra.value;
	var fecingfinobra = form1.busfechaingresofinobra.value;
	var notseg = form1.busnotasseguridad.value;
	
	var swinfobasica = form1.ocuinfobasica.value;
	var swinfosecundaria = form1.ocuinfosecundaria.value;
	var swinmobiliaria = form1.ocuinmobiliaria.value;
	var swvisitalocal = form1.ocuvisitalocal.value;
	var swdiseno = form1.ocudiseno.value;
	var swlicencia = form1.oculicencia.value;
	var swinterventoria = form1.ocuinterventoria.value;
	var swconstructor = form1.ocuconstructor.value;
	var swseguridad = form1.ocuseguridad.value;
	
	var vernombrecajero = form1.vernombrecajero.checked;
	var verdireccion = form1.verdireccion.checked;
	var verestadocajero = form1.verestadocajero.checked;
	var veranocontable = form1.veranocontable.checked;
	var verregion = form1.verregion.checked;
	var verdepartamento = form1.verdepartamento.checked;
	var vermunicipio = form1.vermunicipio.checked;
	var vertipologia = form1.vertipologia.checked;
	var verintervencion = form1.verintervencion.checked;
	var vermodalidad = form1.vermodalidad.checked;
	
	var vercodigocajero = form1.vercodigocajero.checked;
	var vercentrocostos = form1.vercentrocostos.checked;
	var verreferencia = form1.verreferencia.checked;
	var verubicacion = form1.verubicacion.checked;
	var vercodigosuc = form1.vercodigosuc.checked;
	var verubicacionatm = form1.verubicacionatm.checked;
	var veratiende = form1.veratiende.checked;
	var verriesgo = form1.verriesgo.checked;
	var verarea = form1.verarea.checked;
	var vercodigorecibido = form1.vercodigorecibido.checked;
	
	var verinmobiliaria = form1.verinmobiliaria1.checked;
	var verestadoinm = form1.verestadoinm.checked;
	var verfechainicioinm = form1.verfechainicioinm.checked;
	var verfechaentregainm = form1.verfechaentregainm.checked;
	var vertotaldiasinm = form1.vertotaldiasinm.checked;
	var vernotasinm = form1.vernotasinm.checked;
	
	var vervisita = form1.vervisita1.checked;
	var verestadovis = form1.verestadovis.checked;
	var verfechainiciovis = form1.verfechainiciovis.checked;
	var verfechaentregavis = form1.verfechaentregavis.checked;
	var vertotaldiasvis = form1.vertotaldiasvis.checked;
	var vernotasvis = form1.vernotasvis.checked;
	
	var verdisenador = form1.verdisenador.checked;
	var verestadodis = form1.verestadodis.checked;
	var verfechainiciodis = form1.verfechainiciodis.checked;
	var verfechaentregadis = form1.verfechaentregadis.checked;
	var vertotaldiasdis = form1.vertotaldiasdis.checked;
	var vernotasdis = form1.vernotasdis.checked;
	
	var vergestionador = form1.vergestionador.checked;
	var verestadolic = form1.verestadolic.checked;
	var verfechainiciolic = form1.verfechainiciolic.checked;
	var verfechaentregalic = form1.verfechaentregalic.checked;
	var vertotaldiaslic = form1.vertotaldiaslic.checked;
	var vernotaslic = form1.vernotaslic.checked;
	
	var verinterventor = form1.verinterventor.checked;
	var veroperadorcanal = form1.veroperadorcanal.checked;
	var verfechainicioint = form1.verfechainicioint.checked;
	var verfechaentregaint = form1.verfechaentregaint.checked;
	var verfechapedido = form1.verfechapedido.checked;
	var vernotasint = form1.vernotasint.checked;
	
	var verconstructor = form1.verconstructor1.checked;
	var veravance = form1.veravance.checked;
	var verfechainiciocons = form1.verfechainiciocons.checked;
	var verfechaentregacons = form1.verfechaentregacons.checked;
	var vertotaldiascons = form1.vertotaldiascons.checked;
	var vernotascons = form1.vernotascons.checked;
	
	var verseguridad = form1.verseguridad1.checked;
	var vercodmon = form1.vercodigomonitoreo.checked;
	var verfecingobra = form1.verfechaingresoobra.checked;
	var vernotasseg = form1.vernotasseg.checked;
	
	window.location.href = "informes/informeexcel.php?caj="+cajeros+"&fii="+fii+"&ffi="+ffi+"&fie="+fie+"&ffe="+ffe+"&nomcaj="+nomcaj+"&dircaj="+dircaj+"&est="+est+"&anocon="+anocon+"&reg="+reg+"&dep="+dep+"&mun="+mun+"&tip="+tip+"&tipint="+tipint+"&moda="+moda+"&cod="+cod+"&cencos="+cencos+"&refmaq="+refmaq+"&ubi="+ubi+"&codsuc="+codsuc+"&ubiatm="+ubiatm+"&ati="+ati+"&rie="+rie+"&are="+are+"&fecapagadoatm="+fecapagadoatm+"&nominm="+nominm+"&estinm="+estinm+"&feciniinmdesde="+feciniinmdesde+"&feciniinmhasta="+feciniinmhasta+"&fecentinmdesde="+fecentinmdesde+"&fecentinmhasta="+fecentinmhasta+"&diasinm="+diasinm+"&notinm="+notinm+"&nomvis="+nomvis+"&estvis="+estvis+"&fecinivisdesde="+fecinivisdesde+"&fecinivishasta="+fecinivishasta+"&fecentvisdesde="+fecentvisdesde+"&fecentvishasta="+fecentvishasta+"&diasvis="+diasvis+"&notvis="+notvis+"&nomdis="+nomdis+"&estdis="+estdis+"&fecinidisdesde="+fecinidisdesde+"&fecinidishasta="+fecinidishasta+"&fecentdisdesde="+fecentdisdesde+"&fecentdishasta="+fecentdishasta+"&diasdis="+diasdis+"&notdis="+notdis+"&nomges="+nomges+"&estges="+estges+"&fecinigesdesde="+fecinigesdesde+"&fecinigeshasta="+fecinigeshasta+"&fecentgesdesde="+fecentgesdesde+"&fecentgeshasta="+fecentgeshasta+"&diasges="+diasges+"&notges="+notges+"&nomint="+nomint+"&cancom="+cancom+"&feciniintdesde="+feciniintdesde+"&feciniinthasta="+feciniinthasta+"&fecentintdesde="+fecentintdesde+"&fecentinthasta="+fecentinthasta+"&fecinipedintdesde="+fecinipedintdesde+"&fecinipedinthasta="+fecinipedinthasta+"&notint="+notint+"&nomcons="+nomcons+"&porava="+porava+"&feciniconsdesde="+feciniconsdesde+"&feciniconshasta="+feciniconshasta+"&fecentconsdesde="+fecentconsdesde+"&fecentconshasta="+fecentconshasta+"&diascons="+diascons+"&notcons="+notcons+"&nomseg="+nomseg+"&codmon="+codmon+"&finginiobra="+fecinginiobra+"&fingfinobra="+fecingfinobra+"&notseg="+notseg+"&swinfobasica="+swinfobasica+"&swinfosecundaria="+swinfosecundaria+"&swinmobiliaria="+swinmobiliaria+"&swvisitalocal="+swvisitalocal+"&swdiseno="+swdiseno+"&swlicencia="+swlicencia+"&swinterventoria="+swinterventoria+"&swconstructor="+swconstructor+"&swseguridad="+swseguridad+"&vnomcaj="+vernombrecajero+"&vdir="+verdireccion+"&vestcaj="+verestadocajero+"&vanocont="+veranocontable+"&vreg="+verregion+"&vdep="+verdepartamento+"&vmun="+vermunicipio+"&vtipol="+vertipologia+"&vinter="+verintervencion+"&vmoda="+vermodalidad+"&vcodigocajero="+vercodigocajero+"&vcentrocostos="+vercentrocostos+"&vreferencia="+verreferencia+"&vubicacion="+verubicacion+"&vcodigosuc="+vercodigosuc+"&vubicacionatm="+verubicacionatm+"&vatiende="+veratiende+"&vriesgo="+verriesgo+"&varea="+verarea+"&vcodigorecibido="+vercodigorecibido+"&vinmobiliaria="+verinmobiliaria+"&vestadoinm="+verestadoinm+"&vfechainicioinm="+verfechainicioinm+"&vfechaentregainm="+verfechaentregainm+"&vtotaldiasinm="+vertotaldiasinm+"&vnotasinm="+vernotasinm+"&vvisita="+vervisita+"&vestadovis="+verestadovis+"&vfechainiciovis="+verfechainiciovis+"&vfechaentregavis="+verfechaentregavis+"&vtotaldiasvis="+vertotaldiasvis+"&vnotasvis="+vernotasvis+"&vdisenador="+verdisenador+"&vestadodis="+verestadodis+"&vfechainiciodis="+verfechainiciodis+"&vfechaentregadis="+verfechaentregadis+"&vtotaldiasdis="+vertotaldiasdis+"&vnotasdis="+vernotasdis+"&vgestionador="+vergestionador+"&vestadolic="+verestadolic+"&vfechainiciolic="+verfechainiciolic+"&vfechaentregalic="+verfechaentregalic+"&vtotaldiaslic="+vertotaldiaslic+"&vnotaslic="+vernotaslic+"&vinterventor="+verinterventor+"&voperadorcanal="+veroperadorcanal+"&vfechainicioint="+verfechainicioint+"&vfechaentregaint="+verfechaentregaint+"&vfechapedido="+verfechapedido+"&vnotasint="+vernotasint+"&vconstructor="+verconstructor+"&vavance="+veravance+"&vfechainiciocons="+verfechainiciocons+"&vfechaentregacons="+verfechaentregacons+"&vtotaldiascons="+vertotaldiascons+"&vnotascons="+vernotascons+"&vseguridad="+verseguridad+"&vcodmon="+vercodmon+"&vnotasseg="+vernotasseg+"&vfecingobra="+verfecingobra;
}
function VERMODALIDADES(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('modalidades').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#modalidades").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?vermodalidades=si&tii="+v,true);
	ajax.send(null);
	setTimeout("REFRESCARLISTAMODALIDADES()",800);
}


function OBTENERSELECCIONMULTIPLE(control)
{
    var objCBarray = document.getElementsByName(control);
    valores = "";
    for (i = 0; i < objCBarray.length; i++)
    {
        if (objCBarray[i].checked && objCBarray[i].value != "")
        {
            valores += objCBarray[i].value + ",";
        }
    }

    return valores;
}