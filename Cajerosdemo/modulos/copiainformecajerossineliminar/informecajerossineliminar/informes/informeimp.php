<?php
	include('../../../data/Conexion.php');
	
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_SESSION['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];
		
	// verifica si no se ha loggeado
	if(!isset($_SESSION["persona"]))
	{
	  session_destroy();
	  header("LOCATION:index.php");
	}else{
	}
	
	$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
	$dato = mysqli_fetch_array($con);
	$perfil = $dato['prf_descripcion'];

	$lista = $_GET['lista'];
	$feceje = $_GET['feceje'];
	$contra = $_GET['contra'];
	$sql = mysqli_query($conectar,"select * from programacion p inner join usuario u on (u.usu_clave_int = p.usu_clave_int) inner join frente f on (f.fre_clave_int = p.fre_clave_int) inner join actividad a on (a.act_clave_int = p.act_clave_int) inner join ubicacion ubi on (ubi.ubi_clave_int = p.ubi_clave_int) where p.pro_estado in (0,1,2) and p.pro_clave_int in ($lista) group by u.usu_clave_int");
	$num = mysqli_num_rows($sql);
	if($num > 1 && $perfil <> 'Administrador' && $perfil <> 'Residente')
	{
		echo "<script>alert('No puede imprimir mas de un contratista a la ves');window.location.href = '../informe.php'</script>";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title>INFORME TICKET</title>
<script>
function cerrar() { window.location.href = '../informe.php'; /*window.close();*/ }
</script>
<style type="text/css">
@page
{
size:landscape;
margin:2cm;
}
.auto-style1 {
	border-style: solid;
	border-width: 1px;
	text-align: center;
	font-family: Calibri;
	background-color: #D8D8D8;
}
.auto-style3 {
	color: #CC3000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: small;
}
.auto-style5 {
	font-family: Arial, Helvetica, sans-serif;
	border-left-style: solid;
	border-left-width: 1px;
	border-right-style: solid;
	border-right-width: 1px;
	border-top-style: solid;
	border-top-width: 1px;
	border-bottom-style: none;
	border-bottom-width: medium;
}
.auto-style7 {
	border-style: solid;
	border-width: 1px;
	color: #FF0000;
	background-color: #D8D8D8;
	font-family: Calibri;
}
.auto-style8 {
	border-style: solid;
	border-width: 1px;
	background-color: #D8D8D8;
}
.auto-style11 {
	border-style: solid;
	border-width: 1px;
	font-family: Arial, Helvetica, sans-serif;
	font-size: small;
}
.auto-style12 {
	border-style: solid;
	border-width: 1px;
}
.auto-style13 {
	border-style: solid;
	border-width: 1px;
	text-align: center;
	background-color: #D8D8D8;
}
.auto-style14 {
	border-width: 0px;
}
.auto-style15 {
	border-bottom: 1px solid #000;
	text-align: center;
	background-color: #D8D8D8;
	border-left-style: solid;
	border-left-width: 1px;
	border-right-style: solid;
	border-right-width: 1px;
	border-top-style: solid;
	border-top-width: 1px;
	font-family: Calibri;
}
.auto-style18 {
	border-style: solid;
	border-width: 1px;
	text-align: center;
	font-size: small;
	font-family: Calibri;
}
.auto-style20 {
	border-style: solid;
	border-width: 1px;
	text-align: center;
	background-color: #D8D8D8;
	font-family: Calibri;
}
.auto-style23 {
	border-right: medium none #000;
	border-bottom: medium none #000;
	border-left-style: none;
	border-left-width: medium;
	border-top-style: none;
	border-top-width: medium;
}
.auto-style24 {
	border-right: 0.5px solid #000;
	border-bottom: 1px solid #000;
	border-left-style: solid;
	border-left-width: 1px;
	border-top-style: solid;
	border-top-width: 1px;
}
.auto-style25 {
	border-right: 0.5px solid #000;
	border-bottom: medium none #000;
	border-left-style: solid;
	border-left-width: 1px;
	border-top-style: solid;
	border-top-width: 1px;
}
.auto-style26 {
	border-right: 0.5px solid #000;
	border-bottom: medium none #000;
	border-left-style: solid;
	border-left-width: 1px;
	border-top-style: none;
	border-top-width: medium;
}
.auto-style29 {
	border-top-style: solid;
	border-top-width: 1px;
}
.auto-style31 {
	border-right: 0.5px solid #000;
	border-bottom: 1px solid #000;
	border-left-style: solid;
	border-left-width: 1px;
	border-top-style: none;
	border-top-width: medium;
}
.auto-style32 {
	border-top: 0.5px solid #000;
	border-bottom-style: solid;
	border-bottom-width: 1px;
}
.auto-style33 {
	border-bottom-style: none;
	border-bottom-width: medium;
}
.auto-style34 {
	border-top-style: solid;
	border-top-width: 1px;
	border-bottom-style: none;
	border-bottom-width: medium;
}
.auto-style35 {
	border-bottom: medium none #000;
	border-left-style: solid;
	border-left-width: 1px;
	border-right-style: solid;
	border-right-width: 1px;
	border-top-style: solid;
	border-top-width: 1px;
	font-size: small;
}
.auto-style36 {
	font-family: Calibri;
	font-size: small;
}
.auto-style37 {
	border-style: solid;
	border-width: 1px;
	text-align: center;
}
.auto-style38 {
	border-style: solid;
	border-width: 1px;
	font-size: small;
}
.auto-style39 {
	border-width: 0px;
	font-size: small;
}
</style>
</head>
<body onload="window.print();cerrar();">
<?php
?>
<table style="width: 110%; border-spacing: 0px;" class="auto-style14">
	<tr>
		<td style="height: 47px; width: 111px; border-bottom: 0.5px solid #000; border-right: 0.5px solid #000" class="auto-style12">
		<table style="width: 100%">
			<tr>
				<td>
		<img src="convel.png" alt="" height="72" width="174" /></td>
				<td><img src="../../../images/myt.png" /></td>
			</tr>
		</table>
		</td>
		<td style="height: 47px; border-bottom: 0.5px solid #000;" class="auto-style1"><strong>PROGRAMACIÓN DIARIA 
		CONJUNTO INMOBILIARIO CALLE 50 SUR</strong></td>
	</tr>
	<tr>
		<td style="height: 25px; border-bottom: 0.5px solid #000;" class="auto-style13" colspan="2">
		<span class="auto-style3"><strong>Complementar a los compañeros de 
		equipo se antepone a competir con ellos. John Maxwell</strong></span></td>
	</tr>
	<tr>
		<td class="auto-style11" style="border-bottom: 0.5px solid #000; border-right: 0.5px solid #000">
		<strong>FECHA:</strong></td>
		<td class="auto-style38" style="border-bottom: 0.5px solid #000;">
		<?php 
			if($feceje == '' || $feceje == 'Ejecución')
			{
				echo "Multiples Fechas";
			}
			else
			{
				echo $feceje;
			}
		?>
		</td>
	</tr>
	<tr>
		<td class="auto-style11" style="border-bottom: 0.5px solid #000; border-right: 0.5px solid #000">
		<strong>CONTRATISTA:</strong></td>
		<td class="auto-style35">
		<?php 
			if($contra == '' || $contra == 'Contratista')
			{
				echo "Multiples Contratistas";
			}
			else
			{
				$sql = mysqli_query($conectar,"select * from programacion p inner join usuario u on (u.usu_clave_int = p.usu_clave_int) where p.pro_clave_int in ($lista) limit 1");
				$dato = mysqli_fetch_array($sql);
				$usu = $dato['usu_nombre'];
				echo $usu;
			}
		?>
		</td>
	</tr>
	<tr>
		<td class="auto-style5" colspan="2">
		<table style="width: 100%; border-spacing: 0px" class="auto-style39">
			<tr>
				<td class="auto-style1" style="border-right: 0.5px solid #000" colspan="2"><strong>ACTIVIDADES A EJECUTAR</strong></td>
				<td class="auto-style1" style="border-right: 0.5px solid #000">
				<strong>UBICACIÓN</strong></td>
				<td class="auto-style1" style="border-right: 0.5px solid #000">
				<strong>LOCALIZACIÓN</strong></td>
				<td class="auto-style20" colspan="2" style="border-right: 0.5px solid #000"><strong>CUMPLIÓ</strong></td>
				<td class="auto-style20"><strong>RAZÓN</strong></td>
				<td class="auto-style20"><strong>DESC. RAZÓN</strong></td>
				<td class="auto-style20"><strong>OBSERVACIÓN</strong></td>
			</tr>
			<tr>
				<td class="auto-style1" style="border-bottom: 0.5px solid #000; border-top: 0.5px solid #000; border-right: 0.5px solid #000; width: 19px;">
				<strong>N°</strong></td>
				<td class="auto-style7" style="border-bottom: 0.5px solid #000; border-top: 0.5px solid #000; border-right: 0.5px solid #000"><strong>ESTRUCTURA</strong></td>
				<td class="auto-style7" style="border-bottom: 0.5px solid #000; border-top: 0.5px solid #000; border-right: 0.5px solid #000">
				&nbsp;</td>
				<td class="auto-style7" style="border-bottom: 0.5px solid #000; border-top: 0.5px solid #000; border-right: 0.5px solid #000">
				&nbsp;</td>
				<td class="auto-style20" style="width: 30px; border-bottom: 0.5px solid #000; border-top: 0.5px solid #000; border-right: 0.5px solid #000"><strong>SI</strong></td>
				<td class="auto-style20" style="width: 30px; border-bottom: 0.5px solid #000; border-top: 0.5px solid #000; border-right: 0.5px solid #000"><strong>NO</strong></td>
				<td class="auto-style8" style="border-bottom: 0.5px solid #000; border-top: 0.5px solid #000">
				&nbsp;</td>
				<td class="auto-style8" style="border-bottom: 0.5px solid #000; border-top: 0.5px solid #000">
				&nbsp;</td>
				<td class="auto-style8" style="border-bottom: 0.5px solid #000; border-top: 0.5px solid #000">
				&nbsp;</td>
			</tr>
			<?php
				//CONSULTO LAS PROGRAMACIONES DEL DÍA PARA MOSTRARLAS EN EXCEL

				$sql = mysqli_query($conectar,"select * from programacion pro inner join actividad act on (act.act_clave_int = pro.act_clave_int) left outer join razon raz on (raz.raz_clave_int = pro.raz_clave_int) where pro.pro_clave_int in ($lista)");
				
				$num = mysqli_num_rows($sql);
				for($i = 0; $i < $num; $i++)
				{
					$dato=mysqli_fetch_array($sql);
					$pri = $dato['pro_sw_prioridad'];
					if($i % 2 == 0)
					{
			?>
						<tr>
							<td class="auto-style12" style="border-right: 0.5px solid #000; border-bottom: 0.5px solid #000; width: 19px;">
							<?php if($pri == 1){ echo "<strong>".$dato['pro_clave_int']."</strong>"; }else{ echo $dato['pro_clave_int']; } ?></td>
							<td class="auto-style12" style="border-right: 0.5px solid #000; border-bottom: 0.5px solid #000">
							<?php if($pri == 1){ echo "<strong>".$dato['act_nombre']."</strong>"; }else{ echo $dato['act_nombre']; } ?> </td>
							<td class="auto-style12" style="border-right: 0.5px solid #000; border-bottom: 0.5px solid #000">
							<?php
								$sql1 = mysqli_query($conectar,"select * from ubicacion where ubi_clave_int = '".$dato['ubi_clave_int']."'");
								$datoubi = mysqli_fetch_array($sql1);
								if($pri == 1){ echo "<strong>".$datoubi['ubi_descripcion']."</strong>"; }else{ echo $datoubi['ubi_descripcion']; }
							?>
							</td>
							<td class="auto-style12" style="border-right: 0.5px solid #000; border-bottom: 0.5px solid #000">
							<?php if($pri == 1){ echo "<strong>".$dato['pro_localizacion']."</strong>"; }else{ echo $dato['pro_localizacion']; } ?></td>
							<td class="auto-style37" style="width: 30px; border-right: 0.5px solid #000; border-bottom: 0.5px solid #000">
							<?php
							if($dato['pro_estado'] == 1)
							{
								if($pri == 1){ echo "<strong>X</strong>"; }else{ echo 'X'; }
							}
							?>
							</td>
							<td style="width: 30px; border-right: 0.5px solid #000; border-bottom: 0.5px solid #000" class="auto-style37">
							<?php
							if($dato['pro_estado'] == 2)
							{
								if($pri == 1){ echo "<strong>X</strong>"; }else{ echo 'X'; }
							}
							?>
							</td>
							<td class="auto-style12" style="border-bottom: 0.5px solid #000">
							<?php 
								if($dato['pro_estado'] == 2)
								{
									if($pri == 1){ echo "<strong>".wordwrap($dato['raz_nombre'], 30, "<br>", true)."</strong>"; }else{ echo wordwrap($dato['raz_nombre'], 30, "<br>", true); }
								}
							?>
							</td>
							<td class="auto-style12" style="border-bottom: 0.5px solid #000">
							<?php 
								if($dato['pro_estado'] == 2)
								{
									if($pri == 1){ echo "<strong>".wordwrap($dato['pro_no_cumplida'], 30, "<br>", true)."</strong>"; }else{ echo wordwrap($dato['pro_no_cumplida'], 30, "<br>", true); }
								}
							?></td>
							<td class="auto-style12" style="border-bottom: 0.5px solid #000">
							<?php
								if($pri == 1){ echo "<strong>".wordwrap($dato['pro_observacion'], 30, "<br>", true)."</strong>"; }else{ echo wordwrap($dato['pro_observacion'], 30, "<br>", true); }
							?>
							</td>
						</tr>
			<?php
					}
					else
					{
			?>
						<tr>
							<td class="auto-style12" style="border-right: 0.5px solid #000; border-bottom: 0.5px solid #000; width: 19px;">
							<?php if($pri == 1){ echo "<strong>".$dato['pro_clave_int']."</strong>"; }else{ echo $dato['pro_clave_int']; } ?></td>
							<td class="auto-style12" style="border-right: 0.5px solid #000; border-bottom: 0.5px solid #000">
							<?php if($pri == 1){ echo "<strong>".$dato['act_nombre']."</strong>"; }else{ echo $dato['act_nombre']; } ?></td>
							<td class="auto-style12" style="border-right: 0.5px solid #000; border-bottom: 0.5px solid #000">
							<?php
								$sql1 = mysqli_query($conectar,"select * from ubicacion where ubi_clave_int = '".$dato['ubi_clave_int']."'");
								$datoubi = mysqli_fetch_array($sql1);
								if($pri == 1){ echo "<strong>".$datoubi['ubi_descripcion']."</strong>"; }else{ echo $datoubi['ubi_descripcion']; }
							?>
							</td>
							<td class="auto-style12" style="border-right: 0.5px solid #000; border-bottom: 0.5px solid #000">
							<?php if($pri == 1){ echo "<strong>".$dato['pro_localizacion']."</strong>"; }else{ echo $dato['pro_localizacion']; } ?></td>
							<td class="auto-style37" style="width: 30px; border-right: 0.5px solid #000; border-bottom: 0.5px solid #000">
							<?php
							if($dato['pro_estado'] == 1)
							{
								if($pri == 1){ echo "<strong>X</strong>"; }else{ echo 'X'; }
							}
							?>
							</td>
							<td style="width: 30px; border-right: 0.5px solid #000; border-bottom: 0.5px solid #000" class="auto-style37">
							<?php
							if($dato['pro_estado'] == 2)
							{
								if($pri == 1){ echo "<strong>X</strong>"; }else{ echo 'X'; }
							}
							?>
							</td>
							<td class="auto-style12" style="border-bottom: 0.5px solid #000">
							<?php 
								if($dato['pro_estado'] == 2)
								{
									if($pri == 1){ echo "<strong>".wordwrap($dato['raz_nombre'], 30, "<br>", true)."</strong>"; }else{ echo wordwrap($dato['raz_nombre'], 30, "<br>", true); }
								}
							?>
							</td>
							<td class="auto-style12" style="border-bottom: 0.5px solid #000">
							<?php 
								if($dato['pro_estado'] == 2)
								{
									if($pri == 1){ echo "<strong>".wordwrap($dato['pro_no_cumplida'], 30, "<br>", true)."</strong>"; }else{ echo wordwrap($dato['pro_no_cumplida'], 30, "<br>", true); }
								}
							?>
							</td>
							<td class="auto-style12" style="border-bottom: 0.5px solid #000">
							<?php
								if($pri == 1){ echo "<strong>".wordwrap($dato['pro_observacion'], 30, "<br>", true)."</strong>"; }else{ echo wordwrap($dato['pro_observacion'], 30, "<br>", true); }
							?>
							</td>
						</tr>
			<?php
					}
				}
			?>
			<tr>
				<td class="auto-style12" style="border-right: 0.5px solid #000" colspan="9">
				<table style="width: 100%; border-spacing:0px" class="auto-style39">
					<tr>
						<td colspan="5" class="auto-style15" style="height: 35px; border-bottom: 0.5px solid #000">
						<strong>CONTROL DIARIO PERSONAL POR ACTIVIDADES PPALES CON RESTRICCIONES DE LEY 
						</strong> </td>
					</tr>
					<tr>
						<td class="auto-style18" style="border-bottom: 0.5px solid #000; border-right: 0.5px solid #000"><strong>CARGO</strong></td>
						<td class="auto-style18" style="border-bottom: 0.5px solid #000; border-right: 0.5px solid #000"><strong>CANTIDAD</strong></td>
						<td class="auto-style18" style="border-bottom: 0.5px solid #000; border-right: 0.5px solid #000"><strong>FALTANTE</strong></td>
						<td class="auto-style18" style="border-bottom: 0.5px solid #000; border-right: 0.5px solid #000"><strong>TIPO DE RESTRICCIÓN</strong></td>
						<td class="auto-style18" style="border-bottom: 0.5px solid #000; border-right: 0.5px solid #000"><strong>OBSERVACIONES</strong></td>
					</tr>
					<tr>
						<td class="auto-style12" style="border-bottom: 0.5px solid #000; border-right: 0.5px solid #000">
						&nbsp;</td>
						<td class="auto-style12" style="border-bottom: 0.5px solid #000; border-right: 0.5px solid #000">
						&nbsp;</td>
						<td class="auto-style12" style="border-bottom: 0.5px solid #000; border-right: 0.5px solid #000">
						&nbsp;</td>
						<td class="auto-style12" style="border-bottom: 0.5px solid #000; border-right: 0.5px solid #000">
						&nbsp;</td>
						<td class="auto-style25">&nbsp;</td>
					</tr>
					<tr>
						<td class="auto-style12" style="border-bottom: 0.5px solid #000; border-right: 0.5px solid #000">
						&nbsp;</td>
						<td class="auto-style12" style="border-bottom: 0.5px solid #000; border-right: 0.5px solid #000">
						&nbsp;</td>
						<td class="auto-style12" style="border-bottom: 0.5px solid #000; border-right: 0.5px solid #000">
						&nbsp;</td>
						<td class="auto-style12" style="border-bottom: 0.5px solid #000; border-right: 0.5px solid #000">
						&nbsp;</td>
						<td class="auto-style26">&nbsp;</td>
					</tr>
					<tr>
						<td class="auto-style12" style="border-bottom: 0.5px solid #000; border-right: 0.5px solid #000">
						&nbsp;</td>
						<td class="auto-style12" style="border-bottom: 0.5px solid #000; border-right: 0.5px solid #000">
						&nbsp;</td>
						<td class="auto-style12" style="border-bottom: 0.5px solid #000; border-right: 0.5px solid #000">
						&nbsp;</td>
						<td class="auto-style12" style="border-bottom: 0.5px solid #000; border-right: 0.5px solid #000">
						&nbsp;</td>
						<td class="auto-style26">&nbsp;</td>
					</tr>
					<tr>
						<td class="auto-style12" style="border-bottom: 0.5px solid #000; border-right: 0.5px solid #000">
						&nbsp;</td>
						<td class="auto-style12" style="border-bottom: 0.5px solid #000; border-right: 0.5px solid #000">
						&nbsp;</td>
						<td class="auto-style12" style="border-bottom: 0.5px solid #000; border-right: 0.5px solid #000">
						&nbsp;</td>
						<td class="auto-style12" style="border-bottom: 0.5px solid #000; border-right: 0.5px solid #000">
						&nbsp;</td>
						<td class="auto-style26">&nbsp;</td>
					</tr>
					<tr>
						<td class="auto-style12" style="border-bottom: 0.5px solid #000; border-right: 0.5px solid #000">
						&nbsp;</td>
						<td class="auto-style12" style="border-bottom: 0.5px solid #000; border-right: 0.5px solid #000">
						&nbsp;</td>
						<td class="auto-style12" style="border-bottom: 0.5px solid #000; border-right: 0.5px solid #000">
						&nbsp;</td>
						<td class="auto-style12" style="border-bottom: 0.5px solid #000; border-right: 0.5px solid #000">
						&nbsp;</td>
						<td class="auto-style26">&nbsp;</td>
					</tr>
					<tr>
						<td class="auto-style24">
						&nbsp;</td>
						<td class="auto-style24">
						&nbsp;</td>
						<td class="auto-style24">
						&nbsp;</td>
						<td class="auto-style24">
						&nbsp;</td>
						<td class="auto-style31">&nbsp;</td>
					</tr>
					<tr>
						<td class="auto-style29" style="border-top: 0.5px solid #000;">
						<strong>Notas:</strong></td>
						<td class="auto-style29" style="border-top: 0.5px solid #000;">
						&nbsp;</td>
						<td class="auto-style29" style="border-top: 0.5px solid #000;">
						&nbsp;</td>
						<td class="auto-style29" style="border-top: 0.5px solid #000;">
						&nbsp;</td>
						<td class="auto-style32" style="border-bottom: 0.5px solid #000;">Residente:</td>
					</tr>
					<tr>
						<td class="auto-style33">
						&nbsp;</td>
						<td class="auto-style33">
						&nbsp;</td>
						<td class="auto-style33">
						&nbsp;</td>
						<td class="auto-style33">
						&nbsp;</td>
						<td class="auto-style34">Elaboró</td>
					</tr>
					<tr>
						<td class="auto-style23" colspan="5">
						<table style="width: 100%">
							<tr>
								<td class="auto-style36">1. Importante el 
								control continuo en el proceso de ejecución de 
								las actividades programadas por parte de todos.</td>
							</tr>
							<tr>
								<td class="auto-style36" style="height: 17px">2. 
								Las actividades en negrilla se manejan con 
								especial cuidado y celeridad.</td>
							</tr>
							<tr>
								<td class="auto-style36">3. Las actividades se 
								programan de acuerdo a la trazabilidad marcada 
								en Last Planner (programación semanal)</td>
							</tr>
						</table>
						</td>
					</tr>
					</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	</table>
</body>
</html>
