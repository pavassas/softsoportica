<?php
include("../../data/Conexion.php");
session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_COOKIE['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];
	
	date_default_timezone_set('America/Bogota');	
	// verifica si no se ha loggeado
	if(isset($_SESSION['nw']))
	{
	    if($_SESSION['nw']<time())
	    {
	        unset($_SESSION['nw']);
	        //echo "<script>alert('Tiempo agotado - Loguearse nuevamente');</script>";
	        header("LOCATION:../../data/logout.php");
	    }
	    else
	    {
	        $_SESSION['nw'] = time() + (60 * 60);
	    }
	}
	else
	{
	   //echo "<script>alert('Tiempo agotado - Loguearse nuevamente');</script>";
	   header("LOCATION:../../data/logout.php");
	}
	if($_GET['guardarfestivo'] == 'si')
	{
		//sleep(1);
		$fecha=date("Y/m/d H:i:s");
		$d = $_GET['d'];
		$m = $_GET['m'];
		$y = $_GET['y'];
		
		$con = mysqli_query($conectar,"select * from festivo where fes_fecha = '$y-$m-$d'");
		$num = mysqli_num_rows($con);
		
		if($num > 0)
		{
			mysqli_query($conectar,"delete from festivo where fes_fecha = '$y-$m-$d'");
		}
		else
		{
			mysqli_query($conectar,"insert into festivo(fes_clave_int,fes_fecha,fes_usu_actualiz,fes_fec_actualiz) values(null,'$y-$m-$d','".$usuario."','".$fecha."')");
		}
		
		$output = '';
		$month = $m;
		$year = $y;
			
		if($month == '' && $year == '') { 
			$time = time();
			$month = date('n',$time);
		    $year = date('Y',$time);
		}
		
		$date = getdate(mktime(0,0,0,$month,1,$year));
		$today = getdate();
		$hours = $today['hours'];
		$mins = $today['minutes'];
		$secs = $today['seconds'];
		
		if(strlen($hours)<2) $hours="0".$hours;
		if(strlen($mins)<2) $mins="0".$mins;
		if(strlen($secs)<2) $secs="0".$secs;
		
		$days=date("t",mktime(0,0,0,$month,1,$year));
		$start = $date['wday']+1;
		$name = $date['month'];
		$year2 = $date['year'];
		$offset = $days + $start - 1;
		 
		if($month==12) { 
			$next=1; 
			$nexty=$year + 1; 
		} else { 
			$next=$month + 1; 
			$nexty=$year; 
		}
		
		if($month==1) { 
			$prev=12; 
			$prevy=$year - 1; 
		} else { 
			$prev=$month - 1; 
			$prevy=$year; 
		}
		
		if($offset <= 28) $weeks=28; 
		elseif($offset > 35) $weeks = 42; 
		else $weeks = 35; 
		
		$output .= "
		<table cellspacing='1'>
		<tr class='cal'>
			<td colspan='7'>
				<table align='center' class='calhead' style='width: 90%'>
				<tr>
					<td align='center'>
						<a href='javascript:navigate($prev,$prevy)'><img align='left' src='images/10.png'></a>
					</td>
					<td align='center'>
					<div class='mes' onclick='javascript:navigate(\"\",\"\")'>$name $year2</div>
					</td>
					<td align='center'>
					<a href='javascript:navigate($next,$nexty)'><img align='right' src='images/12.png'></a>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		<tr class='dayhead'>
			<td>Sun</td>
			<td>Mon</td>
			<td>Tue</td>
			<td>Wed</td>
			<td>Thu</td>
			<td>Fri</td>
			<td>Sat</td>
		</tr>";
		
		$col=1;
		$cur=1;
		$next=0;
		for($i=1;$i<=$weeks;$i++) { 
			if($next==3) $next=0;
			if($col==1) $output.="<tr class='dayrow'>"; 
			
			if($cur == 1){ $cur = '01'; }elseif($cur == 2){ $cur = '02'; }elseif($cur == 3){ $cur = '03'; }elseif($cur == 4){ $cur = '04'; }elseif($cur == 5){ $cur = '05'; }elseif($cur == 6){ $cur = '06'; }elseif($cur == 7){ $cur = '07'; }elseif($cur == 8){ $cur = '08'; }elseif($cur == 9){ $cur = '09'; }
		  	$con = mysqli_query($conectar,"select * from festivo where fes_fecha = '$year2/$month/$cur'");
			$num = mysqli_num_rows($con);
		
			if($num > 0)
			{	
				$output.="<td align='center' onclick=FECHA('$cur','$month','$year2') style=background-image:url('images/14.png');background-repeat:no-repeat valign='top' onMouseOver=\"this.className='dayover'\" onMouseOut=\"this.className='dayout'\">";
			}
			else
			{
				$output.="<td align='center' onclick=FECHA('$cur','$month','$year2') style=background-image:url('images/13.png');background-repeat:no-repeat valign='top' onMouseOver=\"this.className='dayover'\" onMouseOut=\"this.className='dayout'\">";
			}
		
			if($i <= ($days+($start-1)) && $i >= $start) {
				$output.="<div class='day'><b";
		
				if(($cur==$today[mday]) && ($name==$today[month])) $output.=" style='color:#C00'";
		
				$output.=">$cur</b></div></td>";
		
				$cur++; 
				$col++; 
				
			} else { 
				$output.="&nbsp;</td>"; 
				$col++; 
			}  
			    
		    if($col==8) { 
			    $output.="</tr>"; 
			    $col=1; 
		    }
		}
		
		$output.="</table>";
		  
		echo $output;

		exit();
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<title>Super Ajax Calendar</title>
<link rel='stylesheet' type='text/css' href='css/calendar_style.css?<?php echo time();?>' />
<script type='text/javascript' src="js/calendar.js"></script>
<script src='js/jquery-1.7.2.min.js'></script>
<script type="text/javascript" src="llamadas.js"></script>

<link rel="stylesheet" href="css/jquery-ui.css" />
<script src='js/jquery.min.js'></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

<?php //CALENDARIO ?>
<link type="text/css" rel="stylesheet" href="css/dhtmlgoodies_calendar.css?random=20051112" media="screen"></LINK>
<SCRIPT type="text/javascript" src="js/dhtmlgoodies_calendar.js?random=20060118"></script>
<script type="text/javascript" language="javascript">
	function OCULTARSCROLL()
	{
		setTimeout("parent.autoResize('iframe19')",500);
		setTimeout("parent.autoResize('iframe19')",1000);
		setTimeout("parent.autoResize('iframe19')",2000);
		setTimeout("parent.autoResize('iframe19')",3000);
		setTimeout("parent.autoResize('iframe19')",4000);
		setTimeout("parent.autoResize('iframe19')",5000);
		setTimeout("parent.autoResize('iframe19')",6000);
		setTimeout("parent.autoResize('iframe19')",7000);
		setTimeout("parent.autoResize('iframe19')",8000);
		setTimeout("parent.autoResize('iframe19')",9000);
		setTimeout("parent.autoResize('iframe19')",10000);
		setTimeout("parent.autoResize('iframe19')",11000);
		setTimeout("parent.autoResize('iframe19')",12000);
		setTimeout("parent.autoResize('iframe19')",13000);
		setTimeout("parent.autoResize('iframe19')",14000);
		setTimeout("parent.autoResize('iframe19')",15000);
		setTimeout("parent.autoResize('iframe19')",16000);
		setTimeout("parent.autoResize('iframe19')",17000);
		setTimeout("parent.autoResize('iframe19')",18000);
		setTimeout("parent.autoResize('iframe19')",19000);
		setTimeout("parent.autoResize('iframe19')",20000);
	}
	setTimeout("parent.autoResize('iframe19')",500);
	setTimeout("parent.autoResize('iframe19')",1000);
	setTimeout("parent.autoResize('iframe19')",2000);
	setTimeout("parent.autoResize('iframe19')",3000);
	setTimeout("parent.autoResize('iframe19')",4000);
	setTimeout("parent.autoResize('iframe19')",5000);
	setTimeout("parent.autoResize('iframe19')",6000);
	setTimeout("parent.autoResize('iframe19')",7000);
	setTimeout("parent.autoResize('iframe19')",8000);
	setTimeout("parent.autoResize('iframe19')",9000);
	setTimeout("parent.autoResize('iframe19')",10000);
	setTimeout("parent.autoResize('iframe19')",11000);
	setTimeout("parent.autoResize('iframe19')",12000);
	setTimeout("parent.autoResize('iframe19')",13000);
	setTimeout("parent.autoResize('iframe19')",14000);
	setTimeout("parent.autoResize('iframe19')",15000);
	setTimeout("parent.autoResize('iframe19')",16000);
	setTimeout("parent.autoResize('iframe19')",17000);
	setTimeout("parent.autoResize('iframe19')",18000);
	setTimeout("parent.autoResize('iframe19')",19000);
	setTimeout("parent.autoResize('iframe19')",20000);
</script>

</head>
<body onLoad='navigate("","")'>

	<div id="calback" align="center" style="width: 680px">
		<table style="width: 50%;">
			<tr>
				<td align="center" style="height: 40px">
				</td>
			</tr>
			<tr>
				<td align="center">Mes:
				<select name="mes" id="mes" class="inputs" onchange="navigate(this.value,$('#ano').val())">
					<option value="1" <?php if(date('m') == 1){ echo 'selected="selected"'; } ?>>1</option>
					<option value="2" <?php if(date('m') == 2){ echo 'selected="selected"'; } ?>>2</option>
					<option value="3" <?php if(date('m') == 3){ echo 'selected="selected"'; } ?>>3</option>
					<option value="4" <?php if(date('m') == 4){ echo 'selected="selected"'; } ?>>4</option>
					<option value="5" <?php if(date('m') == 5){ echo 'selected="selected"'; } ?>>5</option>
					<option value="6" <?php if(date('m') == 6){ echo 'selected="selected"'; } ?>>6</option>
					<option value="7" <?php if(date('m') == 7){ echo 'selected="selected"'; } ?>>7</option>
					<option value="8" <?php if(date('m') == 8){ echo 'selected="selected"'; } ?>>8</option>
					<option value="9" <?php if(date('m') == 9){ echo 'selected="selected"'; } ?>>9</option>
					<option value="10" <?php if(date('m') == 10){ echo 'selected="selected"'; } ?>>10</option>
					<option value="11" <?php if(date('m') == 11){ echo 'selected="selected"'; } ?>>11</option>
					<option value="12" <?php if(date('m') == 12){ echo 'selected="selected"'; } ?>>12</option>
				</select>
				Año:
				<select name="ano" id="ano" class="inputs" onchange="navigate($('#mes').val(),this.value)">
					<option value="2014" <?php if(date('Y') == 2014){ echo 'selected="selected"'; } ?>>2014</option>
					<option value="2015" <?php if(date('Y') == 2015){ echo 'selected="selected"'; } ?>>2015</option>
					<option value="2016" <?php if(date('Y') == 2016){ echo 'selected="selected"'; } ?>>2016</option>
					<option value="2017" <?php if(date('Y') == 2017){ echo 'selected="selected"'; } ?>>2017</option>
					<option value="2018" <?php if(date('Y') == 2018){ echo 'selected="selected"'; } ?>>2018</option>
					<option value="2019" <?php if(date('Y') == 2019){ echo 'selected="selected"'; } ?>>2019</option>
					<option value="2020" <?php if(date('Y') == 2020){ echo 'selected="selected"'; } ?>>2020</option>
				</select>

				<div id="calendar" style="text-align:center; width:100%" align="center"></div>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>