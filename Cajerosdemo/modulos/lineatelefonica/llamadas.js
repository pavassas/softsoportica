function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function EDITAR(v,m)
{	
	if(m == 'CANALCOMUNICACION')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('editarcanalcomunicacion').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#editarcanalcomunicacion").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?editarcan=si&canedi="+v,true);
		ajax.send(null);
	}
}
function GUARDAR(v,id)
{
	if(v == 'CANALCOMUNICACION')
	{
		var can = form1.canalcomunicacion1.value;
		var lcc = can.length;
		var act = form1.activo1.checked;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('datos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#datos").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?guardarcan=si&can="+can+"&act="+act+"&lcc="+lcc+"&cc="+id,true);
		ajax.send(null);
		if(can != '' && lcc >= 3)
		{
			setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
		}
	}
}
function NUEVO(v)
{
	if(v == 'CANALCOMUNICACION')
	{
		var can = form1.canalcomunicacion.value;
		var lcc = can.length;		
		var act = form1.activo.checked;
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('datos1').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#datos1").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?nuevocanalcomunicacion=si&can="+can+"&act="+act+"&lcc="+lcc,true);
		ajax.send(null);
		if(can != '' && lcc >= 3)
		{
			form1.canalcomunicacion.value = '';
			setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
		}
	}
}
function CONSULTAMODULO(v)
{
	if(v == 'TODOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('canalcomunicaciones').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#canalcomunicaciones").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?todos=si",true);
		ajax.send(null);
	}
}
function BUSCAR(m)
{	
	if(m == 'CANALCOMUNICACION')
	{
		var can = form1.canalcomunicacion2.value;
		var act = form1.buscaractivos.value;
				
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('canalcomunicaciones').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#canalcomunicaciones").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?buscarcan=si&can="+can+"&act="+act,true);
		ajax.send(null);
	}
}