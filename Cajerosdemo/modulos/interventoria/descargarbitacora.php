<?php include('../../data/Conexion.php');
$actor = $_GET['actor'];
$clacaj = $_GET['clacaj'];
$claaco = $_GET['claaco'];

function extension_archivo($ruta) 
{
    $res = explode(".", $ruta);
    $extension = $res[count($res)-1];
    return $extension ;
} 
if($actor == 'REQUERIMIENTOINMOBILIARIA')
{
	$con = mysqli_query($conectar,"select cai_adjunto_req_local_inmobiliaria from cajero_inmobiliaria where caj_clave_int = '".$clacaj."'");
	$dato = mysqli_fetch_array($con);
	$adj = $dato['cai_adjunto_req_local_inmobiliaria'];
	
	$ext = extension_archivo($adj);
	$nombre_archivo=$clacaj."-INMOBILIARIA_REQUERIMIENTO_LOCAL.".$ext;
	
	switch ($ext) {
        case "pdf": $ctype="application/pdf"; break;
        case "exe": $ctype="application/octet-stream"; break;
        case "zip": $ctype="application/zip"; break;
        case "doc": $ctype="application/msword"; break;
        case "docx": $ctype="application/msword"; break;
        case "xls": $ctype="application/vnd.ms-excel"; break;
        case "xlsx": $ctype="application/vnd.ms-excel"; break;
        case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
        case "pptx": $ctype="application/vnd.ms-powerpoint"; break;
        case "gif": $ctype="image/gif"; break;
        case "png": $ctype="image/png"; break;
        case "txt": $ctype="text/plain"; break;
        case "htm": $ctype="text/html"; break;
        case "html": $ctype="text/html"; break;
        case "avi": $ctype="video/x-msvideo"; break;
        case "mov": $ctype="video/quicktime"; break;
        case "mpe": $ctype="video/mpeg"; break;
        case "mpg": $ctype="video/mpeg"; break;
        case "mpeg": $ctype="video/mpeg"; break;
        case "jpe": case "jpeg":
        case "jpg": $ctype="image/jpeg"; break;
        case "jpeg": $ctype="image/jpeg"; break;
        default: $ctype="application/force-download";
    }

    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false); // required for certain browsers
    header("Content-Type: $ctype");
    header('Content-Disposition: attachment; filename="' . $nombre_archivo .'"');
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($adj));
    readfile("$adj");
}
else
if($actor == 'REQUERIMIENTOVISITA')
{
	$con = mysqli_query($conectar,"select cav_adjunto_req_visita from cajero_visita where caj_clave_int = '".$clacaj."'");
	$dato = mysqli_fetch_array($con);
	$adj = $dato['cav_adjunto_req_visita'];
	
	$ext = extension_archivo($adj);
	$nombre_archivo=$clacaj."-VISITA_REQUERIMIENTO_VISITA.".$ext;
	
	switch ($ext) {
        case "pdf": $ctype="application/pdf"; break;
        case "exe": $ctype="application/octet-stream"; break;
        case "zip": $ctype="application/zip"; break;
        case "doc": $ctype="application/msword"; break;
        case "docx": $ctype="application/msword"; break;
        case "xls": $ctype="application/vnd.ms-excel"; break;
        case "xlsx": $ctype="application/vnd.ms-excel"; break;
        case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
        case "pptx": $ctype="application/vnd.ms-powerpoint"; break;
        case "gif": $ctype="image/gif"; break;
        case "png": $ctype="image/png"; break;
        case "txt": $ctype="text/plain"; break;
        case "htm": $ctype="text/html"; break;
        case "html": $ctype="text/html"; break;
        case "avi": $ctype="video/x-msvideo"; break;
        case "mov": $ctype="video/quicktime"; break;
        case "mpe": $ctype="video/mpeg"; break;
        case "mpg": $ctype="video/mpeg"; break;
        case "mpeg": $ctype="video/mpeg"; break;
        case "jpe": case "jpeg":
        case "jpg": $ctype="image/jpeg"; break;
        case "jpeg": $ctype="image/jpeg"; break;
        default: $ctype="application/force-download";
    }

    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false); // required for certain browsers
    header("Content-Type: $ctype");
    header('Content-Disposition: attachment; filename="' . $nombre_archivo .'"');
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($adj));
    readfile("$adj");
}
else
if($actor == 'REQUERIMIENTODISENO')
{
	$con = mysqli_query($conectar,"select cad_adjunto_req_diseno from cajero_diseno where caj_clave_int = '".$clacaj."'");
	$dato = mysqli_fetch_array($con);
	$adj = $dato['cad_adjunto_req_diseno'];
	
	$ext = extension_archivo($adj);
	$nombre_archivo=$clacaj."-DISENO_REQUERIMIENTO_DISENO.".$ext;
	
	switch ($ext) {
        case "pdf": $ctype="application/pdf"; break;
        case "exe": $ctype="application/octet-stream"; break;
        case "zip": $ctype="application/zip"; break;
        case "doc": $ctype="application/msword"; break;
        case "docx": $ctype="application/msword"; break;
        case "xls": $ctype="application/vnd.ms-excel"; break;
        case "xlsx": $ctype="application/vnd.ms-excel"; break;
        case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
        case "pptx": $ctype="application/vnd.ms-powerpoint"; break;
        case "gif": $ctype="image/gif"; break;
        case "png": $ctype="image/png"; break;
        case "txt": $ctype="text/plain"; break;
        case "htm": $ctype="text/html"; break;
        case "html": $ctype="text/html"; break;
        case "avi": $ctype="video/x-msvideo"; break;
        case "mov": $ctype="video/quicktime"; break;
        case "mpe": $ctype="video/mpeg"; break;
        case "mpg": $ctype="video/mpeg"; break;
        case "mpeg": $ctype="video/mpeg"; break;
        case "jpe": case "jpeg":
        case "jpg": $ctype="image/jpeg"; break;
        case "jpeg": $ctype="image/jpeg"; break;
        default: $ctype="application/force-download";
    }

    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false); // required for certain browsers
    header("Content-Type: $ctype");
    header('Content-Disposition: attachment; filename="' . $nombre_archivo .'"');
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($adj));
    readfile("$adj");
}
else
if($actor == 'REQUERIMIENTOLICENCIA')
{
	$con = mysqli_query($conectar,"select cal_adjunto_req_licencia from cajero_licencia where caj_clave_int = '".$clacaj."'");
	$dato = mysqli_fetch_array($con);
	$adj = $dato['cal_adjunto_req_licencia'];
	
	$ext = extension_archivo($adj);
	$nombre_archivo=$clacaj."-LICENCIA_REQUERIMIENTO_LICENCIA.".$ext;
	
	switch ($ext) {
        case "pdf": $ctype="application/pdf"; break;
        case "exe": $ctype="application/octet-stream"; break;
        case "zip": $ctype="application/zip"; break;
        case "doc": $ctype="application/msword"; break;
        case "docx": $ctype="application/msword"; break;
        case "xls": $ctype="application/vnd.ms-excel"; break;
        case "xlsx": $ctype="application/vnd.ms-excel"; break;
        case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
        case "pptx": $ctype="application/vnd.ms-powerpoint"; break;
        case "gif": $ctype="image/gif"; break;
        case "png": $ctype="image/png"; break;
        case "txt": $ctype="text/plain"; break;
        case "htm": $ctype="text/html"; break;
        case "html": $ctype="text/html"; break;
        case "avi": $ctype="video/x-msvideo"; break;
        case "mov": $ctype="video/quicktime"; break;
        case "mpe": $ctype="video/mpeg"; break;
        case "mpg": $ctype="video/mpeg"; break;
        case "mpeg": $ctype="video/mpeg"; break;
        case "jpe": case "jpeg":
        case "jpg": $ctype="image/jpeg"; break;
        case "jpeg": $ctype="image/jpeg"; break;
        default: $ctype="application/force-download";
    }

    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false); // required for certain browsers
    header("Content-Type: $ctype");
    header('Content-Disposition: attachment; filename="' . $nombre_archivo .'"');
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($adj));
    readfile("$adj");
}
else
if($actor == 'INFORMEINMOBILIARIA')
{
	$con = mysqli_query($conectar,"select cai_adjunto_informe_inmobiliaria from cajero_inmobiliaria where caj_clave_int = '".$clacaj."'");
	$dato = mysqli_fetch_array($con);
	$adj = $dato['cai_adjunto_informe_inmobiliaria'];
	
	$ext = extension_archivo($adj);
	$nombre_archivo=$clacaj."-INMOBILIARIA_INFORME.".$ext;
	
	switch ($ext) {
        case "pdf": $ctype="application/pdf"; break;
        case "exe": $ctype="application/octet-stream"; break;
        case "zip": $ctype="application/zip"; break;
        case "doc": $ctype="application/msword"; break;
        case "docx": $ctype="application/msword"; break;
        case "xls": $ctype="application/vnd.ms-excel"; break;
        case "xlsx": $ctype="application/vnd.ms-excel"; break;
        case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
        case "pptx": $ctype="application/vnd.ms-powerpoint"; break;
        case "gif": $ctype="image/gif"; break;
        case "png": $ctype="image/png"; break;
        case "txt": $ctype="text/plain"; break;
        case "htm": $ctype="text/html"; break;
        case "html": $ctype="text/html"; break;
        case "avi": $ctype="video/x-msvideo"; break;
        case "mov": $ctype="video/quicktime"; break;
        case "mpe": $ctype="video/mpeg"; break;
        case "mpg": $ctype="video/mpeg"; break;
        case "mpeg": $ctype="video/mpeg"; break;
        case "jpe": case "jpeg":
        case "jpg": $ctype="image/jpeg"; break;
        case "jpeg": $ctype="image/jpeg"; break;
        default: $ctype="application/force-download";
    }

    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false); // required for certain browsers
    header("Content-Type: $ctype");
    header('Content-Disposition: attachment; filename="' . $nombre_archivo .'"');
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($adj));
    readfile("$adj");
}
else
if($actor == 'INFORMEVISITA')
{
	$con = mysqli_query($conectar,"select cav_adjunto_informe_visita from cajero_visita where caj_clave_int = '".$clacaj."'");
	$dato = mysqli_fetch_array($con);
	$adj = $dato['cav_adjunto_informe_visita'];
	
	$ext = extension_archivo($adj);
	$nombre_archivo=$clacaj."-VISITA_INFORME.".$ext;
	
	switch ($ext) {
        case "pdf": $ctype="application/pdf"; break;
        case "exe": $ctype="application/octet-stream"; break;
        case "zip": $ctype="application/zip"; break;
        case "doc": $ctype="application/msword"; break;
        case "docx": $ctype="application/msword"; break;
        case "xls": $ctype="application/vnd.ms-excel"; break;
        case "xlsx": $ctype="application/vnd.ms-excel"; break;
        case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
        case "pptx": $ctype="application/vnd.ms-powerpoint"; break;
        case "gif": $ctype="image/gif"; break;
        case "png": $ctype="image/png"; break;
        case "txt": $ctype="text/plain"; break;
        case "htm": $ctype="text/html"; break;
        case "html": $ctype="text/html"; break;
        case "avi": $ctype="video/x-msvideo"; break;
        case "mov": $ctype="video/quicktime"; break;
        case "mpe": $ctype="video/mpeg"; break;
        case "mpg": $ctype="video/mpeg"; break;
        case "mpeg": $ctype="video/mpeg"; break;
        case "jpe": case "jpeg":
        case "jpg": $ctype="image/jpeg"; break;
        case "jpeg": $ctype="image/jpeg"; break;
        default: $ctype="application/force-download";
    }

    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false); // required for certain browsers
    header("Content-Type: $ctype");
    header('Content-Disposition: attachment; filename="' . $nombre_archivo .'"');
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($adj));
    readfile("$adj");
}
else
if($actor == 'INFORMEDISENO')
{
	$con = mysqli_query($conectar,"select cad_adjunto_informe_diseno from cajero_diseno where caj_clave_int = '".$clacaj."'");
	$dato = mysqli_fetch_array($con);
	$adj = $dato['cad_adjunto_informe_diseno'];
	
	$ext = extension_archivo($adj);
	$nombre_archivo=$clacaj."-INMOBILIARIA_INFORME.".$ext;
	
	switch ($ext) {
        case "pdf": $ctype="application/pdf"; break;
        case "exe": $ctype="application/octet-stream"; break;
        case "zip": $ctype="application/zip"; break;
        case "doc": $ctype="application/msword"; break;
        case "docx": $ctype="application/msword"; break;
        case "xls": $ctype="application/vnd.ms-excel"; break;
        case "xlsx": $ctype="application/vnd.ms-excel"; break;
        case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
        case "pptx": $ctype="application/vnd.ms-powerpoint"; break;
        case "gif": $ctype="image/gif"; break;
        case "png": $ctype="image/png"; break;
        case "txt": $ctype="text/plain"; break;
        case "htm": $ctype="text/html"; break;
        case "html": $ctype="text/html"; break;
        case "avi": $ctype="video/x-msvideo"; break;
        case "mov": $ctype="video/quicktime"; break;
        case "mpe": $ctype="video/mpeg"; break;
        case "mpg": $ctype="video/mpeg"; break;
        case "mpeg": $ctype="video/mpeg"; break;
        case "jpe": case "jpeg":
        case "jpg": $ctype="image/jpeg"; break;
        case "jpeg": $ctype="image/jpeg"; break;
        default: $ctype="application/force-download";
    }

    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false); // required for certain browsers
    header("Content-Type: $ctype");
    header('Content-Disposition: attachment; filename="' . $nombre_archivo .'"');
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($adj));
    readfile("$adj");
}
else
if($actor == 'INFORMELICENCIA')
{
	$con = mysqli_query($conectar,"select cal_adjunto_informe_licencia from cajero_licencia where caj_clave_int = '".$clacaj."'");
	$dato = mysqli_fetch_array($con);
	$adj = $dato['cal_adjunto_informe_licencia'];
	
	$ext = extension_archivo($adj);
	$nombre_archivo=$clacaj."-LICENCIA_INFORME.".$ext;
	
	switch ($ext) {
        case "pdf": $ctype="application/pdf"; break;
        case "exe": $ctype="application/octet-stream"; break;
        case "zip": $ctype="application/zip"; break;
        case "doc": $ctype="application/msword"; break;
        case "docx": $ctype="application/msword"; break;
        case "xls": $ctype="application/vnd.ms-excel"; break;
        case "xlsx": $ctype="application/vnd.ms-excel"; break;
        case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
        case "pptx": $ctype="application/vnd.ms-powerpoint"; break;
        case "gif": $ctype="image/gif"; break;
        case "png": $ctype="image/png"; break;
        case "txt": $ctype="text/plain"; break;
        case "htm": $ctype="text/html"; break;
        case "html": $ctype="text/html"; break;
        case "avi": $ctype="video/x-msvideo"; break;
        case "mov": $ctype="video/quicktime"; break;
        case "mpe": $ctype="video/mpeg"; break;
        case "mpg": $ctype="video/mpeg"; break;
        case "mpeg": $ctype="video/mpeg"; break;
        case "jpe": case "jpeg":
        case "jpg": $ctype="image/jpeg"; break;
        case "jpeg": $ctype="image/jpeg"; break;
        default: $ctype="application/force-download";
    }

    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false); // required for certain browsers
    header("Content-Type: $ctype");
    header('Content-Disposition: attachment; filename="' . $nombre_archivo .'"');
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($adj));
    readfile("$adj");
}
else
if($actor == 'INFORMEINTERVENTORIA')
{
	$con = mysqli_query($conectar,"select cin_adjunto_info_interventoria from cajero_interventoria where caj_clave_int = '".$clacaj."'");
	$dato = mysqli_fetch_array($con);
	$adj = $dato['cin_adjunto_info_interventoria'];
	
	$ext = extension_archivo($adj);
	$nombre_archivo=$clacaj."-INTERVENTORIA_INFORMACION.".$ext;
	switch ($ext) {
        case "pdf": $ctype="application/pdf"; break;
        case "exe": $ctype="application/octet-stream"; break;
        case "zip": $ctype="application/zip"; break;
        case "doc": $ctype="application/msword"; break;
        case "docx": $ctype="application/msword"; break;
        case "xls": $ctype="application/vnd.ms-excel"; break;
        case "xlsx": $ctype="application/vnd.ms-excel"; break;
        case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
        case "pptx": $ctype="application/vnd.ms-powerpoint"; break;
        case "gif": $ctype="image/gif"; break;
        case "png": $ctype="image/png"; break;
        case "txt": $ctype="text/plain"; break;
        case "htm": $ctype="text/html"; break;
        case "html": $ctype="text/html"; break;
        case "avi": $ctype="video/x-msvideo"; break;
        case "mov": $ctype="video/quicktime"; break;
        case "mpe": $ctype="video/mpeg"; break;
        case "mpg": $ctype="video/mpeg"; break;
        case "mpeg": $ctype="video/mpeg"; break;
        case "jpe": case "jpeg":
        case "jpg": $ctype="image/jpeg"; break;
        case "jpeg": $ctype="image/jpeg"; break;
        default: $ctype="application/force-download";
    }

    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false); // required for certain browsers
    header("Content-Type: $ctype");
    header('Content-Disposition: attachment; filename="' . $nombre_archivo .'"');
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($adj));
    readfile("$adj");
}
else
if($actor == 'INFORMECOTIZACION')
{
	$con = mysqli_query($conectar,"select cac_adjunto_cotizacion from cajero_constructor where caj_clave_int = '".$clacaj."'");
	$dato = mysqli_fetch_array($con);
	$adj = $dato['cac_adjunto_cotizacion'];
	
	$ext = extension_archivo($adj);
	$nombre_archivo=$clacaj."-CONSTRUCTOR_COTIZACION.".$ext;
	switch ($ext) {
        case "pdf": $ctype="application/pdf"; break;
        case "exe": $ctype="application/octet-stream"; break;
        case "zip": $ctype="application/zip"; break;
        case "doc": $ctype="application/msword"; break;
        case "docx": $ctype="application/msword"; break;
        case "xls": $ctype="application/vnd.ms-excel"; break;
        case "xlsx": $ctype="application/vnd.ms-excel"; break;
        case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
        case "pptx": $ctype="application/vnd.ms-powerpoint"; break;
        case "gif": $ctype="image/gif"; break;
        case "png": $ctype="image/png"; break;
        case "txt": $ctype="text/plain"; break;
        case "htm": $ctype="text/html"; break;
        case "html": $ctype="text/html"; break;
        case "avi": $ctype="video/x-msvideo"; break;
        case "mov": $ctype="video/quicktime"; break;
        case "mpe": $ctype="video/mpeg"; break;
        case "mpg": $ctype="video/mpeg"; break;
        case "mpeg": $ctype="video/mpeg"; break;
        case "jpe": case "jpeg":
        case "jpg": $ctype="image/jpeg"; break;
        case "jpeg": $ctype="image/jpeg"; break;
        default: $ctype="application/force-download";
    }

    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false); // required for certain browsers
    header("Content-Type: $ctype");
    header('Content-Disposition: attachment; filename="' . $nombre_archivo .'"');
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($adj));
    readfile("$adj");
}
else
if($actor == 'INFORMEFINAL')
{
	$con = mysqli_query($conectar,"select cac_adjunto_liq_fot_act from cajero_constructor where caj_clave_int = '".$clacaj."'");
	$dato = mysqli_fetch_array($con);
	$adj = $dato['cac_adjunto_liq_fot_act'];
	
	$ext = extension_archivo($adj);
	$nombre_archivo=$clacaj."-CONSTRUCTOR_INFORME_FINAL.".$ext;
	switch ($ext) {
        case "pdf": $ctype="application/pdf"; break;
        case "exe": $ctype="application/octet-stream"; break;
        case "zip": $ctype="application/zip"; break;
        case "doc": $ctype="application/msword"; break;
        case "docx": $ctype="application/msword"; break;
        case "xls": $ctype="application/vnd.ms-excel"; break;
        case "xlsx": $ctype="application/vnd.ms-excel"; break;
        case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
        case "pptx": $ctype="application/vnd.ms-powerpoint"; break;
        case "gif": $ctype="image/gif"; break;
        case "png": $ctype="image/png"; break;
        case "txt": $ctype="text/plain"; break;
        case "htm": $ctype="text/html"; break;
        case "html": $ctype="text/html"; break;
        case "avi": $ctype="video/x-msvideo"; break;
        case "mov": $ctype="video/quicktime"; break;
        case "mpe": $ctype="video/mpeg"; break;
        case "mpg": $ctype="video/mpeg"; break;
        case "mpeg": $ctype="video/mpeg"; break;
        case "jpe": case "jpeg":
        case "jpg": $ctype="image/jpeg"; break;
        case "jpeg": $ctype="image/jpeg"; break;
        default: $ctype="application/force-download";
    }

    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false); // required for certain browsers
    header("Content-Type: $ctype");
    header('Content-Disposition: attachment; filename="' . $nombre_archivo .'"');
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($adj));
    readfile("$adj");
}
else
if($actor == 'AVANCE')
{
	$con = mysqli_query($conectar,"select aco_adjunto_avance from adjunto_constructor where aco_clave_int = '".$claaco."'");
	$dato = mysqli_fetch_array($con);
	$adj = $dato['aco_adjunto_avance'];
	
	$ext = extension_archivo($adj);
	$nombre_archivo=$clacaj."-CONSTRUCTOR_AVANCE".$claaco.".".$ext;
	
	switch ($ext) {
        case "pdf": $ctype="application/pdf"; break;
        case "exe": $ctype="application/octet-stream"; break;
        case "zip": $ctype="application/zip"; break;
        case "doc": $ctype="application/msword"; break;
        case "docx": $ctype="application/msword"; break;
        case "xls": $ctype="application/vnd.ms-excel"; break;
        case "xlsx": $ctype="application/vnd.ms-excel"; break;
        case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
        case "pptx": $ctype="application/vnd.ms-powerpoint"; break;
        case "gif": $ctype="image/gif"; break;
        case "png": $ctype="image/png"; break;
        case "txt": $ctype="text/plain"; break;
        case "htm": $ctype="text/html"; break;
        case "html": $ctype="text/html"; break;
        case "avi": $ctype="video/x-msvideo"; break;
        case "mov": $ctype="video/quicktime"; break;
        case "mpe": $ctype="video/mpeg"; break;
        case "mpg": $ctype="video/mpeg"; break;
        case "mpeg": $ctype="video/mpeg"; break;
        case "jpe": case "jpeg":
        case "jpg": $ctype="image/jpeg"; break;
        case "jpeg": $ctype="image/jpeg"; break;
        default: $ctype="application/force-download";
    }

    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false); // required for certain browsers
    header("Content-Type: $ctype");
    header('Content-Disposition: attachment; filename="' . $nombre_archivo .'"');
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($adj));
    readfile("$adj");
}
else
if($actor == 'INFORMESEGURIDAD')
{
	$con = mysqli_query($conectar,"select cas_adjunto_seguridad from cajero_seguridad where caj_clave_int = '".$clacaj."'");
	$dato = mysqli_fetch_array($con);
	$adj = $dato['cas_adjunto_seguridad'];
	
	$ext = extension_archivo($adj);
	$nombre_archivo=$clacaj."-SEGURIDAD_INFORMACION.".$ext;
	
	switch ($ext) {
        case "pdf": $ctype="application/pdf"; break;
        case "exe": $ctype="application/octet-stream"; break;
        case "zip": $ctype="application/zip"; break;
        case "doc": $ctype="application/msword"; break;
        case "docx": $ctype="application/msword"; break;
        case "xls": $ctype="application/vnd.ms-excel"; break;
        case "xlsx": $ctype="application/vnd.ms-excel"; break;
        case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
        case "pptx": $ctype="application/vnd.ms-powerpoint"; break;
        case "gif": $ctype="image/gif"; break;
        case "png": $ctype="image/png"; break;
        case "txt": $ctype="text/plain"; break;
        case "htm": $ctype="text/html"; break;
        case "html": $ctype="text/html"; break;
        case "avi": $ctype="video/x-msvideo"; break;
        case "mov": $ctype="video/quicktime"; break;
        case "mpe": $ctype="video/mpeg"; break;
        case "mpg": $ctype="video/mpeg"; break;
        case "mpeg": $ctype="video/mpeg"; break;
        case "jpe": case "jpeg":
        case "jpg": $ctype="image/jpeg"; break;
        case "jpeg": $ctype="image/jpeg"; break;
        default: $ctype="application/force-download";
    }

    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false); // required for certain browsers
    header("Content-Type: $ctype");
    header('Content-Disposition: attachment; filename="' . $nombre_archivo .'"');
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($adj));
    readfile("$adj");
}
else
if($actor == 'FACTURACIONINMOBILIARIA')
{
	$con = mysqli_query($conectar,"select cai_adjunto_fact_inmobiliaria from cajero_inmobiliaria where caj_clave_int = '".$clacaj."'");
	$dato = mysqli_fetch_array($con);
	$adj = $dato['cai_adjunto_fact_inmobiliaria'];
	
	$ext = extension_archivo($adj);
	$nombre_archivo=$clacaj."-INMOBILIARIA_FACTURACION.".$ext;
	switch ($ext) {
        case "pdf": $ctype="application/pdf"; break;
        case "exe": $ctype="application/octet-stream"; break;
        case "zip": $ctype="application/zip"; break;
        case "doc": $ctype="application/msword"; break;
        case "docx": $ctype="application/msword"; break;
        case "xls": $ctype="application/vnd.ms-excel"; break;
        case "xlsx": $ctype="application/vnd.ms-excel"; break;
        case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
        case "pptx": $ctype="application/vnd.ms-powerpoint"; break;
        case "gif": $ctype="image/gif"; break;
        case "png": $ctype="image/png"; break;
        case "txt": $ctype="text/plain"; break;
        case "htm": $ctype="text/html"; break;
        case "html": $ctype="text/html"; break;
        case "avi": $ctype="video/x-msvideo"; break;
        case "mov": $ctype="video/quicktime"; break;
        case "mpe": $ctype="video/mpeg"; break;
        case "mpg": $ctype="video/mpeg"; break;
        case "mpeg": $ctype="video/mpeg"; break;
        case "jpe": case "jpeg":
        case "jpg": $ctype="image/jpeg"; break;
        case "jpeg": $ctype="image/jpeg"; break;
        default: $ctype="application/force-download";
    }

    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false); // required for certain browsers
    header("Content-Type: $ctype");
    header('Content-Disposition: attachment; filename="' . $nombre_archivo .'"');
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($adj));
    readfile("$adj");
}
else
if($actor == 'FACTURACIONVISITA')
{
	$con = mysqli_query($conectar,"select cav_adjunto_fact_visita from cajero_visita where caj_clave_int = '".$clacaj."'");
	$dato = mysqli_fetch_array($con);
	$adj = $dato['cav_adjunto_fact_visita'];
	
	$ext = extension_archivo($adj);
	$nombre_archivo=$clacaj."-VISITA_FACTURACION.".$ext;
	switch ($ext) {
        case "pdf": $ctype="application/pdf"; break;
        case "exe": $ctype="application/octet-stream"; break;
        case "zip": $ctype="application/zip"; break;
        case "doc": $ctype="application/msword"; break;
        case "docx": $ctype="application/msword"; break;
        case "xls": $ctype="application/vnd.ms-excel"; break;
        case "xlsx": $ctype="application/vnd.ms-excel"; break;
        case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
        case "pptx": $ctype="application/vnd.ms-powerpoint"; break;
        case "gif": $ctype="image/gif"; break;
        case "png": $ctype="image/png"; break;
        case "txt": $ctype="text/plain"; break;
        case "htm": $ctype="text/html"; break;
        case "html": $ctype="text/html"; break;
        case "avi": $ctype="video/x-msvideo"; break;
        case "mov": $ctype="video/quicktime"; break;
        case "mpe": $ctype="video/mpeg"; break;
        case "mpg": $ctype="video/mpeg"; break;
        case "mpeg": $ctype="video/mpeg"; break;
        case "jpe": case "jpeg":
        case "jpg": $ctype="image/jpeg"; break;
        case "jpeg": $ctype="image/jpeg"; break;
        default: $ctype="application/force-download";
    }

    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false); // required for certain browsers
    header("Content-Type: $ctype");
    header('Content-Disposition: attachment; filename="' . $nombre_archivo .'"');
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($adj));
    readfile("$adj");
}
else
if($actor == 'FACTURACIONDISENO')
{
	$con = mysqli_query($conectar,"select cad_adjunto_fact_diseno from cajero_diseno where caj_clave_int = '".$clacaj."'");
	$dato = mysqli_fetch_array($con);
	$adj = $dato['cad_adjunto_fact_diseno'];
	
	$ext = extension_archivo($adj);
	$nombre_archivo=$clacaj."-DISENO_FACTURACION.".$ext;
	switch ($ext) {
        case "pdf": $ctype="application/pdf"; break;
        case "exe": $ctype="application/octet-stream"; break;
        case "zip": $ctype="application/zip"; break;
        case "doc": $ctype="application/msword"; break;
        case "docx": $ctype="application/msword"; break;
        case "xls": $ctype="application/vnd.ms-excel"; break;
        case "xlsx": $ctype="application/vnd.ms-excel"; break;
        case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
        case "pptx": $ctype="application/vnd.ms-powerpoint"; break;
        case "gif": $ctype="image/gif"; break;
        case "png": $ctype="image/png"; break;
        case "txt": $ctype="text/plain"; break;
        case "htm": $ctype="text/html"; break;
        case "html": $ctype="text/html"; break;
        case "avi": $ctype="video/x-msvideo"; break;
        case "mov": $ctype="video/quicktime"; break;
        case "mpe": $ctype="video/mpeg"; break;
        case "mpg": $ctype="video/mpeg"; break;
        case "mpeg": $ctype="video/mpeg"; break;
        case "jpe": case "jpeg":
        case "jpg": $ctype="image/jpeg"; break;
        case "jpeg": $ctype="image/jpeg"; break;
        default: $ctype="application/force-download";
    }

    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false); // required for certain browsers
    header("Content-Type: $ctype");
    header('Content-Disposition: attachment; filename="' . $nombre_archivo .'"');
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($adj));
    readfile("$adj");
}
else
if($actor == 'FACTURACIONLICENCIA')
{
	$con = mysqli_query($conectar,"select cal_adjunto_fact_licencia from cajero_licencia where caj_clave_int = '".$clacaj."'");
	$dato = mysqli_fetch_array($con);
	$adj = $dato['cal_adjunto_fact_licencia'];
	
	$ext = extension_archivo($adj);
	$nombre_archivo=$clacaj."-LICENCIA_FACTURACION.".$ext;
	switch ($ext) {
        case "pdf": $ctype="application/pdf"; break;
        case "exe": $ctype="application/octet-stream"; break;
        case "zip": $ctype="application/zip"; break;
        case "doc": $ctype="application/msword"; break;
        case "docx": $ctype="application/msword"; break;
        case "xls": $ctype="application/vnd.ms-excel"; break;
        case "xlsx": $ctype="application/vnd.ms-excel"; break;
        case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
        case "pptx": $ctype="application/vnd.ms-powerpoint"; break;
        case "gif": $ctype="image/gif"; break;
        case "png": $ctype="image/png"; break;
        case "txt": $ctype="text/plain"; break;
        case "htm": $ctype="text/html"; break;
        case "html": $ctype="text/html"; break;
        case "avi": $ctype="video/x-msvideo"; break;
        case "mov": $ctype="video/quicktime"; break;
        case "mpe": $ctype="video/mpeg"; break;
        case "mpg": $ctype="video/mpeg"; break;
        case "mpeg": $ctype="video/mpeg"; break;
        case "jpe": case "jpeg":
        case "jpg": $ctype="image/jpeg"; break;
        case "jpeg": $ctype="image/jpeg"; break;
        default: $ctype="application/force-download";
    }
    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false); // required for certain browsers
    header("Content-Type: $ctype");
    header('Content-Disposition: attachment; filename="' . $nombre_archivo .'"');
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($adj));
    readfile("$adj");
}
else
if($actor == 'FACTURACIONINTERVENTORIA')
{
	$con = mysqli_query($conectar,"select cin_adjunto_fact_interventoria from cajero_interventoria where caj_clave_int = '".$clacaj."'");
	$dato = mysqli_fetch_array($con);
	$adj = $dato['cin_adjunto_fact_interventoria'];
	
	$ext = extension_archivo($adj);
	$nombre_archivo=$clacaj."-INTERVENTORIA_FACTURACION.".$ext;
	switch ($ext) {
        case "pdf": $ctype="application/pdf"; break;
        case "exe": $ctype="application/octet-stream"; break;
        case "zip": $ctype="application/zip"; break;
        case "doc": $ctype="application/msword"; break;
        case "docx": $ctype="application/msword"; break;
        case "xls": $ctype="application/vnd.ms-excel"; break;
        case "xlsx": $ctype="application/vnd.ms-excel"; break;
        case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
        case "pptx": $ctype="application/vnd.ms-powerpoint"; break;
        case "gif": $ctype="image/gif"; break;
        case "png": $ctype="image/png"; break;
        case "txt": $ctype="text/plain"; break;
        case "htm": $ctype="text/html"; break;
        case "html": $ctype="text/html"; break;
        case "avi": $ctype="video/x-msvideo"; break;
        case "mov": $ctype="video/quicktime"; break;
        case "mpe": $ctype="video/mpeg"; break;
        case "mpg": $ctype="video/mpeg"; break;
        case "mpeg": $ctype="video/mpeg"; break;
        case "jpe": case "jpeg":
        case "jpg": $ctype="image/jpeg"; break;
        case "jpeg": $ctype="image/jpeg"; break;
        default: $ctype="application/force-download";
    }

    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false); // required for certain browsers
    header("Content-Type: $ctype");
    header('Content-Disposition: attachment; filename="' . $nombre_archivo .'"');
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($adj));
    readfile("$adj");
}
else
if($actor == 'FACTURACIONCONSTRUCTOR')
{
	$con = mysqli_query($conectar,"select cac_adjunto_fact_constructor from cajero_constructor where caj_clave_int = '".$clacaj."'");
	$dato = mysqli_fetch_array($con);
	$adj = $dato['cac_adjunto_fact_constructor'];
	
	$ext = extension_archivo($adj);
	$nombre_archivo=$clacaj."-CONSTRUCTOR_FACTURACION.".$ext;
	switch ($ext) {
        case "pdf": $ctype="application/pdf"; break;
        case "exe": $ctype="application/octet-stream"; break;
        case "zip": $ctype="application/zip"; break;
        case "doc": $ctype="application/msword"; break;
        case "docx": $ctype="application/msword"; break;
        case "xls": $ctype="application/vnd.ms-excel"; break;
        case "xlsx": $ctype="application/vnd.ms-excel"; break;
        case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
        case "pptx": $ctype="application/vnd.ms-powerpoint"; break;
        case "gif": $ctype="image/gif"; break;
        case "png": $ctype="image/png"; break;
        case "txt": $ctype="text/plain"; break;
        case "htm": $ctype="text/html"; break;
        case "html": $ctype="text/html"; break;
        case "avi": $ctype="video/x-msvideo"; break;
        case "mov": $ctype="video/quicktime"; break;
        case "mpe": $ctype="video/mpeg"; break;
        case "mpg": $ctype="video/mpeg"; break;
        case "mpeg": $ctype="video/mpeg"; break;
        case "jpe": case "jpeg":
        case "jpg": $ctype="image/jpeg"; break;
        case "jpeg": $ctype="image/jpeg"; break;
        default: $ctype="application/force-download";
    }

    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false); // required for certain browsers
    header("Content-Type: $ctype");
    header('Content-Disposition: attachment; filename="' . $nombre_archivo .'"');
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($adj));
    readfile("$adj");
}
else
if($actor == 'FACTURACIONSEGURIDAD')
{
	$con = mysqli_query($conectar,"select cas_adjunto_fact_seguridad from cajero_seguridad where caj_clave_int = '".$clacaj."'");
	$dato = mysqli_fetch_array($con);
	$adj = $dato['cas_adjunto_fact_seguridad'];
	
	$ext = extension_archivo($adj);
	$nombre_archivo=$clacaj."-SEGURIDAD_FACTURACION.".$ext;
	switch ($ext) {
        case "pdf": $ctype="application/pdf"; break;
        case "exe": $ctype="application/octet-stream"; break;
        case "zip": $ctype="application/zip"; break;
        case "doc": $ctype="application/msword"; break;
        case "docx": $ctype="application/msword"; break;
        case "xls": $ctype="application/vnd.ms-excel"; break;
        case "xlsx": $ctype="application/vnd.ms-excel"; break;
        case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
        case "pptx": $ctype="application/vnd.ms-powerpoint"; break;
        case "gif": $ctype="image/gif"; break;
        case "png": $ctype="image/png"; break;
        case "txt": $ctype="text/plain"; break;
        case "htm": $ctype="text/html"; break;
        case "html": $ctype="text/html"; break;
        case "avi": $ctype="video/x-msvideo"; break;
        case "mov": $ctype="video/quicktime"; break;
        case "mpe": $ctype="video/mpeg"; break;
        case "mpg": $ctype="video/mpeg"; break;
        case "mpeg": $ctype="video/mpeg"; break;
        case "jpe": case "jpeg":
        case "jpg": $ctype="image/jpeg"; break;
        case "jpeg": $ctype="image/jpeg"; break;
        default: $ctype="application/force-download";
    }

    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false); // required for certain browsers
    header("Content-Type: $ctype");
    header('Content-Disposition: attachment; filename="' . $nombre_archivo .'"');
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($adj));
    readfile("$adj");
}
else
if($actor == 'INFORMEOTINMOBILIARIA')
{
	$con = mysqli_query($conectar,"select caf_adjunto_ot_inmobiliaria from cajero_facturacion where caj_clave_int = '".$clacaj."'");
	$dato = mysqli_fetch_array($con);
	$adj = $dato['caf_adjunto_ot_inmobiliaria'];
	
	$ext = extension_archivo($adj);
	$nombre_archivo=$clacaj."FACTURACION_INMOBILIARIA.".$ext;
	switch ($ext) {
        case "pdf": $ctype="application/pdf"; break;
        case "exe": $ctype="application/octet-stream"; break;
        case "zip": $ctype="application/zip"; break;
        case "doc": $ctype="application/msword"; break;
        case "docx": $ctype="application/msword"; break;
        case "xls": $ctype="application/vnd.ms-excel"; break;
        case "xlsx": $ctype="application/vnd.ms-excel"; break;
        case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
        case "pptx": $ctype="application/vnd.ms-powerpoint"; break;
        case "gif": $ctype="image/gif"; break;
        case "png": $ctype="image/png"; break;
        case "txt": $ctype="text/plain"; break;
        case "htm": $ctype="text/html"; break;
        case "html": $ctype="text/html"; break;
        case "avi": $ctype="video/x-msvideo"; break;
        case "mov": $ctype="video/quicktime"; break;
        case "mpe": $ctype="video/mpeg"; break;
        case "mpg": $ctype="video/mpeg"; break;
        case "mpeg": $ctype="video/mpeg"; break;
        case "jpe": case "jpeg":
        case "jpg": $ctype="image/jpeg"; break;
        case "jpeg": $ctype="image/jpeg"; break;
        default: $ctype="application/force-download";
    }

    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false); // required for certain browsers
    header("Content-Type: $ctype");
    header('Content-Disposition: attachment; filename="' . $nombre_archivo .'"');
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($adj));
    readfile("$adj");
}
else
if($actor == 'INFORMEOTVISITA')
{
	$con = mysqli_query($conectar,"select caf_adjunto_ot_visita from cajero_facturacion where caj_clave_int = '".$clacaj."'");
	$dato = mysqli_fetch_array($con);
	$adj = $dato['caf_adjunto_ot_visita'];
	
	$ext = extension_archivo($adj);
	$nombre_archivo=$clacaj."FACTURACION_VISITA.".$ext;
	switch ($ext) {
        case "pdf": $ctype="application/pdf"; break;
        case "exe": $ctype="application/octet-stream"; break;
        case "zip": $ctype="application/zip"; break;
        case "doc": $ctype="application/msword"; break;
        case "docx": $ctype="application/msword"; break;
        case "xls": $ctype="application/vnd.ms-excel"; break;
        case "xlsx": $ctype="application/vnd.ms-excel"; break;
        case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
        case "pptx": $ctype="application/vnd.ms-powerpoint"; break;
        case "gif": $ctype="image/gif"; break;
        case "png": $ctype="image/png"; break;
        case "txt": $ctype="text/plain"; break;
        case "htm": $ctype="text/html"; break;
        case "html": $ctype="text/html"; break;
        case "avi": $ctype="video/x-msvideo"; break;
        case "mov": $ctype="video/quicktime"; break;
        case "mpe": $ctype="video/mpeg"; break;
        case "mpg": $ctype="video/mpeg"; break;
        case "mpeg": $ctype="video/mpeg"; break;
        case "jpe": case "jpeg":
        case "jpg": $ctype="image/jpeg"; break;
        case "jpeg": $ctype="image/jpeg"; break;
        default: $ctype="application/force-download";
    }

    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false); // required for certain browsers
    header("Content-Type: $ctype");
    header('Content-Disposition: attachment; filename="' . $nombre_archivo .'"');
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($adj));
    readfile("$adj");
}
else
if($actor == 'INFORMEOTDISENO')
{
	$con = mysqli_query($conectar,"select caf_adjunto_ot_diseno from cajero_facturacion where caj_clave_int = '".$clacaj."'");
	$dato = mysqli_fetch_array($con);
	$adj = $dato['caf_adjunto_ot_diseno'];
	
	$ext = extension_archivo($adj);
	$nombre_archivo=$clacaj."FACTURACION_DISENO.".$ext;
	switch ($ext) {
        case "pdf": $ctype="application/pdf"; break;
        case "exe": $ctype="application/octet-stream"; break;
        case "zip": $ctype="application/zip"; break;
        case "doc": $ctype="application/msword"; break;
        case "docx": $ctype="application/msword"; break;
        case "xls": $ctype="application/vnd.ms-excel"; break;
        case "xlsx": $ctype="application/vnd.ms-excel"; break;
        case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
        case "pptx": $ctype="application/vnd.ms-powerpoint"; break;
        case "gif": $ctype="image/gif"; break;
        case "png": $ctype="image/png"; break;
        case "txt": $ctype="text/plain"; break;
        case "htm": $ctype="text/html"; break;
        case "html": $ctype="text/html"; break;
        case "avi": $ctype="video/x-msvideo"; break;
        case "mov": $ctype="video/quicktime"; break;
        case "mpe": $ctype="video/mpeg"; break;
        case "mpg": $ctype="video/mpeg"; break;
        case "mpeg": $ctype="video/mpeg"; break;
        case "jpe": case "jpeg":
        case "jpg": $ctype="image/jpeg"; break;
        case "jpeg": $ctype="image/jpeg"; break;
        default: $ctype="application/force-download";
    }

    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false); // required for certain browsers
    header("Content-Type: $ctype");
    header('Content-Disposition: attachment; filename="' . $nombre_archivo .'"');
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($adj));
    readfile("$adj");
}
else
if($actor == 'INFORMEOTLICENCIA')
{
	$con = mysqli_query($conectar,"select caf_adjunto_ot_licencia from cajero_facturacion where caj_clave_int = '".$clacaj."'");
	$dato = mysqli_fetch_array($con);
	$adj = $dato['caf_adjunto_ot_licencia'];
	
	$ext = extension_archivo($adj);
	$nombre_archivo=$clacaj."FACTURACION_LICENCIA.".$ext;
	switch ($ext) {
        case "pdf": $ctype="application/pdf"; break;
        case "exe": $ctype="application/octet-stream"; break;
        case "zip": $ctype="application/zip"; break;
        case "doc": $ctype="application/msword"; break;
        case "docx": $ctype="application/msword"; break;
        case "xls": $ctype="application/vnd.ms-excel"; break;
        case "xlsx": $ctype="application/vnd.ms-excel"; break;
        case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
        case "pptx": $ctype="application/vnd.ms-powerpoint"; break;
        case "gif": $ctype="image/gif"; break;
        case "png": $ctype="image/png"; break;
        case "txt": $ctype="text/plain"; break;
        case "htm": $ctype="text/html"; break;
        case "html": $ctype="text/html"; break;
        case "avi": $ctype="video/x-msvideo"; break;
        case "mov": $ctype="video/quicktime"; break;
        case "mpe": $ctype="video/mpeg"; break;
        case "mpg": $ctype="video/mpeg"; break;
        case "mpeg": $ctype="video/mpeg"; break;
        case "jpe": case "jpeg":
        case "jpg": $ctype="image/jpeg"; break;
        case "jpeg": $ctype="image/jpeg"; break;
        default: $ctype="application/force-download";
    }

    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false); // required for certain browsers
    header("Content-Type: $ctype");
    header('Content-Disposition: attachment; filename="' . $nombre_archivo .'"');
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($adj));
    readfile("$adj");
}
else
if($actor == 'INFORMEOTINTERVENTORIA')
{
	$con = mysqli_query($conectar,"select caf_adjunto_ot_interventoria from cajero_facturacion where caj_clave_int = '".$clacaj."'");
	$dato = mysqli_fetch_array($con);
	$adj = $dato['caf_adjunto_ot_interventoria'];
	
	$ext = extension_archivo($adj);
	$nombre_archivo=$clacaj."FACTURACION_INTERVENTORIA.".$ext;
	switch ($ext) {
        case "pdf": $ctype="application/pdf"; break;
        case "exe": $ctype="application/octet-stream"; break;
        case "zip": $ctype="application/zip"; break;
        case "doc": $ctype="application/msword"; break;
        case "docx": $ctype="application/msword"; break;
        case "xls": $ctype="application/vnd.ms-excel"; break;
        case "xlsx": $ctype="application/vnd.ms-excel"; break;
        case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
        case "pptx": $ctype="application/vnd.ms-powerpoint"; break;
        case "gif": $ctype="image/gif"; break;
        case "png": $ctype="image/png"; break;
        case "txt": $ctype="text/plain"; break;
        case "htm": $ctype="text/html"; break;
        case "html": $ctype="text/html"; break;
        case "avi": $ctype="video/x-msvideo"; break;
        case "mov": $ctype="video/quicktime"; break;
        case "mpe": $ctype="video/mpeg"; break;
        case "mpg": $ctype="video/mpeg"; break;
        case "mpeg": $ctype="video/mpeg"; break;
        case "jpe": case "jpeg":
        case "jpg": $ctype="image/jpeg"; break;
        case "jpeg": $ctype="image/jpeg"; break;
        default: $ctype="application/force-download";
    }

    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false); // required for certain browsers
    header("Content-Type: $ctype");
    header('Content-Disposition: attachment; filename="' . $nombre_archivo .'"');
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($adj));
    readfile("$adj");
}
else
if($actor == 'INFORMEOTCONSTRUCTOR')
{
	$con = mysqli_query($conectar,"select caf_adjunto_ot_constructor from cajero_facturacion where caj_clave_int = '".$clacaj."'");
	$dato = mysqli_fetch_array($con);
	$adj = $dato['caf_adjunto_ot_constructor'];
	
	$ext = extension_archivo($adj);
	$nombre_archivo=$clacaj."FACTURACION_CONSTRUCTOR.".$ext;
	switch ($ext) {
        case "pdf": $ctype="application/pdf"; break;
        case "exe": $ctype="application/octet-stream"; break;
        case "zip": $ctype="application/zip"; break;
        case "doc": $ctype="application/msword"; break;
        case "docx": $ctype="application/msword"; break;
        case "xls": $ctype="application/vnd.ms-excel"; break;
        case "xlsx": $ctype="application/vnd.ms-excel"; break;
        case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
        case "pptx": $ctype="application/vnd.ms-powerpoint"; break;
        case "gif": $ctype="image/gif"; break;
        case "png": $ctype="image/png"; break;
        case "txt": $ctype="text/plain"; break;
        case "htm": $ctype="text/html"; break;
        case "html": $ctype="text/html"; break;
        case "avi": $ctype="video/x-msvideo"; break;
        case "mov": $ctype="video/quicktime"; break;
        case "mpe": $ctype="video/mpeg"; break;
        case "mpg": $ctype="video/mpeg"; break;
        case "mpeg": $ctype="video/mpeg"; break;
        case "jpe": case "jpeg":
        case "jpg": $ctype="image/jpeg"; break;
        case "jpeg": $ctype="image/jpeg"; break;
        default: $ctype="application/force-download";
    }

    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false); // required for certain browsers
    header("Content-Type: $ctype");
    header('Content-Disposition: attachment; filename="' . $nombre_archivo .'"');
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($adj));
    readfile("$adj");
}
else
if($actor == 'INFORMEOTSEGURIDAD')
{
	$con = mysqli_query($conectar,"select caf_adjunto_ot_seguridad from cajero_facturacion where caj_clave_int = '".$clacaj."'");
	$dato = mysqli_fetch_array($con);
	$adj = $dato['caf_adjunto_ot_seguridad'];
	
	$ext = extension_archivo($adj);
	$nombre_archivo=$clacaj."FACTURACION_SEGURIDAD.".$ext;
	switch ($ext) {
        case "pdf": $ctype="application/pdf"; break;
        case "exe": $ctype="application/octet-stream"; break;
        case "zip": $ctype="application/zip"; break;
        case "doc": $ctype="application/msword"; break;
        case "docx": $ctype="application/msword"; break;
        case "xls": $ctype="application/vnd.ms-excel"; break;
        case "xlsx": $ctype="application/vnd.ms-excel"; break;
        case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
        case "pptx": $ctype="application/vnd.ms-powerpoint"; break;
        case "gif": $ctype="image/gif"; break;
        case "png": $ctype="image/png"; break;
        case "txt": $ctype="text/plain"; break;
        case "htm": $ctype="text/html"; break;
        case "html": $ctype="text/html"; break;
        case "avi": $ctype="video/x-msvideo"; break;
        case "mov": $ctype="video/quicktime"; break;
        case "mpe": $ctype="video/mpeg"; break;
        case "mpg": $ctype="video/mpeg"; break;
        case "mpeg": $ctype="video/mpeg"; break;
        case "jpe": case "jpeg":
        case "jpg": $ctype="image/jpeg"; break;
        case "jpeg": $ctype="image/jpeg"; break;
        default: $ctype="application/force-download";
    }
    header("Pragma: public"); // required
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false); // required for certain browsers
    header("Content-Type: $ctype");
    header('Content-Disposition: attachment; filename="' . $nombre_archivo .'"');
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($adj));
    readfile("$adj");
}
?>