<?php
	include('../../data/Conexion.php');
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_SESSION['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];
		
	// verifica si no se ha loggeado	
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");
	$opcion = $_POST['opcion'];
function CORREGIRTEXTO($string)
{
    $string = str_replace("REEMPLAZARNUMERAL", "#", $string);
    $string = str_replace("REEMPLAZARMAS", "+",$string);
    $string = str_replace("REEMPLAZARMENOS", "-", $string);
    return $string;
}
  /*  function CALCULARFECHA($fi,$d)
    {
        $confec = mysqli_query($conectar,"select DATE_ADD('".$fi."', INTERVAL $d DAY) fecha from usuario LIMIT 1");
        $datofec = mysqli_fetch_array($confec);
        return $datofec['fecha'];
    }
*/

if($opcion=="TODOS")
{
    $est = $_POST['est'];
   ?>
    <table style="width: 100%">
        <tr>
            <td align="left" class="auto-style2">
                <table style="width: 50%">
                    <tr>
                        <td style="width: 60px" rowspan="2"><strong>
                                <span class="auto-style13">Filtro:</span><img src="../../img/buscar.png" alt="" height="18" width="15" /></strong></td>
                        <td style="width: 150px; height: 31px;">
                            <input class="inputs" onkeyup="BUSCAR('CAJERO','')" name="busconsecutivo" id="busconsecutivo" maxlength="70" type="text" placeholder="Consecutivo" style="width: 150px" />
                        </td>
                        <td style="width: 150px; height: 31px;">
                            <input class="inputs" onkeyup="BUSCAR('CAJERO','')" name="busnombre" id="busnombre" maxlength="70" type="text" placeholder="Nombre Cajero" style="width: 150px" />
                        </td>
                        <td style="width: 150px; height: 31px;">
                            <input class="inputs" onkeyup="BUSCAR('CAJERO','')" name="busanocontable" id="busanocontable" maxlength="70" type="text" placeholder="Año Contable" style="width: 150px" />
                        </td>
                        <td style="width: 150px; height: 31px;" class="alinearizq">
                            <select name="buscentrocostos" id="buscentrocostos" onchange="BUSCAR('CAJERO','')" tabindex="4" class="inputs" style="width: 158px">
                                <option value="">-Centro Costos-</option>
                                <?php
                                $con = mysqli_query($conectar,"select * from centrocostos where cco_sw_activo = 1 order by cco_nombre");
                                $num = mysqli_num_rows($con);
                                for($i = 0; $i < $num; $i++)
                                {
                                    $dato = mysqli_fetch_array($con);
                                    $clave = $dato['cco_clave_int'];
                                    $nombre = $dato['cco_nombre'];
                                    ?>
                                    <option value="<?php echo $clave; ?>" <?php if($clave == $cencos){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </td>
                        <td align="left" style="height: 31px">
                            <input class="auto-style5" onkeyup="BUSCAR('CAJERO','')" name="buscodigo" id="buscodigo" maxlength="70" type="text" placeholder="Código Cajero" style="width: 150px" />
                        </td>
                        <td align="left" style="height: 31px">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 150px; height: 31px;">
                            <select name="busregion" id="busregion" onchange="BUSCAR('CAJERO','')" tabindex="6" class="inputs buslista" style="width: 148px">
                                <option value="">-Región-</option>
                                <?php
                                $con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo IS NULL and pog_nieto IS NULL order by pog_nombre");
                                $num = mysqli_num_rows($con);
                                for($i = 0; $i < $num; $i++)
                                {
                                    $dato = mysqli_fetch_array($con);
                                    $clave = $dato['pog_clave_int'];
                                    $nombre = $dato['pog_nombre'];
                                    ?>
                                    <option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
                                    <?php
                                }
                                ?>
                            </select></td>
                        <td style="width: 150px; height: 31px;">
                            <select name="busmunicipio" id="busmunicipio" onchange="BUSCAR('CAJERO','')" tabindex="8" class="inputs buslista" style="width: 148px">
                                <option value="">-Municipio-</option>
                                <?php
                                $con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo IS NULL and pog_nieto IS NOT NULL order by pog_nombre");
                                $num = mysqli_num_rows($con);
                                for($i = 0; $i < $num; $i++)
                                {
                                    $dato = mysqli_fetch_array($con);
                                    $clave = $dato['pog_clave_int'];
                                    $nombre = $dato['pog_nombre'];
                                    ?>
                                    <option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
                                    <?php
                                }
                                ?>
                            </select></td>
                        <td style="width: 150px; height: 31px;">
                            <select name="bustipologia" id="bustipologia" onchange="BUSCAR('CAJERO','')" tabindex="9" class="inputs buslista" style="width: 148px">
                                <option value="">-Tipología-</option>
                                <?php
                                $con = mysqli_query($conectar,"select * from tipologia where tip_sw_activo = 1 order by tip_nombre");
                                $num = mysqli_num_rows($con);
                                for($i = 0; $i < $num; $i++)
                                {
                                    $dato = mysqli_fetch_array($con);
                                    $clave = $dato['tip_clave_int'];
                                    $nombre = $dato['tip_nombre'];
                                    ?>
                                    <option value="<?php echo $clave; ?>" <?php if($clave == $tip){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </td>
                        <td style="width: 150px; height: 31px;" class="alinearizq">
                            <select name="bustipointervencion" id="bustipointervencion" onchange="VERMODALIDADES1(this.value)" tabindex="10" class="inputs buslista" style="width: 158px">
                                <option value="">-Tipo Intervención-</option>
                                <?php
                                $con = mysqli_query($conectar,"select * from tipointervencion order by tii_nombre");
                                $num = mysqli_num_rows($con);
                                for($i = 0; $i < $num; $i++)
                                {
                                    $dato = mysqli_fetch_array($con);
                                    $clave = $dato['tii_clave_int'];
                                    $nombre = $dato['tii_nombre'];
                                    ?>
                                    <option value="<?php echo $clave; ?>" <?php if($clave == $tipint){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
                                    <?php
                                }
                                ?>
                            </select></td>
                        <td align="left" style="height: 31px">
                            <div id="modalidades1" style="float:left">
                                <select name="busmodalidad" id="busmodalidad" onchange="BUSCAR('CAJERO','');" tabindex="8" class="inputs buslista" data-placeholder="-Seleccione-" style="width: 160px">
                                    <option value="">-Modalidad-</option>
                                    <?php
                                    $con = mysqli_query($conectar,"select * from modalidad order by mod_nombre");
                                    $num = mysqli_num_rows($con);
                                    for($i = 0; $i < $num; $i++)
                                    {
                                        $dato = mysqli_fetch_array($con);
                                        $clave = $dato['mod_clave_int'];
                                        $nombre = $dato['mod_nombre'];
                                        ?>
                                        <option value="<?php echo $clave; ?>" <?php if($moda == $clave){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </td>
                        <td align="left" style="height: 31px">
                            <select name="busactor" id="busactor" onchange="BUSCAR('CAJERO','')" tabindex="20" class="inputs buslista" style="width: 150px">
                                <option value="">-Actor-</option>
                                <?php
                                if($claprf == 1)
                                {
                                    $con = mysqli_query($conectar,"select * from usuario where usu_sw_inmobiliaria = 1 or usu_sw_visita = 1 or usu_sw_diseno = 1 or usu_sw_licencia = 1 or usu_sw_interventoria = 1 or usu_sw_constructor = 1 or usu_sw_seguridad = 1 order by usu_nombre");
                                }
                                else
                                {
                                    $con = mysqli_query($conectar,"select * from usuario where (usu_categoria = 2 and usu_sw_interventoria = 1 and usu_sw_activo = 1) OR (usu_clave_int NOT IN (select usu_clave_int from usuario_actor) and usu_sw_interventoria = 1 and usu_sw_activo = 1) order by usu_nombre");
                                }


                                $num = mysqli_num_rows($con);
                                for($i = 0; $i < $num; $i++)
                                {
                                    $dato = mysqli_fetch_array($con);
                                    $clave = $dato['usu_clave_int'];
                                    $nombre = $dato['usu_nombre'];
                                    ?>
                                    <option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">
                <div id="busqueda">

        </td>
    </tr>
    </table>
    <?php
    echo "<style onload=INICIALIZARLISTAS()></style>";
    echo "<style onload=BUSCAR('CAJERO','')></style>";
}
else if($opcion=="BUSCAR")
{
    ?>
    <script src="jslistacajeros.js"></script>
    <table style="width: 100%" class="table stripe table-striped" id="tbCajeros">
        <thead>
        <tr>
            <th class="alinearizq">&nbsp;</th>
            <th class="alinearizq"><strong>Consecutivo</strong></th>
            <th class="alinearizq"><strong>Código</strong></th>
            <th class="alinearizq"><strong>Nombre</strong></th>
            <th class="alinearizq"><strong>Padre/Hijo</strong></th>
            <th class="alinearizq"><strong>Región</strong></th>
            <th class="alinearizq"><strong>Tipología</strong></th>
            <th class="alinearizq"><strong>Tip. Intervención</strong></th>
            <th class="alinearizq"><strong>Estado</strong></th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        </tfoot>
        <tbody></tbody>
    </table>
    <?php
}
else if($opcion=="VERMODALIDADES")
{
    $tii = $_POST['tii'];
    if($tii=="")
    {
        $con = mysqli_query($conectar,"select * from modalidad order by mod_nombre");
    }
    else {
        $con = mysqli_query($conectar,"select * from modalidad where mod_clave_int in (select mod_clave_int from intervencion_modalidad where tii_clave_int = " . $tii . " or '" . $tii . "' IS NULL or '" . $tii . "' = '') order by mod_nombre");
    }
    $num = mysqli_num_rows($con);
    for($i = 0; $i < $num; $i++) {
        $dato = mysqli_fetch_array($con);
        $clave = $dato['mod_clave_int'];
        $nombre = $dato['mod_nombre'];
        $datos[] = array("id"=>$clave,"literal"=>$nombre);
    }
    echo json_encode($datos);
}

?>