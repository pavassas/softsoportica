function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function EDITAR(v,m)
{	
	if(m == 'REFERENCIAMAQUINA')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('editarreferenciamaquina').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#editarreferenciamaquina").html("<img alt='cargando' src='img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?editarrem=si&remedi="+v,true);
		ajax.send(null);
	}
}
function GUARDAR(v,id)
{
	if(v == 'REFERENCIAMAQUINA')
	{
		var rem = form1.referenciamaquina1.value;
		var lr = rem.length;
		var mar = form1.marca1.value;
		var act = form1.activo1.checked;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('datos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#datos").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?guardarrem=si&rem="+rem+"&mar="+mar+"&act="+act+"&lr="+lr+"&r="+id,true);
		ajax.send(null);
		if(rem != '' && mar != '' && lr >= 3)
		{
			setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
		}
	}
}
function NUEVO(v)
{
	if(v == 'REFERENCIAMAQUINA')
	{
		var rem = form1.referenciamaquina.value;
		var lr = rem.length;		
		var mar = form1.marca.value;
		var act = form1.activo.checked;
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('datos1').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#datos1").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?nuevareferenciamaquina=si&rem="+rem+"&mar="+mar+"&act="+act+"&lr="+lr,true);
		ajax.send(null);
		if(rem != '' && mar != '' && lr >= 3)
		{
			form1.referenciamaquina.value = '';
			form1.marca.value = '';
			setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
		}
	}
}
function CONSULTAMODULO(v)
{
	if(v == 'TODOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('referenciamaquina').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#referenciamaquina").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?todos=si",true);
		ajax.send(null);
	}
}
function BUSCAR(m)
{	
	if(m == 'REFERENCIAMAQUINA')
	{
		var rem = form1.referenciamaquina2.value;
		var act = form1.buscaractivos.value;
				
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('referenciamaquina').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#referenciamaquina").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?buscarrem=si&rem="+rem+"&act="+act,true);
		ajax.send(null);
	}
}