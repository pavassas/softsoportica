<?php
	include('../../data/Conexion.php');
	require("../../Classes/PHPMailer-master/class.phpmailer.php");
	
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_SESSION['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];
		
	date_default_timezone_set('America/Bogota');	
	// verifica si no se ha loggeado
	if(isset($_SESSION['nw']))
	{
	    if($_SESSION['nw']<time())
	    {
	        unset($_SESSION['nw']);
	        //echo "<script>alert('Tiempo agotado - Loguearse nuevamente');</script>";
	        header("LOCATION:../../data/logout.php");
	    }
	    else
	    {
	        $_SESSION['nw'] = time() + (60 * 60);
	    }
	}
	else
	{
	   //echo "<script>alert('Tiempo agotado - Loguearse nuevamente');</script>";
	   header("LOCATION:../../data/logout.php");
	}
	$fecha=date("Y/m/d H:i:s");
	$anocont = date("Y");
	require_once("lib/Zebra_Pagination.php");
	
	$con = mysqli_query($conectar,"select u.usu_clave_int,p.prf_clave_int,p.prf_descripcion,p.prf_sw_actualiza_info from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
	$dato = mysqli_fetch_array($con);
	$clausu = $dato['usu_clave_int'];
	$claprf = $dato['prf_clave_int'];
	$perfil = $dato['prf_descripcion'];
	$actualizainfo = $dato['prf_sw_actualiza_info'];
	
	$con = mysqli_query($conectar,"select per_metodo from permiso where prf_clave_int = '".$claprf."' and ven_clave_int = 4");
	$dato = mysqli_fetch_array($con);
	$metodo = $dato['per_metodo'];

function CORREGIRTEXTO($string)
{
    $string = str_replace("REEMPLAZARNUMERAL", "#", $string);
    $string = str_replace("REEMPLAZARMAS", "+",$string);
    $string = str_replace("REEMPLAZARMENOS", "-", $string);
    return $string;
}
	
	if($_GET['buscar'] == 'si')
	{
		$caj = $_GET['caj'];
		$anocon = $_GET['anocon'];
		$cencos = $_GET['cencos'];
		$cod = $_GET['cod'];
		$reg = $_GET['reg'];
		$mun = $_GET['mun'];
		$tip = $_GET['tip'];
		$tipint = $_GET['tipint'];
		$moda = $_GET['moda'];
		$actor = $_GET['actor'];
		$est = $_GET['est'];
		
		$seleccionados = explode(',',$caj);
		$num = count($seleccionados);
		$cajeros = array();
		for($i = 0; $i < $num; $i++)
		{
			if($seleccionados[$i] != '')
			{
				$cajeros[$i]=$seleccionados[$i];
			}
		}
		$listacajeros=implode(',',$cajeros);
?>
		<fieldset name="Group1">
		<legend align="center">CAJEROS ELIMINADOS<br>
		<button name="Accion" value="excel" title="Exportar a Excel" onclick="EXPORTAR()" type="button" style="cursor:pointer;">
		<img src="../../images/excel.png" height="25" width="25"></button>
		</legend>
		<table style="width: 100%">
		<tr>
			<td class="alinearizq"><strong>Cajero</strong></td>
			<td class="alinearizq"><strong>Nombre</strong></td>
			<td class="alinearizq"><strong>Dirección</strong></td>
			<td class="alinearizq"><strong>Año Cont.</strong></td>
			<td class="alinearizq"><strong>Región</strong></td>
			<td class="alinearizq"><strong>Departamento</strong></td>
			<td class="alinearizq"><strong>Municipio</strong></td>
			<td class="alinearizq"><strong>Tipología</strong></td>
			<td class="alinearizq"><strong>Tip. Intervención</strong></td>
			<td class="alinearizq"><strong>Modalidad</strong></td>
			<td class="alinearizq"><strong>Estado</strong></td>
		</tr>
		<?php
		$sql = '';
		if($listacajeros == '')
		{
			$sql = "(c.caj_clave_int = '".$consec."' OR '".$consec."' IS NULL OR '".$consec."' = '') and (caj_nombre LIKE REPLACE('%".$nom."%',' ','%') OR '".$nom."' IS NULL OR '".$nom."' = '') and (caj_ano_contable = '".$anocon."' OR '".$anocon."' IS NULL OR '".$anocon."' = '') and (cco_clave_int = '".$cencos."' OR '".$cencos."' IS NULL OR '".$cencos."' = '') and (caj_codigo_cajero LIKE REPLACE('%".$cod."%',' ','%') OR '".$cod."' IS NULL OR '".$cod."' = '') and (caj_region = '".$reg."' OR '".$reg."' IS NULL OR '".$reg."' = '') and (caj_municipio = '".$mun."' OR '".$mun."' IS NULL OR '".$mun."' = '') and (tip_clave_int = '".$tip."' OR '".$tip."' IS NULL OR '".$tip."' = '') and (tii_clave_int = '".$tipint."' OR '".$tipint."' IS NULL OR '".$tipint."' = '') and (mod_clave_int = '".$moda."' OR '".$moda."' IS NULL OR '".$moda."' = '') and (caj_estado_proyecto = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') and (cai_inmobiliaria = '".$actor."' or cav_visitante = '".$actor."' or cad_disenador = '".$actor."' or cal_gestionador = '".$actor."' or cin_interventor = '".$actor."' or cac_constructor = '".$actor."' or cas_instalador = '".$actor."' or '".$actor."' IS NULL or '".$actor."' = '') and c.caj_sw_eliminado = 1";
		}
		else
		{
			$sql = "(c.caj_clave_int = '".$consec."' OR '".$consec."' IS NULL OR '".$consec."' = '') and (caj_nombre LIKE REPLACE('%".$nom."%',' ','%') OR '".$nom."' IS NULL OR '".$nom."' = '') and (caj_ano_contable = '".$anocon."' OR '".$anocon."' IS NULL OR '".$anocon."' = '') and (cco_clave_int = '".$cencos."' OR '".$cencos."' IS NULL OR '".$cencos."' = '') and (caj_codigo_cajero LIKE REPLACE('%".$cod."%',' ','%') OR '".$cod."' IS NULL OR '".$cod."' = '') and (caj_region = '".$reg."' OR '".$reg."' IS NULL OR '".$reg."' = '') and (caj_municipio = '".$mun."' OR '".$mun."' IS NULL OR '".$mun."' = '') and (tip_clave_int = '".$tip."' OR '".$tip."' IS NULL OR '".$tip."' = '') and (tii_clave_int = '".$tipint."' OR '".$tipint."' IS NULL OR '".$tipint."' = '') and (mod_clave_int = '".$moda."' OR '".$moda."' IS NULL OR '".$moda."' = '') and (caj_estado_proyecto = '".$est."' OR '".$est."' IS NULL OR '".$est."' = '') and (cai_inmobiliaria = '".$actor."' or cav_visitante = '".$actor."' or cad_disenador = '".$actor."' or cal_gestionador = '".$actor."' or cin_interventor = '".$actor."' or cac_constructor = '".$actor."' or cas_instalador = '".$actor."' or '".$actor."' IS NULL or '".$actor."' = '') and c.caj_sw_eliminado = 1 and c.caj_clave_int in (".$listacajeros.")";
		}
		
		$query = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int) inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where ".$sql."");
		//$res = $con->query($query);
		$num_registros = mysqli_num_rows($query);

		$resul_x_pagina = 100;
		//Paginar:
		$paginacion = new Zebra_Pagination();
		$paginacion->records($num_registros);
		$paginacion->records_per_page($resul_x_pagina);
		
		$con = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int) inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where ".$sql." LIMIT ".(($paginacion->get_page() - 1) * $resul_x_pagina). ',' .$resul_x_pagina);
		$num = mysqli_num_rows($con);
		for($i = 0; $i < $num; $i++)
		{
			$dato = mysqli_fetch_array($con);
			//Info Base
			$clacaj = $dato['clacaj'];
			$nomcaj = CORREGIRTEXTO($dato['caj_nombre']);
			$dir = CORREGIRTEXTO($dato['caj_direccion']);
			$anocon = $dato['caj_ano_contable'];
			$reg = $dato['caj_region'];
			$dep = $dato['caj_departamento'];
			$mun = $dato['caj_municipio'];
			$tip = $dato['tip_clave_int'];
			$tipint = $dato['tii_clave_int'];
			$mod = $dato['mod_clave_int'];
			$conmod = mysqli_query($conectar,"select mod_nombre from modalidad where mod_clave_int = '".$mod."'");
			$datomod = mysqli_fetch_array($conmod);
			$moda = $datomod['mod_nombre'];
			//Info Secun
			$codcaj = $dato['caj_codigo_cajero'];
			$cencos = $dato['cco_clave_int'];
			$refmaq = $dato['rem_clave_int'];
			$ubi = $dato['ubi_clave_int'];
			$codsuc = $dato['caj_codigo_suc'];
			$ubiamt = $dato['caj_ubicacion_atm'];
			$ati = $dato['ati_clave_int'];
			$rie = $dato['caj_riesgo'];
			$are = $dato['are_clave_int'];
			$codrec = $dato['caj_cod_recibido_monto'];
			$codrec = $dato['caj_fecha_creacion'];
			//Info Inmobiliaria
			$reqinm = $dato['cai_req_inmobiliaria'];
			$inmob = $dato['cai_inmobiliaria'];
			$feciniinmob = $dato['cai_fecha_ini_inmobiliaria'];
			$estinmob = $dato['esi_clave_int'];
			$fecentinmob = $dato['cai_fecha_entrega_info_inmobiliaria'];
			//Info Visita Local
			$reqvis = $dato['cav_req_visita_local'];
			$vis = $dato['cav_visitante'];
			$fecvis = $dato['cav_fecha_visita'];
			//Info Diseño
			$reqdis = $dato['cad_req_diseno'];
			$dis = $dato['cad_disenador'];
			$fecinidis = $dato['cad_fecha_inicio_diseno'];
			$estdis = $dato['esd_clave_int'];
			$fecentdis = $dato['cad_fecha_entrega_info_diseno'];
			//Info Licencia
			$reqlic = $dato['cal_req_licencia'];
			$lic = $dato['cal_gestionador'];
			$fecinilic = $dato['cal_fecha_inicio_licencia'];
			$estlic = $dato['esl_clave_int'];
			$fecentlic = $dato['cal_fecha_entrega_info_licencia'];
			//Info Interventoria
			$int = $dato['cin_interventor'];
			$fecteoent = $dato['cin_fecha_teorica_entrega'];
			$feciniobra = $dato['cin_fecha_inicio_obra'];
			$fecpedsum = $dato['cin_fecha_pedido_suministro'];
			$aprovcotiz = $dato['cin_sw_aprov_cotizacion'];
			$cancom = $dato['can_clave_int'];
			$aprovliqui = $dato['cin_sw_aprov_liquidacion'];
			$swimgnex = $dato['cin_sw_img_nexos'];
			$swfot = $dato['cin_sw_fotos'];
			$swact = $dato['cin_sw_actas'];
			$estpro = $dato['caj_estado_proyecto'];
			//Info Constructor
			$cons = $dato['cac_constructor'];
			$cons = $dato['cac_fecha_entrega_atm'];
			$cons = $dato['cac_porcentaje_avance'];
			$cons = $dato['cac_fecha_entrega_cotizacion'];
			$cons = $dato['cac_fecha_entrega_liquidacion'];
			//Info Instalador Seguridad
			$seg = $dato['cas_instalador'];
			$fecingseg = $dato['cas_fecha_ingreso_obra_inst'];
			$codmon = $dato['cas_codigo_monitoreo'];
		?>
		<tr>
			<td class="alinearizq"><?php echo $clacaj; ?></td>
			<td class="alinearizq"><?php echo $nomcaj; ?></td>
			<td class="alinearizq"><?php echo $dir; ?></td>
			<td class="alinearizq"><?php echo $anocon; ?></td>
			<td class="alinearizq">
			<?php 
				$sql = mysqli_query($conectar,"select pog_nombre from posicion_geografica where pog_clave_int = '".$reg."'");
				$datosql = mysqli_fetch_array($sql);
				$nomreg = $datosql['pog_nombre'];
				echo $nomreg; 
			?>
			</td>
			<td class="alinearizq">
			<?php 
				$sql = mysqli_query($conectar,"select pog_nombre from posicion_geografica where pog_clave_int = '".$dep."'");
				$datosql = mysqli_fetch_array($sql);
				$nomdep = $datosql['pog_nombre'];
				echo $nomdep;
			?>
			</td>
			<td class="alinearizq">
			<?php 
				$sql = mysqli_query($conectar,"select pog_nombre from posicion_geografica where pog_clave_int = '".$mun."'");
				$datosql = mysqli_fetch_array($sql);
				$nommun = $datosql['pog_nombre'];
				echo $nommun;
			?>
			</td>
			<td class="alinearizq">
			<?php 
				$sql = mysqli_query($conectar,"select tip_nombre from tipologia where tip_clave_int = '".$tip."'");
				$datosql = mysqli_fetch_array($sql);
				$nomtip = $datosql['tip_nombre'];
				echo $nomtip;
			?>
			</td>
			<td class="alinearizq">
			<?php 
				$sql = mysqli_query($conectar,"select tii_nombre from tipointervencion where tii_clave_int = '".$tipint."'");
				$datosql = mysqli_fetch_array($sql);
				$nomtii = $datosql['tii_nombre'];
				echo $nomtii;
			?>
			</td>
			<td class="alinearizq">
			<?php if($mod <= 0){ echo "Sin Definir "; }else{ echo $moda; } ?></td>
			<td class="alinearizq"><?php if($estpro == 1){ echo "ACTIVO"; }elseif($estpro == 2){ echo "SUSPENDIDO"; }elseif($estpro == 3){ echo "ENTREGADO"; }elseif($estpro == 4){ echo "CANCELADO"; }elseif($estpro == 5){ echo "PROGRAMADO"; } ?></td>
		</tr>
		<tr>
			<td class="alinearizq" colspan="11">
			<hr>
			</td>
		</tr>
		<?php
		}
		?>
		</table>
		</fieldset>
<?php
		$paginacion->render();
		exit();
	}
	if($_GET['verciudades'] == 'si')
	{
		$reg = $_GET['reg'];
?>
		<select name="busmunicipio" id="busmunicipio" onchange="BUSCAR('CAJERO','')" tabindex="8" data-placeholder="-Seleccione-" class="inputs" style="width: 140px">
		<option value="">-Municipio-</option>
		<?php
			$con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo IS NULL and pog_nieto IS NOT NULL and pog_nieto IN (select pog_clave_int from posicion_geografica where pog_hijo = '".$reg."') order by pog_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clave = $dato['pog_clave_int'];
				$nombre = $dato['pog_nombre'];
		?>
			<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
		<?php
			}
		?>
		</select>
<?php
		exit();
	}
	if($_GET['vermodalidades1'] == 'si')
	{
		$tii = $_GET['tii'];
?>
		<select name="busmodalidad" id="busmodalidad" onchange="BUSCAR('CAJERO','')" tabindex="8" class="inputs" data-placeholder="-Seleccione-" style="width: 140px">
		<option value="">-Seleccione-</option>
		<?php
			$con = mysqli_query($conectar,"select * from modalidad where mod_clave_int in (select mod_clave_int from intervencion_modalidad where tii_clave_int = ".$tii.") order by mod_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clave = $dato['mod_clave_int'];
				$nombre = $dato['mod_nombre'];
		?>
			<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
		<?php
			}
		?>
		</select>

<?php
		exit();
	}
?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>SEGUIMIENTO CAJEROS</title>
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />

<?php //VALIDACIONES ?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="llamadas.js"></script>

<script type="text/javascript" src="../../js/jquery.searchabledropdown-1.0.8.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#buscentrocostos").searchable();
	$("#busregion").searchable();
	$("#busmunicipio").searchable();
	$("#bustipologia").searchable();
	$("#bustipointervencion").searchable();
	$("#busmodalidad").searchable();
	$("#busactor").searchable();
	$("#busestado").searchable();
});
function REFRESCARLISTACIUDADES()
{
	$(document).ready(function() {
		$("#busmunicipio").searchable();
	});
}
function REFRESCARLISTAMODALIDADES1()
{
	$(document).ready(function() {
		$("#busmodalidad").searchable();
	});
}
</script>

<?php //CALENDARIO ?>
<link type="text/css" rel="stylesheet" href="../../css/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
<SCRIPT type="text/javascript" src="../../js/dhtmlgoodies_calendar.js?random=20060118"></script>

<link rel="stylesheet" href="../../css/jquery-ui.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

<script>
function OCULTARSCROLL()
{
	parent.autoResize('iframe29');
	setTimeout("parent.autoResize('iframe29')",500);
	setTimeout("parent.autoResize('iframe29')",1000);
	setTimeout("parent.autoResize('iframe29')",1500);
	setTimeout("parent.autoResize('iframe29')",2000);
	setTimeout("parent.autoResize('iframe29')",2500);
	setTimeout("parent.autoResize('iframe29')",3000);
	setTimeout("parent.autoResize('iframe29')",3500);
	setTimeout("parent.autoResize('iframe29')",4000);
	setTimeout("parent.autoResize('iframe29')",4500);
	setTimeout("parent.autoResize('iframe29')",5000);
	setTimeout("parent.autoResize('iframe29')",5500);
	setTimeout("parent.autoResize('iframe29')",6000);
	setTimeout("parent.autoResize('iframe29')",6500);
	setTimeout("parent.autoResize('iframe29')",7000);
	setTimeout("parent.autoResize('iframe29')",7500);
	setTimeout("parent.autoResize('iframe29')",8000);
	setTimeout("parent.autoResize('iframe29')",8500);
	setTimeout("parent.autoResize('iframe29')",9000);
	setTimeout("parent.autoResize('iframe29')",9500);
	setTimeout("parent.autoResize('iframe29')",10000);
}
parent.autoResize('iframe29');
setTimeout("parent.autoResize('iframe29')",500);
setTimeout("parent.autoResize('iframe29')",1000);
setTimeout("parent.autoResize('iframe29')",1500);
setTimeout("parent.autoResize('iframe29')",2000);
setTimeout("parent.autoResize('iframe29')",2500);
setTimeout("parent.autoResize('iframe29')",3000);
setTimeout("parent.autoResize('iframe29')",3500);
setTimeout("parent.autoResize('iframe29')",4000);
setTimeout("parent.autoResize('iframe29')",4500);
setTimeout("parent.autoResize('iframe29')",5000);
setTimeout("parent.autoResize('iframe29')",5500);
setTimeout("parent.autoResize('iframe29')",6000);
setTimeout("parent.autoResize('iframe29')",6500);
setTimeout("parent.autoResize('iframe29')",7000);
setTimeout("parent.autoResize('iframe29')",7500);
setTimeout("parent.autoResize('iframe29')",8000);
setTimeout("parent.autoResize('iframe29')",8500);
setTimeout("parent.autoResize('iframe29')",9000);
setTimeout("parent.autoResize('iframe29')",9500);
setTimeout("parent.autoResize('iframe29')",10000);
</script>
<?php //********ESTAS LIBRERIAS JS Y CSS SIRVEN PARA HACER LA BUSQUEDA DINAMICA CON CHECKLIST************//?>
<link rel="stylesheet" type="text/css" href="../../css/checklist/jquery.multiselect.css" />
<link rel="stylesheet" type="text/css" href="../../css/checklist/jquery.multiselect.filter.css" />
<link rel="stylesheet" type="text/css" href="../../css/checklist/styleselect.css" />
<link rel="stylesheet" type="text/css" href="../../css/checklist/prettify.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script type="text/javascript" src="../../js/checklist/jquery.multiselect.js"></script>
<script type="text/javascript" src="../../js/checklist/jquery.multiselect.filter.js"></script>
<script type="text/javascript" src="../../js/checklist/prettify.js"></script>
<?php //**************************************************************************************************//?>

</head>
<body style="background-color:#FDFDFC" id="content">
<form name="form1" id="form1" method="post">
<!--[if lte IE 7]>
<div class="ieWarning">Este navegador no es compatible con el sistema. Por favor, use Chrome, Safari, Firefox o Internet Explorer 8 o superior.</div>
<![endif]-->
	<input name="ocultoestado" id="ocultoestado" value="" type="hidden" />
	<table style="width: 100%">
		<tr>
			<td class="auto-style2" colspan="5" align="center">
			<div id="cajeros">
				<table style="width:100%">
			<tr>
				<td align="left" class="auto-style2">
					<table style="width: 50%">
						<tr>
							<td style="width: 60px" rowspan="2"><strong>
							<span class="auto-style13">Filtro:</span><img src="../../img/buscar.png" alt="" height="18" width="15" /></strong></td>
							<td style="width: 150px; height: 31px;" align="center">
							<strong>CAJERO:</strong></td>
							<td style="height: 31px;" colspan="2">
							<select multiple="multiple" onchange="BUSCAR('CAJERO','')" name="buscajero" id="buscajero" style="width:320px">
							<?php
								$con = mysqli_query($conectar,"select * from cajero where caj_sw_eliminado = 1 order by caj_nombre");
								$num = mysqli_num_rows($con);
								for($i = 0; $i < $num; $i++)
								{
									$dato = mysqli_fetch_array($con);
									$clave = $dato['caj_clave_int'];
									$nombre = $dato['caj_nombre'];
							?>
								<option value="<?php echo $clave; ?>"><?php echo $clave." - ".$nombre; ?></option>
							<?php
								}
							?>
							</select>
							</td>
							<td style="width: 150px; height: 31px;" class="alinearizq">
							<input class="inputs" onkeyup="BUSCAR('CAJERO','')" name="busanocontable" id="busanocontable" maxlength="70" type="text" placeholder="Año Contable" style="width: 150px" /></td>
							<td align="left" style="height: 31px">
							<select name="buscentrocostos" id="buscentrocostos" onchange="BUSCAR('CAJERO','')" tabindex="4" class="inputs" style="width: 158px">
							<option value="">-Centro Costos-</option>
							<?php
								$con = mysqli_query($conectar,"select * from centrocostos where cco_sw_activo = 1 order by cco_nombre");
								$num = mysqli_num_rows($con);
								for($i = 0; $i < $num; $i++)
								{
									$dato = mysqli_fetch_array($con);
									$clave = $dato['cco_clave_int'];
									$nombre = $dato['cco_nombre'];
							?>
								<option value="<?php echo $clave; ?>" <?php if($clave == $cencos){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
							<?php
								}
							?>
							</select></td>
							<td align="left" style="height: 31px">
							<input class="auto-style5" onkeyup="BUSCAR('CAJERO','')" name="buscodigo" id="buscodigo" maxlength="70" type="text" placeholder="Código Cajero" style="width: 150px" /></td>
							<td align="left" style="height: 31px">
							&nbsp;</td>
						</tr>
						<tr>
							<td style="width: 150px; height: 31px;">
							<select name="busregion" id="busregion" onchange="BUSCAR('CAJERO','');VERCIUDADES(this.value)" tabindex="6" class="inputs" style="width: 148px">
								<option value="">-Región-</option>
								<?php
									$con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo IS NULL and pog_nieto IS NULL order by pog_nombre");
									$num = mysqli_num_rows($con);
									for($i = 0; $i < $num; $i++)
									{
										$dato = mysqli_fetch_array($con);
										$clave = $dato['pog_clave_int'];
										$nombre = $dato['pog_nombre'];
								?>
									<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
								<?php
									}
								?>
								</select></td>
							<td style="width: 150px; height: 31px;">
							<div id="ciudades" style="float:left">
								<select name="busmunicipio" id="busmunicipio" onchange="BUSCAR('CAJERO','')" tabindex="8" class="inputs" style="width: 148px">
								<option value="">-Municipio-</option>
								<?php
									$con = mysqli_query($conectar,"select * from posicion_geografica where pog_hijo IS NULL and pog_nieto IS NOT NULL order by pog_nombre");
									$num = mysqli_num_rows($con);
									for($i = 0; $i < $num; $i++)
									{
										$dato = mysqli_fetch_array($con);
										$clave = $dato['pog_clave_int'];
										$nombre = $dato['pog_nombre'];
								?>
									<option value="<?php echo $clave; ?>"><?php echo $nombre; ?></option>
								<?php
									}
								?>
								</select>
								</div>
								</td>
							<td style="width: 150px; height: 31px;">
								<select name="bustipologia" id="bustipologia" onchange="BUSCAR('CAJERO','')" tabindex="9" class="inputs" style="width: 148px">
								<option value="">-Tipología-</option>
								<?php
									$con = mysqli_query($conectar,"select * from tipologia where tip_sw_activo = 1 order by tip_nombre");
									$num = mysqli_num_rows($con);
									for($i = 0; $i < $num; $i++)
									{
										$dato = mysqli_fetch_array($con);
										$clave = $dato['tip_clave_int'];
										$nombre = $dato['tip_nombre'];
								?>
									<option value="<?php echo $clave; ?>" <?php if($clave == $tip){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
								<?php
									}
								?>
								</select></td>
							<td style="width: 150px; height: 31px;" class="alinearizq">
							<select name="bustipointervencion" id="bustipointervencion" onchange="BUSCAR('CAJERO','');VERMODALIDADES1(this.value)" tabindex="10" class="inputs" style="width: 158px">
							<option value="">Tipo Intervención</option>
							<?php
								$con = mysqli_query($conectar,"select * from tipointervencion order by tii_nombre");
								$num = mysqli_num_rows($con);
								for($i = 0; $i < $num; $i++)
								{
									$dato = mysqli_fetch_array($con);
									$clave = $dato['tii_clave_int'];
									$nombre = $dato['tii_nombre'];
							?>
								<option value="<?php echo $clave; ?>" <?php if($clave == $tipint){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
							<?php
								}
							?>
							</select></td>
							<td align="left" style="height: 31px">
							<div id="modalidades1" style="float:left">
							<select name="busmodalidad" id="busmodalidad" onchange="BUSCAR('CAJERO','')" tabindex="8" class="inputs" data-placeholder="-Seleccione-" style="width: 160px">
							<option value="">-Modalidad-</option>
							<?php
								$con = mysqli_query($conectar,"select * from modalidad order by mod_nombre");
								$num = mysqli_num_rows($con);
								for($i = 0; $i < $num; $i++)
								{
									$dato = mysqli_fetch_array($con);
									$clave = $dato['mod_clave_int'];
									$nombre = $dato['mod_nombre'];
							?>
								<option value="<?php echo $clave; ?>" <?php if($moda == $clave){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
							<?php
								}
							?>
							</select>
							</div>
							</td>
							<td align="left" style="height: 31px">
							<select name="busactor" id="busactor" onchange="BUSCAR('CAJERO','')" tabindex="20" class="inputs" style="width: 150px">
							<option value="">-Actor-</option>
							<?php
								$con = mysqli_query($conectar,"select * from usuario where usu_sw_inmobiliaria = 1 or usu_sw_visita = 1 or usu_sw_diseno = 1 or usu_sw_licencia = 1 or usu_sw_interventoria = 1 or usu_sw_constructor = 1 or usu_sw_seguridad = 1 order by usu_nombre");
								$num = mysqli_num_rows($con);
								for($i = 0; $i < $num; $i++)
								{
									$dato = mysqli_fetch_array($con);
									$clave = $dato['usu_clave_int'];
									$nombre = $dato['usu_nombre'];
							?>
								<option value="<?php echo $clave; ?>" <?php if($clave == $inmob){ echo 'selected="selected"'; } ?>><?php echo $nombre; ?></option>
							<?php
								}
							?>
							</select>
							</td>
							<td align="left" style="height: 31px">
							<select name="busestado" id="busestado" onchange="BUSCAR('CAJERO','')" tabindex="20" class="inputs" style="width: 150px">
							<option value="">-Estado-</option>
							<option value="1">Activos</option>
							<option value="4">Cancelados</option>
							<option value="3">Entregados</option>
							<option value="5">Programados</option>
							<option value="2">Suspendidos</option>
							</select>
							</td>
						</tr>
						</table>
				</td>
			</tr>
			<tr>
				<td class="auto-style2">
				<div id="busqueda">
				<fieldset name="Group1">
				<legend align="center">CAJEROS ELIMINADOS<br>
				<button name="Accion" value="excel" title="Exportar a Excel" onclick="EXPORTAR()" type="button" style="cursor:pointer;">
				<img src="../../images/excel.png" height="25" width="25"></button>
				</legend>
				<table style="width: 100%">
				<tr>
					<td class="alinearizq"><strong>Cajero</strong></td>
					<td class="alinearizq"><strong>Nombre</strong></td>
					<td class="alinearizq"><strong>Dirección</strong></td>
					<td class="alinearizq"><strong>Año Cont.</strong></td>
					<td class="alinearizq"><strong>Región</strong></td>
					<td class="alinearizq"><strong>Departamento</strong></td>
					<td class="alinearizq"><strong>Municipio</strong></td>
					<td class="alinearizq"><strong>Tipología</strong></td>
					<td class="alinearizq"><strong>Tip. Intervención</strong></td>
					<td class="alinearizq"><strong>Modalidad</strong></td>
					<td class="alinearizq"><strong>Estado</strong></td>
				</tr>
				<?php
				$query = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int)  inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where c.caj_sw_eliminado = 1");
				//$res = $con->query($query);
				$num_registros = mysqli_num_rows($query);
		
				$resul_x_pagina = 100;
				//Paginar:
				$paginacion = new Zebra_Pagination();
				$paginacion->records($num_registros);
				$paginacion->records_per_page($resul_x_pagina);
						
				$con = mysqli_query($conectar,"select *,c.caj_clave_int clacaj from cajero c left outer join cajero_inmobiliaria cajinm on (cajinm.caj_clave_int = c.caj_clave_int) left outer join cajero_visita cajvis on (cajvis.caj_clave_int = c.caj_clave_int) left outer join cajero_diseno cajdis on (cajdis.caj_clave_int = c.caj_clave_int) left outer join cajero_licencia cajlic on (cajlic.caj_clave_int = c.caj_clave_int)  inner join cajero_interventoria cajint on (cajint.caj_clave_int = c.caj_clave_int) left outer join cajero_constructor cajcons on (cajcons.caj_clave_int = c.caj_clave_int) left outer join cajero_seguridad cajseg on (cajseg.caj_clave_int = c.caj_clave_int) left outer join cajero_facturacion cajfac on (cajfac.caj_clave_int = c.caj_clave_int) where c.caj_sw_eliminado = 1 LIMIT ".(($paginacion->get_page() - 1) * $resul_x_pagina). ',' .$resul_x_pagina);
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					//Info Base
					$clacaj = $dato['clacaj'];
					$nomcaj = CORREGIRTEXTO($dato['caj_nombre']);
					$dir = CORREGIRTEXTO($dato['caj_direccion']);
					$anocon = $dato['caj_ano_contable'];
					$reg = $dato['caj_region'];
					$dep = $dato['caj_departamento'];
					$mun = $dato['caj_municipio'];
					$tip = $dato['tip_clave_int'];
					$tipint = $dato['tii_clave_int'];
					$mod = $dato['mod_clave_int'];
					$conmod = mysqli_query($conectar,"select mod_nombre from modalidad where mod_clave_int = '".$mod."'");
					$datomod = mysqli_fetch_array($conmod);
					$moda = $datomod['mod_nombre'];
					//Info Secun
					$codcaj = $dato['caj_codigo_cajero'];
					$cencos = $dato['cco_clave_int'];
					$refmaq = $dato['rem_clave_int'];
					$ubi = $dato['ubi_clave_int'];
					$codsuc = $dato['caj_codigo_suc'];
					$ubiamt = $dato['caj_ubicacion_atm'];
					$ati = $dato['ati_clave_int'];
					$rie = $dato['caj_riesgo'];
					$are = $dato['are_clave_int'];
					$codrec = $dato['caj_cod_recibido_monto'];
					$codrec = $dato['caj_fecha_creacion'];
					//Info Inmobiliaria
					$reqinm = $dato['cai_req_inmobiliaria'];
					$inmob = $dato['cai_inmobiliaria'];
					$feciniinmob = $dato['cai_fecha_ini_inmobiliaria'];
					$estinmob = $dato['esi_clave_int'];
					$fecentinmob = $dato['cai_fecha_entrega_info_inmobiliaria'];
					//Info Visita Local
					$reqvis = $dato['cav_req_visita_local'];
					$vis = $dato['cav_visitante'];
					$fecvis = $dato['cav_fecha_visita'];
					//Info Diseño
					$reqdis = $dato['cad_req_diseno'];
					$dis = $dato['cad_disenador'];
					$fecinidis = $dato['cad_fecha_inicio_diseno'];
					$estdis = $dato['esd_clave_int'];
					$fecentdis = $dato['cad_fecha_entrega_info_diseno'];
					//Info Licencia
					$reqlic = $dato['cal_req_licencia'];
					$lic = $dato['cal_gestionador'];
					$fecinilic = $dato['cal_fecha_inicio_licencia'];
					$estlic = $dato['esl_clave_int'];
					$fecentlic = $dato['cal_fecha_entrega_info_licencia'];
					//Info Interventoria
					$int = $dato['cin_interventor'];
					$fecteoent = $dato['cin_fecha_teorica_entrega'];
					$feciniobra = $dato['cin_fecha_inicio_obra'];
					$fecpedsum = $dato['cin_fecha_pedido_suministro'];
					$aprovcotiz = $dato['cin_sw_aprov_cotizacion'];
					$cancom = $dato['can_clave_int'];
					$aprovliqui = $dato['cin_sw_aprov_liquidacion'];
					$swimgnex = $dato['cin_sw_img_nexos'];
					$swfot = $dato['cin_sw_fotos'];
					$swact = $dato['cin_sw_actas'];
					$estpro = $dato['caj_estado_proyecto'];
					//Info Constructor
					$cons = $dato['cac_constructor'];
					$cons = $dato['cac_fecha_entrega_atm'];
					$cons = $dato['cac_porcentaje_avance'];
					$cons = $dato['cac_fecha_entrega_cotizacion'];
					$cons = $dato['cac_fecha_entrega_liquidacion'];
					//Info Instalador Seguridad
					$seg = $dato['cas_instalador'];
					$fecingseg = $dato['cas_fecha_ingreso_obra_inst'];
					$codmon = $dato['cas_codigo_monitoreo'];
				?>
				<tr>
					<td class="alinearizq"><?php echo $clacaj; ?></td>
					<td class="alinearizq"><?php echo $nomcaj; ?></td>
					<td class="alinearizq"><?php echo $dir; ?></td>
					<td class="alinearizq"><?php echo $anocon; ?></td>
					<td class="alinearizq">
					<?php 
						$sql = mysqli_query($conectar,"select pog_nombre from posicion_geografica where pog_clave_int = '".$reg."'");
						$datosql = mysqli_fetch_array($sql);
						$nomreg = $datosql['pog_nombre'];
						echo $nomreg; 
					?>
					</td>
					<td class="alinearizq">
					<?php 
						$sql = mysqli_query($conectar,"select pog_nombre from posicion_geografica where pog_clave_int = '".$dep."'");
						$datosql = mysqli_fetch_array($sql);
						$nomdep = $datosql['pog_nombre'];
						echo $nomdep;
					?>
					</td>
					<td class="alinearizq">
					<?php 
						$sql = mysqli_query($conectar,"select pog_nombre from posicion_geografica where pog_clave_int = '".$mun."'");
						$datosql = mysqli_fetch_array($sql);
						$nommun = $datosql['pog_nombre'];
						echo $nommun;
					?>
					</td>
					<td class="alinearizq">
					<?php 
						$sql = mysqli_query($conectar,"select tip_nombre from tipologia where tip_clave_int = '".$tip."'");
						$datosql = mysqli_fetch_array($sql);
						$nomtip = $datosql['tip_nombre'];
						echo $nomtip;
					?>
					</td>
					<td class="alinearizq">
					<?php 
						$sql = mysqli_query($conectar,"select tii_nombre from tipointervencion where tii_clave_int = '".$tipint."'");
						$datosql = mysqli_fetch_array($sql);
						$nomtii = $datosql['tii_nombre'];
						echo $nomtii;
					?>
					</td>
					<td class="alinearizq">
					<?php if($mod <= 0){ echo "Sin Definir "; }else{ echo $moda; } ?></td>
					<td class="alinearizq"><?php if($estpro == 1){ echo "ACTIVO"; }elseif($estpro == 2){ echo "SUSPENDIDO"; }elseif($estpro == 3){ echo "ENTREGADO"; }elseif($estpro == 4){ echo "CANCELADO"; }elseif($estpro == 5){ echo "PROGRAMADO"; } ?></td>
				</tr>
				<tr>
					<td class="alinearizq" colspan="11"><hr></td>
				</tr>
				<?php
				}
				?>
				</table>
				</fieldset>
				<?php $paginacion->render(); ?>
				</div>
			</td>
			</tr>
			</table>		
			</div>
			</td>
		</tr>
		<tr>
			<td style="width: 100px">&nbsp;</td>
			<td style="width: 100px">&nbsp;</td>
			<td style="width: 100px">&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
<script type="text/javascript" language="javascript">
    $("#buscajero").multiselect().multiselectfilter();
</script>
</form>
</body>
</html>