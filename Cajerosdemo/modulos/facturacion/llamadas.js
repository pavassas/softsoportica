function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function MODULO(v,e)
{	
	if(v == 'FACTURACION')
	{
		window.location.href = "facturacion.php";
	}
	else
	if(v == 'TODOS')
	{
        $("#cajeros").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />");
        $.post('fnCajeros.php',{opcion:v,est:e},function (data) {
            $('#cajeros').html(data);
        })
		/*var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('cajeros').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#cajeros").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?todos=si&est="+e,true);
		ajax.send(null);*/
		OCULTARSCROLL();
	}
}
function BUSCAR(v,p)
{	
	var consec = form1.busconsecutivo.value;
	var nom = form1.busnombre.value;
	var anocon = form1.busanocontable.value;
	var cencos = form1.buscentrocostos.value;
	var cod = form1.buscodigo.value;
	var reg = form1.busregion.value;
	var mun = form1.busmunicipio.value;
	var tip = form1.bustipologia.value;
	var tipint = form1.bustipointervencion.value;
	var moda = form1.busmodalidad.value;
	var actor = form1.busactor.value;
	var est = form1.ocultoestado.value;
	
	if(v == 'CAJERO')
	{
        $("#busqueda").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='50' width='50' />");
        $.post('fnCajeros.php',{opcion:"BUSCAR"},function (data){
            $("#busqueda").html(data);
        })
		/*var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('busqueda').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#busqueda").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
		if(p > 0)
		{
			ajax.open("GET","?buscar=si&consec="+consec+"&nom="+nom+"&anocon="+anocon+"&cencos="+cencos+"&cod="+cod+"&reg="+reg+"&mun="+mun+"&tip="+tip+"&tipint="+tipint+"&moda="+moda+"&est="+est+"&actor="+actor+"&page="+p,true);
		}
		else
		{
			ajax.open("GET","?buscar=si&consec="+consec+"&nom="+nom+"&anocon="+anocon+"&cencos="+cencos+"&cod="+cod+"&reg="+reg+"&mun="+mun+"&tip="+tip+"&tipint="+tipint+"&moda="+moda+"&est="+est+"&actor="+actor,true);
		}		
		ajax.send(null);*/
	}
	OCULTARSCROLL();
}
function VERREGISTRO(v)
{	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('cajeros').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#cajeros").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?verregistro=si&clacaj="+v,true);
	ajax.send(null);
	OCULTARSCROLL();
}
function AVISO()
{
	alert("Porfavor seleccione un cajero para agregar notas");
}
function GUARDAR(v)
{
	var numotinm = $('#numotinm').val();
	var vrinm = $('#vrinm').val();
	var swcotizliquiinm = $('input[name=swcotizliquiinm]:checked', '#form1').val();
	var numotvis = $('#numotvis').val();
	var vrvis = $('#vrvis').val();
	var swcotizliquivis = $('input[name=swcotizliquivis]:checked', '#form1').val();
	var numotdis = $('#numotdis').val();
	var vrdis = $('#vrdis').val();
	var swcotizliquidis = $('input[name=swcotizliquidis]:checked', '#form1').val();
	var numotlic = $('#numotlic').val();
	var vrlic = $('#vrlic').val();
	var swcotizliquilic = $('input[name=swcotizliquilic]:checked', '#form1').val();
	var numotint = $('#numotint').val();
	var vrint = $('#vrint').val();
	var swcotizliquiint = $('input[name=swcotizliquiint]:checked', '#form1').val();
	var numotcons = $('#numotcons').val();
	var vrcons = $('#vrcons').val();
	var swcotizliquicons = $('input[name=swcotizliquicons]:checked', '#form1').val();
	var numotseg = $('#numotseg').val();
	var vrseg = $('#vrseg').val();
	var swcotizliquiseg = $('input[name=swcotizliquiseg]:checked', '#form1').val();
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('estadoregistro').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#estadoregistro").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?guardardatos=si&numotinm="+numotinm+"&vrinm="+vrinm+"&swcotizliquiinm="+swcotizliquiinm+"&numotvis="+numotvis+"&vrvis="+vrvis+"&swcotizliquivis="+swcotizliquivis+"&numotdis="+numotdis+"&vrdis="+vrdis+"&swcotizliquidis="+swcotizliquidis+"&numotlic="+numotlic+"&vrlic="+vrlic+"&swcotizliquilic="+swcotizliquilic+"&numotint="+numotint+"&vrint="+vrint+"&swcotizliquiint="+swcotizliquiint+"&numotcons="+numotcons+"&vrcons="+vrcons+"&swcotizliquicons="+swcotizliquicons+"&numotseg="+numotseg+"&vrseg="+vrseg+"&swcotizliquiseg="+swcotizliquiseg+"&caj="+v,true);	
	ajax.send(null);
}
function RESULTADOADJUNTO(v,cc)
{
	if(v == 'INMOBILIARIA')
	{
		var div = 'resultadoadjuntoinm';
	}
	else
	if(v == 'VISITA')
	{
		var div = 'resultadoadjuntovis';
	}
	else
	if(v == 'DISENO')
	{
		var div = 'resultadoadjuntodis';
	}
	else
	if(v == 'LICENCIA')
	{
		var div = 'resultadoadjuntolic';
	}
	else
	if(v == 'INTERVENTORIA')
	{
		var div = 'resultadoadjuntoint';
	}
	else
	if(v == 'CONSTRUCTOR')
	{
		var div = 'resultadoadjuntocon';
	}
	else
	if(v == 'SEGURIDAD')
	{
		var div = 'resultadoadjuntoseg';
	}
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById(div).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#"+div).html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?estadoadjunto=si&actor="+v+"&clacaj="+cc,true);
	ajax.send(null);
	OCULTARSCROLL();
}
function MOSTRARRUTA(v)
{
	if(v == 'INMOBILIARIA')
	{
		var div = 'resultadoadjuntoinm';
		var nomadj = form1.adjuntoinm.value;
	}
	else
	if(v == 'VISITA')
	{
		var div = 'resultadoadjuntovis';
		var nomadj = form1.adjuntovis.value;
	}
	else
	if(v == 'DISENO')
	{
		var div = 'resultadoadjuntodis';
		var nomadj = form1.adjuntodis.value;
	}
	else
	if(v == 'LICENCIA')
	{
		var div = 'resultadoadjuntolic';
		var nomadj = form1.adjuntolic.value;
	}
	else
	if(v == 'INTERVENTORIA')
	{
		var div = 'resultadoadjuntoint';
		var nomadj = form1.adjuntoint.value;
	}
	else
	if(v == 'CONSTRUCTOR')
	{
		var div = 'resultadoadjuntocon';
		var nomadj = form1.adjuntocon.value;
	}
	else
	if(v == 'SEGURIDAD')
	{
		var div = 'resultadoadjuntoseg';
		var nomadj = form1.adjuntoseg.value;
	}
	else
	if(v == 'ADJUNTO')
	{
		var div = 'resultadoadjunto';
		var nomadj = $('#archivoadjunto').val();
	}
		
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById(div).innerHTML=ajax.responseText;
	    }
	}
	jQuery("#"+div).html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarruta=si&nomadj="+nomadj,true);
	ajax.send(null);
	OCULTARSCROLL();
}
function SUMATOTAL()
{	
	var vrinm = form1.vrinm.value;
	var vrvis = form1.vrvis.value;
	var vrdis = form1.vrdis.value;
	var vrlic = form1.vrlic.value;
	var vrint = form1.vrint.value;
	var vrcons = form1.vrcons.value;
	var vrseg = form1.vrseg.value;
	
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('sumatotal').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#sumatotal").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='10' width='10' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?sumatotal=si&vrinm="+vrinm+"&vrvis="+vrvis+"&vrdis="+vrdis+"&vrlic="+vrlic+"&vrint="+vrint+"&vrcons="+vrcons+"&vrseg="+vrseg,true);
	ajax.send(null);
}
function MOSTRARBITACORA(c)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todaslasnotas').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todaslasnotas").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarbitacora=si&clacaj="+c,true);
	ajax.send(null);
}
function MOSTRARBITACORAADJUNTOS(c)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('todoslosadjuntosbitacora').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#todoslosadjuntosbitacora").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostrarbitacoraadjuntos=si&clacaj="+c,true);
	ajax.send(null);
}
function VISOR(c,v)
{
    var url = c.replace('../../','www.pavas.com.co/Cajeros/');
    var mdl =  "<div class='embed-responsive embed-responsive-16by9'><iframe class='embed-responsive-item' src='https://docs.google.com/viewer?url="+url+"&embedded=true' frameborder='0' allowfullscreen></iframe></div>";
    $('#' + v).html(mdl);

}
function MOSTRARADJUNTOS(c,a)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('adjuntoarchivos').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#adjuntoarchivos").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='100' width='100' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?mostraradjuntos=si&clacaj="+c+"&actor="+a,true);
	ajax.send(null);
}
function EDITARADJUNTO(c,a)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('editaradjunto').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#editaradjunto").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?editaradjunto=si&claada="+c+"&act="+a,true);
	ajax.send(null);
}
function RESULTADOADJUNTOINFORMACION(ca)
{
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('resultadoadjunto').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#resultadoadjunto").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?estadoadjuntoinformacion=si&claada="+ca,true);
	ajax.send(null);
}
function ELIMINARADJUNTO(v)
{
	if(confirm('Esta seguro/a de Eliminar este archivo?'))
	{
		var dataString = 'id='+v;
		
		$.ajax({
	        type: "POST",
	        url: "deletearchivo.php",
	        data: dataString,
	        success: function() {
				//$('#delete-ok').empty();
				//$('#delete-ok').append('<div class="correcto">Se ha eliminado correctamente la entrada con id='+v+'.</div>').fadeIn("slow");
				$('#service'+v).fadeOut("slow");
				//$('#'+v).remove();
	        }
	    });
	}
}
function VERMODALIDADES1(v)
{
    var mod = $('#busmodalidad');
    var tii = $('#bustipointervencion').val();
    mod.find('option').remove().end().append('<option value="">Cargando...</option>').val('');
    $.post('fnCajeros.php',{opcion:'VERMODALIDADES',tii:tii},
        function(data)
        {
            mod.empty();
            mod.append('<option value="">-Modalidad-</option>');

            for (var i=0; i<data.length; i++)
            {
                mod.append('<option value="' + data[i].id + '">' + data[i].literal + '</option>');
            }
            //setTimeout("REFRESCARLISTAMODALIDADES1()",800);
            setTimeout(function() { BUSCAR('CAJERO','') },1000);

        },"json");
    /*
	var ajax;
	ajax=new ajaxFunction();
	ajax.onreadystatechange=function()
	{
		if(ajax.readyState==4)
	    {
   		     document.getElementById('modalidades1').innerHTML=ajax.responseText;
	    }
	}
	jQuery("#modalidades1").html("<img alt='cargando' src='../../images/ajax-loader.gif' height='30' width='30' />"); //loading gif will be overwrited when ajax have success
	ajax.open("GET","?vermodalidades1=si&tii="+v,true);
	ajax.send(null);
	//setTimeout("REFRESCARLISTAMODALIDADES1()",800);*/
}

function INICIALIZARLISTAS(){
    $(".buslista").searchable();
}