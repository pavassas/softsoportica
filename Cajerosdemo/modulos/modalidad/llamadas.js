function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }
function EDITAR(v,m)
{	
	if(m == 'MODALIDAD')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('editarmodalidad').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#editarmodalidad").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?editarmod=si&modedi="+v,true);
		ajax.send(null);
	}
}
function GUARDAR(v,id)
{
	if(v == 'MODALIDAD')
	{
		var mod = form1.modalidad1.value;
		var lm = mod.length;
		var act = form1.activo1.checked;
		
		var inm = form1.inmobiliaria1.checked;
		var dinm = form1.diasinmobiliaria1.value;
		var vis = form1.visita1.checked;
		var dvis = form1.diasvisita1.value;
		var comite = form1.comite1.checked;
		var dcom = form1.diascomite1.value;
		var contrato = form1.contrato1.checked;
		var dcon = form1.diascontrato1.value;
		var dis = form1.diseno1.checked;
		var ddis = form1.diasdiseno1.value;
		
		var prefact = form1.prefactibilidad1.checked;
		var dprefact = form1.diasprefactibilidad1.value;
		var ped = form1.pedidomaquina1.checked;
		var dped = form1.diaspedidomaquina1.value;
		
		var lic = form1.licencia1.checked;
		var dlic = form1.diaslicencia1.value;
		
		var can = form1.canal1.checked;
		var dcan = form1.diascanal1.value;
		var pro = form1.proyeccion1.checked;
		var dpro = form1.diasproyeccion1.value;
        var inf = form1.infraestructura1.checked;
        var dinf = form1.diasinfraestructura1.value;
		
		var preliminar = form1.preliminar1.checked;
		var dpre = form1.diaspreliminar1.value;
		var inter = form1.interventor1.checked;
		var cons = form1.constructor1.checked;
		var dcons = form1.diasconstructor1.value;
		var seg = form1.seguridad1.checked;
		
		var tipmod = $('input[name=tipomodadlidad1]:checked', '#form1').val();
		if(tipmod != 1 && tipmod != 0){ tipmod = ''; }
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('datos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#datos").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?guardarmod=si&mod="+mod+"&act="+act+"&inm="+inm+"&dinm="+dinm+"&vis="+vis+"&dvis="+dvis+"&comite="+comite+"&dcom="+dcom+"&contrato="+contrato+"&dcon="+dcon+"&dis="+dis+"&ddis="+ddis+"&lic="+lic+"&dlic="+dlic+"&preliminar="+preliminar+"&dpre="+dpre+"&inter="+inter+"&cons="+cons+"&dcons="+dcons+"&seg="+seg+"&lm="+lm+"&prefact="+prefact+"&dprefact="+dprefact+"&ped="+ped+"&dped="+dped+"&can="+can+"&dcan="+dcan+"&pro="+pro+"&dpro="+dpro+"&tipmod="+tipmod+"&m="+id+"&inf="+inf+"&dinf="+dinf,true);
		ajax.send(null);
		if(mod != '' && lm >= 3)
		{
			setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
		}
	}
}
function NUEVO(v)
{
	if(v == 'MODALIDAD')
	{
		var mod = form1.modalidad.value;
		var lm = mod.length;		
		var act = form1.activo.checked;
		var inm = form1.inmobiliaria.checked;
		var dinm = form1.diasinmobiliaria.value;
		var vis = form1.visita.checked;
		var dvis = form1.diasvisita.value;
		var comite = form1.comite.checked;
		var dcom = form1.diascomite.value;
		var contrato = form1.contrato.checked;
		var dcon = form1.diascontrato.value;
		var dis = form1.diseno.checked;
		var ddis = form1.diasdiseno.value;
		
		var prefact = form1.prefactibilidad.checked;
		var dprefact = form1.diasprefactibilidad.value;
		var ped = form1.pedidomaquina.checked;
		var dped = form1.diaspedidomaquina.value;
		
		var lic = form1.licencia.checked;
		var dlic = form1.diaslicencia.value;
		
		var can = form1.canal.checked;
		var dcan = form1.diascanal.value;
		var pro = form1.proyeccion.checked;
		var dpro = form1.diasproyeccion.value;
        var inf = form1.infraestructura.checked;
        var dinf = form1.diasinfraestructura.value;
		
		var preliminar = form1.preliminar.checked;
		var dpre = form1.diaspreliminar.value;
		var inter = form1.interventor.checked;
		var cons = form1.constructor.checked;
		var dcons = form1.diasconstructor.value;
		var seg = form1.seguridad.checked;
		
		var tipmod = $('input[name=tipomodadlidad]:checked', '#form1').val();
		if(tipmod != 1 && tipmod != 0){ tipmod = ''; }
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('datos1').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#datos1").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?nuevamodalidad=si&mod="+mod+"&act="+act+"&inm="+inm+"&dinm="+dinm+"&vis="+vis+"&dvis="+dvis+"&comite="+comite+"&dcom="+dcom+"&contrato="+contrato+"&dcon="+dcon+"&dis="+dis+"&ddis="+ddis+"&lic="+lic+"&dlic="+dlic+"&preliminar="+preliminar+"&dpre="+dpre+"&inter="+inter+"&cons="+cons+"&dcons="+dcons+"&seg="+seg+"&prefact="+prefact+"&dprefact="+dprefact+"&ped="+ped+"&dped="+dped+"&can="+can+"&dcan="+dcan+"&pro="+pro+"&dpro="+dpro+"&tipmod="+tipmod+"&lm="+lm+"&inf="+inf+"&dinf="+dinf,true);
		ajax.send(null);
		if(mod != '' && lm >= 3)
		{
			form1.modalidad.value = '';
			setTimeout("CONSULTAMODULO('TODOS');",1000);//setInterval("window.location.href='usuarios.php';",3000);
		}
	}
}
function CONSULTAMODULO(v)
{
	if(v == 'TODOS')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modalidad').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modalidad").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='80' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?todos=si",true);
		ajax.send(null);
	}
}
function BUSCAR(m)
{	
	if(m == 'MODALIDAD')
	{
		var mod = form1.modalidad2.value;
		var act = form1.buscaractivos.value;
				
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('modalidad').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#modalidad").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?buscarmod=si&mod="+mod+"&act="+act,true);
		ajax.send(null);
	}
}