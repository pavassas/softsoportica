<?php
	include('../../data/Conexion.php');
	session_start();
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_SESSION['usuario'];
	$idUsuario= $_COOKIE["usIdentificacion"];
	$clave= $_COOKIE["clave"];
		
	// verifica si no se ha loggeado
	if(!isset($_SESSION["persona"]))
	{
	  session_destroy();
	  header("LOCATION:index.php");
	}else{
	}
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");
	
	if($_GET['accion'] == 'ACTIVARINACTIVAR')
	{
		$usuact = $_GET['act'];
		$con = mysqli_query($conectar,"select * from usuario where usu_clave_int = '".$usuact."'");
		$dato = mysqli_fetch_array($con);
		$act = $dato['usu_sw_activo'];
		if($act == 1)
		{
			$con = mysqli_query($conectar,"update usuario set usu_sw_activo = 0 where usu_usuario = '".$usuact."'");
		}
		elseif($act == 0)
		{
			$con = mysqli_query($conectar,"update usuario set usu_sw_activo = 1 where usu_usuario = '".$usuact."'");
		}
	}

	if($_GET['editarusu'] == 'si')
	{
		$usuedi = $_GET['usuedi'];
		$con = mysqli_query($conectar,"select * from usuario where usu_clave_int = '".$usuedi."'");
		$dato = mysqli_fetch_array($con);
		$cat = $dato['usu_categoria'];
		$nom = $dato['usu_nombre'];
		$usu = $dato['usu_usuario'];
		$con = $dato['usu_clave'];
		$ema = $dato['usu_email'];
		$act = $dato['usu_sw_activo'];
		$per = $dato['prf_clave_int'];
		
		$inm = $dato['usu_sw_inmobiliaria'];
		$vis = $dato['usu_sw_visita'];
		$lic = $dato['usu_sw_licencia'];
		$dis = $dato['usu_sw_diseno'];
		$int = $dato['usu_sw_interventoria'];
		$cons = $dato['usu_sw_constructor'];
		$seg = $dato['usu_sw_seguridad'];
		$otr = $dato['usu_sw_otro'];
		$her = $dato['usu_sw_heredar'];
		
		if($inm == 1){ $sql1 = 'usu_sw_inmobiliaria = 1'; }else{ $sql1 = 'usu_sw_inmobiliaria = 2'; }
		if($vis == 1){ $sql2 = 'or usu_sw_visita = 1'; }else{ $sql2 = 'or usu_sw_visita = 2'; }
		if($lic == 1){ $sql3 = 'or usu_sw_licencia = 1'; }else{ $sql3 = 'or usu_sw_licencia = 2'; }
		if($dis == 1){ $sql4 = 'or usu_sw_diseno = 1'; }else{ $sql4 = 'or usu_sw_diseno = 2'; }
		if($int == 1){ $sql5 = 'or usu_sw_interventoria = 1'; }else{ $sql5 = 'or usu_sw_interventoria = 2'; }
		if($cons == 1){ $sql6 = 'or usu_sw_constructor = 1'; }else{ $sql6 = 'or usu_sw_constructor = 2'; }
		if($seg == 1){ $sql7 = 'or usu_sw_seguridad = 1'; }else{ $sql7 = 'or usu_sw_seguridad = 2'; }
		if($otr == 1){ $sql8 = 'or usu_sw_otro = 1'; }else{ $sql8 = 'or usu_sw_otro = 2'; }
?>
		<table style="width: 38%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Categoria:</td>
			<td class="auto-style3">
			<select class="inputs" name="categoria1" id="categoria1" onchange="ACTIVARACTORES(this.value)" style="width: 260px">
			<option value="1" <?php if($cat == 1){ echo 'selected="selected"'; } ?>>Usuario</option>
			<option value="2" <?php if($cat == 2){ echo 'selected="selected"'; } ?>>Grupo</option>
			</select>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Nombre:</td>
			<td class="auto-style3"><input class="inputs" name="nombre1" maxlength="50" value="<?php echo $nom; ?>" type="text" style="width: 250px" />
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Usuario:</td>
			<td class="auto-style3"><input class="inputs" name="usuario1" <?php if($cat == 2){ echo 'disabled="disabled"'; } ?> maxlength="15" type="text" value="<?php echo $usu; ?>" style="width: 250px" />
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Contraseña:</td>
			<td class="auto-style3"><input class="inputs" name="Password3" <?php if($cat == 2){ echo 'disabled="disabled"'; } ?> maxlength="15" type="password" value="<?php echo $con; ?>" style="width: 250px" />
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Repetir Contraseña:</td>
			<td class="auto-style3">
			<input class="inputs" name="Password4" <?php if($cat == 2){ echo 'disabled="disabled"'; } ?> maxlength="15" onkeyup="VALIDAR('CONTRASENA')" type="password" value="<?php echo $con; ?>" style="width: 250px" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Perfil de acceso:</td>
			<td class="auto-style3">
				<select class="inputs" name="perfil1" <?php if($cat == 2){ echo 'disabled="disabled"'; } ?> style="width: 260px">
				<?php
					$con = mysqli_query($conectar,"select * from perfil order by prf_descripcion");
					$num = mysqli_num_rows($con);
					for($i = 0; $i < $num; $i++)
					{
						$dato = mysqli_fetch_array($con);
						$clave = $dato['prf_clave_int'];
						$perfil = $dato['prf_descripcion'];
				?>
					<option value="<?php echo $clave; ?>" <?php $con1 = mysqli_query($conectar,"select prf_clave_int from usuario where usu_clave_int = '".$usuedi."'"); $dato = mysqli_fetch_array($con1); $p = $dato['prf_clave_int']; if($p == $clave){  echo "selected='selected'"; } ?>><?php echo $perfil; ?></option>
				<?php
					}
				?>
				</select>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">E-mail:</td>
			<td class="auto-style3"><input class="inputs" name="email1" <?php if($cat == 2){ echo 'disabled="disabled"'; } ?> maxlength="100" type="text" value="<?php echo $ema; ?>" style="width: 250px" /></td>
			<td>&nbsp;</td>
		</tr>
		<?php
		$con = mysqli_query($conectar,"select * from perfil where prf_clave_int = '".$per."'");
		$dato = mysqli_fetch_array($con);
		$prf = $dato['prf_descripcion'];
		?>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3">Actor:</td>
			<td class="auto-style3"><label for="heredar1" style="cursor:pointer">Heredar Actores:</label> <input name="heredar1" id="heredar1" class="inputs" <?php if($cat == 1){ echo 'disabled="disabled"'; } ?> <?php if($her == 1){ echo 'checked="checked"'; } ?> type="checkbox" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3" colspan="2">
			<table style="width: 100%">
				<tr>
					<td>
					<input name="inmobiliaria1" id="inmobiliaria1" onclick="MOSTRARACTORES1('<?php echo $usuedi; ?>')" <?php if($inm == 1){ echo 'checked="checked"'; } ?> type="checkbox" />
					</td>
					<td><label for="inmobiliaria1" style="cursor:pointer">Inmobiliaria</label></td>
					<td>
					<input name="visita1" id="visita1" onclick="MOSTRARACTORES1('<?php echo $usuedi; ?>')" <?php if($vis == 1){ echo 'checked="checked"'; } ?> type="checkbox" /></td>
					<td><label for="visita1" style="cursor:pointer">Visita</label></td>
					<td>
					<input name="diseno1" id="diseno1" onclick="MOSTRARACTORES1('<?php echo $usuedi; ?>')" <?php if($dis == 1){ echo 'checked="checked"'; } ?> type="checkbox" /></td>
					<td><label for="diseno1" style="cursor:pointer">Diseñador</label></td>
					<td>
					<input name="licencia1" id="licencia1" onclick="MOSTRARACTORES1('<?php echo $usuedi; ?>')" <?php if($lic == 1){ echo 'checked="checked"'; } ?> type="checkbox" /></td>
					<td><label for="licencia1" style="cursor:pointer">Licencia</label></td>
				</tr>
				<tr>
					<td>
					<input name="interventor1" id="interventor1" onclick="MOSTRARACTORES1('<?php echo $usuedi; ?>')" <?php if($int == 1){ echo 'checked="checked"'; } ?> type="checkbox" /></td>
					<td><label for="interventor1" style="cursor:pointer">Interventor</label></td>
					<td>
					<input name="constructor1" id="constructor1" onclick="MOSTRARACTORES1('<?php echo $usuedi; ?>')" <?php if($cons == 1){ echo 'checked="checked"'; } ?> type="checkbox" /></td>
					<td><label for="constructor1" style="cursor:pointer">Constructor</label></td>
					<td>
					<input name="seguridad1" id="seguridad1" onclick="MOSTRARACTORES1('<?php echo $usuedi; ?>')" <?php if($seg == 1){ echo 'checked="checked"'; } ?> type="checkbox" /></td>
					<td><label for="seguridad1" style="cursor:pointer">Seguridad</label></td>
					<td>
					<input name="otro1" id="otro1" onclick="MOSTRARACTORES1('<?php echo $usuedi; ?>')" <?php if($otr == 1){ echo 'checked="checked"'; } ?> type="checkbox" />
					</td>
					<td><label for="otro1" style="cursor:pointer">Otro</label></td>
				</tr>
			</table>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3" colspan="2">
			<table style="width: 100%">
				<tr>
					<td colspan="3"><strong>AGREGAR ACTORES AL USUARIO: <?php echo $nom; ?></strong></td>
				</tr>
				<tr>
					<td>
					<div id="agregar">
						<select class="auto-style8" multiple="multiple" <?php if($cat == 1){ echo 'disabled="disabled"'; } ?> ondblclick="AGREGAR('<?php echo $usuedi; ?>')" name="agregarusu" id="agregarusu" style="width: 190px" size="8">
						<?php
							$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where usu_sw_activo = 1 and (".$sql1." ".$sql2." ".$sql3." ".$sql4." ".$sql5." ".$sql6." ".$sql7." ".$sql8.") and u.usu_clave_int <> '".$usuedi."' and u.usu_clave_int NOT IN (select usu_clave_int from usuario_actor where usa_actor = '".$usuedi."') and usu_categoria = 1 order by usu_nombre");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$clave = $dato['usu_clave_int'];
								$nombre = $dato['usu_nombre'];
								$per = $dato['prf_descripcion'];
						?>
							<option value="<?php echo $clave; ?>"><?php echo $nombre." - ".$per; ?></option>
						<?php
							}
						?>
						</select>
					</div>
					</td>
					<td align="center">
					<div style="width: 60px">
						<input name="pasar" id="pasar" <?php if($cat == 1){ echo 'disabled="disabled"'; } ?> type="button" class="pasar izq" onclick="AGREGAR('<?php echo $usuedi; ?>')" value="&raquo;" style="width: 30px"><input name="quitar" id="quitar" <?php if($cat == 1){ echo 'disabled="disabled"'; } ?> type="button" onclick="QUITAR('<?php echo $usuedi; ?>')" class="quitar der" value="&laquo;" style="width: 30px"><br />
						<input name="pasartodos" id="pasartodos" <?php if($cat == 1){ echo 'disabled="disabled"'; } ?> type="button" class="pasartodos izq" onclick="PONERTODOS('<?php echo $usuedi; ?>')" value="&raquo;&raquo;" style="width: 30px"><input name="quitartodos" id="quitartodos" <?php if($cat == 1){ echo 'disabled="disabled"'; } ?> type="button" onclick="REMOVERTODOS('<?php echo $usuedi; ?>')" class="quitartodos der" value="&laquo;&laquo;" style="width: 30px">
					</div>
					</td>
					<td>
					<div id="agregados">
					<select size="8" multiple="multiple" <?php if($cat == 1){ echo 'disabled="disabled"'; } ?> class="auto-style8" ondblclick="QUITAR('<?php echo $usuedi; ?>')" name="quitarusu" id="quitarusu" style="width: 190px">
					<?php 
			        	$sql = mysqli_query($conectar,"select * from usuario_actor ua inner join usuario u on (u.usu_clave_int = ua.usu_clave_int) inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where ua.usa_actor = '".$usuedi."' order by u.usu_nombre");
			        	$num = mysqli_num_rows($sql);
			        	for($i = 0; $i < $num; $i++)
			        	{
			        		$dato = mysqli_fetch_array($sql);
							$clave = $dato['usu_clave_int'];
							$nombre = $dato['usu_nombre'];
							$per = $dato['prf_descripcion'];
						?>
							<option value="<?php echo $clave; ?>"><?php echo $nombre." - ".$per; ?></option>
					<?php } ?>
			        </select>
					</div>
					</td>
				</tr>
			</table>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="auto-style3" colspan="2">Activo:<input class="inputs" <?php if($act == 1){ echo 'checked="checked"'; } ?> name="activo1" type="checkbox" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4">
			<input name="submit" type="button" value="Guardar" onclick="GUARDAR('USUARIO','<?php echo $usuedi; ?>')"  style="width: 430px; height: 25px; cursor:pointer" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="2">
			<div id="datos">
			</div>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
<?php
		exit();
	}
	if($_GET['editarper'] == 'si')
	{
		$peredi = $_GET['peredi'];
		$con = mysqli_query($conectar,"select * from perfil where prf_clave_int = '".$peredi."'");
		$dato = mysqli_fetch_array($con); 
		$cod = $dato['prf_codigo'];
		$des = $dato['prf_descripcion'];
?>
		<table style="width: 70%">
			<tr>
				<td>
				<strong>Editar perfil:</strong></td>
				<td>
				<input class="inputs" name="codigoper1" id="codigoper1" value="<?php echo $cod; ?>" maxlength="70" type="text" value="Código" onBlur="if(this.value=='') this.value='Código'" onFocus="if(this.value =='Código' ) this.value=''" style="width: 150px" /></td>
				<td>
				<input class="inputs" name="descripcionper1" id="descripcionper1" value="<?php echo $des; ?>" maxlength="70" type="text" value="Descripción" onBlur="if(this.value=='') this.value='Descripción'" onFocus="if(this.value =='Descripción' ) this.value=''" style="width: 150px" /></td>
				<td><a onclick="GUARDAR('PERFIL','<?php echo $peredi; ?>')" style="cursor:pointer" class="auto-style26"><span class="auto-style27">
				<strong>ACTUALIZAR PERFIL</strong></span></a></td>
				<td>
				<a onclick="NUEVO('PERFIL1')" style="cursor:pointer" class="auto-style26"><span class="auto-style27">
				<strong>NUEVO PERFIL</strong></span></a>
				</td>
			</tr>
		</table>
<?php
		exit();
	}
	if($_GET['nuevoper'] == 'si')
	{
?>
		<table style="width: 60%">
			<tr>
				<td>
				<strong>Nuevo perfil:</strong></td>
				<td>
				<input class="inputs" name="codigoper" id="codigoper" maxlength="70" type="text" value="Código" onBlur="if(this.value=='') this.value='Código'" onFocus="if(this.value =='Código' ) this.value=''" style="width: 150px" /></td>
				<td>
				<input class="inputs" name="descripcionper" id="descripcionper" maxlength="70" type="text" value="Descripción" onBlur="if(this.value=='') this.value='Descripción'" onFocus="if(this.value =='Descripción' ) this.value=''" style="width: 150px" /></td>
				<td><a onclick="NUEVO('PERFIL')" style="cursor:pointer" class="auto-style26"><span class="auto-style27">
				<strong>GUARDAR PERFIL</strong></span></a></td>
				<td></td>
			</tr>
		</table>
<?php
		exit();
	}
	if($_GET['guardarusu'] == 'si')
	{
		sleep(1);
		$cat = $_GET['cat'];
		$nom = $_GET['nom'];
		$usu = $_GET['usu'];
		$con1 = $_GET['con1'];
		$con2 = $_GET['con2'];
		$per = $_GET['per'];
		$ema = $_GET['ema'];
		$act = $_GET['act'];
		$lu = $_GET['lu'];
		$lc = $_GET['lc'];
		$u = $_GET['u'];
		
		$inm = $_GET['inm'];
		$vis = $_GET['vis'];
		$lic = $_GET['lic'];
		$dis = $_GET['dis'];
		$int = $_GET['int'];
		$cons = $_GET['cons'];
		$seg = $_GET['seg'];
		$otr = $_GET['otr'];
		$her = $_GET['her'];
		
		$sql = mysqli_query($conectar,"select * from usuario where (UPPER(usu_usuario) = UPPER('".$usu."') OR UPPER(usu_email) = UPPER('".$ema."')) AND usu_clave_int <> '".$u."'");
		$dato = mysqli_fetch_array($sql);
		$conusu = $dato['usu_usuario'];
		$conema = $dato['usu_email'];
		
		if($nom == '')
		{
			echo "<div class='validaciones'>Debe ingresar el Nombre</div>";
		}
		else
		if(($usu == '' || is_null($usu)) and ($cat == 1))
		{
			echo "<div class='validaciones'>Debe ingresar nombre de Usuario</div>";
		}
		else
		if((STRTOUPPER($conusu) == STRTOUPPER($usu)) and ($cat == 1))
		{
			echo "<div class='validaciones'>El usuario ingresado ya existe</div>";
		}
		else
		if($lu < 6 and $cat == 1)
		{
			echo "<div class='validaciones'>El usuario debe ser mí­nimo de 6 dijitos $usu</div>";
		}
		else
		if(($con1 != $con2) and ($cat == 1))
		{
			echo "<div class='validaciones'>Las contraseñas no coinciden</div>";
		}
		else
		if($lc < 6 and $cat == 1)
		{
			echo "<div class='validaciones'>La contraseña debe ser mí­nimo de 6 dijitos</div>";
		}
		else
		if($per == '' and $cat == 1)
		{
			echo "<div class='validaciones'>Debe elegir el perfil</div>";
		}
		else
		{
			if($act == 'false'){ $swact = 0; }elseif($act == 'true'){ $swact = 1; }
			
			if($inm == 'false'){ $inm = 0; }elseif($inm == 'true'){ $inm = 1; }
			if($vis == 'false'){ $vis = 0; }elseif($vis == 'true'){ $vis = 1; }
			if($dis == 'false'){ $dis = 0; }elseif($dis == 'true'){ $dis = 1; }
			if($lic == 'false'){ $lic = 0; }elseif($lic == 'true'){ $lic = 1; }
			if($int == 'false'){ $int = 0; }elseif($int == 'true'){ $int = 1; }
			if($cons == 'false'){ $cons = 0; }elseif($cons == 'true'){ $cons = 1; }
			if($seg == 'false'){ $seg = 0; }elseif($seg == 'true'){ $seg = 1; }
			if($otr == 'false'){ $otr = 0; }elseif($otr == 'true'){ $otr = 1; }
			if($her == 'false'){ $her = 0; }elseif($her == 'true'){ $her = 1; }
			
			if($conema != '' and $ema != '' and STRTOUPPER($conema) == STRTOUPPER($ema))
			{
				echo "<div class='validaciones'>El e-mail ingresado ya existe</div>";
			}
			else
			{
				//Si la categoria es usuario, no puede heredar nada
				if($cat == 1){ $her = 0; }
				$con = mysqli_query($conectar,"update usuario set usu_categoria = '".$cat."', usu_usuario = '".$usu."', usu_clave = '".$con1."', usu_nombre = '".$nom."', prf_clave_int = '".$per."', usu_sw_activo = '".$swact."', usu_email = '".$ema."', usu_usu_actualiz = '".$usuario."', usu_fec_actualiz = '".$fecha."', usu_sw_inmobiliaria = '".$inm."', usu_sw_visita = '".$vis."', usu_sw_diseno = '".$dis."', usu_sw_licencia = '".$lic."', usu_sw_interventoria = '".$int."', usu_sw_constructor = '".$cons."', usu_sw_seguridad = '".$seg."', usu_sw_otro = '".$otr."', usu_sw_heredar = '".$her."' where usu_clave_int = '".$u."'");

				if($con >= 1)
				{
					echo "<div class='ok'>Datos grabados correctamente</div>";
					//Verifico si la categoria es usuario= 1 borro todos los hijos que tenga relacionados
					if($cat == 1)
					{
						mysqli_query($conectar,"delete from usuario_actor where usa_actor = '".$u."'");
					}
				}
				else
				{
					echo "<div class='validaciones'>No se han podido guardar los datos</div>";
				}
			}	
		}

		exit();
	}
	if($_GET['guardarper'] == 'si')
	{
		sleep(1);
		$cod = $_GET['cod'];
		$des = $_GET['des'];
		$p = $_GET['p'];
		
		if($cod == 'Código'){ $cod = ''; }
		if($des == 'Descripción'){ $des = ''; }
		
		$con = mysqli_query($conectar,"select prf_codigo from perfil where prf_codigo = '".$cod."' and prf_clave_int <> '".$p."'");
		$datocod = mysqli_fetch_array($con);
		$concod = $datocod['prf_codigo'];
		
		$con = mysqli_query($conectar,"select prf_descripcion from perfil where UPPER(prf_descripcion) = UPPER('".$des."') and prf_clave_int <> '".$p."'");
		$datodes = mysqli_fetch_array($con);
		$condes = $datodes['prf_descripcion'];
		
		if($cod == '')
		{
			echo "<div class='validaciones' style='width: 99%' align='center'>Debe ingresar el Código</div>";
		}
		else
		if($cod == $concod)
		{
			echo "<div class='validaciones' style='width: 99%' align='center'>El Código ingresado ya existe</div>";
		}
		else
		if($des == '')
		{
			echo "<div class='validaciones' style='width: 99%' align='center'>Debe ingresar la Descripción</div>";
		}
		else
		if(STRTOUPPER($des) == STRTOUPPER($condes))
		{
			echo "<div class='validaciones' style='width: 99%' align='center'>La Descripción ingresada ya existe</div>";
		}
		else
		{			
			$con = mysqli_query($conectar,"update perfil set prf_codigo = '".$cod."', prf_descripcion = '".$des."', prf_usu_actualiz = '".$usuario."', prf_fec_actualiz = '".$fecha."' where prf_clave_int = '".$p."'");
			
			if($con >= 1)
			{
				echo "<div class='ok'>Datos grabados correctamente</div>";
			}
			else
			{
				echo "<div class='validaciones'>No se han podido guardar los datos</div>";
			}
		}
		
		$rows=mysqli_query($conectar,"select * from perfil");
		$total=mysqli_num_rows($rows);
?>
		<table style="width: 100%">
			<tr>
				<td class="auto-style3" style="width: 27px">
					<input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<? echo $total;?>,this);" class="auto-style6" /><span class="auto-style6">
					</span>
				</td>
				<td class="auto-style3" colspan="5">
				<table style="width: 30%">
					<tr>
						<td class="auto-style1">
						<p style="cursor:pointer; width: 100px;">
						<img src="../../img/eliminar.png" alt="" class="auto-style6" /><input type="submit" value="Eliminar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
						<td class="auto-style1"></td>
						<td class="auto-style1"></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td class="auto-style5" style="width: 27px">&nbsp;</td>
				<td class="auto-style5" style="width: 180px"><strong>Código</strong></td>
				<td class="auto-style5" style="width: 120px"><strong>Descripción</strong></td>
				<td class="auto-style5" style="width: 98px"><strong>Usuario</strong></td>
				<td class="auto-style5" style="width: 127px"><strong>
					Actualización</strong></td>
				<td class="auto-style5">&nbsp;</td>
			</tr>
			<?php
				$contador=0;
				$con = mysqli_query($conectar,"select * from perfil order by prf_descripcion");
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$claprf = $dato['prf_clave_int'];
					$cod = $dato['prf_codigo'];
					$des = $dato['prf_descripcion'];
					$usuact = $dato['prf_usu_actualiz'];
					$fecact = $dato['prf_fec_actualiz'];
					$contador=$contador+1;
			?>
			<tr>
				<td class="auto-style3" style="width: 27px">
					<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<? echo $contador;?>" value="<? echo $claprf; ?>" class="auto-style6" /></td>
				<td class="auto-style5" style="width: 180px"><?php echo $cod; ?></td>
				<td class="auto-style5" style="width: 120px"><?php echo $des; ?></td>
				<td class="auto-style5" style="width: 98px"><?php echo $usuact; ?></td>
				<td class="auto-style5" style="width: 127px"><?php echo $fecact; ?></td>
				<td class="auto-style5"><a style="cursor:pointer" onclick="EDITAR('<?php echo $claprf; ?>','PERFIL')"><img src="../../img/editar.png" alt="" height="22" width="21" /></a></td>
			</tr>
			<?php
				}
			?>
			<tr>
				<td class="auto-style5" style="width: 27px">&nbsp;</td>
				<td class="auto-style5" style="width: 180px">&nbsp;</td>
				<td class="auto-style5" style="width: 120px">&nbsp;</td>
				<td class="auto-style5" style="width: 98px">&nbsp;</td>
				<td class="auto-style5" style="width: 127px">&nbsp;</td>
				<td class="auto-style5">&nbsp;</td>
			</tr>
		</table>
<?php
		exit();
	}
	if($_GET['contrasena'] == 'si')
	{
		$con1 = $_GET['con1'];
		$con2 = $_GET['con2'];
		if($con1 != $con2)
		{
			echo "<div class='validaciones'>Las contraseñas no coinciden</div>";
		}
		else
		{
			
		}
		exit();
	}
	
	if($_GET['nuevousuario'] == 'si')
	{
		sleep(1);
		$cat = $_GET['cat'];
		$nom = $_GET['nom'];
		$usu = $_GET['usu'];
		$con1 = $_GET['con1'];
		$con2 = $_GET['con2'];
		$per = $_GET['per'];
		$ema = $_GET['ema'];
		$act = $_GET['act'];
		$lu = $_GET['lu'];
		$lc = $_GET['lc'];
		
		$inm = $_GET['inm'];
		$vis = $_GET['vis'];
		$lic = $_GET['lic'];
		$dis = $_GET['dis'];
		$int = $_GET['int'];
		$cons = $_GET['cons'];
		$seg = $_GET['seg'];
		$otr = $_GET['otr'];
		$her = $_GET['her'];
		
		$sql = mysqli_query($conectar,"select * from usuario where (UPPER(usu_usuario) = UPPER('".$usu."') OR UPPER(usu_email) = UPPER('".$ema."'))");
		$dato = mysqli_fetch_array($sql);
		$conusu = $dato['usu_usuario'];
		$conema = $dato['usu_email'];
		
		if($nom == '')
		{
			echo "<div class='validaciones'>Debe ingresar el Nombre</div>";
		}
		else
		if(($usu == '' || is_null($usu)) and ($cat == 1))
		{
			echo "<div class='validaciones'>Debe ingresar nombre de Usuario</div>";
		}
		else
		if((STRTOUPPER($conusu) == STRTOUPPER($usu)) and ($cat == 1))
		{
			echo "<div class='validaciones'>El usuario ingresado ya existe</div>";
		}
		else
		if($lu < 6 and $cat == 1)
		{
			echo "<div class='validaciones'>El usuario debe ser mí­nimo de 6 dijitos</div>";
		}
		else
		if(($con1 != $con2) and ($cat == 1))
		{
			echo "<div class='validaciones'>Las contraseñas no coinciden</div>";
		}
		else
		if($lc < 6 and ($cat == 1))
		{
			echo "<div class='validaciones'>La contraseña debe ser mí­nimo de 6 dijitos</div>";
		}
		else
		if($per == '' and $cat == 1)
		{
			echo "<div class='validaciones'>Debe elegir el perfil</div>";
		}
		else
		{
			if($act == 'false'){ $swact = 0; }elseif($act == 'true'){ $swact = 1; }
			
			if($inm == 'false'){ $inm = 0; $sql1 = 'usu_sw_inmobiliaria = 2'; }elseif($inm == 'true'){ $inm = 1; $sql1 = 'usu_sw_inmobiliaria = 1'; }
			if($vis == 'false'){ $vis = 0; $sql2 = 'or usu_sw_visita = 2'; }elseif($vis == 'true'){ $vis = 1; $sql2 = 'or usu_sw_visita = 1'; }
			if($dis == 'false'){ $dis = 0; $sql3 = 'or usu_sw_diseno = 2'; }elseif($dis == 'true'){ $dis = 1; $sql3 = 'or usu_sw_diseno = 1'; }
			if($lic == 'false'){ $lic = 0; $sql4 = 'or usu_sw_licencia = 2'; }elseif($lic == 'true'){ $lic = 1; $sql4 = 'or usu_sw_licencia = 1'; }
			if($int == 'false'){ $int = 0; $sql5 = 'or usu_sw_interventoria = 2'; }elseif($int == 'true'){ $int = 1; $sql5 = 'or usu_sw_interventoria = 1'; }
			if($cons == 'false'){ $cons = 0; $sql6 = 'or usu_sw_constructor = 2'; }elseif($cons == 'true'){ $cons = 1; $sql6 = 'or usu_sw_constructor = 1'; }
			if($seg == 'false'){ $seg = 0; $sql7 = 'or usu_sw_seguridad = 2'; }elseif($seg == 'true'){ $seg = 1; $sql7 = 'or usu_sw_seguridad = 1'; }
			if($otr == 'false'){ $otr = 0; $sql8 = 'or usu_sw_otro = 2'; }elseif($otr == 'true'){ $otr = 1; $sql8 = 'or usu_sw_otro = 1'; }
			if($her == 'false'){ $her = 0; }elseif($her == 'true'){ $her = 1; }
			
			if($conema != '' and $ema != '' and STRTOUPPER($conema) == STRTOUPPER($ema))
			{
				echo "<div class='validaciones'>El e-mail ingresado ya existe</div>";
			}
			else
			{
				//Si la categoria es usuario, no puede heredar nada
				if($cat == 1){ $her = 0; }
				$con = mysqli_query($conectar,"insert into usuario (usu_categoria,usu_usuario,usu_clave,usu_nombre,prf_clave_int,usu_sw_activo,usu_email,usu_sw_inmobiliaria,usu_sw_visita,usu_sw_diseno,usu_sw_licencia,usu_sw_interventoria,usu_sw_constructor,usu_sw_seguridad,usu_sw_otro,usu_sw_heredar,usu_usu_actualiz,usu_fec_actualiz) values('".$cat."','".$usu."','".$con1."','".$nom."','".$per."','".$swact."','".$ema."','".$inm."','".$vis."','".$dis."','".$lic."','".$int."','".$cons."','".$seg."','".$otr."','".$her."','".$usuario."','".$fecha."')");
				
				if($con >= 1)
				{
					echo "<div class='ok'>Datos grabados correctamente</div>";
					//Busco el usuario grabado, no por el último, si no por los datos grabados
					$con = mysqli_query($conectar,"select usu_clave_int,usu_categoria from usuario order by usu_clave_int DESC LIMIT 1");
					$dato = mysqli_fetch_array($con);
					$clausu = $dato['usu_clave_int'];
					$cat = $dato['usu_categoria'];
					//Verifico si la categoria es usuario= 1 borro todos los hijos que tenga relacionados
					if($cat == 1)
					{
						mysqli_query($conectar,"delete from usuario_actor where usa_actor = '".$u."'");
					}
				?>
					<table style="width: 100%">
						<tr>
							<td colspan="3"><strong>AGREGAR ACTORES AL USUARIO: <?php echo $nom; ?></strong></td>
						</tr>
						<tr>
							<td>
							<div id="agregar">
								<select class="auto-style8" <?php if($cat == 1){ echo 'disabled="disabled"'; } ?> multiple="multiple" ondblclick="AGREGAR('<?php echo $clausu; ?>')" name="agregarusu" id="agregarusu" style="width: 190px" size="8">
								<?php
									$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where usu_sw_activo = 1 and (".$sql1." ".$sql2." ".$sql3." ".$sql4." ".$sql5." ".$sql6." ".$sql7." ".$sql8.") and u.usu_clave_int <> '".$clausu."' and usu_categoria = 1 order by usu_nombre");
									$num = mysqli_num_rows($con);
									for($i = 0; $i < $num; $i++)
									{
										$dato = mysqli_fetch_array($con);
										$clave = $dato['usu_clave_int'];
										$nombre = $dato['usu_nombre'];
										$per = $dato['prf_descripcion'];
								?>
									<option value="<?php echo $clave; ?>"><?php echo $nombre." - ".$per; ?></option>
								<?php
									}
								?>
								</select>
							</div>
							</td>
							<td align="center">
							<div style="width: 60px">
								<input type="button" <?php if($cat == 1){ echo 'disabled="disabled"'; } ?> class="pasar izq" onclick="AGREGAR('<?php echo $clausu; ?>')" value="&raquo;" style="width: 30px"><input type="button" onclick="QUITAR('<?php echo $clausu; ?>')" <?php if($cat == 1){ echo 'disabled="disabled"'; } ?> class="quitar der" value="&laquo;" style="width: 30px"><br />
								<input type="button" class="pasartodos izq" <?php if($cat == 1){ echo 'disabled="disabled"'; } ?> onclick="PONERTODOS('<?php echo $clausu; ?>')" value="&raquo;&raquo;" style="width: 30px"><input type="button" <?php if($cat == 1){ echo 'disabled="disabled"'; } ?> onclick="REMOVERTODOS('<?php echo $clausu; ?>')" class="quitartodos der" value="&laquo;&laquo;" style="width: 30px">
							</div>
							</td>
							<td>
							<div id="agregados">
								<select class="auto-style8" <?php if($cat == 1){ echo 'disabled="disabled"'; } ?> multiple="multiple" ondblclick="QUITAR('<?php echo $clausu; ?>')" name="quitarusu" id="quitarusu" style="width: 190px" size="8">
								</select>
							</div>
							</td>
						</tr>
					</table>
				<?php
				}
				else
				{
					echo "<div class='validaciones'>No se han podido guardar los datos</div>";
				}
			}	
		}
		exit();
	}
	if($_GET['todos'] == 'si')
	{
		sleep(1);
		$rows=mysqli_query($conectar,"select * from usuario");
		$total=mysqli_num_rows($rows);
?>
		<table style="width: 100%">
		<tr>
			<td class="auto-style3" style="width: 27px">
				<input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<? echo $total;?>,this);" class="auto-style6" /><span class="auto-style6">
				</span>
			</td>
			<td class="auto-style3" colspan="9">
			<table style="width: 30%">
				<tr>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/activo.png" alt="" class="auto-style6" /><input type="submit" value="Activar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/inactivo.png" alt="" class="auto-style6" /><input type="submit" value="Inactivar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/eliminar.png" alt="" class="auto-style6" /><input type="submit" value="Eliminar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 180px"><strong>Nombre</strong></td>
			<td class="auto-style5" style="width: 120px"><strong>Usuario</strong></td>
			<td class="auto-style5" style="width: 110px"><strong>Perfil</strong></td>
			<td class="auto-style5" style="width: 200px"><strong>E-mail</strong></td>
			<td class="auto-style5" style="width: 98px"><strong>Categoria</strong></td>
			<td class="auto-style5" style="width: 98px"><strong>Creado Por</strong></td>
			<td class="auto-style5" style="width: 127px"><strong>
				Actualización</strong></td>
			<td class="auto-style5" style="width: 29px"><strong>
			Activo</strong></td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
		<?php
			$contador=0;
			$con = mysqli_query($conectar,"select * from usuario u inner join perfil prf ON (prf.prf_clave_int = u.prf_clave_int) order by u.usu_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clausu = $dato['usu_clave_int'];
				$cat = $dato['usu_categoria'];
				$nom = $dato['usu_nombre'];
				$usu = $dato['usu_usuario'];
				$pernom = $dato['prf_descripcion'];
				$act = $dato['usu_sw_activo'];
				$ema = $dato['usu_email'];
				$usuact = $dato['usu_usu_actualiz'];
				$fecact = $dato['usu_fec_actualiz'];
				$contador=$contador+1;
		?>
		<tr>
			<td class="auto-style3" style="width: 27px">
				<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<? echo $contador;?>" value="<? echo $dato['usu_clave_int'];?>" class="auto-style6" /></td>
			<td class="auto-style5" style="width: 180px"><?php echo $nom; ?></td>
			<td class="auto-style5" style="width: 120px"><?php echo $usu; ?></td>
			<td class="auto-style5" style="width: 110px"><?php echo $pernom; ?></td>
			<td class="auto-style5" style="width: 200px"><?php echo $ema; ?></td>
			<td class="auto-style5" style="width: 98px">
			<?php
				if($cat == 1)
				{
					echo 'Usuario';
				}
				else
				{
					echo 'Grupo';
				}
				?>
			</td>
			<td class="auto-style5" style="width: 98px"><?php echo $usuact; ?></td>
			<td class="auto-style5" style="width: 127px"><?php echo $fecact; ?></td>
			<td class="auto-style1" style="width: 30px">
			<input name="activarinactivar" id="activarinactivar" type="checkbox" <?php if($act == 1){ echo 'checked="checked"'; } ?> disabled="disabled" class="auto-style6" ></td>
			<td class="auto-style5"><a data-reveal-id="editarusuario" data-animation="fade" style="cursor:pointer" onclick="EDITAR('<?php echo $dato['usu_clave_int']; ?>','USUARIO')"><img src="../../img/editar.png" alt="" height="22" width="21" /></a></td>
		</tr>
		<?php
			}
		?>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 180px">&nbsp;</td>
			<td class="auto-style5" style="width: 120px">&nbsp;</td>
			<td class="auto-style5" style="width: 110px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 98px">&nbsp;</td>
			<td class="auto-style5" style="width: 98px">&nbsp;</td>
			<td class="auto-style5" style="width: 127px">&nbsp;</td>
			<td class="auto-style5" style="width: 29px">&nbsp;</td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
	</table>
<?php
		exit();
	}
	if($_GET['perfil'] == 'si')
	{
		sleep(1);
		
		if($_GET['nuevo'] == 'si')
		{
			$fecha=date("Y/m/d H:i:s");
			$cod = $_GET['cod'];
			$des = $_GET['des'];
			
			if($cod == 'Código'){ $cod = ''; }
			if($des == 'Descripción'){ $des = ''; }
			
			$con = mysqli_query($conectar,"select prf_codigo from perfil where prf_codigo = '".$cod."'");
			$datocod = mysqli_fetch_array($con);
			$concod = $datocod['prf_codigo'];
			
			$con = mysqli_query($conectar,"select prf_descripcion from perfil where UPPER(prf_descripcion) = UPPER('".$des."')");
			$datodes = mysqli_fetch_array($con);
			$condes = $datodes['prf_descripcion'];
			
			if($cod == '')
			{
				echo "<div class='validaciones' style='width: 99%' align='center'>Debe ingresar el Código</div>";
			}
			else
			if($cod == $concod)
			{
				echo "<div class='validaciones' style='width: 99%' align='center'>El Código ingresado ya existe</div>";
			}
			else
			if($des == '')
			{
				echo "<div class='validaciones' style='width: 99%' align='center'>Debe ingresar la Descripción</div>";
			}
			else
			if(STRTOUPPER($des) == STRTOUPPER($condes))
			{
				echo "<div class='validaciones' style='width: 99%' align='center'>La Descripción ingresada ya existe</div>";
			}
			else
			{
				$con = mysqli_query($conectar,"insert into perfil(prf_clave_int,prf_codigo,prf_descripcion,prf_usu_actualiz,prf_fec_actualiz) values(null,'".$cod."','".$des."','".$usuario."','".$fecha."')");
				
				if($con > 0)
				{
					echo "<div class='ok' style='width: 99%' align='center'>Datos Guardados Correctamente</div>";
				}
				else
				{
					echo "<div class='validaciones' style='width: 99%' align='center'>No se han podido guardar los datos</div>";
				}
			}
		}
		$rows=mysqli_query($conectar,"select * from perfil");
		$total=mysqli_num_rows($rows);
?>
		<table style="width: 100%">
		<tr>
			<td class="auto-style3" style="width: 27px">
				<input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<? echo $total;?>,this);" class="auto-style6" /><span class="auto-style6">
				</span>
			</td>
			<td class="auto-style3" colspan="5">
			<table style="width: 30%">
				<tr>
					<td class="auto-style1">
					<p style="cursor:pointer; width: 120px;">
					<img src="../../img/eliminar.png" alt="" class="auto-style6" /><input type="submit" value="Eliminar Perfil" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"></td>
					<td class="auto-style1"></td>
				</tr>
			</table>
			</td>
			<td class="auto-style3">
			&nbsp;</td>
		</tr>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 180px"><strong>Código</strong></td>
			<td class="auto-style5" style="width: 120px"><strong>Descripción</strong></td>
			<td class="auto-style5" style="width: 98px"><strong>Creado Por</strong></td>
			<td class="auto-style5" style="width: 127px"><strong>
				Actualización</strong></td>
			<td class="auto-style5" style="width: 30px">&nbsp;</td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
		<?php
			$contador=0;
			$con = mysqli_query($conectar,"select * from perfil order by prf_descripcion");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$claprf = $dato['prf_clave_int'];
				$cod = $dato['prf_codigo'];
				$des = $dato['prf_descripcion'];
				$usuact = $dato['prf_usu_actualiz'];
				$fecact = $dato['prf_fec_actualiz'];
				$contador=$contador+1;
		?>
		<tr>
			<td class="auto-style3" style="width: 27px">
				<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<? echo $contador;?>" value="<? echo $claprf; ?>" class="auto-style6" /></td>
			<td class="auto-style5" style="width: 180px"><?php echo $cod; ?></td>
			<td class="auto-style5" style="width: 120px"><?php echo $des; ?></td>
			<td class="auto-style5" style="width: 98px"><?php echo $usuact; ?></td>
			<td class="auto-style5" style="width: 127px"><?php echo $fecact; ?></td>
			<td class="auto-style5" style="width: 30px"><a style="cursor:pointer" onclick="EDITAR('<?php echo $claprf; ?>','PERFIL')"><img src="../../img/editar.png" alt="" height="22" width="21" title="Editar" /></a></td>
			<td class="auto-style5">
			<a data-reveal-id="permisos" data-animation="fade" style="cursor:pointer" onclick="PERMISOS('<?php echo $claprf; ?>')"><img src="../../images/permisos.png" alt="" height="22" width="21" title="Permisos" /></a>
			</td>
		</tr>
		<?php
			}
		?>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 180px">&nbsp;</td>
			<td class="auto-style5" style="width: 120px">&nbsp;</td>
			<td class="auto-style5" style="width: 98px">&nbsp;</td>
			<td class="auto-style5" style="width: 127px">&nbsp;</td>
			<td class="auto-style5" style="width: 30px">&nbsp;</td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
	</table>
<?php
		exit();
	}
	if($_GET['buscarusu'] == 'si')
	{
		$nom = $_GET['nom'];
		$ema = $_GET['ema'];
		$usu = $_GET['usu'];
		
		if($nom == 'Nombre'){ $nom = ''; }
		if($ema == 'E-mail'){ $ema = ''; }
		if($usu == 'Usuario'){ $usu = ''; }

		$rows=mysqli_query($conectar,"select * from usuario u where (u.usu_nombre LIKE REPLACE('".$nom."%',' ','%') OR '".$nom."' IS NULL OR '".$nom."' = '') and (u.usu_email LIKE REPLACE('".$ema."%',' ','%')  OR '".$ema."' IS NULL OR '".$ema."' = '') and (u.usu_usuario LIKE '".$usu."%' OR '".$usu."' IS NULL OR '".$usu."' = '')");
		$total=mysqli_num_rows($rows);
?>
		<table style="width: 100%">
		<tr>
			<td class="auto-style3" style="width: 27px">
				<input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<? echo $total;?>,this);" class="auto-style6" /><span class="auto-style6">
				</span>
			</td>
			<td class="auto-style3" colspan="9">
			<table style="width: 30%">
				<tr>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/activo.png" alt="" class="auto-style6" /><input type="submit" value="Activar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/inactivo.png" alt="" class="auto-style6" /><input type="submit" value="Inactivar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"><p style="cursor:pointer">
					<img src="../../img/eliminar.png" alt="" class="auto-style6" /><input type="submit" value="Eliminar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 180px"><strong>Nombre</strong></td>
			<td class="auto-style5" style="width: 120px"><strong>Usuario</strong></td>
			<td class="auto-style5" style="width: 110px"><strong>Perfil</strong></td>
			<td class="auto-style5" style="width: 200px"><strong>E-mail</strong></td>
			<td class="auto-style5" style="width: 110px"><strong>Categoria</strong></td>
			<td class="auto-style5" style="width: 98px"><strong>Creado Por</strong></td>
			<td class="auto-style5" style="width: 127px"><strong>
				Actualización</strong></td>
			<td class="auto-style5" style="width: 29px"><strong>
			Activo</strong></td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
		<?php
			$contador=0;
			$con = mysqli_query($conectar,"select * from usuario u inner join perfil prf ON (prf.prf_clave_int = u.prf_clave_int) where (u.usu_nombre LIKE REPLACE('".$nom."%',' ','%') OR '".$nom."' IS NULL OR '".$nom."' = '') and (u.usu_email LIKE REPLACE('".$ema."%',' ','%')  OR '".$ema."' IS NULL OR '".$ema."' = '') and (u.usu_usuario LIKE '".$usu."%' OR '".$usu."' IS NULL OR '".$usu."' = '') order by u.usu_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clausu = $dato['usu_clave_int'];
				$cat = $dato['usu_categoria'];
				$nom = $dato['usu_nombre'];
				$usu = $dato['usu_usuario'];
				$pernom = $dato['prf_descripcion'];
				$act = $dato['usu_sw_activo'];
				$ema = $dato['usu_email'];
				$usuact = $dato['usu_usu_actualiz'];
				$fecact = $dato['usu_fec_actualiz'];
				$contador=$contador+1;
		?>
		<tr>
			<td class="auto-style3" style="width: 27px">
				<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<? echo $contador;?>" value="<? echo $dato['usu_clave_int'];?>" class="auto-style6" /></td>
			<td class="auto-style5" style="width: 180px"><?php echo $nom; ?></td>
			<td class="auto-style5" style="width: 120px"><?php echo $usu; ?></td>
			<td class="auto-style5" style="width: 110px"><?php echo $pernom; ?></td>
			<td class="auto-style5" style="width: 200px"><?php echo $ema; ?></td>
			<td class="auto-style5" style="width: 110px">
			<?php
				if($cat == 1)
				{
					echo 'Usuario';
				}
				else
				{
					echo 'Grupo';
				}
				?>
			</td>
			<td class="auto-style5" style="width: 98px"><?php echo $usuact; ?></td>
			<td class="auto-style5" style="width: 127px"><?php echo $fecact; ?></td>
			<td class="auto-style1" style="width: 30px">
			<input name="activarinactivar" id="activarinactivar" type="checkbox" <?php if($act == 1){ echo 'checked="checked"'; } ?> disabled="disabled" class="auto-style6" ></td>
			<td class="auto-style5"><a data-reveal-id="editarusuario" data-animation="fade" style="cursor:pointer" onclick="EDITAR('<?php echo $dato['usu_clave_int']; ?>','USUARIO')"><img src="../../img/editar.png" alt="" height="22" width="21" /></a></td>
		</tr>
		<?php
			}
		?>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 180px">&nbsp;</td>
			<td class="auto-style5" style="width: 120px">&nbsp;</td>
			<td class="auto-style5" style="width: 110px">&nbsp;</td>
			<td class="auto-style5" style="width: 200px">&nbsp;</td>
			<td class="auto-style5" style="width: 110px">&nbsp;</td>
			<td class="auto-style5" style="width: 98px">&nbsp;</td>
			<td class="auto-style5" style="width: 127px">&nbsp;</td>
			<td class="auto-style5" style="width: 29px">&nbsp;</td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
	</table>
<?php
		exit();
	}
	if($_GET['buscarper'] == 'si')
	{
		$cod = $_GET['cod'];
		$des = $_GET['des'];
		
		if($cod == 'Codigo'){ $cod = ''; }
		if($des == 'Descripcion'){ $des = ''; }

		$rows=mysqli_query($conectar,"select * from perfil where (prf_codigo LIKE REPLACE('".$cod."%',' ','%') OR '".$cod."' IS NULL OR '".$cod."' = '') and (prf_descripcion LIKE REPLACE('".$des."%',' ','%')  OR '".$des."' IS NULL OR '".$des."' = '')");
		$total=mysqli_num_rows($rows);
?>
		<table style="width: 100%">
		<tr>
			<td class="auto-style3" style="width: 27px">
				<input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<? echo $total;?>,this);" class="auto-style6" /><span class="auto-style6">
				</span>
			</td>
			<td class="auto-style3" colspan="5">
			<table style="width: 30%">
				<tr>
					<td class="auto-style1">
					<p style="cursor:pointer; width: 120px;">
					<img src="../../img/eliminar.png" alt="" class="auto-style6" /><input type="submit" value="Eliminar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" class="auto-style6" /></p></td>
					<td class="auto-style1"></td>
					<td class="auto-style1"></td>
				</tr>
			</table>
			</td>
			<td class="auto-style3">
			&nbsp;</td>
		</tr>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 180px"><strong>Código</strong></td>
			<td class="auto-style5" style="width: 120px"><strong>Descripción</strong></td>
			<td class="auto-style5" style="width: 98px"><strong>Creado Por</strong></td>
			<td class="auto-style5" style="width: 127px"><strong>
				Actualización</strong></td>
			<td class="auto-style5" style="width: 30px">&nbsp;</td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
		<?php
			$contador=0;
			$con = mysqli_query($conectar,"select * from perfil where (prf_codigo LIKE REPLACE('".$cod."%',' ','%') OR '".$cod."' IS NULL OR '".$cod."' = '') and (prf_descripcion LIKE REPLACE('".$des."%',' ','%')  OR '".$des."' IS NULL OR '".$des."' = '') order by prf_descripcion");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$cla = $dato['prf_clave_int'];
				$cod = $dato['prf_codigo'];
				$des = $dato['prf_descripcion'];
				$usuact = $dato['prf_usu_actualiz'];
				$fecact = $dato['prf_fec_actualiz'];
				$contador=$contador+1;
		?>
		<tr>
			<td class="auto-style3" style="width: 27px">
				<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<? echo $contador;?>" value="<? echo $claprf; ?>" class="auto-style6" /></td>
			<td class="auto-style5" style="width: 180px"><?php echo $cod; ?></td>
			<td class="auto-style5" style="width: 120px"><?php echo $des; ?></td>
			<td class="auto-style5" style="width: 98px"><?php echo $usuact; ?></td>
			<td class="auto-style5" style="width: 127px"><?php echo $fecact; ?></td>
			<td class="auto-style5" style="width: 30px"><a style="cursor:pointer" onclick="EDITAR('<?php echo $claprf; ?>','PERFIL')"><img src="../../img/editar.png" alt="" height="22" width="21" title="Editar" /></a></td>
			<td class="auto-style5"><a data-reveal-id="permisos" data-animation="fade" style="cursor:pointer" onclick="PERMISOS('<?php echo $claprf; ?>')"><img src="../../images/permisos.png" alt="" height="22" width="21" title="Permisos" /></a></td>
		</tr>
		<?php
			}
		?>
		<tr>
			<td class="auto-style5" style="width: 27px">&nbsp;</td>
			<td class="auto-style5" style="width: 180px">&nbsp;</td>
			<td class="auto-style5" style="width: 120px">&nbsp;</td>
			<td class="auto-style5" style="width: 98px">&nbsp;</td>
			<td class="auto-style5" style="width: 127px">&nbsp;</td>
			<td class="auto-style5" style="width: 30px">&nbsp;</td>
			<td class="auto-style5">&nbsp;</td>
		</tr>
	</table>
<?php
		exit();
	}
	if($_GET['filtroperfil'] == 'si')
	{
?>
		<table style="width: 50%">
			<tr>
			<td class="auto-style1"><strong>Filtro:<img src="../../img/buscar.png" alt="" height="18" width="15" /></strong></td>
			<td class="auto-style1">
			<input class="inputs" onkeyup="BUSCAR('PERFIL')" name="buscodigoper" id="buscodigoper" maxlength="70" type="text" value="Codigo" onBlur="if(this.value=='') this.value='Codigo'" onFocus="if(this.value =='Codigo' ) this.value=''" style="width: 150px" /></td>
			<td class="auto-style1">
			<input class="inputs" onkeyup="BUSCAR('PERFIL')" name="busdescripcionper" id="busdescripcionper" maxlength="70" type="text" value="Descripcion" onBlur="if(this.value=='') this.value='Descripcion'" onFocus="if(this.value =='Descripcion' ) this.value=''" style="width: 150px" /></td>
			<td class="auto-style1">
			&nbsp;</td>
			<td class="auto-style1">
			&nbsp;</td>
			</tr>
		</table>		
<?php
		exit();
	}
	if($_GET['filtrousuario'] == 'si')
	{
?>
		<table style="width: 60%">
			<tr>
			<td class="auto-style1"><strong>Filtro:<img src="../../img/buscar.png" alt="" height="18" width="15" /></strong></td>
			<td class="auto-style1">
			<input class="inputs" onkeyup="BUSCAR('USUARIO')" name="nombre2" maxlength="70" type="text" value="Nombre" onBlur="if(this.value=='') this.value='Nombre'" onFocus="if(this.value =='Nombre' ) this.value=''" style="width: 150px" /></td>
			<td class="auto-style1">
			<input class="inputs" onkeyup="BUSCAR('USUARIO')" name="email2" maxlength="70" type="text" value="E-mail" onBlur="if(this.value=='') this.value='E-mail'" onFocus="if(this.value =='E-mail' ) this.value=''" style="width: 150px" /></td>
			<td class="auto-style1">
			<input class="inputs" onkeyup="BUSCAR('USUARIO')" name="usuario2" maxlength="70" type="text" value="Usuario" onBlur="if(this.value=='') this.value='Usuario'" onFocus="if(this.value =='Usuario' ) this.value=''" /></td>
			</tr>
		</table>		
<?php
		exit();
	}
	if($_GET['agregar'] == 'si')
	{
		$per = $_GET['per'];
		$con = mysqli_query($conectar,"select prf_descripcion from perfil where prf_clave_int = '".$per."'");
		$dato = mysqli_fetch_array($con);
		$nom = $dato['prf_descripcion'];
?>
	<table style="width: 38%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td align="center" colspan="3">
			<?php echo "<div class='ok' style='width: 99%' align='center'>Perfil: $nom</div>"; ?>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td align="center">
			<div id="agregar" align="center">
				<fieldset name="Group1" style="height: 320px">
				<legend>Registros Sin Seleccionar</legend>
					<select name="agregarven" id="agregarven" multiple="multiple" ondblclick="AGREGARPERMISOS('<?php echo $per; ?>')" style="width: 300px; height: 300px;" size="8">
					<?php
						$con = mysqli_query($conectar,"select ven_clave_int,ven_opcion from ventana where ven_clave_int not in (select ven_clave_int from permiso where prf_clave_int = '".$per."') order by ven_opcion");
						$num = mysqli_num_rows($con);
						for($i = 0; $i < $num; $i++)
						{
							$dato = mysqli_fetch_array($con);
							$clave = $dato['ven_clave_int'];
							$ventana = $dato['ven_opcion'];
					?>
						<option value="<?php echo $clave; ?>"><?php echo $ventana; ?></option>
					<?php
						}
					?>
					</select>
				</fieldset>
			</div>
			</td>
			<td align="center">
			<div style="width: 140px">
				<input type="button" class="pasar izq" onclick="AGREGARPERMISOS('<?php echo $per; ?>')" value="Pasar &raquo;"><input type="button" onclick="ELIMINARPERMISO('<?php echo $per; ?>')" class="quitar der" value="&laquo; Quitar"><br />
				<input type="button" class="pasartodos izq" onclick="AGREGARTODOS('<?php echo $per; ?>')" value="Todos &raquo;"><input type="button" onclick="QUITARTODOS('<?php echo $per; ?>')" class="quitartodos der" value="&laquo; Todos">
			</div>
			</td>
			<td align="center">
			<div id="agregados">
				<fieldset name="Group1" style="height: 320px">
					<legend>Registros Seleccionados</legend>
						<div style="overflow:auto;height: 300px; width:300px">
							<table style="width: 100%">
							   <tr>
								   <td>&nbsp;</td>
								   <td><strong>VENTANA</strong></td>
								   <td><strong>PERMISO</strong></td>
							   </tr>
							   <?php
									$con = mysqli_query($conectar,"select p.per_clave_int cla,v.ven_opcion ven, p.per_metodo met from permiso p inner join ventana v on (v.ven_clave_int = p.ven_clave_int) where p.prf_clave_int = '".$per."'");
									$num = mysqli_num_rows($con);
									for($i = 0; $i < $num; $i++)
									{
										$dato = mysqli_fetch_array($con);
										$claven = $dato['cla'];
										$ven = $dato['ven'];
										$met = $dato['met'];
								?>
							   <tr style="cursor:pointer" class="service_list" id="service<?php echo $claven; ?>" data="<?php echo $claven; ?>">
								   <td><input name="metodoseleccionado" id="metodoseleccionado<?php echo $claven; ?>" value="<?php echo $claven; ?>" type="checkbox" /></td>
								   <td><label for="metodoseleccionado<?php echo $claven; ?>"><?php echo $ven; ?></label></td>
								   <td>
								   <select name="metodo" id="metodo" onchange="METODO('<?php echo $claven; ?>')">
										<option value="0" <?php if($met == 0){ echo "selected='selected'"; } ?>>Consulta</option>
										<option value="1" <?php if($met == 1){ echo "selected='selected'"; } ?>>Modificación</option>
									</select>
								   </td>
							   </tr>
							   <?php
									}
								?>
						   </table>
						</div>
				</fieldset>
			</div>
			</td>
			<td>&nbsp;</td>
		</tr>
		</table>
<?php
		exit();
	}
	if($_GET['agregarseleccionados'] == 'si')
	{
		$ven = $_GET['ven'];
		$per = $_GET['per'];
		
		$seleccionados = explode(',',$ven);
		
		$num = count($seleccionados);
		
		for($i = 0; $i < $num; $i++)
		{
			$con = mysqli_query($conectar,"insert into permiso(per_clave_int,prf_clave_int,ven_clave_int,per_metodo) values(null,'".$per."','".$seleccionados[$i]."',1)");
		}
?>
		<fieldset name="Group1" style="height: 320px">
			<legend>Registros Seleccionados</legend>
				<div style="overflow:auto;height: 300px; width:300px">
					<table style="width: 100%">
					   <tr>
						   <td>&nbsp;</td>
						   <td><strong>VENTANA</strong></td>
						   <td><strong>MÉTODO</strong></td>
					   </tr>
					   <?php
							$con = mysqli_query($conectar,"select p.per_clave_int cla,v.ven_opcion ven, p.per_metodo met from permiso p inner join ventana v on (v.ven_clave_int = p.ven_clave_int) where p.prf_clave_int = '".$per."'");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$claven = $dato['cla'];
								$ven = $dato['ven'];
								$met = $dato['met'];
						?>
					   <tr style="cursor:pointer" class="service_list" id="service<?php echo $claven; ?>" data="<?php echo $claven; ?>">
						   <td><input name="metodoseleccionado" id="metodoseleccionado<?php echo $claven; ?>" value="<?php echo $claven; ?>" type="checkbox" /></td>
						   <td><label for="metodoseleccionado<?php echo $claven; ?>"><?php echo $ven; ?></label></td>
						   <td>
						   <select name="metodo" id="metodo" onchange="METODO('<?php echo $claven; ?>')">
								<option value="0" <?php if($met == 0){ echo "selected='selected'"; } ?>>Consulta</option>
								<option value="1" <?php if($met == 1){ echo "selected='selected'"; } ?>>Modificación</option>
							</select>
						   </td>
					   </tr>
					   <?php
							}
						?>
				   </table>
				</div>
		</fieldset>
<?php
		exit();
	}
	if($_GET['eliminaragregados'] == 'si')
	{
		$perm = $_GET['permiso'];
		$per = $_GET['per'];
		$seleccionados = explode(',',$perm);
		
		$num = count($seleccionados);
		
		for($i = 0; $i < $num; $i++)
		{
			if($seleccionados[$i] != '')
			{
				$con = mysqli_query($conectar,"delete from permiso where per_clave_int = '".$seleccionados[$i]."'");
			}
		}
?>
		<fieldset name="Group1" style="height: 320px">
		<legend>Registros Sin Seleccionar</legend>
			<select name="agregarven" id="agregarven" multiple="multiple" ondblclick="AGREGARPERMISOS('<?php echo $per; ?>')" style="width: 300px; height: 300px;" size="8">
			<?php
				$con = mysqli_query($conectar,"select ven_clave_int,ven_opcion from ventana where ven_clave_int not in (select ven_clave_int from permiso where prf_clave_int = '".$per."') order by ven_opcion");
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$clave = $dato['ven_clave_int'];
					$ventana = $dato['ven_opcion'];
			?>
				<option value="<?php echo $clave; ?>"><?php echo $ventana; ?></option>
			<?php
				}
			?>
			</select>
		</fieldset>
<?php
		exit();
	}
	if($_GET['agregartodos'] == 'si')
	{
		$per = $_GET['per'];
		
		$con = mysqli_query($conectar,"insert into permiso select null,'".$per."',ven_clave_int,1 from ventana where ven_clave_int not in (select ven_clave_int from permiso where prf_clave_int = '".$per."')");
?>
		<fieldset name="Group1" style="height: 320px">
			<legend>Registros Seleccionados</legend>
				<div style="overflow:auto;height: 300px; width:300px">
					<table style="width: 100%">
					   <tr>
						   <td>&nbsp;</td>
						   <td><strong>VENTANA</strong></td>
						   <td><strong>PERMISO</strong></td>
					   </tr>
					   <?php
							$con = mysqli_query($conectar,"select p.per_clave_int cla,v.ven_opcion ven, p.per_metodo met from permiso p inner join ventana v on (v.ven_clave_int = p.ven_clave_int) where p.prf_clave_int = '".$per."'");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$claven = $dato['cla'];
								$ven = $dato['ven'];
								$met = $dato['met'];
						?>
					   <tr style="cursor:pointer" class="service_list" id="service<?php echo $claven; ?>" data="<?php echo $claven; ?>">
						   <td><input name="metodoseleccionado" id="metodoseleccionado<?php echo $claven; ?>" value="<?php echo $claven; ?>" type="checkbox" /></td>
						   <td><label for="metodoseleccionado<?php echo $claven; ?>"><?php echo $ven; ?></label></td>
						   <td>
						   <select name="metodo" id="metodo" onchange="METODO('<?php echo $claven; ?>')">
								<option value="0" <?php if($met == 0){ echo "selected='selected'"; } ?>>Consulta</option>
								<option value="1" <?php if($met == 1){ echo "selected='selected'"; } ?>>Modificación</option>
							</select>
						   </td>
					   </tr>
					   <?php
							}
						?>
				   </table>
				</div>
		</fieldset>
<?php
		exit();
	}
	if($_GET['eliminartodos'] == 'si')
	{
		$per = $_GET['per'];
		$con = mysqli_query($conectar,"delete from permiso where prf_clave_int = '".$per."'");
?>
		<fieldset name="Group1" style="height: 320px">
		<legend>Registros Sin Seleccionar</legend>
			<select name="agregarven" id="agregarven" multiple="multiple" ondblclick="AGREGARPERMISOS('<?php echo $per; ?>')" style="width: 300px; height: 300px;" size="8">
			<?php
				$con = mysqli_query($conectar,"select ven_clave_int,ven_opcion from ventana where ven_clave_int not in (select ven_clave_int from permiso where prf_clave_int = '".$per."') order by ven_opcion");
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$clave = $dato['ven_clave_int'];
					$ventana = $dato['ven_opcion'];
			?>
				<option value="<?php echo $clave; ?>"><?php echo $ventana; ?></option>
			<?php
				}
			?>
			</select>
		</fieldset>
<?php
		exit();
	}
	if($_GET['cambiarmetodo'] == 'si')
	{
		$per = $_GET['per'];
		$met = $_GET['met'];
		
		$con = mysqli_query($conectar,"update permiso set per_metodo = '".$met."' where per_clave_int = '".$per."'");
		
		exit();
	}
	if($_GET['agregarusuario'] == "si")
	{
		$fecha=date("Y/m/d H:i:s");
		$usu = $_GET['usu'];
		$act = $_GET['act'];
				
		$seleccionados = explode(',',$usu);	
		$num = count($seleccionados);
		
		for($i = 0; $i < $num; $i++)
		{
			$con = mysqli_query($conectar,"select * from usuario_actor where usa_actor = '".$act."' and usu_clave_int = '".$seleccionados[$i]."'");
			$num1 = mysqli_num_rows($con);
			
			if($num1 <= 0 and $usu != '' and $usu != 0 and $act != '' and $act != 0)
			{
				$sql = mysqli_query($conectar,"insert into usuario_actor(usa_actor,usu_clave_int,usa_usu_actualiz,usa_fec_actualiz) values('".$act."','".$seleccionados[$i]."','".$usuario."','".$fecha."')");
			}
		}
		$sql1 = mysqli_query($conectar,"update usuario set usu_usu_actualiz = '".$usuario."', usu_fec_actualiz = '".$fecha."' where usu_clave_int = '".$act."'");
?>
		<select size="8" multiple="multiple" class="auto-style8" ondblclick="QUITAR('<?php echo $act; ?>')" name="quitarusu1" id="quitarusu" style="width: 190px">
		<?php 
        	$sql = mysqli_query($conectar,"select * from usuario_actor ua inner join usuario u on (u.usu_clave_int = ua.usu_clave_int) inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where ua.usa_actor = '".$act."' order by u.usu_nombre");
        	$num = mysqli_num_rows($sql);
        	for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($sql);
				$clave = $dato['usu_clave_int'];
				$nombre = $dato['usu_nombre'];
				$per = $dato['prf_descripcion'];
		?>
			<option value="<?php echo $clave; ?>"><?php echo $nombre." - ".$per; ?></option>
		<?php
			} 
		?>
        </select>
<?php
		exit();
	}
	if($_GET['quitarusuario'] == "si")
	{
		$fecha=date("Y/m/d H:i:s");
		$act = $_GET['act'];
		$usu = $_GET['usu'];
				
		$seleccionados = explode(',',$usu);	
		$num = count($seleccionados);
		
		for($i = 0; $i < $num; $i++)
		{
			if($act != '' and $usu != '')
			{
				$sql = mysqli_query($conectar,"delete from usuario_actor where usu_clave_int = '".$seleccionados[$i]."' and usa_actor = '".$act."'");
			}
		}
		$sql1 = mysqli_query($conectar,"update usuario set usu_usu_actualiz = '".$usuario."', usu_fec_actualiz = '".$fecha."' where usu_clave_int = '".$act."'");
		
		$con = mysqli_query($conectar,"select * from usuario where usu_clave_int = '".$act."'");
		$dato = mysqli_fetch_array($con); 
		
		$inm = $dato['usu_sw_inmobiliaria'];
		$vis = $dato['usu_sw_visita'];
		$lic = $dato['usu_sw_licencia'];
		$dis = $dato['usu_sw_diseno'];
		$int = $dato['usu_sw_interventoria'];
		$cons = $dato['usu_sw_constructor'];
		$seg = $dato['usu_sw_seguridad'];
		$otr = $dato['usu_sw_otro'];
		
		if($inm == 1){ $sql1 = 'usu_sw_inmobiliaria = 1'; }else{ $sql1 = 'usu_sw_inmobiliaria = 2'; }
		if($vis == 1){ $sql2 = 'or usu_sw_visita = 1'; }else{ $sql2 = 'or usu_sw_visita = 2'; }
		if($lic == 1){ $sql3 = 'or usu_sw_licencia = 1'; }else{ $sql3 = 'or usu_sw_licencia = 2'; }
		if($dis == 1){ $sql4 = 'or usu_sw_diseno = 1'; }else{ $sql4 = 'or usu_sw_diseno = 2'; }
		if($int == 1){ $sql5 = 'or usu_sw_interventoria = 1'; }else{ $sql5 = 'or usu_sw_interventoria = 2'; }
		if($cons == 1){ $sql6 = 'or usu_sw_constructor = 1'; }else{ $sql6 = 'or usu_sw_constructor = 2'; }
		if($seg == 1){ $sql7 = 'or usu_sw_seguridad = 1'; }else{ $sql7 = 'or usu_sw_seguridad = 2'; }
		if($otr == 1){ $sql8 = 'or usu_sw_otro = 1'; }else{ $sql8 = 'or usu_sw_otro = 2'; }
?>
		<select size="8" multiple="multiple" class="auto-style8" name="agregarusu" id="agregarusu" style="width: 190px" ondblclick="AGREGAR('<?php echo $act; ?>')">
		<?php
			$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where usu_clave_int NOT IN (select usu_clave_int from usuario_actor where usa_actor = '".$act."') and (".$sql1." ".$sql2." ".$sql3." ".$sql4." ".$sql5." ".$sql6." ".$sql7." ".$sql8.") and u.usu_clave_int <> '".$act."' and usu_categoria = 1 order by usu_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clave = $dato['usu_clave_int'];
				$nombre = $dato['usu_nombre'];
				$per = $dato['prf_descripcion'];
		?>
			<option value="<?php echo $clave; ?>"><?php echo $nombre." - ".$per; ?></option>
		<?php
			}
		?>
        </select>
<?php
		exit();
	}
	if($_GET['ponertodos'] == "si")
	{
		$fecha=date("Y/m/d H:i:s");
		$act = $_GET['act'];
		
		$con = mysqli_query($conectar,"select * from usuario where usu_clave_int = '".$act."'");
		$dato = mysqli_fetch_array($con); 
		
		$inm = $dato['usu_sw_inmobiliaria'];
		$vis = $dato['usu_sw_visita'];
		$lic = $dato['usu_sw_licencia'];
		$dis = $dato['usu_sw_diseno'];
		$int = $dato['usu_sw_interventoria'];
		$cons = $dato['usu_sw_constructor'];
		$seg = $dato['usu_sw_seguridad'];
		$otr = $dato['usu_sw_otro'];
		
		if($inm == 1){ $sql1 = 'usu_sw_inmobiliaria = 1'; }else{ $sql1 = 'usu_sw_inmobiliaria = 2'; }
		if($vis == 1){ $sql2 = 'or usu_sw_visita = 1'; }else{ $sql2 = 'or usu_sw_visita = 2'; }
		if($lic == 1){ $sql3 = 'or usu_sw_licencia = 1'; }else{ $sql3 = 'or usu_sw_licencia = 2'; }
		if($dis == 1){ $sql4 = 'or usu_sw_diseno = 1'; }else{ $sql4 = 'or usu_sw_diseno = 2'; }
		if($int == 1){ $sql5 = 'or usu_sw_interventoria = 1'; }else{ $sql5 = 'or usu_sw_interventoria = 2'; }
		if($cons == 1){ $sql6 = 'or usu_sw_constructor = 1'; }else{ $sql6 = 'or usu_sw_constructor = 2'; }
		if($seg == 1){ $sql7 = 'or usu_sw_seguridad = 1'; }else{ $sql7 = 'or usu_sw_seguridad = 2'; }
		if($otr == 1){ $sql8 = 'or usu_sw_otro = 1'; }else{ $sql8 = 'or usu_sw_otro = 2'; }
		
		$seleccionados = explode(',',$act);	
		$num = count($seleccionados);
		$con = mysqli_query($conectar,"insert into usuario_actor select null,'".$act."',usu_clave_int,'".$usuario."','".$fecha."' from usuario where usu_clave_int not in (select usu_clave_int from usuario_actor where usa_actor = '".$act."') and (".$sql1." ".$sql2." ".$sql3." ".$sql4." ".$sql5." ".$sql6." ".$sql7." ".$sql8.") and usu_clave_int <> '".$act."'");
?>
		<select size="8" multiple="multiple" class="auto-style8" ondblclick="QUITAR('<?php echo $act; ?>')" name="quitarusu" id="quitarusu" style="width: 190px">
		<?php 
        	$sql = mysqli_query($conectar,"select * from usuario_actor ua inner join usuario u on (u.usu_clave_int = ua.usu_clave_int) inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where ua.usa_actor = '".$act."' order by u.usu_nombre");
        	$num = mysqli_num_rows($sql);
        	for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($sql);
				$clave = $dato['usu_clave_int'];
				$nombre = $dato['usu_nombre'];
				$per = $dato['prf_descripcion'];
		?>
			<option value="<?php echo $clave; ?>"><?php echo $nombre." - ".$per; ?></option>
		<?php
			} 
		?>
        </select>
<?php
		exit();
	}
	if($_GET['quitartodos'] == "si")
	{
		$fecha=date("Y/m/d H:i:s");
		$act = $_GET['act'];
		$con = mysqli_query($conectar,"delete from usuario_actor where usa_actor = '".$act."'");
		
		$con = mysqli_query($conectar,"select * from usuario where usu_clave_int = '".$act."'");
		$dato = mysqli_fetch_array($con); 
		
		$inm = $dato['usu_sw_inmobiliaria'];
		$vis = $dato['usu_sw_visita'];
		$lic = $dato['usu_sw_licencia'];
		$dis = $dato['usu_sw_diseno'];
		$int = $dato['usu_sw_interventoria'];
		$cons = $dato['usu_sw_constructor'];
		$seg = $dato['usu_sw_seguridad'];
		$otr = $dato['usu_sw_otro'];
		
		if($inm == 1){ $sql1 = 'usu_sw_inmobiliaria = 1'; }else{ $sql1 = 'usu_sw_inmobiliaria = 2'; }
		if($vis == 1){ $sql2 = 'or usu_sw_visita = 1'; }else{ $sql2 = 'or usu_sw_visita = 2'; }
		if($lic == 1){ $sql3 = 'or usu_sw_licencia = 1'; }else{ $sql3 = 'or usu_sw_licencia = 2'; }
		if($dis == 1){ $sql4 = 'or usu_sw_diseno = 1'; }else{ $sql4 = 'or usu_sw_diseno = 2'; }
		if($int == 1){ $sql5 = 'or usu_sw_interventoria = 1'; }else{ $sql5 = 'or usu_sw_interventoria = 2'; }
		if($cons == 1){ $sql6 = 'or usu_sw_constructor = 1'; }else{ $sql6 = 'or usu_sw_constructor = 2'; }
		if($seg == 1){ $sql7 = 'or usu_sw_seguridad = 1'; }else{ $sql7 = 'or usu_sw_seguridad = 2'; }
		if($otr == 1){ $sql8 = 'or usu_sw_otro = 1'; }else{ $sql8 = 'or usu_sw_otro = 2'; }
?>
		<select class="auto-style8" multiple="multiple" ondblclick="AGREGAR('<?php echo $act; ?>')" name="agregarusu" id="agregarusu" style="width: 190px" size="8">
		<?php
			$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where usu_clave_int NOT IN (select usu_clave_int from usuario_actor where usa_actor = '".$act."') and (".$sql1." ".$sql2." ".$sql3." ".$sql4." ".$sql5." ".$sql6." ".$sql7." ".$sql8.") and u.usu_clave_int <> '".$act."' and usu_categoria = 1 order by usu_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clave = $dato['usu_clave_int'];
				$nombre = $dato['usu_nombre'];
				$per = $dato['prf_descripcion'];
		?>
			<option value="<?php echo $clave; ?>"><?php echo $nombre." - ".$per; ?></option>
		<?php
			}
		?>
		</select>
<?php
		exit();
	}
	if($_GET['mostraractores'] == 'si')
	{
		$act = $_GET['act'];
		$inm = $_GET['inm'];
		$vis = $_GET['vis'];
		$lic = $_GET['lic'];
		$dis = $_GET['dis'];
		$int = $_GET['int'];
		$cons = $_GET['cons'];
		$seg = $_GET['seg'];
		$otr = $_GET['otr'];
		
		if($inm == 'false'){ $inm = 0; $sql1 = 'usu_sw_inmobiliaria = 2'; mysqli_query($conectar,"delete from usuario_actor where usa_actor = '".$act."' and usu_clave_int in (select usu_clave_int from usuario where usu_sw_inmobiliaria = 1)"); }elseif($inm == 'true'){ $inm = 1; $sql1 = 'usu_sw_inmobiliaria = 1'; }
		if($vis == 'false'){ $vis = 0; $sql2 = 'or usu_sw_visita = 2'; mysqli_query($conectar,"delete from usuario_actor where usa_actor = '".$act."' and usu_clave_int in (select usu_clave_int from usuario where usu_sw_visita = 1)"); }elseif($vis == 'true'){ $vis = 1; $sql2 = 'or usu_sw_visita = 1'; }
		if($dis == 'false'){ $dis = 0; $sql3 = 'or usu_sw_diseno = 2'; mysqli_query($conectar,"delete from usuario_actor where usa_actor = '".$act."' and usu_clave_int in (select usu_clave_int from usuario where usu_sw_diseno = 1)"); }elseif($dis == 'true'){ $dis = 1; $sql3 = 'or usu_sw_diseno = 1'; }
		if($lic == 'false'){ $lic = 0; $sql4 = 'or usu_sw_licencia = 2'; mysqli_query($conectar,"delete from usuario_actor where usa_actor = '".$act."' and usu_clave_int in (select usu_clave_int from usuario where usu_sw_licencia = 1)"); }elseif($lic == 'true'){ $lic = 1; $sql4 = 'or usu_sw_licencia = 1'; }
		if($int == 'false'){ $int = 0; $sql5 = 'or usu_sw_interventoria = 2'; mysqli_query($conectar,"delete from usuario_actor where usa_actor = '".$act."' and usu_clave_int in (select usu_clave_int from usuario where usu_sw_interventoria = 1)"); }elseif($int == 'true'){ $int = 1; $sql5 = 'or usu_sw_interventoria = 1'; }
		if($cons == 'false'){ $cons = 0; $sql6 = 'or usu_sw_constructor = 2'; mysqli_query($conectar,"delete from usuario_actor where usa_actor = '".$act."' and usu_clave_int in (select usu_clave_int from usuario where usu_sw_constructor = 1)"); }elseif($cons == 'true'){ $cons = 1; $sql6 = 'or usu_sw_constructor = 1'; }
		if($seg == 'false'){ $seg = 0; $sql7 = 'or usu_sw_seguridad = 2'; mysqli_query($conectar,"delete from usuario_actor where usa_actor = '".$act."' and usu_clave_int in (select usu_clave_int from usuario where usu_sw_seguridad = 1)"); }elseif($seg == 'true'){ $seg = 1; $sql7 = 'or usu_sw_seguridad = 1'; }
		if($otr == 'false'){ $otr = 0; $sql8 = 'or usu_sw_otro = 2'; mysqli_query($conectar,"delete from usuario_actor where usa_actor = '".$act."' and usu_clave_int in (select usu_clave_int from usuario where usu_sw_otro = 1)"); }elseif($otr == 'true'){ $otr = 1; $sql8 = 'or usu_sw_otro = 1'; }
		
		$con = mysqli_query($conectar,"update usuario set usu_sw_inmobiliaria = '".$inm."', usu_sw_visita = '".$vis."', usu_sw_diseno = '".$dis."', usu_sw_licencia = '".$lic."', usu_sw_interventoria = '".$int."', usu_sw_constructor = '".$cons."', usu_sw_seguridad = '".$seg."', usu_sw_otro = '".$otr."' where usu_clave_int = '".$act."'");
		
		$con = mysqli_query($conectar,"select * from usuario where usu_clave_int = '".$act."'");
		$dato = mysqli_fetch_array($con);
		$cat = $dato['usu_categoria'];
?>
		<select class="auto-style8" multiple="multiple" <?php if($cat == 1){ echo 'disabled="disabled"'; } ?> ondblclick="AGREGAR('<?php echo $act; ?>')" name="agregarusu" id="agregarusu" style="width: 190px" size="8">
		<?php
			$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where usu_clave_int NOT IN (select usu_clave_int from usuario_actor where usa_actor = '".$act."') and (".$sql1." ".$sql2." ".$sql3." ".$sql4." ".$sql5." ".$sql6." ".$sql7." ".$sql8.") and u.usu_clave_int <> '".$act."' and usu_categoria = 1 order by usu_nombre");
			$num = mysqli_num_rows($con);
			for($i = 0; $i < $num; $i++)
			{
				$dato = mysqli_fetch_array($con);
				$clave = $dato['usu_clave_int'];
				$nombre = $dato['usu_nombre'];
				$per = $dato['prf_descripcion'];
		?>
			<option value="<?php echo $clave; ?>"><?php echo $nombre." - ".$per; ?></option>
		<?php
			}
		?>
		</select>
<?php
		exit();
	}
	if($_GET['mostraractoresagregados'] == 'si')
	{
		$act = $_GET['act'];
		$con = mysqli_query($conectar,"select * from usuario where usu_clave_int = '".$act."'");
		$dato = mysqli_fetch_array($con);
		$cat = $dato['usu_categoria'];
		$inm = $dato['usu_sw_inmobiliaria'];
		$vis = $dato['usu_sw_visita'];
		$lic = $dato['usu_sw_licencia'];
		$dis = $dato['usu_sw_diseno'];
		$int = $dato['usu_sw_interventoria'];
		$cons = $dato['usu_sw_constructor'];
		$seg = $dato['usu_sw_seguridad'];
		$otr = $dato['usu_sw_otro'];
		
		if($inm == 1){ $sql1 = 'usu_sw_inmobiliaria = 1'; }else{ $sql1 = 'usu_sw_inmobiliaria = 2'; }
		if($vis == 1){ $sql2 = 'or usu_sw_visita = 1'; }else{ $sql2 = 'or usu_sw_visita = 2'; }
		if($lic == 1){ $sql3 = 'or usu_sw_licencia = 1'; }else{ $sql3 = 'or usu_sw_licencia = 2'; }
		if($dis == 1){ $sql4 = 'or usu_sw_diseno = 1'; }else{ $sql4 = 'or usu_sw_diseno = 2'; }
		if($int == 1){ $sql5 = 'or usu_sw_interventoria = 1'; }else{ $sql5 = 'or usu_sw_interventoria = 2'; }
		if($cons == 1){ $sql6 = 'or usu_sw_constructor = 1'; }else{ $sql6 = 'or usu_sw_constructor = 2'; }
		if($seg == 1){ $sql7 = 'or usu_sw_seguridad = 1'; }else{ $sql7 = 'or usu_sw_seguridad = 2'; }
		if($otr == 1){ $sql8 = 'or usu_sw_otro = 1'; }else{ $sql8 = 'or usu_sw_otro = 2'; }
		?>
		<select size="8" multiple="multiple" <?php if($cat == 1){ echo 'disabled="disabled"'; } ?> class="auto-style8" ondblclick="QUITAR('<?php echo $usuedi; ?>')" name="quitarusu" id="quitarusu" style="width: 190px">
		<?php 
        	$sql = mysqli_query($conectar,"select * from usuario_actor ua inner join usuario u on (u.usu_clave_int = ua.usu_clave_int) inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where ua.usa_actor = '".$act."' order by u.usu_nombre");
        	$num = mysqli_num_rows($sql);
        	for($i = 0; $i < $num; $i++)
        	{
        		$dato = mysqli_fetch_array($sql);
				$clave = $dato['usu_clave_int'];
				$nombre = $dato['usu_nombre'];
				$per = $dato['prf_descripcion'];
			?>
				<option value="<?php echo $clave; ?>"><?php echo $nombre." - ".$per; ?></option>
		<?php } ?>
        </select>
		<?php
		exit();
	}
?>
<!DOCTYPE HTML>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

	
<title>CONTROL DE OBRAS</title>
<meta name="description" content="Service Desk">
<meta name="author" content="InvGate S.R.L.">

<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
<link rel="stylesheet" href="css/style.css" type="text/css" />

<script type="text/javascript" language="javascript">
	selecteds=0;
	
	function CheckUncheck(total,check){
		checkbox=null;
		for(i=1;i<=total;i++){
			checkbox=document.getElementById("idcat"+i);
			//alert(checkbox.value);
			checkbox.checked=check.checked;
		}
		
		if(check.checked){
			selecteds=total;
		}else{
			selecteds=0;
		}
		
	}
	
	function contadorVals(check){
		if(check.checked){
			selecteds=selecteds+1;
		}else{
			selecteds=selecteds-1;
		}
	}
	
	function selectedVals(){
		if(selecteds==0){
			alert("Seleccione al menos un registro.");
			return false;
		}else{
			return true;
		}
	}
	function ACTIVARACTORES(v)
	{
		if(v == 1)
		{
			form1.agregarusu.disabled = true;
			form1.pasar.disabled = true;
			form1.quitar.disabled = true;
			form1.pasartodos.disabled = true;
			form1.quitartodos.disabled = true;
			form1.quitarusu.disabled = true;
			
			form1.usuario1.disabled = false;
			form1.Password3.disabled = false;
			form1.Password4.disabled = false;
			form1.perfil1.disabled = false;
			form1.email1.disabled = false;
			
			form1.heredar1.disabled = true;
			$("#heredar1").attr('checked', false);
		}
		else
		{
			form1.agregarusu.disabled = false;
			form1.pasar.disabled = false;
			form1.quitar.disabled = false;
			form1.pasartodos.disabled = false;
			form1.quitartodos.disabled = false;
			form1.quitarusu.disabled = false;
			
			form1.usuario1.disabled = true;
			form1.Password3.disabled = true;
			form1.Password4.disabled = true;
			form1.perfil1.disabled = true;
			form1.email1.disabled = true;
			
			form1.heredar1.disabled = false;
		}
	}
	function ACTIVARACTORES1(v)
	{
		if(v == 1)
		{
			form1.usuario.disabled = false;
			form1.Password1.disabled = false;
			form1.Password2.disabled = false;
			form1.perfil.disabled = false;
			form1.email.disabled = false;
			
			form1.heredar.disabled = true;
			$("#heredar").attr('checked', false);
		}
		else
		{
			form1.usuario.disabled = true;
			form1.Password1.disabled = true;
			form1.Password2.disabled = true;
			form1.perfil.disabled = true;
			form1.email.disabled = true;
			
			form1.heredar.disabled = false;
		}
	}
	function OCULTARSCROLL()
	{
		setTimeout("parent.autoResize('iframe1')",1000);
	}
	setTimeout("parent.autoResize('iframe1')",1000);
</script>
<?php //VALIDACIONES ?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="llamadas.js"></script>

<?php //VENTANA EMERGENTE ?>
<link rel="stylesheet" href="../../css/reveal.css" />
<script type="text/javascript" src="../../js/jquery.reveal.js"></script>
</head>
<body>
<?php
$rows=mysqli_query($conectar,"select * from usuario");
$total=mysqli_num_rows($rows);
?>
<form name="form1" id="form1" action="confirmar.php" method="post" onsubmit="return selectedVals();">
<!--[if lte IE 7]>
<div class="ieWarning">Este navegador no es compatible con el sistema. Por favor, use Chrome, Safari, Firefox o Internet Explorer 8 o superior.</div>
<![endif]-->
<table style="width: 100%">
	<tr>
		<td class="auto-style2" onclick="CONSULTAMODULO('TODOS')" onmouseover="this.style.backgroundColor='#445B74';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="width: 80px; cursor:pointer">
		Todos
		<?php
			$con = mysqli_query($conectar,"select COUNT(*) cant from usuario");
			$dato = mysqli_fetch_array($con);
			echo $dato['cant'];
		?>
		</td>
		<td class="auto-style2" onclick="CONSULTAMODULO('PERFIL')" onmouseover="this.style.backgroundColor='#445B74';this.style.color='#ffffff';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="width: 80px; cursor:pointer">
		Perfil
		<?php
			$con = mysqli_query($conectar,"select COUNT(*) cant from perfil");
			$dato = mysqli_fetch_array($con);
			echo $dato['cant'];
		?>
		</td>
		<td class="auto-style7" style="cursor:pointer" colspan="4">
		
		USUARIOS Y PERFILES</td>
		<td class="auto-style2" onmouseover="this.style.backgroundColor='#CCCCCC';this.style.color='#0F213C';"  onmouseout="this.style.backgroundColor='#ffffff';this.style.color='#000000';" style="width: 55px; cursor:pointer">
		<a data-reveal-id="nuevousuario" data-animation="fade" style="cursor:pointer"><table style="width: 100%">
			<tr>
				<td style="width: 14px"><a data-reveal-id="nuevousuario" data-animation="fade" style="cursor:pointer"><img alt="" src="../../img/add2.png"></a></td>
				<td>Añadir</td>
			</tr>
		</table></a>
		</td>
	</tr>
	<tr>
		<td class="auto-style2" colspan="7">
		<div id="filtro">
			<table style="width: 60%">
				<tr>
					<td class="auto-style1"><strong>Filtro:<img src="../../img/buscar.png" alt="" height="18" width="15" /></strong></td>
					<td class="auto-style1">
			<input class="inputs" onkeyup="BUSCAR('USUARIO')" name="nombre2" maxlength="70" type="text" value="Nombre" onBlur="if(this.value=='') this.value='Nombre'" onFocus="if(this.value =='Nombre' ) this.value=''" style="width: 150px" /></td>
					<td class="auto-style1">
			<input class="inputs" onkeyup="BUSCAR('USUARIO')" name="email2" maxlength="70" type="text" value="E-mail" onBlur="if(this.value=='') this.value='E-mail'" onFocus="if(this.value =='E-mail' ) this.value=''" style="width: 150px" /></td>
					<td class="auto-style1">
			<input class="inputs" onkeyup="BUSCAR('USUARIO')" name="usuario2" maxlength="70" type="text" value="Usuario" onBlur="if(this.value=='') this.value='Usuario'" onFocus="if(this.value =='Usuario' ) this.value=''" /></td>
				</tr>
			</table>
		</div>
		</td>
	</tr>
	<tr>
		<td class="auto-style2" colspan="7">
		<div id="nuevoperfil" style="display:none">
			<table style="width: 60%">
				<tr>
					<td>
					<strong>Nuevo perfil:</strong></td>
					<td>
			<input class="inputs" name="codigoper" id="codigoper" maxlength="70" type="text" value="Código" onBlur="if(this.value=='') this.value='Código'" onFocus="if(this.value =='Código' ) this.value=''" style="width: 150px" /></td>
					<td>
			<input class="inputs" name="descripcionper" id="descripcionper" maxlength="70" type="text" value="Descripción" onBlur="if(this.value=='') this.value='Descripción'" onFocus="if(this.value =='Descripción' ) this.value=''" style="width: 150px" /></td>
					<td><a onclick="NUEVO('PERFIL')" style="cursor:pointer" class="auto-style26"><span class="auto-style27">
					<strong>GUARDAR PERFIL</strong></span></a></td>
				</tr>
			</table>
		</div>
		</td>
	</tr>
	<tr>
		<td colspan="7" class="auto-style2">
		<div id="editarusuario" class="reveal-modal" style="left: 57%; top: 50px; height: 510px; width: 450px;">
			<table style="width: 38%" align="center">
				<tr>
					<td>&nbsp;</td>
					<td class="auto-style3">Nombre:</td>
					<td class="auto-style3"><input class="inputs" name="nombre1" maxlength="50" value="<?php echo $nom; ?>" type="text" style="width: 200px" />
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="auto-style3">Usuario:</td>
					<td class="auto-style3"><input class="inputs" name="usuario1" maxlength="15" type="text" value="<?php echo $usu; ?>" style="width: 200px" />
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="auto-style3">Contraseña:</td>
					<td class="auto-style3"><input class="inputs" name="Password3" maxlength="15" type="password" value="<?php echo $con; ?>" style="width: 200px" />
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="auto-style3">Repetir Contraseña:</td>
					<td class="auto-style3">
					<input class="inputs" name="Password4" maxlength="15" onkeyup="VALIDAR('CONTRASENA')" type="password" value="<?php echo $con; ?>" style="width: 200px" /></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="auto-style3">Perfil de acceso:</td>
					<td class="auto-style3">
						<select class="inputs" name="perfil1" style="width: 200px">
						<?php
							$con = mysqli_query($conectar,"select * from perfil order by prf_descripcion");
							$num = mysqli_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = mysqli_fetch_array($con);
								$clave = $dato['prf_clave_int'];
								$perfil = $dato['prf_descripcion'];
						?>
							<option value="<?php echo $clave; ?>" <?php $con1 = mysqli_query($conectar,"select prf_clave_int from usuario where usu_clave_int = '".$usuedi."'"); $dato = mysqli_fetch_array($con1); $p = $dato['prf_clave_int']; if($p == $clave){  echo "selected='selected'"; } ?>><?php echo $perfil; ?></option>
						<?php
							}
						?>
						</select>
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="auto-style3">E-mail:</td>
					<td class="auto-style3"><input class="inputs" name="email1" maxlength="100" type="text" value="<?php echo $ema; ?>" style="width: 200px" /></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="auto-style3">empresa:</td>
					<td class="auto-style3">
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="auto-style3">Activo:</td>
					<td class="auto-style3"><input class="inputs" <?php if($act == 1){ echo 'checked="checked"'; } ?> name="activo1" type="checkbox" /></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="4">
					<input name="submit" type="button" value="Guardar" onclick="GUARDAR('USUARIO','<?php echo $usuedi; ?>')"  style="width: 348px; height: 25px; cursor:pointer" /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td colspan="2">
					<div id="datos">
					</div>
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
			<a class="close-reveal-modal">&#215;</a>
		</div>
		<div id="usuarios">
		<table style="width: 100%">
			<tr>
				<td class="auto-style3" style="width: 27px">
					<input type="checkbox" name="selectall" id="selectall" onclick="CheckUncheck(<? echo $total;?>,this);" />
				</td>
				<td class="auto-style3" colspan="9">
				<table style="width: 30%">
					<tr>
						<td class="auto-style1"><p style="cursor:pointer"><img src="../../img/activo.png" alt="" /><input type="submit" value="Activar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" /></p></td>
						<td class="auto-style1"><p style="cursor:pointer"><img src="../../img/inactivo.png" alt="" /><input type="submit" value="Inactivar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" /></p></td>
						<td class="auto-style1"><p style="cursor:pointer"><img src="../../img/eliminar.png" alt="" /><input type="submit" value="Eliminar" name="Accion" style="border-style: none; border-color: inherit; border-width: thin; cursor: pointer; background-color:inherit" /></p></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td class="auto-style3" style="width: 27px">&nbsp;</td>
				<td class="auto-style3" style="width: 180px"><strong>Nombre</strong></td>
				<td class="auto-style3" style="width: 120px"><strong>Usuario</strong></td>
				<td class="auto-style3" style="width: 110px"><strong>Perfil</strong></td>
				<td class="auto-style3" style="width: 200px"><strong>
				E-mail</strong></td>
				<td class="auto-style3" style="width: 110px"><strong>Categoria</strong></td>
				<td class="auto-style3" style="width: 98px"><strong>Creado Por</strong></td>
				<td class="auto-style3" style="width: 127px"><strong>
				Actualización</strong></td>
				<td class="auto-style3" style="width: 29px"><strong>
				Activo</strong></td>
				<td class="auto-style3">&nbsp;</td>
			</tr>
			<?php
				$contador=0;
				$con = mysqli_query($conectar,"select * from usuario u inner join perfil prf ON (prf.prf_clave_int = u.prf_clave_int) order by u.usu_nombre");
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$clausu = $dato['usu_clave_int'];
					$cat = $dato['usu_categoria'];
					$nom = $dato['usu_nombre'];
					$usu = $dato['usu_usuario'];
					$pernom = $dato['prf_descripcion'];
					$act = $dato['usu_sw_activo'];
					$ema = $dato['usu_email'];
					$usuact = $dato['usu_usu_actualiz'];
					$fecact = $dato['usu_fec_actualiz'];
					$contador=$contador+1;
			?>
			<tr>
				<td class="auto-style3" style="width: 27px">
					<input onclick="contadorVals(this);" type="checkbox" name="idcat[]" id="idcat<? echo $contador;?>" value="<? echo $dato['usu_clave_int'];?>" /></td>
				<td class="auto-style3" style="width: 180px"><?php echo $nom; ?></td>
				<td class="auto-style3" style="width: 120px"><?php echo $usu; ?></td>
				<td class="auto-style3" style="width: 110px"><?php echo $pernom; ?></td>
				<td class="auto-style3" style="width: 200px"><?php echo $ema; ?></td>
				<td class="auto-style3" style="width: 110px">
				<?php
				if($cat == 1)
				{
					echo 'Usuario';
				}
				else
				{
					echo 'Grupo';
				}
				?>
				</td>
				<td class="auto-style3" style="width: 98px"><?php echo $usuact; ?></td>
				<td class="auto-style3" style="width: 127px"><?php echo $fecact; ?></td>
				<td class="auto-style1" style="width: 30px">
				<input name="activarinactivar" id="activarinactivar" type="checkbox" <?php if($act == 1){ echo 'checked="checked"'; } ?> disabled="disabled" ></td>
				<td class="auto-style3">
				<a data-reveal-id="editarusuario" data-animation="fade" style="cursor:pointer" onclick="EDITAR('<?php echo $dato['usu_clave_int']; ?>','USUARIO')">
				<img src="../../img/editar.png" alt="" height="22" width="21" />
				</a>
				</td>
				<?php
					}
				?>
			</tr>
			<tr>
				<td class="auto-style3" style="width: 27px">&nbsp;</td>
				<td class="auto-style3" style="width: 180px">&nbsp;</td>
				<td class="auto-style3" style="width: 120px">&nbsp;</td>
				<td class="auto-style3" style="width: 110px">&nbsp;</td>
				<td class="auto-style3" style="width: 200px">&nbsp;</td>
				<td class="auto-style3" style="width: 110px">&nbsp;</td>
				<td class="auto-style3" style="width: 98px">&nbsp;</td>
				<td class="auto-style3" style="width: 127px">&nbsp;</td>
				<td class="auto-style3" style="width: 29px">&nbsp;</td>
				<td class="auto-style3">&nbsp;</td>
			</tr>
		</table>
		</div>
<div id="nuevousuario" class="auto-style4" style="left: 57%; top: 50px; height: 510px; width: 450px;">
<table style="width: 38%" align="center">
	<tr>
		<td>&nbsp;</td>
		<td>Categoria:</td>
		<td>
		<select class="inputs" name="categoria" id="categoria" onchange="ACTIVARACTORES1(this.value)" style="width: 260px">
		<option value="1">Usuario</option>
		<option value="2">Grupo</option>
		</select>
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Nombre:</td>
		<td><input class="inputs" name="nombre" maxlength="50" type="text" style="width: 250px" />
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Usuario:</td>
		<td><input class="inputs" name="usuario" maxlength="15" type="text" style="width: 250px" />
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Contraseña:</td>
		<td><input class="inputs" name="Password1" maxlength="15" type="password" style="width: 250px" />
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Repetir Contraseña:</td>
		<td>
		<input class="inputs" name="Password2" maxlength="15" onkeyup="VALIDAR('CONTRASENA')" type="password" style="width: 250px" /></td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Perfil de acceso:</td>
		<td>
			<select class="inputs" name="perfil" style="width: 260px">
			<?php
				$con = mysqli_query($conectar,"select * from perfil order by prf_descripcion");
				$num = mysqli_num_rows($con);
				for($i = 0; $i < $num; $i++)
				{
					$dato = mysqli_fetch_array($con);
					$clave = $dato['prf_clave_int'];
					$perfil = $dato['prf_descripcion'];
			?>
				<option value="<?php echo $clave; ?>"><?php echo $perfil; ?></option>
			<?php
				}
			?>
			</select>
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>E-mail:</td>
		<td><input class="inputs" name="email" maxlength="100" type="text" style="width: 250px" /></td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Actor:</td>
		<td><label for="heredar" style="cursor:pointer">Heredar Actores:</label> <input name="heredar" id="heredar" disabled="disabled" class="inputs" type="checkbox" /></td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2">
		<table style="width: 100%">
			<tr>
				<td>
				<input name="inmobiliaria" id="inmobiliaria" type="checkbox" />
				</td>
				<td><label for="inmobiliaria" style="cursor:pointer">Inmobiliaria</label></td>
				<td>
				<input name="visita" id="visita" type="checkbox" /></td>
				<td><label for="visita" style="cursor:pointer">Visita</label></td>
				<td>
				<input name="diseno" id="diseno" type="checkbox" /></td>
				<td><label for="diseno" style="cursor:pointer">Diseñador</label></td>
				<td>
				<input name="licencia" id="licencia" type="checkbox" /></td>
				<td><label for="licencia" style="cursor:pointer">Licencia</label></td>
			</tr>
			<tr>
				<td>
				<input name="interventor" id="interventor" type="checkbox" /></td>
				<td><label for="interventor" style="cursor:pointer">Interventor</label></td>
				<td>
				<input name="constructor" id="constructor" type="checkbox" /></td>
				<td><label for="constructor" style="cursor:pointer">Constructor</label></td>
				<td>
				<input name="seguridad" id="seguridad" type="checkbox" /></td>
				<td><label for="seguridad" style="cursor:pointer">Seguridad</label></td>
				<td>
				<input name="otro" id="otro" type="checkbox" />
				</td>
				<td><label for="otro" style="cursor:pointer">Otro</label></td>
			</tr>
		</table>
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Activo: <input class="inputs" name="activo" checked="checked" type="checkbox" /></td>
		<td></td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="4">
		<input name="nuevo1" type="button" value="Guardar" onclick="NUEVO('USUARIO')"  style="width: 430px; height: 25px; cursor:pointer" /></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td colspan="2">
		<div id="datos1">
		<table style="width: 100%">
			<tr>
				<td>Actor:</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>
				<div id="agregar">
					<select class="auto-style8" disabled='disabled' name="agregarusu" id="agregarusu" style="width: 190px" size="8">
					<?php
						$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_sw_activo = 1 order by u.usu_nombre");
						$num = mysqli_num_rows($con);
						for($i = 0; $i < $num; $i++)
						{
							$dato = mysqli_fetch_array($con);
							$clave = $dato['usu_clave_int'];
							$nombre = $dato['usu_nombre'];
							$per = $dato['prf_descripcion'];
					?>
						<option value="<?php echo $clave; ?>"><?php echo $nombre." - ".$per; ?></option>
					<?php
						}
					?>
					</select>
				</div>
				</td>
				<td align="center">
				<div style="width: 65px">
					<input name="pasar" id="pasar" type="button" class="pasar izq" disabled="disabled" value="&raquo;" style="width: 30px">
					<input name="quitar" id="quitar" type="button" disabled="disabled" class="quitar der" value="&laquo;" style="width: 30px"><br />
					<input name="pasartodos" id="pasartodos" type="button" class="pasartodos izq" disabled="disabled" value="&raquo;&raquo;" style="width: 30px"><input name="quitartodos" id="quitartodos" type="button" disabled="disabled" class="quitartodos der" value="&laquo;&laquo;" style="width: 30px">
				</div>
				</td>
				<td>
				<div id="quitar">
					<select class="auto-style8" disabled='disabled' name="quitarusu" id="quitarusu" style="width: 190px" size="8">
					</select>
				</div>
				</td>
			</tr>
		</table>
		</div>
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>
</div>
		</td>
	</tr>
	<tr>
		<td style="width: 80px">&nbsp;</td>
		<td style="width: 80px">&nbsp;</td>
		<td style="width: 80px">&nbsp;</td>
		<td style="width: 80px">&nbsp;</td>
		<td style="width: 80px">&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td style="width: 80px">&nbsp;</td>
		<td style="width: 80px">&nbsp;</td>
		<td style="width: 80px">&nbsp;</td>
		<td style="width: 80px">&nbsp;</td>
		<td style="width: 80px">&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>
<div id="permisos" class="reveal-modal" style="left: 36%; top: 50px; height: 345px; width: 830px;">
	<table style="width: 38%" align="center">
		<tr>
			<td>&nbsp;</td>
			<td align="center">
			<div id="agregar" style="height: 180px" align="center">
				<fieldset name="Group1">
				<legend>Registros Sin Seleccionar</legend>
					<select class="auto-style8" <?php if($prf <> 'Cliente'){ echo "disabled='disabled'"; } ?> ondblclick="AGREGAR('EQUIPO','<?php echo $usuedi; ?>');CONSULTAMODULO('CLIENTES')" name="agregarequ" style="width: 150px; height: 150px;" size="8">
					<?php
						$con = mysqli_query($conectar,"select * from equipo where equ_clave_int NOT IN (select equ_clave_int from equipo_usuarios) order by equ_nombre");
						$num = mysqli_num_rows($con);
						for($i = 0; $i < $num; $i++)
						{
							$dato = mysqli_fetch_array($con);
							$clave = $dato['equ_clave_int'];
							$equipo = $dato['equ_nombre'];
					?>
						<option value="<?php echo $clave; ?>"><?php echo $equipo; ?></option>
					<?php
						}
					?>
					</select>
				</fieldset>
			</div>
			</td>
			<td align="center">
			<img src="../../images/agregar.png" height="30" width="40" style="cursor:pointer" />
			<img src="../../images/quitar.png" height="30" width="40" style="cursor:pointer" />
			</td>
			<td align="center">
			<div id="quitar" style="height: 180px">
				<fieldset name="Group1">
				<legend>Registros Seleccionados</legend>
					<select class="auto-style8" <?php if($prf <> 'Cliente'){ echo "disabled='disabled'"; } ?> ondblclick="QUITAR('EQUIPO','<?php echo $usuedi; ?>');CONSULTAMODULO('CLIENTES')" name="quitarequ" style="width: 150px; height: 150px;" size="3">
					<?php
						$con = mysqli_query($conectar,"select * from equipo_usuarios eu inner join equipo e on (e.equ_clave_int = eu.equ_clave_int) where eu.usu_clave_int = '".$usuedi."' order by e.equ_nombre");
						$num = mysqli_num_rows($con);
						for($i = 0; $i < $num; $i++)
						{
							$dato = mysqli_fetch_array($con);
							$clave = $dato['eus_clave_int'];
							$equipo = $dato['equ_nombre'];
					?>
						<option value="<?php echo $clave; ?>"><?php echo $equipo; ?></option>
					<?php
						}
					?>
					</select>
				</fieldset>
			</div>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="5">
			<input name="submit" type="button" value="Guardar" onclick="GUARDAR('USUARIO','<?php echo $usuedi; ?>')"  style="width: 348px; height: 25px; cursor:pointer" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="3">
			<div id="datos">
			</div>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
</div>
</form>
</body>
</html>